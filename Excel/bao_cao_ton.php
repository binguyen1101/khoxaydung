<?php
    include("config.php");

    if (isset($_COOKIE['user']) && $_COOKIE['user'] != "" && $_COOKIE['role'] == 2) {
		$token = $_COOKIE['acc_token'];
		$curl = curl_init();
		$data = array();
		curl_setopt($curl, CURLOPT_POST, 1);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
		curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/user_info_employee.php');
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));
		$response = curl_exec($curl);
		curl_close($curl);
		$data_tt = json_decode($response, true);
		$tt_user = $data_tt['data']['user_info_result'];
	}
    
    $id_cty = $tt_user['com_id'];

    $kho = getValue('id','int','GET','');

    $bao_cao_ton_kho = "SELECT DISTINCT `dsvt_id`, `dsvt_maVatTuThietBi`, `dsvt_name`, `dsvt_dateCreate`, `dsvt_kho`, `dsvt_soLuongTon`,
    `dvt_name`, `hsx_name`, `xx_name`
    FROM `danh-sach-vat-tu` 
    LEFT JOIN `don-vi-tinh` ON `danh-sach-vat-tu`.`dsvt_donViTinh` = `don-vi-tinh`.`dvt_id` 
    LEFT JOIN `hang-san-xuat` ON `danh-sach-vat-tu`.`dsvt_hangSanXuat` = `hang-san-xuat`.`hsx_id` 
    LEFT JOIN `xuat-xu` ON `danh-sach-vat-tu`.`dsvt_xuatXu` = `xuat-xu`.`xx_id` 
    LEFT JOIN `nhom-vat-tu-thiet-bi` ON `danh-sach-vat-tu`.`dsvt_nhomVatTuThietBi` = `nhom-vat-tu-thiet-bi`.`nvt_id`
    LEFT JOIN `so-luong-vat-tu` ON `danh-sach-vat-tu`.`dsvt_id` = `so-luong-vat-tu`.`slvt_maVatTuThietBi`
    WHERE `dsvt_check` = 1 AND `dvt_check` = 1 AND `dsvt_id_ct` = $id_cty ";

    $bao_cao_ton_kho = new db_query($bao_cao_ton_kho);

    $responsive = [];
    $i = 0 ;
    while (($item = mysql_fetch_assoc($bao_cao_ton_kho->result))) {
        $check_kho = explode(',',$item['dsvt_kho']);
        if(in_array($kho,$check_kho)){
            $info_k['dsvt_id'] = $item['dsvt_id'];
            $info_k['dsvt_name'] = $item['dsvt_name'];
            $info_k['dsvt_soLuongTon'] = $item['dsvt_soLuongTon'];
            $info_k['hsx_name'] = $item['hsx_name'];
            $info_k['dvt_name'] = $item['dvt_name'];
            $info_k['xx_name'] = $item['xx_name'];
            $info_k['dsvt_kho'] = $item['dsvt_kho'];
            $responsive[$i] = $info_k;
            $i++;
        }

    }

    foreach ($responsive as $key => $value) {
        if (empty($value)) {
           unset($responsive[$key]);
        }
    }

    header("Content-type: application/octet-stream; charset=utf-8");
    header("Content-Disposition: attachment; filename=excel_bao_cao_ton_kho.xls");
    header("Pragma: no-cache");
    header("Expires: 0");

    echo '<table border="1px solid black">';
    echo '<tr><th colspan="7" style="font-size:18px;height:60px;vertical-align: middle;">Báo cáo tồn kho</th></tr>';
?>

<tr class="tit_tbl color_white font_s16 line_h19 font_w500 back_blue">
    <th>STT
        <span class="span_tbody"></span>
    </th>
    <th>Mã vật tư thiết bị
        <span class="span_tbody"></span>
    </th>
    <th>Tên đầy đủ vật tư thiết bị
        <span class="span_tbody"></span>
    </th>
    <th>Đơn vị tính
        <span class="span_tbody"></span>
    </th>
    <th>Hãng sản xuất
        <span class="span_tbody"></span>
    </th>
    <th>Xuất xứ
        <span class="span_tbody"></span>
    </th>
    <th>Số lượng
    </th>
    </tr>
    <?php $i=1; if(!empty($responsive)){
    foreach ($responsive as $val){ ?>
    <tr class="color_grey font_s14 line_h17 font_w400" data-id="<?= $val['dsvt_id']; ?>">
        <td><?= $i++; ?></td>
        <td>VT - <?=$val['dsvt_id'];?></td>
        <td><a href="/danh-sach-vat-tu-thiet-bi-chi-tiet-<?= $val['dsvt_id']; ?>.html" class="color_blue font_w500"><?= $val['dsvt_name'];?></a></td>
        <td><?= $val['dvt_name'];?></td>
        <td style="text-align: left;"><?= $val['hsx_name'];?></td>
        <td><?= $val['xx_name'];?></td>
        <?php $sl = json_decode($val['dsvt_soLuongTon']); ?>
        <td><?= $sl->$kho ?></td>
    </tr>
    <?php }}?>