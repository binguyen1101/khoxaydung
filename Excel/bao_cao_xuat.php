<?php
    include("config.php");

    if(isset($_COOKIE['acc_token']) && isset($_COOKIE['rf_token']) && isset($_COOKIE['role']) && $_COOKIE['role'] == 2){
        $curl = curl_init();
        $token = $_COOKIE['acc_token'];
        curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/list_all_my_partner.php?get_all=true');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer '.$token));
        $response = curl_exec($curl);
        curl_close($curl);

        $data_list = json_decode($response,true);
        $data_list_nv =$data_list['data']['items'];
    }else{
        $curl = curl_init();
        $token = $_COOKIE['acc_token'];
        curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/list_all_employee_of_company.php');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer '.$token));
        $response = curl_exec($curl);
        curl_close($curl);

        $data_list = json_decode($response,true);
        $data_list_nv =$data_list['data']['items'];

    }
    $count = count($data_list_nv);

    $user = [];
    for ($i = 0; $i < count($data_list_nv); $i++){
        $nv = $data_list_nv[$i];
        $user[$nv["ep_id"]] = $nv;
    }

    if (isset($_COOKIE['user']) && $_COOKIE['user'] != "" && $_COOKIE['role'] == 2) {
		$token = $_COOKIE['acc_token'];
		$curl = curl_init();
		$data = array();
		curl_setopt($curl, CURLOPT_POST, 1);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
		curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/user_info_employee.php');
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));
		$response = curl_exec($curl);
		curl_close($curl);
		$data_tt = json_decode($response, true);
		$tt_user = $data_tt['data']['user_info_result'];
	}
    
    $id_cty = $tt_user['com_id'];


    $bao_cao_xuat_kho = "SELECT `kcxl_id`, `kcxl_soPhieu`, `kcxl_trangThai`, `kcxl_hinhThuc`, `kcxl_nguoiTao`,
    `kcxl_ngayHoanThanh`, `kho_name`, `kcxl_khoXuat`, `kcxl_ngayTao`
    FROM `kho-cho-xu-li` INNER JOIN `kho`
    ON `kho-cho-xu-li`.`kcxl_khoXuat` = `kho`.`kho_id`
    WHERE `kho-cho-xu-li`.`kcxl_check` = 1 AND `kcxl_soPhieu` = 'PXK' AND `kcxl_trangThai` = 6 AND `kcxl_id_ct` = $id_cty ";

    $bao_cao_xuat_kho = new db_query($bao_cao_xuat_kho); 

    header("Content-type: application/octet-stream; charset=utf-8");
    header("Content-Disposition: attachment; filename=excel_bao_cao_xuat_kho.xls");
    header("Pragma: no-cache");
    header("Expires: 0");

    echo '<table border="1px solid black">';
    echo '<tr><th colspan="7" style="font-size:18px;height:60px;vertical-align: middle;">Báo cáo xuất kho</th></tr>';
?>

<tr class="tit_tbl color_white font_s16 line_h19 font_w500 back_blue">
    <th>STT
        <span class="span_tbody"></span>
    </th>
    <th>Số phiếu
        <span class="span_tbody"></span>
    </th>
    <th>Trạng thái
        <span class="span_tbody"></span>
    </th>
    <th>Hình thức nhập kho
        <span class="span_tbody"></span>
    </th>
    <th style="width: 225px">Người tạo
        <span class="span_tbody"></span>
    </th>
    <th>Ngày tạo
        <span class="span_tbody"></span>
    </th>
    <th>Ngày hoàn thành
    </th>
    </tr>
    <?php $i=1; while($row = mysql_fetch_assoc($bao_cao_xuat_kho->result)) : ?>
    <tr class="color_grey font_s14 line_h17 font_w400">
        <td><?= $i++; ?></td> 
        <td><a href="/xuat-kho-detail.html?id=<?= $row['kcxl_id']; ?>" class="color_blue font_w500">PXK - <?=$row['kcxl_id'];?></a></td>
        <td class="color_green"><?php 
            if($row['kcxl_trangThai'] == 6){
            echo 'Đã duyệt - Đã xuất kho';
            }
        ?></td>
        <td>
        <?=hinh_thuc_xuat($row['kcxl_hinhThuc'])?>
        </td>
        <td style="text-align: left;">
            <?php 
            $nguoi_tao = $row['kcxl_nguoiTao'];
            $user_id = $user[$nguoi_tao];
            $ten_nguoi_tao = $user_id['ep_name'];
            ?>
            <div class="d_flex flex_start align_c">
                <p><?= $ten_nguoi_tao; ?></p>
            </div>
        </td>
        <td><?php echo date('d/m/Y',strtotime($row['kcxl_ngayTao'])); ?></td>
        <td><?php echo date('d/m/Y',strtotime($row['kcxl_ngayHoanThanh'])); ?></td>
    </tr>
    <?php endwhile; ?>