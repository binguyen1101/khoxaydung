<?php
    include("config.php");
    $id_phieu = getValue('id','int','GET','');

    $chi_tiet_phieu = new db_query("SELECT `kcxl_id`, `kcxl_soPhieu`, `kcxl_trangThai`, `kcxl_ghi_chu`, `kcxl_nguoiTao`, `kcxl_liDoTuChoi`,
    `kcxl_ngayTao`, `kcxl_ngayThucHienDieuChuyen`, `kcxl_ngayYeuCauHoanThanh`, `kcxl_khoNhap`, `kcxl_khoXuat`, 	`kcxl_nguoiThucHien`, `kcxl_nguoiNhan`
    FROM `kho-cho-xu-li`
    WHERE `kcxl_soPhieu` = 'ĐCK' AND `kcxl_id` = $id_phieu AND `kcxl_soPhieu` = 'ĐCK' AND `kcxl_check` = 1
    ");
    $row = mysql_fetch_assoc($chi_tiet_phieu->result);

    $kho_nhap = new db_query("SELECT `kho_name`
    FROM `kho-cho-xu-li`
    INNER JOIN `kho` ON `kho-cho-xu-li`.`kcxl_khoNhap` = `kho`.`kho_id`
    WHERE `kcxl_soPhieu` = 'ĐCK' AND `kcxl_id` = $id_phieu AND `kcxl_check` = 1
    ");
    $kho_name1 = mysql_fetch_assoc($kho_nhap->result);

    $kho_xuat = new db_query("SELECT `kho_name`
    FROM `kho-cho-xu-li`
    INNER JOIN `kho` ON `kho-cho-xu-li`.`kcxl_khoXuat` = `kho`.`kho_id`
    WHERE `kcxl_soPhieu` = 'ĐCK' AND `kcxl_id` = $id_phieu AND `kcxl_check` = 1
    ");
    $kho_name2 = mysql_fetch_assoc($kho_xuat->result);

    $danh_sach_vat_tu = new db_query("SELECT `dsvt_id`,`slvt_idPhieu`, `slvt_maPhieu`, 
    `dsvt_maVatTuThietBi`, `dsvt_name`, `dvt_name`, `hsx_name`, `xx_name`, `slvt_soLuongDieuChuyen`
    FROM `danh-sach-vat-tu`
    INNER JOIN `don-vi-tinh` ON `danh-sach-vat-tu`.`dsvt_donViTinh` = `don-vi-tinh`.`dvt_id`
    INNER JOIN `hang-san-xuat` ON `danh-sach-vat-tu`.`dsvt_hangSanXuat` = `hang-san-xuat`.`hsx_id`
    INNER JOIN `xuat-xu` ON `danh-sach-vat-tu`.`dsvt_xuatXu` = `xuat-xu`.`xx_id`
    INNER JOIN `so-luong-vat-tu` ON `danh-sach-vat-tu`.`dsvt_id` = `so-luong-vat-tu`.`slvt_maVatTuThietBi`
    WHERE `dsvt_check` = 1 AND `slvt_idPhieu` = $id_phieu
    ORDER BY `dsvt_id` ASC
    ");

    if(isset($_COOKIE['acc_token']) && isset($_COOKIE['rf_token']) && isset($_COOKIE['role']) && $_COOKIE['role'] == 2){
        $curl = curl_init();
        $token = $_COOKIE['acc_token'];
        curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/list_all_my_partner.php?get_all=true');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer '.$token));
        $response = curl_exec($curl);
        curl_close($curl);

        $data_list = json_decode($response,true);
        $data_list_nv =$data_list['data']['items'];
    }else{
        $curl = curl_init();
        $token = $_COOKIE['acc_token'];
        curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/list_all_employee_of_company.php');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer '.$token));
        $response = curl_exec($curl);
        curl_close($curl);

        $data_list = json_decode($response,true);
        $data_list_nv =$data_list['data']['items'];

    }
    $count = count($data_list_nv);

    $user = [];
    for ($i = 0; $i < count($data_list_nv); $i++){
        $nv = $data_list_nv[$i];
        $user[$nv["ep_id"]] = $nv;
    }

    if (isset($_COOKIE['user']) && $_COOKIE['user'] != "" && $_COOKIE['role'] == 2) {
		$token = $_COOKIE['acc_token'];
		$curl = curl_init();
		$data = array();
		curl_setopt($curl, CURLOPT_POST, 1);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
		curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/user_info_employee.php');
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));
		$response = curl_exec($curl);
		curl_close($curl);
		$data_tt = json_decode($response, true);
		$tt_user = $data_tt['data']['user_info_result'];
	}
    
    $id_cty = $tt_user['com_id'];

    header("Content-type: application/octet-stream; charset=utf-8");
    header("Content-Disposition: attachment; filename=excel_chi_tiet_phieu_dieu_chuyen.xls");
    header("Pragma: no-cache");
    header("Expires: 0");
?>
<table border="1px solid black">
    <thead>
        <th colspan="2" style="font-size:18px;height:60px;vertical-align: left;">Thông tin chi tiết phiếu điều chuyển kho</th>
    </thead>
    <tr>
        <td>Số phiếu</td>
        <td>ĐCK - <?= $row['kcxl_id'] ?></td>
    </tr>
    <tr>
        <td>Trạng thái</td>
        <td><?= trang_thai($row['kcxl_trangThai']); ?></td>
    </tr>
    <?php if($row['kcxl_trangThai'] == 2){?>
        <tr>
            <td>Lí do từ chối</td>
            <td><?= $row['kcxl_liDoTuChoi']; ?></td>
        </tr>
    <?}?>
    <?php 
        $id_nguoi_tao = $row['kcxl_nguoiTao'];
        $ten_nguoi_tao = $user[$id_nguoi_tao]['ep_name'];
    ?>
    <tr>
        <td>Người tạo</td>
        <td><?= $ten_nguoi_tao; ?></td>
    </tr>
    <tr>
        <td>Ngày tạo</td>
        <td><?= date('d/m/Y',strtotime($row['kcxl_ngayTao'])); ?></td>
    </tr>
    <tr>
        <td>Kho xuất</td>
        <td><?= $kho_name2['kho_name']; ?></td>
    </tr>
    <tr>
        <td>Kho nhập</td>
        <td><?= $kho_name1['kho_name']; ?></td>
    </tr>
    <?php 
        $id_nguoi_giao = $row['kcxl_nguoiThucHien'];
        $ten_nguoi_giao = $user[$id_nguoi_giao]['ep_name'];
        $phong_ban_nguoi_giao = $user[$id_nguoi_giao]['dep_name'];
    ?>
    <tr>
        <td>Người giao hàng</td>
        <td><?= $ten_nguoi_giao; ?></td>
    </tr>
    <tr>
        <td>Phòng ban</td>
        <td><?= explode('-',$phong_ban_nguoi_giao)[0]; ?></td>
    </tr>
    <?php 
        $id_nguoi_nhan = $row['kcxl_nguoiNhan'];
        $ten_nguoi_nhan = $user[$id_nguoi_nhan]['ep_name'];
        $phong_ban_nguoi_nhan = $user[$id_nguoi_nhan]['dep_name'];
    ?>
    <tr>
        <td>Người nhận</td>
        <td><?= $ten_nguoi_nhan; ?></td>
    </tr>
    <tr>
        <td>Phòng ban</td>
        <td><?= explode('-',$phong_ban_nguoi_nhan)[0]; ?></td>
    </tr>
    <tr>
        <td>Ngày thực hiện điều chuyển</td>
        <td><?= date('d/m/Y', strtotime($row['kcxl_ngayThucHienDieuChuyen'])); ?></td>
    </tr>
    <tr>
        <td>Ngày yêu cầu hoàn thành</td>
        <td><?= date('d/m/Y', strtotime($row['kcxl_ngayYeuCauHoanThanh'])); ?></td>
    </tr>
    <tr>
        <td>Ghi chú</td>
        <td><?= $row['kcxl_ghi_chu']; ?></td>
    </tr>
</table>

<?php if($row['kcxl_trangThai'] == 7) {
        $phieu_nhap_xuat = new db_query("SELECT `kcxl_id`,`kcxl_soPhieu`, `kcxl_trangThai`, `kcxl_ghi_chu`, `kcxl_nguoiTao`, `kcxl_ngayTao`, 
        `kcxl_nguoiDuyet`, `kcxl_ngayDuyet`, `kcxl_nguoiThucHien`, `kcxl_ngayHoanThanh`
        FROM `kho-cho-xu-li`
        WHERE `kcxl_check` = 1 AND `kcxl_phieuDieuChuyenKho` = $id_phieu
        AND (`kcxl_soPhieu` = 'PNK' OR `kcxl_soPhieu` = 'PXK')");    
    ?>
<table>
    <tr></tr>
    <tr></tr>
</table>

<table border="1px solid black">
    <thead>
        <th colspan="11" style="font-size:18px; height:60px; vertical-align: left;">Phiếu nhập - phiếu xuất</th>
    </thead>
    <tr class="tit_tbl color_white font_s16 line_h19 font_w500 back_blue">
        <th>STT<span class="span_tbody"></span></th>
        <th>Số phiếu<span class="span_tbody"></span></th>
        <th>Loại phiếu<span class="span_tbody"></span></th>
        <th>Trạng thái<span class="span_tbody"></span></th>
        <th>Ghi chú<span class="span_tbody"></span></th>
        <th>Người tạo<span class="span_tbody"></span></th>
        <th>Ngày tạo<span class="span_tbody"></span></th>
        <th>Người duyệt<span class="span_tbody"></span></th>
        <th>Ngày duyệt<span class="span_tbody"></span></th>
        <th>Người hoàn thành<span class="span_tbody"></span></th>
        <th>Ngày hoàn thành</th>
    </tr>
    <?php $i++; while($row_nhap_xuat = mysql_fetch_assoc($phieu_nhap_xuat->result)) : ?>
        <tr>
            <td><?= $i++;?></td>
            <td><?= ($row_nhap_xuat['kcxl_soPhieu'] == "PNK") ? "PNK - " : "PXK - " ?><?=$row_nhap_xuat['kcxl_id']?></td>
            <td><?= loai_phieu($row_nhap_xuat['kcxl_soPhieu']); ?></td>
            <td class="<?=trang_thai_color($row_nhap_xuat['kcxl_trangThai'])?>"><?= trang_thai($row_nhap_xuat['kcxl_trangThai']); ?></td>
            <td><?= $row_nhap_xuat['kcxl_ghi_chu']; ?></td>
            <td style="text-align: left;">
                <?php 
                    $id_nguoi_tao_p = $row_nhap_xuat['kcxl_nguoiTao'];
                    $ten_nguoi_tao_p = $user[$id_nguoi_tao_p]['ep_name'];
                    $phong_ban_nguoi_tao_p = $user[$id_nguoi_tao_p]['dep_name'];
                ?>
                <div class="d_flex flex_start align_c">
                <p><?= $ten_nguoi_tao_p; ?></p>
                </div>
                </td>
            <td><?= date('d/m/Y',strtotime($row_nhap_xuat['kcxl_ngayTao'])); ?></td>
            <td style="text-align: left;">
                <?php 
                    $id_nguoi_duyet = $row_nhap_xuat['kcxl_nguoiDuyet'];
                    $ten_nguoi_duyet = $user[$id_nguoi_duyet]['ep_name'];
                    $phong_ban_nguoi_duyet = $user[$id_nguoi_duyet]['dep_name'];
                ?>
                <div class="d_flex flex_start align_c">
                <p><?= $ten_nguoi_duyet; ?></p>
                </div>
            </td>
            <td><?= date('d/m/Y',strtotime($row_nhap_xuat['kcxl_ngayDuyet'])); ?></td>
            <td style="text-align: left;">
                <?php 
                    $id_nguoi_ht = $row_nhap_xuat['kcxl_nguoiHoanThanh'];
                    $ten_nguoi_ht = $user[$id_nguoi_ht]['ep_name'];
                    $phong_ban_nguoi_ht = $user[$id_nguoi_ht]['dep_name'];
                ?>
                    <div class="d_flex flex_start align_c">
                    <p><?= $ten_nguoi_ht; ?></p>
                    </div>
            </td>
            <td><?= date('d/m/Y',strtotime($row_nhap_xuat['kcxl_ngayHoanThanh'])); ?></td>
        </tr>
    <?php endwhile; ?>
</table>
<?}?>

<table>
    <tr></tr>
    <tr></tr>
</table>

<table border="1px solid black">
    <?php
        $danh_sach_vat_tu2 = new db_query("SELECT `kcxl_id`, `kcxl_soPhieu`, `kcxl_trangThai`, `dsvt_id`,`slvt_idPhieu`, `slvt_maPhieu`, 
        `dsvt_maVatTuThietBi`, `dsvt_name`, `dvt_name`, `hsx_name`, `xx_name`, `slvt_soLuongDieuChuyen`, `slvt_soLuongNhapKho`, `slvt_soLuongXuatKho`
        FROM `danh-sach-vat-tu`
        INNER JOIN `don-vi-tinh` ON `danh-sach-vat-tu`.`dsvt_donViTinh` = `don-vi-tinh`.`dvt_id`
        INNER JOIN `hang-san-xuat` ON `danh-sach-vat-tu`.`dsvt_hangSanXuat` = `hang-san-xuat`.`hsx_id`
        INNER JOIN `xuat-xu` ON `danh-sach-vat-tu`.`dsvt_xuatXu` = `xuat-xu`.`xx_id`
        INNER JOIN `so-luong-vat-tu` ON `danh-sach-vat-tu`.`dsvt_id` = `so-luong-vat-tu`.`slvt_maVatTuThietBi`
        INNER JOIN `kho-cho-xu-li` ON `so-luong-vat-tu`.`slvt_idPhieu` = `kho-cho-xu-li`.`kcxl_id`
        WHERE `dsvt_check` = 1 AND `slvt_idPhieu` = $id_phieu
        ORDER BY `dsvt_id` ASC
        ");
    ?>
    <?php if($row['kcxl_trangThai'] == 1 || $row['kcxl_trangThai'] == 2) {?>
        <thead>
            <th colspan="7" style="font-size:18px; height:60px; vertical-align: left;">Danh sách vật tư</th>
        </thead>
        <tr class="tit_tbl color_white font_s16 line_h19 font_w500 back_blue">
            <th>STT<span class="span_tbody"></span></th>
            <th>Mã vật tư thiết bị<span class="span_tbody"></span></th>
            <th>Tên đầy đủ vật tư thiết bị<span class="span_tbody"></span></th>
            <th>Đơn vị tính<span class="span_tbody"></span></th>
            <th>Hãng sản xuất<span class="span_tbody"></span></th>
            <th>Xuất xứ<span class="span_tbody"></span></th>
            <th>Số lượng yêu cầu</th>
        </tr>
        <?php $i=1; while($row_vt1 = mysql_fetch_assoc($danh_sach_vat_tu->result)) { ?>
            <tr class="color_grey font_s14 line_h17 font_w400" data-id="<?= $row_vt1['dsvt_id']; ?>">
                <td><?= $i++; ?></td>
                <td>VT - <?=$row_vt1['dsvt_id']; ?></td>
                <td class="color_blue font_w500" style="text-align: left;"><?= $row_vt1['dsvt_name']; ?></td>
                <td><?= $row_vt1['dvt_name']; ?></td>
                <td style="text-align: left;"><?= $row_vt1['hsx_name']; ?></td>
                <td><?= $row_vt1['xx_name']; ?></td>
                <td style="text-align: right;"><?= $row_vt1['slvt_soLuongDieuChuyen']; ?></td>
            </tr>
        <?php } 
    }else{?>
    <thead>
        <th colspan="9" style="font-size:18px; height:60px; vertical-align: left;">Danh sách vật tư</th>
    </thead>
    <tr class="tit_tbl color_white font_s16 line_h19 font_w500 back_blue">
        <th>STT<span class="span_tbody"></span></th>
        <th>Mã vật tư thiết bị<span class="span_tbody"></span></th>
        <th>Tên đầy đủ vật tư thiết bị<span class="span_tbody"></span></th>
        <th>Đơn vị tính<span class="span_tbody"></span></th>
        <th>Hãng sản xuất<span class="span_tbody"></span></th>
        <th>Xuất xứ<span class="span_tbody"></span></th>
        <th>Số lượng yêu cầu<span class="span_tbody"></span></th>
        <th>Số lượng thực tế xuất kho<span class="span_tbody"></span></th>
        <th>Số lượng thực tế nhập kho</th>
    </tr>
    <?php $j=1; while($row_ds_vt = mysql_fetch_assoc($danh_sach_vat_tu2->result)) {?>
        <tr class="color_grey font_s14 line_h17 font_w400" data-id="<?= $row_ds_vt['dsvt_id']; ?>">
            <td><?= $j++; ?></td>
            <td>VT - <?=$row_ds_vt['dsvt_id']; ?></td>
            <td class="color_blue font_w500" style="text-align: left;"><?= $row_ds_vt['dsvt_name']; ?></td>
            <td><?= $row_ds_vt['dvt_name']; ?></td>
            <td style="text-align: left;"><?= $row_ds_vt['hsx_name']; ?></td>
            <td><?= $row_ds_vt['xx_name']; ?></td>
            <td style="text-align: right;"><?= $row_ds_vt['slvt_soLuongDieuChuyen']; ?></td>
            <?php
                $id_vattu = $row_ds_vt['dsvt_id'];
            
                $sl_thuc_te_xuat_sql = new db_query("SELECT `slvt_soLuongXuatKho` FROM `so-luong-vat-tu` WHERE `slvt_maVatTuThietBi` = $id_vattu AND `slvt_checkDieuChuyen` = $id_phieu AND `slvt_id_ct` = $id_cty AND `slvt_maPhieu` = 'PXK'");
                $sl_thuc_te_xuat = mysql_fetch_assoc($sl_thuc_te_xuat_sql->result)['slvt_soLuongXuatKho'];

                $sl_thuc_te_nhap_sql = new db_query("SELECT `slvt_soLuongNhapKho` FROM `so-luong-vat-tu` WHERE `slvt_maVatTuThietBi` = $id_vattu AND `slvt_checkDieuChuyen` = $id_phieu AND `slvt_id_ct` = $id_cty AND `slvt_maPhieu` = 'PNK'");
                $sl_thuc_te_nhap = mysql_fetch_assoc($sl_thuc_te_nhap_sql->result)['slvt_soLuongNhapKho'];
            ?>
            <td style="text-align: right;"><?=$sl_thuc_te_xuat?></td>
            <td style="text-align: right;"><?=$sl_thuc_te_nhap?></td>
        </tr>
    <?php }}?>
</table>