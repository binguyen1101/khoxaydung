<?php
include("config.php");
$id_phieu = getValue('id', 'int', 'GET', '');


if (isset($_COOKIE['acc_token']) && isset($_COOKIE['rf_token']) && isset($_COOKIE['role']) && $_COOKIE['role'] == 2) {
  $curl = curl_init();
  $token = $_COOKIE['acc_token'];
  curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/list_all_my_partner.php?get_all=true');
  curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
  curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));
  $response = curl_exec($curl);
  curl_close($curl);

  $data_list = json_decode($response, true);
  $data_list_nv = $data_list['data']['items'];
} else {
  $curl = curl_init();
  $token = $_COOKIE['acc_token'];
  curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/list_all_employee_of_company.php');
  curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
  curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));
  $response = curl_exec($curl);
  curl_close($curl);

  $data_list = json_decode($response, true);
  $data_list_nv = $data_list['data']['items'];
}
$count = count($data_list_nv);

$user = [];
for ($i = 0; $i < count($data_list_nv); $i++) {
  $nv = $data_list_nv[$i];
  $user[$nv["ep_id"]] = $nv;
}

if (isset($_COOKIE['user']) && $_COOKIE['user'] != "" && $_COOKIE['role'] == 2) {
  $token = $_COOKIE['acc_token'];
  $curl = curl_init();
  $data = array();
  curl_setopt($curl, CURLOPT_POST, 1);
  curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
  curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/user_info_employee.php');
  curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
  curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));
  $response = curl_exec($curl);
  curl_close($curl);
  $data_tt = json_decode($response, true);
  $tt_user = $data_tt['data']['user_info_result'];
}

$id_cty = $tt_user['com_id'];

$id_nguoi_lg = $_SESSION['ep_id'];

// API vật tư đơn hàng
$curl_vtdh = curl_init();
curl_setopt($curl_vtdh, CURLOPT_POST, 1);
curl_setopt($curl_vtdh, CURLOPT_URL, 'https://phanmemquanlycungung.timviec365.vn/api/vat_tu_dh.php');
curl_setopt($curl_vtdh, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($curl_vtdh, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
curl_setopt($curl_vtdh, CURLOPT_POSTFIELDS, [
  'com_id' => $id_cty,
  'dh_id' => $info_item['kcxl_donHang'],
]);
$response_vtdh = curl_exec($curl_vtdh);
curl_close($curl_vtdh);
$data_list_vtdh = json_decode($response_vtdh, true);
$list_vtdh = $data_list_vtdh['data']['items'];

$list_vtdh1 = [];
for ($j = 0; $j < count($list_vtdh); $j++) {
  $list_vtdh_p = $list_vtdh[$j];
  $list_vtdh1[$list_vtdh_p["id_vat_tu"]] = $list_vtdh_p;
}

// API vật tư theo phiếu yêu cầu
$curl_vt_yc = curl_init();
curl_setopt($curl_vt_yc, CURLOPT_POST, 1);
curl_setopt($curl_vt_yc, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($curl_vt_yc, CURLOPT_URL, "https://phanmemquanlycungung.timviec365.vn/api/vat_tu_yc.php");
curl_setopt($curl_vt_yc, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
curl_setopt($curl_vt_yc, CURLOPT_POSTFIELDS, [
  'id_phieu' => $id_pyc,
]);
$response_vt_yc = curl_exec($curl_vt_yc);
curl_close($curl_vt_yc);
$emp_json = json_decode($response_vt_yc, true);
$emp_arr = $emp_json['data']['items'];
$emp_vt_yc = [];
for ($i = 0; $i < count($emp_arr); $i++) {
  $vt_yc = $emp_arr[$i];
  $emp_vt_yc[$vt_yc["id_vat_tu"]] = $vt_yc;
}

// API công trình
$curl_ct = curl_init();
curl_setopt($curl_ct, CURLOPT_POST, 1);
curl_setopt($curl_ct, CURLOPT_URL, 'https://phanmemquanlycongtrinh.timviec365.vn/api/congtrinh.php');
curl_setopt($curl_ct, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($curl_ct, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
curl_setopt($curl_ct, CURLOPT_POSTFIELDS, [
  'id_com' => $id_cty,
]);
$response_ct = curl_exec($curl_ct);
curl_close($curl_ct);
$data_list_ct = json_decode($response_ct, true);
$list_ct = $data_list_ct['data']['items'];
$list_ct_arr = [];
for ($i = 0; $i < count($list_ct); $i++) {
  $ctr = $list_ct[$i];
  $list_ct_arr[$ctr["ctr_id"]] = $ctr;
}

$chi_tiet_phieuKK = new db_query("SELECT *, `kho_name` FROM `kho-cho-xu-li` 
    LEFT JOIN `kho` ON `kcxl_khoThucHienKiemKe` = `kho_id`
    WHERE `kcxl_soPhieu` = 'PKK' AND `kcxl_id` = $id_phieu  AND `kcxl_check`='1'
    ORDER BY `kcxl_id` ASC
    ");


$info_item = mysql_fetch_assoc($chi_tiet_phieuKK->result);


$kho_nhap = new db_query("SELECT `kho_name`,`kcxl_ngayNhapKho` FROM `kho-cho-xu-li`
        INNER JOIN `kho` ON `kho-cho-xu-li`.`kcxl_khoNhap` = `kho`.`kho_id`
        WHERE `kcxl_soPhieu` = 'PXK' AND `kcxl_id` = $id_phieu AND `kho_id_ct` = $id_cty
        ");
$nhap = mysql_fetch_assoc($kho_nhap->result);

$kho_xuat = new db_query("SELECT `kho_name`,`kcxl_ngayXuatKho`
        FROM `kho-cho-xu-li`
        INNER JOIN `kho` ON `kho-cho-xu-li`.`kcxl_khoXuat` = `kho`.`kho_id`
        WHERE `kcxl_soPhieu` = 'PXK' AND `kcxl_id` = $id_phieu AND `kho_id_ct` = $id_cty
        ");
$xuat = mysql_fetch_assoc($kho_xuat->result);


header("Content-type: application/octet-stream; charset=utf-8");
header("Content-Disposition: attachment; filename=excel_chi_tiet_phieu_nhap.xls");
header("Pragma: no-cache");
header("Expires: 0");
?>
<table border="1px solid black">
  <thead>
    <th colspan="2" style="font-size:18px;height:60px;vertical-align: left;">Thông tin chi tiết phiếu xuất</th>
  </thead>
  <?php
  $id_nguoi_nhan = $info_item['kcxl_nguoiNhan'];
  $id_nguoi_giao = $info_item['kcxl_nguoiThucHien'];
  $ten_nguoi_nhan = $user[$id_nguoi_nhan]['ep_name'];
  $ten_nguoi_giao = $user[$id_nguoi_giao]['ep_name'];
  $phong_ban_nguoi_nhan = $user[$id_nguoi_nhan]['dep_name'];
  $phong_ban_nguoi_giao = $user[$id_nguoi_giao]['dep_name'];
  ?>
  <tr>
    <td>Số phiếu</td>
    <td>PXK - <?= $info_item['kcxl_id']; ?></td>
  </tr>
  <tr>
    <td>Trạng thái</td>
    <td><?= trang_thai($info_item['kcxl_trangThai']); ?></td>
  </tr>
  <?php if ($info_item['kcxl_trangThai'] == 10) { ?>
    <tr>
      <td>Lí do từ chối</td>
      <td><?= $info_item['kcxl_liDoTuChoi']; ?></td>
    </tr>
  <? } ?>
  <?php
  $id_nguoi_tao = $info_item['kcxl_nguoiTao'];
  $ten_nguoi_tao = $user[$id_nguoi_tao]['ep_name'];
  ?>
  <tr>
    <td>Người tạo</td>
    <td><?= $ten_nguoi_tao; ?></td>
  </tr>
  <tr>
    <td>Ngày tạo</td>
    <td><?= date('d/m/Y', strtotime($info_item['kcxl_ngayTao'])); ?></td>
  </tr>
  <tr>
    <td>Kho thực hiện kiểm kê</td>
    <td><?= $info_item['kho_name']; ?></td>
  </tr>
  <tr>
    <td>Người thực hiện kiểm kê</td>
    <td><?= $ten_nguoi_giao ?></td>
  </tr>
  <tr>
    <td>Ngày thực hiện kiểm kê</td>
    <td><?= date('d/m/Y', strtotime($info_item['kcxl_ngayThucHienKiemKe'])); ?></td>
  </tr>
  <tr>
    <td>Giờ thực hiện kiểm kê</td>
    <td><?= $info_item['kcxl_thoiGianThucHien'] ?></td>
  </tr>
  <tr>
    <td>Ngày yêu cầu hoàn thành</td>
    <td><?= date('d/m/Y', strtotime($info_item['kcxl_ngayYeuCauHoanThanh'])); ?></td>
  </tr>
  <tr>
    <td>Giờ yêu cầu hoàn thành</td>
    <td><?= $info_item['kcxl_thoiGianHoanThanh'] ?></td>
  </tr>
  <!--  -->
  <tr>
    <td>Ghi chú</td>
    <td><?= $info_item['kcxl_ghi_chu']; ?></td>
  </tr>

  <?php if ($info_item['kcxl_trangThai'] == 8 || $info_item['kcxl_trangThai'] == 10 || $info_item['kcxl_trangThai'] == 11 || $info_item['kcxl_trangThai'] == 12 || $info_item['kcxl_trangThai'] == 14) { ?>
    <?php
    $id_nguoi_duyet = $info_item['kcxl_nguoiDuyet'];
    $ten_nguoi_duyet = $user[$id_nguoi_duyet]['ep_name'];
    ?>
    <tr>
      <td>Người duyệt</td>
      <td><?= $ten_nguoi_duyet; ?></td>
    </tr>
    <tr>
      <td>Ngày duyệt</td>
      <td><?= $info_item['kcxl_ngayDuyet']; ?></td>
    </tr>
  <? } ?>

  <?php if ($info_item['kcxl_trangThai'] == 9 || $info_item['kcxl_trangThai'] == 10 || $info_item['kcxl_trangThai'] == 11 || $info_item['kcxl_trangThai'] == 12) {
    $id_nguoi_ht = $info_item['kcxl_nguoiHoanThanh'];
    $ten_nguoi_ht = $user[$id_nguoi_ht]['ep_name'];
  ?>
    <tr>
      <td>Người hoàn thành</td>
      <td><?= $ten_nguoi_ht; ?></td>
    </tr>
    <tr>
      <td>Ngày hoàn thành</td>
      <td><?= $info_item['kcxl_ngayHoanThanh']; ?></td>
    </tr>
  <? } ?>
</table>

<table>
  <tr></tr>
  <tr></tr>
</table>


<table border="1px solid black">
  <?php
  $vattu = new db_query("SELECT `kcxl_id`, `kcxl_soPhieu`, `kcxl_trangThai`, `dsvt_id`,`slvt_idPhieu`, `slvt_maPhieu`, 
                `dsvt_maVatTuThietBi`, `dsvt_name`, `dvt_name`, `hsx_name`, `xx_name`, `slvt_soLuongKiemKe`, `slvt_so_luong_he_thong`,`slvt_slTon`,`slvt_ghiChu`
                FROM `danh-sach-vat-tu`
                LEFT JOIN `don-vi-tinh` ON `dsvt_donViTinh` =`dvt_id`
                LEFT JOIN `hang-san-xuat` ON `dsvt_hangSanXuat` = `hsx_id`
                LEFT JOIN `xuat-xu` ON `dsvt_xuatXu` = `xx_id`
                LEFT JOIN `so-luong-vat-tu` ON `dsvt_id` = `slvt_maVatTuThietBi`
                LEFT JOIN `kho-cho-xu-li` ON `slvt_idPhieu` = `kcxl_id`
                WHERE `dsvt_check` = '1' AND `slvt_idPhieu` = '$id_phieu' AND `dsvt_id_ct` = '$id_cty'
                ORDER BY `dsvt_id` ASC ");
  $stt = 1;
  ?>
  <thead>
    <th colspan="10" style="font-size:18px; height:60px; text-align: left;">Danh sách vật tư</th>
  </thead>
  <tr class="tit_tbl color_white font_s16 line_h19 font_w500 back_blue">
    <td rowspan="2" style="width:9%">Mã vật tư thiết bị</td>
    <td rowspan="2">Tên đầy đủ vật tư thiết bị</td>
    <td rowspan="2">Đơn vị tính</td>
    <td rowspan="2">Hãng sản xuất</td>
    <td rowspan="2">Xuất xứ</td>
    <td rowspan="2">Số lượng trên hệ thống</td>
    <td rowspan="2">Số lượng kiểm kê</td>
    <td colspan="2">Chênh lệch </td>
    <td rowspan="2" style="border:1px solid #CCCCCC;width:24%">Ghi chú</td>
  </tr>
  <tr class="tit_tbl color_white font_s16 line_h19 font_w500 back_blue">
    <td style="border-radius:0%;">Thừa</td>
    <td style="border-radius:0%;border: 1px solid #CCCCCC;">Thiếu</td>
  </tr>
  <? while ($item1 = mysql_fetch_assoc($vattu->result)) { ?>
    <tr class="color_grey font_s14 line_h17 font_w400" data="<?= $item1['dsvt_id'] ?>">
      <td><?= phieu($item1['dsvt_id'], $item1['dsvt_maVatTuThietBi']) ?></td>
      <td class="color_blue font_w500" style="text-align: left;"><?= $item1['dsvt_name']; ?></td>
      <td><?= $item1['dvt_name']; ?></td>
      <td style="text-align: left;"><?= $item1['hsx_name']; ?></td>
      <td><?= $item1['xx_name']; ?></td>
      <td style="text-align: right;" class="slht"><?= $item1['slvt_so_luong_he_thong']  ?></td>
      <td class="font_s14 line_h17 color_grey font_w400 slkk" style="text-align: right;"><?= $item1['slvt_soLuongKiemKe'] ?></td>
      <td>
        <? if ($item1['slvt_soLuongKiemKe'] > $item1['slvt_so_luong_he_thong']) {
          echo ($item1['slvt_soLuongKiemKe'] - $item1['slvt_so_luong_he_thong']) ?>
        <? } else if ($item1['slvt_soLuongKiemKe'] < $item1['slvt_so_luong_he_thong']) {
          echo ('') ?>
        <?php } else if ($item1['slvt_soLuongKiemKe'] = $item1['slvt_so_luong_he_thong']) {
          echo ('0') ?>
        <?php } ?>
      </td>
      <td>
        <? if ($item1['slvt_soLuongKiemKe'] < $item1['slvt_so_luong_he_thong']) {
          echo ($item1['slvt_so_luong_he_thong'] - $item1['slvt_soLuongKiemKe']) ?>
        <? } else if ($item1['slvt_soLuongKiemKe'] > $item1['slvt_so_luong_he_thong']) {
          echo ('') ?>
        <?php } else if ($item1['slvt_soLuongKiemKe'] = $item1['slvt_so_luong_he_thong']) {
          echo ('0') ?>
        <?php } ?>
      </td>
      <td><?= $item1['slvt_ghiChu']; ?></td>
    </tr>
  <? }
  ?>
</table>