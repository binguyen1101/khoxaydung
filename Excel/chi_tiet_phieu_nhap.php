<?php
    include("config.php");
    $id_phieu = getValue('id', 'int', 'GET', '');

    $chi_tiet_phieunhap = new db_query("SELECT * FROM `kho-cho-xu-li` 
    WHERE`kcxl_soPhieu` = 'PNK' AND `kcxl_id` = $id_phieu AND `kcxl_check`='1'
    ");
    $item = mysql_fetch_assoc($chi_tiet_phieunhap->result);
    $id_dh = $item['kcxl_donHang'];
    $id_pyc = $item['kcxl_phieuYeuCau'];


    $kho_nhap = new db_query("SELECT `kho_name`,`kcxl_ngayNhapKho` FROM `kho-cho-xu-li`
        INNER JOIN `kho` ON `kho-cho-xu-li`.`kcxl_khoNhap` = `kho`.`kho_id`
        WHERE `kcxl_soPhieu` = 'PNK' AND `kcxl_id` = $id_phieu
        ");
    $nhap = mysql_fetch_assoc($kho_nhap->result);

    $kho_xuat = new db_query("SELECT `kho_name`,`kcxl_ngayXuatKho`
        FROM `kho-cho-xu-li`
        INNER JOIN `kho` ON `kho-cho-xu-li`.`kcxl_khoXuat` = `kho`.`kho_id`
        WHERE `kcxl_soPhieu` = 'PNK' AND `kcxl_id` = $id_phieu
        ");
    $xuat = mysql_fetch_assoc($kho_xuat->result);

    if(isset($_COOKIE['acc_token']) && isset($_COOKIE['rf_token']) && isset($_COOKIE['role']) && $_COOKIE['role'] == 2){
        $curl = curl_init();
        $token = $_COOKIE['acc_token'];
        curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/list_all_my_partner.php?get_all=true');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer '.$token));
        $response = curl_exec($curl);
        curl_close($curl);

        $data_list = json_decode($response,true);
        $data_list_nv =$data_list['data']['items'];
    }else{
        $curl = curl_init();
        $token = $_COOKIE['acc_token'];
        curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/list_all_employee_of_company.php');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer '.$token));
        $response = curl_exec($curl);
        curl_close($curl);

        $data_list = json_decode($response,true);
        $data_list_nv =$data_list['data']['items'];

    }
    $count = count($data_list_nv);

    $user = [];
    for ($i = 0; $i < count($data_list_nv); $i++){
        $nv = $data_list_nv[$i];
        $user[$nv["ep_id"]] = $nv;
    }

    if (isset($_COOKIE['user']) && $_COOKIE['user'] != "" && $_COOKIE['role'] == 2) {
		$token = $_COOKIE['acc_token'];
		$curl = curl_init();
		$data = array();
		curl_setopt($curl, CURLOPT_POST, 1);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
		curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/user_info_employee.php');
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));
		$response = curl_exec($curl);
		curl_close($curl);
		$data_tt = json_decode($response, true);
		$tt_user = $data_tt['data']['user_info_result'];
	}

    $id_cty = $tt_user['com_id'];

    $id_nguoi_lg = $_SESSION['ep_id'];

    // API vật tư đơn hàng
    $curl_vtdh = curl_init();
    curl_setopt($curl_vtdh, CURLOPT_POST, 1);
    curl_setopt($curl_vtdh, CURLOPT_URL, 'https://phanmemquanlycungung.timviec365.vn/api/vat_tu_dh.php');
    curl_setopt($curl_vtdh, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl_vtdh, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($curl_vtdh, CURLOPT_POSTFIELDS, [
        'com_id' => $id_cty,
        'dh_id' => $item['kcxl_donHang'],
    ]);
    $response_vtdh = curl_exec($curl_vtdh);
    curl_close($curl_vtdh);
    $data_list_vtdh = json_decode($response_vtdh, true);
    $list_vtdh = $data_list_vtdh['data']['items'];

    $list_vtdh1 = [];
    for ($j = 0; $j < count($list_vtdh); $j++) {
        $list_vtdh_p = $list_vtdh[$j];
        $list_vtdh1[$list_vtdh_p["id_vat_tu"]] = $list_vtdh_p;
    }

    // API vật tư theo phiếu yêu cầu
    $curl_vt_yc = curl_init();
    curl_setopt($curl_vt_yc, CURLOPT_POST, 1);
    curl_setopt($curl_vt_yc, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl_vt_yc, CURLOPT_URL, "https://phanmemquanlycungung.timviec365.vn/api/vat_tu_yc.php");
    curl_setopt($curl_vt_yc, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($curl_vt_yc, CURLOPT_POSTFIELDS, [
        'id_phieu' => $id_pyc,
    ]);
    $response_vt_yc = curl_exec($curl_vt_yc);
    curl_close($curl_vt_yc);
    $emp_json = json_decode($response_vt_yc, true);
    $emp_arr = $emp_json['data']['items'];
    $emp_vt_yc = [];
    for ($i = 0; $i < count($emp_arr); $i++) {
        $vt_yc = $emp_arr[$i];
        $emp_vt_yc[$vt_yc["id_vat_tu"]] = $vt_yc;
    }

    // API công trình
    $curl_ct = curl_init();
    curl_setopt($curl_ct, CURLOPT_POST, 1);
    curl_setopt($curl_ct, CURLOPT_URL, 'https://phanmemquanlycongtrinh.timviec365.vn/api/congtrinh.php');
    curl_setopt($curl_ct, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl_ct, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($curl_ct, CURLOPT_POSTFIELDS, [
        'id_com' => $id_cty,
    ]);
    $response_ct = curl_exec($curl_ct);
    curl_close($curl_ct);
    $data_list_ct = json_decode($response_ct, true);
    $list_ct = $data_list_ct['data']['items'];
    $list_ct_arr = [];
    for ($i = 0; $i < count($list_ct); $i++) {
        $ctr = $list_ct[$i];
        $list_ct_arr[$ctr["ctr_id"]] = $ctr;
    }
    

    header("Content-type: application/octet-stream; charset=utf-8");
    header("Content-Disposition: attachment; filename=excel_chi_tiet_phieu_nhap.xls");
    header("Pragma: no-cache");
    header("Expires: 0");
?>
<table border="1px solid black">
    <thead>
        <th colspan="2" style="font-size:18px;height:60px;vertical-align: left;">Thông tin chi tiết phiếu nhập</th>
    </thead>
    <tr>
        <td>Số phiếu</td>
        <td>PNK - <?= $item['kcxl_id']; ?></td>
    </tr>
    <tr>
        <td>Hình thức nhập kho</td>
        <td><?= hinh_thuc_nhap($item['kcxl_hinhThuc']); ?></td>
    </tr>
    <?php 
        $id_nguoi_tao = $item['kcxl_nguoiTao'];
        $ten_nguoi_tao = $user[$id_nguoi_tao]['ep_name'];
    ?>
    <tr>
        <td>Người tạo</td>
        <td><?= $ten_nguoi_tao; ?></td>
    </tr>
    <tr>
        <td>Ngày tạo</td>
        <td><?= date('d/m/Y', strtotime($item['kcxl_ngayTao'])); ?></td>
    </tr>
    <?php if($item['kcxl_hinhThuc'] != "NK5"){?>
        <tr>
            <td>
                <?php 
                    $loaiPhieu = $item['kcxl_hinhThuc'];
                    switch($loaiPhieu){
                        case "NK1":
                            $loaiPhieu = "Đơn hàng";
                            echo $loaiPhieu;
                            break;
                        case "NK2":
                            $loaiPhieu = "Phiếu điều chuyển kho";
                            echo $loaiPhieu;
                            break;
                        case "NK3":
                            $loaiPhieu = "Công trình";
                            echo $loaiPhieu;
                            break;
                        case "NK4":
                            $loaiPhieu = "Phiếu yêu cầu";
                            echo $loaiPhieu;
                            break;
                    }
                ?>
            </td>
            <td>
                <?php if($item['kcxl_hinhThuc']=="NK1") {?>
                    ĐH - <?=$item['kcxl_donHang']?>
                <?}?>
                <?php if($item['kcxl_hinhThuc']=="NK2") {?>
                    ĐCK - <?=$item['kcxl_phieuDieuChuyenKho']?>
                <?}?>
                <?php if($item['kcxl_hinhThuc']=="NK3") {?>
                    <?=$list_ct_arr[$item['kcxl_congTrinh']]['ctr_name']?>
                <?}?>
                <?php if($item['kcxl_hinhThuc']=="NK4") {?>
                    PYC - <?=$item['kcxl_phieuYeuCau']?>
                <?}?>
            </td>
        </tr>
    <? }?>
    <tr>
        <td>Trạng thái</td>
        <td><?= trang_thai($item['kcxl_trangThai']); ?></td>
    </tr>

    <?php if ($item['kcxl_trangThai'] == 2) { ?>
        <tr>
            <td>Lí do từ chối</td>
            <td><?= $item['kcxl_liDoTuChoi']; ?></td>
        </tr>
    <? } ?>

    <!-- Nhập theo biên bản giao hàng -->
    <?php if ($item['kcxl_hinhThuc'] == 'NK1') { ?>
        <tr>
            <td>Nhà cung cấp</td>
            <td><?= $item['kcxl_nhaCungCap'] ?></td>
        </tr>
        <tr>
            <td>Người giao hàng</td>
            <td><?= $item['kcxl_nguoiThucHien'] ?></td>
        </tr>
        <?php 
            $id_nguoi_nhan = $item['kcxl_nguoiNhan'];
            $ten_nguoi_nhan = $user[$id_nguoi_nhan]['ep_name'];
            $phong_ban_nguoi_nhan = $user[$id_nguoi_nhan]['dep_name'];
        ?>
        <tr>
            <td>Người nhận</td>
            <td><?= $ten_nguoi_nhan ?></td>
        </tr>
        <tr>
            <td>Phòng ban</td>
            <td><?= explode('-',$phong_ban_nguoi_nhan)[0]; ?></td>
        </tr>
        <tr>
            <td>Kho nhập</td>
            <td><?= $nhap['kho_name']; ?></td>
        </tr>
        <tr>
            <td>Ngày nhập kho</td>
            <td><?= $item['kcxl_ngayNhapKho']; ?></td>
        </tr>

    <? }?>

    <!-- Nhập điều chuyển  -->
    <?php if ($item['kcxl_hinhThuc'] == 'NK2') { ?>
        <tr>
            <td>Kho xuất</td>
            <td><?= $xuat['kho_name'] ?></td>
        </tr>
        <tr>
            <td>Kho nhập</td>
            <td><?= $nhap['kho_name'] ?></td>
        </tr>
        <?php 
            $id_nguoi_giao = $item['kcxl_nguoiThucHien'];
            $ten_nguoi_giao = $user[$id_nguoi_giao]['ep_name'];
            $phong_ban_nguoi_giao = $user[$id_nguoi_giao]['dep_name'];
        ?>
        <tr>
            <td>Người giao</td>
            <td><?= $ten_nguoi_giao ?></td>
        </tr>
        <tr>
            <td>Phòng ban</td>
            <td><?= explode('-',$phong_ban_nguoi_giao)[0]; ?></td>
        </tr>
        <?php 
            $id_nguoi_nhan = $item['kcxl_nguoiNhan'];
            $ten_nguoi_nhan = $user[$id_nguoi_nhan]['ep_name'];
            $phong_ban_nguoi_nhan = $user[$id_nguoi_nhan]['dep_name'];
        ?>
        <tr>
            <td>Người nhận</td>
            <td><?= $ten_nguoi_nhan ?></td>
        </tr>
        <tr>
            <td>Phòng ban</td>
            <td><?= explode('-',$phong_ban_nguoi_nhan)[0]; ?></td>
        </tr>
        <tr>
            <td>Ngày yêu cầu hoàn thành</td>
            <td><?=$item['kcxl_ngayYeuCauHoanThanh']?></td>
        </tr>
        <tr>
            <td>Ngày nhập kho</td>
            <td><?=$item['kcxl_ngayNhapKho']?></td>
        </tr>

    <? }?>

    <!-- Nhập trả lại từ thi công  -->
    <?php if ($item['kcxl_hinhThuc'] == 'NK3') { ?>
        <?php 
            $id_nguoi_giao = $item['kcxl_nguoiThucHien'];
            $ten_nguoi_giao = $user[$id_nguoi_giao]['ep_name'];
            $phong_ban_nguoi_giao = $user[$id_nguoi_giao]['dep_name'];
        ?>
        <tr>
            <td>Người giao</td>
            <td><?= $ten_nguoi_giao ?></td>
        </tr>
        <tr>
            <td>Phòng ban</td>
            <td><?= explode('-',$phong_ban_nguoi_giao)[0]; ?></td>
        </tr>
        <?php 
            $id_nguoi_nhan = $item['kcxl_nguoiNhan'];
            $ten_nguoi_nhan = $user[$id_nguoi_nhan]['ep_name'];
            $phong_ban_nguoi_nhan = $user[$id_nguoi_nhan]['dep_name'];
        ?>
        <tr>
            <td>Người nhận</td>
            <td><?= $ten_nguoi_nhan ?></td>
        </tr>
        <tr>
            <td>Phòng ban</td>
            <td><?= explode('-',$phong_ban_nguoi_nhan)[0]; ?></td>
        </tr>
        <tr>
            <td>Kho nhập</td>
            <td><?= $nhap['kho_name'] ?></td>
        </tr>
        <tr>
            <td>Ngày nhập kho</td>
            <td><?=$item['kcxl_ngayNhapKho']?></td>
        </tr>

    <? }?>

    <!-- Nhập theo yêu cầu vật tư  -->
    <?php if ($item['kcxl_hinhThuc'] == 'NK4') { ?>
        <?php 
            $id_nguoi_giao = $item['kcxl_nguoiThucHien'];
            $ten_nguoi_giao = $user[$id_nguoi_giao]['ep_name'];
            $phong_ban_nguoi_giao = $user[$id_nguoi_giao]['dep_name'];
        ?>
        <tr>
            <td>Người giao</td>
            <td><?= $ten_nguoi_giao ?></td>
        </tr>
        <tr>
            <td>Phòng ban</td>
            <td><?= explode('-',$phong_ban_nguoi_giao)[0]; ?></td>
        </tr>
        <?php 
            $id_nguoi_nhan = $item['kcxl_nguoiNhan'];
            $ten_nguoi_nhan = $user[$id_nguoi_nhan]['ep_name'];
            $phong_ban_nguoi_nhan = $user[$id_nguoi_nhan]['dep_name'];
        ?>
        <tr>
            <td>Người nhận</td>
            <td><?= $ten_nguoi_nhan ?></td>
        </tr>
        <tr>
            <td>Phòng ban</td>
            <td><?= explode('-',$phong_ban_nguoi_nhan)[0]; ?></td>
        </tr>
        <tr>
            <td>Kho nhập</td>
            <td><?= $nhap['kho_name'] ?></td>
        </tr>
        <tr>
            <td>Ngày nhập kho</td>
            <td><?=$item['kcxl_ngayNhapKho']?></td>
        </tr>
    <? }?>

    <!-- Nhập khác  -->
    <?php if ($item['kcxl_hinhThuc'] == 'NK5') { ?>
        <tr>
            <td>Người giao</td>
            <td><?= $item['kcxl_nguoiThucHien']; ?></td>
        </tr>
        <?php 
            $id_nguoi_nhan = $item['kcxl_nguoiNhan'];
            $ten_nguoi_nhan = $user[$id_nguoi_nhan]['ep_name'];
            $phong_ban_nguoi_nhan = $user[$id_nguoi_nhan]['dep_name'];
        ?>
        <tr>
            <td>Người nhận</td>
            <td><?= $ten_nguoi_nhan ?></td>
        </tr>
        <tr>
            <td>Phòng ban</td>
            <td><?= explode('-',$phong_ban_nguoi_nhan)[0]; ?></td>
        </tr>
        <tr>
            <td>Kho nhập</td>
            <td><?= $nhap['kho_name'] ?></td>
        </tr>
        <tr>
            <td>Ngày nhập kho</td>
            <td><?=$item['kcxl_ngayNhapKho']?></td>
        </tr>
    <? }?>

    <!--  -->
    <tr>
        <td>Ghi chú</td>
        <td><?= $item['kcxl_ghi_chu']; ?></td>
    </tr>

    <?php if($item['kcxl_trangThai'] == 3){ ?>
        <?php
            $id_nguoi_duyet = $item['kcxl_nguoiDuyet'];
            $ten_nguoi_duyet = $user[$id_nguoi_duyet]['ep_name'];
        ?>
        <tr>
            <td>Người duyệt</td>
            <td><?= $ten_nguoi_duyet; ?></td>
        </tr>
        <tr>
            <td>Ngày duyệt</td>
            <td><?= $item['kcxl_ngayDuyet']; ?></td>
        </tr>
    <? }?>

    <?php if($item['kcxl_trangThai'] == 4){
            $id_nguoi_duyet = $item['kcxl_nguoiDuyet'];
            $ten_nguoi_duyet = $user[$id_nguoi_duyet]['ep_name'];
            $id_nguoi_ht = $item['kcxl_nguoiHoanThanh'];
            $ten_nguoi_ht = $user[$id_nguoi_ht]['ep_name'];
        ?>
        <tr>
            <td>Người duyệt</td>
            <td><?= $ten_nguoi_duyet; ?></td>
        </tr>
        <tr>
            <td>Ngày duyệt</td>
            <td><?= $item['kcxl_ngayDuyet']; ?></td>
        </tr>
        <tr>
            <td>Người hoàn thành</td>
            <td><?= $ten_nguoi_ht; ?></td>
        </tr>
        <tr>
            <td>Ngày hoàn thành</td>
            <td><?= $item['kcxl_ngayHoanThanh']; ?></td>
        </tr>
    <? }?>
</table>

<table>
    <tr></tr>
    <tr></tr>
</table>


<table border="1px solid black">
    <?php if($item['kcxl_hinhThuc']== 'NK1'){?>
        <?php  $vattu_nhap1 = new db_query("SELECT `dsvt_id`,`slvt_idPhieu`, `slvt_maPhieu`, 
            `dsvt_maVatTuThietBi`, `dsvt_name`, `dvt_name`, `hsx_name`, `xx_name`, `slvt_soLuongNhapKho`,`dsvt_donGia`
            FROM `danh-sach-vat-tu`
            INNER JOIN `don-vi-tinh` ON `dsvt_donViTinh` =`dvt_id`
            INNER JOIN `hang-san-xuat` ON `dsvt_hangSanXuat` = `hsx_id`
            INNER JOIN `xuat-xu` ON `dsvt_xuatXu` = `xx_id`
            INNER JOIN `so-luong-vat-tu` ON `dsvt_id` = `slvt_maVatTuThietBi`
            INNER JOIN `kho-cho-xu-li` ON `slvt_idPhieu` = `kcxl_id`
            WHERE `dsvt_check` = '1' AND `slvt_idPhieu` = '$id_phieu' AND `dsvt_id_ct` = $id_cty
            ORDER BY `dsvt_id` ASC "); 
            $st =1;
        ?>
        <thead>
            <th colspan="10" style="font-size:18px; height:60px; text-align: left;">Danh sách vật tư</th>
        </thead>
        <tr>
            <th>STT
                <span class="span_thread"></span>
            </th>
            <th>Mã vật tư
                <span class="span_thread"></span>
            </th>
            <th>Tên đầy đủ vật tư thiết bị
                <span class="span_thread"></span>
            </th>
            <th>Đơn vị tính
                <span class="span_thread"></span>
            </th>
            <th> Hãng sản xuất
                <span class="span_thread"></span>
            </th>
            <th> Xuất xứ
                <span class="span_thread"></span>
            </th>
            <th> Số lượng theo đơn hàng
                <span class="span_thread"></span>
            </th>
            <th> Số lượng thực tế nhập kho
                <span class="span_thread"></span>
            </th>
            <th>Đơn giá (VNĐ)
                <span class="span_thread"></span>
            </th>
            <th>Thành tiền (VNĐ)
                <span class="span_thread"></span>
            </th>
        </tr>
        <? while($vt_nhap1 = mysql_fetch_assoc($vattu_nhap1 -> result)){?>
        <tr class="color_grey font_s14 line_h17 font_w400" data-id="<?=$vt_nhap1['dsvt_id']?>">
            <td><?= $st++ ?></td>
            <td>VT - <?= $vt_nhap1['dsvt_id'] ?></td>
            <td class="color_blue font_s14 line_h17 font_w500" style="text-align: left;"><?= $vt_nhap1['dsvt_name']; ?></td>
            <td><?= $vt_nhap1['dvt_name']; ?></td>
            <td><?= $vt_nhap1['hsx_name']; ?></td>
            <td><?= $vt_nhap1['xx_name']; ?></td>
            <?php $id_vtu = $vt_nhap1['dsvt_id'];?>
            <td><?= $list_vtdh1[$id_vtu]['so_luong_ky_nay']?></td>
            <td class="sl_nhap"><?= $vt_nhap1['slvt_soLuongNhapKho'] ?></td>
            <td><?= $vt_nhap1['dsvt_donGia'] ?></td>
            <td><?= $vt_nhap1['dsvt_donGia']*$vt_nhap1['slvt_soLuongNhapKho']?></td>
        </tr>
        <?}
    }?>
    <?php if($item['kcxl_hinhThuc']=='NK2'){?>
        <?php 
            $check_dc =  $item['kcxl_phieuDieuChuyenKho'];
            $vattu_nhap2 = new db_query("SELECT `dsvt_id`,`slvt_idPhieu`, `slvt_maPhieu`, `slvt_checkDieuChuyen`,
            `dsvt_maVatTuThietBi`, `dsvt_name`, `dvt_name`, `hsx_name`, `xx_name`, `slvt_soLuongNhapKho`,`dsvt_donGia`,`slvt_soLuongDieuChuyen`
            FROM `danh-sach-vat-tu`
            INNER JOIN `don-vi-tinh` ON `dsvt_donViTinh` =`dvt_id`
            INNER JOIN `hang-san-xuat` ON `dsvt_hangSanXuat` = `hsx_id`
            INNER JOIN `xuat-xu` ON `dsvt_xuatXu` = `xx_id`
            INNER JOIN `so-luong-vat-tu` ON `dsvt_id` = `slvt_maVatTuThietBi`
            INNER JOIN `kho-cho-xu-li` ON `slvt_idPhieu` = `kcxl_id`
            WHERE `dsvt_check` = '1' AND `slvt_idPhieu` = '$id_phieu' AND `slvt_checkDieuChuyen` = '$check_dc' AND `dsvt_id_ct` = $id_cty
            ORDER BY `dsvt_id` ASC"); 
            $st = 1;
        ?>
        <thead>
            <th colspan="10" style="font-size:18px; height:60px; text-align: left;">Danh sách vật tư</th>
        </thead>
        <tr>
            <th>STT
                <span class="span_thread"></span>
            </th>
            <th>Mã vật tư
                <span class="span_thread"></span>
            </th>
            <th>Tên đầy đủ vật tư thiết bị
                <span class="span_thread"></span>
            </th>
            <th>Đơn vị tính
                <span class="span_thread"></span>
            </th>
            <th> Hãng sản xuất
                <span class="span_thread"></span>
            </th>
            <th> Xuất xứ
                <span class="span_thread"></span>
            </th>
            <th> Số lượng điều chuyển
                <span class="span_thread"></span>
            </th>
            <th> Số lượng thực tế nhập kho
                <span class="span_thread"></span>
            </th>
            <th>Đơn giá (VNĐ)
                <span class="span_thread"></span>
            </th>
            <th>Thành tiền (VNĐ)
                <span class="span_thread"></span>
            </th>
        </tr>
        <? while($vt_nhap2 = mysql_fetch_assoc($vattu_nhap2 -> result)){?>
        <tr class="color_grey font_s14 line_h17 font_w400" data-id="<?=$vt_nhap2['dsvt_id']?>">
            <td><?= $st++; ?></td>
            <td>VT - <?= $vt_nhap2['dsvt_id'] ?></td>
            <td class="color_blue font_s14 line_h17 font_w500"><?= $vt_nhap2['dsvt_name']; ?></td>
            <td><?= $vt_nhap2['dvt_name']; ?></td>
            <td><?= $vt_nhap2['hsx_name']; ?></td>
            <td><?= $vt_nhap2['xx_name']; ?></td>
            <?php
                $idvt_dc = $vt_nhap2['dsvt_id'];
                $sl_dc = mysql_fetch_assoc((new db_query("SELECT `slvt_soLuongDieuChuyen` FROM `danh-sach-vat-tu`
                INNER JOIN `so-luong-vat-tu` ON `dsvt_id` = `slvt_maVatTuThietBi`
                INNER JOIN `kho-cho-xu-li` ON `slvt_idPhieu` = `kcxl_id`
                WHERE `kcxl_check` = '1' AND `slvt_idPhieu` = '$check_dc' AND `dsvt_id` = '$idvt_dc' AND `kcxl_id_ct` = $id_cty
                "))->result)['slvt_soLuongDieuChuyen'];
            ?>
            <td style="background: #FFFFFF;"><?= $sl_dc ?></td>
            <td class="sl_nhap"><?= $vt_nhap2['slvt_soLuongNhapKho']; ?></td>
            <td><?= $vt_nhap2['dsvt_donGia'] ?></td>
            <td><?= $vt_nhap2['dsvt_donGia'] * $vt_nhap2['slvt_soLuongNhapKho']?></td>
        </tr>
        <? }
    }?>
    <?php if($item['kcxl_hinhThuc']=='NK3'){?>
        <?php $vattu_nhap3 = new db_query("SELECT `dsvt_id`,`slvt_idPhieu`, `slvt_maPhieu`, 
                `dsvt_maVatTuThietBi`, `dsvt_name`, `dvt_name`, `hsx_name`, `xx_name`, `slvt_soLuongNhapKho`,`dsvt_donGia` FROM `danh-sach-vat-tu`
                INNER JOIN `don-vi-tinh` ON `dsvt_donViTinh` =`dvt_id`
                INNER JOIN `hang-san-xuat` ON `dsvt_hangSanXuat` = `hsx_id`
                INNER JOIN `xuat-xu` ON `dsvt_xuatXu` = `xx_id`
                INNER JOIN `so-luong-vat-tu` ON `dsvt_id` = `slvt_maVatTuThietBi`
                INNER JOIN `kho-cho-xu-li` ON `slvt_idPhieu` = `kcxl_id`
                WHERE `dsvt_check` = '1' AND `slvt_idPhieu` = '$id_phieu' AND `dsvt_id_ct` = $id_cty
                ORDER BY `dsvt_id` ASC ");
                $stt = 1;
        ?>
        <thead>
            <th colspan="9" style="font-size:18px; height:60px; text-align: left;">Danh sách vật tư</th>
        </thead>
        <tr>
            <th>STT
                <span class="span_thread"></span>
            </th>
            <th>Mã vật tư
                <span class="span_thread"></span>
            </th>
            <th>Tên đầy đủ vật tư thiết bị
                <span class="span_thread"></span>
            </th>
            <th>Đơn vị tính
                <span class="span_thread"></span>
            </th>
            <th> Hãng sản xuất
                <span class="span_thread"></span>
            </th>
            <th> Xuất xứ
                <span class="span_thread"></span>
            </th>
            <th> Số lượng nhập kho
                <span class="span_thread"></span>
            </th>
            <th>Đơn giá (VNĐ)
                <span class="span_thread"></span>
            </th>
            <th>Thành tiền (VNĐ)
                <span class="span_thread"></span>
            </th>
        </tr>
        <?php while($vt_nhap3 = mysql_fetch_assoc($vattu_nhap3 -> result)){?>
        <tr class="color_grey font_s14 line_h17 font_w400" data-id="<?=$vt_nhap3['dsvt_id']?>">
            <td>
                <?= $stt++ ?>
            </td>
            <td class="color_grey font_s14 line_h17 font_w400" style="background: #FFFFFF;">VT - <?=$vt_nhap3['dsvt_id']?>
            </td>
            <td class="color_blue font_s14 line_h17 font_w500"><?= $vt_nhap3['dsvt_name']; ?></td>
            <td style="background: #FFFFFF;"><?= $vt_nhap3['dvt_name']; ?></td>
            <td style="background: #FFFFFF;"><?= $vt_nhap3['hsx_name']; ?></td>
            <td style="background: #FFFFFF;"><?= $vt_nhap3['xx_name']; ?></td>
            <td class="sl_nhap"><?= $vt_nhap3['slvt_soLuongNhapKho']; ?></td>
            <td><?= $vt_nhap3['dsvt_donGia'] ?></td>
            <td><?= $vt_nhap3['dsvt_donGia'] * $vt_nhap3['slvt_soLuongNhapKho']?></td>
        </tr>
        <?}
    }?>
    <?php if($item['kcxl_hinhThuc']=='NK4'){?>
        <?php $vattu4 = new db_query("SELECT `kcxl_id`, `kcxl_soPhieu`, `kcxl_trangThai`, `dsvt_id`,`slvt_idPhieu`, `slvt_maPhieu`, 
            `dsvt_maVatTuThietBi`, `dsvt_name`, `dvt_name`, `hsx_name`, `xx_name`, `slvt_soLuongNhapKho`,`dsvt_donGia` FROM `danh-sach-vat-tu`
            INNER JOIN `don-vi-tinh` ON `dsvt_donViTinh` =`dvt_id`
            INNER JOIN `hang-san-xuat` ON `dsvt_hangSanXuat` = `hsx_id`
            INNER JOIN `xuat-xu` ON `dsvt_xuatXu` = `xx_id`
            INNER JOIN `so-luong-vat-tu` ON `dsvt_id` = `slvt_maVatTuThietBi`
            INNER JOIN `kho-cho-xu-li` ON `slvt_idPhieu` = `kcxl_id`
            WHERE `dsvt_check` = '1' AND `slvt_idPhieu` = '$id_phieu' AND `dsvt_id_ct` = $id_cty
            ORDER BY `dsvt_id` ASC ");
            $stt =1; 
        ?> 
        <thead>
            <th colspan="11" style="font-size:18px; height:60px; text-align: left;">Danh sách vật tư</th>
        </thead>
        <tr>
            <th>STT
                <span class="span_thread"></span>
            </th>
            <th>Mã vật tư
                <span class="span_thread"></span>
            </th>
            <th>Tên đầy đủ vật tư thiết bị
                <span class="span_thread"></span>
            </th>
            <th>Đơn vị tính
                <span class="span_thread"></span>
            </th>
            <th> Hãng sản xuất
                <span class="span_thread"></span>
            </th>
            <th> Xuất xứ
                <span class="span_thread"></span>
            </th>
            <th> Số lượng theo yêu cầu
                <span class="span_thread"></span>
            </th>
            <th> Số lượng được duyệt
                <span class="span_thread"></span>
            </th>
            <th> Số lượng thực tế nhập kho
                <span class="span_thread"></span>
            </th>
            <th>Đơn giá (VNĐ)
                <span class="span_thread"></span>
            </th>
            <th>Thành tiền (VNĐ)
            </th>
        </tr>
        <?php while($vt_nhap4 = mysql_fetch_assoc($vattu4 -> result)){?>
        <tr class="color_grey font_s14 line_h17 font_w400" data-id="<?=$vt_nhap4['dsvt_id']?>">
            <td>
                <?= $stt++ ?>
            </td>
            <td>VT - <?= $vt_nhap4['dsvt_id']?></td>
            <td class="font_s14 line_h17 color_blue font_w500" style="text-align: left;"><?= $vt_nhap4['dsvt_name']; ?></td>
            <td><?= $vt_nhap4['dvt_name']; ?></td>
            <td><?= $vt_nhap4['hsx_name']; ?></td>
            <td><?= $vt_nhap4['xx_name']; ?></td>
            <?php $id_vtu = $vt_nhap4['dsvt_id'];?>
            <td style="background: #FFFFFF;"><?= $emp_vt_yc[$id_vtu]['so_luong_yc_duyet']?></td>
            <td style="background: #FFFFFF;"><?= $emp_vt_yc[$id_vtu]['so_luong_duyet']?></td>
            <td class="sl_nhap"><?=$vt_nhap4['slvt_soLuongNhapKho']?></td>
            <td><?= $vt_nhap4['dsvt_donGia'] ?></td>
            <td><?= $vt_nhap4['dsvt_donGia'] * $vt_nhap4['slvt_soLuongNhapKho']?></td>
        </tr>
        <?}
    }?>
    <?php if($item['kcxl_hinhThuc']=='NK5'){?>
        <?php $vattu5 = new db_query("SELECT `kcxl_id`, `kcxl_soPhieu`, `kcxl_trangThai`, `dsvt_id`,`slvt_idPhieu`, `slvt_maPhieu`, 
            `dsvt_maVatTuThietBi`, `dsvt_name`, `dvt_name`, `hsx_name`, `xx_name`, `slvt_soLuongNhapKho`,`dsvt_donGia` FROM `danh-sach-vat-tu`
            INNER JOIN `don-vi-tinh` ON `dsvt_donViTinh` =`dvt_id`
            INNER JOIN `hang-san-xuat` ON `dsvt_hangSanXuat` = `hsx_id`
            INNER JOIN `xuat-xu` ON `dsvt_xuatXu` = `xx_id`
            INNER JOIN `so-luong-vat-tu` ON `dsvt_id` = `slvt_maVatTuThietBi`
            INNER JOIN `kho-cho-xu-li` ON `slvt_idPhieu` = `kcxl_id`
            WHERE `dsvt_check` = '1' AND `slvt_idPhieu` = '$id_phieu'
            ORDER BY `dsvt_id` ASC");
            $tt = 1;
        ?>
        <thead>
            <th colspan="9" style="font-size:18px; height:60px; text-align: left;">Danh sách vật tư</th>
        </thead>
        <tr>
            <th>STT
                <span class="span_thread"></span>
            </th>
            <th>Mã vật tư
                <span class="span_thread"></span>
            </th>
            <th>Tên đầy đủ vật tư thiết bị
                <span class="span_thread"></span>
            </th>
            <th>Đơn vị tính
                <span class="span_thread"></span>
            </th>
            <th> Hãng sản xuất
                <span class="span_thread"></span>
            </th>
            <th> Xuất xứ
                <span class="span_thread"></span>
            </th>
            <th> Số lượng nhập kho
                <span class="span_thread"></span>
            </th>
            <th>Đơn giá (VNĐ)
                <span class="span_thread"></span>
            </th>
            <th>Thành tiền (VNĐ)
                <span class="span_thread"></span>
            </th>
        </tr>
        <? while($vt_nhap5 = mysql_fetch_assoc($vattu5 ->result)){?>
        <tr class="color_grey font_s14 line_h17 font_w400" data-id="<?=$vt_nhap5['dsvt_id']?>">
            <td>
                <?= $tt++; ?>
            </td>
            <td style="background: #FFFFFF;">VT - <?= $vt_nhap5['dsvt_id']?></td>
            <td>
            <?= $vt_nhap5['dsvt_name']; ?>
            </td>
            <td style="background: #FFFFFF;"><?= $vt_nhap5['dvt_name']; ?></td>
            <td style="background: #FFFFFF;"><?= $vt_nhap5['hsx_name']; ?></td>
            <td style="background: #FFFFFF;"><?= $vt_nhap5['xx_name']; ?></td>
            <td class="sl_nhap"><?= $vt_nhap5['slvt_soLuongNhapKho']; ?></td>
            <td><?= $vt_nhap5['dsvt_donGia'] ?></td>
            <td><?= $vt_nhap5['dsvt_donGia'] * $vt_nhap5['slvt_soLuongNhapKho']?></td>
        </tr>
        <?}
    }?>
</table>
