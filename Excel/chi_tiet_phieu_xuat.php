<?php
include("config.php");
$id_phieu = getValue('id', 'int', 'GET', '');


if (isset($_COOKIE['acc_token']) && isset($_COOKIE['rf_token']) && isset($_COOKIE['role']) && $_COOKIE['role'] == 2) {
  $curl = curl_init();
  $token = $_COOKIE['acc_token'];
  curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/list_all_my_partner.php?get_all=true');
  curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
  curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));
  $response = curl_exec($curl);
  curl_close($curl);

  $data_list = json_decode($response, true);
  $data_list_nv = $data_list['data']['items'];
} else {
  $curl = curl_init();
  $token = $_COOKIE['acc_token'];
  curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/list_all_employee_of_company.php');
  curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
  curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));
  $response = curl_exec($curl);
  curl_close($curl);

  $data_list = json_decode($response, true);
  $data_list_nv = $data_list['data']['items'];
}
$count = count($data_list_nv);

$user = [];
for ($i = 0; $i < count($data_list_nv); $i++) {
  $nv = $data_list_nv[$i];
  $user[$nv["ep_id"]] = $nv;
}

if (isset($_COOKIE['user']) && $_COOKIE['user'] != "" && $_COOKIE['role'] == 2) {
  $token = $_COOKIE['acc_token'];
  $curl = curl_init();
  $data = array();
  curl_setopt($curl, CURLOPT_POST, 1);
  curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
  curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/user_info_employee.php');
  curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
  curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));
  $response = curl_exec($curl);
  curl_close($curl);
  $data_tt = json_decode($response, true);
  $tt_user = $data_tt['data']['user_info_result'];
}

$id_cty = $tt_user['com_id'];

$id_nguoi_lg = $_SESSION['ep_id'];

// API vật tư đơn hàng
$curl_vtdh = curl_init();
curl_setopt($curl_vtdh, CURLOPT_POST, 1);
curl_setopt($curl_vtdh, CURLOPT_URL, 'https://phanmemquanlycungung.timviec365.vn/api/vat_tu_dh.php');
curl_setopt($curl_vtdh, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($curl_vtdh, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
curl_setopt($curl_vtdh, CURLOPT_POSTFIELDS, [
  'com_id' => $id_cty,
  'dh_id' => $info_item['kcxl_donHang'],
]);
$response_vtdh = curl_exec($curl_vtdh);
curl_close($curl_vtdh);
$data_list_vtdh = json_decode($response_vtdh, true);
$list_vtdh = $data_list_vtdh['data']['items'];

$list_vtdh1 = [];
for ($j = 0; $j < count($list_vtdh); $j++) {
  $list_vtdh_p = $list_vtdh[$j];
  $list_vtdh1[$list_vtdh_p["id_vat_tu"]] = $list_vtdh_p;
}

// API vật tư theo phiếu yêu cầu
$curl_vt_yc = curl_init();
curl_setopt($curl_vt_yc, CURLOPT_POST, 1);
curl_setopt($curl_vt_yc, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($curl_vt_yc, CURLOPT_URL, "https://phanmemquanlycungung.timviec365.vn/api/vat_tu_yc.php");
curl_setopt($curl_vt_yc, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
curl_setopt($curl_vt_yc, CURLOPT_POSTFIELDS, [
  'id_phieu' => $id_pyc,
]);
$response_vt_yc = curl_exec($curl_vt_yc);
curl_close($curl_vt_yc);
$emp_json = json_decode($response_vt_yc, true);
$emp_arr = $emp_json['data']['items'];
$emp_vt_yc = [];
for ($i = 0; $i < count($emp_arr); $i++) {
  $vt_yc = $emp_arr[$i];
  $emp_vt_yc[$vt_yc["id_vat_tu"]] = $vt_yc;
}

// API công trình
$curl_ct = curl_init();
curl_setopt($curl_ct, CURLOPT_POST, 1);
curl_setopt($curl_ct, CURLOPT_URL, 'https://phanmemquanlycongtrinh.timviec365.vn/api/congtrinh.php');
curl_setopt($curl_ct, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($curl_ct, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
curl_setopt($curl_ct, CURLOPT_POSTFIELDS, [
  'id_com' => $id_cty,
]);
$response_ct = curl_exec($curl_ct);
curl_close($curl_ct);
$data_list_ct = json_decode($response_ct, true);
$list_ct = $data_list_ct['data']['items'];
$list_ct_arr = [];
for ($i = 0; $i < count($list_ct); $i++) {
  $ctr = $list_ct[$i];
  $list_ct_arr[$ctr["ctr_id"]] = $ctr;
}

$info_info_item = new db_query("SELECT *
  FROM `kho-cho-xu-li`
   WHERE `kcxl_check` = '1' AND `kcxl_soPhieu` = 'PXK' AND `kcxl_id` = $id_phieu AND `kcxl_id_ct` = $id_cty");


$info_item = mysql_fetch_assoc($info_info_item->result);


$id_dh = $info_item['kcxl_donHang'];
$id_pyc = $info_item['kcxl_phieuYeuCau'];


$kho_nhap = new db_query("SELECT `kho_name`,`kcxl_ngayNhapKho` FROM `kho-cho-xu-li`
        INNER JOIN `kho` ON `kho-cho-xu-li`.`kcxl_khoNhap` = `kho`.`kho_id`
        WHERE `kcxl_soPhieu` = 'PXK' AND `kcxl_id` = $id_phieu AND `kho_id_ct` = $id_cty
        ");
$nhap = mysql_fetch_assoc($kho_nhap->result);

$kho_xuat = new db_query("SELECT `kho_name`,`kcxl_ngayXuatKho`
        FROM `kho-cho-xu-li`
        INNER JOIN `kho` ON `kho-cho-xu-li`.`kcxl_khoXuat` = `kho`.`kho_id`
        WHERE `kcxl_soPhieu` = 'PXK' AND `kcxl_id` = $id_phieu AND `kho_id_ct` = $id_cty
        ");
$xuat = mysql_fetch_assoc($kho_xuat->result);


header("Content-type: application/octet-stream; charset=utf-8");
header("Content-Disposition: attachment; filename=excel_chi_tiet_phieu_nhap.xls");
header("Pragma: no-cache");
header("Expires: 0");
?>
<table border="1px solid black">
  <thead>
    <th colspan="2" style="font-size:18px;height:60px;vertical-align: left;">Thông tin chi tiết phiếu xuất</th>
  </thead>
  <tr>
    <td>Số phiếu</td>
    <td>PXK - <?= $info_item['kcxl_id']; ?></td>
  </tr>
  <tr>
    <td>Hình thức xuất kho</td>
    <td><?= hinh_thuc_xuat($info_item['kcxl_hinhThuc']); ?></td>
  </tr>
  <?php
  $id_nguoi_tao = $info_item['kcxl_nguoiTao'];
  $ten_nguoi_tao = $user[$id_nguoi_tao]['ep_name'];
  ?>
  <tr>
    <td>Người tạo</td>
    <td><?= $ten_nguoi_tao; ?></td>
  </tr>
  <tr>
    <td>Ngày tạo</td>
    <td><?= date('d/m/Y', strtotime($info_item['kcxl_ngayTao'])); ?></td>
  </tr>
  <?php if ($info_item['kcxl_hinhThuc'] != "") { ?>
    <tr>
      <td>
        <?php
        $loaiPhieu = $info_item['kcxl_hinhThuc'];
        switch ($loaiPhieu) {
          case "XK1":
            $loaiPhieu = "Phiếu yêu cầu";
            echo $loaiPhieu;
            break;
          case "XK2":
            $loaiPhieu = "Phiếu điều chuyển kho";
            echo $loaiPhieu;
            break;
          case "XK3":
            $loaiPhieu = "Phiếu yêu cầu";
            echo $loaiPhieu;
            break;
          case "XK4":
            $loaiPhieu = "Đơn hàng";
            echo $loaiPhieu;
            break;
        }
        ?>
      </td>
      <td>
        <?php if ($info_item['kcxl_hinhThuc'] == "XK1") { ?>
          PYC - <?= $info_item['kcxl_phieuYeuCau'] ?>
        <? } ?>
        <?php if ($info_item['kcxl_hinhThuc'] == "XK2") { ?>
          ĐCK - <?= $info_item['kcxl_phieuDieuChuyenKho'] ?>
        <? } ?>
        <?php if ($info_item['kcxl_hinhThuc'] == "XK3") { ?>
          PYC - <?= $info_item['kcxl_phieuYeuCau'] ?>
        <? } ?>
        <?php if ($info_item['kcxl_hinhThuc'] == "XK4") { ?>
          ĐH - <?= $info_item['kcxl_donHang'] ?>
        <? } ?>
      </td>
    </tr>
  <? } ?>
  <tr>
    <td>Trạng thái</td>
    <td><?= trang_thai($info_item['kcxl_trangThai']); ?></td>
  </tr>

  <?php if ($info_item['kcxl_trangThai'] == 2) { ?>
    <tr>
      <td>Lí do từ chối</td>
      <td><?= $info_item['kcxl_liDoTuChoi']; ?></td>
    </tr>
  <? } ?>

  <!-- Nhập theo biên bản giao hàng -->
  <?php if ($info_item['kcxl_hinhThuc'] == 'XK1') { ?>
    <tr>
      <td>Ngày yêu cầu hoàn thành</td>
      <td><?= $info_item['kcxl_ngayYeuCauHoanThanh'] ?></td>
    </tr>
    <tr>
      <td>Công trình</td>
      <td><?= $list_ct_arr[trim($info_item['kcxl_congTrinh'])]['ctr_name'] ?></td>
    </tr>
    <?php
    $id_nguoi_nhan = $info_item['kcxl_nguoiNhan'];
    $id_nguoi_giao = $info_item['kcxl_nguoiThucHien'];
    $ten_nguoi_nhan = $user[$id_nguoi_nhan]['ep_name'];
    $ten_nguoi_giao = $user[$id_nguoi_giao]['ep_name'];
    $phong_ban_nguoi_nhan = $user[$id_nguoi_nhan]['dep_name'];
    $phong_ban_nguoi_giao = $user[$id_nguoi_giao]['dep_name'];
    ?>
    <tr>
      <td>Người giao hàng</td>
      <td><?= $ten_nguoi_giao ?></td>
    </tr>
    <tr>
      <td>Phòng ban</td>
      <td><?= explode('-', $phong_ban_nguoi_giao)[0]; ?></td>
    </tr>
    <tr>
      <td>Người nhận</td>
      <td><?= $ten_nguoi_nhan ?></td>
    </tr>
    <tr>
      <td>Phòng ban</td>
      <td><?= explode('-', $phong_ban_nguoi_nhan)[0]; ?></td>
    </tr>
    <tr>
      <td>Kho xuất</td>
      <td><?= $xuat['kho_name']; ?></td>
    </tr>
    <tr>
      <td>Ngày xuất kho</td>
      <td><?= $info_item['kcxl_ngayXuatKho']; ?></td>
    </tr>
  <? } ?>

  <!-- Nhập điều chuyển  -->
  <?php if ($info_item['kcxl_hinhThuc'] == 'XK2') { ?>
    <tr>
      <td>Kho xuất </td>
      <td><?= $xuat['kho_name'] ?></td>
    </tr>
    <tr>
      <td>Ngày xuất</td>
      <td><?= $info_item['kcxl_ngayXuatKho'] ?></td>
    </tr>
    <tr>
      <td>Kho nhập</td>
      <td><?= $nhap['kho_name'] ?></td>
    </tr>
    <tr>
      <td>
        Ngày yêu cầu hoàn thành điều chuyển
      </td>
      <td><?= $info_item['kcxl_ngayYeuCauHoanThanh'] ?></td>
    </tr>
    <?php
    $id_nguoi_nhan = $info_item['kcxl_nguoiNhan'];
    $id_nguoi_giao = $info_item['kcxl_nguoiThucHien'];
    $ten_nguoi_nhan = $user[$id_nguoi_nhan]['ep_name'];
    $ten_nguoi_giao = $user[$id_nguoi_giao]['ep_name'];
    $phong_ban_nguoi_nhan = $user[$id_nguoi_nhan]['dep_name'];
    $phong_ban_nguoi_giao = $user[$id_nguoi_giao]['dep_name'];
    ?>
    <tr>
      <td>Người giao hàng</td>
      <td><?= $ten_nguoi_giao ?></td>
    </tr>
    <tr>
      <td>Phòng ban</td>
      <td><?= explode('-', $phong_ban_nguoi_giao)[0]; ?></td>
    </tr>
    <tr>
      <td>Người nhận</td>
      <td><?= $ten_nguoi_nhan ?></td>
    </tr>
    <tr>
      <td>Phòng ban</td>
      <td><?= explode('-', $phong_ban_nguoi_nhan)[0]; ?></td>
    </tr>
  <? } ?>

  <!-- Nhập trả lại từ thi công  -->
  <?php if ($info_item['kcxl_hinhThuc'] == 'XK3') { ?>
    <tr>
      <td>Kho xuất </td>
      <td><?= $xuat['kho_name'] ?></td>
    </tr>
    <tr>
      <td>Ngày xuất</td>
      <td><?= $info_item['kcxl_ngayXuatKho'] ?></td>
    </tr>
    <tr>
      <td>Kho nhập</td>
      <td><?= $nhap['kho_name'] ?></td>
    </tr>
    <tr>
      <td>
        Ngày yêu cầu hoàn thành điều chuyển
      </td>
      <td><?= $info_item['kcxl_ngayYeuCauHoanThanh'] ?></td>
    </tr>
    <?php
    $id_nguoi_nhan = $info_item['kcxl_nguoiNhan'];
    $id_nguoi_giao = $info_item['kcxl_nguoiThucHien'];
    $ten_nguoi_nhan = $user[$id_nguoi_nhan]['ep_name'];
    $ten_nguoi_giao = $user[$id_nguoi_giao]['ep_name'];
    $phong_ban_nguoi_nhan = $user[$id_nguoi_nhan]['dep_name'];
    $phong_ban_nguoi_giao = $user[$id_nguoi_giao]['dep_name'];
    ?>
    <tr>
      <td>Người giao hàng</td>
      <td><?= $ten_nguoi_giao ?></td>
    </tr>
    <tr>
      <td>Người nhận</td>
      <td><?= $ten_nguoi_nhan ?></td>
    </tr>
    <tr>
      <td>Phòng ban</td>
      <td><?= explode('-', $phong_ban_nguoi_nhan)[0]; ?></td>
    </tr>
  <? } ?>

  <!-- Nhập theo yêu cầu vật tư  -->
  <?php if ($info_item['kcxl_hinhThuc'] == 'XK4') { ?>
    <tr>
      <td>Kho xuất </td>
      <td><?= $xuat['kho_name'] ?></td>
    </tr>
    <tr>
      <td>Ngày xuất</td>
      <td><?= $info_item['kcxl_ngayXuatKho'] ?></td>
    </tr>
    <?php
    // $id_nguoi_nhan = $info_item['kcxl_nguoiNhan'];
    $id_nguoi_giao = $info_item['kcxl_nguoiThucHien'];
    // $ten_nguoi_nhan = $user[$id_nguoi_nhan]['ep_name'];
    $ten_nguoi_giao = $user[$id_nguoi_giao]['ep_name'];
    // $phong_ban_nguoi_nhan = $user[$id_nguoi_nhan]['dep_name'];
    $phong_ban_nguoi_giao = $user[$id_nguoi_giao]['dep_name'];
    ?>
    <tr>
      <td>Người xuất kho</td>
      <td><?= $ten_nguoi_giao ?></td>
    </tr>
    <tr>
      <td>Phòng ban</td>
      <td><?= explode('-', $phong_ban_nguoi_giao)[0]; ?></td>
    </tr>
    <tr>
      <td>
        Thời hạn hoàn thành
      </td>
      <td><?= $info_item['kcxl_ngayYeuCauHoanThanh'] ?></td>
    </tr>
  <? } ?>

  <!-- Nhập khác  -->
  <?php if ($info_item['kcxl_hinhThuc'] == '') { ?>
    <?php
    $id_nguoi_nhan = $info_item['kcxl_nguoiNhan'];
    $id_nguoi_giao = $info_item['kcxl_nguoiThucHien'];
    $ten_nguoi_nhan = $user[$id_nguoi_nhan]['ep_name'];
    $ten_nguoi_giao = $user[$id_nguoi_giao]['ep_name'];
    $phong_ban_nguoi_nhan = $user[$id_nguoi_nhan]['dep_name'];
    $phong_ban_nguoi_giao = $user[$id_nguoi_giao]['dep_name'];
    ?>
    <tr>
      <td>Kho xuất </td>
      <td><?= $xuat['kho_name'] ?></td>
    </tr>
    <tr>
      <td>Ngày xuất</td>
      <td><?= $info_item['kcxl_ngayXuatKho'] ?></td>
    </tr>
    <tr>
      <td>Người giao hàng</td>
      <td><?= $ten_nguoi_giao ?></td>
    </tr>
    <tr>
      <td>Phòng ban</td>
      <td><?= explode('-', $phong_ban_nguoi_giao)[0]; ?></td>
    </tr>
    <tr>
      <td>Người nhận</td>
      <td><?= $ten_nguoi_nhan ?></td>
    </tr>
    <tr>
      <td>Phòng ban</td>
      <td><?= explode('-', $phong_ban_nguoi_nhan)[0]; ?></td>
    </tr>
  <? } ?>

  <!--  -->
  <tr>
    <td>Ghi chú</td>
    <td><?= $info_item['kcxl_ghi_chu']; ?></td>
  </tr>

  <?php if ($info_item['kcxl_trangThai'] == 5) { ?>
    <?php
    $id_nguoi_duyet = $info_item['kcxl_nguoiDuyet'];
    $ten_nguoi_duyet = $user[$id_nguoi_duyet]['ep_name'];
    ?>
    <tr>
      <td>Người duyệt</td>
      <td><?= $ten_nguoi_duyet; ?></td>
    </tr>
    <tr>
      <td>Ngày duyệt</td>
      <td><?= $info_item['kcxl_ngayDuyet']; ?></td>
    </tr>
  <? } ?>

  <?php if ($info_item['kcxl_trangThai'] == 6) {
    $id_nguoi_duyet = $info_item['kcxl_nguoiDuyet'];
    $ten_nguoi_duyet = $user[$id_nguoi_duyet]['ep_name'];
    $id_nguoi_ht = $info_item['kcxl_nguoiHoanThanh'];
    $ten_nguoi_ht = $user[$id_nguoi_ht]['ep_name'];
  ?>
    <tr>
      <td>Người duyệt</td>
      <td><?= $ten_nguoi_duyet; ?></td>
    </tr>
    <tr>
      <td>Ngày duyệt</td>
      <td><?= $info_item['kcxl_ngayDuyet']; ?></td>
    </tr>
    <tr>
      <td>Người hoàn thành</td>
      <td><?= $ten_nguoi_ht; ?></td>
    </tr>
    <tr>
      <td>Ngày hoàn thành</td>
      <td><?= $info_item['kcxl_ngayHoanThanh']; ?></td>
    </tr>
  <? } ?>
</table>

<table>
  <tr></tr>
  <tr></tr>
</table>


<table border="1px solid black">
  <?php if ($info_item['kcxl_hinhThuc'] == 'XK1') { ?>
    <?php
    $vattu_nhap1 = new db_query("SELECT `dsvt_id`, `dsvt_name`, `dvt_name`, `hsx_name`, `xx_name`, `slvt_soLuongXuatKho`,`dsvt_donGia`,`slvt_so_luong_yeu_cau`,`slvt_so_luong_duyet`
        FROM `danh-sach-vat-tu`
        INNER JOIN `don-vi-tinh` ON `dsvt_donViTinh` =`dvt_id`
        INNER JOIN `hang-san-xuat` ON `dsvt_hangSanXuat` = `hsx_id`
        INNER JOIN `xuat-xu` ON `dsvt_xuatXu` = `xx_id`
        INNER JOIN `so-luong-vat-tu` ON `dsvt_id` = `slvt_maVatTuThietBi`
        INNER JOIN `kho-cho-xu-li` ON `slvt_idPhieu` = `kcxl_id`
        WHERE `dsvt_check` = '1' AND `slvt_idPhieu` = '$id_phieu' AND `dsvt_id_ct` = '$id_cty '
        ORDER BY `dsvt_id` ASC ");
    $stt = 1;
    ?>
    <thead>
      <th colspan="10" style="font-size:18px; height:60px; text-align: left;">Danh sách vật tư</th>
    </thead>
    <tr class="color_white font_s16 line_h19 font_w500">
      <th>STT
        <span class="span_tbody"></span>
      </th>
      <th>Mã vật tư thiết bị
        <span class="span_tbody"></span>
      </th>
      <th>Tên đầy đủ vật tư thiết bị
        <span class="span_tbody"></span>
      </th>
      <th>Đơn vị tính
        <span class="span_tbody"></span>
      </th>
      <th>Hãng sản xuất
        <span class="span_tbody"></span>
      </th>
      <th>Xuất xứ
        <span class="span_tbody"></span>
      </th>
      <th>Số lượng yêu cầu
        <span class="span_tbody"></span>
      </th>
      <th>Số lượng được duyệt
        <span class="span_tbody"></span>
      </th>
      <th>Số lượng thực tế xuất kho
        <span class="span_tbody"></span>
      </th>
      <th>Đơn giá (VNĐ)
        <span class="span_tbody"></span>
      </th>
      <th>Thành tiền (VNĐ)
      </th>
    </tr>
    <? while ($info_dsvt = mysql_fetch_assoc($vattu_nhap1->result)) { ?>
      <tr>
        <td><?= $stt++; ?></td>
        <td>VT-<?= $info_dsvt['dsvt_id']; ?></td>
        <td style="text-align: left;">
          <?= $info_dsvt['dsvt_name']; ?>
        </td>
        <td><?= $info_dsvt['dvt_name'] ?></td>
        <td style="text-align: left;"><?= $info_dsvt['hsx_name'] ?></td>
        <td><?= $info_dsvt['xx_name'] ?></td>
        <td style="text-align: right; background: #EEEEEE;">
          <?= $info_dsvt['slvt_so_luong_yeu_cau']; ?>
        </td>
        <td style="text-align: right; background: #EEEEEE;">
          <?= $info_dsvt['slvt_so_luong_duyet']; ?>
        </td>
        <td>
          <?= $info_dsvt['slvt_soLuongXuatKho'] ?>
        </td>
        <td style="text-align: right;" class="don_gia_3"><?= $info_dsvt['dsvt_donGia']; ?></td>
        <td style="text-align: right;" class="thanh_tien_3">
          <?= (number_format($info_dsvt['slvt_soLuongXuatKho'] * $info_dsvt['dsvt_donGia'], 0, '', '.')); ?>
        </td>
      </tr>
  <? }
  } ?>
  <?php if ($info_item['kcxl_hinhThuc'] == 'XK2') { ?>
    <?php
    $vattu_nhap1 = new db_query("SELECT `dsvt_id`, `dsvt_name`, `dvt_name`, `hsx_name`, `xx_name`, `slvt_soLuongXuatKho`,`dsvt_donGia`,`slvt_so_luong_yeu_cau`,`slvt_so_luong_duyet`
    FROM `danh-sach-vat-tu`
    INNER JOIN `don-vi-tinh` ON `dsvt_donViTinh` =`dvt_id`
    INNER JOIN `hang-san-xuat` ON `dsvt_hangSanXuat` = `hsx_id`
    INNER JOIN `xuat-xu` ON `dsvt_xuatXu` = `xx_id`
    INNER JOIN `so-luong-vat-tu` ON `dsvt_id` = `slvt_maVatTuThietBi`
    INNER JOIN `kho-cho-xu-li` ON `slvt_idPhieu` = `kcxl_id`
    WHERE `dsvt_check` = '1' AND `slvt_idPhieu` = '$id_phieu' AND `dsvt_id_ct` = '$id_cty '
    ORDER BY `dsvt_id` ASC ");
    $stt = 1;
    ?>
    <thead>
      <th colspan="10" style="font-size:18px; height:60px; text-align: left;">Danh sách vật tư</th>
    </thead>
    <tr class="color_white font_s16 line_h19 font_w500">
      <th>STT
        <span class="span_tbody"></span>
      </th>
      <th>Mã vật tư thiết bị
        <span class="span_tbody"></span>
      </th>
      <th>Tên đầy đủ vật tư thiết bị
        <span class="span_tbody"></span>
      </th>
      <th>Đơn vị tính
        <span class="span_tbody"></span>
      </th>
      <th>Hãng sản xuất
        <span class="span_tbody"></span>
      </th>
      <th>Xuất xứ
        <span class="span_tbody"></span>
      </th>
      <th>Số lượng yêu cầu
        <span class="span_tbody"></span>
      </th>
      <th>Số lượng thực tế xuất kho
        <span class="span_tbody"></span>
      </th>
      <th>Đơn giá (VNĐ)
        <span class="span_tbody"></span>
      </th>
      <th>Thành tiền (VNĐ)
      </th>
    </tr>
    <? while ($info_dsvt = mysql_fetch_assoc($vattu_nhap1->result)) { ?>
      <tr>
        <td><?= $stt++; ?></td>
        <td>VT-<?= $info_dsvt['dsvt_id']; ?></td>
        <td style="text-align: left;">
          <?= $info_dsvt['dsvt_name']; ?>
        </td>
        <td><?= $info_dsvt['dvt_name'] ?></td>
        <td style="text-align: left;"><?= $info_dsvt['hsx_name'] ?></td>
        <td><?= $info_dsvt['xx_name'] ?></td>
        <td style="text-align: right; background: #EEEEEE;">
          <?= $info_dsvt['slvt_so_luong_yeu_cau']; ?>
        </td>
        <td>
          <?= $info_dsvt['slvt_soLuongXuatKho'] ?>
        </td>
        <td style="text-align: right;" class="don_gia_3"><?= $info_dsvt['dsvt_donGia']; ?></td>
        <td style="text-align: right;" class="thanh_tien_3">
          <?= (number_format($info_dsvt['slvt_soLuongXuatKho'] * $info_dsvt['dsvt_donGia'], 0, '', '.')); ?>
        </td>
      </tr>
  <? }
  } ?>
  <?php if ($info_item['kcxl_hinhThuc'] == 'XK3') { ?>
    <?php
    $vattu_nhap1 = new db_query("SELECT `dsvt_id`, `dsvt_name`, `dvt_name`, `hsx_name`, `xx_name`, `slvt_soLuongXuatKho`,`dsvt_donGia`,`slvt_so_luong_yeu_cau`,`slvt_so_luong_duyet`
     FROM `danh-sach-vat-tu`
     INNER JOIN `don-vi-tinh` ON `dsvt_donViTinh` =`dvt_id`
     INNER JOIN `hang-san-xuat` ON `dsvt_hangSanXuat` = `hsx_id`
     INNER JOIN `xuat-xu` ON `dsvt_xuatXu` = `xx_id`
     INNER JOIN `so-luong-vat-tu` ON `dsvt_id` = `slvt_maVatTuThietBi`
     INNER JOIN `kho-cho-xu-li` ON `slvt_idPhieu` = `kcxl_id`
     WHERE `dsvt_check` = '1' AND `slvt_idPhieu` = '$id_phieu' AND `dsvt_id_ct` = '$id_cty '
     ORDER BY `dsvt_id` ASC ");
    $stt = 1;
    ?>
    <thead>
      <th colspan="9" style="font-size:18px; height:60px; text-align: left;">Danh sách vật tư</th>
    </thead>
    <tr class="color_white font_s16 line_h19 font_w500">
      <th>STT
        <span class="span_tbody"></span>
      </th>
      <th>Mã vật tư thiết bị
        <span class="span_tbody"></span>
      </th>
      <th>Tên đầy đủ vật tư thiết bị
        <span class="span_tbody"></span>
      </th>
      <th>Đơn vị tính
        <span class="span_tbody"></span>
      </th>
      <th>Hãng sản xuất
        <span class="span_tbody"></span>
      </th>
      <th>Xuất xứ
        <span class="span_tbody"></span>
      </th>
      <th>Số lượng yêu cầu
        <span class="span_tbody"></span>
      </th>
      <th>Số lượng được duyệt
        <span class="span_tbody"></span>
      </th>
      <th>Số lượng thực tế xuất kho
        <span class="span_tbody"></span>
      </th>
      <th>Đơn giá (VNĐ)
        <span class="span_tbody"></span>
      </th>
      <th>Thành tiền (VNĐ)
      </th>
    </tr>
    <?php while ($info_dsvt = mysql_fetch_assoc($vattu_nhap1->result)) { ?>
      <tr>
        <td><?= $stt++; ?></td>
        <td>VT-<?= $info_dsvt['dsvt_id']; ?></td>
        <td style="text-align: left;">
          <?= $info_dsvt['dsvt_name']; ?>
        </td>
        <td><?= $info_dsvt['dvt_name'] ?></td>
        <td style="text-align: left;"><?= $info_dsvt['hsx_name'] ?></td>
        <td><?= $info_dsvt['xx_name'] ?></td>
        <td style="text-align: right; background: #EEEEEE;">
          <?= $info_dsvt['slvt_so_luong_yeu_cau']; ?>
        </td>
        <td style="text-align: right; background: #EEEEEE;">
          <?= $info_dsvt['slvt_so_luong_duyet']; ?>
        </td>
        <td>
          <?= $info_dsvt['slvt_soLuongXuatKho'] ?>
        </td>
        <td style="text-align: right;" class="don_gia_3"><?= $info_dsvt['dsvt_donGia']; ?></td>
        <td style="text-align: right;" class="thanh_tien_3">
          <?= (number_format($info_dsvt['slvt_soLuongXuatKho'] * $info_dsvt['dsvt_donGia'], 0, '', '.')); ?>
        </td>
      </tr>
  <? }
  } ?>
  <?php if ($info_item['kcxl_hinhThuc'] == 'XK4') { ?>
    <?php
    $vattu_nhap1 = new db_query("SELECT `dsvt_id`, `dsvt_name`, `dvt_name`, `hsx_name`, `xx_name`, `slvt_soLuongXuatKho`,`dsvt_donGia`,`slvt_so_luong_yeu_cau`,`slvt_so_luong_duyet`
    FROM `danh-sach-vat-tu`
    INNER JOIN `don-vi-tinh` ON `dsvt_donViTinh` =`dvt_id`
    INNER JOIN `hang-san-xuat` ON `dsvt_hangSanXuat` = `hsx_id`
    INNER JOIN `xuat-xu` ON `dsvt_xuatXu` = `xx_id`
    INNER JOIN `so-luong-vat-tu` ON `dsvt_id` = `slvt_maVatTuThietBi`
    INNER JOIN `kho-cho-xu-li` ON `slvt_idPhieu` = `kcxl_id`
    WHERE `dsvt_check` = '1' AND `slvt_idPhieu` = '$id_phieu' AND `dsvt_id_ct` = '$id_cty '
    ORDER BY `dsvt_id` ASC ");
    $stt = 1;
    ?>
    <thead>
      <th colspan="11" style="font-size:18px; height:60px; text-align: left;">Danh sách vật tư</th>
    </thead>
    <tr class="color_white font_s16 line_h19 font_w500">
      <th>STT
        <span class="span_tbody"></span>
      </th>
      <th>Mã vật tư thiết bị
        <span class="span_tbody"></span>
      </th>
      <th>Tên đầy đủ vật tư thiết bị
        <span class="span_tbody"></span>
      </th>
      <th>Đơn vị tính
        <span class="span_tbody"></span>
      </th>
      <th>Hãng sản xuất
        <span class="span_tbody"></span>
      </th>
      <th>Xuất xứ
        <span class="span_tbody"></span>
      </th>
      <th>Số lượng yêu cầu
        <span class="span_tbody"></span>
      </th>
      <th>Số lượng thực tế xuất kho
        <span class="span_tbody"></span>
      </th>
      <th>Đơn giá (VNĐ)
        <span class="span_tbody"></span>
      </th>
      <th>Thành tiền (VNĐ)
      </th>
    </tr>
    <?php while ($info_dsvt = mysql_fetch_assoc($vattu_nhap1->result)) { ?>
      <tr>
        <td><?= $stt++; ?></td>
        <td>VT-<?= $info_dsvt['dsvt_id']; ?></td>
        <td style="text-align: left;">
          <?= $info_dsvt['dsvt_name']; ?>
        </td>
        <td><?= $info_dsvt['dvt_name'] ?></td>
        <td style="text-align: left;"><?= $info_dsvt['hsx_name'] ?></td>
        <td><?= $info_dsvt['xx_name'] ?></td>
        <td style="text-align: right; background: #EEEEEE;">
          <?= $info_dsvt['slvt_so_luong_yeu_cau']; ?>
        </td>
        <td>
          <?= $info_dsvt['slvt_soLuongXuatKho'] ?>
        </td>
        <td style="text-align: right;" class="don_gia_3"><?= $info_dsvt['dsvt_donGia']; ?></td>
        <td style="text-align: right;" class="thanh_tien_3">
          <?= (number_format($info_dsvt['slvt_soLuongXuatKho'] * $info_dsvt['dsvt_donGia'], 0, '', '.')); ?>
        </td>
      </tr>
  <? }
  } ?>
  <?php if ($info_item['kcxl_hinhThuc'] == '') { ?>
    <?php
    $vattu_nhap1 = new db_query("SELECT `dsvt_id`, `dsvt_name`, `dvt_name`, `hsx_name`, `xx_name`, `slvt_soLuongXuatKho`,`dsvt_donGia`,`slvt_so_luong_yeu_cau`,`slvt_so_luong_duyet`
    FROM `danh-sach-vat-tu`
    INNER JOIN `don-vi-tinh` ON `dsvt_donViTinh` =`dvt_id`
    INNER JOIN `hang-san-xuat` ON `dsvt_hangSanXuat` = `hsx_id`
    INNER JOIN `xuat-xu` ON `dsvt_xuatXu` = `xx_id`
    INNER JOIN `so-luong-vat-tu` ON `dsvt_id` = `slvt_maVatTuThietBi`
    INNER JOIN `kho-cho-xu-li` ON `slvt_idPhieu` = `kcxl_id`
    WHERE `dsvt_check` = '1' AND `slvt_idPhieu` = '$id_phieu' AND `dsvt_id_ct` = '$id_cty '
    ORDER BY `dsvt_id` ASC ");
    $stt = 1;
    ?>
    <thead>
      <th colspan="9" style="font-size:18px; height:60px; text-align: left;">Danh sách vật tư</th>
    </thead>
    <tr class="color_white font_s16 line_h19 font_w500">
      <th>
        <span class="span_tbody"></span>
      </th>
      <th>Mã vật tư thiết bị
        <span class="span_tbody"></span>
      </th>
      <th>Tên đầy đủ vật tư thiết bị
        <span class="span_tbody"></span>
      </th>
      <th>Đơn vị tính
        <span class="span_tbody"></span>
      </th>
      <th>Hãng sản xuất
        <span class="span_tbody"></span>
      </th>
      <th>Xuất xứ
        <span class="span_tbody"></span>
      </th>
      <th>Số lượng xuất kho
        <span class="span_tbody"></span>
      </th>
      <th>Đơn giá (VNĐ)
        <span class="span_tbody"></span>
      </th>
      <th>Thành tiền (VNĐ)
      </th>
    </tr>
    <? while ($info_dsvt = mysql_fetch_assoc($vattu_nhap1->result)) { ?>
      <tr>
        <td><?= $stt++; ?></td>
        <td>VT-<?= $info_dsvt['dsvt_id']; ?></td>
        <td style="text-align: left;">
          <?= $info_dsvt['dsvt_name']; ?>
        </td>
        <td><?= $info_dsvt['dvt_name'] ?></td>
        <td style="text-align: left;"><?= $info_dsvt['hsx_name'] ?></td>
        <td><?= $info_dsvt['xx_name'] ?></td>
        <td>
          <?= $info_dsvt['slvt_soLuongXuatKho'] ?>
        </td>
        <td style="text-align: right;" class="don_gia_3"><?= $info_dsvt['dsvt_donGia']; ?></td>
        <td style="text-align: right;" class="thanh_tien_3">
          <?= (number_format($info_dsvt['slvt_soLuongXuatKho'] * $info_dsvt['dsvt_donGia'], 0, '', '.')); ?>
        </td>
      </tr>
  <? }
  } ?>
</table>