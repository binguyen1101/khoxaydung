<?php
    include("config.php");

    if (isset($_COOKIE['user']) && $_COOKIE['user'] != "" && $_COOKIE['role'] == 2) {
		$token = $_COOKIE['acc_token'];
		$curl = curl_init();
		$data = array();
		curl_setopt($curl, CURLOPT_POST, 1);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
		curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/user_info_employee.php');
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));
		$response = curl_exec($curl);
		curl_close($curl);
		$data_tt = json_decode($response, true);
		$tt_user = $data_tt['data']['user_info_result'];
	}
    
    $id_cty = $tt_user['com_id'];

    if(isset($_COOKIE['acc_token']) && isset($_COOKIE['rf_token']) && isset($_COOKIE['role']) && $_COOKIE['role'] == 2){
        $curl = curl_init();
        $token = $_COOKIE['acc_token'];
        curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/list_all_my_partner.php?get_all=true');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer '.$token));
        $response = curl_exec($curl);
        curl_close($curl);

        $data_list = json_decode($response,true);
        $data_list_nv =$data_list['data']['items'];
    }else{
        $curl = curl_init();
        $token = $_COOKIE['acc_token'];
        curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/list_all_employee_of_company.php');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer '.$token));
        $response = curl_exec($curl);
        curl_close($curl);

        $data_list = json_decode($response,true);
        $data_list_nv =$data_list['data']['items'];

    }
    $count = count($data_list_nv);

    $user = [];
    for ($i = 0; $i < count($data_list_nv); $i++){
        $nv = $data_list_nv[$i];
        $user[$nv["ep_id"]] = $nv;
    }


    $all_kho = new db_query("SELECT * FROM `kho` WHERE `kho_id_ct` = $id_cty ORDER BY `kho_id` DESC ");

    header("Content-type: application/octet-stream; charset=utf-8");
    header("Content-Disposition: attachment; filename=excel_kho.xls");
    header("Pragma: no-cache");
    header("Expires: 0");

    echo '<table border="1px solid black">';
    echo '<tr><th colspan="7" style="font-size:18px;height:60px;vertical-align: middle;">Thông tin kho</th></tr>';
?>

<thead>
    <tr class="tittle_ds_kho font_w500 font_s16 line_h19">
        <td>STT
            <span class="span_thead"></span>
        </td>
        <td>Mã kho
            <span class="span_thead"></span>
        </td>
        <td>Tên kho
            <span class="span_thead"></span>
        </td>
        <td>Công trình
            <span class="span_thead"></span>
        </td>
        <td>Nhân viên quản lí kho
            <span class="span_thead"></span>
        </td>
        <td>Địa chỉ
            <span class="span_thead"></span>
        </td>
        <td>
            Mô tả
        </td>
    </tr>
</thead>
<tbody>
    <? while($item = mysql_fetch_assoc($all_kho->result)){ ?>
        <tr class="info_k" data-id="<?= $item['kho_id']?>">
            <td><?= $stt++ ?></td>
            <td class="color_gray">KHO - <?= $item['kho_id'] ?></td>
            <td><a class="ten_kho font_wB color_blue" href="danh-sach-kho-chi-tiet-<?= $item['kho_id'] ?>.html"> <?=$item['kho_name'] ?></a></td>
            <td class="ten_cong_trinh"><?= $item['kho_congTrinh'] ?></td>
            <td style="text-align: left;" class="ten_nguoi_ql" data-userCr="<?=$item['kho_nhanVienQuanLiKho']?>">
                <?php 
                    $nguoi_ql = $item['kho_nhanVienQuanLiKho'];
                    $user_id = $user[$nguoi_ql];
                    $ten_nguoi_ql = $user_id['ep_name'];
                ?>
                <div class="d_flex flex_start align_c">
                    <p><?= $ten_nguoi_ql; ?></p>
                </div>
            </td>
            <td class="font_s14 line_h17 dia_chi"><?= $item['kho_address'] ?></td>
            <td class="mo_ta"><?=$item['kho_description'] ?></td>
        </tr>
    <?}?>
</tbody>