<?php
    include("config.php");

    if (isset($_COOKIE['user']) && $_COOKIE['user'] != "" && $_COOKIE['role'] == 2) {
        $token = $_COOKIE['acc_token'];
        $curl = curl_init();
        $data = array();
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/user_info_employee.php');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));
        $response = curl_exec($curl);
        curl_close($curl);
        $data_tt = json_decode($response, true);
        $tt_user = $data_tt['data']['user_info_result'];
    }

    $id_cty = $tt_user['com_id'];
    $id_nguoi_xoa = $_SESSION['ep_id'];


    $list_all = "SELECT `dsvt_id`, `dsvt_maVatTuThietBi`, `dsvt_name`, `dsvt_donGia`, `dsvt_description`, `nvt_name`, `dvt_name`, `hsx_name`, `xx_name` FROM `danh-sach-vat-tu` 
    LEFT JOIN `nhom-vat-tu-thiet-bi` ON `dsvt_nhomVatTuThietBi` = `nvt_id`
    LEFT JOIN `don-vi-tinh` ON `dsvt_donViTinh` = `dvt_id`
    LEFT JOIN `hang-san-xuat` ON `dsvt_hangSanXuat` = `hsx_id`
    LEFT JOIN `xuat-xu` ON `dsvt_xuatXu` = `xx_id`
    WHERE `dsvt_check` = 1 AND `dsvt_id_ct` = $id_cty ";


    $list_all = new db_query($list_all);    

    header("Content-type: application/octet-stream; charset=utf-8");
    header("Content-Disposition: attachment; filename=excel_dsvt.xls");
    header("Pragma: no-cache");
    header("Expires: 0");

    echo '<table border="1px solid black">';
    echo '<tr><th colspan="9" style="font-size:18px;height:60px;vertical-align: middle;">Thông tin danh sách vật tư</th></tr>';
?>
<tr>
    <th>
        STT
        <span class="span_tbody"></span>
    </th>
    <th>Mã vật tư thiết bị
        <span class="span_tbody"></span>
    </th>
    <th>Tên đầy đủ vật tư thiết bị
        <span class="span_tbody"></span>
    </th>
    <th>Nhóm vật tư thiết bị
        <span class="span_tbody"></span>
    </th>
    <th>Đơn vị tính
        <span class="span_tbody"></span>
    </th>
    <th>Hãng sản xuất
        <span class="span_tbody"></span>
    </th>
    <th>Xuất xứ
        <span class="span_tbody"></span>
    </th>
    <th>Mô tả
        <span class="span_tbody"></span>
    </th>
    <th>Đơn giá (VNĐ)
        <span class="span_tbody"></span>
    </th>
</tr>
    <? $stt = 1; while ($row_list_all = mysql_fetch_assoc($list_all->result)) { ?>
        <tr data-id="<?=$row_list_all['dsvt_id']?>">
            <td class="font_s14 line_16 color_grey"><?= $stt++ ?></td>  
            <td class="font_s14 line_16 color_grey">VT - <?= $row_list_all['dsvt_id']; ?></td>
            <td>
                <a class="color_blue line_16 font_s14 font_w500" href="/danh-sach-vat-tu-thiet-bi-chi-tiet-<?= $row_list_all['dsvt_id'] ?>.html">
                    <?= $row_list_all['dsvt_name'] ?></a>
            </td>
            <td class="font_s14 line_16 color_grey"><?= $row_list_all['nvt_name'] ?></td>
            <td class="font_s14 line_16 color_grey"><?= $row_list_all['dvt_name'] ?></td>
            <td class="font_s14 line_16 color_grey"><?= $row_list_all['hsx_name'] ?></td>
            <td class="font_s14 line_16 color_grey"><?= $row_list_all['xx_name']?></td>
            <td class="font_s14 line_16 color_grey"><?= $row_list_all['dsvt_description']?></td>
            <td class="font_s14 line_16 color_grey text_a_r"><?= $row_list_all['dsvt_donGia']?></td>
        </tr>
    <? } ?>