<?php
    include("config.php");

    if(isset($_COOKIE['acc_token']) && isset($_COOKIE['rf_token']) && isset($_COOKIE['role']) && $_COOKIE['role'] == 2){
        $curl = curl_init();
        $token = $_COOKIE['acc_token'];
        curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/list_all_my_partner.php?get_all=true');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer '.$token));
        $response = curl_exec($curl);
        curl_close($curl);

        $data_list = json_decode($response,true);
        $data_list_nv =$data_list['data']['items'];
    }else{
        $curl = curl_init();
        $token = $_COOKIE['acc_token'];
        curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/list_all_employee_of_company.php');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer '.$token));
        $response = curl_exec($curl);
        curl_close($curl);

        $data_list = json_decode($response,true);
        $data_list_nv =$data_list['data']['items'];

    }
    $count = count($data_list_nv);

    $user = [];
    for ($i = 0; $i < count($data_list_nv); $i++){
        $nv = $data_list_nv[$i];
        $user[$nv["ep_id"]] = $nv;
    }

    if (isset($_COOKIE['user']) && $_COOKIE['user'] != "" && $_COOKIE['role'] == 2) {
		$token = $_COOKIE['acc_token'];
		$curl = curl_init();
		$data = array();
		curl_setopt($curl, CURLOPT_POST, 1);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
		curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/user_info_employee.php');
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));
		$response = curl_exec($curl);
		curl_close($curl);
		$data_tt = json_decode($response, true);
		$tt_user = $data_tt['data']['user_info_result'];
	}
    
    $id_cty = $tt_user['com_id'];


    $dieu_chuyen_kho = "SELECT `kcxl_id`,`kcxl_soPhieu`, `kcxl_trangThai`, `kcxl_ghi_chu`, `kcxl_nguoiTao`, `kcxl_ngayTao`, 
    `kcxl_ngayThucHienDieuChuyen`, `kcxl_phieuDieuChuyenKho`, `kcxl_ngayYeuCauHoanThanh`, `kcxl_khoNhap`, `kcxl_khoXuat`
    FROM `kho-cho-xu-li`
    WHERE `kcxl_check` = 1 AND `kcxl_soPhieu` = 'ĐCK' AND `kcxl_id_ct` = $id_cty ";

    $dieu_chuyen_kho = new db_query($dieu_chuyen_kho);

    header("Content-type: application/octet-stream; charset=utf-8");
    header("Content-Disposition: attachment; filename=excel_dieu_chuyen_kho.xls");
    header("Pragma: no-cache");
    header("Expires: 0");

    echo '<table border="1px solid black">';
    echo '<tr><th colspan="10" style="font-size:18px;height:60px;vertical-align: middle;">Thông tin phiếu điều chuyển kho</th></tr>';
?>

<tr class="tit_tbl color_white font_s16 line_h19 font_w500 back_blue">
    <th>STT
        <span class="span_tbody"></span>
    </th>
    <th>Số phiếu
        <span class="span_tbody"></span>
    </th>
    <th>Trạng thái
        <span class="span_tbody"></span>
    </th>
    <th>Ghi chú
        <span class="span_tbody"></span>
    </th>
    <th>Người tạo
        <span class="span_tbody"></span>
    </th>
    <th>Ngày tạo
        <span class="span_tbody"></span>
    </th>
    <th>Ngày thực hiện điều chuyển
        <span class="span_tbody"></span>
    </th>
    <th>Ngày yêu cầu hoàn thành
        <span class="span_tbody"></span>
    </th>
    <th>Kho xuất
        <span class="span_tbody"></span>
    </th>
    <th>Kho nhập
    </th>
</tr>
<?php $i=1; 
while($row = mysql_fetch_assoc($dieu_chuyen_kho->result)) {
?>
<tr class="color_grey font_s14 line_h17 font_w400" data-id="<?= $row['kcxl_id']; ?>">
    <td><?= $i++; ?></td>
    <td><a href="dieu-chuyen-kho-chi-tiet-<?= $row['kcxl_id']; ?>.html" class="color_blue font_w500">ĐCK - <?= $row['kcxl_id']; ?></a></td>
    <td class="<?= trang_thai_color($row['kcxl_trangThai']); ?>"><?= trang_thai($row['kcxl_trangThai']); ?></td>
    <td style="text-align: left;"><?= $row['kcxl_ghi_chu']; ?></td>
    <td style="text-align: left;">
    <?php 
    $nguoi_tao = $row['kcxl_nguoiTao'];
    $user_id = $user[$nguoi_tao];
    $ten_nguoi_tao = $user_id['ep_name'];
    ?>
    <div class="d_flex flex_start align_c">
        <p><?= $ten_nguoi_tao; ?></p>
    </div>
    </td>
    <td><?php echo date('d/m/Y',strtotime($row['kcxl_ngayTao'])); ?></td>
    <td><?php echo date('d/m/Y',strtotime($row['kcxl_ngayThucHienDieuChuyen'])); ?></td>
    <td><?php echo date('d/m/Y',strtotime($row['kcxl_ngayYeuCauHoanThanh'])); ?></td>
    <?
    $kho_nhap = $row['kcxl_khoNhap'];
    $kho_id = new db_query("SELECT `kho_id`, `kho_name` FROM `kho` WHERE `kho_id` = '".$kho_nhap."' ");
    $kho_name = mysql_fetch_assoc($kho_id->result)['kho_name'];

    $kho_xuat = $row['kcxl_khoXuat'];
    $kho_id1 = new db_query("SELECT `kho_id`, `kho_name` FROM `kho` WHERE `kho_id` = '".$kho_xuat."' ");
    $kho_name1 = mysql_fetch_assoc($kho_id1->result)['kho_name'];
    ?>
    <td style="text-align: left;"><?= $kho_name1 ?></td>
    <td style="text-align: left;"><?= $kho_name; ?></td>
</tr>
<?php }?>