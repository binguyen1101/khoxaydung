<?php
    include("config.php");

    if (isset($_COOKIE['user']) && $_COOKIE['user'] != "" && $_COOKIE['role'] == 2) {
		$token = $_COOKIE['acc_token'];
		$curl = curl_init();
		$data = array();
		curl_setopt($curl, CURLOPT_POST, 1);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
		curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/user_info_employee.php');
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));
		$response = curl_exec($curl);
		curl_close($curl);
		$data_tt = json_decode($response, true);
		$tt_user = $data_tt['data']['user_info_result'];
	}
    
    $id_cty = $tt_user['com_id'];
    $id_nguoi_xoa = $_SESSION['ep_id'];


    $don_vi_tinh = new db_query("SELECT * FROM `don-vi-tinh` WHERE `dvt_check`=1  AND `dvt_id_ct` = $id_cty ");

    header("Content-type: application/octet-stream; charset=utf-8");
    header("Content-Disposition: attachment; filename=excel_dvt.xls");
    header("Pragma: no-cache");
    header("Expires: 0");

    echo '<table border="1px solid black">';
    echo '<tr><th colspan="4" style="font-size:18px;height:60px;vertical-align: middle;">Thông tin đơn vị tính</th></tr>';
?>

<tr class="tit_tbl color_white font_s16 line_h19 font_w500 back_blue">
    <th>STT<span class="span_tbody"></span></th>
    <th>Mã đơn vị tính<span class="span_tbody"></span></th>
    <th>Tên đơn vị tính<span class="span_tbody"></span></th>
    <th>Mô tả</th>
</tr>
<?php $i=1; while($row = mysql_fetch_assoc($don_vi_tinh->result)) : ?>
    <tr class="color_grey font_s14 line_h17 font_w400" data-id=<?= $row['dvt_id']; ?>>
    <td><?php echo $i; ?></td>
    <td>ĐVT - <?=$row['dvt_id'];?></td>
    <td style="text-align: left; padding-left: 15px;"><a class="font_w500 color_grey"><?php echo $row['dvt_name']; ?></a></td>
    <td style="text-align: left; padding-left: 15px;"><?php echo $row['dvt_description']; ?></td>
    </tr>
<?php $i++; endwhile; ?>