<?php
    include("config.php");

    if (isset($_COOKIE['user']) && $_COOKIE['user'] != "" && $_COOKIE['role'] == 2) {
		$token = $_COOKIE['acc_token'];
		$curl = curl_init();
		$data = array();
		curl_setopt($curl, CURLOPT_POST, 1);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
		curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/user_info_employee.php');
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));
		$response = curl_exec($curl);
		curl_close($curl);
		$data_tt = json_decode($response, true);
		$tt_user = $data_tt['data']['user_info_result'];
	}
    
    $id_cty = $tt_user['com_id'];
    $id_nguoi_xoa = $_SESSION['ep_id'];


    $item = new db_query("SELECT `hsx_id`, `hsx_maHangSanXuat`, `hsx_name`, `hsx_description` FROM `hang-san-xuat` WHERE `hsx_check` = 1 AND `hsx_id_ct` = $id_cty ORDER BY `hsx_id` DESC ");

    header("Content-type: application/octet-stream; charset=utf-8");
    header("Content-Disposition: attachment; filename=excel_hsx.xls");
    header("Pragma: no-cache");
    header("Expires: 0");

    echo '<table border="1px solid black">';
    echo '<tr><th colspan="4" style="font-size:18px;height:60px;vertical-align: middle;">Thông tin hãng sản xuất</th></tr>';
?>

<tr class="tit_tbl color_white font_s16 line_h19 font_w500 back_blue">
    <td>STT</td>
    <td>Mã hãng sản xuất</td>
    <td>Tên hãng sản xuất</td>
    <td>Mô tả</td>
</tr>
<? $stt = 1; while (($data = mysql_fetch_assoc($item->result))) { ?>
    <tr class="color_grey font_s14 line_h17 font_w400">
    <td><?= $stt++ ?></td>
    <td>HSX-<?= $data['hsx_id'] ?></td>
    <td style="text-align: left; padding-left: 15px;">
        <a class="color_blue font_w500" href="/hang-san-xuat-chi-tiet-<?= $data['hsx_id'] ?>.html"><?= $data['hsx_name'] ?></a>
    </td>
    <td style="text-align: left; padding-left: 15px;"><?= $data['hsx_description'] ?></td>
    </tr>
<? } ?>