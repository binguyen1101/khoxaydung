<?php
    include("config.php");

    if(isset($_COOKIE['acc_token']) && isset($_COOKIE['rf_token']) && isset($_COOKIE['role']) && $_COOKIE['role'] == 2){
        $curl = curl_init();
        $token = $_COOKIE['acc_token'];
        curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/list_all_my_partner.php?get_all=true');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer '.$token));
        $response = curl_exec($curl);
        curl_close($curl);

        $data_list = json_decode($response,true);
        $data_list_nv =$data_list['data']['items'];
    }else{
        $curl = curl_init();
        $token = $_COOKIE['acc_token'];
        curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/list_all_employee_of_company.php');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer '.$token));
        $response = curl_exec($curl);
        curl_close($curl);

        $data_list = json_decode($response,true);
        $data_list_nv =$data_list['data']['items'];

    }
    $count = count($data_list_nv);

    $user = [];
    for ($i = 0; $i < count($data_list_nv); $i++){
        $nv = $data_list_nv[$i];
        $user[$nv["ep_id"]] = $nv;
    }

    if (isset($_COOKIE['user']) && $_COOKIE['user'] != "" && $_COOKIE['role'] == 2) {
        $token = $_COOKIE['acc_token'];
        $curl = curl_init();
        $data = array();
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/user_info_employee.php');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));
        $response = curl_exec($curl);
        curl_close($curl);
        $data_tt = json_decode($response, true);
        $tt_user = $data_tt['data']['user_info_result'];
    }

    $id_cty = $tt_user['com_id'];


    $kho_cho_xu_li = "SELECT `kcxl_id`, `kcxl_soPhieu`, `kcxl_trangThai`, `kcxl_nguoiTao`, `kcxl_ngayTao`, `kcxl_nguoiThucHien`, `kcxl_ghi_chu` 
    FROM `kho-cho-xu-li` 
    WHERE `kcxl_check` = 1
    AND NOT `kcxl_trangThai` = 4 
    AND NOT `kcxl_trangThai` = 6 
    AND NOT `kcxl_trangThai` = 7 
    AND NOT `kcxl_trangThai` = 12 
    AND `kcxl_id_ct` = $id_cty ";


    $kho_cho_xu_li = new db_query($kho_cho_xu_li);

    header("Content-type: application/octet-stream; charset=utf-8");
    header("Content-Disposition: attachment; filename=excel_kcxl.xls");
    header("Pragma: no-cache");
    header("Expires: 0");

    echo '<table border="1px solid black">';
    echo '<tr><th colspan="8" style="font-size:18px;height:60px;vertical-align: middle;">Thông tin kho chờ xử lí</th></tr>';
?>
<tr class="tit_tbl color_white font_s16 line_h19 font_w500 back_blue">
    <th>STT<span class="span_tbody"></span></th>
    <th>Số phiếu<span class="span_tbody"></span></th>
    <th>Loại phiếu<span class="span_tbody"></span></th>
    <th>Trạng thái<span class="span_tbody"></span></th>
    <th>Người tạo<span class="span_tbody"></span></th>
    <th>Ngày tạo<span class="span_tbody"></span></th>
    <th>Người thực hiện<span class="span_tbody"></span></th>
    <th>Ghi chú</th>
</tr>
<?php $i=1; while($row=mysql_fetch_assoc($kho_cho_xu_li->result)) : ?>
    <tr class="color_grey font_s14 line_h17 font_w400" data-id="<?= $row['kcxl_id']; ?>">
        <td><?php echo $i++; ?></td>
        <td><a class="color_blue font_w500" href="<?php
                $link_phieu = $row['kcxl_soPhieu'];
                switch($link_phieu) {
                    case 'PNK':
                        $link_phieu = 'nhap-kho-chi-tiet-'.$row['kcxl_id'].'.html';
                        echo $link_phieu;
                        break;
                    case 'PXK':
                        $link_phieu = 'xuat-kho-chi-tiet-'.$row['kcxl_id'].'.html';
                        echo $link_phieu;
                        break;
                    case 'ĐCK':
                        $link_phieu = 'dieu-chuyen-kho-chi-tiet-'.$row['kcxl_id'].'.html';
                        echo $link_phieu;
                        break;
                    case 'PKK':
                        $link_phieu = 'kiem-ke-kho-chi-tiet-'.$row['kcxl_id'].'.html';
                        echo $link_phieu;
                        break;
                    default:
                        $link_phieu = 'nghiep-vu-kho-cho-xu-ly-detail-rq.html?id='.$row['kcxl_id'];
                        echo $link_phieu;
                        break;
                }
            ?> ">
        <?php
            $ma_phieu = $row['kcxl_soPhieu'];
            switch($ma_phieu) {
                case 'PNK':
                    $ma_phieu = 'PNK - '.$row['kcxl_id'];
                    echo $ma_phieu;
                    break;
                case 'PXK':
                    $ma_phieu = 'PXK - '.$row['kcxl_id'];
                    echo $ma_phieu;
                    break;
                case 'ĐCK':
                    $ma_phieu = 'ĐCK - '.$row['kcxl_id'];
                    echo $ma_phieu;
                    break;
                case 'PKK':
                    $ma_phieu = 'PKK - '.$row['kcxl_id'];
                    echo $ma_phieu;
                    break;
                default:
                    break;
            }
        ?>
    </a></td>
        <td><?= loai_phieu($row['kcxl_soPhieu']); ?></td>
        <td class="color_blue"><?= trang_thai($row['kcxl_trangThai']);?></td>
        <td>
        <?php 
            $nguoi_tao = $row['kcxl_nguoiTao'];
            $user_id = $user[$nguoi_tao];
            $ten_nguoi_tao = $user_id['ep_name'];
                    $anh_nguoi_tao = $user_id['ep_image'];
                    if($anh_nguoi_tao == ""){
                        $anh = '../images/ava_ad.png';
                    }else{
                        $anh = 'https://chamcong.24hpay.vn/upload/employee/'.$anh_nguoi_tao;
                    }
        ?>
            <div class="d_flex flex_start align_c">
            <p><?= $ten_nguoi_tao; ?></p>
            </div>
        </td>
        <td><?php echo date('d/m/Y',strtotime($row['kcxl_ngayTao'])); ?></td>
        <td>
        <?php 
            $nguoi_thuc_hien = $row['kcxl_nguoiThucHien'];
            $user_id_ntt = $user[$nguoi_thuc_hien];
            $ten_nguoi_thuc_hien = $user_id_ntt['ep_name'];
                    $anh_nguoi_thuc_hien = $user_id_ntt['ep_image'];
                    if($anh_nguoi_thuc_hien == ""){
                        $anh_ntt = '../images/ava_ad.png';
                    }else{
                        $anh_ntt = 'https://chamcong.24hpay.vn/upload/employee/'.$anh_nguoi_thuc_hien;
                    }
        ?>
            <div class="d_flex flex_start align_c">
            <p><?= $ten_nguoi_thuc_hien; ?></p>
            </div>
        </td>
        <td style="text-align: left;"><?php echo $row['kcxl_ghi_chu']; ?></td>
    </tr>
<?php 
endwhile; ?>

