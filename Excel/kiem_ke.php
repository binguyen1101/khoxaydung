<?php
    include("config.php");

    if(isset($_COOKIE['acc_token']) && isset($_COOKIE['rf_token']) && isset($_COOKIE['role']) && $_COOKIE['role'] == 2){
        $curl = curl_init();
        $token = $_COOKIE['acc_token'];
        curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/list_all_my_partner.php?get_all=true');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer '.$token));
        $response = curl_exec($curl);
        curl_close($curl);

        $data_list = json_decode($response,true);
        $data_list_nv =$data_list['data']['items'];
    }else{
        $curl = curl_init();
        $token = $_COOKIE['acc_token'];
        curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/list_all_employee_of_company.php');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer '.$token));
        $response = curl_exec($curl);
        curl_close($curl);

        $data_list = json_decode($response,true);
        $data_list_nv =$data_list['data']['items'];

    }
    $count = count($data_list_nv);

    $user = [];
    for ($i = 0; $i < count($data_list_nv); $i++){
        $nv = $data_list_nv[$i];
        $user[$nv["ep_id"]] = $nv;
    }

    if (isset($_COOKIE['user']) && $_COOKIE['user'] != "" && $_COOKIE['role'] == 2) {
        $token = $_COOKIE['acc_token'];
        $curl = curl_init();
        $data = array();
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/user_info_employee.php');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));
        $response = curl_exec($curl);
        curl_close($curl);
        $data_tt = json_decode($response, true);
        $tt_user = $data_tt['data']['user_info_result'];
    }

    $id_cty = $tt_user['com_id'];


    $kiemke = "SELECT `kcxl_id`, `kcxl_soPhieu`,`kcxl_trangThai`,`kcxl_khoThucHienKiemKe`,`kcxl_ngayThucHienKiemKe`,`kcxl_ngayYeuCauHoanThanh`,`kcxl_ngayHoanThanh`,`kcxl_nguoiTao`, `kcxl_ngayTao`,`kcxl_ghi_chu`,`kho_name` FROM `kho-cho-xu-li`
    INNER JOIN `kho` ON `kcxl_khoThucHienKiemKe` = `kho_id`
    WHERE `kcxl_soPhieu` = 'PKK' AND `kcxl_check`= 1 AND `kcxl_id_ct` = $id_cty ";

    $kiemke = new db_query($kiemke);

    header("Content-type: application/octet-stream; charset=utf-8");
    header("Content-Disposition: attachment; filename=excel_kiem_ke_kho.xls");
    header("Pragma: no-cache");
    header("Expires: 0");

    echo '<table border="1px solid black">';
    echo '<tr><th colspan="10" style="font-size:18px;height:60px;vertical-align: middle;">Thông tin phiếu kiểm kê kho</th></tr>';
?>

<tr class="tit_tbl color_white font_s16 line_h19 font_w500 back_blue">
    <td style="width: 2.6%;" class="color_white font_s16 line_h19 font_w500">STT
        <span class="span_tbody"></span>
    </td>
    <td class="color_white font_s16 line_h19 font_w500">Số phiếu
        <span class="span_tbody"></span>
    </td>
    <td class="color_white font_s16 line_h19 font_w500" style="width:10%;">Trạng thái
        <span class="span_tbody"></span>
    </td>
    <td class="color_white font_s16 line_h19 font_w500">Kho kiểm kê
        <span class="span_tbody"></span>
    </td>
    <td class="color_white font_s16 line_h19 font_w500">Ngày thực hiện kiểm kê
        <span class="span_tbody"></span>
    </td>
    <td class="color_white font_s16 line_h19 font_w500">Ngày yêu cầu hoàn thành
        <span class="span_tbody"></span>
    </td>
    <td class="color_white font_s16 line_h19 font_w500">Ngày hoàn thành
        <span class="span_tbody"></span>
    </td>
    <td class="color_white font_s16 line_h19 font_w500">Người tạo
        <span class="span_tbody"></span>
    </td>
    <td class="color_white font_s16 line_h19 font_w500">Ngày tạo
        <span class="span_tbody"></span>
    </td>
    <td class="color_white font_s16 line_h19 font_w500">Ghi chú
    </td>
</tr>
<? $stt = 1; while($row= mysql_fetch_assoc($kiemke -> result)){?>
<tr class="color_grey font_s14 line_h17 font_w400"  data-id="<?= $row['kcxl_id']; ?>">
    <td><?=$stt++ ?></td>
    <td><a href="kiem-ke-kho-chi-tiet-<?= $row['kcxl_id']; ?>.html" class="color_blue font_w500">PKK - <?= $row['kcxl_id'];?></a></td>
    <td class="<?= trang_thai_color($row['kcxl_trangThai']); ?>"><?= trang_thai($row['kcxl_trangThai']); ?></td>
    <td style="text-align: left;"><?=$row['kho_name']?></td>
    <td><?php echo date('d/m/Y',strtotime($row['kcxl_ngayThucHienKiemKe'])); ?></td>
    <td><?php echo date('d/m/Y',strtotime($row['kcxl_ngayYeuCauHoanThanh'])); ?></td>
    <td><?php echo date('d/m/Y',strtotime($row['kcxl_ngayHoanThanh'])); ?></td>
    <td style="text-align: left;">
    <?php 
        $nguoi_tao = $row['kcxl_nguoiTao'];
        $user_id = $user[$nguoi_tao];
        $ten_nguoi_tao = $user_id['ep_name'];
    ?>
    <div class="d_flex flex_start align_c">
        <p><?= $ten_nguoi_tao; ?></p>
    </div>
    </td>
    <td><?php echo date('d/m/Y',strtotime($row['kcxl_ngayTao'])); ?></td>
    <td style="text-align: left;"><?=$row['kcxl_ghi_chu'] ?></td>
<?}?>