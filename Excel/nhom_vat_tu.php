<?php
    include("config.php");

    if (isset($_COOKIE['user']) && $_COOKIE['user'] != "" && $_COOKIE['role'] == 2) {
        $token = $_COOKIE['acc_token'];
        $curl = curl_init();
        $data = array();
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/user_info_employee.php');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));
        $response = curl_exec($curl);
        curl_close($curl);
        $data_tt = json_decode($response, true);
        $tt_user = $data_tt['data']['user_info_result'];
    }

    $id_cty = $tt_user['com_id'];
    $id_nguoi_xoa = $_SESSION['ep_id'];


    $nhom_vat_tu = new db_query("SELECT * FROM `nhom-vat-tu-thiet-bi` WHERE `nvt_check` = 1 AND `nvt_id_ct` = $id_cty ORDER BY `nvt_id` DESC ");   

    header("Content-type: application/octet-stream; charset=utf-8");
    header("Content-Disposition: attachment; filename=excel_nvt.xls");
    header("Pragma: no-cache");
    header("Expires: 0");

    echo '<table border="1px solid black">';
    echo '<tr><th colspan="4" style="font-size:18px;height:60px;vertical-align: middle;">Thông tin nhóm vật tư</th></tr>';
?>

<tr class="tit_tbl color_white font_s16 line_h19 font_w500 back_blue">
    <th>STT<span class="span_tbody"></span></th>
    <th>Mã nhóm vật tư thiết bị<span class="span_tbody"></span></th>
    <th>Tên nhóm vật tư thiết bị<span class="span_tbody"></span></th>
    <th>Mô tả</th>
</tr>
<?php $i = 1; while($row = mysql_fetch_assoc($nhom_vat_tu->result)) : ?>
<tr class="color_grey font_s14 line_h17 font_w400" data-id="<?= $row['nvt_id']; ?>">
    <td><?php echo $i++ ?></td>
    <td>NVT - <?=$row['nvt_id'];?></td>
    <td style="text-align: left; padding-left: 15px;">
        <a class="color_blue font_w500" href="/nhom-vat-tu-thiet-bi-chi-tiet-<?= $row['nvt_id']; ?>.html"><?php echo $row['nvt_name']; ?></a>
    </td>
    <td style="text-align: left; padding-left: 15px;"><?php echo $row['nvt_description']; ?></td>
</tr>
<?php endwhile;?>