<?php
    include("config.php");

    if (isset($_SESSION['quyen']) && $_SESSION['quyen'] == 1) {
        $curl = curl_init();
        $token = $_COOKIE['acc_token'];
        curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/list_all_employee_of_company.php');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));
        $response = curl_exec($curl);
        curl_close($curl);
    } elseif (isset($_SESSION['quyen']) && ($_SESSION['quyen'] == 2)) {
        $curl = curl_init();
        $token = $_COOKIE['acc_token'];
        curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/list_all_my_partner.php?get_all=true');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));
        $response = curl_exec($curl);
        curl_close($curl);
    }

    $data_list = json_decode($response, true);
    $data_list_nv = $data_list['data']['items'];

    $user_ct = [];
    for ($i = 0; $i < count($data_list_nv); $i++){
        $item = $data_list_nv[$i];
        $user_ct[$i] = $item;
    }

    $chuc_vu = array("1"=>"SINH VIÊN THỰC TẬP", "2"=>"NHÂN VIÊN THỬ VIỆC", "3"=>"NHÂN VIÊN CHÍNH THỨC",
                    "4"=>"TRƯỞNG NHÓM", "5"=>"PHÓ TRƯỞNG PHÒNG", "6"=>"TRƯỞNG PHÒNG",
                    "7"=>"PHÓ GIÁM ĐỐC", "8"=>"GIÁM ĐỐC", "9"=>"NHÂN VIÊN PART TIME",
                    "10"=>"PHÓ BAN DỰ ÁN", "11"=>"TRƯỞNG BAN DỰ ÁN", "12"=>"PHÓ TỔ TRƯỞNG",
                    "13"=>"TỔ TRƯỞNG", "14"=>"PHÓ TỔNG GIÁM ĐỐC", "16"=>"TỔNG GIÁM ĐỐC",
                    "17"=>"THÀNH VIÊN HỘI ĐỒNG QUẢN TRỊ", "18"=>"PHÓ CHỦ TỊCH HỘI ĐỒNG QUẢN TRỊ", "19"=>"CHỦ TỊCH HỘI ĐỒNG QUẢN TRỊ",
                    "20"=>"NHÓM PHÓ", "21"=>"TỔNG GIÁM ĐỐC TẬP ĐOÀN", "22"=>"PHÓ TỔNG GIÁM ĐỐC TẬP ĐOÀN"
                    );

    header("Content-type: application/octet-stream; charset=utf-8");
    header("Content-Disposition: attachment; filename=excel_nv.xls");
    header("Pragma: no-cache");
    header("Expires: 0");

    echo '<table border="1px solid black">';
    echo '<tr><th colspan="4" style="font-size:18px;height:60px;vertical-align: middle;">Thông tin nhân viên</th></tr>';
?>

<tr>
    <th>Mã NV
        <span class="span_tbody"></span>
    </th>
    <th>Họ tên
        <span class="span_tbody"></span>
    </th>
    <th>Phòng ban
        <span class="span_tbody"></span>
    </th>
    <th>Chức danh
    </th>
</tr>
<?php foreach($user_ct as $user) {?>
<tr data-id = "<?=$user['ep_id']?>">
    <td class="font_s14 line_16 color_grey">NV -
        <?=$user['ep_id']?>
    </td>
    <td class="font_s14 line_16 color_grey">
        <div class="d_flex align_c">
            <p>
                <?=$user['ep_name']?>
            </p>
        </div>
    </td>
    <td class="font_s14 line_16 color_grey" style="text-align: left;">
        <?=$user['dep_name']?>
    </td>
    <td class="font_s14 line_16 color_grey">
        <?php 
        $id_chu_vu = $user['position_id'];
        echo $chuc_vu[$id_chu_vu];
    ?>
    </td>
</tr>
<?php }?>