<?php
    include("config.php");

    if(isset($_COOKIE['acc_token']) && isset($_COOKIE['rf_token']) && isset($_COOKIE['role']) && $_COOKIE['role'] == 2){
        $curl = curl_init();
        $token = $_COOKIE['acc_token'];
        curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/list_all_my_partner.php?get_all=true');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer '.$token));
        $response = curl_exec($curl);
        curl_close($curl);

        $data_list = json_decode($response,true);
        $data_list_nv =$data_list['data']['items'];
    }else{
        $curl = curl_init();
        $token = $_COOKIE['acc_token'];
        curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/list_all_employee_of_company.php');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer '.$token));
        $response = curl_exec($curl);
        curl_close($curl);

        $data_list = json_decode($response,true);
        $data_list_nv =$data_list['data']['items'];

    }
    $count = count($data_list_nv);

    $user = [];
    for ($i = 0; $i < count($data_list_nv); $i++){
        $nv = $data_list_nv[$i];
        $user[$nv["ep_id"]] = $nv;
    }

    if (isset($_COOKIE['user']) && $_COOKIE['user'] != "" && $_COOKIE['role'] == 2) {
		$token = $_COOKIE['acc_token'];
		$curl = curl_init();
		$data = array();
		curl_setopt($curl, CURLOPT_POST, 1);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
		curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/user_info_employee.php');
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));
		$response = curl_exec($curl);
		curl_close($curl);
		$data_tt = json_decode($response, true);
		$tt_user = $data_tt['data']['user_info_result'];
	}
    
    $id_cty = $tt_user['com_id'];


    $main_nhapKho = "SELECT `kcxl_id`,`kcxl_hinhThuc`,`kcxl_soPhieu`,`kcxl_trangThai`,`kcxl_ngayNhapKho`,`kcxl_khoNhap`,`kcxl_nguoiTao`,`kcxl_ngayTao`,`kcxl_ghi_chu`,`kho_name` FROM `kho-cho-xu-li`
    LEFT JOIN `kho` ON `kcxl_khoNhap` = `kho_id`
    WHERE `kcxl_soPhieu`= 'PNK' AND `kcxl_check`=1 AND `kcxl_id_ct` = $id_cty ";
    $main_nhapKho = new db_query($main_nhapKho);

    header("Content-type: application/octet-stream; charset=utf-8");
    header("Content-Disposition: attachment; filename=excel_phieu_nhap.xls");
    header("Pragma: no-cache");
    header("Expires: 0");

    echo '<table border="1px solid black">';
    echo '<tr><th colspan="9" style="font-size:18px;height:60px;vertical-align: middle;">Thông tin phiếu nhập kho</th></tr>';
?>

<tr>
    <th class="font_s16 line_h19">STT
        <span class="span_thread"></span>
    </th>
    <th class="font_s16 line_h19">Số phiếu
        <span class="span_thread"></span>
    </th>
    <th class="font_s16 line_h19">Hình thức nhập kho
        <span class="span_thread"></span>
    </th>
    <th class="font_s16 line_h19">Trạng thái
        <span class="span_thread"></span>
    </th>
    <th>Ghi chú
        <span class="span_thread"></span>
    </th>
    <th class="font_s16 line_h19">Người tạo
        <span class="span_thread"></span>
    </th>
    <th class="font_s16 line_h19">Ngày tạo
        <span class="span_thread"></span>
    </th>
    <th class="font_s16 line_h19">Ngày nhập kho
        <span class="span_thread"></span>
    </th>
    <th class="font_s16 line_h19">Kho nhập
    </th>
</tr>
<?php $stt =1; while($item = mysql_fetch_assoc($main_nhapKho -> result)){?>
<tr class="color_grey font_s14 line_h17 font_w400" data-id="<?= $item['kcxl_id']; ?>">
    <td><?= $stt++; ?></td>
    <td><a href="nhap-kho-chi-tiet-<?= $item['kcxl_id']; ?>.html" class="color_blue font_s14 line_h17 font_w500">PNK - <?= $item['kcxl_id']; ?></a></td>
    <td class="color_grey font_s14 line_h16"><?= hinh_thuc_nhap($item['kcxl_hinhThuc']);?>                
    </td>
    <td class="<?= trang_thai_color($item['kcxl_trangThai']); ?> font_s14 line_h17 font_w400"><?= trang_thai($item['kcxl_trangThai']); ?></td>
    <td class="font_s14 line_h16" style="text-align: left;"><?=$item['kcxl_ghi_chu'] ?></td>
    <td style="text-align: left;">
        <?php 
        $nguoi_tao = $item['kcxl_nguoiTao'];
        $user_id = $user[$nguoi_tao];
        $ten_nguoi_tao = $user_id['ep_name'];
        ?>
        <div class="d_flex flex_start align_c">
            <p><?= $ten_nguoi_tao; ?></p>
        </div>
    </td>
    <td class="font_s14 line_h16"><?=$item['kcxl_ngayTao']; ?></td>
    <td class="font_s14 line_h16"><?=$item['kcxl_ngayNhapKho']; ?></td>
    <td class="font_s14 line_h16" style="text-align: left;"><?=$item['kho_name']; ?></td>
</tr>
<?}?>      