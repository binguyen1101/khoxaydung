<?php
    include("config.php");

    if (isset($_COOKIE['user']) && $_COOKIE['user'] != "" && $_COOKIE['role'] == 2) {
        $token = $_COOKIE['acc_token'];
        $curl = curl_init();
        $data = array();
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/user_info_employee.php');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));
        $response = curl_exec($curl);
        curl_close($curl);
        $data_tt = json_decode($response, true);
        $tt_user = $data_tt['data']['user_info_result'];
    }

    $id_cty = $tt_user['com_id'];

    $id_kho = getValue('id', 'int', 'GET', '');

    $list_all = new db_query("SELECT `dsvt_id`, `dsvt_maVatTuThietBi`, `dsvt_name`, `dsvt_donGia`, `dsvt_soLuongTon`, `dsvt_kho`, `dsvt_description`, `nvt_name`, `dvt_name`, `hsx_name`, `xx_name` FROM `danh-sach-vat-tu` 
    LEFT JOIN `nhom-vat-tu-thiet-bi` ON `dsvt_nhomVatTuThietBi` = `nvt_id`
    LEFT JOIN `don-vi-tinh` ON `dsvt_donViTinh` = `dvt_id`
    LEFT JOIN `hang-san-xuat` ON `dsvt_hangSanXuat` = `hsx_id`
    LEFT JOIN `xuat-xu` ON `dsvt_xuatXu` = `xx_id`
    WHERE `dsvt_check` = 1 AND `dsvt_id_ct` = $id_cty ");

    $responsive = [];
    $i=0;
    while (($item = mysql_fetch_assoc($list_all->result))) {
        $check_kho = explode(',',$item['dsvt_kho']);
        if(in_array($id_kho,$check_kho)){
            $info_k['dsvt_id'] = $item['dsvt_id'];
            $info_k['dsvt_name'] = $item['dsvt_name'];
            $info_k['dsvt_soLuongTon'] = $item['dsvt_soLuongTon'];
            $info_k['nvt_name'] = $item['nvt_name'];
            $info_k['hsx_name'] = $item['hsx_name'];
            $info_k['dvt_name'] = $item['dvt_name'];
            $info_k['xx_name'] = $item['xx_name'];
            $info_k['dsvt_kho'] = $item['dsvt_kho'];
            $info_k['dsvt_description'] = $item['dsvt_description'];
            $responsive[$i] = $info_k;
            $i++;
        }
    }
    

    header("Content-type: application/octet-stream; charset=utf-8");
    header("Content-Disposition: attachment; filename=excel_dsvt.xls");
    header("Pragma: no-cache");
    header("Expires: 0");

    echo '<table border="1px solid black">';
    echo '<tr><th colspan="8" style="font-size:18px;height:60px;vertical-align: middle;">Thông tin vật tư</th></tr>';
?>

<tr>
    <th>
        STT
        <span class="span_tbody"></span>
    </th>
    <th>Mã vật tư thiết bị
        <span class="span_tbody"></span>
    </th>
    <th>Tên đầy đủ vật tư thiết bị
        <span class="span_tbody"></span>
    </th>
    <th>Nhóm vật tư thiết bị
        <span class="span_tbody"></span>
    </th>
    <th>Đơn vị tính
        <span class="span_tbody"></span>
    </th>
    <th>Số lượng
        <span class="span_tbody"></span>
    </th>
    <th>Hãng sản xuất
        <span class="span_tbody"></span>
    </th>
    <th>Xuất xứ
        <span class="span_tbody"></span>
    </th>
</tr>
<?php $stt = 1; if(!empty($responsive)){
    foreach ($responsive as $val){ ?>
<tr class="font_s14 line_16 color_grey" data-id="<?= $item['dsvt_id'] ?>">
    <td><?= $stt++ ?></td>
    <td>VT - <?=$val['dsvt_id']?></td>
    <td style="text-align: left;">
        <a class="color_blue line_16 font_s14 font_w500" href="/danh-sach-vat-tu-thiet-bi-chi-tiet-<?= $val['dsvt_id'] ?>.html"><?= $val['dsvt_name']; ?></a>
    </td>
    <td style="text-align: left;"><?= $val['nvt_name']; ?></td>
    <td><?= $val['dvt_name']; ?></td>
    <td><?php  $sl = json_decode($val['dsvt_soLuongTon']);
                echo $sl->$id_kho;
    ?></td>										
    <td style="text-align: left;"><?= $val['hsx_name']; ?></td>
    <td><?= $val['xx_name']; ?></td>
</tr>
<?php }}?>