<?php
    include("config.php");

    $id_vt = getValue('id', 'int', 'GET','');

    isset($_GET['id']) ? $id = $_GET['id'] : $id = "";
    if ($id != "") {
        $item = new db_query("SELECT `dsvt_id`, `dsvt_maVatTuThietBi`, `dsvt_name`, `dsvt_donGia`, `dsvt_description`,`dsvt_dateCreate`,`dsvt_img`, `nvt_name`, `dvt_name`, `hsx_name`, `xx_name`, `dsvt_userCreateId` FROM `danh-sach-vat-tu` 
        INNER JOIN `nhom-vat-tu-thiet-bi` ON `dsvt_nhomVatTuThietBi` = `nvt_id`
        INNER JOIN `don-vi-tinh` ON `dsvt_donViTinh` = `dvt_id`
        INNER JOIN `hang-san-xuat` ON `dsvt_hangSanXuat` = `hsx_id`
        INNER JOIN `xuat-xu` ON `dsvt_xuatXu` = `xx_id`
        WHERE `dsvt_id` = $id");
        if (isset($_SESSION['quyen']) && $_SESSION['quyen'] == 1) {
            $curl = curl_init();
            $token = $_COOKIE['acc_token'];
            curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/list_all_employee_of_company.php');
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
            curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));
            $response = curl_exec($curl);
            curl_close($curl);

            $data_list = json_decode($response, true);

            $data_list_nv = $data_list['data']['items'];
            $count = count($data_list_nv);
        } elseif (isset($_SESSION['quyen']) && ($_SESSION['quyen'] == 2)) {
            $curl = curl_init();
            $token = $_COOKIE['acc_token'];
            curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/list_all_my_partner.php?get_all=true');
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
            curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));
            $response = curl_exec($curl);
            curl_close($curl);

            $data_list = json_decode($response, true);
            $data_list_nv = $data_list['data']['items'];
            $count = count($data_list_nv);
        }
        $newArr = [];
        for ($i = 0; $i < count($data_list_nv); $i++) {
            $value = $data_list_nv[$i];
            $newArr[$value["ep_id"]] = $value;
        }
    };
    $data = mysql_fetch_assoc($item->result);

    header("Content-type: application/octet-stream; charset=utf-8");
    header("Content-Disposition: attachment; filename=excel_vattu.xls");
    header("Pragma: no-cache");
    header("Expires: 0");

    echo '<table border="1px solid black">';
    // echo '<tr><th colspan="3" style="font-size:18px;height:60px;vertical-align: middle;">Thông tin vật tư</th></tr>';

?>
<thead>
    <th colspan="2" style="font-size:18px;height:60px;vertical-align: left;">Thông tin vật tư thiết bị</th>
</thead>
<tr>
    <td>Mã vật tư thiết bị:</td>
    <td>VT-<?= $data['dsvt_id']; ?></td>
</tr>
<tr>
    <td>Tên đầy đủ vật tư thiết bị:</td>
    <td><?= $data['dsvt_name']; ?></td>
</tr>
<tr>
    <td>Nhóm vật tư thiết bị:</td>
    <td><?= $data['nvt_name']; ?></td>
</tr>
<tr>
    <td>Đơn vị tính:</td>
    <td><?= $data['dvt_name']; ?></td>
</tr>
<tr>
    <td>Đơn giá:</td>
    <td><?= $data['dsvt_donGia']; ?> VNĐ</td>
</tr>
<tr>
    <td>Hãng sản xuất:</td>
    <td><?= $data['hsx_name']; ?></td>
</tr>
<tr>
    <td>Xuất xứ:</td>
    <td><?= $data['xx_name']; ?></td>
</tr>
<tr>
    <td>Người tạo:</td>
    <td><?= $newArr[$data['dsvt_userCreateId']]['ep_name']; ?></td>
</tr>
<tr>
    <td>Ngày tạo:</td>
    <td><?= $data['dsvt_dateCreate'] ?></td>
</tr>
<tr>
    <td>Mô tả:</td>
    <td><?= $data['dsvt_description'] ?></td>
</tr>
