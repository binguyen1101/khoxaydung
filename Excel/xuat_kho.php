<?php
    include("config.php");

    if(isset($_COOKIE['acc_token']) && isset($_COOKIE['rf_token']) && isset($_COOKIE['role']) && $_COOKIE['role'] == 2){
        $curl = curl_init();
        $token = $_COOKIE['acc_token'];
        curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/list_all_my_partner.php?get_all=true');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer '.$token));
        $response = curl_exec($curl);
        curl_close($curl);

        $data_list = json_decode($response,true);
        $data_list_nv =$data_list['data']['items'];
    }else{
        $curl = curl_init();
        $token = $_COOKIE['acc_token'];
        curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/list_all_employee_of_company.php');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer '.$token));
        $response = curl_exec($curl);
        curl_close($curl);

        $data_list = json_decode($response,true);
        $data_list_nv =$data_list['data']['items'];

    }
    $count = count($data_list_nv);

    $user = [];
    for ($i = 0; $i < count($data_list_nv); $i++){
        $nv = $data_list_nv[$i];
        $user[$nv["ep_id"]] = $nv;
    }

    if (isset($_COOKIE['user']) && $_COOKIE['user'] != "" && $_COOKIE['role'] == 2) {
		$token = $_COOKIE['acc_token'];
		$curl = curl_init();
		$data = array();
		curl_setopt($curl, CURLOPT_POST, 1);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
		curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/user_info_employee.php');
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));
		$response = curl_exec($curl);
		curl_close($curl);
		$data_tt = json_decode($response, true);
		$tt_user = $data_tt['data']['user_info_result'];
	}
    
    $id_cty = $tt_user['com_id'];


    $item = "SELECT `kcxl_id`, `kcxl_hinhThuc`, `kcxl_nguoiTao`, `kcxl_ngayTao`, `kcxl_trangThai`, `kcxl_khoXuat`, `kcxl_ngayXuatKho`, `kcxl_ghi_chu`, `kho_name` FROM `kho-cho-xu-li`
    INNER JOIN `kho` ON `kcxl_khoXuat` = `kho_id` WHERE `kcxl_soPhieu` = 'PXK' AND `kcxl_check` = 1 AND `kcxl_id_ct` = $id_cty ";
    $item = new db_query($item);

    header("Content-type: application/octet-stream; charset=utf-8");
    header("Content-Disposition: attachment; filename=excel_phieu_xuat.xls");
    header("Pragma: no-cache");
    header("Expires: 0");

    echo '<table border="1px solid black">';
    echo '<tr><th colspan="9" style="font-size:18px;height:60px;vertical-align: middle;">Thông tin phiếu xuất kho</th></tr>';
?>

<tr class="tit_tbl color_white font_s16 line_h19 font_w500 back_blue">
    <th>STT
        <span class="span_tbody"></span>
    </th>
    <th>Số phiếu
        <span class="span_tbody"></span>
    </th>
    <th>Hình thức xuất kho
        <span class="span_tbody"></span>
    </th>
    <th>Trạng thái
        <span class="span_tbody"></span>
    </th>
    <th>Ghi chú
        <span class="span_tbody"></span>
    </th>
    <th>Người tạo
        <span class="span_tbody"></span>
    </th>
    <th>Ngày tạo
        <span class="span_tbody"></span>
    </th>
    <th>Ngày xuất kho
        <span class="span_tbody"></span>
    </th>
    <th>Kho xuất
    </th>
</tr>
<?php $stt = 1; while (($data = mysql_fetch_assoc($item->result))) { ?>
<tr class="color_grey font_s14 line_h16 font_w400">
    <td>
        <?= $stt++ ?>
    </td>
    <td>
        <a href="/xuat-kho-chi-tiet-<?= $data['kcxl_id'] ?>.html" class="color_blue font_w500">PXK -
            <?= $data['kcxl_id']?>
        </a>
    </td>
    <td>
        <?= hinh_thuc_xuat($data['kcxl_hinhThuc']) ?>
    </td>

    <td class="<?= trang_thai_color($data['kcxl_trangThai'])?>">
        <?= trang_thai($data['kcxl_trangThai']) ?>
    </td>
    <td>
        <?= $data['kcxl_ghi_chu']?>
    </td>
    <td style="text-align: left;">
        <div class="d_flex align_c">
            <p class=" font_s14 line_16 color_grey font_w500">
                <?= $user[$data['kcxl_nguoiTao']]['ep_name'] ?>
            </p>
        </div>
    </td>
    <td>
        <?= $data['kcxl_ngayTao']?>
    </td>
    <td>
        <?= $data['kcxl_ngayXuatKho']?>
    </td>
    <td style="text-align: left;">
        <?= $data['kho_name']?>
    </td>
</tr>
<? } ?>      