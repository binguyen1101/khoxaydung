<?
include("config.php");

if (isset($_COOKIE['user']) && $_COOKIE['user'] != "" && $_COOKIE['role'] == 1) {
  $token = $_COOKIE['acc_token'];
  $curl = curl_init();
  $data = array();
  curl_setopt($curl, CURLOPT_POST, 1);
  curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
  curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/user_info_company.php');
  curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
  curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));

  $response = curl_exec($curl);
  curl_close($curl);
  $data_tt = json_decode($response, true);
  $tt_user = $data_tt['data']['user_info_result'];
  $user_id = $_SESSION['com_id'];
}
if (isset($_COOKIE['user']) && $_COOKIE['user'] != "" && $_COOKIE['role'] == 2) {
  $token = $_COOKIE['acc_token'];
  $curl = curl_init();
  $data = array();
  curl_setopt($curl, CURLOPT_POST, 1);
  curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
  curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/user_info_employee.php');
  curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
  curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));
  $response = curl_exec($curl);
  curl_close($curl);
  $data_tt = json_decode($response, true);
  $tt_user = $data_tt['data']['user_info_result'];
  $user_id = $_SESSION['ep_id'];
}

$id_pkk = $_POST['id_pkk'];
$value = $_POST['value'];
$id_cty = $_POST['id_cty'];
$trangThai = $_POST['trangThai'];
$kcxl_liDoTuChoi = $_POST['kcxl_liDoTuChoi'];
$vat_tu = $_POST['vat_tu'];
// $ghichu = $_POST['ghichu'];
$date = date('Y-m-d', time());

if ($value == '10') {
  if ($_COOKIE['role'] == 1) {
    $edit = new db_query("UPDATE `kho-cho-xu-li` SET `kcxl_trangThai` = 10, `kcxl_liDoTuChoi` = '$kcxl_liDoTuChoi' WHERE `kcxl_id_ct` = $id_cty AND `kcxl_id` = $id_pkk");
  }
  if ($_COOKIE['role'] == 2) {
    $edit = new db_query("UPDATE `kho-cho-xu-li` SET `kcxl_trangThai` = 10, `kcxl_liDoTuChoi` = '$kcxl_liDoTuChoi' WHERE `kcxl_id_ct` = $id_cty AND `kcxl_id` = $id_pkk");
  }
}
if ($value == '11') {
  if ($_COOKIE['role'] == 1) {
    $edit = new db_query("UPDATE `kho-cho-xu-li` SET `kcxl_trangThai` = 11 WHERE `kcxl_id_ct` = $id_cty AND `kcxl_id` = $id_pkk");
  }
  if ($_COOKIE['role'] == 2) {
    $edit = new db_query("UPDATE `kho-cho-xu-li` SET `kcxl_trangThai` = 11 WHERE `kcxl_id_ct` = $id_cty AND `kcxl_id` = $id_pkk");
  }
}
if ($value == '13') {
  if ($_COOKIE['role'] == 1) {
    $edit = new db_query("UPDATE `kho-cho-xu-li` SET `kcxl_check` = 0, `kcxl_nguoiXoa` = 0 WHERE `kcxl_id_ct` = $id_cty AND `kcxl_id` = $id_pkk");
  }
  if ($_COOKIE['role'] == 2) {
    $edit = new db_query("UPDATE `kho-cho-xu-li` SET `kcxl_check` = 0, `kcxl_nguoiXoa` = '$user_id' WHERE `kcxl_id_ct` = $id_cty AND `kcxl_id` = $id_pkk");
  }
}
if ($value == '9') {
  if (!isset($vat_tu)) {
    if ($_COOKIE['role'] == 1) {
      $edit = new db_query("UPDATE `kho-cho-xu-li` SET `kcxl_trangThai` = 9, `kcxl_nguoiHoanThanh` = 0, `kcxl_ngayHoanThanh` = '$date'  WHERE `kcxl_id_ct` = $id_cty AND `kcxl_id` = $id_pkk");
    }
    if ($_COOKIE['role'] == 2) {
      $edit = new db_query("UPDATE `kho-cho-xu-li` SET `kcxl_trangThai` = 9, `kcxl_nguoiHoanThanh` = '$user_id' , `kcxl_ngayHoanThanh` = '$date'  WHERE `kcxl_id_ct` = $id_cty AND `kcxl_id` = $id_pkk");
    }
  }
  if (isset($vat_tu)) {
    if ($_COOKIE['role'] == 1) {
      $edit = new db_query("UPDATE `kho-cho-xu-li` SET `kcxl_trangThai` = 9, `kcxl_nguoiHoanThanh` = 0 , `kcxl_ngayHoanThanh` = '$date'  WHERE `kcxl_id_ct` = $id_cty AND `kcxl_id` = $id_pkk");
    }
    if ($_COOKIE['role'] == 2) {
      $edit = new db_query("UPDATE `kho-cho-xu-li` SET `kcxl_trangThai` = 9, `kcxl_nguoiHoanThanh` = '$user_id', `kcxl_ngayHoanThanh` = '$date'  WHERE `kcxl_id_ct` = $id_cty AND `kcxl_id` = $id_pkk");
    }
    $delete = new db_execute("DELETE FROM `so-luong-vat-tu` WHERE `slvt_idPhieu` = $id_pkk");
    for ($i = 0; $i < count(json_decode($vat_tu)); $i++) {
      $data_id = json_decode($vat_tu)[$i]->id;
      $data_soluong = json_decode($vat_tu)[$i]->sl;
      $data_ghichu = json_decode($vat_tu)[$i]->ghichu;
      $data_slht = json_decode($vat_tu)[$i]->slht;
      $data_kho = json_decode($vat_tu)[$i]->kho;
      $add_vattu = new db_execute("INSERT INTO `so-luong-vat-tu`(`slvt_idPhieu`, `slvt_maPhieu`, `slvt_maVatTuThietBi`, `slvt_maKho`, `slvt_soLuongKiemKe`, `slvt_ghiChu`,
      `slvt_id_ct`, `slvt_timeCurrentDay`, `slvt_so_luong_he_thong`) 
      VALUES ('$id_pkk','PKK','$data_id', '$data_kho' ,'$data_soluong', '$data_ghichu' ,'$id_cty', CURRENT_TIMESTAMP,'$data_slht')
      ");
    }
  }
}

if ($value == '14') {
  if ($_COOKIE['role'] == 1) {
    $edit = new db_query("UPDATE `kho-cho-xu-li` SET `kcxl_trangThai` = 14, `kcxl_nguoiDuyet` = 0, `kcxl_ngayDuyet` = '$date' WHERE `kcxl_id_ct` = $id_cty AND `kcxl_id` = $id_pkk");
  }
  if ($_COOKIE['role'] == 2) {
    $edit = new db_query("UPDATE `kho-cho-xu-li` SET `kcxl_trangThai` = 14, `kcxl_ngayDuyet` = '$date', `kcxl_nguoiDuyet` = '$user_id' WHERE `kcxl_id_ct` = $id_cty AND `kcxl_id` = $id_pkk");
  }
  $delete = new db_execute("DELETE FROM `so-luong-vat-tu` WHERE `slvt_idPhieu` = $id_pkk");
  for ($i = 0; $i < count(json_decode($vat_tu)); $i++) {
    $data_id = json_decode($vat_tu)[$i]->id;
    $data_soluong = json_decode($vat_tu)[$i]->sl;
    $data_ghichu = json_decode($vat_tu)[$i]->ghichu;
    $data_slht = json_decode($vat_tu)[$i]->slht;
    $data_kho = json_decode($vat_tu)[$i]->kho;
    $add_vattu = new db_execute("INSERT INTO `so-luong-vat-tu`(`slvt_idPhieu`, `slvt_maPhieu`, `slvt_maVatTuThietBi`, `slvt_maKho`, `slvt_soLuongKiemKe`, `slvt_ghiChu`,
    `slvt_id_ct`, `slvt_timeCurrentDay`, `slvt_so_luong_he_thong`) 
    VALUES ('$id_pkk','PKK','$data_id', '$data_kho' ,'$data_soluong', '$data_ghichu' ,'$id_cty', CURRENT_TIMESTAMP,'$data_slht')
    ");
  }
}

if (isset($add_vattu)) {
  echo "";
} else {
  echo "lỗi";
}
