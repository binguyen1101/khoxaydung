<?php
    include("config.php");

    $id_phieu = getValue('id_phieu','int','POST','');

    $kcxl_hinhThuc = $_POST['kcxl_hinhThuc'];
    $kcxl_nguoiTao = getValue('kcxl_nguoiTao','int','POST','');
    $kcxl_ngayTao = $_POST['kcxl_ngayTao'];
    $kcxl_phieuDieuChuyenKho = getValue('kcxl_phieuDieuChuyenKho','int','POST','');
    $kcxl_donHang = getValue('kcxl_donHang', 'int', 'POST', '');
    $kcxl_trangThai = getValue('kcxl_trangThai','int','POST','');
    $kcxl_nhaCungCap = $_POST['kcxl_nhaCungCap'];
    $kcxl_congTrinh = $_POST['kcxl_congTrinh'];
    $kcxl_phieuYeuCau = $_POST['kcxl_phieuYeuCau'];
    $kcxl_ngayYeuCauHoanThanh = $_POST['kcxl_ngayYeuCauHoanThanh'];
    $kcxl_nguoiThucHien = $_POST['kcxl_nguoiThucHien'];
    $kcxl_phongBanNguoiGiao = $_POST['kcxl_phongBanNguoiGiao'];
    $kcxl_nguoiNhan = $_POST['kcxl_nguoiNhan'];
    $kcxl_phongBanNguoiNhan = $_POST['kcxl_phongBanNguoiNhan'];
    $kcxl_khoXuat = getValue('kcxl_khoXuat','int','POST','');
    $kcxl_khoNhap = getValue('kcxl_khoNhap','int','POST','');
    $kcxl_ngayNhapKho = $_POST['kcxl_ngayNhapKho'];
    $kcxl_ghi_chu = $_POST['kcxl_ghi_chu'];
    $kcxl_ngayHoanThanh = $_POST['kcxl_ngayHoanThanh'];
    $listTable = $_POST['listTable'];

    $id_cty = getValue('id_cty','int','POST','');
    $date = date('Y-m-d H:i:s', time());

    // Biên bản giao hàng
    if ($kcxl_hinhThuc == 'NK1' && $kcxl_trangThai == 4) {
        $edit = new db_execute("UPDATE `kho-cho-xu-li` SET `kcxl_hinhThuc` = '$kcxl_hinhThuc', `kcxl_nguoiTao` = '$kcxl_nguoiTao', `kcxl_ngayTao` = '$kcxl_ngayTao',
        `kcxl_donHang` = '$kcxl_donHang', `kcxl_trangThai` = '$kcxl_trangThai', `kcxl_nhaCungCap` = '$kcxl_nhaCungCap', `kcxl_nguoiThucHien` = '$kcxl_nguoiThucHien', `kcxl_ngayHoanThanh` = '$kcxl_ngayHoanThanh',
        `kcxl_nguoiNhan` = '$kcxl_nguoiNhan', `kcxl_phongBanNguoiNhan` = '$kcxl_phongBanNguoiNhan', `kcxl_khoNhap` = '$kcxl_khoNhap', `kcxl_ngayNhapKho` = '$kcxl_ngayNhapKho',
        `kcxl_ghi_chu` = '$kcxl_ghi_chu', `kcxl_timeCurrentDay` = '$date' WHERE `kcxl_id` = $id_phieu AND `kcxl_id_ct` = $id_cty
        ");
    }else{
        $edit = new db_execute("UPDATE `kho-cho-xu-li` SET `kcxl_hinhThuc` = '$kcxl_hinhThuc', `kcxl_nguoiTao` = '$kcxl_nguoiTao', `kcxl_ngayTao` = '$kcxl_ngayTao',
        `kcxl_donHang` = '$kcxl_donHang', `kcxl_trangThai` = '$kcxl_trangThai', `kcxl_nhaCungCap` = '$kcxl_nhaCungCap', `kcxl_nguoiThucHien` = '$kcxl_nguoiThucHien',
        `kcxl_nguoiNhan` = '$kcxl_nguoiNhan', `kcxl_phongBanNguoiNhan` = '$kcxl_phongBanNguoiNhan', `kcxl_khoNhap` = '$kcxl_khoNhap', `kcxl_ngayNhapKho` = '$kcxl_ngayNhapKho',
        `kcxl_ghi_chu` = '$kcxl_ghi_chu', `kcxl_timeCurrentDay` = '$date' WHERE `kcxl_id` = $id_phieu AND `kcxl_id_ct` = $id_cty
        ");
    }

    // Điều chuyển
    if ($kcxl_hinhThuc == 'NK2' && $kcxl_trangThai == 4) {
        $edit_dc = new db_execute("UPDATE `kho-cho-xu-li` SET `kcxl_hinhThuc` = '$kcxl_hinhThuc', `kcxl_nguoiTao` = '$kcxl_nguoiTao', `kcxl_ngayTao` = '$kcxl_ngayTao',
        `kcxl_phieuDieuChuyenKho` = '$kcxl_phieuDieuChuyenKho', `kcxl_trangThai` = '$kcxl_trangThai', `kcxl_khoXuat` = '$kcxl_khoXuat', `kcxl_khoNhap` = '$kcxl_khoNhap', `kcxl_ngayHoanThanh` = '$kcxl_ngayHoanThanh',
        `kcxl_nguoiThucHien` = '$kcxl_nguoiThucHien', `kcxl_phongBanNguoiGiao` = '$kcxl_phongBanNguoiGiao', `kcxl_nguoiNhan` = '$kcxl_nguoiNhan', `kcxl_phongBanNguoiNhan` = '$kcxl_phongBanNguoiNhan',
        `kcxl_ngayYeuCauHoanThanh` = '$kcxl_ngayYeuCauHoanThanh', `kcxl_ngayNhapKho` = '$kcxl_ngayNhapKho', `kcxl_ghi_chu` = '$kcxl_ghi_chu', `kcxl_timeCurrentDay` = '$date' 
        WHERE `kcxl_id` = $id_phieu AND `kcxl_id_ct` = $id_cty");
    }else{
        $edit_dc = new db_execute("UPDATE `kho-cho-xu-li` SET `kcxl_hinhThuc` = '$kcxl_hinhThuc', `kcxl_nguoiTao` = '$kcxl_nguoiTao', `kcxl_ngayTao` = '$kcxl_ngayTao',
        `kcxl_phieuDieuChuyenKho` = '$kcxl_phieuDieuChuyenKho', `kcxl_trangThai` = '$kcxl_trangThai', `kcxl_khoXuat` = '$kcxl_khoXuat', `kcxl_khoNhap` = '$kcxl_khoNhap',
        `kcxl_nguoiThucHien` = '$kcxl_nguoiThucHien', `kcxl_phongBanNguoiGiao` = '$kcxl_phongBanNguoiGiao', `kcxl_nguoiNhan` = '$kcxl_nguoiNhan', `kcxl_phongBanNguoiNhan` = '$kcxl_phongBanNguoiNhan',
        `kcxl_ngayYeuCauHoanThanh` = '$kcxl_ngayYeuCauHoanThanh', `kcxl_ngayNhapKho` = '$kcxl_ngayNhapKho', `kcxl_ghi_chu` = '$kcxl_ghi_chu', `kcxl_timeCurrentDay` = '$date' 
        WHERE `kcxl_id` = $id_phieu AND `kcxl_id_ct` = $id_cty");
    }

    // Nhập trả lại từ thi công
    if ($kcxl_hinhThuc == 'NK3' && $kcxl_trangThai == 4) {
        $edit = new db_execute("UPDATE `kho-cho-xu-li` SET `kcxl_hinhThuc` = '$kcxl_hinhThuc', `kcxl_nguoiTao` = '$kcxl_nguoiTao', `kcxl_ngayTao` = '$kcxl_ngayTao',
        `kcxl_congTrinh` = '$kcxl_congTrinh', `kcxl_nguoiThucHien` = '$kcxl_nguoiThucHien', `kcxl_phongBanNguoiGiao` = '$kcxl_phongBanNguoiGiao', `kcxl_ngayHoanThanh` = '$kcxl_ngayHoanThanh',
        `kcxl_nguoiNhan` = '$kcxl_nguoiNhan', `kcxl_phongBanNguoiNhan` = '$kcxl_phongBanNguoiNhan', `kcxl_khoNhap` = '$kcxl_khoNhap', `kcxl_ngayNhapKho` = '$kcxl_ngayNhapKho',
        `kcxl_ghi_chu` = '$kcxl_ghi_chu', `kcxl_timeCurrentDay` = '$date' WHERE `kcxl_id` = $id_phieu AND `kcxl_id_ct` = $id_cty");
    }else{
        $edit = new db_execute("UPDATE `kho-cho-xu-li` SET `kcxl_hinhThuc` = '$kcxl_hinhThuc', `kcxl_nguoiTao` = '$kcxl_nguoiTao', `kcxl_ngayTao` = '$kcxl_ngayTao',
        `kcxl_congTrinh` = '$kcxl_congTrinh', `kcxl_nguoiThucHien` = '$kcxl_nguoiThucHien', `kcxl_phongBanNguoiGiao` = '$kcxl_phongBanNguoiGiao',
        `kcxl_nguoiNhan` = '$kcxl_nguoiNhan', `kcxl_phongBanNguoiNhan` = '$kcxl_phongBanNguoiNhan', `kcxl_khoNhap` = '$kcxl_khoNhap', `kcxl_ngayNhapKho` = '$kcxl_ngayNhapKho',
        `kcxl_ghi_chu` = '$kcxl_ghi_chu', `kcxl_timeCurrentDay` = '$date' WHERE `kcxl_id` = $id_phieu AND `kcxl_id_ct` = $id_cty");
    }

    // Yêu cầu vật tư
    if ($kcxl_hinhThuc == 'NK4' && $kcxl_trangThai == 4) {
        $edit = new db_execute("UPDATE `kho-cho-xu-li` SET `kcxl_hinhThuc` = '$kcxl_hinhThuc', `kcxl_nguoiTao` = '$kcxl_nguoiTao', `kcxl_ngayTao` = '$kcxl_ngayTao',
        `kcxl_phieuYeuCau` = '$kcxl_phieuYeuCau', `kcxl_nguoiThucHien` = '$kcxl_nguoiThucHien', `kcxl_phongBanNguoiGiao` = '$kcxl_phongBanNguoiGiao',`kcxl_ngayHoanThanh` = '$kcxl_ngayHoanThanh',
        `kcxl_nguoiNhan` = '$kcxl_nguoiNhan', `kcxl_phongBanNguoiNhan` = '$kcxl_phongBanNguoiNhan', `kcxl_khoNhap` = '$kcxl_khoNhap', `kcxl_ngayNhapKho` = '$kcxl_ngayNhapKho',
        `kcxl_ghi_chu` = '$kcxl_ghi_chu', `kcxl_timeCurrentDay` = '$date' WHERE `kcxl_id` = $id_phieu AND `kcxl_id_ct` = $id_cty");
    }else {
        $edit = new db_execute("UPDATE `kho-cho-xu-li` SET `kcxl_hinhThuc` = '$kcxl_hinhThuc', `kcxl_nguoiTao` = '$kcxl_nguoiTao', `kcxl_ngayTao` = '$kcxl_ngayTao',
        `kcxl_phieuYeuCau` = '$kcxl_phieuYeuCau', `kcxl_nguoiThucHien` = '$kcxl_nguoiThucHien', `kcxl_phongBanNguoiGiao` = '$kcxl_phongBanNguoiGiao',
        `kcxl_nguoiNhan` = '$kcxl_nguoiNhan', `kcxl_phongBanNguoiNhan` = '$kcxl_phongBanNguoiNhan', `kcxl_khoNhap` = '$kcxl_khoNhap', `kcxl_ngayNhapKho` = '$kcxl_ngayNhapKho',
        `kcxl_ghi_chu` = '$kcxl_ghi_chu', `kcxl_timeCurrentDay` = '$date' WHERE `kcxl_id` = $id_phieu AND `kcxl_id_ct` = $id_cty");
    }

    // Khác
    if ($kcxl_hinhThuc == 'NK5' && $kcxl_trangThai == 4) {
        $edit = new db_execute("UPDATE `kho-cho-xu-li` SET `kcxl_hinhThuc` = '$kcxl_hinhThuc', `kcxl_nguoiTao` = '$kcxl_nguoiTao', `kcxl_ngayTao` = '$kcxl_ngayTao', `kcxl_ngayHoanThanh` = '$kcxl_ngayHoanThanh',
        `kcxl_nguoiThucHien` = '$kcxl_nguoiThucHien', `kcxl_nguoiNhan` = '$kcxl_nguoiNhan', `kcxl_phongBanNguoiNhan` = '$kcxl_phongBanNguoiNhan', `kcxl_khoNhap` = '$kcxl_khoNhap', `kcxl_ngayNhapKho` = '$kcxl_ngayNhapKho',
        `kcxl_ghi_chu` = '$kcxl_ghi_chu', `kcxl_timeCurrentDay` = '$date' WHERE `kcxl_id` = $id_phieu AND `kcxl_id_ct` = $id_cty");
    }else{
        $edit = new db_execute("UPDATE `kho-cho-xu-li` SET `kcxl_hinhThuc` = '$kcxl_hinhThuc', `kcxl_nguoiTao` = '$kcxl_nguoiTao', `kcxl_ngayTao` = '$kcxl_ngayTao',
        `kcxl_nguoiThucHien` = '$kcxl_nguoiThucHien', `kcxl_nguoiNhan` = '$kcxl_nguoiNhan', `kcxl_phongBanNguoiNhan` = '$kcxl_phongBanNguoiNhan', `kcxl_khoNhap` = '$kcxl_khoNhap', `kcxl_ngayNhapKho` = '$kcxl_ngayNhapKho',
        `kcxl_ghi_chu` = '$kcxl_ghi_chu', `kcxl_timeCurrentDay` = '$date' WHERE `kcxl_id` = $id_phieu AND `kcxl_id_ct` = $id_cty");
    }

    if(isset($edit)){
        $delete = new db_execute("DELETE FROM `so-luong-vat-tu` WHERE `slvt_idPhieu` = $id_phieu");
        for($i = 0; $i < count(json_decode($listTable)); $i++){
            $data_id = json_decode($listTable)[$i]->id;
            $data_soluong = json_decode($listTable)[$i]->soluong;
            $edit_vattu = new db_execute ("INSERT INTO `so-luong-vat-tu`(`slvt_idPhieu`, `slvt_maPhieu`, `slvt_maVatTuThietBi`, `slvt_maKho`,
            `slvt_soLuongNhapKho`, `slvt_id_ct`, `slvt_timeCurrentDay`) 
            VALUES ('$id_phieu','PNK','$data_id','$kcxl_khoNhap','$data_soluong','$id_cty', CURRENT_TIMESTAMP)
            ");
        }
    }else if (isset($edit_dc)) {
        $delete = new db_execute("DELETE FROM `so-luong-vat-tu` WHERE `slvt_idPhieu` = $id_phieu");
        for($i = 0; $i < count(json_decode($listTable)); $i++){
            $data_id = json_decode($listTable)[$i]->id;
            $data_soluong = json_decode($listTable)[$i]->soluong;
            $edit_vattu = new db_execute ("INSERT INTO `so-luong-vat-tu`(`slvt_idPhieu`, `slvt_maPhieu`,`slvt_checkDieuChuyen` , `slvt_maVatTuThietBi`, `slvt_maKho`,
            `slvt_soLuongNhapKho`, `slvt_id_ct`, `slvt_timeCurrentDay`) 
            VALUES ('$id_phieu','PNK','$kcxl_phieuDieuChuyenKho' ,'$data_id','$kcxl_khoNhap','$data_soluong','$id_cty', CURRENT_TIMESTAMP)
            ");
        }
    }


?>
