<?
include("config.php");
$id_kho = $_POST['id_kho'];
// $id_kho = getValue('id_kho', 'int', 'POST', '');
$id_cty = getValue('id_cty', 'int', 'POST', '');
$trang_thai = getValue('trang_thai', 'int', 'POST', '');
$id_vt = getValue('id_vt', 'int', 'POST', '');

$oninput = "this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');";

$list_dsvt = new db_query("SELECT `dsvt_id`, `dsvt_name`, `hsx_name`, `dvt_name`, `xx_name`, `dsvt_soLuongTon`, `slvt_soLuongKiemKe`, `slvt_ghiChu`, `dsvt_kho`, `dsvt_donGia`
	FROM `danh-sach-vat-tu`
	LEFT JOIN `hang-san-xuat` on `dsvt_hangSanXuat` = `hsx_id` AND `hsx_check`= 1       
	LEFT JOIN `don-vi-tinh` on `dsvt_donViTinh` = `dvt_id` AND `dvt_check`= 1
	LEFT JOIN `xuat-xu` on `dsvt_xuatXu` = `xx_id`
	LEFT JOIN `so-luong-vat-tu` on  `dsvt_id` = `slvt_maVatTuThietBi`
	WHERE `dsvt_check` = 1 AND `dsvt_id` = $id_vt AND `dsvt_id_ct` = $id_cty
");

$dsvt = new db_query("SELECT `dsvt_id`, `dsvt_name`, `dsvt_kho` FROM `danh-sach-vat-tu` WHERE `dsvt_id_ct` = $id_cty");

$responsive_sl = [];
$j = 0;
while ($item = mysql_fetch_assoc($dsvt->result)) {
	$check_kho = explode(',', $item['dsvt_kho']);
	if (in_array($id_kho, $check_kho)) {
		$info_k['dsvt_id'] = $item['dsvt_id'];
		$info_k['dsvt_name'] = $item['dsvt_name'];
		$responsive_sl[$j] = $info_k;
		$j++;
	}
}
$responsive = [];
while (($selected = mysql_fetch_assoc($list_dsvt->result))) {
	$check_kho = explode(',', $selected['dsvt_kho']);
	if (in_array($id_kho, $check_kho)) {
		$sl_ht = json_decode($selected['dsvt_soLuongTon']);
		$item['id'] = $selected['dsvt_id'];
		$item['ten_vt'] = $selected['dsvt_name'];
		$item['ten_hang'] = $selected['hsx_name'];
		$item['ten_dvt'] = $selected['dvt_name'];
		$item['ten_xx'] = $selected['xx_name'];
		$item['don_gia'] = $selected['dsvt_donGia'];
		$item['sl_ht'] = $sl_ht->$id_kho;
		$item['sl_kk'] = $selected['slvt_soLuongKiemKe'];
		$responsive[$selected['dsvt_id']] = $item;
	}
}
$data = json_encode($responsive);

if ($trang_thai == 1 || $trang_thai == 8) {
	if ($responsive != "") {
		if ($id_vt == '') {
?>
			<tr class="color_grey font_s14 line_h17 font_w400 table_3 delete_3">
				<td onclick="deleteRow(this)"><img class="cursor_p delete_3" src="../images/del_row.png" alt="" style="margin: 0; width: 16px; height:16px;"></td>
				<td class="ma_vat_tu_3 delete_3" style="background-color: #EEEEEE;"></td>
				<td class="color_blue font_w500 delete_3" style="text-align: left;">
					<select class="select_tb color_grey3 font_s14 line_h17 font_w400 select_tb_1 delete_3" style="width: 100%">
						<option value=""></option>
						<? foreach ($responsive_sl as $value) { ?>
							<option value="<?= $value['dsvt_id'] ?>" <?= ($value['dsvt_id'] == $id_vt) ? "selected" : "" ?>><?= $value['dsvt_name'] ?></option>
						<? } ?>
					</select>
				</td>
				<td style="background-color: #EEEEEE;" class="don_vi_tinh_3 delete_3"></td>
				<td style="text-align: left; background-color: #EEEEEE;" class="hang_san_xuat_3 delete_3"></td>
				<td style="background-color: #EEEEEE;" class="xuat_xu_3 delete_3"></td>
				<td style="text-align: right; background-color: #EEEEEE;" class="sltht"></td>
			</tr>
		<? }
	}

	if ($id_vt != '') {
		?>
		<td onclick="deleteRow(this)"><img class="cursor_p delete_3" src="../images/del_row.png" alt="" style="margin: 0; width: 16px; height:16px;"></td>
		<td class="ma_vat_tu_3 delete_3" style="background-color: #EEEEEE;">VT - <?= $responsive[$id_vt]['id'] ?></td>
		<td class="color_blue font_w500 delete_3" style="text-align: left;">
			<select class="select_tb color_grey3 font_s14 line_h17 font_w400 select_tb_1 delete_3" style="width: 100%">
				<option value=""></option>
				<? foreach ($responsive_sl as $value) { ?>
					<option value="<?= $value['dsvt_id'] ?>" <?= ($value['dsvt_id'] == $id_vt) ? "selected" : "" ?>><?= $value['dsvt_name'] ?></option>
				<? } ?>
			</select>
		</td>
		<td style="background-color: #EEEEEE;" class="don_vi_tinh_3 delete_3"> <?= $responsive[$id_vt]['ten_dvt'] ?></td>
		<td style="text-align: left; background-color: #EEEEEE;" class="hang_san_xuat_3 delete_3"><?= $responsive[$id_vt]['ten_hang'] ?></td>
		<td style="background-color: #EEEEEE;" class="xuat_xu_3 delete_3"><?= $responsive[$id_vt]['ten_xx'] ?></td>
		<td style="text-align: right; background-color: #EEEEEE;" class="sltht"><?= $responsive[$id_vt]['sl_ht'] ?></td>
		<? } else if ($responsive == "") {
		echo 'loi';
	}
}
if ($trang_thai == 2 || $trang_thai == 9 || $trang_thai == 10 || $trang_thai == 11 || $trang_thai == 12 || $trang_thai == 14) {
	if ($responsive != "") {
		if ($id_vt == '') {
		?>
			<tr class="color_grey font_s14 line_h17 font_w400 table_3 delete_3">
				<td onclick="deleteRow(this)"><img class="cursor_p delete_3" src="../images/del_row.png" alt="" style="margin: 0; width: 16px; height:16px;"></td>
				<td class="ma_vat_tu_3 delete_3" style="background-color: #EEEEEE;"></td>
				<td class="color_blue font_w500 delete_3" style="text-align: left;">
					<select class="select_tb color_grey3 font_s14 line_h17 font_w400 select_tb_3 delete_3" style="width: 100%">
						<option value=""></option>
						<? foreach ($responsive_sl as $value) { ?>
							<option value="<?= $value['dsvt_id'] ?>" <?= ($value['dsvt_id'] == $id_vt) ? "selected" : "" ?>><?= $value['dsvt_name'] ?></option>
						<? } ?>
					</select>
				</td>
				<td style="background-color: #EEEEEE;" class="don_vi_tinh_3 delete_3"></td>
				<td style="text-align: left; background-color: #EEEEEE;" class="hang_san_xuat_3 delete_3"></td>
				<td style="background-color: #EEEEEE;" class="xuat_xu_3 delete_3"></td>
				<td style="text-align: right; background-color: #EEEEEE;"></td>
				<td><input type="text" class="nhap_sl_3 delete_3 color_grey font_s14 line_h17 font_w400" placeholder="Nhập số lượng"></td>
				<td style="background-color: #EEEEEE;"></td>
				<td style="background-color: #EEEEEE;"></td>
				<td><input type="text" style="text-align: left;" class="nhap_ghi_chu color_grey font_s14 line_h17 font_w400" placeholder="Nhập ghi chú"></td>
			</tr>
		<? }
	}
	if ($id_vt != '') {
		?>
		<td onclick="deleteRow(this)"><img class="cursor_p delete_3" src="../images/del_row.png" alt="" style="margin: 0; width: 16px; height:16px;"></td>
		<td class="ma_vat_tu_3 delete_3" style="background-color: #EEEEEE;">VT - <?= $responsive[$id_vt]['id'] ?> </td>
		<td class="color_blue font_w500 delete_3" style="text-align: left;">
			<select class="select_tb color_grey3 font_s14 line_h17 font_w400 select_tb_3 delete_3" style="width: 100%">
				<option value=""></option>
				<? foreach ($responsive_sl as $value) { ?>
							<option value="<?= $value['dsvt_id'] ?>" <?= ($value['dsvt_id'] == $id_vt) ? "selected" : "" ?>><?= $value['dsvt_name'] ?></option>
						<? } ?>
			</select>
		</td>
		<td style="background-color: #EEEEEE;" class="don_vi_tinh_3 delete_3"> <?= $responsive[$id_vt]['ten_dvt'] ?></td>
		<td style="text-align: left; background-color: #EEEEEE;" class="hang_san_xuat_3 delete_3"><?= $responsive[$id_vt]['ten_hang'] ?></td>
		<td style="background-color: #EEEEEE;" class="xuat_xu_3 delete_3"><?= $responsive[$id_vt]['ten_xx'] ?></td>
		<td style="text-align: right; background-color: #EEEEEE;" class="sl_hethong"><?= $responsive[$id_vt]['sl_ht'] ?></td>
		<td><input type="text" class="nhap_sl_3 delete_3 color_grey font_s14 line_h17 font_w400" oninput="<?= $oninput ?>" onkeyup="thong_ke(this)" placeholder="Nhập số lượng" ></td>
		<td style="background-color: #EEEEEE;" class="sl_thua"></td>		
		<td style="background-color: #EEEEEE;" class="sl_thieu"></td>
		<td><input type="text" style="text-align: left;" class="nhap_ghi_chu color_grey font_s14 line_h17 font_w400" placeholder="Nhập ghi chú"></td>
<? } else if ($responsive == "") {
		echo 'lỗi';
	}
}
?>
<script>
	$('.select_tb').select2({
		placeholder: "Chọn vật tư",
		templateSelection: formatState
	});
	change_value();
</script>