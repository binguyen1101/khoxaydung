<?php

// Trạng thái phiếu
    function trang_thai($row){
        $trang_thai = $row; 
        switch($trang_thai) {
            case 1:
                $trang_thai = 'Chờ duyệt';
                echo $trang_thai;
                break;
            case 2:
                $trang_thai = 'Từ chối';
                echo $trang_thai;
                break;
            case 3:
                $trang_thai = 'Đã duyệt - Chờ nhập kho';
                echo $trang_thai;
                break;
            case 4:
                $trang_thai = 'Hoàn thành';
                echo $trang_thai;
                break;
            case 5:
                $trang_thai = 'Đã duyệt - Chờ xuất kho';
                echo $trang_thai;
                break;
            case 6:
                $trang_thai = 'Hoàn thành';
                echo $trang_thai;
                break;
            case 7:
                $trang_thai = 'Đã duyệt';
                echo $trang_thai;
                break;
            case 8:
                $trang_thai = 'Chờ kiểm kê';
                echo $trang_thai;
                break;
            case 9:
                $trang_thai = 'Kiểm kê chờ duyệt';
                echo $trang_thai;
                break;
            case 10:
                $trang_thai = 'Từ chối duyệt kiểm kê';
                echo $trang_thai;
                break;
            case 11:
                $trang_thai = 'Duyệt kiểm kê';
                echo $trang_thai;
                break;
            case 12:
                $trang_thai = 'Đã cập nhật kho';
                echo $trang_thai;
                break;
            case 14:
                $trang_thai = 'Chờ kiểm kê';
                echo $trang_thai;
                break;
            default:
                $trang_thai = $trang_thai;
                echo $trang_thai;
                break;
        }
    }

// Màu trạng thái
    function trang_thai_color($row){
        $trang_thai_color = $row; 
        switch($trang_thai_color) {
            case 1:
                $trang_thai_color = 'color_org';
                echo $trang_thai_color;
                break;
            case 2:
                $trang_thai_color = 'color_red';
                echo $trang_thai_color;
                break;
            case 3:
                $trang_thai_color = 'color_blue2';
                echo $trang_thai_color;
                break;
            case 4:
                $trang_thai_color = 'color_green';
                echo $trang_thai_color;
                break;
            case 5:
                $trang_thai_color = 'color_blue2';
                echo $trang_thai_color;
                break;
            case 6:
                $trang_thai_color = 'color_green';
                echo $trang_thai_color;
                break;
            case 7:
                $trang_thai_color = 'color_green';
                echo $trang_thai_color;
                break;
            case 8:
                $trang_thai_color = 'color_org';
                echo $trang_thai_color;
                break;
            case 14:
                $trang_thai_color = 'color_org';
                echo $trang_thai_color;
                break;
            case 9:
                $trang_thai_color = 'color_blue2';
                echo $trang_thai_color;
                break;
            case 10:
                $trang_thai_color = 'color_red';
                echo $trang_thai_color;
                break;
            case 11:
                $trang_thai_color = 'color_green_tb';
                echo $trang_thai_color;
                break;
            case 12:
                $trang_thai_color = 'color_green_kk';
                echo $trang_thai_color;
                break;
            default:
                $trang_thai_color = $trang_thai_color;
                echo $trang_thai_color;
                break;
        }
    }
// Số phiếu
    function phieu($id_phieu, $so_phieu){
        if(floor($id_phieu / 10) == 0){
            echo $so_phieu.'-000'.$id_phieu;
        }elseif(floor($id_phieu / 100) == 0){
            echo $so_phieu.'-00'.$id_phieu;
        }elseif(floor($id_phieu / 1000) == 0){
            echo $so_phieu.'-0'.$id_phieu;
        }else{
            echo $so_phieu.'-'.$id_phieu;
        }
    }

// Loại phiếu 
    function loai_phieu($loai_phieu){
        switch($loai_phieu) {
            case 'PNK':
                $loai_phieu = 'Phiếu nhập kho';
                break;
            case 'PXK':
                $loai_phieu = 'Phiếu xuất kho';
                break;
            case 'ĐCK':
                $loai_phieu = 'Phiếu điều chuyển';
                break;
            case 'PKK':
                $loai_phieu = 'Phiếu kiểm kê	';
                break;
            default:
                $loai_phieu = 'NO NAME';
                break;
        }
        return $loai_phieu;
    }

// Hình thức phiếu nhập
    function hinh_thuc_nhap($hinh_thuc_nhap){
        switch($hinh_thuc_nhap) {
            case 'NK1':
                $hinh_thuc_nhap = 'Nhập theo biên bản giao hàng';
                break;
            case 'NK2':
                $hinh_thuc_nhap = 'Nhập điều chuyển';
                break;
            case 'NK3':
                $hinh_thuc_nhap = 'Nhập trả lại từ thi công';
                break;
            case 'NK4':
                $hinh_thuc_nhap = 'Nhập theo yêu cầu vật tư';
                break;
            default:
                $hinh_thuc_nhap = 'Nhập khác';
                break;
        }
        return $hinh_thuc_nhap;
    }

// Hình thức phiếu xuất
    function hinh_thuc_xuat($hinh_thuc_xuat){
        switch($hinh_thuc_xuat) {
            case 'XK1':
                $hinh_thuc_xuat = 'Xuất thi công';
                break;
            case 'XK2':
                $hinh_thuc_xuat = 'Xuất điều chuyển';
                break;
            case 'XK3':
                $hinh_thuc_xuat = 'Xuất theo yêu cầu vật tư';
                break;
            case 'XK4':
                $hinh_thuc_xuat = 'Xuất theo đơn hàng bán vật tư';
                break;
            default:
                $hinh_thuc_xuat = 'Xuất khác';
                break;
        }
        return $hinh_thuc_xuat;
    }

?>