<?php 

if (isset($_COOKIE['acc_token']) && isset($_COOKIE['rf_token']) && isset($_COOKIE['role'])) {
    header('Location: /tong-quan.html');
    exit;
}

?>
<!DOCTYPE html>
<html lang="vi">

<head>
    <title>Phần mềm Quản lý kho xây dựng miễn phí, tốt nhất</title>
    <meta charset="UTF-8">
    <meta name="robots" content="noindex,nofollow" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="canonical" href="https://phanmemquanlykhoxaydung.timviec365.vn/" />
    <meta name="Keywords" content="phần mềm quản lý kho xây dựng, phần mềm quản lý kho vật tư xây dựng"/>
    <meta name="google-site-verification" content="tkR0DL2EWeg8OJfQypncyEWVoR3Mvl-Vbk4yl-8q1sQ" />
    <meta property="og:locale" content="vi_VN" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Phần mềm Quản lý kho xây dựng miễn phí, tốt nhất" />
    <meta property="og:description" content="Phần mềm quản lý kho xây dựng miễn phí, tốt nhất. Tham khảo và sử dụng ngay phần mềm quản lý kho vật tư xây dựng 365" />
    <meta property="og:image" content="https://phanmemquanlykhoxaydung.timviec365.vn/images/bg_header_home.png" />
    <meta name="twitter:card" content="summary" />
    <meta name="twitter:description" content="Phần mềm quản lý kho xây dựng miễn phí, tốt nhất. Tham khảo và sử dụng ngay phần mềm quản lý kho vật tư xây dựng 365" />
    <meta name="twitter:title" content="Phần mềm Quản lý kho xây dựng miễn phí, tốt nhất" />


    <link href="https://timviec365.vn/favicon.ico" rel="shortcut icon">
    <link rel="preload" as="style" href="../css/style.css?v=<?= $ver ?>">
    <link rel="stylesheet" media="all" href="../css/style.css?v=<?= $ver ?>" media="all"
        onload="if (media != 'all')media='all'">
    <link rel="preload" as="style" href="../css/style_n.css?v=<?= $ver ?>">
    <link rel="stylesheet" media="all" href="../css/style_n.css?v=<?= $ver ?>" media="all"
        onload="if (media != 'all')media='all'">
</head>

<body>
    <div class="home_page" id="home_page">
        <div class="main_home">
            <div class="box_home">
                <div class="header_home">
                    <?php include("../includes/inc_header.php"); ?>
                    <div class="header_body">
                        <div class="box_header_body position_r">
                            <img src="../images/bg_header_home.png" alt="" class="picture_header_home">
                            <div class="text_slogen position_a">
                                <h1 class="font_s40 line_h47 color_white font_w700">Phần mềm Quản lý kho vật tư xây dựng 365</h1>
                                <p class="slogen_content font_s20 line_h27 color_white font_w400 text_a_j">Quản lý tập
                                    trung vật
                                    tư
                                    theo công trình,
                                    theo dõi chi tiết vật tư ở từng kho. Quản lý quá trình xuất nhập kho, cùng với các
                                    tính
                                    năng điều chuyển, kiểm kê, giúp các hoạt động nhập xuất vật tư được diễn ra suôn sẻ,
                                    dễ
                                    dàng kiểm soát, hạn chế thất thoát vật tư công trình.</p>
                                <button class="register_now font_s18 line_h21 color_white font_w500 cursor_p">
                                    <a rel='nofollow' href="https://quanlychung.timviec365.vn/lua-chon-dang-ky.html"></a>
                                    Đăng ký sử
                                        dụng
                                        ngay</button>
                            </div>
                            <div class="chat_w_us d_flex align_e flex_column">
                                <picture>
                                    <img src="../images/chat_skype.png" alt="skype">
                                </picture>
                                <p class="tit_chat_us font_s16 line_h19 color_white font_w500">Hãy chat với chúng tôi
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="salient_features">
                    <div class="box_features">
                        <h2 class="tit_box_home color_grey font_s28 line_h33 font_w700 text_a_c">Tính năng nổi bật của
                            phần
                            mềm Quản lý kho vật tư xây dựng 365</h2>
                        <div class="list_features d_flex space_b flex_w">
                            <div class="item_features">
                                <div class="header_item_features d_flex align_c">
                                    <h3 class="tit_header_item font_s18 line_h26 font_w700">QUẢN LÝ DANH SÁCH VẬT TƯ
                                        THIẾT BỊ
                                    </h3>
                                    <img src="../images/icon_vattu.png" alt="QUẢN LÝ DANH SÁCH VẬT TƯ THIẾT BỊ" class="icon_header_item">
                                </div>
                                <div class="body_item_features">
                                    <p class="color_grey font_s16 line_h19 font_w400">Quản lý danh sách vật tư thiết bị,
                                        thông tin của vật tư (thông số kỹ thuật, thuộc tính,...), nhóm vật tư, hãng sản
                                        xuất, đơn vị tính.</p>
                                </div>
                            </div>
                            <div class="item_features">
                                <div class="header_item_features d_flex align_c">
                                    <h3 class="tit_header_item font_s18 line_h26 font_w700">QUẢN LÝ THÔNG TIN TỒN KHO</h3>
                                    <img src="../images/icon_tonkho.png" alt="QUẢN LÝ THÔNG TIN TỒN KHO" class="icon_header_item">
                                </div>
                                <div class="body_item_features">
                                    <p class="color_grey font_s16 line_h19 font_w400">Thống kê số lượng vật tư thiết bị
                                        tồn
                                        kho, thống kê số lượng tồn kho dưới hạn mức. Thiết lập tồn kho cho vật tư thiết
                                        bị
                                        theo từng kho.</p>
                                </div>
                            </div>
                            <div class="item_features">
                                <div class="header_item_features d_flex align_c">
                                    <h3 class="tit_header_item font_s18 line_h26 font_w700">QUẢN LÝ THÔNG TIN NHẬP KHO
                                    </h3>
                                    <img src="../images/icon_nhapkho.png" alt="QUẢN LÝ THÔNG TIN NHẬP KHO" class="icon_header_item">
                                </div>
                                <div class="body_item_features">
                                    <p class="color_grey font_s16 line_h19 font_w400">Quản lý nhập kho theo biên bản
                                        giao
                                        hàng, nhập kho điều chuyển, nhập vật tư trả lại từ thi công, nhập theo yêu cầu
                                        vật
                                        tư, nhập khác.</p>
                                </div>
                            </div>
                            <div class="item_features">
                                <div class="header_item_features d_flex align_c">
                                    <h3 class="tit_header_item font_s18 line_h26 font_w700">QUẢN LÝ THÔNG TIN XUẤT KHO
                                    </h3>
                                    <img src="../images/icon_xuatkho.png" alt="QUẢN LÝ THÔNG TIN XUẤT KHO" class="icon_header_item">
                                </div>
                                <div class="body_item_features">
                                    <p class="color_grey font_s16 line_h19 font_w400">Quản lý xuất kho thi công, xuất
                                        kho
                                        theo yêu cầu vật tư, xuất kho điều chuyển, xuất kho sử dụng, xuất theo đơn hàng
                                        bán
                                        vật tư, xuất khác.</p>
                                </div>
                            </div>
                            <div class="item_features">
                                <div class="header_item_features d_flex align_c">
                                    <h3 class="tit_header_item font_s18 line_h26 font_w700">QUẢN LÝ THÔNG TIN KIỂM KÊ</h3>
                                    <img src="../images/icon_kiemke.png" alt="QUẢN LÝ THÔNG TIN KIỂM KÊ" class="icon_header_item">
                                </div>
                                <div class="body_item_features">
                                    <p class="color_grey font_s16 line_h19 font_w400">Quản lý kiểm kê theo nhóm vật tư
                                        hoặc
                                        đích danh vật tư. Cập nhật tồn kho theo biên bản kiểm kê vật tư.</p>
                                </div>
                            </div>
                            <div class="item_features">
                                <div class="header_item_features d_flex align_c">
                                    <h3 class="tit_header_item font_s18 line_h26 font_w700">BÁO CÁO THỐNG KÊ ĐẦY ĐỦ CHI
                                        TIẾT
                                    </h3>
                                    <img src="../images/icon_thongke.png" alt="BÁO CÁO THỐNG KÊ ĐẦY ĐỦ CHI TIẾT" class="icon_header_item">
                                </div>
                                <div class="body_item_features">
                                    <p class="color_grey font_s16 line_h19 font_w400">Báo cáo thống kê chi tiết tồn kho,
                                        nhập kho, xuất kho của doanh nghiệp.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="manage_supplies365">
                    <div class="box_supplies365">
                        <h2 class="tit_box_home color_grey font_s28 line_h33 font_w700 text_a_c">Quản lý kho vật tư xây
                            dựng
                            365 giải pháp hàng đầu cho doanh nghiệp</h2>
                        <div class="list_supplies365">
                            <div class="item_supplies365 d_flex space_b align_c">
                                <picture class="picture_item_supplies365">
                                    <img src="../images/img_ql_ds.png" alt="Quản lý danh sách vật tư thiết bị">
                                </picture>
                                <div class="content_item_supplies365">
                                    <div class="tit_content_item">
                                        <h3 class="color_blue font_s22 line_h26 font_w700">Quản lý danh sách vật tư thiết
                                            bị
                                        </h3>
                                    </div>
                                    <div class="body_content_item">
                                        <div class="content_manage d_flex">
                                            <img src="../images/check_green.png" alt="lợi ích 1" class="check_green">
                                            <p class="color_grey font_s16 line_h19 font_w400">Quản lý danh sách vật tư
                                                thiết
                                                bị giúp tìm kiếm và quản lý thông tin
                                                vật
                                                tư
                                                thiết bị dễ dàng nhanh chóng.</p>
                                        </div>
                                        <div class="content_manage d_flex">
                                            <img src="../images/check_green.png" alt="lợi ích 2" class="check_green">
                                            <p class="color_grey font_s16 line_h19 font_w400">Quản lý thông số kỹ thuật,
                                                các
                                                thuộc tính,... của vật tư thiết bị.
                                            </p>
                                        </div>
                                        <div class="content_manage d_flex">
                                            <img src="../images/check_green.png" alt="lợi ích 3" class="check_green">
                                            <p class="color_grey font_s16 line_h19 font_w400">Quản lý hãng sản xuất,
                                                nhóm
                                                các vật tư thiết bị, đơn vị tính.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item_supplies365 d_flex flex_row-reverse space_b align_c">
                                <picture class="picture_item_supplies365">
                                    <img src="../images/img_ql_tk.png" alt="Quản lý tồn kho">
                                </picture>
                                <div class="content_item_supplies365">
                                    <div class="tit_content_item">
                                        <h3 class="color_blue font_s22 line_h26 font_w700">Quản lý tồn kho</h3>
                                    </div>
                                    <div class="body_content_item">
                                        <div class="content_manage d_flex">
                                            <img src="../images/check_green.png" alt="lợi ích 1" class="check_green">
                                            <p class="color_grey font_s16 line_h19 font_w400">Quản lý danh sách kho.
                                                Quản lý
                                                thông tin kho, địa chỉ kho, nhân viên quản lý kho.</p>
                                        </div>
                                        <div class="content_manage d_flex">
                                            <img src="../images/check_green.png" alt="lợi ích 2" class="check_green">
                                            <p class="color_grey font_s16 line_h19 font_w400">Thống kê số lượng vật tư
                                                thiết
                                                bị tồn kho.
                                            </p>
                                        </div>
                                        <div class="content_manage d_flex">
                                            <img src="../images/check_green.png" alt="lợi ích 3" class="check_green">
                                            <p class="color_grey font_s16 line_h19 font_w400">Thống kê số lượng vật tư
                                                thiết
                                                bị tồn kho dưới hạn mức.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item_supplies365 d_flex space_b align_c">
                                <picture class="picture_item_supplies365">
                                    <img src="../images/img_ql_nk.png" alt="Quản lý nhập kho">
                                </picture>
                                <div class="content_item_supplies365">
                                    <div class="tit_content_item">
                                        <h3 class="color_blue font_s22 line_h26 font_w700">Quản lý nhập kho
                                        </h3>
                                    </div>
                                    <div class="body_content_item">
                                        <div class="content_manage d_flex">
                                            <img src="../images/check_green.png" alt="lợi ích 1" class="check_green">
                                            <p class="color_grey font_s16 line_h19 font_w400">Quản lý phiếu nhập kho
                                                giúp
                                                doanh nghiệp nắm bắt được tình hình nhập kho của vật tư thiết bị.</p>
                                        </div>
                                        <div class="content_manage d_flex">
                                            <img src="../images/check_green.png" alt="lợi ích 2" class="check_green">
                                            <p class="color_grey font_s16 line_h19 font_w400">Nhập kho theo đơn hàng,
                                                nhập
                                                kho theo phiếu điều chuyển, nhập kho trả lại từ thi công, nhập kho theo
                                                yêu
                                                cầu vật tư, nhập kho khác.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item_supplies365 d_flex flex_row-reverse space_b align_c">
                                <picture class="picture_item_supplies365">
                                    <img src="../images/img_ql_xk.png" alt="Quản lý xuất kho">
                                </picture>
                                <div class="content_item_supplies365">
                                    <div class="tit_content_item">
                                        <h3 class="color_blue font_s22 line_h26 font_w700">Quản lý xuất kho</h3>
                                    </div>
                                    <div class="body_content_item">
                                        <div class="content_manage d_flex">
                                            <img src="../images/check_green.png" alt="lợi ích 1" class="check_green">
                                            <p class="color_grey font_s16 line_h19 font_w400">Quản lý phiếu xuất kho
                                                giúp
                                                doanh nghiệp theo dõi được tình hình xuất kho của vật tư thiết bị một
                                                cách
                                                dễ dàng.</p>
                                        </div>
                                        <div class="content_manage d_flex">
                                            <img src="../images/check_green.png" alt="lợi ích 2" class="check_green">
                                            <p class="color_grey font_s16 line_h19 font_w400">Xuất kho thi công, xuất
                                                kho
                                                theo điều chuyển, xuất kho theo yêu cầu cung ứng vật tư, xuất kho theo
                                                đơn
                                                hàng bán vật tư, xuất kho khác.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item_supplies365 d_flex space_b align_c">
                                <picture class="picture_item_supplies365">
                                    <img src="../images/kk.png" alt="Kiểm kê">
                                </picture>
                                <div class="content_item_supplies365">
                                    <div class="tit_content_item">
                                        <h3 class="color_blue font_s22 line_h26 font_w700">Kiểm kê
                                        </h3>
                                    </div>
                                    <div class="body_content_item">
                                        <div class="content_manage d_flex">
                                            <img src="../images/check_green.png" alt="lợi ích 1" class="check_green">
                                            <p class="color_grey font_s16 line_h19 font_w400">Hỗ trợ doanh nghiệp kiểm
                                                kê
                                                ngay trên hệ thống một cách dễ dàng, giúp kiểm soát tồn kho của vật tư,
                                                tránh thất thoát vật tư.</p>
                                        </div>
                                        <div class="content_manage d_flex">
                                            <img src="../images/check_green.png" alt="lợi ích 2" class="check_green">
                                            <p class="color_grey font_s16 line_h19 font_w400">Kiểm kê theo nhóm hàng hóa
                                                hoặc đích danh hàng hóa.
                                            </p>
                                        </div>
                                        <div class="content_manage d_flex">
                                            <img src="../images/check_green.png" alt="lợi ích 3" class="check_green">
                                            <p class="color_grey font_s16 line_h19 font_w400">Tự động cập nhật tồn kho
                                                và
                                                các giao dịch điều chỉnh tồn kho.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item_supplies365 d_flex flex_row-reverse space_b align_c">
                                <picture class="picture_item_supplies365">
                                    <img src="../images/img_ql_xk.png" alt="Báo cáo">
                                </picture>
                                <div class="content_item_supplies365">
                                    <div class="tit_content_item">
                                        <h3 class="color_blue font_s22 line_h26 font_w700">Báo cáo</h3>
                                    </div>
                                    <div class="body_content_item">
                                        <div class="content_manage d_flex">
                                            <img src="../images/check_green.png" alt="lợi ích 1" class="check_green">
                                            <p class="color_grey font_s16 line_h19 font_w400">Báo cáo tổng hợp giao dịch
                                                nhập kho chi tiết theo từng kho của doanh nghiệp, giúp doanh nghiệp nắm
                                                bắt
                                                được quá trình nhập kho.</p>
                                        </div>
                                        <div class="content_manage d_flex">
                                            <img src="../images/check_green.png" alt="lợi ích 2" class="check_green">
                                            <p class="color_grey font_s16 line_h19 font_w400">Báo cáo tổng hợp giao dịch
                                                xuất kho chi tiết theo từng kho của doanh nghiệp, giúp doanh nghiệp nắm
                                                bắt
                                                được quá trình xuất kho.
                                            </p>
                                        </div>
                                        <div class="content_manage d_flex">
                                            <img src="../images/check_green.png" alt="lợi ích 3" class="check_green">
                                            <p class="color_grey font_s16 line_h19 font_w400">Báo cáo tồn kho của các
                                                công
                                                trình và nội bộ.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="outstand_of_digital">
                    <div class="box_outstand">
                        <h2 class="tit_box_home color_grey font_s28 line_h33 font_w700 text_a_c">Ưu điểm vượt trội của hệ
                            sinh thái Chuyển đổi số 365</h2>
                        <div class="list_outstand d_flex space_b">
                            <div class="item_outstand">
                                <div class="content_item_outstand d_flex flex_column align_c">
                                    <img src="../images/icon_atvbm.png" alt="An toàn và bảo mật" class="icon_outstands">
                                    <h3 class="tit_outstand color_grey font_s18 line_h21 font_w700 text_a_c">An toàn và
                                        bảo
                                        mật</h3>
                                    <p class="color_grey font_s16 line_h19 font_w400">An toàn, bảo mật tuyệt đối, dữ
                                        liệu
                                        được lưu trữ theo mô hình điện thoán đám
                                        mây.</p>
                                </div>
                                <div class="footer_item_outstand position_r">
                                    <p class="see_details color_blue font_s15 line_h18 font_w400 text_a_c">Xem chi tiết
                                    </p>
                                    <span class="arrow_right position_a"></span>
                                </div>
                            </div>
                            <div class="item_outstand">
                                <div class="content_item_outstand d_flex flex_column align_c">
                                    <img src="../images/icon_nentang.png" alt="Một nền tảng duy nhất" class="icon_outstands">
                                    <h3 class="tit_outstand color_grey font_s18 line_h21 font_w700 text_a_c">Một nền tảng
                                        duy
                                        nhất</h3>
                                    <p class="color_grey font_s16 line_h19 font_w400">Tích hợp tất cả các ứng dụng doanh
                                        nghiệp của bạn đang cần trên một nền tảng duy nhất.</p>
                                </div>
                                <div class="footer_item_outstand position_r">
                                    <p class="see_details color_blue font_s15 line_h18 font_w400 text_a_c">Xem chi tiết
                                    </p>
                                    <span class="arrow_right position_a"></span>
                                </div>
                            </div>
                            <div class="item_outstand">
                                <div class="content_item_outstand d_flex flex_column align_c">
                                    <img src="../images/icon_cnai.png" alt="Ứng dụng công nghệ AI" class="icon_outstands">
                                    <h3 class="tit_outstand color_grey font_s18 line_h21 font_w700 text_a_c">Ứng dụng
                                        công
                                        nghệ AI</h3>
                                    <p class="color_grey font_s16 line_h19 font_w400">Ứng dụng công nghệ AI tự nhận
                                        thức.
                                        Phân tích hành vi người dùng giải quyết toàn diện bài các toán đối với doanh
                                        nghiệp
                                        cụ thể.</p>
                                </div>
                                <div class="footer_item_outstand position_r">
                                    <p class="see_details color_blue font_s15 line_h18 font_w400 text_a_c">Xem chi tiết
                                    </p>
                                    <span class="arrow_right position_a"></span>
                                </div>
                            </div>
                            <div class="item_outstand">
                                <div class="content_item_outstand d_flex flex_column align_c">
                                    <img src="../images/icon_gp.png" alt="Giải pháp số 1 Việt Nam" class="icon_outstands">
                                    <h3 class="tit_outstand color_grey font_s18 line_h21 font_w700 text_a_c">Giải pháp số
                                        1
                                        Việt Nam</h3>
                                    <p class="color_grey font_s16 line_h19 font_w400">Luôn đồng hành và hỗ trợ 24/7. Phù
                                        hợp
                                        với tất cả các tập đoàn xuyên quốc gia đến những công ty SME.</p>
                                </div>
                                <div class="footer_item_outstand position_r">
                                    <p class="see_details color_blue font_s15 line_h18 font_w400 text_a_c">Xem chi tiết
                                    </p>
                                    <span class="arrow_right position_a"></span>
                                </div>
                            </div>
                            <div class="item_outstand">
                                <div class="content_item_outstand d_flex flex_column align_c">
                                    <img src="../images/icon_mpcd.png" alt="Sử dụng miễn phí trọn đời" class="icon_outstands">
                                    <h3 class="tit_outstand color_grey font_s18 line_h21 font_w700 text_a_c">Sử dụng miễn
                                        phí
                                        trọn đời</h3>
                                    <p class="color_grey font_s16 line_h19 font_w400">Miễn phí trọn đời đối với tất cả
                                        các
                                        doanh nghiệp đăng ký trong đại dịch covid-19.</p>
                                </div>
                                <div class="footer_item_outstand position_r">
                                    <p class="see_details color_blue font_s15 line_h18 font_w400 text_a_c">Xem chi tiết
                                    </p>
                                    <span class="arrow_right position_a"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="user_manual">
                    <div class="box_user_manual d_flex">
                        <div class="video_user_manual">
                            <iframe width="466px" height="262px" src="https://youtube.com/embed/DRGRquRiWYc">
                            </iframe>
                        </div>
                        <div class="content_user_manual">
                            <h2 class="color_grey font_s28 line_h36 font_w700">Hướng dẫn sử dụng phần mềm<br>
                                Quản lý kho vật tư xây dựng 365</h2>
                            <p class="text_user_manual color_grey font_s16 line_h19 font_w500">Phần mềm <strong>Quản lý
                                    kho
                                    vật tư xây dựng 365</strong> mang tới giải pháp quản lý
                                kho nguyên vật
                                liệu, vật tư thiết bị xây dựng cho doanh nghiệp một cách hiệu quả, dễ dàng sử dụng, dễ
                                dàng
                                quản lý, an toàn bảo mật và miễn phí trọn đời.</p>
                            <button class="register_now font_s18 line_h21 color_white font_w500 cursor_p">
                            <a rel='nofollow' href="https://quanlychung.timviec365.vn/lua-chon-dang-ky.html"></a>
                            Đăng ký sử
                                dụng
                                ngay</button>
                        </div>
                    </div>
                </div>
                <div class="download_app">
                    <div class="box_download_app">
                        <h2 class="tit_box_home color_grey font_s28 line_h33 font_w700 text_a_c">Download Phần mềm quản lý kho xây dựng 365</h2>
                        <div class="body_download d_flex align_c flex_center">
                            <div class="qr_download">
                                <img src="../images/qr_download.png" alt="Download phần mềm quản lý kho xây dựng">
                            </div>
                            <div class="mobi_download d_flex flex_column">
                                <a href="#" class="gg_play_download">
                                    <img src="../images/gg_play_download.png" alt="Download phần mềm quản lý kho xây dựng trên Android">
                                </a>
                                <a href="#" class="app_store_download">
                                    <img src="../images/app_store_download.png" alt="Download phần mềm quản lý kho xây dựng trên IOS">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="question_frequent">
                    <div class="box_question">
                        <h2 class="tit_box_home color_grey font_s28 line_h33 font_w700 text_a_c">Những câu hỏi thường gặp
                            về
                            phần mềm Quản lý kho vật tư xây dựng 365</h2>
                        <div class="d_flex space_b">
                            <div class="box_question_left position_r">
                                <div class="search_question search_question_mobile d_flex align_c display_none">
                                    <input type="text" placeholder="Nhập nội dung tìm kiếm"
                                        class="color_grey3 font_s14 line_h17 font_w400">
                                    <p class="text_search color_white font_s15 line_h18 font_w500"><span>Tìm kiếm</span>
                                    </p>
                                </div>
                                <div class="list_question">
                                   
                                    <!-- <div class="item_question">
                                        <div class="name_question">
                                            <p class="name_pp color_grey font_s18 line_h21 font_w500">Người dùng ẩn danh
                                            </p>
                                            <p class="time_question color_grey font_s14 line_h17 font_w400">8:00 AM, Thứ
                                                2
                                                ngày
                                                10/10/2021</p>
                                            <p class="text_question color_blue font_s16 line_h19 font_w500">Vì sao đi
                                                điểm
                                                danh,
                                                vị trí nhiều điện thoại định vị vị trí
                                                sai hoặc quá xa trụ sở công ty?</p>
                                        </div>
                                        <div class="rep_question">
                                            <div class="top_rep_question d_flex ">
                                                <img src="../images/ava_ad.png" alt="">
                                                <div>
                                                    <p class="name_pp color_grey font_s18 line_h21 font_w500">Chuyển đổi
                                                        số
                                                        365
                                                    </p>
                                                    <p class="time_question color_grey font_s14 line_h17 font_w400">8:00
                                                        AM,
                                                        Thứ
                                                        2 ngày 10/10/2021</p>
                                                </div>
                                            </div>
                                            <p class="content_rep_question color_grey font_s14 line_h17 font_w400">Thứ
                                                nhất,
                                                Có
                                                thể khi đó điện thoại của bạn đang bật
                                                chế độ tiết kiệm pin. Trên app chấm công 365 cùng nhiều ứng dụng hoạt
                                                động
                                                trên
                                                điện thoại, để kéo dài thời lượng pin, điện thoại của bạn sẽ chặn không
                                                cho
                                                các
                                                ứng dụng của bạn sử dụng vị trí hoặc dữ liệu.
                                            </p>
                                            <div class="bot__rep_question d_flex flex_end position_r">
                                                <p class="see_mores color_blue font_s15 line_h18 font_w400 text_a_r"
                                                    style="display: block;">Xem thêm
                                                    <span class="arrow_right position_a"></span>
                                                </p>
                                                <p class="hide_less color_blue font_s15 line_h18 font_w400 text_a_r"
                                                    style="display: none;">Ẩn bớt
                                                    <span class="arrow_left position_a"></span>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item_question">
                                        <div class="name_question">
                                            <p class="name_pp color_grey font_s18 line_h21 font_w500">Nguyễn Thị Lan Anh
                                            </p>
                                            <p class="time_question color_grey font_s14 line_h17 font_w400">8:00 AM, Thứ
                                                2
                                                ngày
                                                10/10/2021</p>
                                            <p class="text_question color_blue font_s16 line_h19 font_w500">Một công ty
                                                họ
                                                chỉ
                                                đăng ký 1 tài khoản tổng mà họ muốn chấm công ở nhiều vị trí khác nhau
                                                thì
                                                làm
                                                thế nào?</p>
                                        </div>
                                        <div class="rep_question">
                                            <div class="top_rep_question d_flex ">
                                                <img src="../images/ava_ad.png" alt="">
                                                <div>
                                                    <p class="name_pp color_grey font_s18 line_h21 font_w500">Chuyển đổi
                                                        số
                                                        365
                                                    </p>
                                                    <p class="time_question color_grey font_s14 line_h17 font_w400">8:00
                                                        AM,
                                                        Thứ
                                                        2 ngày 10/10/2021</p>
                                                </div>
                                            </div>
                                            <p class="content_rep_question color_grey font_s14 line_h17 font_w400">Thứ
                                                nhất,
                                                Có
                                                thể khi đó điện thoại của bạn đang bật chế độ tiết kiệm pin. Trên app
                                                chấm
                                                công
                                                365 cùng nhiều ứng dụng hoạt động trên điện thoại, để kéo dài thời lượng
                                                pin,
                                                điện thoại của bạn sẽ chặn không cho các ứng dụng của bạn sử dụng vị trí
                                                hoặc dữ
                                                liệu.
                                            </p>
                                            <div class="bot__rep_question d_flex flex_end position_r">
                                                <p class="see_mores color_blue font_s15 line_h18 font_w400 text_a_r"
                                                    style="display: none;">Xem thêm
                                                    <span class="arrow_right position_a"></span>
                                                </p>
                                                <p class="hide_less color_blue font_s15 line_h18 font_w400 text_a_r"
                                                    style="display: block;">Ẩn bớt
                                                    <span class="arrow_left position_a"></span>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item_question">
                                        <div class="name_question">
                                            <p class="name_pp color_grey font_s18 line_h21 font_w500">Người dùng ẩn danh
                                            </p>
                                            <p class="time_question color_grey font_s14 line_h17 font_w400">8:00 AM, Thứ
                                                2
                                                ngày
                                                10/10/2021</p>
                                            <p class="text_question color_blue font_s16 line_h19 font_w500">Vì sao đi
                                                điểm
                                                danh,
                                                vị trí nhiều điện thoại định vị vị trí
                                                sai hoặc quá xa trụ sở công ty?</p>
                                        </div>
                                        <div class="rep_question">
                                            <div class="top_rep_question d_flex ">
                                                <img src="../images/ava_ad.png" alt="">
                                                <div>
                                                    <p class="name_pp color_grey font_s18 line_h21 font_w500">Chuyển đổi
                                                        số
                                                        365
                                                    </p>
                                                    <p class="time_question color_grey font_s14 line_h17 font_w400">8:00
                                                        AM,
                                                        Thứ
                                                        2 ngày 10/10/2021</p>
                                                </div>
                                            </div>
                                            <p class="content_rep_question color_grey font_s14 line_h17 font_w400">Thứ
                                                nhất,
                                                Có
                                                thể khi đó điện thoại của bạn đang bật
                                                chế độ tiết kiệm pin. Trên app chấm công 365 cùng nhiều ứng dụng hoạt
                                                động
                                                trên
                                                điện thoại, để kéo dài thời lượng pin, điện thoại của bạn sẽ chặn không
                                                cho
                                                các
                                                ứng dụng của bạn sử dụng vị trí hoặc dữ liệu.
                                            </p>
                                            <div class="bot__rep_question d_flex flex_end position_r">
                                                <p class="see_mores color_blue font_s15 line_h18 font_w400 text_a_r"
                                                    style="display: block;">Xem thêm
                                                    <span class="arrow_right position_a"></span>
                                                </p>
                                                <p class="hide_less color_blue font_s15 line_h18 font_w400 text_a_r"
                                                    style="display: none;">Ẩn bớt
                                                    <span class="arrow_left position_a"></span>
                                                </p>
                                            </div>
                                        </div>
                                    </div> -->
                                </div>
                                <div class="pre_next_q position_a">
                                    <div class="d_flex space_b">
                                        <div class="pre_q d_flex align_c flex_center">
                                            <span class="pre_arrow"></span>
                                        </div>
                                        <div class="next_q d_flex align_c flex_center">
                                            <span class="next_arrow"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="q_paginition d_flex flex_center">
                                    <span class="q_paginition_detail q_paginition_active"></span>
                                    <span class="q_paginition_detail"></span>
                                    <span class="q_paginition_detail"></span>
                                </div>
                            </div>
                            <div class="box_question_right">
                                <div class="search_question d_flex align_c">
                                    <input type="text" placeholder="Nhập nội dung tìm kiếm"
                                        class="color_grey3 font_s14 line_h17 font_w400">
                                    <p class="text_search color_white font_s15 line_h18 font_w500"><span>Tìm kiếm</span>
                                    </p>
                                </div>
                                <div class="question_f_ad">
                                    <form id="form_question" >
                                        <div class="t_title_a d_flex align_c">
                                            <img src="../images/question.png" alt="câu hỏi">
                                            <p class="color_blue font_s18 line_h21 font_w500">Đặt câu hỏi với Quản lý kho
                                                vật tư
                                                xây dựng 365</p>
                                        </div>
                                        <div class="b_question_a">
                                            <div class="input_name">
                                                <p class="color_grey font_s15 line_h18 font_w500">Họ tên</p>
                                                <input type="text" placeholder="Nhập họ và tên" class="" name="input_name">
                                            </div>
                                            <div class="input_phone_n">
                                                <p class="color_grey font_s15 line_h18 font_w500">Số điện thoại</p>
                                                <input type="text" placeholder="Nhập số điện thoại" class="" name="input_phone_n">
                                            </div>
                                            <div class="input_question">
                                                <p class="color_grey font_s15 line_h18 font_w500">Câu hỏi<span
                                                        style="color: red;">*</span></p>
                                                <textarea name="input_question" id="" rows="5" placeholder="Nhập nội dung"></textarea>
                                            </div>
                                            <div class="input_captcha d_flex space_b">
                                                <div class="capcha_code_q">
                                                    <p class="color_grey font_s15 line_h18 font_w500">Nhập mã Captcha<span
                                                            style="color: red;">*</span></p>
                                                    <input type="text" placeholder="Nhập lại các kí tự trong ảnh"
                                                        class="color_grey font_s14 font_w400 line_h17" name="capcha_code_q">
                                                </div>
                                                <div class="code_cf d_flex align_c">
                                                    <p class="random_code color_grey font_s24 font_wN" id="random_c">456H89
                                                    </p>
                                                    <input type="hidden" class="code_input" id="code_input" value="456H89">
                                                    <img src="../images/reset_code.png" alt="captcha" class="code_reset cursor_p">
                                                </div>
                                            </div>
                                        </div>
                                        <button name="btn_sbm" type="button" class="send_q back_blue color_white font_s18 line_h21 font_w500 cursor_p">Gửi</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- <div class="explain_ql">
                    <div class="box_explain_ql">
                        <p class="explain_ql_w color_grey font_s18 line_h21 font_w500">1. Quản lí kho vật tư xây dựng
                            365 là gì?</p>
                        <p class="color_grey font_s16 line_h18 font_w400"> Ngay từ thời điểm doanh nghiệp hình thành,các
                            thiết bị chấm công kết hợp với việc điểm danh cơ học
                            của nhân sự trở thành căn cứ tiêu biểu giúp các công ty ghi lại được thời điểm đến làm việc,
                            tan
                            làm, nghỉ phép của người lao động từ đó, tính công và lương, thưởng cho người lao động vào
                            cuối
                            tháng một cách chính xác, đồng thời góp phần nâng cao ý thức làm việc và kỷ luật của nhân
                            viên tại
                            nơi làm việc.</p>
                        <div class="img_qlk_w d_flex align_c flex_column">
                            <img src="../images/ql_k_lg.png" alt="Quản lý kho vật tư xây dựng là gì?">
                            <p class="color_grey font_s18 line_h21 font_w400">Quản lý kho vật tư xây dựng là gì?</p>
                        </div>
                    </div>
                </div> -->
                <?php include("../includes/footer-trang-chu.php"); ?>
            </div>
        </div>
    </div>
</body>
<script type="text/javascript" src="../js/jquery-3.4.1.min.js"></script>
<script src="../js/jquery.validate.min.js"></script>

<script>
    $(".send_q").click(function(){
        var form_ch = $("#form_question");
        form_ch.validate({
            errorPlacement: function (error, element) {
            error.appendTo(element.parents(".input_question"));
            error.appendTo(element.parents(".capcha_code_q"));
            error.wrap("<span class='error'>");
            },
            rules: {
                input_question: "required",
                capcha_code_q: "required",
            },
            messages: {
                input_question: "Vui lòng nhập nội dung câu hỏi.",
                capcha_code_q: "Vui nhập mã Captcha.",
            }
        });
        if(form_ch.valid() === true){
            var ho_ten = $("input[name='input_name']").val();
            var so_dien_thoai = $("input[name='input_phone_n']").val();
            var cau_hoi = $("textarea[name='input_question']").val();

            $.ajax({
                url: '../ajax/add_cau_hoi.php',
                type: 'POST',
                data:{
                    ho_ten: ho_ten,
                    so_dien_thoai: so_dien_thoai, 
                    cau_hoi: cau_hoi,
                },
                success: function(data){
                    if(data == ""){
                        alert("Cảm ơn bạn đã gửi câu hỏi cho chúng tôi!");
                        window.location.reload();
                    }else if(data != ""){
                        window.location.reload();
                    }
                }
            })
        }
    })

    var icon_header = $('.icon_header');
    icon_header.click(function () {
        $('.menu_tabl').css('left', '-15px')
        $('.menu_tabl').css('top', '34px')
        $('.menu_tabl').toggle();
    });


    if ($(window).width() <= 480) {
        $('.video_user_manual iframe').css({ width: 320, height: 180 })
    } else {
        $('.video_user_manual iframe').css({ width: 466, height: 262 })
    };

    // Lấy Random Capcha Code
    function random(length) {
        var result = '';
        var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        var charactersLength = characters.length;
        for (var i = 0; i < length; i++) {
            result += characters.charAt(Math.floor(Math.random() *
                charactersLength));
        }
        return result;
    }

    // Xoay random

    function xoay(img, deg) {
        img.css("transform", "rotate(" + deg + "deg)");
        img.css("transition", "0.5s");
    }
    var do_xuay = 0;
    $(".code_reset").click(function () {
        do_xuay += 360;
        console.log(do_xuay)
        xoay($(this), do_xuay);
    })

    $(".random_code").html(random(6));
    $('#code_input').val($(".random_code").html());
    $(".code_reset").click(function () {
        $(".random_code").html(random(6));
        $('#code_input').val($(".random_code").html());
    });

    $('.see_mores').click(function(){
        $(this).hide();
        $(this).parent().find('.hide_less').show();
        $(this).parent().parent().find('.content_rep_question').css('display','block');
    });

    $('.hide_less').click(function(){
        $(this).hide();
        $(this).parent().find('.see_mores').show();
        $(this).parent().parent().find('.content_rep_question').css('display','-webkit-box');
    });

</script>

</html>