<?php 
    
    include("config1.php"); 

    if(!in_array(4,$ro_dc_kho)){
        header("Location: /tong-quan.html");
    }

    $id_dck = getValue('id','int','GET','');

    if (isset($_COOKIE['user']) && $_COOKIE['user'] != "" && $_COOKIE['role'] == 1) {
		$token = $_COOKIE['acc_token'];
		$curl = curl_init();
		$data = array();
		curl_setopt($curl, CURLOPT_POST, 1);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
		curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/user_info_company.php');
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));
	
		$response = curl_exec($curl);
		curl_close($curl);
		$data_tt = json_decode($response, true);
		$tt_user = $data_tt['data']['user_info_result'];
	} else if (isset($_COOKIE['user']) && $_COOKIE['user'] != "" && $_COOKIE['role'] == 2) {
		$token = $_COOKIE['acc_token'];
		$curl = curl_init();
		$data = array();
		curl_setopt($curl, CURLOPT_POST, 1);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
		curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/user_info_employee.php');
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));
		$response = curl_exec($curl);
		curl_close($curl);
		$data_tt = json_decode($response, true);
		$tt_user = $data_tt['data']['user_info_result'];
	}
    $id_cty = $tt_user['com_id'];
	$date = date('Y-m-d', time());

    if (isset($_SESSION['quyen']) && $_SESSION['quyen'] == 1) {
		$curl = curl_init();
		$token = $_COOKIE['acc_token'];
		curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/list_all_employee_of_company.php');
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));
		$response = curl_exec($curl);
		curl_close($curl);

		$data_list = json_decode($response, true);
		$data_list_nv = $data_list['data']['items'];
		$count = count($data_list_nv);
        $user_id = $_SESSION['com_id'];
        
	} elseif (isset($_SESSION['quyen']) && ($_SESSION['quyen'] == 2)) {
		$curl = curl_init();
		$token = $_COOKIE['acc_token'];
		curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/list_all_my_partner.php?get_all=true');
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));
		$response = curl_exec($curl);
		curl_close($curl);
		$data_list = json_decode($response, true);
		$data_list_nv = $data_list['data']['items'];
		$count = count($data_list_nv);
        $user_id = $_SESSION['ep_id'];
	}

    $newArr = [];
    for ($i = 0; $i < count($data_list_nv); $i++) {
        $value = $data_list_nv[$i];
        $newArr[$value["ep_id"]] = $value;
    }

    $edit_dck = new db_query("SELECT `kcxl_trangThai`, `kcxl_ngayHoanThanh`, `kcxl_nguoiTao`, `kcxl_ngayTao`, `kcxl_khoXuat`,
    `kcxl_khoNhap`, `kcxl_nguoiThucHien`, `kcxl_phongBanNguoiGiao`, `kcxl_nguoiNhan`, `kcxl_phongBanNguoiNhan`, `kcxl_ngayThucHienDieuChuyen`, 
    `kcxl_ngayYeuCauHoanThanh`, `kcxl_ghi_chu` 
    FROM `kho-cho-xu-li` WHERE `kcxl_soPhieu` = 'ĐCK' AND `kcxl_id` = $id_dck AND `kcxl_id_ct` = $id_cty");
    
    $info_edit = mysql_fetch_assoc($edit_dck->result);
    
    $id_nguoi_giao = $info_edit['kcxl_nguoiThucHien'];
    $id_nguoi_nhan = $info_edit['kcxl_nguoiNhan'];

    $id_kho_edit = $info_edit['kcxl_khoXuat'];

    $list_dsvt = new db_query("SELECT `dsvt_id`, `dsvt_maVatTuThietBi`, `dsvt_name`, `hsx_name`, `dvt_name`, `xx_name`, `dsvt_donGia`, `slvt_soLuongDieuChuyen`, `dsvt_kho`
    FROM `danh-sach-vat-tu`
	LEFT JOIN `hang-san-xuat` on `dsvt_hangSanXuat` = `hsx_id`       
	LEFT JOIN `don-vi-tinh` on `dsvt_donViTinh` = `dvt_id`
	LEFT JOIN `xuat-xu` on `dsvt_xuatXu` = `xx_id`
    LEFT JOIN `so-luong-vat-tu` on `slvt_maVatTuThietBi` = `dsvt_id`
    LEFT JOIN `kho-cho-xu-li` on `kcxl_id` = `kcxl_soPhieu`
	WHERE `dsvt_check` = 1 AND `slvt_idPhieu` = $id_dck AND `dsvt_id_ct` = $id_cty");

    $kho_xuat = new db_query("SELECT `kho_id`, `kho_maKho`, `kho_name` FROM `kho` WHERE `kho_id_ct` = $id_cty");
    $kho_nhap = new db_query("SELECT `kho_id`, `kho_maKho`, `kho_name` FROM `kho` WHERE `kho_id_ct` = $id_cty");

    $list_all = new db_query("SELECT `dsvt_id`, `dsvt_name`, `hsx_name`, `dvt_name`, `xx_name`,`dsvt_donGia`, `dsvt_kho` FROM `danh-sach-vat-tu`
	LEFT JOIN `hang-san-xuat` on `dsvt_hangSanXuat` = `hsx_id`       
	LEFT JOIN `don-vi-tinh` on `dsvt_donViTinh` = `dvt_id`
	LEFT JOIN `xuat-xu` on `dsvt_xuatXu` = `xx_id`
	WHERE `dsvt_check` = 1 AND `dsvt_id_ct` = $id_cty");

	$responsive = [];
	while (($selected = mysql_fetch_assoc($list_all->result))) {
        $check_kho = explode(',',$selected['dsvt_kho']);
        if(in_array($id_kho_edit,$check_kho)){
            $item['id'] = $selected['dsvt_id'];
            $item['ten_vt'] = $selected['dsvt_name'];
            $item['ten_hang'] = $selected['hsx_name'];
            $item['ten_dvt'] = $selected['dvt_name'];
            $item['ten_xx'] = $selected['xx_name'];
            $item['don_gia'] = $selected['dsvt_donGia'];
            $responsive[$selected['dsvt_id']] = $item;
        }
	}

    // echo "<pre>";
    // print_r($responsive);
    // echo "</pre>";
    // die();

?>
<!DOCTYPE html>
<html lang="vi">

<head>
    <title>Điều chuyển kho</title>
    <meta charset="UTF-8">
    <meta name="robots" content="noindex,nofollow" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="../css/select2.min.css?v=<?= $ver ?>">
    <link rel="stylesheet" type="text/css" href="../css/style.css?v=<?= $ver ?>">
    <link rel="stylesheet" type="text/css" href="../css/style_h.css?v=<?= $ver ?>">
    <link rel="stylesheet" type="text/css" href="../css/style_n.css?v=<?= $ver ?>">
    <link rel="stylesheet" type="text/css" href="../css/style_k.css?v=<?= $ver ?>">
</head>

<body class="seclec2_radius">
    <div class="box_right warehouse_transfer_edit">
        <?php include("../includes/sidebar.php"); ?>
        <div class="box_right_ct" data-id = "<?= $id_dck; ?>">
            <div class="block_change block_wh_tf_add">
                <form action="" method="post" class="f_date_fn">
                    <div class="head_wh d_flex space_b align_c">
                        <div class="head_tab d_flex space_b align_c">
                            <div class="icon_header open_sidebar_w">
                                <span class="icon_header_tbl"></span>
                                <span class="icon_header_tbl"></span>
                                <span class="icon_header_tbl"></span>
                            </div>
                            <?php include("../includes/header.php") ; ?>
                        </div>
                        <p class="color_grey font_s14 line_h17 font_w400">
                            <a href="/dieu-chuyen-kho.html" class="cursor_p">
                                <img src="../images/back_item_g.png" alt="">
                            </a>&nbsp Điều chuyển kho / Chỉnh sửa
                        </p>
                        <?php include("../includes/header.php") ; ?>
                    </div>
                    <div class="add_ex_wh">
                        <div class="tit_info_rq back_blue">

                            <p class="color_white font_s16 line_h19 font_w700">Chỉnh sửa phiếu điều chuyển kho</p>
                        </div>
                        <div class="ct_add_ex_wh">
                            <div class="box_add_ex_wh">
                                <div class="d_flex align_c flex_end wh768">
                                    <!-- <div class="width_create mr_10 d_flex flex_column mb_15">
                                        <p class="color_grey font_s15 line_h18 font_w500">Số phiếu</p>
                                        <input class="color_grey font_s14 line_h17 font_w400" type="text" value="ĐCK-0000" disabled="disabled">
                                    </div> -->
                                    <div class="width_create select_status d_flex space_b">
                                        <div class="d_flex flex_column">
                                            <p class="color_grey font_s15 line_h18 font_w500">Trạng thái</p>
                                            <select name="" id="" class="select_stt color_grey font_s14 line_h17 font_w400" <?= ($_SESSION['quyen'] == '2') ? "disabled" : "" ?> style="width: 100%;">
                                                <option value="1" <?= $info_edit['kcxl_trangThai'] == 1 ? "selected" : "" ?>>Khởi tạo</option>
                                                <?php if($_SESSION['quyen'] == '1'){?>
                                                    <option value="7" <?= $info_edit['kcxl_trangThai'] == 7 ? "selected" : "" ?>>Hoàn thành</option>
                                                <?php }?>
                                            </select>
                                        </div>
                                        <div class="input_date_fn d_flex flex_column">
                                            <p class="color_grey font_s15 line_h18 font_w500">Ngày hoàn thành<span style="color: red;">*</span></p>
                                            <input type="date" name="input_date_fn">
                                        </div>
                                    </div>
                                </div>
                                <div class="d_flex align_c space_b wh768">
                                    <div class="width_create mr_10 d_flex flex_column mb_15">
                                        <p class="color_grey font_s15 line_h18 font_w500">Người tạo</p>
                                        <?php if($_SESSION['quyen'] == 2){ ?>
                                            <input class="nguoi_tao color_grey font_s14 line_h17 font_w400" type="text" value="<?=$newArr[$user_id]['ep_name']; ?>" data="<?= $newArr[$user_id]['ep_id']; ?>" data-ct="<?= $id_cty ?>" disabled="disabled">
                                        <?}else{?>
                                            <input class="nguoi_tao input_value_grey font_14 line_h16 color_grey" readonly type="text" value="<?= $tt_user['com_name'] ?>" data="0" data-ct="<?=$id_cty?>">
                                        <?}?>
                                    </div>
                                    <div class="width_create select_status d_flex flex_column">
                                        <p class="color_grey font_s15 line_h18 font_w500">Ngày tạo</p>
                                        <input class="ngay_tao color_grey font_s14 line_h17 font_w400" type="date" value="<?=$info_edit['kcxl_ngayTao']; ?>" disabled="disabled">
                                    </div>
                                </div>
                                <div class="d_flex align_c space_b wh768">
                                    <div class="box_wh_ex width_create mr_10 d_flex flex_column mb_15">
                                        <p class="color_grey font_s15 line_h18 font_w500">Kho xuất<span style="color: red;">*</span></p>
                                        <select name="select_wh_ex" id="" class="select_wh_ex color_grey font_s14 line_h17 font_w400" style="width: 100%;">
                                            <option value=""></option>
                                            <?php while($row_kho_xuat = mysql_fetch_assoc($kho_xuat->result)){?>
                                                <option value="<?= $row_kho_xuat['kho_id']; ?>" <?=($row_kho_xuat['kho_id'] == $info_edit['kcxl_khoXuat']) ? "selected" : "" ?>> <?=$row_kho_xuat['kho_name'];?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <div class="box_wh_in width_create select_status d_flex flex_column">
                                        <p class="color_grey font_s15 line_h18 font_w500">Kho nhập<span style="color: red;">*</span></p>
                                        <select name="select_wh_in" id="" class="select_wh_in color_grey font_s14 line_h17 font_w400" style="width: 100%;">
                                            <option value=""></option>
                                            <?php while($row_kho_nhap = mysql_fetch_assoc($kho_nhap->result)){?>
                                                <option value="<?= $row_kho_nhap['kho_id']; ?>" <?=($row_kho_nhap['kho_id'] == $info_edit['kcxl_khoNhap']) ? "selected" : "" ?>> <?=$row_kho_nhap['kho_name'];?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="d_flex align_c space_b wh768">
                                    <div class="width_create mr_10 d_flex flex_column mb_15">
                                        <div class="d_flex space_b">
                                            <p class="color_grey font_s15 line_h18 font_w500">Người giao hàng</p>
                                            <a href="#" class="color_blue font_s15 line_h18 font_w400 d_flex align_c"><span
                                                    class="font_s24">+</span>&nbsp;Thêm nhân viên</a>
                                        </div>
                                        <select name="select_shipper_dis" class="select_shipper_dis color_grey font_s14 line_h17 font_w400" style="width: 100%;">
                                            <option value=""></option>
                                            <?php
                                                for($j = 0; $j < count($data_list_nv); $j++){
                                            ?>
                                                <option value="<?=$data_list_nv[$j]['ep_id'];?>" <?=($data_list_nv[$j]['ep_id'] == $info_edit['kcxl_nguoiThucHien']) ? "selected" : "" ?>><?=$data_list_nv[$j]['ep_name']."&nbsp-&nbsp".$data_list_nv[$j]['dep_name'];?></option>
                                            <?php    
                                                }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="width_create select_status d_flex flex_column">
                                        <p class="color_grey font_s15 line_h18 font_w500">Phòng ban</p>
                                        <input type="text" name="name_pb_nguoi_giao" class="name_pb_nguoi_giao color_grey font_s14 line_h17 font_w400" value="<?= $newArr[$id_nguoi_giao]['dep_name']; ?>" disabled="disabled">
                                        <input type="hidden" name="id_pb_nguoi_giao" class="id_pb_nguoi_giao color_grey font_s14 line_h17 font_w400" value="<?= $info_edit['kcxl_phongBanNguoiGiao']; ?>" readonly>
                                    </div>
                                </div>
                                <div class="d_flex align_c space_b wh768">
                                    <div class="width_create mr_10 d_flex flex_column mb_15">
                                        <div class="d_flex space_b">
                                            <p class="color_grey font_s15 line_h18 font_w500">Người nhận</p>
                                            <a href="#" class="color_blue font_s15 line_h18 font_w400 d_flex align_c"><span
                                                    class="font_s24">+</span>&nbsp;Thêm nhân viên</a>
                                        </div>
                                        <select name="select_receiver" class="select_receiver color_grey font_s14 line_h17 font_w400" style="width: 100%;">
                                            <option value=""></option>
                                            <?php
                                                for($j = 0; $j < count($data_list_nv); $j++){
                                            ?>
                                                <option value="<?=$data_list_nv[$j]['ep_id']?>" <?=($data_list_nv[$j]['ep_id'] == $info_edit['kcxl_nguoiNhan']) ? "selected" : "" ?>><?=$data_list_nv[$j]['ep_name']."&nbsp-&nbsp".$data_list_nv[$j]['dep_name']?></option>
                                            <?php    
                                                }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="width_create select_status d_flex flex_column">
                                        <p class="color_grey font_s15 line_h18 font_w500">Phòng ban</p>
                                        <input type="text" class="name_pb_nguoi_nhan color_grey font_s14 line_h17 font_w400" value="<?= $newArr[$id_nguoi_nhan]['dep_name']; ?>" disabled="disabled">
                                        <input type="hidden" class="id_pb_nguoi_nhan color_grey font_s14 line_h17 font_w400" value="<?= $info_edit['kcxl_phongBanNguoiNhan']; ?>" readonly>
                                    </div>
                                </div>
                                <div class="d_flex align_c space_b wh768">
                                    <div class="input_date_tf width_create mr_10 d_flex flex_column mb_15">
                                        <p class="color_grey font_s15 line_h18 font_w500">Ngày thực hiện điều chuyển<span style="color: red;">*</span></p>
                                        <input class="color_grey font_s14 line_h17 font_w400" type="date" name="input_date_tf" value="<?= $info_edit['kcxl_ngayThucHienDieuChuyen']; ?>">
                                    </div>
                                    <div class="input_date_rq_fn width_create select_status d_flex flex_column">
                                        <p class="color_grey font_s15 line_h18 font_w500">Ngày yêu cầu hoàn thành<span style="color: red;">*</span></p>
                                        <input type="date" class="color_grey font_s14 line_h17 font_w400" name="input_date_rq_fn" value="<?= $info_edit['kcxl_ngayYeuCauHoanThanh']; ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="d_flex flex_column">
                                <p class="color_grey font_s15 line_h18 font_w500">Ghi chú</p>
                                <textarea name="note" id="" rows="5" placeholder="Nhập nội dung"><?= $info_edit['kcxl_ghi_chu']; ?></textarea>
                            </div>
                        </div>
                    </div>
                    <p class="tit_table_vt color_blue font_s16 line_h19 font_w700">Danh sách vật tư</p>
                    <div class="tb_operation_wh position_r d_flex align_c">
                        <div class="table_vt_scr">
                            <div class="table_ds_vt">
                                <table style="width: 1453px">
                                    <tr class="tit_tbl color_white font_s16 line_h19 font_w500 back_blue">
                                        <th><span class="span_tbody"></span></th>
                                        <th>Mã vật tư thiết bị <span class="span_tbody"></span></th>
                                        <th>Tên đầy đủ vật tư thiết bị <span class="span_tbody"></span></th>
                                        <th>Đơn vị tính <span class="span_tbody"></span></th>
                                        <th>Hãng sản xuất <span class="span_tbody"></span></th>
                                        <th>Xuất xứ <span class="span_tbody"></span></th>
                                        <th>Số lượng điều chuyển <span class="span_tbody"></span>
                                        </th>
                                        <th>Đơn giá (VNĐ) <span class="span_tbody"></span></th>
                                        <th>Thành tiền (VNĐ)</th>
                                    </tr>
                                    <?php while($row_list_dsvt = mysql_fetch_assoc($list_dsvt->result)) { ?>
                                        <tr class="color_grey font_s14 line_h17 font_w400 table_3 delete_3">
                                            <td onclick="deleteRow(this)"><img class="cursor_p delete_3" src="../images/del_row.png" alt="" style="margin: 0; width: 16px; height: 16px;"></td>
                                            <td class="ma_vat_tu_3 delete_3" style="background-color: #EEEEEE;">VT - <?= $row_list_dsvt['dsvt_id'] ?></td>
                                            <td class="color_blue font_w500 delete_3" style="text-align: left;">
                                                <select class="select_tb color_grey3 font_s14 line_h17 font_w400 select_tb_3 delete_3" style="width: 100%">
                                                    <option value=""></option>
                                                    <? foreach ($responsive as $val) { ?>
                                                        <option value="<?= $val['id']; ?>" <?= ($val['id'] == $row_list_dsvt['dsvt_id']) ? "selected" : ""?>><?= $val['ten_vt'] ?></option>
                                                    <? } ?>
                                                </select>
                                            </td>
                                            <td class="don_vi_tinh_3 delete_3" style="background-color: #EEEEEE;"><?= $row_list_dsvt['dvt_name']; ?></td>
                                            <td class="hang_san_xuat_3 delete_3" style="text-align: left; background-color: #EEEEEE;"><?= $row_list_dsvt['hsx_name']; ?></td>
                                            <td class="xuat_xu_3 delete_3"  style="background-color: #EEEEEE;"><?= $row_list_dsvt['xx_name']; ?></td>
                                            <td><input type="text" value="<?= $row_list_dsvt['slvt_soLuongDieuChuyen']; ?>" class="nhap_sl_3 delete_3 color_grey font_s14 line_h17 font_w400" placeholder="Nhập số lượng" onkeyup="tong_vt(this)"></td>
                                            <td class="don_gia_3 delete_3" style="background-color: #EEEEEE;"><?= number_format($row_list_dsvt['dsvt_donGia'],0,'','.'); ?></td>
                                            <td class="thanh_tien_3 delete_3" style="background-color: #EEEEEE;"><?= (number_format($row_list_dsvt['slvt_soLuongDieuChuyen']*$row_list_dsvt['dsvt_donGia'],0,'','.')); ?></td>
                                        </tr>
                                    <?php
                                        }
                                    ?>
                                    <tbody id="tbody_add"></tbody>
                                    <tr class="tr_add color_blue font_s14 line_h17 font_w500">
                                        <td colspan="2">
                                            <div class="btn_add_vt d_flex align_c cursor_p">
                                                <p class="add_vt d_flex flex_center align_c position_r"><span class="color_white font_s20 position_a">+</span></p>
                                                <p class="">Thêm vật tư</p>
                                            </div>
                                        </td>
                                    </tr>
                                    <!-- <tr class="tr_add color_blue font_s14 line_h17 font_w500">
                                        <td colspan="2">
                                            <div class="btn_add_vt d_flex align_c cursor_p">
                                                <p class="add_vt d_flex flex_center align_c position_r"><span class="color_white font_s20 position_a">+</span></p>
                                                <p class="">Thêm vật tư</p>
                                            </div>
                                        </td>
                                    </tr> -->
                                </table>
                            </div>
                        </div>
                        <div class="pre_q d_flex align_c flex_center position_a">
                            <span class="pre_arrow"></span>
                        </div>
                        <div class="next_q d_flex align_c flex_center position_a">
                            <span class="next_arrow"></span>
                        </div>
                    </div>
                    <div class="btn_cf_rq d_flex flex_center" style="display: flex;">
                        <button type="button" class="btn_cancel back_w color_blue font_s15 line_h18 font_w500 cursor_p">
                        <a href="/dieu-chuyen-kho.html"></a>    
                        Hủy</button>
                        <button type="button" class="btn_save back_blue color_white font_s15 line_h18 font_w500 cursor_p">Lưu</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <? include("../includes/popup_dieu-chuyen-kho.php") ?>
</body>
<script type="text/javascript" src="../js/jquery-3.4.1.min.js"></script>
<script type="text/javascript" src="../js/jquery.validate.min.js"></script>
<script type="text/javascript" src="../js/reset_validate.js"></script>
<script type="text/javascript" src="../js/select2.min.js"></script>
<script type="text/javascript" src="../js/js_h.js"></script>
<script type="text/javascript" src="../js/validate_h.js"></script>
<script type="text/javascript" src="../js/js_k.js"></script>
<script type="text/javascript" src="../js/js_n.js"></script>
<script>
    var data = <?= json_encode($newArr) ?>;
</script>
<script>
    function resetFormValidator(formId) {
 		$(formId).removeData('validator');
 		$(formId).removeData('unobtrusiveValidation');
 		$.validator.unobtrusive.parse(formId);
 	}

    $('.active10').each(function() {
        if ($(this).hasClass('active10')) {
            $(this).find('a').addClass('active');
        }
    });
 
    $('.select_receiver').change(function(){
 		var value1 = $('.select_receiver').val();
 		$('.name_pb_nguoi_nhan').val(data[value1].dep_name);
 		$('.id_pb_nguoi_nhan').val(data[value1].dep_id);
    });

    $('.select_shipper_dis').change(function(){
 		var value2 = $('.select_shipper_dis').val();
 		$('.name_pb_nguoi_giao').val(data[value2].dep_name);
 		$('.id_pb_nguoi_giao').val(data[value2].dep_id);
    });

    $('.btn_add_vt').click(function() {
        var value = $('.select_wh_ex').val()
        var id_cty = $('.nguoi_tao').attr('data-ct');
        if(value != ""){
            $.ajax({
                url: '../ajax/get_dsvt_kho_dieu_chuyen.php',
                type: 'POST',
                data: {
                    id_kho: value,
                    id_cty: id_cty
                },
                success: function(data) {
                    if (data != 'jkkjk') {
                        $("#tbody_add").append(data);
                    } else {
                        $("#tbody_add").append();
                    }
                }
            })
        }
    });

    $('.select_wh_ex').change(function() {
        $('.delete_3').remove();
        var value = $(this).val();
        var id_cty = $('.nguoi_tao').attr('data-ct');
        $.ajax({
            url: '../ajax/get_dsvt_kho_dieu_chuyen.php',
            type: 'POST',
            dataType: 'Json',
            data: {
                id_kho: value,
                id_cty: id_cty
            },
            success: function(response) {
                // console.log(response)
            }
        })
    });

    function tong_vt(id) {
        var sl = Number($(id).val());
        var dg = $(id).parents(".table_3").find('.don_gia_3').text();
        var don_gia = dg.replaceAll('.','');
        var tong = sl * don_gia;
        var thanh_tien = formatNumber(tong);
        $(id).parents(".table_3").find('.thanh_tien_3').text(thanh_tien);
    };

    $(".select_tb").change(function() {
        var value = $(this).val();
        var _this = $(this);
        var value1 = $('.select_wh_ex').val()
        var id_cty = $('.nguoi_tao').attr('data-ct');
        $.ajax({
            url: '../ajax/get_dsvt_kho_dieu_chuyen.php',
            type: 'POST',
            data: {
                id_vt: value,
                id_kho: value1,
                id_cty: id_cty
            },
            success: function(data) {
                _this.parent().parent().html(data);
            }
        })
    });

    function change_value() {
       
        $(".select_tb").change(function() {
            var value = $(this).val();
            var _this = $(this);
            var value1 = $('.select_wh_ex').val()
            var id_cty = $('.nguoi_tao').attr('data-ct');

            $.ajax({
                url: '../ajax/get_dsvt_kho_dieu_chuyen.php',
                type: 'POST',
                data: {
                    id_vt: value,
                    id_kho: value1,
                    id_cty: id_cty
                },
                success: function(data) {
                    _this.parent().parent().html(data);
                }
            })
        });
    }

    $('.btn_save').click(function(){
        // console.log('hello');
        var form_vali = $(".f_date_fn");
        var trang_thai = $('.select_stt').val();
        var ngay_hoan_thanh = $("input[name='input_date_fn']").val();
        var nguoi_tao = $('.nguoi_tao').attr('data');
        var ngay_tao = $('.ngay_tao').val();
        var kho_xuat = $('.select_wh_ex').val();
        var kho_nhap = $('.select_wh_in').val();
        var nguoi_giao_hang = $('.select_shipper_dis').val();
        var phong_ban_ngh = $('.id_pb_nguoi_giao').val();
        var nguoi_nhan = $('.select_receiver').val();
        var phong_ban_nn = $('.id_pb_nguoi_nhan').val();
        var ngay_thuc_hien = $("input[name='input_date_tf']").val();
        var ngay_yc_hoan_thanh = $("input[name='input_date_rq_fn']").val();
        var ghi_chu = $("textarea[name='note']").val();
        var id_cty = $('.nguoi_tao').attr('data-ct');
        var id_dck = $('.box_right_ct').attr('data-id');

        var vat_tu = []
        $(".select_tb").each(function() {
            var id_vattu = $(this).val();
            var soluong = $(this).parent().parent().find('.nhap_sl_3').val();
            if(id_vattu != "" && soluong != ""){
                vat_tu.push({
                    'id': id_vattu,
                    'soluong': soluong,
                });
            }
        }); 

        resetFormValidator(form_vali);
        if(trang_thai === "1"){

            form_vali.validate({
                errorPlacement: function (error, element) {
                error.appendTo(element.parents(".input_date_rq_fn"));
                error.appendTo(element.parents(".input_date_tf"));
                error.appendTo(element.parents(".box_wh_ex"));
                error.appendTo(element.parents(".box_wh_in"));
                error.wrap("<span class='error'>");
                },
                rules: {
                    input_date_rq_fn: "required",
                    input_date_tf: "required",
                    select_wh_ex: "required",
                    select_wh_in: "required",
                },
                messages: {
                    input_date_rq_fn: "Vui lòng chọn ngày.",
                    input_date_tf: "Vui lòng chọn ngày.",
                    select_wh_ex: "Vui lòng chọn kho xuất.",
                    select_wh_in: "Vui lòng chọn kho nhập.",
                }
            });
            
            if(form_vali.valid() === true && vat_tu.length > 0){
                $.ajax({
                    url: '../ajax/edit_dieu_chuyen_kho.php',
                    type: 'POST',
                    data: {
                        id_dck: id_dck,
                        trang_thai: trang_thai,
                        nguoi_tao: nguoi_tao,
                        ngay_tao: ngay_tao,
                        kho_xuat: kho_xuat,
                        kho_nhap: kho_nhap,
                        nguoi_giao_hang: nguoi_giao_hang,
                        phong_ban_ngh: phong_ban_ngh,
                        nguoi_nhan: nguoi_nhan,
                        phong_ban_nn: phong_ban_nn,
                        ngay_thuc_hien: ngay_thuc_hien,
                        ngay_yc_hoan_thanh: ngay_yc_hoan_thanh,
                        ghi_chu: ghi_chu,
                        id_cty: id_cty,
                        vat_tu: vat_tu
                    },
                    success: function(data) {
                        $('.popup_add_notif_succ').show();
                        var text = $('#popup_add_notif_succ .p_add_succ').text('');
                        var text_new = '';
                        text_new += 'Chỉnh sửa phiếu điều chuyển kho';
                        text_new += '<strong>';
                        text_new += '</strong>';
                        text_new += '&nbspthành công!';
                        text.append(text_new);
                    }
                });
            }else{
                alert("Vui lòng điền đầy đủ thông tin!");
            }
        }
        
        if(trang_thai === "7"){
            form_vali.validate({
                errorPlacement: function (error, element) {
                error.appendTo(element.parents(".input_date_fn"));
                error.appendTo(element.parents(".input_date_rq_fn"));
                error.appendTo(element.parents(".input_date_tf"));
                error.appendTo(element.parents(".box_wh_ex"));
                error.appendTo(element.parents(".box_wh_in"));
                error.wrap("<span class='error'>");
                },
                rules: {
                    input_date_fn: "required",
                    input_date_rq_fn: "required",
                    input_date_tf: "required",
                    select_wh_ex: "required",
                    select_wh_in: "required",
                },
                messages: {
                    input_date_fn: "Vui lòng chọn ngày.",
                    input_date_rq_fn: "Vui lòng chọn ngày.",
                    input_date_tf: "Vui lòng chọn ngày.",
                    select_wh_ex: "Vui lòng chọn kho xuất.",
                    select_wh_in: "Vui lòng chọn kho nhập.",
                }
            });
            
            if(form_vali.valid() === true && vat_tu.length > 0){
                $.ajax({
                    url: '../ajax/edit_dieu_chuyen_kho.php',
                    type: 'POST',
                    data: {
                        id_dck: id_dck,
                        trang_thai: trang_thai,
                        ngay_hoan_thanh: ngay_hoan_thanh,
                        nguoi_tao: nguoi_tao,
                        ngay_tao: ngay_tao,
                        kho_xuat: kho_xuat,
                        kho_nhap: kho_nhap,
                        nguoi_giao_hang: nguoi_giao_hang,
                        phong_ban_ngh: phong_ban_ngh,
                        nguoi_nhan: nguoi_nhan,
                        phong_ban_nn: phong_ban_nn,
                        ngay_thuc_hien: ngay_thuc_hien,
                        ngay_yc_hoan_thanh: ngay_yc_hoan_thanh,
                        ghi_chu: ghi_chu,
                        id_cty: id_cty,
                        vat_tu: vat_tu
                    },
                    success: function(data) {
                        $('.popup_add_notif_succ').show();
                        var text = $('#popup_add_notif_succ .p_add_succ').text('');
                        var text_new = '';
                        text_new += 'Chỉnh sửa phiếu điều chuyển kho';
                        text_new += '<strong>';
                        text_new += '</strong>';
                        text_new += '&nbspthành công!';
                        text.append(text_new);
                    }
                });
            }else{
                alert("Vui lòng điền đầy đủ thông tin!");
            }
        }

    });

    $('.btn_close').click(function(){
        window.location.reload();
    });
    

</script>

</html>