<?php include("config.php"); ?>
<!DOCTYPE html>
<html lang="vi">

<head>
    <title>Điều chuyển kho - Chi tiết phiếu điều chuyển kho</title>
    <meta charset="UTF-8">
    <meta name="robots" content="noindex,nofollow" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="../css/select2.min.css?v=<?= $ver ?>">
    <link rel="stylesheet" type="text/css" href="../css/style.css?v=<?= $ver ?>">
    <link rel="stylesheet" type="text/css" href="../css/style_h.css?v=<?= $ver ?>">
    <link rel="stylesheet" type="text/css" href="../css/style_n.css?v=<?= $ver ?>">
    <link rel="stylesheet" type="text/css" href="../css/style_k.css?v=<?= $ver ?>">
</head>

<body>

    <div class="box_right warehouse_transfer warehouse_transfer_detail2">
        <div class="box_right_ct">
            <?php include("../includes/sidebar.php"); ?>
            <div class="block_change block_wh_tf_add">
                <div class="head_wh d_flex space_b align_c">
                <div class="head_tab d_flex space_b align_c">
                        <div class="icon_header open_sidebar_w">
                            <span class="icon_header_tbl"></span>
                            <span class="icon_header_tbl"></span>
                            <span class="icon_header_tbl"></span>
                        </div>
                        <?php include("../includes/header.php") ; ?>
                    </div>
                    <p class="color_grey font_s14 line_h17 font_w400">
                        <a href="/dieu-chuyen-kho.html" class="cursor_p">
                            <img src="../images/back_item_g.png" alt="">
                        </a>&nbsp Điều chuyển kho / Chi tiết phiếu điều chuyển kho
                    </p>
                    <?php include("../includes/header.php") ; ?>
                </div>
                <!-- tùy chọn -->
                <div class="operation_wh d_flex space_b flex_end">
                    <div class="export_wh d_flex space_b align_c">
                        <button class="btn_dt d_flex flex_center align_c">
                            <img src="../images/check__w.png" alt="">
                            <p class="color_white font_s15 line_h18 font_w500">Duyệt</p>
                        </button>
                        <button class="btn_del d_flex flex_center align_c">
                            <p class="color_blue font_s15 line_h18 font_w500">Xóa phiếu</p>
                        </button>
                        <button class="btn_px d_flex flex_center align_c">
                            <img src="../images/edit_w.png" alt="">
                            <p class="color_white font_s15 line_h18 font_w500">Chỉnh sửa</p>
                        </button>
                        <button class="btn_ex d_flex flex_center align_c">
                            <img src="../images/export.png" alt="">
                            <p class="color_white font_s15 line_h18 font_w500">Xuất excel</p>
                        </button>
                    </div>
                </div>
                <!-- Thông tin phiếu điều chuyển kho -->
                <div class="info_rq info_rq_tf" style="display: block;">
                    <div class="info_rq_scr">
                        <div class="tit_info_rq back_blue">
                            <p class="color_white font_s16 line_h19 font_w700">Thông tin phiếu điều chuyển kho</p>
                        </div>
                        <div class="ct_info_rq">
                            <div class="list_ct_info_rq">
                                <div class="item_ct_info_rq d_flex align_c">
                                    <p class="tit_it color_grey font_s14 line_h17 font_w400">Số phiếu:</p>
                                    <p class="color_grey font_s14 line_h17 font_w500">ĐCK-0000</p>
                                </div>
                                <div class="item_ct_info_rq d_flex align_c">
                                    <p class="tit_it color_grey font_s14 line_h17 font_w400">Trạng thái:</p>
                                    <p class="color_org font_s14 line_h17 font_w500">Từ chối</p>
                                </div>
                                <div class="item_ct_info_rq d_flex align_c">
                                    <p class="tit_it color_grey font_s14 line_h17 font_w400">Người tạo:</p>
                                    <div class="d_flex flex_start align_c">
                                        <img src="../images/ava_ad.png" alt="">
                                        <p class="color_grey font_s14 line_h17 font_w500">Nguyễn Văn Nam</p>
                                    </div>
                                </div>
                                <div class="item_ct_info_rq d_flex align_c">
                                    <p class="tit_it color_grey font_s14 line_h17 font_w400">Ngày tạo:</p>
                                    <p class="color_grey font_s14 line_h17 font_w500">10/10/2020</p>
                                </div>
                                <div class="item_ct_info_rq d_flex align_c">
                                    <p class="tit_it color_grey font_s14 line_h17 font_w400">Kho xuất:</p>
                                    <p class="color_grey font_s14 line_h17 font_w500">Kho 1</p>
                                </div>
                                <div class="item_ct_info_rq d_flex align_c">
                                    <p class="tit_it color_grey font_s14 line_h17 font_w400">Kho nhập:</p>
                                    <p class="color_grey font_s14 line_h17 font_w500">Kho 2</p>
                                </div>
                                <div class="item_ct_info_rq d_flex align_c">
                                    <p class="tit_it color_grey font_s14 line_h17 font_w400">Người giao hàng:</p>
                                    <p class="color_grey font_s14 line_h17 font_w500">Nguyễn Thị Hà</p>
                                </div>
                                <div class="item_ct_info_rq d_flex align_c">
                                    <p class="tit_it color_grey font_s14 line_h17 font_w400">Phòng ban:</p>
                                    <p class="color_grey font_s14 line_h17 font_w500">Phòng nhân sự</p>
                                </div>
                                <div class="item_ct_info_rq d_flex align_c">
                                    <p class="tit_it color_grey font_s14 line_h17 font_w400">Người nhận:</p>
                                    <div class="d_flex flex_start align_c">
                                        <img src="../images/ava_ad.png" alt="">
                                        <p class="color_grey font_s14 line_h17 font_w500">Nguyễn Văn Nam</p>
                                    </div>
                                </div>
                                <div class="item_ct_info_rq d_flex align_c">
                                    <p class="tit_it color_grey font_s14 line_h17 font_w400">Phòng ban:</p>
                                    <p class="color_grey font_s14 line_h17 font_w500">Phòng nhân sự</p>
                                </div>
                                <div class="item_ct_info_rq d_flex align_c">
                                    <p class="tit_it color_grey font_s14 line_h17 font_w400">Ngày yêu thực hiện điều chuyển:
                                    </p>
                                    <p class="color_grey font_s14 line_h17 font_w500">Kho ngã tư sở</p>
                                </div>
                                <div class="item_ct_info_rq d_flex align_c">
                                    <p class="tit_it color_grey font_s14 line_h17 font_w400">Ngày yêu cầu hoàn thành:</p>
                                    <p class="color_grey font_s14 line_h17 font_w500">10/10/2020</p>
                                </div>
                                <div class="item_ct_info_rq d_flex align_s">
                                    <p class="tit_it color_grey font_s14 line_h17 font_w400">Ghi chú:</p>
                                    <p class="color_grey font_s14 line_h17 font_w500">Là năng lực thực hiện các công việc,
                                        biến kiến thức thành hành động. Thông thường kỹ năng được chia thành các cấp độ
                                        chính như: bắt chước (quan sát và hành vi khuôn mẫu), ứng dụng (thực hiện một số
                                        hành động bằng cách làm theo hướng dẫn), vận dụng (chính xác hơn với mỗi hoàn cảnh),
                                        vận dụng sáng tạo (trở thành phản xạ tự nhiên).</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Danh sách vật tư -->
                <p class="tit_table_vt color_blue font_s16 line_h19 font_w700">Danh sách vật tư</p>
                <div class="tb_operation_wh position_r d_flex align_c">
                    <div class="table_vt_scr">
                        <div class="table_ds_vt">
                            <table style="width: 1568px">
                                <tr class="tit_tbl color_white font_s16 line_h19 font_w500 back_blue">
                                    <th>STT<span class="span_tbody"></span></th>
                                    <th>Mã vật tư thiết bị<span class="span_tbody"></span></th>
                                    <th>Tên đầy đủ vật tư thiết bị<span class="span_tbody"></span></th>
                                    <th>Đơn vị tính<span class="span_tbody"></span></th>
                                    <th>Hãng sản xuất<span class="span_tbody"></span></th>
                                    <th>Xuất xứ<span class="span_tbody"></span></th>
                                    <th>Số lượng yêu cầu<span class="span_tbody"></span></th>
                                    <th>Số lượng thực tế xuất kho<span class="span_tbody"></span></th>
                                    <th>Số lượng thực tế nhập kho</th>
                                </tr>
                                <tr class="color_grey font_s14 line_h17 font_w400">
                                    <td>1</td>
                                    <td>VT-0000</td>
                                    <td class="color_blue font_w500" style="text-align: left;">Ống nhựa</td>
                                    <td>Chai</td>
                                    <td style="text-align: left;">Việt Hà</td>
                                    <td>Việt Nam</td>
                                    <td style="text-align: right;">100</td>
                                    <td style="text-align: right;">100</td>
                                    <td style="text-align: right;">100</td>
                                </tr>
                                <tr class="color_grey font_s14 line_h17 font_w400">
                                    <td>1</td>
                                    <td>VT-0000</td>
                                    <td class="color_blue font_w500" style="text-align: left;">Ống nhựa</td>
                                    <td>Chai</td>
                                    <td style="text-align: left;">Việt Hà</td>
                                    <td>Việt Nam</td>
                                    <td style="text-align: right;">100</td>
                                    <td style="text-align: right;">100</td>
                                    <td style="text-align: right;">100</td>
                                </tr>
                                <tr class="color_grey font_s14 line_h17 font_w400">
                                    <td>1</td>
                                    <td>VT-0000</td>
                                    <td class="color_blue font_w500" style="text-align: left;">Ống nhựa</td>
                                    <td>Chai</td>
                                    <td style="text-align: left;">Việt Hà</td>
                                    <td>Việt Nam</td>
                                    <td style="text-align: right;">100</td>
                                    <td style="text-align: right;">100</td>
                                    <td style="text-align: right;">100</td>
                                </tr>
                                <tr class="color_grey font_s14 line_h17 font_w400">
                                    <td>1</td>
                                    <td>VT-0000</td>
                                    <td class="color_blue font_w500" style="text-align: left;">Ống nhựa</td>
                                    <td>Chai</td>
                                    <td style="text-align: left;">Việt Hà</td>
                                    <td>Việt Nam</td>
                                    <td style="text-align: right;">100</td>
                                    <td style="text-align: right;">100</td>
                                    <td style="text-align: right;">100</td>
                                </tr>
                                <tr class="color_grey font_s14 line_h17 font_w400">
                                    <td>1</td>
                                    <td>VT-0000</td>
                                    <td class="color_blue font_w500" style="text-align: left;">Ống nhựa</td>
                                    <td>Chai</td>
                                    <td style="text-align: left;">Việt Hà</td>
                                    <td>Việt Nam</td>
                                    <td style="text-align: right;">100</td>
                                    <td style="text-align: right;">100</td>
                                    <td style="text-align: right;">100</td>
                                </tr>
                                <tr class="color_grey font_s14 line_h17 font_w400">
                                    <td>1</td>
                                    <td>VT-0000</td>
                                    <td class="color_blue font_w500" style="text-align: left;">Ống nhựa</td>
                                    <td>Chai</td>
                                    <td style="text-align: left;">Việt Hà</td>
                                    <td>Việt Nam</td>
                                    <td style="text-align: right;">100</td>
                                    <td style="text-align: right;">100</td>
                                    <td style="text-align: right;">100</td>
                                </tr>
                                <tr class="color_grey font_s14 line_h17 font_w400">
                                    <td>1</td>
                                    <td>VT-0000</td>
                                    <td class="color_blue font_w500" style="text-align: left;">Ống nhựa</td>
                                    <td>Chai</td>
                                    <td style="text-align: left;">Việt Hà</td>
                                    <td>Việt Nam</td>
                                    <td style="text-align: right;">100</td>
                                    <td style="text-align: right;">100</td>
                                    <td style="text-align: right;">100</td>
                                </tr>
                                <tr class="color_grey font_s14 line_h17 font_w400">
                                    <td>1</td>
                                    <td>VT-0000</td>
                                    <td class="color_blue font_w500" style="text-align: left;">Ống nhựa</td>
                                    <td>Chai</td>
                                    <td style="text-align: left;">Việt Hà</td>
                                    <td>Việt Nam</td>
                                    <td style="text-align: right;">100</td>
                                    <td style="text-align: right;">100</td>
                                    <td style="text-align: right;">100</td>
                                </tr>
                                <tr class="color_grey font_s14 line_h17 font_w400">
                                    <td>1</td>
                                    <td>VT-0000</td>
                                    <td class="color_blue font_w500" style="text-align: left;">Ống nhựa</td>
                                    <td>Chai</td>
                                    <td style="text-align: left;">Việt Hà</td>
                                    <td>Việt Nam</td>
                                    <td style="text-align: right;">100</td>
                                    <td style="text-align: right;">100</td>
                                    <td style="text-align: right;">100</td>
                                </tr>
                                <tr class="color_grey font_s14 line_h17 font_w400">
                                    <td>1</td>
                                    <td>VT-0000</td>
                                    <td class="color_blue font_w500" style="text-align: left;">Ống nhựa</td>
                                    <td>Chai</td>
                                    <td style="text-align: left;">Việt Hà</td>
                                    <td>Việt Nam</td>
                                    <td style="text-align: right;">100</td>
                                    <td style="text-align: right;">100</td>
                                    <td style="text-align: right;">100</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="pre_q d_flex align_c flex_center position_a">
                        <span class="pre_arrow"></span>
                    </div>
                    <div class="next_q d_flex align_c flex_center position_a">
                        <span class="next_arrow"></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>

</body>
<script type="text/javascript" src="../js/jquery-3.4.1.min.js"></script>
<script type="text/javascript" src="../js/jquery.validate.min.js"></script>
<script type="text/javascript" src="../js/select2.min.js"></script>
<script type="text/javascript" src="../js/js_h.js"></script>
<script type="text/javascript" src="../js/validate_h.js"></script>
<script type="text/javascript" src="../js/js_k.js"></script>
<script type="text/javascript" src="../js/js_n.js"></script>
<script>

</script>

</html>