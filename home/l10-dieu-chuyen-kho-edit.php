<?php include("config.php"); ?>
<!DOCTYPE html>
<html lang="vi">

<head>
    <title>Điều chuyển kho</title>
    <meta charset="UTF-8">
    <meta name="robots" content="noindex,nofollow" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="../css/select2.min.css?v=<?= $ver ?>">
    <link rel="stylesheet" type="text/css" href="../css/style.css?v=<?= $ver ?>">
    <link rel="stylesheet" type="text/css" href="../css/style_h.css?v=<?= $ver ?>">
    <link rel="stylesheet" type="text/css" href="../css/style_n.css?v=<?= $ver ?>">
    <link rel="stylesheet" type="text/css" href="../css/style_k.css?v=<?= $ver ?>">
</head>

<body class="seclec2_radius">
    <div class="box_right warehouse_transfer_add">
        <?php include("../includes/sidebar.php"); ?>
        <div class="box_right_ct">
            <div class="block_change block_wh_tf_add">
                <form action="" method="post" class="f_date_fn">
                    <div class="head_wh d_flex space_b align_c">
                        <div class="head_tab d_flex space_b align_c">
                            <div class="icon_header open_sidebar_w">
                                <span class="icon_header_tbl"></span>
                                <span class="icon_header_tbl"></span>
                                <span class="icon_header_tbl"></span>
                            </div>
                            <?php include("../includes/header.php") ; ?>
                        </div>
                        <p class="color_grey font_s14 line_h17 font_w400">
                            <a href="/dieu-chuyen-kho.html" class="cursor_p">
                                <img src="../images/back_item_g.png" alt="">
                            </a>&nbsp Điều chuyển kho / Chỉnh sửa
                        </p>
                        <?php include("../includes/header.php") ; ?>
                    </div>
                    <div class="add_ex_wh">
                        <div class="tit_info_rq back_blue">

                            <p class="color_white font_s16 line_h19 font_w700">Chỉnh sửa phiếu điều chuyển kho</p>
                        </div>
                        <div class="ct_add_ex_wh">
                            <div class="box_add_ex_wh d_flex space_b">
                                <div class="l_add_ex">
                                    <div class="d_flex flex_column mb_15">
                                        <p class="color_grey font_s15 line_h18 font_w500">Số phiếu</p>
                                        <input class="color_grey font_s14 line_h17 font_w400" type="text" value="ĐCK-0000" disabled="disabled">
                                    </div>
                                    <div class="d_flex flex_column mb_15">
                                        <p class="color_grey font_s15 line_h18 font_w500">Người tạo</p>
                                        <input class="color_grey font_s14 line_h17 font_w400" type="text" value="Nguyễn Văn Nam" disabled="disabled">
                                    </div>
                                    <div class="box_wh_ex d_flex flex_column mb_15">
                                        <p class="color_grey font_s15 line_h18 font_w500">Kho xuất<span style="color: red;">*</span></p>
                                        <select name="select_wh_ex" id="" class="select_wh_ex color_grey font_s14 line_h17 font_w400" style="width: 100%;">
                                            <option value=""></option>
                                            <option value="1">Kho 1</option>
                                            <option value="2">Kho 2</option>
                                            <option value="3">Kho 3</option>
                                            <option value="4">Kho 4</option>
                                            <option value="5">Kho 5</option>
                                            <option value="6">Kho 6</option>
                                            <option value="7">Kho 7</option>
                                        </select>
                                    </div>
                                    <div class="d_flex flex_column mb_15">
                                        <div class="d_flex space_b">
                                            <p class="color_grey font_s15 line_h18 font_w500">Người giao hàng</p>
                                            <a href="#" class="color_blue font_s15 line_h18 font_w400 d_flex align_c"><span
                                                    class="font_s24">+</span>&nbsp;Thêm nhân viên</a>
                                        </div>
                                        <select name="" id="" class="select_shipper_dis color_grey font_s14 line_h17 font_w400" style="width: 100%;">
                                            <option value=""></option>
                                            <option value="1">Ngọc Small</option>
                                            <option value="2">Ngọc Medium</option>
                                            <option value="3">Ngọc Big</option>
                                        </select>
                                    </div>
                                    <div class="d_flex flex_column mb_15">
                                        <div class="d_flex space_b">
                                            <p class="color_grey font_s15 line_h18 font_w500">Người nhận</p>
                                            <a href="#" class="color_blue font_s15 line_h18 font_w400 d_flex align_c"><span
                                                    class="font_s24">+</span>&nbsp;Thêm nhân viên</a>
                                        </div>
                                        <select name="" id="" class="select_receiver color_grey font_s14 line_h17 font_w400" style="width: 100%;">
                                            <option value=""></option>
                                            <option value="1">Ngọc Small</option>
                                            <option value="2">Ngọc Medium</option>
                                            <option value="3">Ngọc Big</option>
                                        </select>
                                    </div>
                                    <div class="input_date_tf d_flex flex_column mb_15">
                                        <p class="color_grey font_s15 line_h18 font_w500">Ngày thực hiện điều chuyển<span style="color: red;">*</span></p>
                                        <input class="color_grey font_s14 line_h17 font_w400" type="date" name="input_date_tf">
                                    </div>
                                </div>
                                <div class="r_add_ex">
                                    <div class="select_status d_flex space_b">
                                        <div class="d_flex flex_column">
                                            <p class="color_grey font_s15 line_h18 font_w500">Trạng thái</p>
                                            <select name="" id="" class="select_stt color_grey font_s14 line_h17 font_w400" style="width: 100%;">
                                                <option value="kt">Khởi tạo</option>
                                                <option value="ht">Hoàn thành</option>
                                            </select>
                                        </div>
                                        <div class="input_date_fn d_flex flex_column">
                                            <p class="color_grey font_s15 line_h18 font_w500">Ngày hoàn thành<span style="color: red;">*</span></p>
                                            <input type="text" placeholder="Chọn ngày" onfocus="(this.type='date')" name="input_date_fn">
                                        </div>
                                    </div>
                                    <div class="d_flex flex_column mb_15">
                                        <p class="color_grey font_s15 line_h18 font_w500">Ngày tạo</p>
                                        <input class="color_grey font_s14 line_h17 font_w400" type="date" value="10/10/2020" disabled="disabled">
                                    </div>
                                    <div class="box_wh_in d_flex flex_column mb_15">
                                        <p class="color_grey font_s15 line_h18 font_w500">Kho nhập<span style="color: red;">*</span></p>
                                        <select name="select_wh_in" id="" class="select_wh_in color_grey font_s14 line_h17 font_w400" style="width: 100%;">
                                            <option value=""></option>
                                            <option value=""></option>
                                            <option value="1">Kho 1</option>
                                            <option value="2">Kho 2</option>
                                            <option value="3">Kho 3</option>
                                            <option value="4">Kho 4</option>
                                            <option value="5">Kho 5</option>
                                            <option value="6">Kho 6</option>
                                            <option value="7">Kho 7</option>
                                        </select>
                                    </div>
                                    <div class="d_flex flex_column mb_15">
                                        <p class="color_grey font_s15 line_h18 font_w500">Phòng ban</p>
                                        <input type="text" class="color_grey font_s14 line_h17 font_w400" value="Hiển thị phòng ban người giao" disabled="disabled">
                                    </div>
                                    <div class="d_flex flex_column mb_15">
                                        <p class="color_grey font_s15 line_h18 font_w500">Phòng ban</p>
                                        <input type="text" class="color_grey font_s14 line_h17 font_w400" value="Hiển thị phòng ban người nhận" disabled="disabled">
                                    </div>
                                    <div class="input_date_rq_fn d_flex flex_column mb_15">
                                        <p class="color_grey font_s15 line_h18 font_w500">Ngày yêu cầu hoàn thành<span style="color: red;">*</span></p>
                                        <input type="date" class="color_grey font_s14 line_h17 font_w400" name="input_date_rq_fn">
                                    </div>
                                </div>
                            </div>
                            <div class="d_flex flex_column">
                                <p class="color_grey font_s15 line_h18 font_w500">Ghi chú</p>
                                <textarea name="" id="" rows="5" placeholder="Nhập nội dung"></textarea>
                            </div>
                        </div>
                    </div>
                    <p class="tit_table_vt color_blue font_s16 line_h19 font_w700">Danh sách vật tư</p>
                    <div class="tb_operation_wh position_r d_flex align_c">
                        <div class="table_vt_scr">
                            <div class="table_ds_vt">
                                <table style="width: 1453px">
                                    <tr class="tit_tbl color_white font_s16 line_h19 font_w500 back_blue">
                                        <th><span class="span_tbody"></span></th>
                                        <th>Mã vật tư thiết bị <span class="span_tbody"></span></th>
                                        <th>Tên đầy đủ vật tư thiết bị <span class="span_tbody"></span></th>
                                        <th>Đơn vị tính <span class="span_tbody"></span></th>
                                        <th>Hãng sản xuất <span class="span_tbody"></span></th>
                                        <th>Xuất xứ <span class="span_tbody"></span></th>
                                        <th>Số lượng điều chuyển <span class="span_tbody"></span>
                                        </th>
                                        <th>Đơn giá (VNĐ) <span class="span_tbody"></span></th>
                                        <th>Thành tiền (VNĐ)</th>
                                    </tr>
                                    <tr class="color_grey font_s14 line_h17 font_w400">
                                        <td><img class="cursor_p" src="../images/del_row.png" alt="" style="margin: 0; width: 16px; height: 16px;"></td>
                                        <td style="background-color: #EEEEEE;">VT-0000</td>
                                        <td class="color_blue font_w500" style="text-align: left;">
                                            <select class="select_tb color_grey3 font_s14 line_h17 font_w400" style="width: 100%">
                                                <option value=""></option>
                                                <option value="1">Vật tư 1</option>
                                                <option value="2">Vật tư 1</option>
                                                <option value="3">Vật tư 1</option>
                                                <option value="4">Vật tư 1</option>
                                                <option value="5">Vật tư 1</option>
                                                <option value="6">Vật tư 1</option>
                                                <option value="7">Vật tư 1</option>
                                            </select>
                                        </td>
                                        <td style="background-color: #EEEEEE;">Chai</td>
                                        <td style="text-align: left; background-color: #EEEEEE;">Việt Hà</td>
                                        <td style="background-color: #EEEEEE;">Việt Nam</td>
                                        <td><input type="text" value="100" class="color_grey" placeholder="Nhập số lượng"></td>
                                        <td style="background-color: #EEEEEE;">10.000.000</td>
                                        <td style="background-color: #EEEEEE;">10.000.000</td>
                                    </tr>
                                    <tr class="color_grey font_s14 line_h17 font_w400">
                                        <td><img class="cursor_p" src="../images/del_row.png" alt="" style="margin: 0; width: 16px; height:16px;"></td>
                                        <td style="background-color: #EEEEEE;">VT-0000</td>
                                        <td class="color_blue font_w500" style="text-align: left;">
                                            <select class="select_tb color_grey3 font_s14 line_h17 font_w400" style="width: 100%">
                                                <option value=""></option>
                                                <option value="1">Vật tư 1</option>
                                                <option value="2">Vật tư 1</option>
                                                <option value="3">Vật tư 1</option>
                                                <option value="4">Vật tư 1</option>
                                                <option value="5">Vật tư 1</option>
                                                <option value="6">Vật tư 1</option>
                                                <option value="7">Vật tư 1</option>
                                            </select>
                                        </td>
                                        <td style="background-color: #EEEEEE;">Chai</td>
                                        <td style="text-align: left; background-color: #EEEEEE;">Việt Hà</td>
                                        <td style="background-color: #EEEEEE;">Việt Nam</td>
                                        <td><input type="text" value="100" class="color_grey" placeholder="Nhập số lượng"></td>
                                        <td style="background-color: #EEEEEE;">10.000.000</td>
                                        <td style="background-color: #EEEEEE;">10.000.000</td>
                                    </tr>
                                    <tr class="color_grey font_s14 line_h17 font_w400">
                                        <td><img class="cursor_p" src="../images/del_row.png" alt="" style="margin: 0; width: 16px; height:16px;"></td>
                                        <td style="background-color: #EEEEEE;"></td>
                                        <td class="color_blue font_w500" style="text-align: left;">
                                            <select class="select_tb color_grey3 font_s14 line_h17 font_w400" style="width: 100%">
                                                <option value=""></option>
                                                <option value="1">Vật tư 1</option>
                                                <option value="2">Vật tư 1</option>
                                                <option value="3">Vật tư 1</option>
                                                <option value="4">Vật tư 1</option>
                                                <option value="5">Vật tư 1</option>
                                                <option value="6">Vật tư 1</option>
                                                <option value="7">Vật tư 1</option>
                                            </select>
                                        </td>
                                        <td style="background-color: #EEEEEE;"></td>
                                        <td style="text-align: left; background-color: #EEEEEE;"></td>
                                        <td style="background-color: #EEEEEE;"></td>
                                        <td><input type="text" class="color_grey" placeholder="Nhập số lượng"></td>
                                        <td style="background-color: #EEEEEE;"></td>
                                        <td style="background-color: #EEEEEE;"></td>
                                    </tr>
                                    <tr class="tr_add color_blue font_s14 line_h17 font_w500">
                                        <td colspan="2">
                                            <div class="btn_add_vt d_flex align_c cursor_p">
                                                <p class="add_vt d_flex flex_center align_c position_r"><span class="color_white font_s20 position_a">+</span></p>
                                                <p class="">Thêm vật tư</p>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="pre_q d_flex align_c flex_center position_a">
                            <span class="pre_arrow"></span>
                        </div>
                        <div class="next_q d_flex align_c flex_center position_a">
                            <span class="next_arrow"></span>
                        </div>
                    </div>
                    <div class="btn_cf_rq d_flex flex_center" style="display: flex;">
                        <button type="button" class="btn_cancel back_w color_blue font_s15 line_h18 font_w500 cursor_p">Hủy</button>
                        <button type="submit" class="btn_save back_blue color_white font_s15 line_h18 font_w500 cursor_p">Lưu</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <? include("../includes/popup_dieu-chuyen-kho.php") ?>
</body>
<script type="text/javascript" src="../js/jquery-3.4.1.min.js"></script>
<script type="text/javascript" src="../js/jquery.validate.min.js"></script>
<script type="text/javascript" src="../js/select2.min.js"></script>
<script type="text/javascript" src="../js/js_h.js"></script>
<script type="text/javascript" src="../js/validate_h.js"></script>
<script type="text/javascript" src="../js/js_k.js"></script>
<script type="text/javascript" src="../js/js_n.js"></script>
<script>
    $('.active10').each(function() {
        if ($(this).hasClass('active10')) {
            $(this).find('a').addClass('active');
        }
    });
</script>

</html>