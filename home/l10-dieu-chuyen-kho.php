<?php 
  include("config1.php");

  if(!in_array(1,$ro_dc_kho)){
    header("Location: /tong-quan.html");
  }

  isset($_GET['page']) ? $page = $_GET['page'] : $page = 1;
  isset($_GET['kn']) ? $kn = $_GET['kn'] : $kn = "";
  isset($_GET['kx']) ? $kx = $_GET['kx'] : $kx = "";
  isset($_GET['th']) ? $th = $_GET['th'] : $th = "";

  isset($_GET['ngts']) ? $ngts = $_GET['ngts'] : $ngts = "";
  isset($_GET['ngte']) ? $ngte = $_GET['ngte'] : $ngte = "";

  isset($_GET['ngths']) ? $ngths = $_GET['ngths'] : $ngths = "";
  isset($_GET['ngthe']) ? $ngthe = $_GET['ngthe'] : $ngthe = "";

  isset($_GET['nghts']) ? $nghts = $_GET['nghts'] : $nghts = "";
  isset($_GET['nghte']) ? $nghte = $_GET['nghte'] : $nghte = "";

  isset($_GET['input']) ? $ip = $_GET['input'] : $ip = "";

  isset($_GET['dis']) ? $dis = $_GET['dis'] : $dis = 10;


  if (isset($_COOKIE['user']) && $_COOKIE['user'] != "" && $_COOKIE['role'] == 1) {
		$token = $_COOKIE['acc_token'];
		$curl = curl_init();
		$data = array();
		curl_setopt($curl, CURLOPT_POST, 1);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
		curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/user_info_company.php');
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));
	
		$response = curl_exec($curl);
		curl_close($curl);
		$data_tt = json_decode($response, true);
		$tt_user = $data_tt['data']['user_info_result'];
	} else if (isset($_COOKIE['user']) && $_COOKIE['user'] != "" && $_COOKIE['role'] == 2) {
		$token = $_COOKIE['acc_token'];
		$curl = curl_init();
		$data = array();
		curl_setopt($curl, CURLOPT_POST, 1);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
		curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/user_info_employee.php');
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));
		$response = curl_exec($curl);
		curl_close($curl);
		$data_tt = json_decode($response, true);
		$tt_user = $data_tt['data']['user_info_result'];
	}
  $id_cty = $tt_user['com_id'];


  $all_kho_xuat = new db_query("SELECT DISTINCT `kho_id`, `kho_name`
  FROM `kho`
  LEFT JOIN `kho-cho-xu-li`  ON `kcxl_khoXuat` = `kho_id`
  WHERE `kcxl_check` = 1 AND `kcxl_soPhieu` = 'ĐCK' AND `kcxl_id_ct` = $id_cty
  ORDER BY `kho`.`kho_id` DESC
  ");
  $all_kho_nhap = new db_query("SELECT DISTINCT `kho_id`, `kho_name`
  FROM `kho`
  LEFT JOIN `kho-cho-xu-li`  ON `kcxl_khoNhap` = `kho_id`
  WHERE `kcxl_check` = 1 AND `kcxl_soPhieu` = 'ĐCK' AND `kcxl_id_ct` = $id_cty
  ORDER BY `kho`.`kho_id` DESC
  ");
   
   if(isset($_COOKIE['acc_token']) && isset($_COOKIE['rf_token']) && isset($_COOKIE['role']) && $_COOKIE['role'] == 2){
    $curl = curl_init();
    $token = $_COOKIE['acc_token'];
    curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/list_all_my_partner.php?get_all=true');
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer '.$token));
    $response = curl_exec($curl);
    curl_close($curl);

    $data_list = json_decode($response,true);
    $data_list_nv =$data_list['data']['items'];
}else{
    $curl = curl_init();
    $token = $_COOKIE['acc_token'];
    curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/list_all_employee_of_company.php');
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer '.$token));
    $response = curl_exec($curl);
    curl_close($curl);

    $data_list = json_decode($response,true);
    $data_list_nv =$data_list['data']['items'];

}

$count = count($data_list_nv);

$user = [];
for ($i = 0; $i < count($data_list_nv); $i++){
    $item = $data_list_nv[$i];
    $user[$item["ep_id"]] = $item;
}


?>
<!DOCTYPE html>
<html lang="vi">

<head>
  <title>Điều chuyển kho</title>
  <meta charset="UTF-8">
  <meta name="robots" content="noindex,nofollow" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" type="text/css" href="../css/select2.min.css?v=<?= $ver ?>">
  <link rel="stylesheet" type="text/css" href="../css/style.css?v=<?= $ver ?>">
  <link rel="stylesheet" type="text/css" href="../css/style_h.css?v=<?= $ver ?>">
  <link rel="stylesheet" type="text/css" href="../css/style_n.css?v=<?= $ver ?>">
  <link rel="stylesheet" type="text/css" href="../css/style_k.css?v=<?= $ver ?>">
</head>

<body>

  <div class="box_right warehouse_transfer_main" style="display: flex;">
    <div class="box_right_ct">
      <?php include("../includes/sidebar.php"); ?>
      <div class="block_change block_wh_tf">
        <div class="head_wh d_flex space_b align_c">
          <div class="head_tab d_flex space_b align_c">
            <div class="icon_header open_sidebar_w">
              <span class="icon_header_tbl"></span>
              <span class="icon_header_tbl"></span>
              <span class="icon_header_tbl"></span>
            </div>
            <?php include("../includes/header.php") ; ?>
          </div>
          <p class="color_grey font_s14 line_h17 font_w400">Điều chuyển kho
          </p>
          <?php include("../includes/header.php"); ?>
        </div>
        <div class="filter_dtl">
          <div class="t_fil d_flex flex_w">
            <div class="all_ex position_r">
              <select class="select_all_ex" name="all_ex" style="width: 100%">
                <option value="">Tất cả các kho xuất</option>
                <?php while($row_all_kho_xuat = mysql_fetch_assoc($all_kho_xuat->result)) {?>
                  <option value="<?= $row_all_kho_xuat['kho_id']; ?>" <?= ($kx == $row_all_kho_xuat['kho_id']) ? "selected" : "" ?>><?= $row_all_kho_xuat['kho_name']; ?></option>
                <? } ?>
              </select>
            </div>
            <div class="all_in position_r">
              <select class="select_all_in" name="all_in" style="width: 100%">
              <option value="">Tất cả các kho nhập</option>
              <?php while($row_all_kho_nhap = mysql_fetch_assoc($all_kho_nhap->result)) {?>
                <option value="<?= $row_all_kho_nhap['kho_id']; ?>" <?= ($kn == $row_all_kho_nhap['kho_id']) ? "selected" : "" ?>><?= $row_all_kho_nhap['kho_name']; ?></option>
              <? } ?>
              </select>
            </div>
            <div class="date_cr d_flex space_b">
              <p class="color_grey font_s14 line_h17 font_w400">Ngày tạo: <span class="date_start" id="date_cr_start" data="<?= $ngts ?>"><?= ($ngts != "") ?  $ngts : "yyyy/mm/dd" ?></span> -
                <span class="date_end" id="date_cr_end" data="<?= $ngte ?>"><?= ($ngte != "") ?  $ngte : "yyyy/mm/dd" ?></span>
              </p>
              <img class="cursor_p" src="../images/date.png" alt="">
            </div>
            <div class="all_st_tf position_r">
              <select class="select_st_tf" name="all_st_tf" style="width: 100%">
                <option value="">Tất cả trạng thái phiếu điều chuyển</option>
                <option value="1" <?= ($th == 1) ? "selected" : "" ?>>Chờ duyệt</option>
                <option value="2" <?= ($th == 2) ? "selected" : "" ?>>Từ chối</option>
                <option value="7" <?= ($th == 7) ? "selected" : "" ?>>Đã duyệt</option>
              </select>
            </div>
            <div class="date_pf d_flex space_b">
              <p class="color_grey font_s14 line_h17 font_w400">Ngày thực hiện: <span class="date_pf_start" id="date_pf_start" data="<?= $ngths ?>"><?= ($ngths != "") ?  $ngths : "yyyy/mm/dd" ?></span>
                - <span class="date_end" id="date_pf_end" data="<?= $ngthe ?>"><?= ($ngthe != "") ?  $ngthe : "yyyy/mm/dd" ?></span></p>
              <img class="cursor_p" src="../images/date.png" alt="">
            </div>
            <div class="date_rq d_flex space_b">
              <p class="color_grey font_s14 line_h17 font_w400">Ngày yêu cầu hoàn thành: <span class="date_rq_start" id="date_rq_start" data="<?= $nghts ?>"><?= ($nghts != "") ?  $nghts : "yyyy/mm/dd" ?></span> 
              - <span class="date_end" id="date_rq_end" data="<?= $nghte ?>"><?= ($nghte != "") ?  $nghte : "yyyy/mm/dd" ?></span></p>
              <img class="cursor_p" src="../images/date.png" alt="">
            </div>
          </div>
        </div>
        <div class="operation_wh d_flex space_b" style="display: flex;">
          <div class="search_wh d_flex space_b" style="display: block;">
            <div class="input_sr_wh">
              <div class="box_input_sr position_r">
                <input type="text" name= "input_search" value="<?= ($ip != "") ? $ip : "" ?>" placeholder="Tìm kiếm theo số phiếu">
                <span class="icon_sr_wh"></span>
              </div>
            </div>
          </div>
          <div class="export_wh d_flex space_b align_c">
            <?php if(in_array(2,$ro_dc_kho)){ ?>
              <button class="btn_px d_flex align_c">
                <a href="/dieu-chuyen-kho-them-moi.html"></a>
                <img src="../images/img_px.png" alt="">
                <p class="color_white font_s15 line_h18 font_w500">Thêm mới</p>
              </button>
            <?php }?>
            <button class="btn_ex d_flex align_c cursor_p">
              <img src="../images/export.png" alt="">
              <p class="color_white font_s15 line_h18 font_w500">Xuất excel</p>
            </button>
            <div class="hd_ex d_flex align_c cursor_p">
              <img src="../images/img_hd.png" alt="">
              <p class="color_blue font_s15 line_h18 font_w500">Hướng dẫn</p>
            </div>
          </div>
        </div>
        <div class="detail_wh" data-page="<?= $page ?>"  
        data-kn="<?= $kn ?>" data-kx="<?= $kx ?>" data-th="<?= $th?>" 
        data-ngts="<?= $ngts?>" data-ngte="<?= $ngte?>" data-ngths="<?= $ngths?>" data-ngthe="<?= $ngthe?>"
        data-nghts="<?= $nghts?>" data-nghte="<?= $nghte?>" data-ip ="<?= $ip ?>" data-dis ="<?= $dis ?>">
          <!--  -->
        </div>
      </div>
    </div>
  </div>
  <?php include("../includes/popup_dieu-chuyen-kho.php"); ?>
  <?php include("../includes/popup_chon_ngay.php"); ?>

</body>
<script type="text/javascript" src="../js/jquery-3.4.1.min.js"></script>
<script type="text/javascript" src="../js/jquery.validate.min.js"></script>
<script type="text/javascript" src="../js/select2.min.js"></script>
<script type="text/javascript" src="../js/js_h.js"></script>
<script type="text/javascript" src="../js/validate_h.js"></script>
<script type="text/javascript" src="../js/js_k.js"></script>
<script type="text/javascript" src="../js/js_n.js"></script>
<script type="text/javascript" src="../js/dieu_chuyen_kho.js"></script>

</html>