<?php

include("config1.php");

if (!in_array(1, $ro_kk_kho)) {
    header("Location: /tong-quan.html");
}

$id_phieu = getValue('id', 'int', 'GET', '');

$chi_tiet_phieuKK = new db_query("SELECT *, `kho_name` FROM `kho-cho-xu-li` 
    LEFT JOIN `kho` ON `kcxl_khoThucHienKiemKe` = `kho_id`
    WHERE `kcxl_soPhieu` = 'PKK' AND `kcxl_id` = $id_phieu  AND `kcxl_check`='1'
    ORDER BY `kcxl_id` ASC
    ");
$row = mysql_fetch_assoc($chi_tiet_phieuKK->result);

if (isset($_COOKIE['acc_token']) && isset($_COOKIE['rf_token']) && isset($_COOKIE['role']) && $_COOKIE['role'] == 2) {
    $curl = curl_init();
    $token = $_COOKIE['acc_token'];
    curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/list_all_my_partner.php?get_all=true');
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));
    $response = curl_exec($curl);
    curl_close($curl);

    $data_list = json_decode($response, true);
    $data_list_nv = $data_list['data']['items'];
} else {
    $curl = curl_init();
    $token = $_COOKIE['acc_token'];
    curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/list_all_employee_of_company.php');
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));
    $response = curl_exec($curl);
    curl_close($curl);

    $data_list = json_decode($response, true);
    $data_list_nv = $data_list['data']['items'];
}
$count = count($data_list_nv);

$user = [];
for ($i = 0; $i < count($data_list_nv); $i++) {
    $nv = $data_list_nv[$i];
    $user[$nv["ep_id"]] = $nv;
}

if (isset($_COOKIE['user']) && $_COOKIE['user'] != "" && $_COOKIE['role'] == 1) {
    $token = $_COOKIE['acc_token'];
    $curl = curl_init();
    $data = array();
    curl_setopt($curl, CURLOPT_POST, 1);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
    curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/user_info_company.php');
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));

    $response = curl_exec($curl);
    curl_close($curl);
    $data_tt = json_decode($response, true);
    $tt_user = $data_tt['data']['user_info_result'];
    $user_id = $_SESSION['com_id'];
}
if (isset($_COOKIE['user']) && $_COOKIE['user'] != "" && $_COOKIE['role'] == 2) {
    $token = $_COOKIE['acc_token'];
    $curl = curl_init();
    $data = array();
    curl_setopt($curl, CURLOPT_POST, 1);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
    curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/user_info_employee.php');
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));
    $response = curl_exec($curl);
    curl_close($curl);
    $data_tt = json_decode($response, true);
    $tt_user = $data_tt['data']['user_info_result'];
    $user_id = $_SESSION['ep_id'];
}
$id_cty = $tt_user['com_id'];
$date = date('Y-m-d', time());
$time = date('h:i', time());
$stt = 1;
// echo "<pre>";
// print_r($time);
// echo "</pre>";
// die();


?>
<!DOCTYPE html>
<html lang="vi">

<head>
    <title>Chi tiết phiếu kiểm kê</title>
    <meta charset="UTF-8">
    <meta name="robots" content="noindex,nofollow" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="../css/style.css?v=<?= $ver ?>">
    <link rel="stylesheet" type="text/css" href="../css/style_h.css?v=<?= $ver ?>">
    <link rel="stylesheet" type="text/css" href="../css/style_n.css?v=<?= $ver ?>">
    <link rel="stylesheet" type="text/css" href="../css/style_k.css?v=<?= $ver ?>">
</head>

<body>

    <div class="box_right inventory_detail1 detail_1">
        <div class="box_right_ct">
            <?php include("../includes/sidebar.php"); ?>
            <div class="header_menu_overview d_flex align_c space_b">
                <p class="color_grey line_16 font_s14 text_link_page" style="display: block;">
                    <a href="/kiem-ke-kho.html" class="cursor_p">
                        <img src="../images/back_item_g.png" alt=""> </a>
                    Kiểm kê kho /chi tiết phiếu kiểm kê
                </p>
                <img class="open_sidebar_w" src="../images/open_sidebar_w.png" alt="" onclick="toggle('main_sidebar')" style="display: none;">
                <?php include('../includes/header.php');  ?>
            </div>
            <div class="block_wh_tf_add">
                <div class="head_wh d_flex space_b align_c" style="display: none;">
                    <div class=" d_flex align_c">
                        <a href="/kiem-ke-kho.html" class="cursor_p">
                            <img src="../images/back_item_g.png" alt="">
                        </a>
                        <p class="color_grey line_16 font_s14 ml_10">
                            Kiểm kê / Chi tiết phiếu kiểm kê
                        </p>
                    </div>
                </div>
                <!-- tùy chọn -->

                <div class="operation_wh d_flex space_b flex_end">
                    <div class="export_wh d_flex space_b align_c">
                        <?php if ($row['kcxl_trangThai'] == 9 && in_array(5, $ro_kk_kho)) { ?>
                            <button class="btn_tc d_flex flex_center align_c">
                                <img src="../images/btn_t.png" alt="">
                                <p class="color_white font_s15 line_h18 font_w500">Từ chối</p>
                            </button>
                        <? } ?>

                        <?php if (($row['kcxl_trangThai'] == 9 || $row['kcxl_trangThai'] == 10) && in_array(5, $ro_kk_kho)) { ?>
                            <button class="btn_dt d_flex flex_center align_c">
                                <img src="../images/check__w.png" alt="">
                                <p class="color_white font_s15 line_h18 font_w500">Duyệt</p>
                            </button>
                        <? } ?>

                        <?php if (($row['kcxl_trangThai'] == 8 && $user_id == $row['kcxl_nguoiThucHien']) || ($row['kcxl_trangThai'] == 8 && $_SESSION['quyen'] == '1') || ($row['kcxl_trangThai'] == 14 && $user_id == $row['kcxl_nguoiThucHien']) || ($row['kcxl_trangThai'] == 14 && $_SESSION['quyen'] == '1')) { ?>
                            <button class="btn_ht d_flex flex_center align_c" style="width:142px;">
                                <img src="../images/check__w.png" alt="">
                                <p class="color_white font_s15 line_h18 font_w500">Hoàn thành</p>
                            </button>
                        <? } ?>
                        <?php if (($row['kcxl_trangThai'] == 8 && $user_id == $row['kcxl_nguoiThucHien']) || ($row['kcxl_trangThai'] == 8 && $_SESSION['quyen'] == '1') || ($row['kcxl_trangThai'] == 14 && $user_id == $row['kcxl_nguoiThucHien']) || ($row['kcxl_trangThai'] == 14 && $_SESSION['quyen'] == '1')) { ?>
                            <button class="btn_kk d_flex flex_center align_c">
                                <a href="kiem-ke-kho-kiem-ke-<?= $id_phieu ?>.html"></a>
                                <img src="../images/edit_w.png" alt="">
                                <p class="color_white font_s15 line_h18 font_w500">Kiểm kê</p>
                            </button>
                        <? } ?>
                        <?php if ($row['kcxl_trangThai'] == 11) { ?>
                            <button class="btn_cn d_flex flex_center align_c" style="background: #2A38A2;margin-right: 15px">
                                <img src="../images/xoay.png" alt="">
                                <p class="color_white font_s15 line_h18 font_w500">Cập nhật kho</p>
                            </button>
                        <? } ?>
                        <!-- onclick="update_trangThai('12')" -->
                        <?php if (in_array(4, $ro_kk_kho)) { ?>
                            <button class="btn_del d_flex flex_center align_c">
                                <p class="color_blue font_s15 line_h18 font_w500">Xóa phiếu</p>
                            </button>
                        <? } ?>
                        <!-- onclick="update_trangThai('13')" -->
                        <button class="btn_ex d_flex flex_center align_c" data="<?= $id_phieu ?>">
                            <img src="../images/export.png" alt="">
                            <p class="color_white font_s15 line_h18 font_w500">Xuất excel</p>
                        </button>

                    </div>
                </div>
                <!-- Thông tin phiếu điều chuyển kho -->

                <div class="info_input_order" style="display: block;">
                    <div class="main_info_ivt_bill">
                        <div class="tit_info_rq back_blue">
                            <p class="color_white font_s16 line_h19 font_w700">Thông tin phiếu kiểm kê</p>
                        </div>
                        <div class="body_info_rq">
                            <div class="list_info_input_order">
                                <div class="item_body_info_rq d_flex align_c">
                                    <p class="tit_it color_grey font_s14 line_h17 font_w400">Số phiếu:</p>
                                    <p class="color_grey font_s14 line_h17 font_w500"><?= phieu($row['kcxl_id'], $row['kcxl_soPhieu']); ?></p>
                                </div>

                                <div class="" style="display: block;">
                                    <div class="item_body_info_rq d_flex align_c fl">
                                        <p class="tit_it color_grey font_s14 line_h17 font_w400">Trạng thái:</p>
                                        <p class="<?= trang_thai_color($row['kcxl_trangThai']); ?> font_s14 line_h17 font_w500"><?= trang_thai($row['kcxl_trangThai']); ?></p>
                                    </div>
                                </div>
                                <?php if ($row['kcxl_trangThai'] == 10) { ?>
                                    <div class="item_ct_info_rq d_flex align_c">
                                        <p class="tit_it color_grey font_s14 line_h17 font_w400">Lí do từ chối:</p>
                                        <p class="color_grey font_s14 line_h17 font_w500"><?= $row['kcxl_liDoTuChoi']; ?></p>
                                    </div>
                                <?php } ?>
                                <div class="item_body_info_rq d_flex align_c">
                                    <p class="tit_it color_grey font_s14 line_h17 font_w400">Người tạo:</p>
                                    <?php
                                    $nguoi_tao = $row['kcxl_nguoiTao'];
                                    $user_id_1 = $user[$nguoi_tao];
                                    if ($nguoi_tao != 0) {
                                        $ten_nguoi_tao = $user_id_1['ep_name'];
                                        $anh_nguoi_tao = $user_id_1['ep_image'];
                                        if ($anh_nguoi_tao == "") {
                                            $anh1 = '../images/ava_ad.png';
                                        } else {
                                            $anh1 = 'https://chamcong.24hpay.vn/upload/employee/' . $anh_nguoi_tao;
                                        }
                                    } else {
                                        $ten_nguoi_tao = $tt_user['com_name'];
                                        $anh1 = 'https://chamcong.24hpay.vn/upload/company/logo/' . $tt_user['com_logo'];
                                    }
                                    ?>
                                    <div class="d_flex flex_start align_c">
                                        <img src="<?= $anh1; ?>" alt="">
                                        <p class="color_grey font_s14 line_h17 font_w500"><?= $ten_nguoi_tao; ?></p>
                                    </div>
                                </div>
                                <div class="item_body_info_rq d_flex align_c">
                                    <p class="tit_it color_grey font_s14 line_h17 font_w400">Ngày tạo:</p>
                                    <p class="color_grey font_s14 line_h17 font_w500"><?= date('d/m/Y', strtotime($row['kcxl_ngayTao'])); ?></p>
                                </div>
                                <div class="item_body_info_rq d_flex align_c">
                                    <p class="tit_it color_grey font_s14 line_h17 font_w400">Kho thực hiện kiểm kê:</p>
                                    <p class="color_grey font_s14 line_h17 font_w500"><?= $row['kho_name']; ?></p>
                                </div>
                                <div class="item_body_info_rq d_flex align_c">
                                    <p class="tit_it color_grey font_s14 line_h17 font_w400">Người thực hiện kiểm kê:</p>
                                    <div class="d_flex">
                                        <?php
                                        $ntt = explode(',', $row['kcxl_nguoiThucHien']);
                                        for ($j = 0; $j < count($ntt); $j++) {
                                            $data = $ntt[$j];
                                            $ten = $user[$data]['ep_name'];
                                            $anh_nguoi_kiemke =  $user[$data]['ep_image'];
                                            if ($anh_nguoi_kiemke == "") {
                                                $anh = '../images/ava_ad.png';
                                            } else {
                                                $anh = 'https://chamcong.24hpay.vn/upload/employee/' . $user[$data]['ep_image'];
                                            }
                                        ?>
                                            <div class="d_flex flex_start align_c mr_15">
                                                <img src="<?= $anh; ?>" alt="">
                                                <p class="color_grey font_s14 line_h17 font_w500"><?= $ten; ?></p>

                                            </div>
                                        <?php
                                        } ?>
                                    </div>
                                </div>
                                <div class="item_body_info_rq d_flex align_c">
                                    <p class="tit_it color_grey font_s14 line_h17 font_w400">Ngày thực hiện kiểm kê:</p>
                                    <p class="color_grey font_s14 line_h17 font_w500"><?= date('d/m/Y', strtotime($row['kcxl_ngayThucHienKiemKe'])); ?></p>
                                </div>
                                <div class="item_body_info_rq d_flex align_c">
                                    <p class="tit_it color_grey font_s14 line_h17 font_w400">Giờ thực hiện kiểm kê:</p>
                                    <p class="color_grey font_s14 line_h17 font_w500"><?= $info_item['kcxl_thoiGianThucHien'] ?></p>
                                </div>
                                <div class="item_body_info_rq d_flex align_c">
                                    <p class="tit_it color_grey font_s14 line_h17 font_w400">Ngày yêu cầu hoàn thành:</p>
                                    <p class="color_grey font_s14 line_h17 font_w500"><?= date('d/m/Y', strtotime($row['kcxl_ngayYeuCauHoanThanh'])); ?></p>
                                </div>
                                <div class="item_body_info_rq d_flex align_c">
                                    <p class="tit_it color_grey font_s14 line_h17 font_w400">Giờ yêu cầu hoàn thành: </p>
                                    <p class="color_grey font_s14 line_h17 font_w500"><?= $info_item['kcxl_thoiGianHoanThanh'] ?></p>
                                </div>
                                <div class="item_ct_info_rq d_flex align_s">
                                    <p class="tit_it color_grey font_s14 line_h17 font_w400">Ghi chú:</p>
                                    <p class="color_grey font_s14 line_h17 font_w500"><?= $row['kcxl_ghi_chu']; ?></p>
                                </div>
                                <?php if ($row['kcxl_trangThai'] == 8 || $row['kcxl_trangThai'] == 10 || $row['kcxl_trangThai'] == 11 || $row['kcxl_trangThai'] == 12 || $row['kcxl_trangThai'] == 14) { ?>
                                    <div class="">
                                        <?php $nguoi_duyet = $row['kcxl_nguoiDuyet']; ?>
                                        <div class="item_body_info_rq d_flex align_c">
                                            <p class="tit_it color_grey font_s14 line_h17 font_w400">Người duyệt:</p>
                                            <div class="d_flex flex_start align_c">
                                                <?php
                                                $nguoi_duyet = $row['kcxl_nguoiDuyet'];
                                                $user_id_duyet = $user[$nguoi_duyet];
                                                if ($nguoi_duyet != 0) {
                                                    $ten_nguoi_duyet = $user_id_duyet['ep_name'];
                                                    $anh_nguoi_duyet = $user_id_duyet['ep_image'];
                                                    if ($anh_nguoi_duyet == "") {
                                                        $anh2 = '../images/ava_ad.png';
                                                    } else {
                                                        $anh2 = 'https://chamcong.24hpay.vn/upload/employee/' . $anh_nguoi_duyet;
                                                    }
                                                } else {
                                                    $ten_nguoi_duyet = $tt_user['com_name'];
                                                    $anh2 = 'https://chamcong.24hpay.vn/upload/company/logo/' . $tt_user['com_logo'];
                                                }
                                                ?>
                                                <div class="d_flex flex_start align_c">
                                                    <img src="<?= $anh2; ?>" alt="">
                                                    <p class="color_grey font_s14 line_h17 font_w500"><?= $ten_nguoi_duyet; ?></p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="item_body_info_rq d_flex align_c">
                                            <p class="tit_it color_grey font_s14 line_h17 font_w400">Ngày duyệt:</p>
                                            <p class="color_grey font_s14 line_h17 font_w500"><?= $row['kcxl_ngayDuyet'] ?></p>
                                        </div>
                                    </div>
                                <? } ?>
                                <? if ($row['kcxl_trangThai'] == 9 || $row['kcxl_trangThai'] == 10 || $row['kcxl_trangThai'] == 11 || $row['kcxl_trangThai'] == 12) { ?>
                                    <div class="">
                                        <div class="item_body_info_rq d_flex align_c">
                                            <p class="tit_it color_grey font_s14 line_h17 font_w400">Người hoàn thành:</p>
                                            <div class="d_flex flex_start align_c">
                                                <?php
                                                $nguoi_hoan_thanh = $row['kcxl_nguoiHoanThanh'];
                                                $user_id_ht = $user[$nguoi_duyet];
                                                if ($nguoi_hoan_thanh != 0) {
                                                    $ten_nguoi_ht = $user_id_ht['ep_name'];
                                                    $anh_nguoi_ht = $user_id_ht['ep_image'];
                                                    if ($anh_nguoi_ht == "") {
                                                        $anh3 = '../images/ava_ad.png';
                                                    } else {
                                                        $anh3 = 'https://chamcong.24hpay.vn/upload/employee/' . $anh_nguoi_ht;
                                                    }
                                                } else {
                                                    $ten_nguoi_ht = $tt_user['com_name'];
                                                    $anh3 = 'https://chamcong.24hpay.vn/upload/company/logo/' . $tt_user['com_logo'];
                                                }
                                                ?>
                                                <div class="d_flex flex_start align_c">
                                                    <img src="<?= $anh3; ?>" alt="">
                                                    <p class="color_grey font_s14 line_h17 font_w500"><?= $ten_nguoi_ht; ?></p>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="item_body_info_rq d_flex align_c">
                                            <p class="tit_it color_grey font_s14 line_h17 font_w400">Ngày hoàn thành:</p>
                                            <p class="color_grey font_s14 line_h17 font_w500"><?= $row['kcxl_ngayHoanThanh'] ?></p>
                                        </div>
                                    </div>
                                <? } ?>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Danh sách vật tư -->

                <p class="tit_table_vt color_blue font_s16 line_h19 font_w700">Danh sách vật tư</p>
                <?php
                $vattu = new db_query("SELECT `kcxl_id`, `kcxl_soPhieu`, `kcxl_trangThai`, `dsvt_id`,`slvt_idPhieu`, `slvt_maPhieu`, 
                    `dsvt_maVatTuThietBi`, `dsvt_name`, `dvt_name`, `hsx_name`, `xx_name`, `slvt_soLuongKiemKe`, `slvt_so_luong_he_thong`,`slvt_slTon`,`slvt_ghiChu`
                    FROM `danh-sach-vat-tu`
                    LEFT JOIN `don-vi-tinh` ON `dsvt_donViTinh` =`dvt_id`
                    LEFT JOIN `hang-san-xuat` ON `dsvt_hangSanXuat` = `hsx_id`
                    LEFT JOIN `xuat-xu` ON `dsvt_xuatXu` = `xx_id`
                    LEFT JOIN `so-luong-vat-tu` ON `dsvt_id` = `slvt_maVatTuThietBi`
                    LEFT JOIN `kho-cho-xu-li` ON `slvt_idPhieu` = `kcxl_id`
                    WHERE `dsvt_check` = '1' AND `slvt_idPhieu` = '$id_phieu' AND `dsvt_id_ct` = '$id_cty'
                    ORDER BY `dsvt_id` ASC ");
                ?>
                <div class="tb_operation_wh position_r d_flex align_c">
                    <div class="table_ds_vt table_vt_scr ">
                        <table style="width: 1879px" class="table_inventory_detail">
                            <tr class="tit_tbl color_white font_s16 line_h19 font_w500 back_blue">
                                <td rowspan="2" style="width:9%">Mã vật tư thiết bị</td>
                                <td rowspan="2">Tên đầy đủ vật tư thiết bị</td>
                                <td rowspan="2">Đơn vị tính</td>
                                <td rowspan="2">Hãng sản xuất</td>
                                <td rowspan="2">Xuất xứ</td>
                                <td rowspan="2">Số lượng trên hệ thống</td>
                                <td rowspan="2">Số lượng kiểm kê</td>
                                <td colspan="2">Chênh lệch </td>
                                <td rowspan="2" style="border:1px solid #CCCCCC;width:24%">Ghi chú</td>
                            </tr>
                            <tr class="tit_tbl color_white font_s16 line_h19 font_w500 back_blue">
                                <td style="border-radius:0%;">Thừa</td>
                                <td style="border-radius:0%;border: 1px solid #CCCCCC;">Thiếu</td>
                            </tr>
                            <? while ($item1 = mysql_fetch_assoc($vattu->result)) { ?>
                                <?php
                                ?>
                                <tr class="color_grey font_s14 line_h17 font_w400" data="<?= $item1['dsvt_id'] ?>">
                                    <td><?= phieu($item1['dsvt_id'], $item1['dsvt_maVatTuThietBi']) ?></td>
                                    <td class="color_blue font_w500" style="text-align: left;"><?= $item1['dsvt_name']; ?></td>
                                    <td><?= $item1['dvt_name']; ?></td>
                                    <td style="text-align: left;"><?= $item1['hsx_name']; ?></td>
                                    <td><?= $item1['xx_name']; ?></td>
                                    <td style="text-align: right;" class="slht"><?= $item1['slvt_so_luong_he_thong']  ?></td>
                                    <td class="font_s14 line_h17 color_grey font_w400 slkk" style="text-align: right;"><?= $item1['slvt_soLuongKiemKe'] ?></td>
                                    <td>
                                        <? if ($item1['slvt_soLuongKiemKe'] > $item1['slvt_so_luong_he_thong']) {
                                            echo ($item1['slvt_soLuongKiemKe'] - $item1['slvt_so_luong_he_thong']) ?>
                                        <? } else if ($item1['slvt_soLuongKiemKe'] < $item1['slvt_so_luong_he_thong']) {
                                            echo ('') ?>
                                        <?php } else if ($item1['slvt_soLuongKiemKe'] = $item1['slvt_so_luong_he_thong']) {
                                            echo ('0') ?>
                                        <?php } ?>
                                    </td>
                                    <td>
                                        <? if ($item1['slvt_soLuongKiemKe'] < $item1['slvt_so_luong_he_thong']) {
                                            echo ($item1['slvt_so_luong_he_thong'] - $item1['slvt_soLuongKiemKe']) ?>
                                        <? } else if ($item1['slvt_soLuongKiemKe'] > $item1['slvt_so_luong_he_thong']) {
                                            echo ('') ?>
                                        <?php } else if ($item1['slvt_soLuongKiemKe'] = $item1['slvt_so_luong_he_thong']) {
                                            echo ('0') ?>
                                        <?php } ?>
                                    </td>
                                    <td><?= $item1['slvt_ghiChu']; ?></td>
                                </tr>
                            <? } ?>
                        </table>
                    </div>
                    <div class="pre_q d_flex align_c flex_center position_a">
                        <span class="pre_arrow"></span>
                    </div>
                    <div class="next_q d_flex align_c flex_center position_a">
                        <span class="next_arrow"></span>
                    </div>
                </div>
            </div>
        </div>
        <?php include("../includes/popup_kiem-ke.php"); ?>
        <?php include('../includes/popup_overview.php');  ?>
    </div>



</body>
<script type="text/javascript" src="../js/jquery-3.4.1.min.js"></script>
<script type="text/javascript" src="../js/jquery.validate.min.js"></script>
<script type="text/javascript" src="../js/select2.min.js"></script>
<script type="text/javascript" src="../js/js_h.js"></script>
<script type="text/javascript" src="../js/validate_h.js"></script>
<!-- <script type="text/javascript" src="../js/js_k.js"></script> -->
<!-- <script type="text/javascript" src="../js/js_n.js"></script> -->
<script>
    $('.active11').each(function() {
        if ($(this).hasClass('active11')) {
            $(this).find('a').addClass('active');
        }
    });

    function thong_ke(id) {
        var sl = Number($(id).val());
        var dg = $(id).parents(".table_3").find('.sl_hethong').text();
        var cal = dg - sl;
        if (cal > 0) {
            $(id).parents(".table_3").find('.sl_thua').text(cal);
            $(id).parents(".table_3").find('.sl_thieu').text(0);
        } else {
            $(id).parents(".table_3").find('.sl_thua').text(0);
            $(id).parents(".table_3").find('.sl_thieu').text(Math.abs(cal));
        }
    };

    function update_trangThai(value) {
        var value = value;
        var id_phieu = $('.popup_comp_kk .btn_save ').attr('data')
        var id_cty = "<?= $id_cty ?>"
        var trangThai = "<?= $row['kcxl_trangThai'] ?>"

        $.ajax({
            url: '../ajax/edit_pkk.php',
            type: 'POST',
            // dataType: 'Json',
            data: {
                id_pkk: id_phieu,
                value: value,
                id_cty: id_cty,
                trangThai: trangThai
            },
            success: function(response) {
                $('#popup_comp_kk').hide();
                $('#popup_add_notif_succ').show();
                var text = $('#popup_add_notif_succ .p_add_succ').text('');
                var text_new = '';
                var name = $("input[name='name_full_dv']").val();
                text_new += 'Duyệt phiếu kiểm kê PKK-';
                text_new += '<strong>';
                text_new += '&nbsp' + id_phieu;
                text_new += '</strong>';
                text_new += '&nbspthành công!';
                text.append(text_new);
            }
        })
    }

    $('.btn_ht').click(function() {
        var id = "<?= $id_phieu ?>"
        console.log(id)
        $('.popup_comp_kk .btn_save ').attr('data', id)
        $('.popup_comp_kk').show();
        $('.content_kt_duyet').text('')
        var text = 'Bạn có chắc chắn muốn hoàn thành kiểm kê<br><strong>PKK-' + id + '</strong>?'
        $('.content_kt_duyet').append(text)
    })

    $('.btn_close').click(function() {
        window.location.reload();
    });

    $('.btn_tc').click(function() {
        $('#popup_deny_kk').show();
        var id = "<?= $id_phieu ?>"
        // console.log(id)
        $('.popup_deny_kk .btn_tuchoi ').attr('data', id)
    })

    $('.btn_tuchoi').click(function() {
        var form_vali = $('#f_deny_ivt');
        form_vali.validate({
            errorPlacement: function(error, element) {
                error.appendTo(element.parents(".vali_li_do"));
                error.wrap("<span class='error'>");
            },
            rules: {
                validate_li_do: "required",
            },
            messages: {
                validate_li_do: "Nhập lí do từ chối."
            }
        });

        if (form_vali.valid() === true) {
            var value = '10';
            var id_phieu = $('.popup_deny_kk .btn_tuchoi ').attr('data')
            var id_cty = "<?= $id_cty ?>"
            var trangThai = "<?= $row['kcxl_trangThai'] ?>"
            var kcxl_liDoTuChoi = $('.validate_li_do').val()
            $.ajax({
                url: '../ajax/edit_pkk.php',
                type: 'POST',
                data: {
                    id_pkk: id_phieu,
                    value: value,
                    id_cty: id_cty,
                    trangThai: trangThai,
                    kcxl_liDoTuChoi: kcxl_liDoTuChoi
                },
                success: function(data) {
                    $('#popup_deny_kk').hide();
                    var text = $('#popup_add_notif_succ .p_add_succ').text('');
                    var text_new = '';
                    var name = $("input[name='name_full_dv']").val();
                    text_new += 'Từ chối phiếu kiểm kê PKK-';
                    text_new += '<strong>';
                    text_new += '&nbsp' + id_phieu;
                    text_new += '</strong>';
                    text_new += '&nbspthành công!';
                    text.append(text_new);
                    $('#popup_add_notif_succ').show();
                }
            });
        }
    })

    $('.btn_dt').click(function() {
        var id = "<?= $id_phieu ?>"
        console.log(id)
        $('.popup_acp_kk .btn_save ').attr('data', id)
        $('.popup_acp_kk').show();
        $('.popup_acp_kk .del_w').text('')
        var text = 'Bạn có chắc chắn muốn duyệt kiểm kê<br><strong>PKK-' + id + '</strong>?'
        $('.popup_acp_kk .del_w').append(text)
    })

    $('.btn_duyet_kk').click(function() {
        var value = '11';
        var id_phieu = $('.popup_acp_kk .btn_save ').attr('data')
        var id_cty = "<?= $id_cty ?>"
        var trangThai = "<?= $row['kcxl_trangThai'] ?>"

        $.ajax({
            url: '../ajax/edit_pkk.php',
            type: 'POST',
            // dataType: 'Json',
            data: {
                id_pkk: id_phieu,
                value: value,
                id_cty: id_cty,
                trangThai: trangThai
            },
            success: function(response) {
                $('#popup_acp_kk').hide();
                $('#popup_add_notif_succ').show();
                var text = $('#popup_add_notif_succ .p_add_succ').text('');
                var text_new = '';
                var name = $("input[name='name_full_dv']").val();
                text_new += 'Duyệt phiếu kiểm kê PKK-';
                text_new += '<strong>';
                text_new += '&nbsp' + id_phieu;
                text_new += '</strong>';
                text_new += '&nbspthành công!';
                text.append(text_new);
            }
        })
    })

    $('.btn_cn').click(function() {
        var id = "<?= $id_phieu ?>"
        console.log(id)
        $('.popup_update_kk .btn_save ').attr('data', id)
        $('.popup_update_kk').show();
        $('.popup_update_kk .del_w').text('')
        var text = 'Bạn có chắc chắn muốn cập nhật kiểm kê<br><strong>PKK-' + id + '</strong>?'
        $('.popup_update_kk .del_w').append(text)
    })

    $('.btn_capnhat_kk').click(function() {
        var value = '12';
        var id_phieu = $('.popup_update_kk .btn_save ').attr('data')
        var id_cty = "<?= $id_cty ?>"
        var trangThai = "<?= $row['kcxl_trangThai'] ?>"
        var listTable = [];
        $(".slkk").each(function() {
            var data = $(this).parents('tr').attr('data')
            var slht = $(this).parents('tr').find('.slht').html();
            var slkk = $(this).html();
            listTable.push({
                'id': data,
                'soluong': slkk,
                'slht': slht,
            });
        });
        var nht = "<?= $user_id ?>"
        var id_kho = "<?= $row['kcxl_khoThucHienKiemKe'] ?>"
        // console.log(listTable)
        $.ajax({
            url: '../ajax/hoan_thanh_pkk.php',
            type: 'POST',
            // dataType: 'Json',
            data: {
                id_pkk: id_phieu,
                value: value,
                trangThai: trangThai,
                kcxl_nguoiHoanThanh: nht,
                listTable: listTable,
                id_kho: id_kho,
            },
            success: function(response) {
                $('#popup_update_kk').hide();
                $('#popup_add_notif_succ').show();
                var text = $('#popup_add_notif_succ .p_add_succ').text('');
                var text_new = '';
                var name = $("input[name='name_full_dv']").val();
                text_new += 'Kiểm kê phiếu PKK-';
                text_new += '<strong>';
                text_new += '&nbsp' + id_phieu;
                text_new += '</strong>';
                text_new += '&nbspthành công!';
                text.append(text_new);
            }
        })
    })

    $('.btn_del').click(function() {
        var id = "<?= $id_phieu ?>"
        console.log(id)
        $('.popup_del_kk .btn_save ').attr('data', id)
        $('.popup_del_kk').show();
        $('.popup_del_kk .del_w').text('')
        var text = 'Bạn có chắc chắn muốn xóa phiếu kiểm kê<br><strong>PKK-' + id + '</strong>?'
        $('.popup_del_kk .del_w').append(text)
    })

    $('.btn_xoa_phieu_kk').click(function() {
        var value = '13';
        var id_phieu = $('.popup_del_kk .btn_save ').attr('data')
        var id_cty = "<?= $id_cty ?>"
        var trangThai = "<?= $row['kcxl_trangThai'] ?>"

        $.ajax({
            url: '../ajax/edit_pkk.php',
            type: 'POST',
            // dataType: 'Json',
            data: {
                id_pkk: id_phieu,
                value: value,
                id_cty: id_cty,
                trangThai: trangThai
            },
            success: function(response) {
                $('#popup_update_kk').hide();
                $('#popup_add_notif_succ_kk').show();
                var text = $('#popup_add_notif_succ_kk .p_add_succ').text('');
                var text_new = '';
                var name = $("input[name='name_full_dv']").val();
                text_new += 'Xóa phiếu kiểm kê PKK-';
                text_new += '<strong>';
                text_new += '&nbsp' + id_phieu;
                text_new += '</strong>';
                text_new += '&nbspthành công!';
                text.append(text_new);
            }
        })
    })

    $('.btn_ex').click(function() {
        var id_phieu = $(this).attr("data");
        window.location.href = '../Excel/chi_tiet_phieu_kiem_ke.php?id=' + id_phieu;
    })
</script>

</html>