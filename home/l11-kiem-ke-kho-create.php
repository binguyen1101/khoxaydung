<?php

include("config1.php");

if (!in_array(2, $ro_kk_kho)) {
    header("Location: /tong-quan.html");
}

if (isset($_COOKIE['user']) && $_COOKIE['user'] != "" && $_COOKIE['role'] == 1) {
    $token = $_COOKIE['acc_token'];
    $curl = curl_init();
    $data = array();
    curl_setopt($curl, CURLOPT_POST, 1);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
    curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/user_info_company.php');
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));

    $response = curl_exec($curl);
    curl_close($curl);
    $data_tt = json_decode($response, true);
    $tt_user = $data_tt['data']['user_info_result'];
}

if (isset($_COOKIE['user']) && $_COOKIE['user'] != "" && $_COOKIE['role'] == 2) {
    $token = $_COOKIE['acc_token'];
    $curl = curl_init();
    $data = array();
    curl_setopt($curl, CURLOPT_POST, 1);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
    curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/user_info_employee.php');
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));
    $response = curl_exec($curl);
    curl_close($curl);
    $data_tt = json_decode($response, true);
    $tt_user = $data_tt['data']['user_info_result'];
}

if (isset($_SESSION['quyen']) && $_SESSION['quyen'] == 1) {
    $curl = curl_init();
    $token = $_COOKIE['acc_token'];
    curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/list_all_employee_of_company.php');
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));
    $response = curl_exec($curl);
    curl_close($curl);

    $data_list = json_decode($response, true);

    $data_list_nv = $data_list['data']['items'];
    $count = count($data_list_nv);
} elseif (isset($_SESSION['quyen']) && ($_SESSION['quyen'] == 2)) {
    $curl = curl_init();
    $token = $_COOKIE['acc_token'];
    curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/list_all_my_partner.php?get_all=true');
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));
    $response = curl_exec($curl);
    curl_close($curl);
    $data_list = json_decode($response, true);
    $data_list_nv = $data_list['data']['items'];
    $count = count($data_list_nv);
}

$newArr = [];
for ($i = 0; $i < count($data_list_nv); $i++) {
    $value = $data_list_nv[$i];
    $newArr[$value["ep_id"]] = $value;
}

$id_cty = $tt_user['com_id'];
$user_id = $_SESSION['ep_id'];
$date = date('Y-m-d', time());

$kho = new db_query("SELECT `kho_id`, `kho_maKho`, `kho_name` FROM `kho` WHERE `kho_id_ct` = $id_cty");

?>
<!DOCTYPE html>
<html lang="vi">

<head>
    <title>Thêm mới phiếu kiểm kê</title>
    <meta charset="UTF-8">
    <meta name="robots" content="noindex,nofollow" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="../css/style.css?v=<?= $ver ?>">
    <link rel="stylesheet" type="text/css" href="../css/select2.min.css?v=<?= $ver ?>">
    <link rel="stylesheet" type="text/css" href="../css/style_h.css?v=<?= $ver ?>">
    <link rel="stylesheet" type="text/css" href="../css/style_n.css?v=<?= $ver ?>">
    <link rel="stylesheet" type="text/css" href="../css/style_k.css?v<=<?= $ver ?>">
</head>

<body class="seclec2_radius">
    <div class="main_wrapper_all">
        <div class="wapper_all">
            <?php include('../includes/sidebar.php');  ?>
        </div>
        <div class="main_overview" id="main_overview">
            <div class="header_menu_overview d_flex align_c space_b">
                <p class="color_grey line_16 font_s14 text_link_page" style="display: block;">
                    <a href="/kiem-ke-kho.html" class="cursor_p">
                        <img src="../images/back_item_g.png" alt="">
                    </a>Kiểm kê /thêm mới
                </p>
                <img class="open_sidebar_w" src="../images/open_sidebar_w.png" alt="" onclick="toggle('main_sidebar')" style="display: none;">
                <?php include('../includes/header.php');  ?>
            </div>
            <div class="main_ivt_create">
                <div class="head_wh d_flex space_b align_c title_wh" style="display: none;">
                    <div class=" d_flex align_c ">
                        <a href="/kiem-ke-kho.html" class="cursor_p">
                            <img src="../images/back_item_g.png" alt="">
                        </a>
                        <p class="color_grey line_16 font_s14 ml_10">
                            Kiểm kê / Thêm mới
                        </p>
                    </div>
                </div>
                <!-- thông tin phiếu kiểm kê -->
                <form method="POST" action="" id="f_ivt_cre">
                    <div class="body_inventory_create">
                        <div class="header_body_b color_white line_h19 font_s16 font_wB">
                            Thêm mới phiếu kiểm kê
                        </div>
                        <div class="body_input_infor_vote">
                            <div class="input_infor_vote">
                                <div class="box_add_ex_wh">
                                    <div class="d_flex align_c space_b wh768">
                                        <div class="width_create d_flex flex_column mb_15">
                                            <p class="color_grey font_s15 line_h18 font_w500 mb_5">Số phiếu</p>
                                            <input class="color_grey font_s14 line_h17 font_w400" placeholder="Hệ thống tự thiết lập" type="text" disabled="disabled" readonly>
                                        </div>
                                        <div class="width_create select_status d_flex space_b">
                                            <div class="d_flex flex_column">
                                                <p class="color_grey font_s15 line_h18 font_w500 mb_5">Trạng thái</p>
                                                <select name="" id="" class="select_stt color_grey font_s14 line_h17 font_w400" style="width: 100%;">
                                                    <option value="1">Khởi tạo</option>
                                                    <option value="2">Hoàn thành</option>
                                                </select>
                                            </div>
                                            <div class="input_date_fn d_flex flex_column">
                                                <p class="color_grey font_s15 line_h18 font_w500 mb_5">Ngày hoàn thành<span style="color: red;">*</span></p>
                                                <input type="date" name="input_date_fn">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="d_flex align_c space_b wh768">
                                        <div class="width_create d_flex flex_column mb_15">
                                            <p class="color_grey font_s15 line_h18 font_w500 mb_5">Người tạo</p>
                                            <? if ($_COOKIE['role'] == 2) { ?>
                                                <input class="nguoi_tao color_grey font_s14 line_h17 font_w400" type="text" value="<?= $newArr[$user_id]['ep_name']; ?>" data="<?= $newArr[$user_id]['ep_id']; ?>" disabled="disabled">
                                            <? } ?>
                                            <? if ($_COOKIE['role'] == 1) { ?>
                                                <input class="nguoi_tao color_grey font_s14 line_h17 font_w400" type="text" value="<?= $tt_user['com_name']; ?>" data="0" disabled="disabled">
                                            <? } ?>
                                        </div>
                                        <div class="width_create d_flex flex_column mb_15">
                                            <p class="color_grey font_s15 line_h18 font_w500 mb_5">Ngày tạo</p>
                                            <input class="ngay_tao color_grey font_s14 line_h17 font_w400" type="date" value="<?= $date ?>" disabled="disabled">
                                        </div>
                                    </div>
                                    <div class="d_flex align_s space_b wh768">
                                        <div class="box_wh_ex width_create d_flex flex_column mb_15">
                                            <p class="color_grey font_s15 line_h18 font_w500 mb_5">Kho thực hiện kiểm kê<span style="color: red;">*</span></p>
                                            <select name="select_wh_ex" id="" class="select_wh_ex color_grey font_s14 line_h17 font_w400" style="width: 100%;">
                                                <option value=""></option>
                                                <?php while ($row_kho = mysql_fetch_assoc($kho->result)) { ?>
                                                    <option value="<?= $row_kho['kho_id']; ?>"><?= $row_kho['kho_name']; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                        <div class="box_wh_in width_create d_flex flex_column mb_15">
                                            <div class="d_flex align_c space_b ">
                                                <div class="d_flex align_c">
                                                    <p class="font_s15 line_h18 font_w500 color_grey mb_5">Người thực hiện kiểm kê</p>
                                                    <span class="color_red alert_red">*</span>
                                                </div>
                                                <a class="color_blue font_s15 line_h18 font_w400 d_flex align_c mb_5" href="#">
                                                    <span class="font_s24">+</span>
                                                    Thêm nhân viên
                                                </a>
                                            </div>
                                            <select name="ivt_person" class="font_s14 line_h16 color_grey font_w400 inventory_men" multiple="multiple" style="width:100%;">
                                                <option></option>
                                                <?php
                                                for ($j = 0; $j < count($data_list_nv); $j++) {
                                                ?>
                                                    <option value="<?= $data_list_nv[$j]['ep_id'] ?>"><?= $data_list_nv[$j]['ep_name'] . "&nbsp-&nbsp" . $data_list_nv[$j]['dep_name'] ?></option>
                                                <?php
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="d_flex align_c space_b wh768">
                                        <div class="width_create d_flex flex_column mb_15">
                                            <div class="d_flex space_b">
                                                <div class="d_flex flex_column" style="width:50%;">
                                                    <p class="font_s15 line_h18 font_w500 color_grey mb_5">Ngày thực hiện kiểm kê</p>
                                                    <input class="ngay_thuc_hien font_14 line_h16 " type="date" placeholder="Chọn ngày">
                                                </div>
                                                <div class="d_flex flex_column" style="width:50%;padding:0px 0px 0px 10px;">
                                                    <div class="d_flex">
                                                        <p class="font_s15 line_h18 font_w500 color_grey mb_5">Giờ thực hiện kiểm kê</p>
                                                    </div>
                                                    <input class="gio_thuc_hien font_14 line_h16" type="time" placeholder="Chọn giờ">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="width_create d_flex flex_column mb_15">
                                            <div class="d_flex space_b int_kk_cre">
                                                <div class="d_flex flex_column" style="width:50%;">
                                                    <p class="font_s15 line_h18 font_w500 color_grey mb_5">Ngày yêu cầu hoàn thành</p>
                                                    <input class="ngay_yc_hoan_thanh font_14 line_h16 " type="date" placeholder="Chọn ngày">
                                                </div>
                                                <div class="d_flex flex_column" style="width:50%;padding:0px 0px 0px 10px;">
                                                    <div class="d_flex">
                                                        <p class="font_s15 line_h18 font_w500 color_grey mb_5">Giờ yêu cầu hoàn thành</p>
                                                    </div>
                                                    <input class="gio_yc_hoan_thanh font_14 line_h16 " type="time" placeholder="Chọn giờ">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="flex_column d_flex note_import_vote_infor">
                                <p class="font_s15 line_h18 font_w500 color_grey mb_5">Ghi chú</p>
                                <textarea name="ghi_chu" id rows="5" placeholder="Nhập nội dung"></textarea>
                            </div>
                        </div>
                    </div>
                    <!-- bảng danh sách vật tư -->
                    <p class="tit_table_vt color_blue font_s16 line_h19 font_w700"> Danh sách vật tư</p>
                    <!-- bảng 1 -->
                    <div class="position_r align_c d_flex table_kt">
                        <div class="table_vt_scr" onscroll="table_scroll(this)">
                            <table class="table_vt">
                                <tr class="tit_tbl color_white font_s16 line_h19 font_w500 back_blue">
                                    <th style="width:5.3%;">
                                        <span class="span_thread"></span>
                                    </th>
                                    <th>Mã vật tư
                                        <span class="span_thread"></span>
                                    </th>
                                    <th>Tên đầy đủ vật tư thiết bị
                                        <span class="span_thread"></span>
                                    </th>
                                    <th>Đơn vị tính
                                        <span class="span_thread"></span>
                                    </th>
                                    <th> Hãng sản xuất
                                        <span class="span_thread"></span>
                                    </th>
                                    <th> Xuất xứ
                                        <span class="span_thread"></span>
                                    </th>
                                    <th style="width:18.1%;"> Số lượng trên hệ thống
                                    </th>

                                </tr>
                                <tbody id="tbody_add"></tbody>
                                <tr class="tr_add color_blue font_s14 line_h17 font_w500">
                                    <td colspan="2">
                                        <div class="btn_add_vt d_flex align_c cursor_p">
                                            <p class="add_vt d_flex flex_center align_c position_r"><span class="color_white font_s20 position_a">+</span></p>
                                            <p class="">Thêm vật tư</p>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="pre_q d_flex align_c flex_center position_a display_none" onclick="pre_q(this)">
                            <span class="pre_arrow"></span>
                        </div>
                        <div class="next_q d_flex align_c flex_center position_a" onclick="next_q(this)">
                            <span class="next_arrow"></span>
                        </div>
                    </div>
                    <!-- bảng 2 -->
                    <div class="tb_operation_wh position_r d_flex align_c table_ht" style="display:none;">
                        <div class="table_vt_scr" onscroll="table_scroll(this)">
                            <div class="table_ds_vt">
                                <table style="width: 1940px" class="table_inventory_detail">
                                    <tr class="tit_tbl color_white font_s16 line_h19 font_w500 back_blue">
                                        <td rowspan="2" style="width:3%;"></td>
                                        <td rowspan="2" style="width:7.9%;">Mã vật tư thiết bị</td>
                                        <td rowspan="2" style="width:19%">Tên đầy đủ vật tư thiết bị</td>
                                        <td rowspan="2" style="width:5.5%">Đơn vị tính</td>
                                        <td rowspan="2" style="width:7.6%">Hãng sản xuất</td>
                                        <td rowspan="2" style="width:5.3%">Xuất xứ</td>
                                        <td rowspan="2" style="width:10%">Số lượng trên hệ thống</td>
                                        <td rowspan="2" style="width:8%">Số lượng kiểm kê</td>
                                        <td colspan="2" style="width:10.3%">Chênh lệch </td>
                                        <td rowspan="2">Ghi chú</td>
                                    </tr>
                                    <tr class="tit_tbl color_white font_s16 line_h19 font_w500 back_blue">
                                        <td style="border-radius:0%;">Thừa</td>
                                        <td style="border-radius:0%;border: 1px solid #CCCCCC;">Thiếu</td>
                                    </tr>
                                    <tbody id="tbody_add2"></tbody>
                                    <tr class="tr_add color_blue font_s14 line_h17 font_w500">
                                        <td colspan="2">
                                            <div class="btn_add_vt2 d_flex align_c cursor_p">
                                                <p class="add_vt d_flex flex_center align_c position_r"><span class="color_white font_s20 position_a">+</span></p>
                                                <p class="">Thêm vật tư</p>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="pre_q d_flex align_c flex_center position_a display_none" onclick="pre_q(this)">
                            <span class="pre_arrow"></span>
                        </div>
                        <div class="next_q d_flex align_c flex_center position_a" onclick="next_q(this)">
                            <span class="next_arrow"></span>
                        </div>
                    </div>
                    <!-- button -->
                    <div class="btn_cf_rq d_flex flex_center" style="display: flex;">
                        <button type="button" class="btn_cancel back_w color_blue font_s15 line_h18 font_w500"><a href="kiem-ke-kho.html"></a> Hủy</button>
                        <button type="button" class="btn_save back_blue color_white font_s15 line_h18 font_w500">Lưu</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <?php include('../includes/popup_dieu-chuyen-kho.php');  ?>
    <?php include('../includes/popup_overview.php');  ?>
</body>

<script type="text/javascript" src="../js/jquery-3.4.1.min.js"></script>
<script type="text/javascript" src="../js/jquery.validate.min.js"></script>
<script type="text/javascript" src="../js/select2.min.js"></script>
<script type="text/javascript" src="../js/validate_h.js"></script>
<script type="text/javascript" src="../js/reset_validate.js"></script>
<script type="text/javascript" src="../js/js_h.js"></script>
<script type="text/javascript" src="../js/js_n.js"></script>
<script type="text/javascript" src="../js/js_k.js"></script>
<script>
    $('.active11').each(function() {
        if ($(this).hasClass('active11')) {
            $(this).find('a').addClass('active');
        }
    });

    $('.select_stt').change(function() {
        var val_tt = $(this).val();
        if (val_tt == "1") {
            $('.table_kt').show();
            $('.table_ht').hide();
        } else {
            $('.table_ht').show();
            $('.table_kt').hide();
        }

    });

    // var data = <?= json_encode($newArr) ?>;

    $('.select_wh_ex').change(function() {
        $('.delete_3').remove()
        var value = $(this).val()
        var trang_thai = $('.select_stt').val();
        var id_cty = $('.nguoi_tao').attr('data');
        $.ajax({
            url: '../ajax/get_dsvt_kho_kiem_ke.php',
            type: 'POST',
            dataType: 'Json',
            data: {
                trang_thai: trang_thai,
                id_kho: value,
                id_cty: id_cty
            },
            success: function(response) {
                // console.log(response)
            }
        })
    });

    $('.btn_add_vt').click(function() {
        var value = $('.select_wh_ex').val()
        var trang_thai = $('.select_stt').val();
        var id_cty = "<?= $id_cty?>";
        $.ajax({
            url: '../ajax/get_dsvt_kho_kiem_ke.php',
            type: 'POST',
            data: {
                trang_thai: trang_thai,
                id_kho: value,
                id_cty: id_cty
            },
            success: function(data) {
                if (data != 'jkkjk') {
                    $("#tbody_add").append(data);
                } else {
                    $("#tbody_add").append();
                }
            }
        })
    });

    $('.btn_add_vt2').click(function() {
        var value = $('.select_wh_ex').val()
        var trang_thai = $('.select_stt').val();
        var id_cty = "<?= $id_cty?>";   
        $.ajax({
            url: '../ajax/get_dsvt_kho_kiem_ke.php',
            type: 'POST',
            data: {
                trang_thai: trang_thai,
                id_kho: value,
                id_cty: id_cty
            },
            success: function(data) {
                if (data != 'jkkjk') {
                    $("#tbody_add2").append(data);
                } else {
                    $("#tbody_add2").append();
                }
            }
        })
    });

    $('.btn_save').click(function() {
        var form_vali = $("#f_ivt_cre");
        var trang_thai = $('.select_stt').val();
        var ngay_hoan_thanh = $("input[name='input_date_fn']").val();
        var nguoi_tao = $('.nguoi_tao').attr('data');
        var ngay_tao = $('.ngay_tao').val();
        var kho = $('.select_wh_ex').val();
        var nguoi_thuc_hien = $(".inventory_men").val();
        var ngay_thuc_hien = $(".ngay_thuc_hien").val();
        var gio_thuc_hien = $(".gio_thuc_hien").val();
        var ngay_yc_hoan_thanh = $(".ngay_yc_hoan_thanh").val();
        var gio_yc_hoan_thanh = $(".gio_yc_hoan_thanh").val();
        var ghi_chu = $("textarea[name='ghi_chu']").val();
        var id_cty = "<?= $id_cty?>";

        resetFormValidator(form_vali);

        if (trang_thai === "1") {
            var vat_tu = [];
            $(".select_tb").each(function() {
                var id_vattu = $(this).val();
                var soluong = $(this).parent().parent().find('.sltht').text();
                if (id_vattu != "") {
                    vat_tu.push({
                        'id': id_vattu,
                        'soluong': soluong
                    });
                }
            });

            form_vali.validate({
                errorPlacement: function(error, element) {
                    error.appendTo(element.parents(".box_wh_in"));
                    error.appendTo(element.parents(".box_wh_ex"));
                    error.wrap("<span class='error'>");
                },
                rules: {
                    ivt_person: "required",
                    select_wh_ex: "required",
                },
                messages: {
                    ivt_person: "Chọn người kiểm kê.",
                    select_wh_ex: "Chọn kho.",
                }
            });

            if (form_vali.valid() === true && vat_tu.length > 0) {
                $.ajax({
                    url: '../ajax/add_kiem_ke_kho.php',
                    type: 'POST',
                    data: {
                        trang_thai: trang_thai,
                        nguoi_tao: nguoi_tao,
                        ngay_tao: ngay_tao,
                        kho: kho,
                        nguoi_thuc_hien: nguoi_thuc_hien,
                        ngay_thuc_hien: ngay_thuc_hien,
                        gio_thuc_hien: gio_thuc_hien,
                        ngay_yc_hoan_thanh: ngay_yc_hoan_thanh,
                        gio_yc_hoan_thanh: gio_yc_hoan_thanh,
                        ghi_chu: ghi_chu,
                        id_cty: id_cty,
                        vat_tu: vat_tu,
                    },
                    success: function(data) {
                        $('.popup_add_notif_succ').show();
                        var text = $('#popup_add_notif_succ .p_add_succ').text('');
                        var text_new = '';
                        text_new += 'Thêm mới phiếu kiểm kê kho';
                        text_new += '<strong>';
                        text_new += '</strong>';
                        text_new += '&nbspthành công!';
                        text.append(text_new);
                    }
                });
            } else {
                alert("Vui lòng điền đầy đủ thông tin!");
            }
        }
        if (trang_thai === "2") {
            var vat_tu = []
            $(".select_tb").each(function() {
                var id_vattu = $(this).val();
                var ghichu = $(this).parent().parent().find('.nhap_ghi_chu').val();
                var soluong = $(this).parent().parent().find('.nhap_sl_3').val();
                var slht = $(this).parent().parent().find('.sl_hethong').val();
                if (id_vattu != "") {
                    vat_tu.push({
                        'id': id_vattu,
                        'soluong': soluong,
                        'ghichu': ghichu,
                        'slht': slht,
                    });
                }
            });

            var check_empty_arr = 0;
            for (var i = 0; i < vat_tu.length; i++) {
                var check_empty = vat_tu[i].soluong;
                if (check_empty == "") {
                    check_empty_arr++;
                }
            }

            form_vali.validate({
                errorPlacement: function(error, element) {
                    error.appendTo(element.parents(".box_wh_in"));
                    error.appendTo(element.parents(".box_wh_ex"));
                    error.appendTo(element.parents(".input_date_fn"));
                    error.wrap("<span class='error'>");
                },
                rules: {
                    ivt_person: "required",
                    select_wh_ex: "required",
                    input_date_fn: "required",
                },
                messages: {
                    ivt_person: "Chọn người kiểm kê.",
                    select_wh_ex: "Chọn kho.",
                    input_date_fn: "Chọn ngày hoàn thành.",
                }
            });

            if (form_vali.valid() === true && check_empty_arr === 0 && vat_tu.length > 0) {
                $.ajax({
                    url: '../ajax/add_kiem_ke_kho.php',
                    type: 'POST',
                    data: {
                        trang_thai: trang_thai,
                        ngay_hoan_thanh: ngay_hoan_thanh,
                        nguoi_tao: nguoi_tao,
                        ngay_tao: ngay_tao,
                        kho: kho,
                        nguoi_thuc_hien: nguoi_thuc_hien,
                        ngay_thuc_hien: ngay_thuc_hien,
                        gio_thuc_hien: gio_thuc_hien,
                        ngay_yc_hoan_thanh: ngay_yc_hoan_thanh,
                        gio_yc_hoan_thanh: gio_yc_hoan_thanh,
                        ghi_chu: ghi_chu,
                        id_cty: id_cty,
                        vat_tu: vat_tu,
                        role: role,
                    },
                    success: function(data) {
                        $('.popup_add_notif_succ').show();
                        var text = $('#popup_add_notif_succ .p_add_succ').text('');
                        var text_new = '';
                        text_new += 'Thêm mới phiếu kiểm kê kho';
                        text_new += '<strong>';
                        text_new += '</strong>';
                        text_new += '&nbspthành công!';
                        text.append(text_new);
                    }
                });
            } else {
                alert("Vui lòng điền đầy đủ thông tin!");
            }
        }
    });

    $('.btn_close').click(function() {
        window.location.href = "/kiem-ke-kho.html"
    });


    function resetFormValidator(formId) {
        $(formId).removeData('validator');
        $(formId).removeData('unobtrusiveValidation');
        $.validator.unobtrusive.parse(formId);
    }

    function thong_ke(id) {
        var sl = Number($(id).val());
        var dg = $(id).parents(".table_3").find('.sl_hethong').text();
        var cal = dg - sl;
        if (cal > 0) {
            $(id).parents(".table_3").find('.sl_thua').text(0);
            $(id).parents(".table_3").find('.sl_thieu').text(cal);
        } else {
            $(id).parents(".table_3").find('.sl_thua').text(Math.abs(cal));
            $(id).parents(".table_3").find('.sl_thieu').text(0);
        }
        if ($(id).val() == "") {
            $(id).parents(".table_3").find('.sl_thua').text(0);
            $(id).parents(".table_3").find('.sl_thieu').text(0);
        }
    };

    function change_value() {
        $(".select_tb").change(function() {
            var value = $(this).val();
            var _this = $(this);
            var trang_thai = $('.select_stt').val();
            var value1 = $('.select_wh_ex').val()
            var id_cty = "<?= $id_cty?>";
            $.ajax({
                url: '../ajax/get_dsvt_kho_kiem_ke.php',
                type: 'POST',
                data: {
                    id_vt: value,
                    trang_thai: trang_thai,
                    id_kho: value1,
                    id_cty: id_cty
                },
                success: function(data) {
                    _this.parent().parent().html(data);
                }
            });
        });
    }
</script>

</html>