<?php include("config.php"); ?>
<!DOCTYPE html>
<html lang="vi">

<head>
    <title>Chi tiết phiếu kiểm kê</title>
    <meta charset="UTF-8">
    <meta name="robots" content="noindex,nofollow" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="../css/style.css?v=<?= $ver ?>">
    <link rel="stylesheet" type="text/css" href="../css/style_h.css?v=<?= $ver ?>">
    <link rel="stylesheet" type="text/css" href="../css/style_n.css?v=<?= $ver ?>">
    <link rel="stylesheet" type="text/css" href="../css/style_k.css?v=<?= $ver ?>">
</head>

<body>

    <div class="box_right inventory_detail1 detail_5">
        <div class="box_right_ct">
            <?php include("../includes/sidebar.php"); ?>
            <div class="header_menu_overview d_flex align_c space_b">
                <p class="color_grey line_16 font_s14 text_link_page" style="display: block;">
                    <a href="/kiem-ke-kho.html" class="cursor_p">
                        <img src="../images/back_item_g.png" alt=""> </a>
                    Kiểm kê kho /chi tiết phiếu kiểm kê
                </p>
                <img class="open_sidebar_w" src="../images/open_sidebar_w.png" alt="" onclick="toggle('main_sidebar')" style="display: none;">
                <?php include('../includes/header.php');  ?>
            </div>
            <div class="block_wh_tf_add">
                <div class="head_wh d_flex space_b align_c" style="display: none;">
                    <div class=" d_flex align_c">
                        <a href="/kiem-ke-kho.html" class="cursor_p">
                            <img src="../images/back_item_g.png" alt="">
                        </a>
                        <p class="color_grey line_16 font_s14 ml_10">
                            Kiểm kê / Chi tiết phiếu kiểm kê
                        </p>
                    </div>
                </div>
                <!-- tùy chọn -->
                <div class="operation_wh d_flex space_b flex_end">
                    <div class="export_wh d_flex space_b align_c">

                        <button class="btn_dt d_flex flex_center align_c">
                            <img src="../images/check__w.png" alt="">
                            <p class="color_white font_s15 line_h18 font_w500">Duyệt</p>
                        </button>

                        <button class="btn_del d_flex flex_center align_c">
                            <p class="color_blue font_s15 line_h18 font_w500">Xóa phiếu</p>
                        </button>

                        <button class="btn_ex d_flex flex_center align_c">
                            <img src="../images/export.png" alt="">
                            <p class="color_white font_s15 line_h18 font_w500">Xuất excel</p>
                        </button>
                    </div>
                </div>
                <!-- Thông tin phiếu điều chuyển kho -->
                <div class=" info_input_order" style="display : block;">
                    <div class="main_info_ivt_bill" style="display: block;">
                        <div class="tit_info_rq back_blue">
                            <p class="color_white font_s16 line_h19 font_w700">Thông tin phiếu kiểm kê</p>
                        </div>
                        <div class="body_info_rq">
                            <div class="list_info_input_order">
                                <div class="item_body_info_rq d_flex align_c">
                                    <p class="tit_it color_grey font_s14 line_h17 font_w400">Số phiếu:</p>
                                    <p class="color_grey font_s14 line_h17 font_w500">PKK-0000</p>
                                </div>

                                <div class="" style="display: block;">
                                    <div class="item_body_info_rq d_flex align_c fl">
                                        <p class="tit_it color_grey font_s14 line_h17 font_w400">Trạng thái:</p>
                                        <p class="color_green font_s14 line_h17 font_w500">Đã kiểm kê <span class="color_org">- từ chối</span></p>
                                    </div>

                                </div>
                                <div class="item_body_info_rq d_flex align_c">
                                    <p class="tit_it color_grey font_s14 line_h17 font_w400">Người tạo:</p>
                                    <div class="d_flex flex_start align_c">
                                        <img src="../images/ava_ad.png" alt="">
                                        <p class="color_grey font_s14 line_h17 font_w500">Nguyễn Văn Nam</p>
                                    </div>
                                </div>
                                <div class="item_body_info_rq d_flex align_c">
                                    <p class="tit_it color_grey font_s14 line_h17 font_w400">Ngày tạo:</p>
                                    <p class="color_grey font_s14 line_h17 font_w500">10/10/2020</p>
                                </div>
                                <div class="item_body_info_rq d_flex align_c">
                                    <p class="tit_it color_grey font_s14 line_h17 font_w400">Kho thực hiện kiểm kê:</p>
                                    <p class="color_grey font_s14 line_h17 font_w500">ĐKho 1</p>
                                </div>
                                <div class="item_body_info_rq d_flex align_c">
                                    <p class="tit_it color_grey font_s14 line_h17 font_w400">Người thực hiện kiểm kê:</p>
                                    <div class="d_flex flex_start align_c">
                                        <img src="../images/ava_ad.png" alt="">
                                        <p class="color_grey font_s14 line_h17 font_w500">Nguyễn Văn Nam</p>
                                    </div>
                                </div>
                                <div class="item_body_info_rq d_flex align_c">
                                    <p class="tit_it color_grey font_s14 line_h17 font_w400">Ngày thực hiện kiểm kê:</p>
                                    <p class="color_grey font_s14 line_h17 font_w500">10/10/2020</p>
                                </div>
                                <div class="item_body_info_rq d_flex align_c">
                                    <p class="tit_it color_grey font_s14 line_h17 font_w400">Giờ thực hiện kiểm kê:</p>
                                    <p class="color_grey font_s14 line_h17 font_w500">10:10</p>
                                </div>
                                <div class="item_body_info_rq d_flex align_c">
                                    <p class="tit_it color_grey font_s14 line_h17 font_w400">Ngày yêu cầu hoàn thành:</p>
                                    <p class="color_grey font_s14 line_h17 font_w500">10/10/2020</p>
                                </div>
                                <div class="item_body_info_rq d_flex align_c">
                                    <p class="tit_it color_grey font_s14 line_h17 font_w400">Giờ yêu cầu hoàn thành:</p>
                                    <p class="color_grey font_s14 line_h17 font_w500">10:10</p>
                                </div>
                                <div class="item_ct_info_rq d_flex align_s">
                                    <p class="tit_it color_grey font_s14 line_h17 font_w400">Ghi chú:</p>
                                    <p class="color_grey font_s14 line_h17 font_w500">Là năng lực thực hiện các công việc, biến kiến thức thành hành động. Thông thường kỹ năng được chia thành các cấp độ chính như: bắt chước (quan sát và hành vi khuôn mẫu), ứng dụng (thực hiện một số hành động bằng cách làm theo hướng dẫn), vận dụng (chính xác hơn với mỗi hoàn cảnh), vận dụng sáng tạo (trở thành phản xạ tự nhiên).</p>
                                </div>
                                <div class="" style="display:block;">
                                    <div class="item_body_info_rq d_flex align_c">
                                        <p class="tit_it color_grey font_s14 line_h17 font_w400">Người hoàn thành:</p>
                                        <div class="d_flex flex_start align_c">
                                            <img src="../images/ava_ad.png" alt="">
                                            <p class="color_grey font_s14 line_h17 font_w500">Nguyễn Văn Nam</p>
                                        </div>
                                    </div>
                                    <div class="item_body_info_rq d_flex align_c">
                                        <p class="tit_it color_grey font_s14 line_h17 font_w400">Ngày duyệt:</p>
                                        <p class="color_grey font_s14 line_h17 font_w500">10/10/2020</p>
                                    </div>
                                </div>
                                <div class="" style="display:block;">
                                    <div class="item_body_info_rq d_flex align_c">
                                        <p class="tit_it color_grey font_s14 line_h17 font_w400">Người duyệt:</p>
                                        <div class="d_flex flex_start align_c">
                                            <img src="../images/ava_ad.png" alt="">
                                            <p class="color_grey font_s14 line_h17 font_w500">Nguyễn Văn Nam</p>
                                        </div>
                                    </div>
                                    <div class="item_body_info_rq d_flex align_c">
                                        <p class="tit_it color_grey font_s14 line_h17 font_w400">Ngày hoàn thành:</p>
                                        <p class="color_grey font_s14 line_h17 font_w500">10/10/2020</p>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- Danh sách vật tư -->
                <p class="tit_table_vt color_blue font_s16 line_h19 font_w700">Danh sách vật tư</p>
                <div class="tb_operation_wh position_r d_flex align_c">
                    <div class="table_ds_vt table_vt_scr">
                        <table style="width: 1879px" class="table_inventory_detail">
                            <tr class="tit_tbl color_white font_s16 line_h19 font_w500 back_blue">
                                <td rowspan="2" style="width:8%;">Mã vật tư thiết bị</td>
                                <td rowspan="2" style="width:27.6%">Tên đầy đủ vật tư thiết bị</td>
                                <td rowspan="2" style="width:5.8%">Đơn vị tính</td>
                                <td rowspan="2" style="width:7.8%">Hãng sản xuất</td>
                                <td rowspan="2" style="width:5.5%">Xuất xứ</td>
                                <td rowspan="2" style="width:10.4%">Số lượng trên hệ thống</td>
                                <td rowspan="2" style="width:8.3%">Số lượng kiểm kê</td>
                                <td colspan="2" style="width:10.6%">Chênh lệch </td>
                                <td rowspan="2" style="border:1px solid #CCCCCC;width:24%">Ghi chú</td>
                            </tr>
                            <tr class="tit_tbl color_white font_s16 line_h19 font_w500 back_blue">
                                <td style="border-radius:0%;">Thừa</td>
                                <td style="border-radius:0%;border: 1px solid #CCCCCC;">Thiếu</td>
                            </tr>
                            <tr class="color_grey font_s14 line_h17 font_w400">
                                <td>VT-0000</td>
                                <td class="color_blue font_w500" style="text-align: left;">Ống nhựa</td>
                                <td>Chai</td>
                                <td style="text-align: left;">Việt Hà</td>
                                <td>Việt Nam</td>
                                <td style="text-align: right;">Hiện số lượng</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr class="color_grey font_s14 line_h17 font_w400">
                                <td>VT-0000</td>
                                <td class="color_blue font_w500" style="text-align: left;">Ống nhựa</td>
                                <td>Chai</td>
                                <td style="text-align: left;">Việt Hà</td>
                                <td>Việt Nam</td>
                                <td style="text-align: right;">Hiện số lượng</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr class="color_grey font_s14 line_h17 font_w400">
                                <td>VT-0000</td>
                                <td class="color_blue font_w500" style="text-align: left;">Ống nhựa</td>
                                <td>Chai</td>
                                <td style="text-align: left;">Việt Hà</td>
                                <td>Việt Nam</td>
                                <td style="text-align: right;">Hiện số lượng</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr class="color_grey font_s14 line_h17 font_w400">
                                <td>VT-0000</td>
                                <td class="color_blue font_w500" style="text-align: left;">Ống nhựa</td>
                                <td>Chai</td>
                                <td style="text-align: left;">Việt Hà</td>
                                <td>Việt Nam</td>
                                <td style="text-align: right;">Hiện số lượng</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr class="color_grey font_s14 line_h17 font_w400">
                                <td>VT-0000</td>
                                <td class="color_blue font_w500" style="text-align: left;">Ống nhựa</td>
                                <td>Chai</td>
                                <td style="text-align: left;">Việt Hà</td>
                                <td>Việt Nam</td>
                                <td style="text-align: right;">Hiện số lượng</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr class="color_grey font_s14 line_h17 font_w400">
                                <td>VT-0000</td>
                                <td class="color_blue font_w500" style="text-align: left;">Ống nhựa</td>
                                <td>Chai</td>
                                <td style="text-align: left;">Việt Hà</td>
                                <td>Việt Nam</td>
                                <td style="text-align: right;">Hiện số lượng</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr class="color_grey font_s14 line_h17 font_w400">
                                <td>VT-0000</td>
                                <td class="color_blue font_w500" style="text-align: left;">Ống nhựa</td>
                                <td>Chai</td>
                                <td style="text-align: left;">Việt Hà</td>
                                <td>Việt Nam</td>
                                <td style="text-align: right;">Hiện số lượng</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr class="color_grey font_s14 line_h17 font_w400">
                                <td>VT-0000</td>
                                <td class="color_blue font_w500" style="text-align: left;">Ống nhựa</td>
                                <td>Chai</td>
                                <td style="text-align: left;">Việt Hà</td>
                                <td>Việt Nam</td>
                                <td style="text-align: right;">Hiện số lượng</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr class="color_grey font_s14 line_h17 font_w400">
                                <td>VT-0000</td>
                                <td class="color_blue font_w500" style="text-align: left;">Ống nhựa</td>
                                <td>Chai</td>
                                <td style="text-align: left;">Việt Hà</td>
                                <td>Việt Nam</td>
                                <td style="text-align: right;">Hiện số lượng</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr class="color_grey font_s14 line_h17 font_w400">
                                <td>VT-0000</td>
                                <td class="color_blue font_w500" style="text-align: left;">Ống nhựa</td>
                                <td>Chai</td>
                                <td style="text-align: left;">Việt Hà</td>
                                <td>Việt Nam</td>
                                <td style="text-align: right;">Hiện số lượng</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                        </table>
                    </div>
                    <div class="pre_q d_flex align_c flex_center position_a">
                        <span class="pre_arrow"></span>
                    </div>
                    <div class="next_q d_flex align_c flex_center position_a">
                        <span class="next_arrow"></span>
                    </div>
                </div>
            </div>
        </div>
        <?php include("../includes/popup_kiem-ke.php"); ?>
    </div>
    <?php include('../includes/popup_overview.php');  ?>


</body>
<script type="text/javascript" src="../js/jquery-3.4.1.min.js"></script>
<script type="text/javascript" src="../js/jquery.validate.min.js"></script>
<script type="text/javascript" src="../js/select2.min.js"></script>
<script type="text/javascript" src="../js/js_h.js"></script>
<script type="text/javascript" src="../js/validate_h.js"></script>
<script type="text/javascript" src="../js/js_k.js"></script>
<script type="text/javascript" src="../js/js_n.js"></script>
<script>
    $('.active11').each(function() {
        if ($(this).hasClass('active11')) {
            $(this).find('a').addClass('active');
        }
    });
</script>

</html>