<?php

include("config1.php");

$id_phieu = getValue("id", "int", "GET", "");

if (!in_array(2, $ro_kk_kho)) {
  header("Location: /tong-quan.html");
}

if (isset($_COOKIE['user']) && $_COOKIE['user'] != "" && $_COOKIE['role'] == 1) {
  $token = $_COOKIE['acc_token'];
  $curl = curl_init();
  $data = array();
  curl_setopt($curl, CURLOPT_POST, 1);
  curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
  curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/user_info_company.php');
  curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
  curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));

  $response = curl_exec($curl);
  curl_close($curl);
  $data_tt = json_decode($response, true);
  $tt_user = $data_tt['data']['user_info_result'];
} else if (isset($_COOKIE['user']) && $_COOKIE['user'] != "" && $_COOKIE['role'] == 2) {
  $token = $_COOKIE['acc_token'];
  $curl = curl_init();
  $data = array();
  curl_setopt($curl, CURLOPT_POST, 1);
  curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
  curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/user_info_employee.php');
  curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
  curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));
  $response = curl_exec($curl);
  curl_close($curl);
  $data_tt = json_decode($response, true);
  $tt_user = $data_tt['data']['user_info_result'];
}
$id_cty = $tt_user['com_id'];

if (isset($_SESSION['quyen']) && $_SESSION['quyen'] == 1) {
  $curl = curl_init();
  $token = $_COOKIE['acc_token'];
  curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/list_all_employee_of_company.php');
  curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
  curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));
  $response = curl_exec($curl);
  curl_close($curl);

  $data_list = json_decode($response, true);
  $data_list_nv = $data_list['data']['items'];
  $count = count($data_list_nv);
  $user_id = $_SESSION['com_id'];
} elseif (isset($_SESSION['quyen']) && ($_SESSION['quyen'] == 2)) {
  $curl = curl_init();
  $token = $_COOKIE['acc_token'];
  curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/list_all_my_partner.php?get_all=true');
  curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
  curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));
  $response = curl_exec($curl);
  curl_close($curl);
  $data_list = json_decode($response, true);
  $data_list_nv = $data_list['data']['items'];
  $count = count($data_list_nv);
  $user_id = $_SESSION['ep_id'];
}
$newArr = [];
for ($i = 0; $i < count($data_list_nv); $i++) {
  $value = $data_list_nv[$i];
  $newArr[$value["ep_id"]] = $value;
}

$date = date('Y-m-d', time());

$kho = new db_query("SELECT `kho_id`, `kho_maKho`, `kho_name` FROM `kho` WHERE `kho_id_ct` = $id_cty");

$info_kk = new db_query("SELECT * FROM `kho-cho-xu-li` WHERE `kcxl_id` = $id_phieu");
$info_kk = mysql_fetch_assoc($info_kk->result);
$id_kho_vt = $info_kk['kcxl_khoThucHienKiemKe'];
$data_user = explode(",", $info_kk['kcxl_nguoiThucHien']);
// die();

?>
<!DOCTYPE html>
<html lang="vi">

<head>
  <title>Thêm mới phiếu kiểm kê</title>
  <meta charset="UTF-8">
  <meta name="robots" content="noindex,nofollow" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" type="text/css" href="../css/style.css?v=<?= $ver ?>">
  <link rel="stylesheet" type="text/css" href="../css/select2.min.css?v=<?= $ver ?>">
  <link rel="stylesheet" type="text/css" href="../css/style_h.css?v=<?= $ver ?>">
  <link rel="stylesheet" type="text/css" href="../css/style_n.css?v=<?= $ver ?>">
  <link rel="stylesheet" type="text/css" href="../css/style_k.css?v<=<?= $ver ?>">
</head>

<body class="seclec2_radius">
  <div class="main_wrapper_all">
    <div class="wapper_all">
      <?php include('../includes/sidebar.php');  ?>
    </div>
    <div class="main_overview" id="main_overview">
      <div class="header_menu_overview d_flex align_c space_b">
        <p class="color_grey line_16 font_s14 text_link_page" style="display: block;">
          <a href="/kiem-ke-kho.html" class="cursor_p">
            <img src="../images/back_item_g.png" alt="">
          </a>Kiểm kê /thêm mới
        </p>
        <img class="open_sidebar_w" src="../images/open_sidebar_w.png" alt="" onclick="toggle('main_sidebar')" style="display: none;">
        <?php include('../includes/header.php');  ?>
      </div>
      <div class="main_ivt_create">
        <div class="head_wh d_flex space_b align_c title_wh" style="display: none;">
          <div class=" d_flex align_c ">
            <a href="/kiem-ke-kho.html" class="cursor_p">
              <img src="../images/back_item_g.png" alt="">
            </a>
            <p class="color_grey line_16 font_s14 ml_10">
              Kiểm kê / Thêm mới
            </p>
          </div>
        </div>
        <!-- thông tin phiếu kiểm kê -->
        <form method="POST" action="" id="f_ivt_cre">
          <div class="body_inventory_create">
            <div class="header_body_b color_white line_h19 font_s16 font_wB">
              Thêm mới phiếu kiểm kê
            </div>
            <div class="body_input_infor_vote">
              <div class="input_infor_vote">
                <div class="box_add_ex_wh">
                  <div class="d_flex align_c flex_end wh768">
                    <div class="width_create mr_10 d_flex flex_column mb_15">
                      <p class="color_grey font_s15 line_h18 font_w500 mb_5">Số phiếu</p>
                      <input class="color_grey font_s14 line_h17 font_w400" type="text" value="PKK - <?= $id_phieu ?>" disabled="disabled">
                    </div>
                    <div class="width_create select_status d_flex space_b ml_10">
                      <div class="d_flex flex_column">
                        <p class="color_grey font_s15 line_h18 font_w500 mb_5">Trạng thái</p>
                        <select name="" id="" class="select_stt color_grey font_s14 line_h17 font_w400" style="width: 100%;" disabled>
                          <option value="8" <?= $info_kk['kcxl_trangThai'] == '8' ? "selected" : "" ?>>Khởi tạo</option>
                          <option value="14" <?= $info_kk['kcxl_trangThai'] == '14' ? "selected" : "" ?>>Khởi tạo</option>
                          <option value="9" <?= $info_kk['kcxl_trangThai'] == '9' ? "selected" : "" ?>>Kiểm kê chờ duyệt</option>
                          <option value="10" <?= $info_kk['kcxl_trangThai'] == '10' ? "selected" : "" ?>>Từ chối duyệt kiểm kê</option>
                          <option value="11" <?= $info_kk['kcxl_trangThai'] == '11' ? "selected" : "" ?>>Duyệt kiểm kê</option>
                          <option value="12" <?= $info_kk['kcxl_trangThai'] == '12' ? "selected" : "" ?>>Cập nhật kho</option>
                        </select>
                      </div>
                      <div class="input_date_fn d_flex flex_column" <?php if ($info_kk['kcxl_trangThai'] == 12) { ?>style="display: block;" <?php } ?>>
                        <p class="color_grey font_s15 line_h18 font_w500 mb_5">Ngày hoàn thành<span style="color: red;">*</span></p>
                        <input type="date" name="input_date_fn" value="<?= $info_kk['kcxl_ngayHoanThanh'] ?>">
                      </div>
                    </div>
                  </div>
                  <div class="d_flex align_c space_b wh768">
                    <div class="width_create d_flex flex_column mb_15">
                      <p class="color_grey font_s15 line_h18 font_w500 mb_5">Người tạo</p>
                      <?php if ($info_kk['kcxl_nguoiTao'] != 0) { ?>
                        <input class="nguoi_tao color_grey font_s14 line_h17 font_w400" type="text" readonly type="text" value="<?= $newArr[$info_kk['kcxl_nguoiTao']]['ep_name'] ?>" data="<?= $info_kk['kcxl_nguoiTao'] ?>">
                      <? } ?>
                      <?php if ($info_kk['kcxl_nguoiTao'] == 0) { ?>
                        <input class="nguoi_tao color_grey font_s14 line_h17 font_w400" type="text" readonly type="text" value="<?= $tt_user['com_name'] ?>" data="0">
                      <? } ?>
                    </div>
                    <div class="width_create d_flex flex_column mb_15">
                      <p class="color_grey font_s15 line_h18 font_w500 mb_5">Ngày tạo</p>
                      <input class="ngay_tao color_grey font_s14 line_h17 font_w400" type="date" value="<?= $info_kk['kcxl_ngayTao'] ?>">
                    </div>
                  </div>
                  <div class="d_flex align_s space_b wh768">
                    <div class="box_wh_ex width_create d_flex flex_column mb_15">
                      <p class="color_grey font_s15 line_h18 font_w500 mb_5">Kho thực hiện kiểm kê<span style="color: red;">*</span></p>
                      <select name="select_wh_ex" id="" class="select_wh_ex color_grey font_s14 line_h17 font_w400" style="width: 100%;">
                        <option value=""></option>
                        <?php while ($row_kho = mysql_fetch_assoc($kho->result)) { ?>
                          <option value="<?= $row_kho['kho_id']; ?>" <?= $row_kho['kho_id'] == $info_kk['kcxl_khoThucHienKiemKe'] ? 'selected' : '' ?>><?= $row_kho['kho_name']; ?></option>
                        <?php } ?>
                      </select>
                    </div>
                    <div class="box_wh_in width_create d_flex flex_column mb_15">
                      <div class="d_flex align_c space_b ">
                        <div class="d_flex align_c">
                          <p class="font_s15 line_h18 font_w500 color_grey mb_5">Người thực hiện kiểm kê</p>
                          <span class="color_red alert_red">*</span>
                        </div>
                        <a class="color_blue font_s15 line_h18 font_w400 d_flex align_c mb_5" href="#">
                          <span class="font_s24">+</span>
                          Thêm nhân viên
                        </a>
                      </div>
                      <select name="ivt_person" class="font_s14 line_h16 color_grey font_w400 inventory_men" multiple="multiple" style="width:100%;">
                        <option></option>
                        <?php
                        $data_user = explode(",", $info_kk['kcxl_nguoiThucHien']);
                        for ($i = 0; $i < count($data_user); $i++) {
                          foreach ($newArr as $key => $value) {
                        ?>
                            <option value="<?= $key ?>" <?= ($key == $data_user[$i]) ? "selected" : "" ?>><?= $value['ep_name'] . ' ' . '-' . ' ' . $value['dep_name'] ?></option>
                        <?php
                          }
                        }
                        ?>
                      </select>
                    </div>
                  </div>
                  <div class="d_flex align_c space_b wh768">
                    <div class="width_create d_flex flex_column mb_15">
                      <div class="d_flex space_b">
                        <div class="d_flex flex_column" style="width:50%;">
                          <p class="font_s15 line_h18 font_w500 color_grey mb_5">Ngày thực hiện kiểm kê</p>
                          <input class="ngay_thuc_hien font_14 line_h16 " type="date" placeholder="Chọn ngày" value="<?= $info_kk['kcxl_ngayThucHienKiemKe'] ?>">
                        </div>
                        <div class="d_flex flex_column" style="width:50%;padding:0px 0px 0px 10px;">
                          <div class="d_flex">
                            <p class="font_s15 line_h18 font_w500 color_grey mb_5">Giờ thực hiện kiểm kê</p>
                          </div>
                          <input class="gio_thuc_hien font_14 line_h16" type="time" placeholder="Chọn giờ" value="<?= $info_kk['kcxl_thoiGianThucHien'] ?>">
                        </div>
                      </div>
                    </div>
                    <div class="width_create d_flex flex_column mb_15">
                      <div class="d_flex space_b int_kk_cre">
                        <div class="d_flex flex_column" style="width:50%;">
                          <p class="font_s15 line_h18 font_w500 color_grey mb_5">Ngày yêu cầu hoàn thành</p>
                          <input class="ngay_yc_hoan_thanh font_14 line_h16 " type="date" placeholder="Chọn ngày" value="<?= $info_kk['kcxl_ngayYeuCauHoanThanh'] ?>">
                        </div>
                        <div class="d_flex flex_column" style="width:50%;padding:0px 0px 0px 10px;">
                          <div class="d_flex">
                            <p class="font_s15 line_h18 font_w500 color_grey mb_5">Giờ yêu cầu hoàn thành</p>
                          </div>
                          <input class="gio_yc_hoan_thanh font_14 line_h16 " type="time" placeholder="Chọn giờ" value="<?= $info_kk['kcxl_thoiGianYeuCauHoanThanh'] ?>">
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="flex_column d_flex note_import_vote_infor">
                <p class="font_s15 line_h18 font_w500 color_grey mb_5">Ghi chú</p>
                <textarea name="ghi_chu" id rows="5" placeholder="Nhập nội dung"><?= $info_kk['kcxl_ghi_chu'] ?></textarea>
              </div>
            </div>
          </div>
          <!-- bảng danh sách vật tư -->
          <p class="tit_table_vt color_blue font_s16 line_h19 font_w700"> Danh sách vật tư</p>
          <!-- bảng 1 -->
          <div class="position_r align_c d_flex table_kt display_none" <?php if ($info_kk['kcxl_trangThai'] == '8') { ?>style="display: block;" <?php } ?>>
            <div class="table_vt_scr" onscroll="table_scroll(this)">
              <table class="table_vt">
                <tr class="tit_tbl color_white font_s16 line_h19 font_w500 back_blue">
                  <th style="width:5.3%;">
                    <span class="span_thread"></span>
                  </th>
                  <th>Mã vật tư
                    <span class="span_thread"></span>
                  </th>
                  <th>Tên đầy đủ vật tư thiết bị
                    <span class="span_thread"></span>
                  </th>
                  <th>Đơn vị tính
                    <span class="span_thread"></span>
                  </th>
                  <th> Hãng sản xuất
                    <span class="span_thread"></span>
                  </th>
                  <th> Xuất xứ
                    <span class="span_thread"></span>
                  </th>
                  <th style="width:18.1%;"> Số lượng trên hệ thống
                    <span class="span_thread"></span>
                  </th>

                </tr>
                <tbody id="tbody_add">
                  <?php
                  $list_dsvt = new db_query("SELECT `dsvt_id`, `dsvt_name`, `hsx_name`, `dvt_name`, `xx_name`, `dsvt_soLuongTon`, `slvt_soLuongKiemKe`, `slvt_ghiChu`, `dsvt_kho`
                  FROM `danh-sach-vat-tu`
                  LEFT JOIN `hang-san-xuat` on `dsvt_hangSanXuat` = `hsx_id`       
                  LEFT JOIN `don-vi-tinh` on `dsvt_donViTinh` = `dvt_id`
                  LEFT JOIN `xuat-xu` on `dsvt_xuatXu` = `xx_id`
                  LEFT JOIN `so-luong-vat-tu` on  `dsvt_id` = `slvt_maVatTuThietBi`
                  WHERE `dsvt_check` = 1 AND `slvt_idPhieu` = $id_phieu AND `dsvt_id_ct` = $id_cty
              ");
                  $responsive1 = [];
                  $i = 0;
                  while ($data = mysql_fetch_assoc($list_dsvt->result)) {
                    $check_kho = explode(',', $data['dsvt_kho']);
                    if (in_array($info_kk['kcxl_khoThucHienKiemKe'], $check_kho)) {
                      $info_k['dsvt_id'] = $data['dsvt_id'];
                      $info_k['dsvt_name'] = $data['dsvt_name'];
                      $info_k['dsvt_soLuongTon'] = $data['dsvt_soLuongTon'];
                      $info_k['nvt_name'] = $data['nvt_name'];
                      $info_k['hsx_name'] = $data['hsx_name'];
                      $info_k['dvt_name'] = $data['dvt_name'];
                      $info_k['xx_name'] = $data['xx_name'];
                      $info_k['dsvt_kho'] = $data['dsvt_kho'];
                      $info_k['dsvt_description'] = $data['dsvt_description'];
                      $responsive1[$i] = $info_k;
                      $i++;
                    }
                  }

                  $dsvt = new db_query("SELECT `dsvt_id`, `dsvt_name`, `dsvt_kho` FROM `danh-sach-vat-tu` WHERE `dsvt_id_ct` = $id_cty");

                  $responsive_sl = [];
                  $j = 0;
                  while ($item = mysql_fetch_assoc($dsvt->result)) {
                    $check_kho = explode(',', $item['dsvt_kho']);
                    if (in_array($info_kk['kcxl_khoThucHienKiemKe'], $check_kho)) {
                      $info_k['dsvt_id'] = $item['dsvt_id'];
                      $info_k['dsvt_name'] = $item['dsvt_name'];
                      $responsive_sl[$j] = $info_k;
                      $j++;
                    }
                  }
                  // print_r($responsive1);
                  foreach ($responsive1 as $data) {
                  ?>
                    <tr class="color_grey font_s14 line_h17 font_w400 table_3 delete_3" data="1">
                      <td onclick="deleteRow(this)"><img class="cursor_p delete_3" src="../images/del_row.png" alt="" style="margin: 0; width: 16px; height:16px;"></td>
                      <td class="ma_vat_tu_3 delete_3" style="background-color: #EEEEEE;">VT - <?= $data['dsvt_id'] ?></td>
                      <td class="color_blue font_w500 delete_3" style="text-align: left;">
                        <select class="select_tb color_grey3 font_s14 line_h17 font_w400 select_tb_1 delete_3" style="width: 100%" onchange="changeValue(this)">
                          <option value=""></option>
                          <? foreach ($responsive_sl as $value) { ?>
                            <option value="<?= $value['dsvt_id'] ?>" <?= ($value['dsvt_id'] == $data['dsvt_id']) ? "selected" : "" ?>><?= $value['dsvt_name'] ?></option>
                          <? } ?>
                        </select>
                      </td>
                      <td style="background-color: #EEEEEE;" class="don_vi_tinh_3 delete_3"><?= $data['dvt_name'] ?></td>
                      <td style="text-align: left; background-color: #EEEEEE;" class="hang_san_xuat_3 delete_3"><?= $data['hsx_name'] ?></td>
                      <td style="background-color: #EEEEEE;" class="xuat_xu_3 delete_3"><?= $data['xx_name'] ?></td>
                      <td style="text-align: right; background-color: #EEEEEE;" class="sltht"><?php $slht = json_decode($data['dsvt_soLuongTon']);
                                                                                              if ($slht != '') {
                                                                                                echo $slht->$info_kk['kcxl_khoThucHienKiemKe'];
                                                                                              } else {
                                                                                                echo 0
                                                                                              ?>
                        <? } ?></td>
                    </tr>
                  <? } ?>
                </tbody>
                <tr class="tr_add color_blue font_s14 line_h17 font_w500">
                  <td colspan="2">
                    <div class="btn_add_vt d_flex align_c cursor_p">
                      <p class="add_vt d_flex flex_center align_c position_r"><span class="color_white font_s20 position_a">+</span></p>
                      <p class="">Thêm vật tư</p>
                    </div>
                  </td>
                </tr>
              </table>
            </div>
            <div class="pre_q d_flex align_c flex_center position_a display_none" onclick="pre_q(this)">
              <span class="pre_arrow"></span>
            </div>
            <div class="next_q d_flex align_c flex_center position_a" onclick="next_q(this)">
              <span class="next_arrow"></span>
            </div>
          </div>
          <!-- bảng 2 -->
          <div class="tb_operation_wh position_r d_flex align_c table_ht display_none" <?php if ($info_kk['kcxl_trangThai'] == "9" || $info_kk['kcxl_trangThai'] == "10" || $info_kk['kcxl_trangThai'] == "11" || $info_kk['kcxl_trangThai'] == "12" || $info_kk['kcxl_trangThai'] == "14") { ?>style="display: block;" <?php } ?>>
            <div class="table_vt_scr" onscroll="table_scroll(this)">
              <div class="table_ds_vt">
                <table style="width: 1940px" class="table_inventory_detail">
                  <tr class="tit_tbl color_white font_s16 line_h19 font_w500 back_blue">
                    <td rowspan="2" style="width:3%;"></td>
                    <td rowspan="2" style="width:7.9%;">Mã vật tư thiết bị</td>
                    <td rowspan="2" style="width:19%">Tên đầy đủ vật tư thiết bị</td>
                    <td rowspan="2" style="width:5.5%">Đơn vị tính</td>
                    <td rowspan="2" style="width:7.6%">Hãng sản xuất</td>
                    <td rowspan="2" style="width:5.3%">Xuất xứ</td>
                    <td rowspan="2" style="width:10%">Số lượng trên hệ thống</td>
                    <td rowspan="2" style="width:8%">Số lượng kiểm kê</td>
                    <td colspan="2" style="width:10.3%">Chênh lệch </td>
                    <td rowspan="2" style="border:1px solid #CCCCCC;width:23.5%">Ghi chú</td>
                  </tr>
                  <tr class="tit_tbl color_white font_s16 line_h19 font_w500 back_blue">
                    <td style="border-radius:0%;">Thừa</td>
                    <td style="border-radius:0%;border: 1px solid #CCCCCC;">Thiếu</td>
                  </tr>
                  <tbody id="tbody_add2">
                    <?php
                    $list_dsvt = new db_query("SELECT `dsvt_id`, `dsvt_name`, `hsx_name`, `dvt_name`, `xx_name`, `dsvt_soLuongTon`, `slvt_soLuongKiemKe`, `slvt_ghiChu`, `dsvt_kho`
																							FROM `danh-sach-vat-tu`
																							LEFT JOIN `hang-san-xuat` on `dsvt_hangSanXuat` = `hsx_id`       
																							LEFT JOIN `don-vi-tinh` on `dsvt_donViTinh` = `dvt_id`
																							LEFT JOIN `xuat-xu` on `dsvt_xuatXu` = `xx_id`
																							LEFT JOIN `so-luong-vat-tu` on  `dsvt_id` = `slvt_maVatTuThietBi`
																							WHERE `dsvt_check` = 1 AND `slvt_idPhieu` = $id_phieu AND `dsvt_id_ct` = $id_cty
																					");
                    $responsive1 = [];
                    $i = 0;
                    while ($data = mysql_fetch_assoc($list_dsvt->result)) {
                      $check_kho = explode(',', $data['dsvt_kho']);
                      if (in_array($info_kk['kcxl_khoThucHienKiemKe'], $check_kho)) {
                        $info_k['dsvt_id'] = $data['dsvt_id'];
                        $info_k['dsvt_name'] = $data['dsvt_name'];
                        $info_k['dsvt_soLuongTon'] = $data['dsvt_soLuongTon'];
                        $info_k['nvt_name'] = $data['nvt_name'];
                        $info_k['hsx_name'] = $data['hsx_name'];
                        $info_k['dvt_name'] = $data['dvt_name'];
                        $info_k['xx_name'] = $data['xx_name'];
                        $info_k['dsvt_kho'] = $data['dsvt_kho'];
                        $info_k['slvt_ghiChu'] = $data['slvt_ghiChu'];
                        $info_k['slvt_soLuongKiemKe'] = $data['slvt_soLuongKiemKe'];
                        $info_k['dsvt_description'] = $data['dsvt_description'];
                        $responsive1[$i] = $info_k;
                        $i++;
                      }
                    }

                    $dsvt = new db_query("SELECT `dsvt_id`, `dsvt_name`, `dsvt_kho` FROM `danh-sach-vat-tu` WHERE `dsvt_id_ct` = $id_cty");

                    $responsive_sl = [];
                    $j = 0;
                    while ($item = mysql_fetch_assoc($dsvt->result)) {
                      $check_kho = explode(',', $item['dsvt_kho']);
                      if (in_array($info_kk['kcxl_khoThucHienKiemKe'], $check_kho)) {
                        $info_k['dsvt_id'] = $item['dsvt_id'];
                        $info_k['dsvt_name'] = $item['dsvt_name'];
                        $responsive_sl[$j] = $info_k;
                        $j++;
                      }
                    }
                    // print_r($responsive1);
                    foreach ($responsive1 as $data) {
                    ?>
                      <tr class="delete_3" data="2" style="border-bottom: 1px solid #CCCCCC;">
                        <td onclick="deleteRow(this)"><img class="cursor_p delete_3" src="../images/del_row.png" alt="" style="margin: 0; width: 16px; height:16px;"></td>
                        <td class="ma_vat_tu_3 delete_3" style="background-color: #EEEEEE;">VT - <?= $data['dsvt_id'] ?> </td>
                        <td class="color_blue font_w500 delete_3" style="text-align: left;">
                          <select class="select_tb color_grey3 font_s14 line_h17 font_w400 select_tb_3 delete_3" onchange="changeValue(this)" style="width: 100%">
                            <option value=""></option>
                            <? foreach ($responsive_sl as $value) { ?>
                              <option value="<?= $value['dsvt_id'] ?>" <?= ($value['dsvt_id'] == $data['dsvt_id']) ? "selected" : "" ?>><?= $value['dsvt_name'] ?></option>
                            <? } ?>
                          </select>
                        </td>
                        <td style="background-color: #EEEEEE;" class="don_vi_tinh_3 delete_3"> <?= $data['dvt_name'] ?></td>
                        <td style="text-align: left; background-color: #EEEEEE;" class="hang_san_xuat_3 delete_3"><?= $data['hsx_name'] ?></td>
                        <td style="background-color: #EEEEEE;" class="xuat_xu_3 delete_3"><?= $data['xx_name'] ?></td>
                        <td style="text-align: right; background-color: #EEEEEE;" class="sl_hethong"><?php $slht = json_decode($data['dsvt_soLuongTon']);
                                                                                                      if ($slht != '') {
                                                                                                        echo $slht->$info_kk['kcxl_khoThucHienKiemKe'];
                                                                                                      } else {
                                                                                                        echo 0
                                                                                                      ?>
                          <? } ?>
                        </td>
                        <td><input type="text" class="nhap_sl_3 delete_3 color_grey font_s14 line_h17 font_w400 " oninput="<?= $oninput ?>" onkeyup="thong_ke(this)" placeholder="Nhập số lượng" value="<?= $data['slvt_soLuongKiemKe'] ?>"></td>
                        <?php if ($slht->$info_kk['kcxl_khoThucHienKiemKe'] - $data['slvt_soLuongKiemKe'] > 0) { ?>
                          <td style="background-color: #EEEEEE;" class="sl_thua">0</td>
                          <td style="background-color: #EEEEEE;" class="sl_thieu"><?= $slht->$info_kk['kcxl_khoThucHienKiemKe'] - $data['slvt_soLuongKiemKe'] ?></td>
                        <?php } else if ($slht->$info_kk['kcxl_khoThucHienKiemKe'] - $data['slvt_soLuongKiemKe'] < 0) { ?>
                          <td style="background-color: #EEEEEE;" class="sl_thua"><?= $data['slvt_soLuongKiemKe'] - $slht->$info_kk['kcxl_khoThucHienKiemKe'] ?></td>
                          <td style="background-color: #EEEEEE;" class="sl_thieu">0</td>
                        <? }  ?>
                        <td><input type="text" style="text-align: left;" class="nhap_ghi_chu color_grey font_s14 line_h17 font_w400" placeholder="Nhập ghi chú" value="<?= $data['slvt_ghiChu'] ?>">
                        </td>
                      </tr>
                    <? } ?>
                  </tbody>
                  <tr class="tr_add color_blue font_s14 line_h17 font_w500">
                    <td colspan="2">
                      <div class="btn_add_vt2 d_flex align_c cursor_p">
                        <p class="add_vt d_flex flex_center align_c position_r"><span class="color_white font_s20 position_a">+</span></p>
                        <p class="">Thêm vật tư</p>
                      </div>
                    </td>
                  </tr>
                </table>
              </div>
            </div>
            <div class="pre_q d_flex align_c flex_center position_a display_none" onclick="pre_q(this)">
              <span class="pre_arrow"></span>
            </div>
            <div class="next_q d_flex align_c flex_center position_a" onclick="next_q(this)">
              <span class="next_arrow"></span>
            </div>
          </div>
          <!-- button -->
          <div class="btn_cf_rq d_flex flex_center" style="display: flex;">
            <button type="button" class="btn_cancel back_w color_blue font_s15 line_h18 font_w500"><a href="kiem-ke-kho.html"></a> Hủy</button>
            <button type="button" class="btn_save back_blue color_white font_s15 line_h18 font_w500">Lưu</button>
          </div>
        </form>
      </div>
    </div>
  </div>
  <?php include('../includes/popup_dieu-chuyen-kho.php');  ?>
  <?php include('../includes/popup_overview.php');  ?>
</body>

<script type="text/javascript" src="../js/jquery-3.4.1.min.js"></script>
<script type="text/javascript" src="../js/jquery.validate.min.js"></script>
<script type="text/javascript" src="../js/select2.min.js"></script>
<script type="text/javascript" src="../js/validate_h.js"></script>
<script type="text/javascript" src="../js/reset_validate.js"></script>
<script type="text/javascript" src="../js/js_h.js"></script>
<script type="text/javascript" src="../js/js_n.js"></script>
<script type="text/javascript" src="../js/js_k.js"></script>
<script>
  var id_ct_change = <?= json_encode($id_cty) ?>;
</script>
<script>
  $('.active11').each(function() {
    if ($(this).hasClass('active11')) {
      $(this).find('a').addClass('active');
    }
  });

  $('.select_stt').change(function() {
    var val_tt = $(this).val();
    if (val_tt == "1") {
      $('.table_kt').show();
      $('.table_ht').hide();
    } else {
      $('.table_ht').show();
      $('.table_kt').hide();
    }

  });

  function changeValue(data) {
    var id_vt = $(data).val();
    var vitri = $(data).parents('tr');
    var id_kho = $('.select_wh_ex').val()
    // var check = vitri.find(data)
    console.log('ssss')
    $.ajax({
      url: '../ajax/change_edit_data_tb_kk.php',
      type: 'POST',
      dataType: 'Json',
      data: {
        id_vt: id_vt,
        id_ct_change: id_ct_change,
        id_kho: id_kho
      },
      success: function(response) {
        console.log(response)
        vitri.find('.ma_vat_tu_3').html('VT' + ' ' + '-' + ' ' + response[0].id);
        vitri.find('.don_vi_tinh_3').html(response[0].ten_dvt);
        vitri.find('.hang_san_xuat_3').html(response[0].ten_hsx);
        vitri.find('.xuat_xu_3').html(response[0].ten_xx);
        vitri.find('.sltht').html(response[0].sl_ht);
      }
    })
  }

  var data = <?= json_encode($newArr) ?>;

  $('.select_wh_ex').change(function() {
    $('.delete_3').remove()
    var value = $(this).val()
    var trang_thai = $('.select_stt').val();
    var id_cty = id_ct_change
    $.ajax({
      url: '../ajax/get_dsvt_kho_kiem_ke.php',
      type: 'POST',
      dataType: 'Json',
      data: {
        trang_thai: trang_thai,
        id_kho: value,
        id_cty: id_cty
      },
      success: function(response) {
        console.log(response)
      }
    })
  });

  $('.btn_add_vt').click(function() {
    var value = $('.select_wh_ex').val()
    var trang_thai = $('.select_stt').val();
    var id_cty = id_ct_change
    $.ajax({
      url: '../ajax/get_dsvt_kho_kiem_ke.php',
      type: 'POST',
      data: {
        trang_thai: trang_thai,
        id_kho: value,
        id_cty: id_cty
      },
      success: function(data) {
        console.log(data)
        if (data != 'jkkjk') {
          $("#tbody_add").append(data);
        } else {
          $("#tbody_add").append();
        }
      }
    })
  });

  $('.btn_add_vt2').click(function() {
    console.log('aaa')
    var value = $('.select_wh_ex').val()
    var trang_thai = $('.select_stt').val();
    var id_cty = id_ct_change
    $.ajax({
      url: '../ajax/get_dsvt_kho_kiem_ke.php',
      type: 'POST',
      data: {
        trang_thai: trang_thai,
        id_kho: value,
        id_cty: id_cty
      },
      success: function(data) {
        if (data != 'jkkjk') {
          $("#tbody_add2").append(data);
        } else {
          $("#tbody_add2").append();
        }
      }
    })
  });

  $('.btn_save').click(function() {
    resetFormValidator(form_vali);
    var form_vali = $("#f_ivt_cre");
    var trang_thai = $('.select_stt').val();
    var ngay_hoan_thanh = $("input[name='input_date_fn']").val();
    var nguoi_tao = $('.nguoi_tao').attr('data');
    var ngay_tao = $('.ngay_tao').val();
    var kho = $('.select_wh_ex').val();
    var nguoi_thuc_hien = $(".inventory_men").val();
    var ngay_thuc_hien = $(".ngay_thuc_hien").val();
    var gio_thuc_hien = $(".gio_thuc_hien").val();
    var ngay_yc_hoan_thanh = $(".ngay_yc_hoan_thanh").val();
    var gio_yc_hoan_thanh = $(".gio_yc_hoan_thanh").val();
    var ghi_chu = $("textarea[name='ghi_chu']").val();
    var id_cty = id_ct_change
    var id_phieu = "<?= $id_phieu ?>"

    if (trang_thai === "8") {
      var vat_tu = [];
      $(".select_tb_1").each(function() {
        var id_vattu = $(this).val();
        var sltht = $(this).parents('tr').find('.sltht').html();
        if (id_vattu != "") {
          vat_tu.push({
            'id': id_vattu,
            'sltht': sltht
          });
        }
      });
      console.log(vat_tu)

      form_vali.validate({
        errorPlacement: function(error, element) {
          error.appendTo(element.parents(".box_wh_in"));
          error.appendTo(element.parents(".box_wh_ex"));
          error.wrap("<span class='error'>");
        },
        rules: {
          ivt_person: "required",
          select_wh_ex: "required",
        },
        messages: {
          ivt_person: "Chọn người kiểm kê.",
          select_wh_ex: "Chọn kho.",
        }
      });

      if (form_vali.valid() === true && vat_tu.length > 0) {
        var vat_tu = JSON.stringify(vat_tu)
        $.ajax({
          url: '../ajax/edit_kiem_ke_kho.php',
          type: 'POST',
          data: {
            id_phieu: id_phieu,
            trang_thai: trang_thai,
            nguoi_tao: nguoi_tao,
            ngay_tao: ngay_tao,
            kho: kho,
            nguoi_thuc_hien: nguoi_thuc_hien,
            ngay_thuc_hien: ngay_thuc_hien,
            gio_thuc_hien: gio_thuc_hien,
            ngay_yc_hoan_thanh: ngay_yc_hoan_thanh,
            gio_yc_hoan_thanh: gio_yc_hoan_thanh,
            ghi_chu: ghi_chu,
            id_cty: id_cty,
            vat_tu: vat_tu
          },
          success: function(data) {
            $('.popup_add_notif_succ').show();
            var text = $('#popup_add_notif_succ .p_add_succ').text('');
            var text_new = '';
            text_new += 'Thêm mới phiếu kiểm kê kho';
            text_new += '<strong>';
            text_new += '</strong>';
            text_new += '&nbspthành công!';
            text.append(text_new);
          }
        });
      } else {
        alert("Vui lòng điền đầy đủ thông tin!");
      }
    }
    if (trang_thai === "9" || trang_thai === "10" || trang_thai === "11" || trang_thai === "12" || trang_thai === "14") {
      var vat_tu = []
      $(".select_tb_3").each(function() {
        var id_vattu = $(this).val();
        var soluong = $(this).parent().parent().find('.nhap_sl_3').val();
        var ghichu = $(this).parent().parent().find('.nhap_ghi_chu').val();
        var sltht = $(this).parents('tr').find('.sl_hethong').html().trim();
        if (id_vattu != "") {
          vat_tu.push({
            'id': id_vattu,
            'soluong': soluong,
            'ghichu': ghichu,
            'sltht': sltht
          });
        }
      });
      console.log(vat_tu)

      var check_empty_arr = 0;
      for (var i = 0; i < vat_tu.length; i++) {
        var check_empty = vat_tu[i].soluong;
        if (check_empty == "") {
          check_empty_arr++;
        }
      }

      form_vali.validate({
        errorPlacement: function(error, element) {
          error.appendTo(element.parents(".box_wh_in"));
          error.appendTo(element.parents(".box_wh_ex"));
          error.appendTo(element.parents(".input_date_fn"));
          error.wrap("<span class='error'>");
        },
        rules: {
          ivt_person: "required",
          select_wh_ex: "required",
          input_date_fn: "required",
        },
        messages: {
          ivt_person: "Chọn người kiểm kê.",
          select_wh_ex: "Chọn kho.",
          input_date_fn: "Chọn ngày hoàn thành.",
        }
      });

      if (form_vali.valid() === true && check_empty_arr === 0 && vat_tu.length > 0) {
        var vat_tu = JSON.stringify(vat_tu)
        $.ajax({
          url: '../ajax/edit_kiem_ke_kho.php',
          type: 'POST',
          data: {
            id_phieu: id_phieu,
            trang_thai: trang_thai,
            ngay_hoan_thanh: ngay_hoan_thanh,
            nguoi_tao: nguoi_tao,
            ngay_tao: ngay_tao,
            kho: kho,
            nguoi_thuc_hien: nguoi_thuc_hien,
            ngay_thuc_hien: ngay_thuc_hien,
            gio_thuc_hien: gio_thuc_hien,
            ngay_yc_hoan_thanh: ngay_yc_hoan_thanh,
            gio_yc_hoan_thanh: gio_yc_hoan_thanh,
            ghi_chu: ghi_chu,
            id_cty: id_cty,
            vat_tu: vat_tu
          },
          success: function(data) {
            $('.popup_add_notif_succ').show();
            var text = $('#popup_add_notif_succ .p_add_succ').text('');
            var text_new = '';
            text_new += 'Thêm mới phiếu kiểm kê kho';
            text_new += '<strong>';
            text_new += '</strong>';
            text_new += '&nbspthành công!';
            text.append(text_new);
          }
        });
      } else {
        alert("Vui lòng điền đầy đủ thông tin!");
      }
    }
  });

  $('.btn_close').click(function() {
    window.location.href = "/kiem-ke-kho.html"
  });


  function resetFormValidator(formId) {
    $(formId).removeData('validator');
    $(formId).removeData('unobtrusiveValidation');
    $.validator.unobtrusive.parse(formId);
  }

  function thong_ke(id) {
    var sl = Number($(id).val());
    // var vitri = $(id).parents('tr');
    var dg = $(id).parent().parent().find('.sltht').text();
    var cal = dg - sl;
    if (cal < 0) {
      $(id).parent().parent().find('.sl_thua').text(Math.abs(cal));
      $(id).parent().parent().find('.sl_thieu').text(0);
    } else {
      $(id).parent().parent().find('.sl_thua').text(0);
      $(id).parent().parent().find('.sl_thieu').text(cal);
    }
  };

  function change_value() {
    $(".select_tb").change(function() {
      var value = $(this).val();
      var _this = $(this);
      var trang_thai = $('.select_stt').val();
      var value1 = $('.select_wh_ex').val()
      var id_cty = id_ct_change
      $.ajax({
        url: '../ajax/get_dsvt_kho_kiem_ke.php',
        type: 'POST',
        data: {
          id_vt: value,
          trang_thai: trang_thai,
          id_kho: value1,
          id_cty: id_cty
        },
        success: function(data) {
          console.log(data)
          _this.parent().parent().html(data);
        }
      });
    });
  }
</script>

</html>