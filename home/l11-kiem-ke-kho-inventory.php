<?php include("config1.php");
$id_phieu = getValue('id', 'int', 'GET', '');

if (isset($_COOKIE['acc_token']) && isset($_COOKIE['rf_token']) && isset($_COOKIE['role']) && $_COOKIE['role'] == 2) {
    $curl = curl_init();
    $token = $_COOKIE['acc_token'];
    curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/list_all_my_partner.php?get_all=true');
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));
    $response = curl_exec($curl);
    curl_close($curl);

    $data_list = json_decode($response, true);
    $data_list_nv = $data_list['data']['items'];
} else {
    $curl = curl_init();
    $token = $_COOKIE['acc_token'];
    curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/list_all_employee_of_company.php');
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));
    $response = curl_exec($curl);
    curl_close($curl);

    $data_list = json_decode($response, true);
    $data_list_nv = $data_list['data']['items'];
}
$count = count($data_list_nv);

$user = [];
for ($i = 0; $i < count($data_list_nv); $i++) {
    $nv = $data_list_nv[$i];
    $user[$nv["ep_id"]] = $nv;
}

if (isset($_COOKIE['user']) && $_COOKIE['user'] != "" && $_COOKIE['role'] == 2) {
    $token = $_COOKIE['acc_token'];
    $curl = curl_init();
    $data = array();
    curl_setopt($curl, CURLOPT_POST, 1);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
    curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/user_info_employee.php');
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));
    $response = curl_exec($curl);
    curl_close($curl);
    $data_tt = json_decode($response, true);
    $tt_user = $data_tt['data']['user_info_result'];
}
$id_cty = $tt_user['com_id'];
$user_id = $_SESSION['ep_id'];
$date = date('Y-m-d', time());
$stt = 1;

$item = new db_query("SELECT `slvt_so_luong_he_thong`, `slvt_maVatTuThietBi` ,`slvt_maKho` FROM `so-luong-vat-tu` 
    WHERE `slvt_maPhieu` = 'PKK' AND `slvt_idPhieu` = $id_phieu 
    ORDER BY `slvt_id` ASC
    ");

?>
<!DOCTYPE html>
<html lang="vi">

<head>
    <title>Chi tiết phiếu kiểm kê</title>
    <meta charset="UTF-8">
    <meta name="robots" content="noindex,nofollow" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="../css/style.css?v=<?= $ver ?>">
    <link rel="stylesheet" type="text/css" href="../css/style_h.css?v=<?= $ver ?>">
    <link rel="stylesheet" type="text/css" href="../css/style_n.css?v=<?= $ver ?>">
    <link rel="stylesheet" type="text/css" href="../css/style_k.css?v=<?= $ver ?>">
</head>

<body>

    <div class="box_right inventory_detail1 table_inventory">
        <div class="box_right_ct">
            <?php include("../includes/sidebar.php"); ?>
            <div class="header_menu_overview d_flex align_c space_b">
                <p class="color_grey line_16 font_s14 text_link_page" style="display: block;">
                    <a href="/kiem-ke-kho.html" class="cursor_p">
                        <img src="../images/back_item_g.png" alt=""> </a>
                    Kiểm kê kho /chi tiết phiếu kiểm kê / kiểm kê
                </p>
                <img class="open_sidebar_w" src="../images/open_sidebar_w.png" alt="" onclick="toggle('main_sidebar')" style="display: none;">
                <?php include('../includes/header.php');  ?>
            </div>
            <div class="block_wh_tf_add">
                <div class="head_wh d_flex space_b align_c" style="display: none;">
                    <div class=" d_flex align_c">
                        <a href="/kiem-ke-kho.html" class="cursor_p">
                            <img src="../images/back_item_g.png" alt="">
                        </a>
                        <p class="color_grey line_16 font_s14 ml_10">
                            Kiểm kê / Chi tiết phiếu kiểm kê / kiểm kê
                        </p>
                    </div>
                </div>
                <!-- Danh sách vật tư -->
                <p class="tit_table_vt color_blue font_s16 line_h19 font_w700">Danh sách vật tư</p>
                <div class="operation_wh position_r d_flex align_c">
                    <div class="table_ds_vt table_vt_scr">
                        <table style="width: 1879px" class="table_inventory_detail">
                            <tr class="tit_tbl color_white font_s16 line_h19 font_w500 back_blue">
                                <td rowspan="2">Mã vật tư thiết bị</td>
                                <td rowspan="2">Tên đầy đủ vật tư thiết bị</td>
                                <td rowspan="2">Đơn vị tính</td>
                                <td rowspan="2">Hãng sản xuất</td>
                                <td rowspan="2">Xuất xứ</td>
                                <td rowspan="2">Số lượng trên hệ thống</td>
                                <td rowspan="2">Số lượng kiểm kê</td>
                                <td colspan="2">Chênh lệch </td>
                                <td rowspan="2" style="border:1px solid #CCCCCC;">Ghi chú</td>
                            </tr>
                            <tr class="tit_tbl color_white font_s16 line_h19 font_w500 back_blue">
                                <td style="border-radius:0%;">Thừa</td>
                                <td style="border-radius:0%;border: 1px solid #CCCCCC;">Thiếu</td>
                            </tr>
                            <?php while (($selected = mysql_fetch_assoc($item->result))) {
                                $idvt = $selected['slvt_maVatTuThietBi'];
                                $list_all = new db_query("SELECT `dsvt_name`, `hsx_name`, `dvt_name`, `xx_name`,`dsvt_donGia` FROM `danh-sach-vat-tu`
                                    LEFT JOIN `hang-san-xuat` on `dsvt_hangSanXuat` = `hsx_id`       
                                    LEFT JOIN `don-vi-tinh` on `dsvt_donViTinh` = `dvt_id`
                                    LEFT JOIN `xuat-xu` on `dsvt_xuatXu` = `xx_id`
                                    WHERE `dsvt_check` = 1 AND `dsvt_id` = $idvt AND `dsvt_id_ct` = $id_cty");
                                $ttvt = mysql_fetch_assoc($list_all->result)
                            ?>
                                <tr class="color_grey font_s14 line_h17 font_w400" data="<?= $idvt ?>" value="<?= $selected['slvt_maKho'] ?>">
                                    <td>VT - <?= $idvt ?> </td>
                                    <td class="color_blue font_w500" style="text-align: left;"><?= $ttvt['dsvt_name'] ?></td>
                                    <td><?= $ttvt['dvt_name'] ?></td>
                                    <td style="text-align: left;"><?= $ttvt['hsx_name'] ?></td>
                                    <td><?= $ttvt['xx_name'] ?></td>
                                    <td style="text-align: right;" class="slht"><?= $selected['slvt_so_luong_he_thong'] ?></td>
                                    <td class="font_s14 line_h17 color_grey font_w400" style="text-align: right;padding: 0px 15px 0px 0px;"><input class="nhap_sl" placeholder="Nhập số lượng" onkeyup="changeValue(this)"></td>
                                    <td class="thua"></td>
                                    <td class="thieu"></td>
                                    <td style="text-align: left;padding: 0px 0px 0px 15px;"><input class="ghi_chu" style="text-align: left;" type="text" placeholder="Nhập ghi chú"></td>
                                </tr>
                            <? } ?>
                        </table>
                    </div>
                    <div class="pre_q d_flex align_c flex_center position_a">
                        <span class="pre_arrow"></span>
                    </div>
                    <div class="next_q d_flex align_c flex_center position_a">
                        <span class="next_arrow"></span>
                    </div>
                </div>
                <div class="btn_ivt_kk d_flex flex_center" style="display: flex;">
                    <a href="/kiem-ke-kho-chi-tiet-<?= $id_phieu ?>.html">
                        <button class="btn_cancel back_w color_blue font_s15 line_h18 font_w500">Hủy</button>
                    </a>
                    <button class="btn_save back_blue color_white font_s15 line_h18 font_w500">Lưu</button>
                    <button class="btn_comp back_green color_white font_s15 line_h18 font_w500">Hoàn thành</button>
                </div>
            </div>
        </div>
    </div>
    <?php include("../includes/popup_kiem-ke.php"); ?>
    </div>
    <?php include('../includes/popup_overview.php');  ?>
</body>
<script type="text/javascript" src="../js/jquery-3.4.1.min.js"></script>
<script type="text/javascript" src="../js/jquery.validate.min.js"></script>
<script type="text/javascript" src="../js/select2.min.js"></script>
<script type="text/javascript" src="../js/js_h.js"></script>
<script type="text/javascript" src="../js/validate_h.js"></script>
<script type="text/javascript" src="../js/js_k.js"></script>
<script type="text/javascript" src="../js/js_n.js"></script>
<script>
    $('.active11').each(function() {
        if ($(this).hasClass('active11')) {
            $(this).find('a').addClass('active');
        }
    });

    function changeValue(data) {
        var sl = parseInt($(data).val());
        var vitri = $(data).parents('tr');
        var slht = parseInt(vitri.find('.slht').html())
        console.log(typeof sl, typeof slht)
        if (sl > slht) {
            vitri.find('.thieu').text('0')
            var text = sl - slht
            vitri.find('.thua').text(text)
        }
        if (sl < slht) {
            vitri.find('.thua').text('0')
            var text = slht - sl
            vitri.find('.thieu').text(text)
        }
    }

    $('.btn_save').click(function() {
        vat_tu = []
        $(".nhap_sl").each(function() {
            var sl = $(this).val();
            var vitri = $(this).parents('tr');
            var id = vitri.attr('data')
            var kho = vitri.attr('value')
            var ghichu = vitri.find('.ghi_chu').val();
            var slht = vitri.find('.slht').text().trim();
            if (sl != "") {
                vat_tu.push({
                    'id': id,
                    'sl': sl,
                    'kho': kho,
                    'ghichu': ghichu,
                    'slht': slht,
                });
            }
        });
        var vat_tu = JSON.stringify(vat_tu)
        var id_phieu = "<?= $id_phieu ?>"
        var id_cty = "<?= $id_cty ?>"
        var value = "14"
        $.ajax({
            url: '../ajax/edit_pkk.php',
            type: 'POST',
            // dataType: 'Json',
            data: {
                id_pkk: id_phieu,
                value: value,
                id_cty: id_cty,
                vat_tu: vat_tu,
            },
            success: function(response) {
                $('#popup_update_kk').hide();
                $('#popup_add_notif_succ_kk').show();
                var text = $('#popup_add_notif_succ_kk .p_add_succ').text('');
                var text_new = '';
                var name = $("input[name='name_full_dv']").val();
                text_new += 'Xóa phiếu kiểm kê PKK-';
                text_new += '<strong>';
                text_new += '&nbsp' + id_phieu;
                text_new += '</strong>';
                text_new += '&nbspthành công!';
                text.append(text_new);
            }
        })
    })
    $('.btn_comp').click(function() {
        vat_tu = []
        $(".nhap_sl").each(function() {
            var sl = $(this).val();
            var vitri = $(this).parents('tr');
            var id = vitri.attr('data')
            var kho = vitri.attr('value')
            var ghichu = vitri.find('.ghi_chu').val();
            var slht = vitri.find('.slht').text().trim();
            if (sl != "") {
                vat_tu.push({
                    'id': id,
                    'sl': sl,
                    'kho': kho,
                    'ghichu': ghichu,
                    'slht': slht,
                });
            }
        });
        var id_phieu = "<?= $id_phieu ?>"
        var id_cty = "<?= $id_cty ?>"
        var value = "9"
        var vat_tu = JSON.stringify(vat_tu)
        $.ajax({
            url: '../ajax/edit_pkk.php',
            type: 'POST',
            // dataType: 'Json',
            data: {
                id_pkk: id_phieu,
                value: value,
                id_cty: id_cty,
                vat_tu: vat_tu,
            },
            success: function(response) {
                $('#popup_update_kk').hide();
                $('#popup_add_notif_succ_kk').show();
                var text = $('#popup_add_notif_succ_kk .p_add_succ').text('');
                var text_new = '';
                var name = $("input[name='name_full_dv']").val();
                text_new += 'Xóa phiếu kiểm kê PKK-';
                text_new += '<strong>';
                text_new += '&nbsp' + id_phieu;
                text_new += '</strong>';
                text_new += '&nbspthành công!';
                text.append(text_new);
            }
        })
    })
</script>

</html>