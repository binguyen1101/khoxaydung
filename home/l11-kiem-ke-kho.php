<?php 

  include("config1.php"); 

  if(!in_array(1,$ro_kk_kho)){
    header("Location: /tong-quan.html");
  }
  
  isset($_GET['page']) ? $page = $_GET['page'] : $page = 1;
  isset($_GET['all_k']) ? $all_k = $_GET['all_k'] : $all_k = "";
  isset($_GET['th']) ? $th = $_GET['th'] : $th = "";

  isset($_GET['ngts']) ? $ngts = $_GET['ngts'] : $ngts = "";
  isset($_GET['ngte']) ? $ngte = $_GET['ngte'] : $ngte = "";

  isset($_GET['ngths']) ? $ngths = $_GET['ngths'] : $ngths = "";
  isset($_GET['ngthe']) ? $ngthe = $_GET['ngthe'] : $ngthe = "";

  isset($_GET['ngychts']) ? $ngychts = $_GET['ngychts'] : $ngychts = "";
  isset($_GET['ngychte']) ? $ngychte = $_GET['ngychte'] : $ngychte = "";

  isset($_GET['nghts']) ? $nghts = $_GET['nghts'] : $nghts = "";
  isset($_GET['nghte']) ? $nghte = $_GET['nghte'] : $nghte = "";

  isset($_GET['input']) ? $ip = $_GET['input'] : $ip = "";
  isset($_GET['dis']) ? $dis = $_GET['dis'] : $dis = 10;


  if (isset($_COOKIE['user']) && $_COOKIE['user'] != "" && $_COOKIE['role'] == 2) {
		$token = $_COOKIE['acc_token'];
		$curl = curl_init();
		$data = array();
		curl_setopt($curl, CURLOPT_POST, 1);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
		curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/user_info_employee.php');
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));
		$response = curl_exec($curl);
		curl_close($curl);
		$data_tt = json_decode($response, true);
		$tt_user = $data_tt['data']['user_info_result'];
	}

  $id_cty = $tt_user['com_id'];

  $all_kho = new db_query("SELECT `kho_id`, `kho_name`
  FROM `kho`
  WHERE `kho_id_ct` = $id_cty
  ORDER BY `kho_id` DESC
  ");

?>
<!DOCTYPE html>
<html lang="vi">

<head>
  <title>Kiểm kê</title>
  <meta charset="UTF-8">
  <meta name="robots" content="noindex,nofollow" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" type="text/css" href="../css/select2.min.css?v=<?= $ver ?>">
  <link rel="stylesheet" type="text/css" href="../css/style.css?v=<?= $ver ?>">
  <link rel="stylesheet" type="text/css" href="../css/style_h.css?v=<?= $ver ?>">
  <link rel="stylesheet" type="text/css" href="../css/style_n.css?v=<?= $ver ?>">
  <link rel="stylesheet" type="text/css" href="../css/style_k.css?v=<?= $ver ?>">
</head>

<body>

  <div class="box_right inventory_main" style="display: flex;">
    <div class="box_right_ct">
      <div class="wrapper_all">
        <?php include('../includes/sidebar.php');  ?>
      </div>
      <div class="header_menu_overview d_flex align_c space_b">
        <p class="color_grey line_16 font_s14 text_link_page" style="display: block;">Kiểm kê </p>
        <img class="open_sidebar_w" src="../images/open_sidebar_w.png" alt="" onclick="toggle('main_sidebar')" style="display: none;">
        <?php include('../includes/header.php');  ?>
      </div>
      <div class="block_wh_tf">
        <p class="color_grey line_16 font_s14 title_wh" style="display: none;">Kiểm kê </p>
        <div class="filter_dtl">
          <div class="b_fil d_flex" style="flex-wrap: wrap;">
            <div class="all_ex position_r">
              <select class="select_all_wh" name="all_kho" style="width: 100%">
                <option value="">Tất cả các kho</option>
                <?php while($row_all_kho = mysql_fetch_assoc($all_kho->result)) {?>
                    <option value="<?= $row_all_kho['kho_id']; ?>" <?= ($all_k == $row_all_kho['kho_id']) ? "selected" : "" ?>><?= $row_all_kho['kho_name']; ?></option>
                <? } ?>
              </select>
            </div>
            <div class="date_cr d_flex space_b">
                <p class="color_grey font_s14 line_h17 font_w400">Ngày tạo: <span class="date_start" id="date_cr_start" data="<?= $ngts ?>"><?= ($ngts != "") ?  $ngts : "yyyy/mm/dd" ?></span> -
                    <span class="date_end" id="date_cr_end" data="<?= $ngte ?>"><?= ($ngte != "") ?  $ngte : "yyyy/mm/dd" ?></span>
                </p>
                <img class="cursor_p" src="../images/date.png" alt="">
            </div>
            <div class="date_pf d_flex space_b">
              <p class="color_grey font_s14 line_h17 font_w400">Ngày thực hiện: <span class="date_pf_start" id="date_pf_start" data="<?= $ngths ?>"><?= ($ngths != "") ?  $ngths : "yyyy/mm/dd" ?></span>
                - <span class="date_end" id="date_pf_end" data="<?= $ngthe ?>"><?= ($ngthe != "") ?  $ngthe : "yyyy/mm/dd" ?></span></p>
              <img class="cursor_p" src="../images/date.png" alt="">
            </div>

            <div class="all_iv_tf position_r">
              <select class="select_iv_tf" name="all_iv_tf" style="width: 100%">
                <option value="">Tất cả trạng thái phiếu kiểm kê</option>
                <option value="8" <?= ($th == 8) ? "selected" : "" ?>>Chờ kiểm kê</option>
                <option value="9" <?= ($th == 9) ? "selected" : "" ?>>Kiểm kê chờ duyệt</option>
                <option value="10" <?= ($th == 10) ? "selected" : "" ?>>Từ chối duyệt kiểm kê</option>
                <option value="11" <?= ($th == 11) ? "selected" : "" ?>>Duyệt kiểm kê</option>
                <option value="12" <?= ($th == 12) ? "selected" : "" ?>>Đã cập nhật kho</option>
              </select>
            </div>
            <div class="date_rq d_flex space_b">
              <p class="color_grey font_s14 line_h17 font_w400">Ngày yêu cầu hoàn thành: <span class="date_rq_start" id="date_rq_start" data="<?= $ngychts ?>"><?= ($ngychts != "") ?  $ngychts : "yyyy/mm/dd" ?></span> 
              - <span class="date_end" id="date_rq_end" data="<?= $ngychte ?>"><?= ($ngychte != "") ?  $ngychte : "yyyy/mm/dd" ?></span></p>
              <img class="cursor_p" src="../images/date.png" alt="">
            </div>
            <div class="date_fn d_flex space_b">
              <p class="color_grey font_s14 line_h17 font_w400">Ngày hoàn thành: <span class="date_fn_start" id="date_fn_start" data="<?= $nghts ?>"><?= ($nghts != "") ?  $nghts : "yyyy/mm/dd" ?></span>
                - <span class="date_fn_end" class="date_fn_end" id="date_fn_end" data="<?= $nghte ?>"><?= ($nghte != "") ?  $nghte : "yyyy/mm/dd" ?></span></p>
              <img class="cursor_p" src="../images/date.png" alt="">
            </div>
          </div>
        </div>
        <div class="operation_wh d_flex space_b" style="display: flex;">
          <div class="search_wh d_flex space_b" style="display: block;">
            <div class="input_sr_wh position_r">
              <input type="text" name="input_search" value="<?= ($ip != "") ? $ip : "" ?>" placeholder="Tìm kiếm theo số phiếu">
              <span class="icon_sr_wh"></span>
            </div>
          </div>
          <div class="export_wh d_flex space_b align_c">
          <?php if(in_array(2,$ro_kk_kho)){ ?>
            <button class="btn_px d_flex align_c">
              <a href="kiem-ke-kho-them-moi.html" class="mr_15"></a>
              <img src="../images/img_px.png" alt="">
              <p class="color_white font_s15 line_h18 font_w500">Thêm mới</p>
            </button>
          <?php }?>
            <button class="btn_ex d_flex align_c">
              <img src="../images/export.png" alt="">
              <p class="color_white font_s15 line_h18 font_w500">Xuất excel</p>
            </button>
            <div class="hd_ex d_flex align_c">
              <img src="../images/img_hd.png" alt="">
              <p class="color_blue font_s15 line_h18 font_w500">Hướng dẫn</p>
            </div>
          </div>
        </div>
        <div class="detail_table" data-page="<?= $page ?>" data-k="<?= $all_k ?>" data-th="<?= $th ?>" data-ngts="<?= $ngts?>" data-ngte="<?= $ngte?>" 
        data-ngths="<?= $ngths?>" data-ngthe="<?= $ngthe?>" data-ngychts="<?= $ngychts?>" data-ngychte="<?= $ngychte?>" data-nghts="<?= $nghts?>" data-nghte="<?= $nghte?>"
        data-ip ="<?= $ip ?>" data-dis ="<?= $dis ?>">
                    <!--  -->
                </div>
      </div>
    </div>
  <?php include('../includes/popup_kiem-ke.php');  ?>

  </div>
  <?php include('../includes/popup_overview.php');  ?>
</body>
<script type="text/javascript" src="../js/jquery-3.4.1.min.js"></script>
<script type="text/javascript" src="../js/jquery.validate.min.js"></script>
<script type="text/javascript" src="../js/select2.min.js"></script>
<script type="text/javascript" src="../js/js_h.js"></script>
<script type="text/javascript" src="../js/validate_h.js"></script>
<script type="text/javascript" src="../js/js_k.js"></script>
<script type="text/javascript" src="../js/js_n.js"></script>
<script type="text/javascript" src="../js/kiem_ke_kho.js"></script>

</html>