<?php 
  include("config1.php"); 

  if(!in_array(1,$ro_bao_cao)){
    header("Location: /tong-quan.html");
  }

  isset($_GET['page']) ? $page = $_GET['page'] : $page = 1;
  isset($_GET['kho']) ? $k = $_GET['kho'] : $k = "";
  isset($_GET['hinhthuc']) ? $ht = $_GET['hinhthuc'] : $ht = "";

  isset($_GET['ngts']) ? $ngts = $_GET['ngts'] : $ngts = "";
  isset($_GET['ngte']) ? $ngte = $_GET['ngte'] : $ngte = "";

  isset($_GET['ngths']) ? $ngths = $_GET['ngths'] : $ngths = "";
  isset($_GET['ngthe']) ? $ngthe = $_GET['ngthe'] : $ngthe = "";


  isset($_GET['input']) ? $ip = $_GET['input'] : $ip = "";
  isset($_GET['dis']) ? $dis = $_GET['dis'] : $dis = 10;


  if (isset($_COOKIE['user']) && $_COOKIE['user'] != "" && $_COOKIE['role'] == 1) {
		$token = $_COOKIE['acc_token'];
		$curl = curl_init();
		$data = array();
		curl_setopt($curl, CURLOPT_POST, 1);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
		curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/user_info_company.php');
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));
	
		$response = curl_exec($curl);
		curl_close($curl);
		$data_tt = json_decode($response, true);
		$tt_user = $data_tt['data']['user_info_result'];
	} else if (isset($_COOKIE['user']) && $_COOKIE['user'] != "" && $_COOKIE['role'] == 2) {
		$token = $_COOKIE['acc_token'];
		$curl = curl_init();
		$data = array();
		curl_setopt($curl, CURLOPT_POST, 1);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
		curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/user_info_employee.php');
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));
		$response = curl_exec($curl);
		curl_close($curl);
		$data_tt = json_decode($response, true);
		$tt_user = $data_tt['data']['user_info_result'];
	}
  $id_cty = $tt_user['com_id'];

  $kho = new db_query("SELECT `kho_id`, `kho_name`
  FROM `kho` WHERE `kho_id_ct` = $id_cty
  ORDER BY `kho_id` DESC
  ");

?>
<!DOCTYPE html>
<html lang="vi">

<head>
  <title>Báo cáo nhập kho</title>
  <meta charset="UTF-8">
  <meta name="robots" content="noindex,nofollow" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" type="text/css" href="../css/select2.min.css?v=<?= $ver ?>">
  <link rel="stylesheet" type="text/css" href="../css/style.css?v=<?= $ver ?>">
  <link rel="stylesheet" type="text/css" href="../css/style_h.css?v=<?= $ver ?>">
  <link rel="stylesheet" type="text/css" href="../css/style_n.css?v=<?= $ver ?>">
  <link rel="stylesheet" type="text/css" href="../css/style_k.css?v=<?= $ver ?>">
</head>

<body>

  <div class="box_right report_wh_in">
    <div class="box_right_ct">
      <?php include("../includes/sidebar.php"); ?>
      <div class="block_change block_wh_tf">
        <div class="head_wh d_flex space_b align_c">
        <div class="head_tab d_flex space_b align_c">
            <div class="icon_header open_sidebar_w">
              <span class="icon_header_tbl"></span>
              <span class="icon_header_tbl"></span>
              <span class="icon_header_tbl"></span>
            </div>
            <?php include("../includes/header.php") ; ?>
          </div>
          <p class="color_grey font_s14 line_h17 font_w400">Báo cáo / Nhập kho
          </p>
          <?php include("../includes/header.php") ; ?>
        </div>
        <div class="filter_dtl">
          <div class="t_fil d_flex flex_w">
            <div class="all_ex position_r">
              <select class="select_all_wh" name="all_ex" style="width: 100%">
                <option value="">Tất cả các kho</option>
                <?php while($row_kho = mysql_fetch_assoc($kho->result)){?>
                  <option value="<?=$row_kho['kho_id'];?>" <?= ($k == $row_kho['kho_id']) ? "selected" : "" ?>><?=$row_kho['kho_name'];?></option>
                <?php } ?>
              </select>
            </div>
            <div class="date_cr d_flex space_b">
              <p class="color_grey font_s14 line_h17 font_w400">Ngày tạo: 
                <span class="date_start" id="date_cr_start" data="<?= $ngts ?>"><?= ($ngts != "") ?  $ngts : "yyyy/mm/dd" ?></span> -
                <span class="date_end" id="date_cr_end" data="<?= $ngte ?>"><?= ($ngte != "") ?  $ngte : "yyyy/mm/dd" ?></span>
              </p>
                <img class="cursor_p" src="../images/date.png" alt="">
            </div>
            <div class="date_fn d_flex space_b">
              <p class="color_grey font_s14 line_h17 font_w400">Ngày hoàn thành: <span class="date_fn_start" id="date_fn_start" data="<?= $ngths ?>"><?= ($ngths != "") ?  $ngths : "yyyy/mm/dd" ?></span>
                - <span class="date_end" class="date_fn_end" id="date_fn_end" data="<?= $ngthe ?>"><?= ($ngthe != "") ?  $ngthe : "yyyy/mm/dd" ?></span></p>
              <img class="cursor_p" src="../images/date.png" alt="">
            </div>
            <div class="all_in_wh position_r">
              <select class="select_all_in_wh" name="all_st_tf" style="width: 100%">
                <option value="">Tất cả hình thức nhập kho</option>
                <option value="NK1" <?= ($ht == 'NK1') ? "selected" : "" ?>>Nhập theo biên bản giao hàng</option>
                <option value="NK2" <?= ($ht == 'NK2') ? "selected" : "" ?>>Nhập điều chuyển</option>
                <option value="NK3" <?= ($ht == 'NK3') ? "selected" : "" ?>>Nhập trả lại từ thi công</option>
                <option value="NK4" <?= ($ht == 'NK4') ? "selected" : "" ?>>Nhập theo yêu cầu vật tư</option>
                <option value="NK5" <?= ($ht == 'NK5') ? "selected" : "" ?>>Nhập khác</option>
              </select>
            </div>
          </div>
        </div>
        <div class="operation_wh d_flex space_b">
          <div class="search_wh d_flex space_b">
            <div class="input_sr_wh">
              <div class="box_input_sr position_r">
                <input name="input_search" type="text" value="<?= ($ip != "") ? $ip : "" ?>" placeholder="Tìm kiếm theo số phiếu">
                <span class="icon_sr_wh"></span>
              </div>
            </div>
          </div>
          <div class="export_wh d_flex space_b align_c">
            <button class="btn_ex d_flex align_c cursor_p">
              <img src="../images/export.png" alt="">
              <p class="color_white font_s15 line_h18 font_w500">Xuất excel</p>
            </button>
            <div class="hd_ex d_flex align_c cursor_p">
              <img src="../images/img_hd.png" alt="">
              <p class="color_blue font_s15 line_h18 font_w500">Hướng dẫn</p>
            </div>
          </div>
        </div>
        <div class="detail_wh" data-page="<?= $page ?>" data-k="<?= $k ?>" data-ht="<?= $ht ?>" data-ngts="<?= $ngts?>" data-ngte="<?= $ngte?>"
        data-ngths="<?= $ngths?>" data-ngthe="<?= $ngthe?>" data-ip ="<?= $ip ?>" data-dis ="<?= $dis ?>">
          
        </div>
      </div>
    </div>
  </div>
  <?php include('../includes/popup_overview.php');  ?>
  <?php include("../includes/popup_chon_ngay.php"); ?>
</body>
<script type="text/javascript" src="../js/jquery-3.4.1.min.js"></script>
<script type="text/javascript" src="../js/jquery.validate.min.js"></script>
<script type="text/javascript" src="../js/select2.min.js"></script>
<script type="text/javascript" src="../js/js_h.js"></script>
<script type="text/javascript" src="../js/validate_h.js"></script>
<script type="text/javascript" src="../js/js_k.js"></script>
<script type="text/javascript" src="../js/js_n.js"></script>
<script type="text/javascript" src="../js/bao_cao_nk.js"></script>
<script>
$('.active12').each(function() {
  if ($(this).hasClass('active12')) {
    $(this).find('a').addClass('active');
  }
});
</script>

</html>