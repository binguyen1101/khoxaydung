<?php 
  include("config1.php"); 
  
  if(!in_array(1,$ro_bao_cao)){
    header("Location: /tong-quan.html");
  }

  isset($_GET['page']) ? $page = $_GET['page'] : $page = 1;
  isset($_GET['kho']) ? $k = $_GET['kho'] : $k = "";
  isset($_GET['ngay_tao']) ? $nt = $_GET['ngay_tao'] : $nt = "";
  // isset($_GET['sort']) ? $sort = $_GET['sort'] : $sort = "";
  isset($_GET['input']) ? $ip = $_GET['input'] : $ip = "";
  isset($_GET['dis']) ? $dis = $_GET['dis'] : $dis = 10;

  if (isset($_COOKIE['user']) && $_COOKIE['user'] != "" && $_COOKIE['role'] == 1) {
		$token = $_COOKIE['acc_token'];
		$curl = curl_init();
		$data = array();
		curl_setopt($curl, CURLOPT_POST, 1);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
		curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/user_info_company.php');
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));
	
		$response = curl_exec($curl);
		curl_close($curl);
		$data_tt = json_decode($response, true);
		$tt_user = $data_tt['data']['user_info_result'];
	} else if (isset($_COOKIE['user']) && $_COOKIE['user'] != "" && $_COOKIE['role'] == 2) {
		$token = $_COOKIE['acc_token'];
		$curl = curl_init();
		$data = array();
		curl_setopt($curl, CURLOPT_POST, 1);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
		curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/user_info_employee.php');
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));
		$response = curl_exec($curl);
		curl_close($curl);
		$data_tt = json_decode($response, true);
		$tt_user = $data_tt['data']['user_info_result'];
	}
    
  $id_cty = $tt_user['com_id'];

  $kho = new db_query("SELECT `kho_id`, `kho_name`
  FROM `kho` WHERE `kho_id_ct` = $id_cty 
  ORDER BY `kho_id` DESC
  ");


?>
<!DOCTYPE html>
<html lang="vi">



<head>
  <title>Báo cáo tồn kho</title>
  <meta charset="UTF-8">
  <meta name="robots" content="noindex,nofollow" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" type="text/css" href="../css/select2.min.css?v=<?= $ver ?>">
  <link rel="stylesheet" type="text/css" href="../css/style.css?v=<?= $ver ?>">
  <link rel="stylesheet" type="text/css" href="../css/style_h.css?v=<?= $ver ?>">
  <link rel="stylesheet" type="text/css" href="../css/style_n.css?v=<?= $ver ?>">
  <link rel="stylesheet" type="text/css" href="../css/style_k.css?v=<?= $ver ?>">
</head>

<body>

  <div class="box_right report_wh_inventory" style="display: flex;">
    <div class="box_right_ct">
      <?php include("../includes/sidebar.php"); ?>
      <div class="block_change block_wh_tf">
        <div class="head_wh d_flex space_b align_c">
        <div class="head_tab d_flex space_b align_c">
            <div class="icon_header open_sidebar_w">
              <span class="icon_header_tbl"></span>
              <span class="icon_header_tbl"></span>
              <span class="icon_header_tbl"></span>
            </div>
            <?php include("../includes/header.php") ; ?>
          </div>
          <p class="color_grey font_s14 line_h17 font_w400">Báo cáo / Tồn kho
          </p>
          <?php include("../includes/header.php") ; ?>
        </div>
        <div class="filter_dtl">
          <div class="t_fil d_flex flex_w">
            <div class="all_ex position_r">
              <select class="select_all_wh" name="all_ex" style="width: 100%">
                  <option value="">Chọn kho</option>
                <?php while($row_kho = mysql_fetch_assoc($kho->result)){?>
                  <option value="<?=$row_kho['kho_id'];?>" <?= ($k == $row_kho['kho_id']) ? "selected" : "" ?>><?=$row_kho['kho_name'];?></option>
                <?php } ?>
              </select>
            </div>
            <div class="date_invt d_flex space_b align_c position_r">
                <p class="color_grey font_s14 line_h17 font_w400">Ngày:&nbsp<span class="text_date_invt">&nbsp <input type="date" value="<?= ($nt != "") ? $nt : "mm/dd/yyyy" ?>"></span></p>
            </div>
            <!-- <div class="sort_invt position_r">
              <select class="select_sort_invt" name="sort_invt" style="width: 100%">
                <option value="">Không sắp xếp số lượng</option>
                <option value="1" <?= ($sort == 1) ? "selected" : "" ?>>Số lượng tăng dần</option>
                <option value="2" <?= ($sort == 2) ? "selected" : "" ?>>Số lượng giảm dần</option>
              </select>
            </div> -->
          </div>
        </div>
        <div class="operation_wh d_flex space_b">
          <div class="search_wh d_flex space_b">
            <div class="input_sr_wh">
              <div class="box_input_sr position_r">
                <input name="input_search" type="text" value="<?= ($ip != "") ? $ip : "" ?>" placeholder="Tìm kiếm theo mã, tên vật tư">
                <span class="icon_sr_wh"></span>
              </div>
            </div>
          </div>
          <div class="export_wh d_flex space_b align_c">
            <button class="btn_ex d_flex align_c cursor_p" data="<?= $k ?>">
              <img src="../images/export.png" alt="">
              <p class="color_white font_s15 line_h18 font_w500">Xuất excel</p>
            </button>
            <div class="hd_ex d_flex align_c cursor_p">
              <img src="../images/img_hd.png" alt="">
              <p class="color_blue font_s15 line_h18 font_w500">Hướng dẫn</p>
            </div>
          </div>
        </div>
        <div class="detail_wh" data-page="<?= $page ?>" data-k="<?= $k ?>" data-nt="<?= $nt ?>" data-ip ="<?= $ip ?>" data-sort="<?= $sort ?>"  data-dis ="<?= $dis ?>">
        </div>
      </div>
    </div>
  </div>
  <?php include('../includes/popup_overview.php');  ?>

</body>
<script type="text/javascript" src="../js/jquery-3.4.1.min.js"></script>
<script type="text/javascript" src="../js/jquery.validate.min.js"></script>
<script type="text/javascript" src="../js/select2.min.js"></script>
<script type="text/javascript" src="../js/js_h.js"></script>
<script type="text/javascript" src="../js/validate_h.js"></script>
<script type="text/javascript" src="../js/js_k.js"></script>
<script type="text/javascript" src="../js/js_n.js"></script>
<script type="text/javascript" src="../js/bao_cao_tk.js"></script>
<script>
$('.active14').each(function() {
  if ($(this).hasClass('active14')) {
    $(this).find('a').addClass('active');
  }
});
</script>

</html>