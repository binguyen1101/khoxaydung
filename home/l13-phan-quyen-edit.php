 <?php 
 
    include("config1.php");

    if(!in_array(1,$ro_phan_quyen)){
      header("Location: /tong-quan.html");
    }

    $id_user = getValue('id', 'int', 'GET', "");

    if (isset($_SESSION['quyen']) && $_SESSION['quyen'] == 1) {
        $curl = curl_init();
        $token = $_COOKIE['acc_token'];
        curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/list_all_employee_of_company.php');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));
        $response = curl_exec($curl);
        curl_close($curl);
    } elseif (isset($_SESSION['quyen']) && ($_SESSION['quyen'] == 2)) {
        $curl = curl_init();
        $token = $_COOKIE['acc_token'];
        curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/list_all_my_partner.php?get_all=true');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));
        $response = curl_exec($curl);
        curl_close($curl);
    }

    $data_list = json_decode($response, true);
    $data_list_nv = $data_list['data']['items'];

    $user = [];
    for ($i = 0; $i < count($data_list_nv); $i++){
        $nv = $data_list_nv[$i];
        $user[$nv["ep_id"]] = $nv;
    }

    if (isset($_COOKIE['user']) && $_COOKIE['user'] != "" && $_COOKIE['role'] == 2) {
      $token = $_COOKIE['acc_token'];
      $curl = curl_init();
      $data = array();
      curl_setopt($curl, CURLOPT_POST, 1);
      curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
      curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/user_info_employee.php');
      curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
      curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));
      $response = curl_exec($curl);
      curl_close($curl);
      $data_tt = json_decode($response, true);
      $tt_user = $data_tt['data']['user_info_result'];
  }
  $id_cty = $tt_user['com_id'];

  $check_ro = new db_query("SELECT * FROM `roles_user` WHERE `ro_user` = $id_user AND `ro_com` = $id_cty");
 
 ?>
<!DOCTYPE html>
<html lang="vi">

<head>
  <title>Phân quyền</title>
  <meta charset="UTF-8">
  <meta name="robots" content="noindex,nofollow" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" type="text/css" href="../css/select2.min.css?v=<?= $ver ?>">
  <link rel="stylesheet" type="text/css" href="../css/style.css?v=<?= $ver ?>">
  <link rel="stylesheet" type="text/css" href="../css/style_h.css?v=<?= $ver ?>">
  <link rel="stylesheet" type="text/css" href="../css/style_n.css?v=<?= $ver ?>">
</head>

<body>
  <div class="main_wrapper_all">
    <div class="wapper_all">
      <?php include('../includes/sidebar.php');  ?>
    </div>
    <div class="main_overview" id="main_overview">
      <div class="header_menu_overview d_flex align_c space_b">
        <div class="text_link_header_back" style="display: block;">
          <div class=" d_flex align_c">
            <a href="/phan-quyen.html" class="cursor_p">
              <img src="../images/back_item_g.png" alt="">
            </a>
            <p class="color_grey line_16 font_s14 ml_5">Phân quyền / <?=$user[$id_user]['ep_name']?></p>
          </div>
        </div>
        <img class="open_sidebar_w" src="../images/open_sidebar_w.png" alt="" onclick="toggle('main_sidebar')"
          style="display: none;">
        <?php include('../includes/header.php');  ?>
      </div>
      <div class="body_equipment_supplies" data-user="<?=$id_user?>" data-com="<?=$id_cty?>">
        <div class="text_link_body_back" style="display: none;">
          <div class=" d_flex align_c mb_15">
            <a href="/phan-quyen.html" class="cursor_p">
              <img src="../images/back_item_g.png" alt="">
            </a>
            <p class="color_grey line_16 font_s14 ml_5">Phân quyền / Nguyễn Trần Trung Quân</p>
          </div>
        </div>
        <div class="detail_wh position_r d_flex align_c">
          <div class="main_table table_vt_scr">
            <table class="table table_decentralization">
              <tr>
                <th>
                  Tên quyền
                  <span class="span_tbody"></span>
                </th>
                <th>Xem
                  <span class="span_tbody"></span>
                </th>
                <th>Thêm
                  <span class="span_tbody"></span>
                </th>
                <th>Sửa
                  <span class="span_tbody"></span>
                </th>
                <th>Xóa
                  <span class="span_tbody"></span>
                </th>
                <th>Duyệt/Xác nhận
                    <span class="span_tbody"></span>
                </th>
                <!-- <th>Xem dữ liệu công ty
                </th> -->
              </tr>
              <?php $check_input = mysql_fetch_assoc($check_ro->result) ?>
                <tr class="ro_vat_tu">
                  <td class="font_s14 line_16 color_grey font_w500">
                    <div class="flex_start d_flex">
                      Danh sách vật tư thiết bị
                    </div>
                  </td>
                  <td>
                    <input class="hw_check ro_xem" name="xem" type="checkbox" <?= in_array('1',explode(',',$check_input['ro_thiet_bi'])) ? "checked" : ""?>>
                  </td>
                  <td>
                    <input class="hw_check ro_them" name="them" type="checkbox" <?= in_array('2',explode(',',$check_input['ro_thiet_bi'])) ? "checked" : ""?>>
                  </td>
                  <td>
                    <input class="hw_check ro_sua" name="sua" type="checkbox" <?= in_array('3',explode(',',$check_input['ro_thiet_bi'])) ? "checked" : ""?>>
                  </td>
                  <td>
                    <input class="hw_check ro_xoa" name="xoa" type="checkbox" <?= in_array('4',explode(',',$check_input['ro_thiet_bi'])) ? "checked" : ""?>>
                  </td>
                  <td>
                  </td>
                  <!-- <td>
                    <input class="hw_check" name="xem_dl_cty" type="checkbox">
                  </td> -->
                </tr>
                <tr class="ro_nhom_vat_tu">
                  <td class="font_s14 line_16 color_grey font_w500">
                    <div class="flex_start d_flex">
                      Nhóm vật tư thiết bị
                    </div>
                  </td>
                  <td>
                    <input class="hw_check ro_xem" name="xem1" type="checkbox" <?= in_array('1',explode(',',$check_input['ro_nhom_thiet_bi'])) ? "checked" : ""?>>
                  </td>
                  <td>
                    <input class="hw_check ro_them" name="them1" type="checkbox" <?= in_array('2',explode(',',$check_input['ro_nhom_thiet_bi'])) ? "checked" : ""?>>
                  </td>
                  <td>
                    <input class="hw_check ro_sua" name="sua1" type="checkbox" <?= in_array('3',explode(',',$check_input['ro_nhom_thiet_bi'])) ? "checked" : ""?>>
                  </td>
                  <td>
                    <input class="hw_check ro_xoa" name="xoa1" type="checkbox" <?= in_array('4',explode(',',$check_input['ro_nhom_thiet_bi'])) ? "checked" : ""?>>
                  </td>
                  <td>
                  </td>
                  <!-- <td>
                    <input class="hw_check" name="xem_dl_cty1" type="checkbox">
                  </td> -->
                </tr>
                <tr class="ro_hang_san_xuat">
                  <td class="font_s14 line_16 color_grey font_w500">

                    <div class="flex_start d_flex">
                      Hãng sản xuất
                    </div>
                  </td>
                  <td>
                    <input class="hw_check ro_xem" name="xem2" type="checkbox" <?= in_array('1',explode(',',$check_input['ro_hang_san_xuat'])) ? "checked" : ""?>>
                  </td>
                  <td>
                    <input class="hw_check ro_them" name="them2" type="checkbox" <?= in_array('2',explode(',',$check_input['ro_hang_san_xuat'])) ? "checked" : ""?>> 
                  </td>
                  <td>
                    <input class="hw_check ro_sua" name="sua2" type="checkbox" <?= in_array('3',explode(',',$check_input['ro_hang_san_xuat'])) ? "checked" : ""?>>
                  </td>
                  <td>
                    <input class="hw_check ro_xoa" name="xoa2" type="checkbox" <?= in_array('4',explode(',',$check_input['ro_hang_san_xuat'])) ? "checked" : ""?>>
                  </td>
                  <td>
                  </td>
                  <!-- <td>
                    <input class="hw_check" name="xem_dl_cty2" type="checkbox">
                  </td> -->
                </tr>
                <tr class="ro_don_vi_tinh">
                  <td class="font_s14 line_16 color_grey font_w500">

                    <div class="flex_start d_flex">
                      Đơn vị tính
                    </div>
                  </td>
                  <td>
                    <input class="hw_check ro_xem" name="xem3" type="checkbox" <?= in_array('1',explode(',',$check_input['ro_don_vi_tinh'])) ? "checked" : ""?>>
                  </td>
                  <td>
                    <input class="hw_check ro_them" name="them3" type="checkbox" <?= in_array('2',explode(',',$check_input['ro_don_vi_tinh'])) ? "checked" : ""?>>
                  </td>
                  <td>
                    <input class="hw_check ro_sua" name="sua3" type="checkbox" <?= in_array('3',explode(',',$check_input['ro_don_vi_tinh'])) ? "checked" : ""?>>
                  </td>
                  <td>
                    <input class="hw_check ro_xoa" name="xoa3" type="checkbox" <?= in_array('4',explode(',',$check_input['ro_don_vi_tinh'])) ? "checked" : ""?>>
                  </td>
                  <td>
                  </td>
                  <!-- <td>
                    <input class="hw_check" name="xem_dl_cty3" type="checkbox">
                  </td> -->
                </tr>
                <tr class="ro_ton_kho">
                  <td class="font_s14 line_16 color_grey font_w500">

                    <div class="flex_start d_flex">
                      Tồn kho
                    </div>
                  </td>
                  <td>
                    <input class="hw_check ro_xem" name="xem4" type="checkbox" <?= in_array('1',explode(',',$check_input['ro_ton_kho'])) ? "checked" : ""?>>
                  </td>
                  <td>
                    <input class="hw_check ro_them" name="them4" type="checkbox" <?= in_array('2',explode(',',$check_input['ro_ton_kho'])) ? "checked" : ""?>>
                  </td>
                  <td>
                    <input class="hw_check ro_sua" name="sua4" type="checkbox" <?= in_array('3',explode(',',$check_input['ro_ton_kho'])) ? "checked" : ""?>>
                  </td>
                  <td>
                    <input class="hw_check ro_xoa" name="xoa4" type="checkbox" <?= in_array('4',explode(',',$check_input['ro_ton_kho'])) ? "checked" : ""?>>
                  </td>
                  <td>
                  </td>
                  <!-- <td>
                    <input class="hw_check" name="xem_dl_cty4" type="checkbox">
                  </td> -->
                </tr>
                <tr class="ro_nhap_kho">
                  <td class="font_s14 line_16 color_grey font_w500">

                    <div class="flex_start d_flex">
                      Nhập kho
                    </div>
                  </td>
                  <td>
                    <input class="hw_check ro_xem" name="xem5" type="checkbox" <?= in_array('1',explode(',',$check_input['ro_nhap_kho'])) ? "checked" : ""?>>
                  </td>
                  <td>
                    <input class="hw_check ro_them" name="them5" type="checkbox" <?= in_array('2',explode(',',$check_input['ro_nhap_kho'])) ? "checked" : ""?>>
                  </td>
                  <td>
                    <input class="hw_check ro_sua" name="sua5" type="checkbox" <?= in_array('3',explode(',',$check_input['ro_nhap_kho'])) ? "checked" : ""?>>
                  </td>
                  <td>
                    <input class="hw_check ro_xoa" name="xoa5" type="checkbox" <?= in_array('4',explode(',',$check_input['ro_nhap_kho'])) ? "checked" : ""?>>
                  </td>
                  <td>
                    <input class="hw_check ro_duyet" name="duyet5" type="checkbox" <?= in_array('5',explode(',',$check_input['ro_nhap_kho'])) ? "checked" : ""?>>
                  </td>
                  <!-- <td>
                    <input class="hw_check" name="xem_dl_cty5" type="checkbox">
                  </td> -->
                </tr>
                <tr class="ro_xuat_kho">
                  <td class="font_s14 line_16 color_grey font_w500">

                    <div class="flex_start d_flex">
                      Xuất kho
                    </div>
                  </td>
                  <td>
                    <input class="hw_check ro_xem" name="xem6" type="checkbox" <?= in_array('1',explode(',',$check_input['ro_xuat_kho'])) ? "checked" : ""?>>
                  </td>
                  <td>
                    <input class="hw_check ro_them" name="them6" type="checkbox" <?= in_array('2',explode(',',$check_input['ro_xuat_kho'])) ? "checked" : ""?>>
                  </td>
                  <td>
                    <input class="hw_check ro_sua" name="sua6" type="checkbox" <?= in_array('3',explode(',',$check_input['ro_xuat_kho'])) ? "checked" : ""?>>
                  </td>
                  <td>
                    <input class="hw_check ro_xoa" name="xoa6" type="checkbox" <?= in_array('4',explode(',',$check_input['ro_xuat_kho'])) ? "checked" : ""?>>
                  </td>
                  <td>
                    <input class="hw_check ro_duyet" name="duyet6" type="checkbox" <?= in_array('5',explode(',',$check_input['ro_xuat_kho'])) ? "checked" : ""?>>
                  </td>
                  <!-- <td>
                    <input class="hw_check" name="xem_dl_cty6" type="checkbox">
                  </td> -->
                </tr>
                <tr class="ro_dieu_chuyen_kho">
                  <td class="font_s14 line_16 color_grey font_w500">

                    <div class="flex_start d_flex">
                      Điều chuyển kho
                    </div>
                  </td>
                  <td>
                    <input class="hw_check ro_xem" name="xem7" type="checkbox" <?= in_array('1',explode(',',$check_input['ro_dieu_chuyen_kho'])) ? "checked" : ""?>>
                  </td>
                  <td>
                    <input class="hw_check ro_them" name="them7" type="checkbox" <?= in_array('2',explode(',',$check_input['ro_dieu_chuyen_kho'])) ? "checked" : ""?>>
                  </td>
                  <td>
                    <input class="hw_check ro_sua" name="sua7" type="checkbox" <?= in_array('3',explode(',',$check_input['ro_dieu_chuyen_kho'])) ? "checked" : ""?>>
                  </td>
                  <td>
                    <input class="hw_check ro_xoa" name="xoa7" type="checkbox" <?= in_array('4',explode(',',$check_input['ro_dieu_chuyen_kho'])) ? "checked" : ""?>>
                  </td>
                  <td>
                    <input class="hw_check ro_duyet" name="duyet7" type="checkbox" <?= in_array('5',explode(',',$check_input['ro_dieu_chuyen_kho'])) ? "checked" : ""?>>
                  </td>
                  <!-- <td>
                    <input class="hw_check" name="xem_dl_cty7" type="checkbox">
                  </td> -->
                </tr>
                <tr class="ro_kiem_ke">
                  <td class="font_s14 line_16 color_grey font_w500">

                    <div class="flex_start d_flex">
                      Kiểm kê
                    </div>
                  </td>
                  <td>
                    <input class="hw_check ro_xem" name="xem8" type="checkbox" <?= in_array('1',explode(',',$check_input['ro_kiem_ke'])) ? "checked" : ""?>>
                  </td>
                  <td>
                    <input class="hw_check ro_them" name="them8" type="checkbox" <?= in_array('2',explode(',',$check_input['ro_kiem_ke'])) ? "checked" : ""?>>
                  </td>
                  <td>
                    <input class="hw_check ro_sua" name="sua8" type="checkbox" <?= in_array('3',explode(',',$check_input['ro_kiem_ke'])) ? "checked" : ""?>>
                  </td>
                  <td>
                    <input class="hw_check ro_xoa" name="xoa8" type="checkbox" <?= in_array('4',explode(',',$check_input['ro_kiem_ke'])) ? "checked" : ""?>>
                  </td>
                  <td>
                    <input class="hw_check ro_duyet" name="duyet8" type="checkbox" <?= in_array('5',explode(',',$check_input['ro_kiem_ke'])) ? "checked" : ""?>>
                  </td>
                  <!-- <td>
                    <input class="hw_check" name="xem_dl_cty8" type="checkbox">
                  </td> -->
                </tr>
                <tr class="ro_bao_cao">
                  <td class="font_s14 line_16 color_grey font_w500">

                    <div class="flex_start d_flex">
                      Báo cáo
                    </div>
                  </td>
                  <td>
                    <input class="hw_check ro_xem" name="xem9" type="checkbox" <?= in_array('1',explode(',',$check_input['ro_bao_cao'])) ? "checked" : ""?>>
                  </td>
                  <td>
                  </td>
                  <td>
                  </td>
                  <td>
                  </td>
                  <td>
                  </td>
                  <!-- <td>
                    <input class="hw_check" name="xem_dl_cty9" type="checkbox">
                  </td> -->
                </tr>
                <tr class="ro_phan_quyen">
                  <td class="font_s14 line_16 color_grey font_w500">

                    <div class="flex_start d_flex">
                      Phân quyền
                    </div>
                  </td>
                  <td>
                    <input class="hw_check ro_xem" name="xem10" type="checkbox" <?= in_array('1',explode(',',$check_input['ro_phan_quyen'])) ? "checked" : ""?>>
                  </td>
                  <td>
                    <input class="hw_check ro_them" name="them10" type="checkbox" <?= in_array('2',explode(',',$check_input['ro_phan_quyen'])) ? "checked" : ""?>>
                  </td>
                  <td>
                    <input class="hw_check ro_sua" name="sua10" type="checkbox" <?= in_array('3',explode(',',$check_input['ro_phan_quyen'])) ? "checked" : ""?>>
                  </td>
                  <td>
                  </td>
                  <td>
                  </td>
                  <!-- <td>
                    <input class="hw_check" name="xem_dl_cty10" type="checkbox">
                  </td> -->
                </tr>
            </table>
          </div>
          <div class="pre_q d_flex align_c flex_center position_a">
            <span class="pre_arrow"></span>
          </div>
          <div class="next_q d_flex align_c flex_center position_a">
            <span class="next_arrow"></span>  
          </div>
        </div>
      </div>
      <div class="d_flex align_c flex_center button_add_new_equipment_supplies">
        <button class="button_close color_blue font_s15 line_h18 font_w500">Hủy
          <a href="/phan-quyen.html"></a>
        </button>
        <button class="button_accp color_white font_s15 line_h18 font_w500">Lưu
        </button>
      </div>
      <?php include('../includes/popup_overview.php');  ?>
      <?php include('../includes/popup_h.php');  ?>
    </div>
</body>
<script type="text/javascript" src="../js/jquery-3.4.1.min.js"></script>
<script type="text/javascript" src="../js/jquery.validate.min.js"></script>
<script type="text/javascript" src="../js/select2.min.js"></script>
<script type="text/javascript" src="../js/js_h.js"></script>
<script type="text/javascript" src="../js/validate_h.js"></script>
<script type="text/javascript" src="../js/js_k.js"></script>
<script type="text/javascript" src="../js/js_n.js"></script>
<script type="text/javascript" src="../js/phan_quyen.js"></script>
<script>
    $('.active15').each(function() {
        if ($(this).hasClass('active15')) {
            $(this).find('a').addClass('active');
        }
    });
</script>

</html>