<?php
  include("config1.php"); 

  if(!in_array(1,$ro_phan_quyen)){
    header("Location: /tong-quan.html");
  }

  isset($_GET['page']) ? $page = $_GET['page'] : $page = 1;
  isset($_GET['search']) ? $search = $_GET['search'] : "";

  if (isset($_COOKIE['user']) && $_COOKIE['user'] != "" && $_COOKIE['role'] == 2) {
    $token = $_COOKIE['acc_token'];
    $curl = curl_init();
    $data = array();
    curl_setopt($curl, CURLOPT_POST, 1);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
    curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/user_info_employee.php');
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));
    $response = curl_exec($curl);
    curl_close($curl);
    $data_tt = json_decode($response, true);
    $tt_user = $data_tt['data']['user_info_result'];
}

    $com_id = $tt_user['com_id'];

    if (isset($_SESSION['quyen']) && $_SESSION['quyen'] == 1) {
        $curl = curl_init();
        $token = $_COOKIE['acc_token'];
        curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/list_all_employee_of_company.php');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));
        $response = curl_exec($curl);
        curl_close($curl);
    } elseif (isset($_SESSION['quyen']) && ($_SESSION['quyen'] == 2)) {
        $curl = curl_init();
        $token = $_COOKIE['acc_token'];
        curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/list_all_my_partner.php?get_all=true');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));
        $response = curl_exec($curl);
        curl_close($curl);
    }

    $data_list = json_decode($response, true);
    $data_list_nv = $data_list['data']['items'];
    $count = count($data_list_nv);

    $user = [];
    for ($i = 0; $i < count($data_list_nv); $i++){
        $item = $data_list_nv[$i];
        $user[$item["ep_id"]] = $item;
    }
    // print_r($user);
    // die();
?>
<!DOCTYPE html>
<html lang="vi">

<head>
  <title>Phân quyền</title>
  <meta charset="UTF-8">
  <meta name="robots" content="noindex,nofollow" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" type="text/css" href="../css/select2.min.css?v=<?= $ver ?>">
  <link rel="stylesheet" type="text/css" href="../css/style.css?v=<?= $ver ?>">
  <link rel="stylesheet" type="text/css" href="../css/style_h.css?v=<?= $ver ?>">
  <link rel="stylesheet" type="text/css" href="../css/style_n.css?v=<?= $ver ?>">
</head>

<body>
  <div class="main_wrapper_all">
    <div class="wapper_all">
      <?php include('../includes/sidebar.php');  ?>
    </div>
    <div class="main_overview" id="main_overview">
      <div class="header_menu_overview d_flex align_c space_b">
        <p class="color_grey line_16 font_s14 text_link_page" style="display: block;">Phân quyền</p>
        <img class="open_sidebar_w" src="../images/open_sidebar_w.png" alt="" onclick="toggle('main_sidebar')"
          style="display: none;">
        <?php include('../includes/header.php');  ?>
      </div>
      <div class="body_equipment_supplies">
        <p class="color_grey line_16 font_s14 text_link_page_re mb_15" style="display: none;">Phân quyền</p>
        <div class="d_flex space_b mb_20" style="flex-wrap: wrap;">
          <div class="position_r" id="block22">
              <select class="select_user" name="select_user" id="">
                    <option value="">Tìm kiếm theo mã, tên nhân viên</option>
                    <?php foreach($user as $val){?>
                    <option value="<?=$val['ep_id']?>" <?=($val['ep_id'] == $search) ? "selected" : ""?>><?= "[".$val['ep_id']."]"." - ".$val['ep_name']?></option>
                    <?php }?>
              </select>
          </div>
          <div class="d_flex align_c" style="flex-wrap: wrap;" id="block23">
            <div id="block20">
              <a class= "cursor_p">
              <?php if(in_array(2,$ro_phan_quyen) || in_array(3,$ro_phan_quyen)) { ?>
                <div class="go_set_permissions" style="display:none">
                  <button class="d_flex align_c mr_15 cursor_p button_set_permissions">
                    <p class="color_white line_18 font_s15 font_w500 text_a_c">Thiết lập quyền</p>
                  </button>
                </div>
              <?php }?>
              </a>
            </div>
            <div class="d_flex align_c" id="block21">
              <button class="d_flex align_c button_export_excel mr_15 cursor_p">
                <img class="mr_10 img_button_export_excel" src="../images/export_excel.png" alt="">
                <p class="color_white line_18 font_s15 font_w500">Xuất excel</p>
              </button>
              <div class="d_flex align_c">
                <img src="../images/img_hd.png" alt="">
                <p class="padding_l5 color_blue line_18 font_s15 font_w500">Hướng dẫn</p>
              </div>
            </div>
          </div>
        </div>
        <div class="detail_wh position_r" data-page="<?= $page ?> " data-comid ="<?= $com_id?>" data-search = "<?= $search ?>">
          
        </div>
      </div>
      <?php include('../includes/popup_overview.php');  ?>
    </div>
</body>
<script type="text/javascript" src="../js/jquery-3.4.1.min.js"></script>
<script type="text/javascript" src="../js/jquery.validate.min.js"></script>
<script type="text/javascript" src="../js/select2.min.js"></script>
<script type="text/javascript" src="../js/js_h.js"></script>
<script type="text/javascript" src="../js/validate_h.js"></script>
<script type="text/javascript" src="../js/js_k.js"></script>
<script type="text/javascript" src="../js/js_n.js"></script>
<script>

    var page = $('.detail_wh').attr('data-page');
    var com_id = $('.detail_wh').attr('data-comid');
    var search = $('.select_user').val();
    $.ajax({
        url: "../render/tb_phan_quyen.php",
        type: "POST",
        data:{
            search: search,
            page: page,
            com_id: com_id
        },
        success: function(data){
            $('.detail_wh').append(data);
        }
    });

    $('.select_user').change(function(){
        var id_search = $(this).val();
        var page = 1;

        if(id_search == ""){
            window.location.href = "/phan-quyen.html";
        }else{
            window.location.href = "/phan-quyen.html?search=" + id_search;
        }
    });

    $('.button_set_permissions').click(function(){
        var id_checked = 0;
        $('.hw_check').each(function(){
            if($(this).is(':checked')){
                var data_id = $(this).parent().parent().attr('data-id');
                id_checked = data_id;
            }
        });
        window.location.href = "/phan-quyen-chi-tiet-" + id_checked+ ".html";
    });

    $(".button_export_excel").click(function() {
        window.location.href = '../Excel/phan_quyen.php';
    });

</script>

</html>