<?php 
    include("config1.php");

    // if($_SESSION['quyen'] != 1){ 
    //     header("Location: /tong-quan.html");
    // }

    isset($_GET['page']) ? $page = $_GET['page'] : $page = 1;
    isset($_GET['input']) ? $ip = $_GET['input'] : $ip = "";

    if (isset($_COOKIE['user']) && $_COOKIE['user'] != "" && $_COOKIE['role'] == 1) {
		$token = $_COOKIE['acc_token'];
		$curl = curl_init();
		$data = array();
		curl_setopt($curl, CURLOPT_POST, 1);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
		curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/user_info_company.php');
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));
	
		$response = curl_exec($curl);
		curl_close($curl);
		$data_tt = json_decode($response, true);
		$tt_user = $data_tt['data']['user_info_result'];
	} else if (isset($_COOKIE['user']) && $_COOKIE['user'] != "" && $_COOKIE['role'] == 2) {
		$token = $_COOKIE['acc_token'];
		$curl = curl_init();
		$data = array();
		curl_setopt($curl, CURLOPT_POST, 1);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
		curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/user_info_employee.php');
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));
		$response = curl_exec($curl);
		curl_close($curl);
		$data_tt = json_decode($response, true);
		$tt_user = $data_tt['data']['user_info_result'];
	}
    $id_cty = $tt_user['com_id'];
?>
<!DOCTYPE html>
<html lang="vi">

<head>
    <title>Dữ liệu đã xóa gần đây - Hãng sản xuất</title>
    <meta charset="UTF-8">
    <meta name="robots" content="noindex,nofollow" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="../css/select2.min.css?v=<?= $ver ?>">
    <link rel="stylesheet" type="text/css" href="../css/style.css?v=<?= $ver ?>">
    <link rel="stylesheet" type="text/css" href="../css/style_h.css?v=<?= $ver ?>">
    <link rel="stylesheet" type="text/css" href="../css/style_n.css?v=<?= $ver ?>">
    <link rel="stylesheet" type="text/css" href="../css/style_k.css?v=<?= $ver ?>">
</head>

<body>

    <div class="box_right dt_dl_n dt_dl_brand">
        <?php include("../includes/sidebar.php"); ?>
        <div class="box_right_ct">
            <div class="block_change block_wh_tf">
                <div class="head_wh d_flex space_b align_c">
                    <div class="head_tab d_flex space_b align_c">
                        <div class="icon_header open_sidebar_w">
                            <span class="icon_header_tbl"></span>
                            <span class="icon_header_tbl"></span>
                            <span class="icon_header_tbl"></span>
                        </div>
                        <?php include("../includes/header.php") ; ?>
                    </div>
                    <p class="color_grey font_s14 line_h17 font_w400">
                        <a href="/du-lieu-da-xoa-gan-day.html" class="cursor_p">
                            <img src="../images/back_item_g.png" alt="">
                        </a>
                        &nbsp Dữ liệu đã xóa gần đây / Hãng sản xuất
                    </p>
                    <?php include("../includes/header.php") ; ?>
                </div>
                <div class="operation_wh d_flex space_b">
                    <div class="search_wh d_flex space_b">
                        <div class="input_sr_wh">
                            <div class="box_input_sr position_r">
                                <input type="text" name="input_search" value="<?= ($ip != "") ? $ip : "" ?>" placeholder="Tìm kiếm theo mã, tên hãng sản xuất">
                                <span class="icon_sr_wh"></span>
                            </div>
                        </div>
                    </div>
                    <div class="export_wh d_flex space_b align_c">
                        <button class="btn_del_vv back_w d_flex align_c flex_center" style="display: none;">
                            <p class="color_blue font_s15 line_h18 font_w500">Xóa vĩnh viễn</p>
                        </button>
                        <button class="btn_del_rm back_blue d_flex align_c flex_center" style="display: none;">
                            <p class="color_white font_s15 line_h18 font_w500">Khôi phục</p>
                        </button>
                        <div class="hd_ex d_flex align_c">
                            <img src="../images/img_hd.png" alt="">
                            <p class="color_blue font_s15 line_h18 font_w500">Hướng dẫn</p>
                        </div>
                    </div>
                </div>
                <div class="detail_wh" data-page="<?= $page?>" data-ip ="<?= $ip ?>">
                </div>
            </div>
        </div>
    </div>
    <?php include('../includes/popup_overview.php');  ?>
    <?php include('../includes/popup_dieu-chuyen-kho.php') ?>

</body>
<script type="text/javascript" src="../js/jquery-3.4.1.min.js"></script>
<script type="text/javascript" src="../js/jquery.validate.min.js"></script>
<script type="text/javascript" src="../js/select2.min.js"></script>
<script type="text/javascript" src="../js/js_h.js"></script>
<script type="text/javascript" src="../js/validate_h.js"></script>
<script type="text/javascript" src="../js/js_k.js"></script>
<script type="text/javascript" src="../js/js_n.js"></script>
<script>
    $('.active16').each(function () {
        if ($(this).hasClass('active16')) {
            $(this).find('a').addClass('active');
        }
    });

    var id_ct = <?=  json_encode($id_cty); ?>;

</script>

<script type="text/javascript" src="../js/deleted_hang_san_xuat.js"></script>

</html>