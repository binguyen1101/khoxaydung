<?php 
    include("config1.php"); 
    // if($_SESSION['quyen'] != 1){ 
    //     header("Location: /tong-quan.html");
    // }


    if (isset($_COOKIE['user']) && $_COOKIE['user'] != "" && $_COOKIE['role'] == 1) {
		$token = $_COOKIE['acc_token'];
		$curl = curl_init();
		$data = array();
		curl_setopt($curl, CURLOPT_POST, 1);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
		curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/user_info_company.php');
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));
	
		$response = curl_exec($curl);
		curl_close($curl);
		$data_tt = json_decode($response, true);
		$tt_user = $data_tt['data']['user_info_result'];
	} else if (isset($_COOKIE['user']) && $_COOKIE['user'] != "" && $_COOKIE['role'] == 2) {
		$token = $_COOKIE['acc_token'];
		$curl = curl_init();
		$data = array();
		curl_setopt($curl, CURLOPT_POST, 1);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
		curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/user_info_employee.php');
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));
		$response = curl_exec($curl);
		curl_close($curl);
		$data_tt = json_decode($response, true);
		$tt_user = $data_tt['data']['user_info_result'];
	}
    
    $id_cty = $tt_user['com_id'];

    // So luong vat tu da xoa
    $sl_vattu = new db_query("SELECT COUNT(`dsvt_id`) AS sl_vt FROM `danh-sach-vat-tu` WHERE `dsvt_id_ct` = $id_cty AND `dsvt_check` = 0");
    $sl_vattu_dx = mysql_fetch_assoc($sl_vattu->result)['sl_vt'];

    // So luong nhom vat tu da xoa
    $sl_nhom_vattu = new db_query("SELECT COUNT(`nvt_id`) AS sl_nvt FROM `nhom-vat-tu-thiet-bi` WHERE `nvt_id_ct` = $id_cty AND `nvt_check` = 0");
    $sl_nhom_vattu_dx = mysql_fetch_assoc($sl_nhom_vattu->result)['sl_nvt'];

    // So luong hang san xuat da xoa
    $sl_hsx = new db_query("SELECT COUNT(`hsx_id`) AS sl_hsx FROM `hang-san-xuat` WHERE `hsx_id_ct` = $id_cty AND `hsx_check` = 0");
    $sl_hsx_dx = mysql_fetch_assoc($sl_hsx->result)['sl_hsx'];

    // So luong don vi tinh da xoa
    $sl_dvt = new db_query("SELECT COUNT(`dvt_id`) AS sl_dvt FROM `don-vi-tinh` WHERE `dvt_id_ct` = $id_cty AND `dvt_check` = 0");
    $sl_dvt_dx = mysql_fetch_assoc($sl_dvt->result)['sl_dvt'];

    // So luong PNK da xoa
    $sl_pnk = new db_query("SELECT COUNT(`kcxl_id`) AS sl_pnk FROM `kho-cho-xu-li` WHERE `kcxl_id_ct` = $id_cty AND `kcxl_soPhieu` = 'PNK' AND `kcxl_check` = 0");
    $sl_pnk_dx = mysql_fetch_assoc($sl_pnk->result)['sl_pnk'];

    // So luong PXK da xoa
    $sl_pxk = new db_query("SELECT COUNT(`kcxl_id`) AS sl_pxk FROM `kho-cho-xu-li` WHERE `kcxl_id_ct` = $id_cty AND `kcxl_soPhieu` = 'PXK' AND `kcxl_check` = 0");
    $sl_pxk_dx = mysql_fetch_assoc($sl_pxk->result)['sl_pxk'];

    // So luong ĐCK da xoa
    $sl_dck = new db_query("SELECT COUNT(`kcxl_id`) AS sl_dck FROM `kho-cho-xu-li` WHERE `kcxl_id_ct` = $id_cty AND `kcxl_soPhieu` = 'ĐCK' AND `kcxl_check` = 0");
    $sl_dck_dx = mysql_fetch_assoc($sl_dck->result)['sl_dck'];

    // So luong PKK da xoa
    $sl_pkk = new db_query("SELECT COUNT(`kcxl_id`) AS sl_pkk FROM `kho-cho-xu-li` WHERE `kcxl_id_ct` = $id_cty AND `kcxl_soPhieu` = 'PKK' AND `kcxl_check` = 0");
    $sl_pkk_dx = mysql_fetch_assoc($sl_pkk->result)['sl_pkk'];

?>
<!DOCTYPE html>
<html lang="vi">

<head>
    <title>Dữ liệu đã xóa gần đây</title>
    <meta charset="UTF-8">
    <meta name="robots" content="noindex,nofollow" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="../css/style.css?v=<?= $ver ?>">
    <link rel="stylesheet" type="text/css" href="../css/style_h.css?v=<?= $ver ?>">
    <link rel="stylesheet" type="text/css" href="../css/style_n.css?v=<?= $ver ?>">
    <link rel="stylesheet" type="text/css" href="../css/style_k.css?v=<?= $ver ?>">
</head>

<body>

    <div class="box_right data_deleted_n">
        <?php include("../includes/sidebar.php"); ?>
        <div class="box_right_ct">
            <div class="block_change block_wh_tf">
                <div class="head_wh d_flex space_b align_c">
                    <div class="head_tab d_flex space_b align_c">
                        <div class="icon_header open_sidebar_w">
                            <span class="icon_header_tbl"></span>
                            <span class="icon_header_tbl"></span>
                            <span class="icon_header_tbl"></span>
                        </div>
                        <?php include("../includes/header.php") ; ?>
                    </div>
                    <p class="color_grey font_s14 line_h17 font_w400">Dữ liệu đã xóa gần đây
                    </p>
                    <?php include("../includes/header.php") ; ?>
                </div>
                <div class="list_data_deleted d_flex flex_start flex_w">
                    <div class="item_data_deleted">
                        <div class="tit_dt_dl d_flex align_c mb_15">
                            <img src="../images/vt_deleted.png" alt="">
                            <p class="color_blue font_16 line_h17 font_w500">Vật tư thiết bị</p>
                        </div>
                        <div class="body_dt_dl d_flex align_c">
                            <span class="color_red font_16 line_h17 font_w700"><?= $sl_vattu_dx ?></span>
                            <p class="color_grey font_14 line_h17 font_w400">Vật tư thiết bị đã xóa</p>
                        </div>
                        <a class="href_item" href="/du-lieu-da-xoa-gan-day-vat-tu-thiet-bi.html"></a>
                    </div>
                    <div class="item_data_deleted">
                        <div class="tit_dt_dl d_flex align_c mb_15">
                            <img src="../images/g_vt_deleted.png" alt="">
                            <p class="color_blue font_16 line_h17 font_w500">Nhóm vật tư thiết bị </p>
                        </div>
                        <div class="body_dt_dl d_flex align_c">
                            <span class="color_red font_16 line_h17 font_w700"><?= $sl_nhom_vattu_dx ?></span>
                            <p class="color_grey font_14 line_h17 font_w400">Nhóm vật tư thiết bị đã xóa</p>
                        </div>
                        <a class="href_item" href="/du-lieu-da-xoa-gan-day-nhom-vat-tu-thiet-bi.html"></a>
                    </div>
                    <div class="item_data_deleted">
                        <div class="tit_dt_dl d_flex align_c mb_15">
                            <img src="../images/hsx_deleted.png" alt="">
                            <p class="color_blue font_16 line_h17 font_w500">Hãng sản xuất</p>
                        </div>
                        <div class="body_dt_dl d_flex align_c">
                            <span class="color_red font_16 line_h17 font_w700"><?= $sl_hsx_dx ?></span>
                            <p class="color_grey font_14 line_h17 font_w400">Hãng sản xuất đã xóa</p>
                        </div>
                        <a class="href_item" href="/du-lieu-da-xoa-gan-day-hang-san-xuat.html"></a>
                    </div>
                    <div class="item_data_deleted">
                        <div class="tit_dt_dl d_flex align_c mb_15">
                            <img src="../images/dvt_deleted.png" alt="">
                            <p class="color_blue font_16 line_h17 font_w500">Đơn vị tính</p>
                        </div>
                        <div class="body_dt_dl d_flex align_c">
                            <span class="color_red font_16 line_h17 font_w700"><?= $sl_dvt_dx ?></span>
                            <p class="color_grey font_14 line_h17 font_w400">Đơn vị tính đã xóa</p>
                        </div>
                        <a class="href_item" href="/du-lieu-da-xoa-gan-day-don-vi-tinh.html"></a>
                    </div>
                    <div class="item_data_deleted">
                        <div class="tit_dt_dl d_flex align_c mb_15">
                            <img src="../images/pnk_deleted.png" alt="">
                            <p class="color_blue font_16 line_h17 font_w500">Phiếu nhập kho</p>
                        </div>
                        <div class="body_dt_dl d_flex align_c">
                            <span class="color_red font_16 line_h17 font_w700"><?= $sl_pnk_dx ?></span>
                            <p class="color_grey font_14 line_h17 font_w400">Phiếu nhập kho đã xóa</p>
                        </div>
                        <a class="href_item" href="/du-lieu-da-xoa-gan-day-phieu-nhap-kho.html"></a>
                    </div>
                    <div class="item_data_deleted">
                        <div class="tit_dt_dl d_flex align_c mb_15">
                            <img src="../images/pxk_deleted.png" alt="">
                            <p class="color_blue font_16 line_h17 font_w500">Phiếu xuất kho</p>
                        </div>
                        <div class="body_dt_dl d_flex align_c">
                            <span class="color_red font_16 line_h17 font_w700"><?= $sl_pxk_dx ?></span>
                            <p class="color_grey font_14 line_h17 font_w400">Phiếu xuất kho đã xóa</p>
                        </div>
                        <a class="href_item" href="/du-lieu-da-xoa-gan-day-phieu-xuat-kho.html"></a>
                    </div>
                    <div class="item_data_deleted">
                        <div class="tit_dt_dl d_flex align_c mb_15">
                            <img src="../images/pdck_deleted.png" alt="">
                            <p class="color_blue font_16 line_h17 font_w500">Phiếu điều chuyển kho</p>
                        </div>
                        <div class="body_dt_dl d_flex align_c">
                            <span class="color_red font_16 line_h17 font_w700"><?= $sl_dck_dx ?></span>
                            <p class="color_grey font_14 line_h17 font_w400">Phiếu điều chuyển kho đã xóa</p>
                        </div>
                        <a class="href_item" href="/du-lieu-da-xoa-gan-day-phieu-dieu-chuyen-kho.html"></a>
                    </div>
                    <div class="item_data_deleted">
                        <div class="tit_dt_dl d_flex align_c mb_15">
                            <img src="../images/pkk_deleted.png" alt="">
                            <p class="color_blue font_16 line_h17 font_w500">Phiếu kiểm kê</p>
                        </div>
                        <div class="body_dt_dl d_flex align_c">
                            <span class="color_red font_16 line_h17 font_w700"><?= $sl_pkk_dx ?></span>
                            <p class="color_grey font_14 line_h17 font_w400">Phiếu kiểm kê đã xóa</p>
                        </div>
                        <a class="href_item" href="/du-lieu-da-xoa-gan-day-phieu-kiem-ke.html"></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php include('../includes/popup_overview.php');  ?>
    <?php include('../includes/popup_dieu-chuyen-kho.php') ?>

</body>
<script type="text/javascript" src="../js/jquery-3.4.1.min.js"></script>
<script type="text/javascript" src="../js/jquery.validate.min.js"></script>
<script type="text/javascript" src="../js/select2.min.js"></script>
<script type="text/javascript" src="../js/js_h.js"></script>
<script type="text/javascript" src="../js/validate_h.js"></script>
<script type="text/javascript" src="../js/js_k.js"></script>
<script type="text/javascript" src="../js/js_n.js"></script>
<script>

</script>

</html>