<?php include("config1.php"); ?>
<!DOCTYPE html>
<html lang="vi">

<head>
  <title>Cài đặt chung</title>
  <meta charset="UTF-8">
  <meta name="robots" content="noindex,nofollow" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" type="text/css" href="../css/style.css?v=<?= $ver ?>">
  <link rel="stylesheet" type="text/css" href="../css/style_h.css?v=<?= $ver ?>">
  <link rel="stylesheet" type="text/css" href="../css/style_n.css?v=<?= $ver ?>">
</head>

<body>
  <div class="main_wrapper_all">
    <div class="wapper_all">
      <?php include('../includes/sidebar.php');  ?>
    </div>
    <div class="main_overview" id="main_overview">
      <div class="header_menu_overview d_flex align_c space_b">
        <p class="color_grey line_16 font_s14 text_link_page" style="display: block;">Cài đặt / Cài đặt chung</p>
        <img class="open_sidebar_w" src="../images/open_sidebar_w.png" alt="" onclick="toggle('main_sidebar')"
          style="display: none;">
        <?php include('../includes/header.php');  ?>
      </div>
      <div class="main_body_setting">
        <p class="color_grey line_16 font_s14 text_link_page_re" style="display: none;">Cài đặt / Cài đặt chung</p>
        <div class="position_r">
          <div class="d_flex mt_20 position_r" id="block24">
            <div class="d_flex align_c"id="block25">
              <a href="/cai-dat-chung.html" class="position_r">
                <p class='mr_30 font_s14 line_h16 color_blue'>Cài đặt chung</p>
                <div class="a position_a"></div>
              </a>
              <a href="/nhat-ky-hoat-dong.html" class="position_r">
                <p class="mr_30 font_s14 line_h16 color_grey">Nhật ký hoạt động</p>
                <div class="a position_a"></div>
              </a>
            </div>
            <a href="/thong-tin-bao-mat.html" class="position_r" id="block26">
              <p class="font_s14 line_h16 color_grey">Thông tin bảo mật</p>
              <div class="a position_a"></div>
            </a>
            <span class="d position_a"></span>
          </div>
        </div>
        <div class="backgroud_setting mt_20">
          <div class="d_flex align_c padding_b40" id="block27">
            <p class="font_s15 line_h18 font_w500 color_grey width_setting">Ngôn ngữ:</p>
            <div class="d_flex align_c" id="block28">
              <div class="d_flex align_c mr_109" id="block29">
                <input class="hw_check" type="checkbox">
                <p class="ml_10 font_s14 line_h16 color_grey">Tiếng Việt</p>
                <img class="ml_5" src="../images/vn.png" alt="">
              </div>
              <div class="d_flex align_c" id="block30">
                <input class="hw_check" type="checkbox">
                <p class="ml_10 font_s14 line_h16 color_grey">Tiếng Anh</p>
                <img class="ml_5" src="../images/en.png" alt="">
              </div>
            </div>
          </div>
          <div class="d_flex align_c padding_b40" id="block27">
            <p class="font_s15 line_h18 font_w500 color_grey width_setting">Giao diện:</p>
            <div class="d_flex align_c"  id="block28">
              <div class="d_flex align_c mr_140" id="block29">
                <input class="hw_check" type="checkbox">
                <p class="ml_10 font_s14 line_h16 color_grey">Xanh</p>
                <span class="circle_b ml_5"></span>
              </div>
              <div class="d_flex align_c" id="block30">
                <input class="hw_check" type="checkbox">
                <p class="ml_10 font_s14 line_h16 color_grey">Trắng</p>
                <span class="circle_w ml_5"></span>
              </div>
            </div>
          </div>
          <div class="d_flex padding_b40"id="block27">
            <p class="font_s15 line_h18 font_w500 color_grey width_setting">Thông báo:</p>
            <div class="">
              <div class="d_flex align_c" id="block30">
                <input class="hw_check " type="checkbox">
                <p class="ml_10 font_s14 line_h16 color_grey hw_check_80">Nhận thông báo khi có thay đổi ở tất cả nội dung tôi xem
                  được</p>
              </div>
              <div class="d_flex align_c mt_15">
                <input class="hw_check " type="checkbox">
                <p class="ml_10 font_s14 line_h16 color_grey hw_check_80">Nhận thông có khi có phản hồi từ các nội dung tôi tạo ra
                </p>
              </div>
              <div class="d_flex align_c mt_15">
                <input class="hw_check " type="checkbox">
                <p class="ml_10 font_s14 line_h16 color_grey hw_check_80">Nhận thông báo khi có thay đổi ở tất cả nội dung tôi tạo
                  ra</p>
              </div>
            </div>
          </div>
          <div class="d_flex" id="block27">
            <p class="font_s15 line_h18 font_w500 color_grey width_setting">Nhắc nhở:</p>
            <div class="">
              <div class="d_flex align_c" id="block30">
                <input class="hw_check " type="checkbox">
                <p class="ml_10 font_s14 line_h16 color_grey hw_check_80">Nhắc nhở tất cả các nội dung tôi tạo ra khi đến hạn/quá
                  hạn</p>
              </div>
              <div class="d_flex align_c mt_15">
                <input class="hw_check " type="checkbox">
                <p class="ml_10 font_s14 line_h16 color_grey hw_check_80">Nhắc nhở tất cả các nội dung tôi xem được khi đến hạn/quá
                  hạn</p>
              </div>
              <div class="d_flex align_c mt_15">
                <input class="hw_check " type="checkbox">
                <p class="ml_10 font_s14 line_h16 color_grey hw_check_80">Nhắc nhở tất cả các nội dung liên quan đến tôi khi đến
                  hạn/quá hạn</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <?php include('../includes/popup_overview.php');  ?>
  </div>
</body>
<script type="text/javascript" src="../js/jquery-3.4.1.min.js"></script>
<script type="text/javascript" src="../js/jquery.validate.min.js"></script>
<script type="text/javascript" src="../js/select2.min.js"></script>
<script type="text/javascript" src="../js/js_h.js"></script>
<script type="text/javascript" src="../js/validate_h.js"></script>
<script type="text/javascript" src="../js/js_k.js"></script>
<script type="text/javascript" src="../js/js_n.js"></script>
<script>
$('.active17').each(function() {
  if ($(this).hasClass('active17')) {
    $(this).find('a').addClass('active');
  }
});
</script>

</html>