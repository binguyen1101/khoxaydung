<?php include("config1.php"); ?>
<!DOCTYPE html>
<html lang="vi">

<head>
  <title>Thông tin bảo mật</title>
  <meta charset="UTF-8">
  <meta name="robots" content="noindex,nofollow" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" type="text/css" href="../css/select2.min.css?v=<?= $ver ?>">
  <link rel="stylesheet" type="text/css" href="../css/style.css?v=<?= $ver ?>">
  <link rel="stylesheet" type="text/css" href="../css/style_h.css?v=<?= $ver ?>">
  <link rel="stylesheet" type="text/css" href="../css/style_n.css?v=<?= $ver ?>">
</head>

<body>
  <div class="main_wrapper_all">
    <div class="wapper_all">
      <?php include('../includes/sidebar.php');  ?>
    </div>
    <div class="main_overview" id="main_overview">
      <div class="header_menu_overview d_flex align_c space_b">
        <p class="color_grey line_16 font_s14 text_link_page" style="display: block;">Cài đặt / Thông tin bảo mật</p>
        <img class="open_sidebar_w" src="../images/open_sidebar_w.png" alt="" onclick="toggle('main_sidebar')"
          style="display: none;">
        <?php include('../includes/header.php');  ?>
      </div>
      <div class="body_equipment_supplies">
        <p class="color_grey line_16 font_s14 text_link_page_re mb_15" style="display: none;">Cài đặt / Thông tin bảo
          mật</p>
        <div class="position_r">
          <div class="d_flex position_r" id="block24">
            <div class="d_flex align_c" id="block25">
              <a href="/cai-dat-chung.html" class="position_r">
                <p class='mr_30 font_s14 line_h16 color_grey'>Cài đặt chung</p>
                <div class="a position_a"></div>
              </a>
              <a href="/nhat-ky-hoat-dong.html" class="position_r">
                <p class="mr_30 font_s14 line_h16 color_grey">Nhật ký hoạt động</p>
                <div class="a position_a"></div>
              </a>
            </div>
            <a href="/thong-tin-bao-mat.html" class="position_r" id="block26">
              <p class="font_s14 line_h16 color_blue">Thông tin bảo mật</p>
              <div class="a position_a"></div>
            </a>
            <span class="b position_a"></span>
          </div>
        </div>
        <div class="detail_wh position_r d_flex align_c table_thong_tin">
          <div class="main_table mb_15 mt_20 table_vt_scr">
            <table class="table table_decentralization">
              <tr>
                <th>
                  STT
                  <span class="span_tbody"></span>
                </th>
                <th>Thiết bị
                  <span class="span_tbody"></span>
                </th>
                <th>Chi tiết
                  <span class="span_tbody"></span>
                </th>
                <th>Chức năng
                  <span class="span_tbody"></span>
                </th>
              </tr>
              <tr>
                <td class="font_s14 line_16 color_grey">
                  1
                </td>
                <td class="font_s14 line_16 color_grey" style="text-align: left;">
                  <p>IPHONE 6PLUS</p>
                </td>
                <td class="font_s14 line_16 color_grey" style="text-align: left;">
                  <p>Version: IOS 13.0.1</p>
                </td>
                <td class="font_s14 line_16 color_grey">
                  <div class="d_flex align_c flex_center" onclick="openAndHide('','delete_link_equipment')">
                    <img style="margin-right: 5px;" src="../images/delete_r.png" alt="">
                    <p class="color_red">Bỏ liên kết</p>
                  </div>
                </td>
              </tr>
            </table>
          </div>
          <div class="pre_q d_flex align_c flex_center position_a">
            <span class="pre_arrow"></span>
          </div>
          <div class="next_q d_flex align_c flex_center position_a">
            <span class="next_arrow"></span>
          </div>
        </div>
        <div class="d_flex align_c space_b" id="block08">
          <div class="d_flex align_c">
            <p class="line_h16 font_s14 color_grey">Hiển thị:</p>
            <div class="d_flex align_c space_b list_choonse_item ml_10 position_r cursor_p">
              <div class="d_flex align_c" onclick="toggle('list_item_choonse_show')">
                <p class="line_h16 font_s14 color_grey">40</p>
                <img class="ml_10" src="../images/list_item.png" alt="">
              </div>
              <div class="list_item_choonse_show position_a" style="display: none;">
                <p class="item_choonse_show text_a_c font_s14 line_16 color_grey">10</p>
                <p class="item_choonse_show text_a_c font_s14 line_16 color_grey">20</p>
                <p class="item_choonse_show text_a_c font_s14 line_16 color_grey">40</p>
                <p class="item_choonse_show text_a_c font_s14 line_16 color_grey">80</p>
                <p class="item_choonse_show text_a_c font_s14 line_16 color_grey">100</p>
              </div>
            </div>
          </div>
          <div class="d_flex align_c pagination">
            <div class="item_pagination cursor_p">
              <img src="../images/back_item_g.png" alt="">
            </div>
            <p class="item_pagination ml_15 font_s13 line_h15 color_grey3 number_item_pagination cursor_p">1</p>
            <p class="item_pagination ml_10 font_s13 line_h15 color_grey3 number_item_pagination cursor_p">2</p>
            <p class="item_pagination ml_10 font_s13 line_h15 color_grey3 number_item_pagination cursor_p">3</p>
            <p class="item_pagination ml_10 font_s13 line_h15 color_grey3 number_item_pagination cursor_p">4</p>
            <p class="item_pagination ml_10 font_s13 line_h15 color_grey3 number_item_pagination cursor_p">5</p>
            <div class="item_pagination  ml_15  next_item_g cursor_p">
              <img src="../images/next_item_g.png" alt="">
            </div>
          </div>
        </div>
      </div>
      <?php include('../includes/popup_overview.php');  ?>
      <?php include('../includes/popup_h.php');  ?>
    </div>
</body>
<script type="text/javascript" src="../js/jquery-3.4.1.min.js"></script>
<script type="text/javascript" src="../js/jquery.validate.min.js"></script>
<script type="text/javascript" src="../js/select2.min.js"></script>
<script type="text/javascript" src="../js/js_h.js"></script>
<script type="text/javascript" src="../js/validate_h.js"></script>
<script type="text/javascript" src="../js/js_k.js"></script>
<script type="text/javascript" src="../js/js_n.js"></script>
<script>
$('.active17').each(function() {
  if ($(this).hasClass('active17')) {
    $(this).find('a').addClass('active');
  }
});
</script>

</html>