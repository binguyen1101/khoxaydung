<?php include("config.php"); ?>
<!DOCTYPE html>
<html lang="vi">

<head>
    <title>Nghiệp vụ kho chờ xử lí - Chi tiết phiếu điều chuyển kho</title>
    <meta charset="UTF-8">
    <meta name="robots" content="noindex,nofollow" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="../css/select2.min.css?v=<?= $ver ?>">
    <link rel="stylesheet" type="text/css" href="../css/style.css?v=<?= $ver ?>">
    <link rel="stylesheet" type="text/css" href="../css/style_h.css?v=<?= $ver ?>">
    <link rel="stylesheet" type="text/css" href="../css/style_n.css?v=<?= $ver ?>">
    <link rel="stylesheet" type="text/css" href="../css/style_k.css?v=<?= $ver ?>">
</head>

<body class="seclec2_radius">

    <div class="box_right wh_pd_detail_tf_add">
        <?php include("../includes/sidebar.php"); ?>
        <div class="box_right_ct">
            <div class="block_change">
                <div class="head_wh d_flex space_b align_c">
                <div class="head_tab d_flex space_b align_c">
                        <div class="icon_header open_sidebar_w">
                            <span class="icon_header_tbl"></span>
                            <span class="icon_header_tbl"></span>
                            <span class="icon_header_tbl"></span>
                        </div>
                        <?php include("../includes/header.php") ; ?>
                    </div>
                    <p class="color_grey font_s14 line_h17 font_w400">
                        <a href="/nghiep-vu-kho-cho-xu-ly-detail-tf.html" class="cursor_p">
                            <img src="../images/back_item_g.png" alt="">
                        </a>
                        &nbsp Nghiệp vụ kho chờ xử lý / Chi tiết phiếu điều chuyển kho / Thêm phiếu xuất
                    </p>
                    <?php include("../includes/header.php") ; ?>
                </div>
                <form action="" method="post" class="f_date_fn">
                    <div class="add_ex_wh">
                        <div class="tit_info_rq back_blue">
                            <p class="color_white font_s16 line_h19 font_w700">Thêm mới phiếu xuất kho</p>
                        </div>
                        <div class="ct_add_ex_wh">
                            <div class="box_add_ex_wh d_flex space_b">
                                <div class="l_add_ex">
                                    <div class="d_flex flex_column mb_15">
                                        <p class="color_grey font_s15 line_h18 font_w500">Số phiếu</p>
                                        <input class="color_grey font_s14 line_h17 font_w400" type="text" value="VT-0000"
                                            disabled="disabled">
                                    </div>
                                    <div class="d_flex flex_column mb_15">
                                        <p class="color_grey font_s15 line_h18 font_w500">Người tạo</p>
                                        <input class="color_grey font_s14 line_h17 font_w400" type="text"
                                            value="Nguyễn Văn Nam" disabled="disabled">
                                    </div>
                                    <div class="d_flex flex_column mb_15">
                                        <p class="color_grey font_s15 line_h18 font_w500">Phiếu điều chuyển<span
                                                style="color: red;">*</span></p>
                                        <select name="" id="" disabled="disabled"
                                            class="color_grey font_s14 line_h17 font_w400">
                                            <option value="">YC-0000</option>
                                        </select>
                                    </div>
                                    <div class="d_flex flex_column mb_15">
                                        <p class="color_grey font_s15 line_h18 font_w500">Kho xuất</p>
                                        <input class="color_grey font_s14 line_h17 font_w400" type="text"
                                            value="Hiển thị kho xuất từ phiếu điều chuyển" disabled="disabled">
                                    </div>
                                    <div class="d_flex flex_column mb_15">
                                        <p class="color_grey font_s15 line_h18 font_w500">Kho nhập</p>
                                        <input class="color_grey font_s14 line_h17 font_w400" type="text"
                                            value="Hiển thị kho nhập từ phiếu điều chuyển" disabled="disabled">
                                    </div>
                                    <div class="d_flex flex_column mb_15">
                                        <p class="color_grey font_s15 line_h18 font_w500">Người giao hàng</p>
                                        <input class="color_grey font_s14 line_h17 font_w400" type="text"
                                            value="Hiển thị tên người giao hàng từ phiếu điều chuyển" disabled="disabled">
                                    </div>
                                    <div class="d_flex flex_column mb_15">
                                        <p class="color_grey font_s15 line_h18 font_w500">Người nhận</p>
                                        <input class="color_grey font_s14 line_h17 font_w400" type="text"
                                            value="Hiển thị tên người nhận từ phiếu điều chuyển" disabled="disabled">
                                    </div>
                                </div>
                                <div class="r_add_ex">
                                    <div class="d_flex flex_column mb_15">
                                        <p class="color_grey font_s15 line_h18 font_w500">Hình thức xuất kho</p>
                                        <select name="" id="" disabled="disabled"
                                            class="color_grey font_s14 line_h17 font_w400">
                                            <option value="">Xuất điều chuyển</option>
                                        </select>
                                    </div>
                                    <div class="d_flex flex_column mb_15">
                                        <p class="color_grey font_s15 line_h18 font_w500">Ngày tạo</p>
                                        <input class="color_grey font_s14 line_h17 font_w400" type="date"
                                            disabled="disabled">
                                    </div>
                                    <div class="select_status d_flex space_b">
                                        <div class="d_flex flex_column">
                                            <p class="color_grey font_s15 line_h18 font_w500">Trạng thái</p>
                                            <select name="" id="" class="select_stt color_grey font_s14 line_h17 font_w400" style="width: 100%;">
                                                <option value="kt">Khởi tạo</option>
                                                <option value="ht">Hoàn thành</option>
                                            </select>
                                        </div>
                                        <div class="input_date_fn d_flex flex_column">
                                            <p class="color_grey font_s15 line_h18 font_w500">Ngày hoàn thành<span
                                                    style="color: red;">*</span></p>
                                            <input type="text" placeholder="Chọn ngày" onfocus="(this.type='date')" name="input_date_fn">
                                        </div>
                                    </div>
                                    <div class="d_flex flex_column mb_15">
                                        <p class="color_grey font_s15 line_h18 font_w500">Ngày xuất kho</p>
                                        <input placeholder="Chọn ngày xuất kho"
                                            class="color_grey font_s14 line_h17 font_w400" type="text"
                                            onfocus="(this.type='date')">
                                    </div>
                                    <div class="d_flex flex_column mb_15">
                                        <p class="color_grey font_s15 line_h18 font_w500">Ngày yêu cầu hoàn thành điều chuyển
                                        </p>
                                        <input type="date" class="color_grey font_s14 line_h17 font_w400"
                                            placeholder="Hiển thị ngày yêu cầu hoàn thành cung ứng" disabled="disabled">
                                        <!-- <input placeholder="Hiển thị ngày yêu cầu hoàn thành cung ứng" class="color_grey font_s14 line_h17 font_w400" type="text" onfocus="(this.type='date')"> -->
                                    </div>
                                    <div class="d_flex flex_column mb_15">
                                        <p class="color_grey font_s15 line_h18 font_w500">Phòng ban</p>
                                        <input type="text" class="color_grey font_s14 line_h17 font_w400"
                                            value="Hiển thị phòng ban người giao" disabled="disabled">
                                    </div>
                                    <div class="d_flex flex_column mb_15">
                                        <p class="color_grey font_s15 line_h18 font_w500">Phòng ban</p>
                                        <input type="text" class="color_grey font_s14 line_h17 font_w400"
                                            value="Hiển thị phòng ban người nhận" disabled="disabled">
                                    </div>
                                </div>
                            </div>
                            <div class="d_flex flex_column">
                                <p class="color_grey font_s15 line_h18 font_w500">Ghi chú</p>
                                <textarea name="" id="" rows="5" placeholder="Nhập nội dung"></textarea>
                            </div>
                        </div>
                    </div>
                    <p class="tit_table_vt color_blue font_s16 line_h19 font_w700">Danh sách vật tư</p>
                    <div class="tb_operation_wh position_r d_flex align_c">
                        <div class="table_vt_scr">
                            <div class="table_ds_vt">
                                <table style="width: 1568px;"> 
                                    <tr class="tit_tbl color_white font_s16 line_h19 font_w500 back_blue">
                                        <th>STT<span class="span_tbody"></span></th>
                                        <th>Mã vật tư thiết bị<span class="span_tbody"></span></th>
                                        <th>Tên đầy đủ vật tư thiết bị<span class="span_tbody"></span></th>
                                        <th>Đơn vị tính<span class="span_tbody"></span></th>
                                        <th>Hãng sản xuất<span class="span_tbody"></span></th>
                                        <th>Xuất xứ<span class="span_tbody"></span></th>
                                        <th>Số lượng yêu cầu<span class="span_tbody"></span></th>
                                        <th>Số lượng thực tế xuất kho<span class="span_tbody"></span></th>
                                        <th>Đơn giá (VNĐ)<span class="span_tbody"></span></th>
                                        <th>Thành tiền (VNĐ)</th>
                                    </tr>
                                    <tr class="color_grey font_s14 line_h17 font_w400">
                                        <td>1</td>
                                        <td>VT-0000</td>
                                        <td style="text-align: left;"><a href="#" class="color_blue font_w500">Ống nhựa</a></td>
                                        <td>Chai</td>
                                        <td style="text-align: left;">Việt Hà</td>
                                        <td>Việt Nam</td>
                                        <td style="background: #EEEEEE; text-align: right;">100</td>
                                        <td style="text-align: right;"><input type="text" placeholder="Nhập số lượng"></td>
                                        <td style="text-align: right;">10.000.000</td>
                                        <td style="text-align: right;">10.000.000</td>
                                    </tr>
                                    <tr class="color_grey font_s14 line_h17 font_w400">
                                        <td>1</td>
                                        <td>VT-0000</td>
                                        <td style="text-align: left;"><a href="#" class="color_blue font_w500">Ống nhựa</a></td>
                                        <td>Chai</td>
                                        <td style="text-align: left;">Việt Hà</td>
                                        <td>Việt Nam</td>
                                        <td style="background: #EEEEEE; text-align: right;">100</td>
                                        <td style="text-align: right;"><input type="text" placeholder="Nhập số lượng"></td>
                                        <td style="text-align: right;">10.000.000</td>
                                        <td style="text-align: right;">10.000.000</td>
                                    </tr>
                                    <tr class="color_grey font_s14 line_h17 font_w400">
                                        <td>1</td>
                                        <td>VT-0000</td>
                                        <td style="text-align: left;"><a href="#" class="color_blue font_w500">Ống nhựa</a></td>
                                        <td>Chai</td>
                                        <td style="text-align: left;">Việt Hà</td>
                                        <td>Việt Nam</td>
                                        <td style="background: #EEEEEE; text-align: right;">100</td>
                                        <td style="text-align: right;"><input type="text" placeholder="Nhập số lượng"></td>
                                        <td style="text-align: right;">10.000.000</td>
                                        <td style="text-align: right;">10.000.000</td>
                                    </tr>
                                    <tr class="color_grey font_s14 line_h17 font_w400">
                                        <td>1</td>
                                        <td>VT-0000</td>
                                        <td style="text-align: left;"><a href="#" class="color_blue font_w500">Ống nhựa</a></td>
                                        <td>Chai</td>
                                        <td style="text-align: left;">Việt Hà</td>
                                        <td>Việt Nam</td>
                                        <td style="background: #EEEEEE; text-align: right;">100</td>
                                        <td style="text-align: right;"><input type="text" placeholder="Nhập số lượng"></td>
                                        <td style="text-align: right;">10.000.000</td>
                                        <td style="text-align: right;">10.000.000</td>
                                    </tr>
                                    <tr class="color_grey font_s14 line_h17 font_w400">
                                        <td>1</td>
                                        <td>VT-0000</td>
                                        <td style="text-align: left;"><a href="#" class="color_blue font_w500">Ống nhựa</a></td>
                                        <td>Chai</td>
                                        <td style="text-align: left;">Việt Hà</td>
                                        <td>Việt Nam</td>
                                        <td style="background: #EEEEEE; text-align: right;">100</td>
                                        <td style="text-align: right;"><input type="text" placeholder="Nhập số lượng"></td>
                                        <td style="text-align: right;">10.000.000</td>
                                        <td style="text-align: right;">10.000.000</td>
                                    </tr>
                                    <tr class="color_grey font_s14 line_h17 font_w400">
                                        <td>1</td>
                                        <td>VT-0000</td>
                                        <td style="text-align: left;"><a href="#" class="color_blue font_w500">Ống nhựa</a></td>
                                        <td>Chai</td>
                                        <td style="text-align: left;">Việt Hà</td>
                                        <td>Việt Nam</td>
                                        <td style="background: #EEEEEE; text-align: right;">100</td>
                                        <td style="text-align: right;"><input type="text" placeholder="Nhập số lượng"></td>
                                        <td style="text-align: right;">10.000.000</td>
                                        <td style="text-align: right;">10.000.000</td>
                                    </tr>
                                    <tr class="color_grey font_s14 line_h17 font_w400">
                                        <td>1</td>
                                        <td>VT-0000</td>
                                        <td style="text-align: left;"><a href="#" class="color_blue font_w500">Ống nhựa</a></td>
                                        <td>Chai</td>
                                        <td style="text-align: left;">Việt Hà</td>
                                        <td>Việt Nam</td>
                                        <td style="background: #EEEEEE; text-align: right;">100</td>
                                        <td style="text-align: right;"><input type="text" placeholder="Nhập số lượng"></td>
                                        <td style="text-align: right;">10.000.000</td>
                                        <td style="text-align: right;">10.000.000</td>
                                    </tr>
                                    <tr class="color_grey font_s14 line_h17 font_w400">
                                        <td>1</td>
                                        <td>VT-0000</td>
                                        <td style="text-align: left;"><a href="#" class="color_blue font_w500">Ống nhựa</a></td>
                                        <td>Chai</td>
                                        <td style="text-align: left;">Việt Hà</td>
                                        <td>Việt Nam</td>
                                        <td style="background: #EEEEEE; text-align: right;">100</td>
                                        <td style="text-align: right;"><input type="text" placeholder="Nhập số lượng"></td>
                                        <td style="text-align: right;">10.000.000</td>
                                        <td style="text-align: right;">10.000.000</td>
                                    </tr>
                                    <tr class="color_grey font_s14 line_h17 font_w400">
                                        <td>1</td>
                                        <td>VT-0000</td>
                                        <td style="text-align: left;"><a href="#" class="color_blue font_w500">Ống nhựa</a></td>
                                        <td>Chai</td>
                                        <td style="text-align: left;">Việt Hà</td>
                                        <td>Việt Nam</td>
                                        <td style="background: #EEEEEE; text-align: right;">100</td>
                                        <td style="text-align: right;"><input type="text" placeholder="Nhập số lượng"></td>
                                        <td style="text-align: right;">10.000.000</td>
                                        <td style="text-align: right;">10.000.000</td>
                                    </tr>
                                    <tr class="color_grey font_s14 line_h17 font_w400">
                                        <td>1</td>
                                        <td>VT-0000</td>
                                        <td style="text-align: left;"><a href="#" class="color_blue font_w500">Ống nhựa</a></td>
                                        <td>Chai</td>
                                        <td style="text-align: left;">Việt Hà</td>
                                        <td>Việt Nam</td>
                                        <td style="background: #EEEEEE; text-align: right;">100</td>
                                        <td style="text-align: right;"><input type="text" placeholder="Nhập số lượng"></td>
                                        <td style="text-align: right;">10.000.000</td>
                                        <td style="text-align: right;">10.000.000</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="pre_q d_flex align_c flex_center position_a">
                            <span class="pre_arrow"></span>
                        </div>
                        <div class="next_q d_flex align_c flex_center position_a">
                            <span class="next_arrow"></span>
                        </div>
                    </div>
                    <div class="btn_cf_rq d_flex flex_center">
                        <button type="button" class="btn_cancel back_w color_blue font_s15 line_h18 font_w500 cursor_p">Hủy</button>
                        <button type="submit" class="btn_save back_blue color_white font_s15 line_h18 font_w500 cursor_p">Lưu</button>
                    </div>
                </form>
            </div>

        </div>
    </div>
  </div>
  <?php include('../includes/popup_overview.php');  ?>
  <?php include('../includes/popup_nghiep-vu-kho-cho-xu-li.php');  ?>
</body>
<script type="text/javascript" src="../js/jquery-3.4.1.min.js"></script>
<script type="text/javascript" src="../js/select2.min.js"></script>
<script type="text/javascript" src="../js/jquery.validate.min.js"></script>
<script type="text/javascript" src="../js/js_h.js"></script>
<script type="text/javascript" src="../js/js_n.js"></script>
<script>
$('.active2').each(function() {
  if ($(this).hasClass('active2')) {
    $(this).find('a').addClass('active');
  }
});
</script>

</html>