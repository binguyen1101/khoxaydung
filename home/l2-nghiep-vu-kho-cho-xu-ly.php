<?php 
    include("config1.php"); 

    isset($_GET['page']) ? $page = $_GET['page'] : $page = 1;
    isset($_GET['type']) ? $type = $_GET['type'] : $type = "";
    isset($_GET['input']) ? $ip = $_GET['input'] : $ip = "";
    isset($_GET['dis']) ? $dis = $_GET['dis'] : $dis = 10;

?>
<!DOCTYPE html>
<html lang="vi">

<head>
    <title>Nghiệp vụ kho chờ xử lí</title>
    <meta charset="UTF-8">
    <meta name="robots" content="noindex,nofollow" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="../css/select2.min.css?v=<?= $ver ?>">
    <link rel="stylesheet" type="text/css" href="../css/style.css?v=<?= $ver ?>">
    <link rel="stylesheet" type="text/css" href="../css/style_h.css?v=<?= $ver ?>">
    <link rel="stylesheet" type="text/css" href="../css/style_n.css?v=<?= $ver ?>">
    <link rel="stylesheet" type="text/css" href="../css/style_k.css?v=<?= $ver ?>">
</head>

<body>

    <div class="box_right warehouse_pending">
        <?php include("../includes/sidebar.php"); ?>
        <div class="box_right_ct">
            <div class="block_change">
                <div class="head_wh d_flex space_b align_c">
                    <div class="head_tab d_flex space_b align_c">
                        <div class="icon_header open_sidebar_w">
                            <span class="icon_header_tbl"></span>
                            <span class="icon_header_tbl"></span>
                            <span class="icon_header_tbl"></span>
                        </div>
                        <?php include("../includes/header.php") ; ?>
                    </div>
                    <p class="color_grey font_s14 line_h17 font_w400">Nghiệp vụ kho chờ xử lý</p>
                    <?php include("../includes/header.php") ; ?>
                </div>
                <div class="operation_wh d_flex space_b">
                    <div class="search_wh d_flex space_b">
                        <div class="fil_wh">
                            <select class="all_type" name="all_type" style="width: 100%">
                                <option value="">Tất cả loại phiếu</option>
                                <!-- <option value="PYCCU">Phiếu yêu cầu cung ứng vật tư</option> -->
                                <option value="PNK" <?= ($type == "PNK") ? "selected" : "" ?>>Phiếu nhập kho</option>
                                <option value="PXK" <?= ($type == "PXK") ? "selected" : "" ?>>Phiếu xuất kho</option>
                                <option value="ĐCK" <?= ($type == "ĐCK") ? "selected" : "" ?>>Phiếu điều chuyển</option>
                                <option value="PKK" <?= ($type == "PKK") ? "selected" : "" ?>>Phiếu kiểm kê</option>
                            </select>
                        </div>
                        <div class="input_sr_wh">
                            <div class="box_input_sr position_r">
                                <input type="text" name="input_search" placeholder="Tìm kiếm theo số phiếu" value="<?= ($ip != "") ? $ip : "" ?>">
                                <span class="icon_sr_wh"></span>
                            </div>
                        </div>
                    </div>
                    <div class="export_wh d_flex space_b align_c">
                        <button class="btn_ex d_flex align_c cursor_p">
                            <img src="../images/export.png" alt="">
                            <p class="color_white font_s15 line_h18 font_w500">Xuất excel</p>
                        </button>
                        <div class="hd_ex d_flex align_c cursor_p">
                            <img src="../images/img_hd.png" alt="">
                            <p class="color_blue font_s15 line_h18 font_w500">Hướng dẫn</p>
                        </div>
                    </div>
                </div>
                <div class="ct_blockchange" data-page="<?= $page ?>" data-type="<?= $type ?>" data-ip ="<?= $ip ?>" data-dis ="<?= $dis ?>">
                <!--  -->
                </div>
            </div>

        </div>
    </div>
  </div>
  <?php include('../includes/popup_overview.php');  ?>
  <?php include('../includes/ghi_chu.php');  ?>
</body>
<script type="text/javascript" src="../js/jquery-3.4.1.min.js"></script>
<script type="text/javascript" src="../js/select2.min.js"></script>
<script type="text/javascript" src="../js/jquery.validate.min.js"></script>
<script type="text/javascript" src="../js/js_h.js"></script>
<script type="text/javascript" src="../js/js_n.js"></script>

<script>
$('.active2').each(function() {
  if ($(this).hasClass('active2')) {
    $(this).find('a').addClass('active');
  }
});

var page = $(".ct_blockchange").attr("data-page");
var type = $(".ct_blockchange").attr("data-type");
var input_val = $(".ct_blockchange").attr("data-ip");
var curr = $(".ct_blockchange").attr("data-dis");

$.ajax({
    url: "../render/tb_kho_cho_xu_li.php",
    type: "POST",
    data:{
        type: type,
        input_val: input_val,
        page: page,
        curr: curr
    },
    success: function(data){
        $(".ct_blockchange").append(data);
    }
});


$(".btn_ex").click(function() {
    window.location.href = '../Excel/kho_cho_xu_li.php';
});

$(".all_type").on("change",function () {
    var page = 1;
    var type = $('.all_type').val();
    var input_val = $("input[name='input_search']").val();
    var curr = $('.show_tr_tb').val();
    if(type == "" && input_val == "" && curr == 10){
        window.location.href = "/nghiep-vu-kho-cho-xu-ly.html?dis=" + curr + '&page=' + page;
    }else{
        window.location.href = "/nghiep-vu-kho-cho-xu-ly.html?type=" + type + "&input=" + input_val + "&dis=" + curr+ '&page=' + page;
    }
    $("input[name='input_search'").val('');
});

$(".icon_sr_wh").click(function(){
    var page = 1;
    var type = $('.all_type').val();
    var input_val = $("input[name='input_search']").val();
    var curr = $('.show_tr_tb').val();
    if(type == "" && input_val == "" && curr == 10){
        window.location.href = "/nghiep-vu-kho-cho-xu-ly.html?dis=" + curr + '&page=' + page;
    }else{
        window.location.href = "/nghiep-vu-kho-cho-xu-ly.html?type=" + type + "&input=" + input_val + "&dis=" + curr+ '&page=' + page;
    }
    $("input[name='input_search'").val('');
});

$(document).keyup(function (e) {
    if ($("input[name='input_search']").is(":focus") && (e.keyCode == 13)) {
        $(".icon_sr_wh").click();
    }
});

function display(select){
    var curr = $(select).val();
    var page = 1;
    var type = $('.all_type').val();
    var input_val = $("input[name='input_search']").val();
    if(type == "" && input_val == "" && curr == 10){
        window.location.href = "/nghiep-vu-kho-cho-xu-ly.html?dis=" + curr + '&page=' + page;
    }else{
        window.location.href = "/nghiep-vu-kho-cho-xu-ly.html?type=" + type + "&input=" + input_val + "&dis=" + curr + '&page=' + page;
    }
    $("input[name='input_search'").val('');
}
</script>

</html>