<?php
include("config1.php");

if (!in_array(1, $ro_vattu)) {
    header("Location: /tong-quan.html");
}

$id_vt = getValue('id', 'int', 'GET', '');

if (isset($_COOKIE['user']) && $_COOKIE['user'] != "" && $_COOKIE['role'] == 1) {
    $token = $_COOKIE['acc_token'];
    $curl = curl_init();
    $data = array();
    curl_setopt($curl, CURLOPT_POST, 1);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
    curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/user_info_company.php');
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));

    $response = curl_exec($curl);
    curl_close($curl);
    $data_tt = json_decode($response, true);
    $tt_user = $data_tt['data']['user_info_result'];
}
if (isset($_COOKIE['user']) && $_COOKIE['user'] != "" && $_COOKIE['role'] == 2) {
    $token = $_COOKIE['acc_token'];
    $curl = curl_init();
    $data = array();
    curl_setopt($curl, CURLOPT_POST, 1);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
    curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/user_info_employee.php');
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));
    $response = curl_exec($curl);
    curl_close($curl);
    $data_tt = json_decode($response, true);
    $tt_user = $data_tt['data']['user_info_result'];
}
$id_cty = $tt_user['com_id'];

if ($id_vt != "") {
    $item = new db_query("SELECT `dsvt_id`, `dsvt_maVatTuThietBi`, `dsvt_name`, `dsvt_donGia`, `dsvt_description`,`dsvt_dateCreate`,`dsvt_img`, `nvt_name`, `dvt_name`, `hsx_name`, `xx_name`, `dsvt_userCreateId` FROM `danh-sach-vat-tu` 
        LEFT JOIN `nhom-vat-tu-thiet-bi` ON `dsvt_nhomVatTuThietBi` = `nvt_id`
        LEFT JOIN `don-vi-tinh` ON `dsvt_donViTinh` = `dvt_id`
        LEFT JOIN `hang-san-xuat` ON `dsvt_hangSanXuat` = `hsx_id`
        LEFT JOIN `xuat-xu` ON `dsvt_xuatXu` = `xx_id`
        WHERE `dsvt_id` = $id_vt AND `dsvt_id_ct` = $id_cty");
    if (isset($_SESSION['quyen']) && $_SESSION['quyen'] == 1) {
        $curl = curl_init();
        $token = $_COOKIE['acc_token'];
        curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/list_all_employee_of_company.php');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));
        $response = curl_exec($curl);
        curl_close($curl);

        $data_list = json_decode($response, true);
        $data_list_nv = $data_list['data']['items'];
        $count = count($data_list_nv);
    } 
    if (isset($_SESSION['quyen']) && ($_SESSION['quyen'] == 2)) {
        $curl = curl_init();
        $token = $_COOKIE['acc_token'];
        curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/list_all_my_partner.php?get_all=true');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));
        $response = curl_exec($curl);
        curl_close($curl);

        $data_list = json_decode($response, true);
        $data_list_nv = $data_list['data']['items'];
        $count = count($data_list_nv);
    }
    $newArr = [];
    for ($i = 0; $i < count($data_list_nv); $i++) {
        $value = $data_list_nv[$i];
        $newArr[$value["ep_id"]] = $value;
    }
};
// echo "<pre>";
// print_r($tt_user);
// echo "</pre>";
// die();

?>
<!DOCTYPE html>
<html lang="vi">

<head>
    <title>Danh sách vật tư thiết bị</title>
    <meta charset="UTF-8">
    <meta name="robots" content="noindex,nofollow" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="../css/style.css?v=<?= $ver ?>">
    <link rel="stylesheet" type="text/css" href="../css/style_h.css?v=<?= $ver ?>">
    <link rel="stylesheet" type="text/css" href="../css/style_n.css?v=<?= $ver ?>">
</head>

<body>
    <div class="main_wrapper_all">
        <div class="wapper_all">
            <?php include('../includes/sidebar.php');  ?>
        </div>
        <div class="main_overview" id="main_overview">
            <div class="header_menu_overview d_flex align_c space_b">
                <div class="text_link_header_back" style="display: block;">
                    <div class=" d_flex align_c">
                        <a href="/danh-sach-vat-tu-thiet-bi.html" class="cursor_p">
                            <img src="../images/back_item_g.png" alt="">
                        </a>
                        <p class="color_grey line_16 font_s14 ml_10">Thông tin vật tư thiết bị / Danh sách vật tư thiết bị /
                            Chi tiết</p>
                    </div>
                </div>
                <img class="open_sidebar_w" src="../images/open_sidebar_w.png" alt="" onclick="toggle('main_sidebar')">
                <?php include('../includes/header.php');  ?>
            </div>
            <div class="body_equipment_supplies">
                <div class="text_link_body_back" style="display: none;">
                    <div class=" d_flex align_c mb_15">
                        <a href="/danh-sach-vat-tu-thiet-bi.html" class="cursor_p">
                            <img src="../images/back_item_g.png" alt="">
                        </a>
                        <p class="color_grey line_16 font_s14 ml_10">Thông tin vật tư thiết bị / Danh sách vật tư thiết bị /
                            Chi tiết</p>
                    </div>
                </div>
                <div class="d_flex align_c space_b mb_30">
                    <div>
                    </div>
                    <div class="d_flex align_c flex_w">
                        <?php if (in_array(4, $ro_vattu)) { ?>
                            <div id="index2" class="d_flex align_c">
                                <button class="button_delete cursor_p" onclick="openAndHide('','delete_materials_equipment')">
                                    <p class="color_blue font_s15 line_h18 font_w500">Xóa vật tư</p>
                                </button>
                            </div>
                        <?php } ?>
                        <div id="index1" class="d_flex align_c">
                            <?php if (in_array(3, $ro_vattu)) { ?>
                                <a href="/danh-sach-vat-tu-thiet-bi-sua-<?= $id_vt ?>.html">
                                    <button class="d_flex align_c button_add_new cursor_p ml_15">
                                        <img class="mr_10 img_button_add_new" src="../images/pen_edit.png" alt="">
                                        <p class="color_white line_18 font_s15 font_w500">Chỉnh sửa</p>
                                    </button>
                                </a>
                            <?php } ?>
                            <button class="d_flex align_c button_export_excel ml_15 cursor_p">
                                <a href="../Excel/vat_tu.php?id=<?= $id_vt ?>"></a>
                                <img class="mr_10 img_button_export_excel" src="../images/export_excel.png" alt="">
                                <p class="color_white line_18 font_s15 font_w500">Xuất excel</p>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="body_equipment_supplies_detail">
                    <div class="header_body_b color_white line_h19 font_s16 font_wB">
                        Thông tin vật tư thiết bị
                    </div>
                    <div class="srcoll_body_equipment_supplies_detail">
                        <? while ($data = mysql_fetch_assoc($item->result)) { ?>
                            <div class="d_flex flex_w srcoll_equipment_supplies_detail" style="flex-direction: row;">
                                <div id="description_detail_table" class="description_detail_table">
                                    <div class="d_flex align_c boder_detail mt_17">
                                        <p class="title_detail font_s14 line_16 color_grey">Mã vật tư thiết bị:</p>
                                        <p class="description_detail font_s14 line_16 color_grey font_w500">VT-<?= $data['dsvt_id'] ?></p>
                                    </div>
                                    <div class="d_flex align_c boder_detail mt_17">
                                        <p class="title_detail font_s14 line_16 color_grey">Tên đầy đủ vật tư thiết bị:</p>
                                        <p class="description_detail font_s14 line_16 color_grey font_w500"><?= $data['dsvt_name'] ?></p>
                                    </div>
                                    <div class="d_flex align_c boder_detail mt_17">
                                        <p class="title_detail font_s14 line_16 color_grey">Nhóm vật tư thiết bị:</p>
                                        <p class="description_detail font_s14 line_16 color_grey font_w500"><?= $data['nvt_name'] ?></p>
                                    </div>
                                    <div class="d_flex align_c boder_detail mt_17">
                                        <p class="title_detail font_s14 line_16 color_grey">Đơn vị tính:</p>
                                        <p class="description_detail font_s14 line_16 color_grey font_w500"><?= $data['dvt_name'] ?></p>
                                    </div>
                                    <div class="d_flex align_c boder_detail mt_17">
                                        <p class="title_detail font_s14 line_16 color_grey">Đơn giá:</p>
                                        <p class="description_detail font_s14 line_16 color_grey font_w500"><?= $data['dsvt_donGia'] ?> VNĐ</p>
                                    </div>
                                    <div class="d_flex align_c boder_detail mt_17">
                                        <p class="title_detail font_s14 line_16 color_grey">Hãng sản xuất:</p>
                                        <p class="description_detail font_s14 line_16 color_grey font_w500"><?= $data['hsx_name'] ?></p>
                                    </div>
                                    <div class="d_flex align_c boder_detail mt_17">
                                        <p class="title_detail font_s14 line_16 color_grey">Xuất xứ:</p>
                                        <p class="description_detail font_s14 line_16 color_grey font_w500"><?= $data['xx_name'] ?></p>
                                    </div>
                                    <div class="d_flex align_c boder_detail mt_17">
                                        <p class="title_detail font_s14 line_16 color_grey">Người tạo:</p>
                                        <div class="d_flex align_c">    
                                            <? if ($data['dsvt_userCreateId'] == 0) { ?>
                                                <img class="ava_human_table" src="<?= 'https://chamcong.24hpay.vn/upload/company/logo/' . $tt_user['com_logo'] ?>" alt="">
                                                <p class=" font_s14 line_16 color_grey font_w500"><?= $tt_user['com_name'] ?></p>
                                            <? } ?>
                                            <? if ($data['dsvt_userCreateId'] != 0) { ?>
                                                <img class="ava_human_table" src="<?= 'https://chamcong.24hpay.vn/upload/employee/' . $newArr[$data['dsvt_userCreateId']]['ep_image'] ?>" alt="">
                                                <p class=" font_s14 line_16 color_grey font_w500"><?= $newArr[$data['dsvt_userCreateId']]['ep_name'] ?></p>
                                            <? } ?>
                                        </div>
                                    </div>
                                    <div class="d_flex align_c boder_detail mt_17">
                                        <p class="title_detail font_s14 line_16 color_grey">Ngày tạo:</p>
                                        <p class="description_detail font_s14 line_16 color_grey font_w500"><?= $data['dsvt_dateCreate'] ?></p>
                                    </div>
                                </div>
                                <div id="upload_equipment_supplies" class="upload_equipment_supplies mt_20">
                                    <?php
                                    if ($data['dsvt_img'] == "") {
                                        echo "<img class='img_detail' src='../images/defau_img.png'>";
                                    } else {
                                        echo "<img class='img_detail' src='../pictures/" . $data['dsvt_img'] . "'>";
                                    }
                                    ?>
                                </div>
                            </div>
                            <div class="d_flex ml_20 padding_b20 mr_20 description_detail_equipment">
                                <p class="title_detail font_s14 line_16 color_grey">Mô tả:</p>
                                <div class="description_detail font_s14 line_16 color_grey font_w500">
                                    <?= $data['dsvt_description'] ?>
                                </div>
                            </div>
                        <? } ?>
                    </div>
                </div>
            </div>
        </div>
        <?php include('../includes/popup_overview.php');  ?>
        <?php include('../includes/popup_don-vi-tinh.php');  ?>
        <?php include('../includes/popup_h.php');  ?>
    </div>
</body>
<script type="text/javascript" src="../js/jquery-3.4.1.min.js"></script>
<script type="text/javascript" src="../js/jquery.validate.min.js"></script>
<script type="text/javascript" src="../js/select2.min.js"></script>
<script type="text/javascript" src="../js/js_h.js"></script>
<script type="text/javascript" src="../js/validate_h.js"></script>
<script>
    $('.active3').each(function() {
        if ($(this).hasClass('active3')) {
            $(this).parent().addClass('show');
            $(this).parent().parent().find('.item_sidebar_cha').addClass('active');
            $(this).find('a').addClass('active');
        }
    });
</script>

</html>