<?php
include("config1.php");
if (!in_array(3, $ro_vattu)) {
    header("Location: /danh-sach-vat-tu-thiet-bi.html");
}
isset($_GET['id']) ? $id = $_GET['id'] : $id = "";

if (isset($_COOKIE['user']) && $_COOKIE['user'] != "" && $_COOKIE['role'] == 1) {
    $token = $_COOKIE['acc_token'];
    $curl = curl_init();
    $data = array();
    curl_setopt($curl, CURLOPT_POST, 1);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
    curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/user_info_company.php');
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));

    $response = curl_exec($curl);
    curl_close($curl);
    $data_tt = json_decode($response, true);
    $tt_user = $data_tt['data']['user_info_result'];
    // $id_nguoi_xoa = $_SESSION['com_id'];
} else if (isset($_COOKIE['user']) && $_COOKIE['user'] != "" && $_COOKIE['role'] == 2) {
    $token = $_COOKIE['acc_token'];
    $curl = curl_init();
    $data = array();
    curl_setopt($curl, CURLOPT_POST, 1);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
    curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/user_info_employee.php');
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));
    $response = curl_exec($curl);
    curl_close($curl);
    $data_tt = json_decode($response, true);
    $tt_user = $data_tt['data']['user_info_result'];
    // $id_nguoi_xoa = $_SESSION['ep_id'];
}

$id_cty = $tt_user['com_id'];
if ($id != "") {
    $item = new db_query("SELECT `dsvt_id`, `dsvt_maVatTuThietBi`,`dsvt_nhomVatTuThietBi`,`dsvt_donViTinh`,`dsvt_hangSanXuat`, `dsvt_name`, `dsvt_donGia`, `dsvt_description`,`dsvt_xuatXu`,`dsvt_dateCreate`,`dsvt_img`, `nvt_name`, `dvt_name`, `hsx_name`, `xx_name`, `dsvt_userCreateId` FROM `danh-sach-vat-tu` 
        LEFT JOIN `nhom-vat-tu-thiet-bi` ON `dsvt_nhomVatTuThietBi` = `nvt_id`
        LEFT JOIN `don-vi-tinh` ON `dsvt_donViTinh` = `dvt_id`
        LEFT JOIN `hang-san-xuat` ON `dsvt_hangSanXuat` = `hsx_id`
        LEFT JOIN `xuat-xu` ON `dsvt_xuatXu` = `xx_id`
        WHERE `dsvt_id` = $id ");
    $select_vttb = new db_query("SELECT `nvt_id`, `nvt_name` FROM `nhom-vat-tu-thiet-bi` WHERE `nvt_check` ='1' AND `nvt_id_ct` = $id_cty");
    $select_dvt = new db_query("SELECT `dvt_id`, `dvt_name` FROM `don-vi-tinh` WHERE `dvt_check` ='1' AND `dvt_id_ct` = $id_cty");
    $select_hsx = new db_query("SELECT `hsx_id`, `hsx_name` FROM `hang-san-xuat` WHERE `hsx_check` ='1' AND `hsx_id_ct`= $id_cty");
    $select_xx = new db_query("SELECT `xx_id`, `xx_name` FROM `xuat-xu`");
    $date = date('Y-m-d', time());
    // echo "<pre>";
    // print_r($item);
    // echo "</pre>";   
    // die();
    if (isset($_SESSION['quyen']) && $_SESSION['quyen'] == 1) {
        $curl = curl_init();
        $token = $_COOKIE['acc_token'];
        curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/list_all_employee_of_company.php');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));
        $response = curl_exec($curl);
        curl_close($curl);

        $data_list = json_decode($response, true);

        $data_list_nv = $data_list['data']['items'];
        $count = count($data_list_nv);
    } elseif (isset($_SESSION['quyen']) && ($_SESSION['quyen'] == 2)) {
        $curl = curl_init();
        $token = $_COOKIE['acc_token'];
        curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/list_all_my_partner.php?get_all=true');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));
        $response = curl_exec($curl);
        curl_close($curl);

        $data_list = json_decode($response, true);
        $data_list_nv = $data_list['data']['items'];
        $count = count($data_list_nv);
        $newArr = [];
        for ($i = 0; $i < count($data_list_nv); $i++) {
            $value = $data_list_nv[$i];
            $newArr[$value["ep_id"]] = $value;
        }
    }
};
?>
<!DOCTYPE html>
<html lang="vi">

<head>
    <title>Danh sách vật tư thiết bị</title>
    <meta charset="UTF-8">
    <meta name="robots" content="noindex,nofollow" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="../css/select2.min.css?v=<?= $ver ?>">
    <link rel="stylesheet" type="text/css" href="../css/style.css?v=<?= $ver ?>">
    <link rel="stylesheet" type="text/css" href="../css/style_h.css?v=<?= $ver ?>">
    <link rel="stylesheet" type="text/css" href="../css/style_n.css?v=<?= $ver ?>">
</head>

<body class="seclec2_radius">
    <div class="main_wrapper_all">
        <div class="wapper_all">
            <?php include('../includes/sidebar.php');  ?>
        </div>
        <div class="main_overview" id="main_overview">
            <div class="header_menu_overview d_flex align_c space_b">
                <div class="text_link_header_back" style="display: block;">
                    <div class=" d_flex align_c">
                        <a href="/danh-sach-vat-tu-thiet-bi.html" class="cursor_p">
                            <img src="../images/back_item_g.png" alt="">
                        </a>
                        <p class="color_grey line_16 font_s14 ml_10">Thông tin vật tư thiết bị / Danh sách vật tư thiết bị /
                            Chỉnh sửa</p>
                    </div>
                </div>
                <img class="open_sidebar_w" src="../images/open_sidebar_w.png" alt="" onclick="toggle('main_sidebar')">
                <?php include('../includes/header.php');  ?>
            </div>
            <div class="body_equipment_supplies">
                <div class="text_link_body_back" style="display: none;">
                    <div class=" d_flex align_c mb_15">
                        <a href="/danh-sach-vat-tu-thiet-bi.html" class="cursor_p">
                            <img src="../images/back_item_g.png" alt="">
                        </a>
                        <p class="color_grey line_16 font_s14 ml_10">Thông tin vật tư thiết bị / Danh sá ch vật tư thiết bị /
                            Chỉnh sửa</p>
                    </div>
                </div>
                <form id="edit_equipment_supplies" name="edit_equipment_supplies">
                    <? while ($data = mysql_fetch_assoc($item->result)) { ?>
                        <div class="body_equipment_supplies_edit">
                            <div class="header_body_b color_white line_h19 font_s16 font_wB">
                                Chỉnh sửa vật tư thiết bị
                            </div>
                            <div class="d_flex flex_w" style="flex-direction: row;">
                                <div id="add_infomation" class="input_create_equipment_supplies mt_20">
                                    <div>
                                        <p class="font_s15 line_h18 font_w500 color_grey mb_5">Mã vật tư thiết bị</p>
                                        <input class="input_value_grey font_14 line_h16" disabled type="text" value="VT - <?= $data['dsvt_id'] ?>">
                                    </div>
                                    <div class="mt_20 fullname_equipment">
                                        <div class="d_flex">
                                            <p class="font_s15 line_h18 font_w500 color_grey mb_5 ">Tên đầy đủ thiết bị vật tư</p>
                                            <span class="color_red alert_red">*</span>
                                        </div>
                                        <input class="input_value font_14 line_h16" name="name_equipment" id="name_equipment" type="text" placeholder="Nhập tên vật tư thiết bị" value="<?= $data['dsvt_name'] ?>">
                                    </div>
                                    <div class="mt_20">
                                        <div class="d_flex align_c space_b">
                                            <p class="font_s15 line_h18 font_w500 color_grey mb_5 ">Nhóm vật tư thiết bị</p>
                                            <div class="d_flex align_c cursor_p" onclick="openAndHide('','add_new_materials_equipment')">
                                                <img src="../images/add_circle_b.png" alt="">
                                                <p class="color_blue line_h16 font_s14 ml_5">Thêm nhóm</p>
                                            </div>
                                        </div>
                                        <div class="select_create">
                                            <select class="select_create group_materials_equipment" name="group_materials_equipment">
                                                <option value="">Chọn nhóm vật tư thiết bị</option>
                                                <? while ($nvttb = mysql_fetch_assoc($select_vttb->result)) { ?>
                                                    <option value="<?= $nvttb['nvt_id'] ?>" <?= ($nvttb['nvt_id'] == $data['dsvt_nhomVatTuThietBi']) ? "selected" : "" ?>> <?= $nvttb['nvt_name'] ?></option>
                                                <? } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="mt_20 unit_equipment">
                                        <div class="d_flex align_c space_b">
                                            <div class="d_flex">
                                                <p class="font_s15 line_h18 font_w500 color_grey mb_5 ">Đơn vị tính</p>
                                                <span class="color_red alert_red">*</span>
                                            </div>
                                            <div class="d_flex align_c btn_add_unit cursor_p">
                                                <img src="../images/add_circle_b.png" alt="">
                                                <p class="color_blue line_h16 font_s14 ml_5">Thêm đơn vị tính</p>
                                            </div>
                                        </div>
                                        <div class="select_create">
                                            <select class="select_create select_unit" name="select_unit" id="select_unit">
                                                <? while ($dvt = mysql_fetch_assoc($select_dvt->result)) { ?>
                                                    <option value="<?= $dvt['dvt_id'] ?>" <?= ($dvt['dvt_id'] == $data['dsvt_donViTinh']) ? "selected" : "" ?>><?= $dvt['dvt_name'] ?></option>
                                                <? } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="mt_20">
                                        <div class="d_flex">
                                            <p class="font_s15 line_h18 font_w500 color_grey mb_5 ">Đơn giá</p>
                                        </div>
                                        <div class="position_r">
                                            <input class="input_value font_14 line_h16 unit_price" name="unit_price" type="text" placeholder="Nhập đơn giá" value="<?= $data['dsvt_donGia'] ?>">
                                            <span class="position_a text_unit_price font_s14 line_h16 color_grey">VND</span>
                                        </div>
                                    </div>
                                </div>
                                <div id="add_equip" class="input_create_equipment_supplies mb_20">
                                    <div class="mt_20">
                                        <div class="d_flex align_c space_b">
                                            <p class="font_s15 line_h18 font_w500 color_grey mb_5 ">Hãng sản xuất</p>
                                        </div>
                                        <div class="select_create">
                                            <select class="select_create select_manufacturer" name="select_manufacturer">
                                                <option value="">Chọn hãng sản xuất</option>
                                                <? while ($hsx = mysql_fetch_assoc($select_hsx->result)) { ?>
                                                    <option value="<?= $hsx['hsx_id'] ?>" <?= ($hsx['hsx_id'] == $data['dsvt_hangSanXuat']) ? "selected" : "" ?>><?= $hsx['hsx_name'] ?></option>
                                                <? } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="mt_20">
                                        <div class="d_flex align_c space_b">
                                            <p class="font_s15 line_h18 font_w500 color_grey mb_5 ">Xuất xứ</p>
                                        </div>
                                        <div class="select_create">
                                            <select class="select_create select_origin" name="select_origin">
                                                <option value="">Chọn xuất xứ</option>
                                                <? while ($xx = mysql_fetch_assoc($select_xx->result)) { ?>
                                                    <option value="<?= $xx['xx_id'] ?>" <?= ($xx['xx_id'] == $data['dsvt_xuatXu']) ? "selected" : "" ?>><?= $xx['xx_name'] ?></option>
                                                <? } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="mt_20">
                                        <p class="font_s15 line_h18 font_w500 color_grey mb_5">Người tạo</p>
                                        <!-- <input class="input_value_grey font_14 line_h16 " readonly type="text" data='<?= $data['dsvt_userCreateId'] ?>' value="<?= $newArr[$data['dsvt_userCreateId']]['ep_name'] ?>"> -->

                                        <? if ($data['dsvt_userCreateId'] == 0) { ?>
                                            <input class="input_value_grey font_14 line_h16 " readonly data='<?= $data['dsvt_userCreateId'] ?>' value="<?= $tt_user['com_name'] ?>"></input>
                                        <? } ?>
                                        <? if ($data['dsvt_userCreateId'] != 0) { ?>
                                            <input class="input_value_grey font_14 line_h16 " readonly data='<?= $data['dsvt_userCreateId'] ?>' value="<?= $newArr[$data['dsvt_userCreateId']]['ep_name'] ?>"></input>
                                        <? } ?>
                                    </div>
                                    <div class="mt_20">
                                        <p class="font_s15 line_h18 font_w500 color_grey mb_5">Ngày tạo</p>
                                        <input class="input_value_grey font_14 line_h16 " readonly type="text" placeholder="10/10/2020" value="<?= $data['dsvt_dateCreate'] ?>">
                                    </div>
                                </div>
                                <div id="upload_equips" class="upload_equipment_supplies mt_20 ">
                                    <p class=" font_s15 line_h18 font_w500 color_grey mb_5">Hình ảnh thiết bị vật tư</p>
                                    <label for="input_file_chat" class="input_file_img1 input_file_img position_r">
                                        <div class="upload_file_img d_flex align_c flex_center <?= ($data['dsvt_img'] == "") ? "d_flex" : "display_none" ?>">
                                            <picture>
                                                <img class="d_flex align_c margin_a" src="../images/camera_b.png" alt="">
                                                <p class="font_s14 line_h16 color_blue text_a_c">Tải lên
                                                    hình
                                                    ảnh</p>
                                                <input type="file" id="input_file_chat" class="" hidden>
                                            </picture>
                                        </div>
                                    </label>
                                    <div class="upload_logo_vehicle_done position_r <?= ($data['dsvt_img'] == "") ? "display_none" : "" ?>">
                                        <!-- <img class="ready_upload_logo" src="" alt=""> -->
                                        <?php
                                        if ($data['dsvt_img'] == "") {
                                            echo "<img class='ready_upload_logo' src='' alt=''>";
                                        } else {
                                            echo "<img class='ready_upload_logo' src='../pictures/" . $data['dsvt_img'] . "'>";
                                        }
                                        ?>
                                        <label for="upload_logo">
                                            <img class="add_logo position_a" src="" alt="">
                                            <input type="file" id="upload_logo" class=" display_none" accept=".png, .jpg, .jpeg">
                                        </label>
                                        <img class="del_logo position_a cursor_p" src="../images/close_b.png" alt="">
                                    </div>
                                </div>
                                <div id="details_equip" class="upload_equipment_supplies mb_20">
                                    <p class="mt_20 font_s15 line_h18 font_w500 color_grey mb_5">Mô tả vật tư thiết bị</p>
                                    <div id="editor" style="height: 258px">
                                        <?= $data['dsvt_description'] ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <? } ?>
                    <div class="d_flex align_c flex_center button_add_new_equipment_supplies">
                        <button class="button_close color_blue font_s15 line_h18 font_w500" onclick="openAndHide('','popup_cancel_equipment_materials')">Hủy</button>
                        <button class="button_accp color_white font_s15 line_h18 font_w500" data="<?= $id ?>" type="button">Lưu</button>
                        <!-- onclick="openAndHide('','edit_equipment_materials_success')" -->
                    </div>
                </form>
            </div>
        </div>
        <?php include('../includes/popup_overview.php');  ?>
        <?php include('../includes/popup_don-vi-tinh.php');  ?>
        <?php include('../includes/popup_h.php');  ?>
    </div>
</body>
<script type="text/javascript" src="../js/jquery-3.4.1.min.js"></script>
<script type="text/javascript" src="../js/jquery.validate.min.js"></script>
<script type="text/javascript" src="../js/select2.min.js"></script>
<script type="text/javascript" src="../js/js_h.js"></script>
<script type="text/javascript" src="../js/validate_h.js"></script>
<script type="text/javascript" src="../js/js_k.js"></script>
<script type="text/javascript" src="../js/js_n.js"></script>
<script src="//cdn.ckeditor.com/4.17.1/standard/ckeditor.js"></script>
<script>
    CKEDITOR.replace('editor');
</script>
<script>
    $('.active3').each(function() {
        if ($(this).hasClass('active3')) {
            $(this).parent().addClass('show');
            $(this).parent().parent().find('.item_sidebar_cha').addClass('active');
            $(this).find('a').addClass('active');
        }
    });

    $(".them_moi_nvttb").click(function() {
        var form_add = $("#add_materials_equipment");
        form_add.validate({
            errorPlacement: function(error, element) {
                error.appendTo(element.parents(".input_materials_equipment"));
                error.wrap("<span class='error'>")
            },
            rules: {
                name_materials_equipment: "required",
            },
            messages: {
                name_materials_equipment: "Vui lòng nhập tên nhóm thiết bị vật tư",
            },
        });
        if (form_add.valid() === true) {
            var nvttb_name = $('input[name="name_materials_equipment"]').val()
            var nvttb_description = $('textarea[name="description_materials_equipment"]').val()
            $.ajax({
                url: '../ajax/add_nhom_vat_tu.php',
                type: 'POST',
                data: {
                    nvttb_name: nvttb_name,
                    nvttb_description: nvttb_description
                },
                success: function(data) {
                    if (data == '') {
                        var text = $('.add_new_materials_equipment_success .add_new_succ').text('');
                        var text_new = '';
                        text_new += 'Thêm mới nhóm vật tư thiết bị';
                        text_new += '<strong>';
                        text_new += '&nbsp' + nvttb_name;
                        text_new += '</strong>';
                        text_new += '&nbspthành công!';
                        text.append(text_new);
                        $('.add_new_materials_equipment').hide()
                        $('.add_new_materials_equipment_success').show()
                    } else if (data != '') {
                        alert("Lỗi!");
                    }
                }
            });
            $('.btn_close').on("click", function() {
                window.location.reload();
            })
        }
    });

    $('.them_moi_dvt').click(function() {
        var form_add = $("#add_unit");
        form_add.validate({
            errorPlacement: function(error, element) {
                error.appendTo(element.parents(".input_unit"));
                error.wrap("<span class='error'>")
            },
            rules: {
                name_unit: "required",
            },
            messages: {
                name_unit: "Vui lòng nhập tên đơn vị tính",
            },
        });
        if (form_add.valid() === true) {
            var dvt_name = $('input[name="name_unit"]').val()
            var dvt_description = $('textarea[name="description_unit"]').val()
            $.ajax({
                url: '../ajax/add_don_vi_tinh.php',
                type: 'POST',
                data: {
                    dvt_name: dvt_name,
                    dvt_description: dvt_description
                },
                success: function(data) {
                    var text = $('.add_new_succ').text('');
                    var text_new = '';
                    text_new += 'Thêm mới đơn vị tính';
                    text_new += '<strong>';
                    text_new += '&nbsp' + dvt_name;
                    text_new += '</strong>';
                    text_new += '&nbspthành công!';
                    text.append(text_new);
                    $('.add_new_unit').hide()
                    $('.add_new_materials_equipment_success').show()
                }
            });
            $('.btn_close').on("click", function() {
                window.location.reload();
            })
        }
    });

    $('.button_accp').click(function() {
        var dsvt_name = $('input[name="name_equipment"]').val()
        var dsvt_nhomVatTuThietBi = $('select[name="group_materials_equipment"]').val()
        var dsvt_donGia = $('input[name="unit_price"]').val()
        var dsvt_hangSanXuat = $('select[name="select_manufacturer"]').val()
        var dsvt_donViTinh = $('select[name="select_unit"]').val()
        var dsvt_xuatXu = $('select[name="select_origin"]').val()
        var dsvt_dateCreate = $('input[name="date_time_create"]').val()
        var dsvt_description = CKEDITOR.instances.editor.getData();
        var dsvt_id = $(this).attr('data');
        var id_cty = "<?= $id_cty ?>";
        var file_data = $('#input_file_chat').prop('files')[0];
        var form_add = $("#edit_equipment_supplies");
        form_add.validate({
            errorPlacement: function(error, element) {
                error.appendTo(element.parents(".fullname_equipment"));
                error.appendTo(element.parents(".unit_equipment"));
                error.wrap("<span class='error'>")
            },
            rules: {
                name_equipment: "required",
                select_unit: "required",
            },
            messages: {
                name_equipment: "Vui lòng nhập tên nhóm thiết bị vật tư",
                select_unit: "Vui lòng nhập tên nhóm thiết bị vật tư",
            },
        });
        if (form_add.valid() === true) {
            var fd = new FormData();
            fd.append('file', file_data);
            fd.append('id_cty', id_cty);
            fd.append('dsvt_name', dsvt_name);
            fd.append('dsvt_nhomVatTuThietBi', dsvt_nhomVatTuThietBi);
            fd.append('dsvt_donViTinh', dsvt_donViTinh);
            fd.append('dsvt_donGia', dsvt_donGia);
            fd.append('dsvt_hangSanXuat', dsvt_hangSanXuat);
            fd.append('dsvt_xuatXu', dsvt_xuatXu);
            // fd.append('dsvt_userCreateId', dsvt_userCreateId);
            fd.append('dsvt_dateCreate', dsvt_dateCreate);
            fd.append('dsvt_description', dsvt_description);
            // fd.append('role', role);
            fd.append('dsvt_id', dsvt_id);
            $.ajax({
                url: '../ajax/edit_danh_sach_vat_tu.php',
                type: 'POST',
                // dataType: 'Json',
                contentType: false,
                processData: false,
                data: fd,
                // data: {
                    // dsvt_id: dsvt_id,
                    // dsvt_name: dsvt_name,
                    // dsvt_nhomVatTuThietBi: dsvt_nhomVatTuThietBi,
                    // dsvt_donGia: dsvt_donGia,
                    // dsvt_hangSanXuat: dsvt_hangSanXuat,
                    // dsvt_xuatXu: dsvt_xuatXu,
                    // dsvt_donViTinh: dsvt_donViTinh,
                    // dsvt_dateCreate: dsvt_dateCreate,
                    // dsvt_description: dsvt_description,
                    // file: file_data,
                    // id_cty: id_cty
                // },
                success: function(data) {
                    var text = $('.add_new_succ').text('');
                    var text_new = '';
                    text_new += 'Sửa vật tư thiết bị';
                    text_new += '<strong>';
                    text_new += '&nbsp' + dsvt_name;
                    text_new += '</strong>';
                    text_new += '&nbspthành công!';
                    text.append(text_new);
                    $('.add_new_unit').hide()
                    $('.add_new_materials_equipment_success').show()
                }
            });
            $('.btn_close').on("click", function() {
                window.location.href = "/danh-sach-vat-tu-thiet-bi-chi-tiet-" + dsvt_id + ".html";
            })
        }
    })
</script>

</html>