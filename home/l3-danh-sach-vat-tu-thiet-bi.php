<?php
include("config1.php");

if (!in_array(1, $ro_vattu)) {
    header("Location: /tong-quan.html");
}

isset($_GET['page']) ? $page = $_GET['page'] : $page = 1;
isset($_GET['nvt']) ? $nvt = $_GET['nvt'] : $nvt = "";
isset($_GET['hsx']) ? $hsx = $_GET['hsx'] : $hsx = "";
isset($_GET['xx']) ? $xx = $_GET['xx'] : $xx = "";
isset($_GET['sort']) ? $sort = $_GET['sort'] : $sort = "";
isset($_GET['input']) ? $ip = $_GET['input'] : $ip = "";
isset($_GET['dis']) ? $dis = $_GET['dis'] : $dis = 10;

if (isset($_COOKIE['user']) && $_COOKIE['user'] != "" && $_COOKIE['role'] == 1) {
    $token = $_COOKIE['acc_token'];
    $curl = curl_init();
    $data = array();
    curl_setopt($curl, CURLOPT_POST, 1);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
    curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/user_info_company.php');
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));

    $response = curl_exec($curl);
    curl_close($curl);
    $data_tt = json_decode($response, true);
    $tt_user = $data_tt['data']['user_info_result'];
    $id_nguoi_xoa = $_SESSION['com_id'];
}
if (isset($_COOKIE['user']) && $_COOKIE['user'] != "" && $_COOKIE['role'] == 2) {
    $token = $_COOKIE['acc_token'];
    $curl = curl_init();
    $data = array();
    curl_setopt($curl, CURLOPT_POST, 1);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
    curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/user_info_employee.php');
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));
    $response = curl_exec($curl);
    curl_close($curl);
    $data_tt = json_decode($response, true);
    $tt_user = $data_tt['data']['user_info_result'];
    $id_nguoi_xoa = $_SESSION['ep_id'];
}

$id_cty = $tt_user['com_id'];

$sql_nhom_vat_tu = new db_query("SELECT `nvt_id`, `nvt_name` FROM `nhom-vat-tu-thiet-bi` WHERE `nvt_check` = 1 AND `nvt_id_ct` = $id_cty");
$sql_hang_san_xuat = new db_query("SELECT `hsx_id`, `hsx_name` FROM `hang-san-xuat` WHERE `hsx_check` = 1 AND `hsx_id_ct` = $id_cty");
$sql_xuat_xu = new db_query("SELECT `xx_id`, `xx_name` FROM `xuat-xu`");


?>
<!DOCTYPE html>
<html lang="vi">

<head>
    <title>Danh sách vật tư thiết bị</title>
    <meta charset="UTF-8">
    <meta name="robots" content="noindex,nofollow" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="../css/select2.min.css?v=<?= $ver ?>">
    <link rel="stylesheet" type="text/css" href="../css/style.css?v=<?= $ver ?>">
    <link rel="stylesheet" type="text/css" href="../css/style_h.css?v=<?= $ver ?>">
    <link rel="stylesheet" type="text/css" href="../css/style_n.css?v=<?= $ver ?>">
</head>

<body>
    <div class="main_wrapper_all">
        <div class="wapper_all">
            <?php include('../includes/sidebar.php');  ?>
        </div>
        <div class="main_overview" id="main_overview">
            <div class="header_menu_overview d_flex align_c space_b">
                <p class="color_grey line_16 font_s14 text_link_page" style="display: block;">Thông tin vật tư thiết bị / Danh
                    sách vật tư thiết bị</p>
                <img class="open_sidebar_w" src="../images/open_sidebar_w.png" alt="" onclick="toggle('main_sidebar')">
                <?php include('../includes/header.php');  ?>
            </div>
            <div class="body_equipment_supplies">
                <p class="color_grey line_16 font_s14 text_link_page_re mb_15" style="display: none;">Thông tin vật tư
                    thiết bị / Danh sách vật tư thiết bị</p>
                <div class="d_flex align_c mb_20 flex_swap_select_equipment_supplies">
                    <div id="block04" class="d_flex align_c">
                        <div id="block01" class="d_flex align_c">
                            <div class="mr_15">
                                <select class="all_equipment_supplies" name="all_equipment_supplies">
                                    <option value="">Tất cả nhóm vật tư thiết bị</option>
                                    <?php while ($row_nvt = mysql_fetch_assoc($sql_nhom_vat_tu->result)) { ?>
                                        <option value="<?= $row_nvt['nvt_id']; ?>" <?= ($nvt == $row_nvt['nvt_id']) ? "selected" : "" ?>>
                                            <?= $row_nvt['nvt_name']; ?>
                                        </option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div id="block03" class="d_flex align_c">
                            <div class="mr_15">
                                <select class="all_manufacturer" name="all_manufacturer">
                                    <option value="">Tất cả hãng sản xuất</option>
                                    <?php while ($row_hsx = mysql_fetch_assoc($sql_hang_san_xuat->result)) { ?>
                                        <option value="<?= $row_hsx['hsx_id']; ?>" <?= ($hsx == $row_hsx['hsx_id']) ? "selected" : "" ?>><?= $row_hsx['hsx_name']; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="mr_15">
                                <select class="all_provenance" name="all_provenance">
                                    <option value="">Tất cả xuất xứ</option>
                                    <?php while ($row_xx = mysql_fetch_assoc($sql_xuat_xu->result)) { ?>
                                        <option value="<?= $row_xx['xx_id']; ?>" <?= ($xx == $row_xx['xx_id']) ? "selected" : "" ?>><?= $row_xx['xx_name']; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div id="block02" class="d_flex align_c space_b">
                        <select class="no_sort_price" name="no_sort_price">
                            <option value="">Không sắp xếp đơn giá</option>
                            <option value="1" <?= ($sort == 1) ? "selected" : "" ?>>Đơn giá tăng dần</option>
                            <option value="2" <?= ($sort == 2) ? "selected" : "" ?>>Đơn giá giảm dần</option>
                        </select>
                        <div class="d_flex align_c">
                            <img src="../images/img_hd.png" alt="">
                            <p class="padding_l5 color_blue line_18 font_s15 font_w500">Hướng dẫn</p>
                        </div>
                    </div>
                </div>
                <div class="d_flex align_c space_b mb_30" id="block07">
                    <div class="position_r" id="block05">
                        <input class="search_equipment_id d_flex align_c space_b" type="text" name="input_search" placeholder="Tìm kiếm theo mã, tên vật tư thiết bị" value="<?= ($ip != "") ? $ip : "" ?>">
                        <img class="position_a icon_search_equipment_id cursor_p" src="../images/icon-sr.png" alt="">
                    </div>
                    <div class="d_flex align_c" id="block06">
                        <?php if (in_array(2, $ro_vattu)) { ?>
                            <a href="/danh-sach-vat-tu-thiet-bi-them.html">
                                <button class="d_flex align_c button_add_new cursor_p">
                                    <img class="mr_10 img_button_add_new" src="../images/add_new.png" alt="">
                                    <p class="color_white line_18 font_s15 font_w500">Thêm mới</p>
                                </button>
                            </a>
                        <?php } ?>
                        <?php ?>
                        <button class="d_flex align_c button_export_excel ml_15 cursor_p">
                            <img class="mr_10 img_button_export_excel" src="../images/export_excel.png" alt="">
                            <p class="color_white line_18 font_s15 font_w500">Xuất excel</p>
                        </button>
                    </div>
                </div>
                <div class="table_danh_sach_vat_tu" data-page="<?= $page ?>" data-nvt="<?= $nvt ?>" data-hsx="<?= $hsx ?>" data-xx="<?= $xx ?>" data-sort="<?= $sort ?>" data-ip="<?= $ip ?>" data-dis ="<?= $dis ?>">

                </div>
            </div>
            <?php include('../includes/popup_overview.php');  ?>
            <?php include('../includes/popup_don-vi-tinh.php');  ?>
            <?php include('../includes/popup_h.php');  ?>
            <?php include('../includes/ghi_chu.php');  ?>
        </div>
</body>
<script type="text/javascript" src="../js/jquery-3.4.1.min.js"></script>
<script type="text/javascript" src="../js/jquery.validate.min.js"></script>
<script type="text/javascript" src="../js/select2.min.js"></script>
<script type="text/javascript" src="../js/js_h.js"></script>
<script type="text/javascript" src="../js/validate_h.js"></script>
<script type="text/javascript" src="../js/js_k.js"></script>
<script type="text/javascript" src="../js/js_n.js"></script>
<script>
    var page = $(".table_danh_sach_vat_tu").attr("data-page");
    var nvt = $(".table_danh_sach_vat_tu").attr("data-nvt");
    var hsx = $(".table_danh_sach_vat_tu").attr("data-hsx");
    var xx = $(".table_danh_sach_vat_tu").attr("data-xx");
    var sort = $(".table_danh_sach_vat_tu").attr("data-sort");
    var input_val = $(".table_danh_sach_vat_tu").attr("data-ip");
    var curr = $(".table_danh_sach_vat_tu").attr("data-dis");

    $.ajax({
        url: '../render/tb_danh_sach_vat_tu.php',
        type: 'POST',
        data: {
            page: page,
            nvt: nvt,
            hsx: hsx,
            xx: xx,
            sort: sort,
            input_val: input_val,
            curr: curr
        },
        success: function(data) {
            $(".table_danh_sach_vat_tu").append(data);
        }
    })

    $('.all_equipment_supplies, .all_manufacturer, .all_provenance, .no_sort_price').change(function() {
        var page = 1;
        var nvt = $("select[name='all_equipment_supplies']").val();
        var hsx = $("select[name='all_manufacturer']").val();
        var xx = $("select[name='all_provenance']").val();
        var sort = $("select[name='no_sort_price']").val();
        var input_val = $("input[name='input_search']").val();
        var curr = $('.show_tr_tb').val();

        if (nvt == "" && hsx == "" && xx == "" && sort == "" && input_val == "" && curr == 10) {
            window.location.href = "/danh-sach-vat-tu-thiet-bi.html?dis=" + curr + '&page=' + page;
        } else {
            window.location.href = "/danh-sach-vat-tu-thiet-bi.html?nvt=" + nvt + "&hsx=" + hsx + "&xx=" + xx + "&sort=" + sort + "&input=" + input_val + "&dis=" + curr + "&page=" + page;
        }
        $("input[name='input_search'").val('');
    });

    $(".icon_search_equipment_id").click(function() {
        var page = 1;
        var nvt = $("select[name='all_equipment_supplies']").val();
        var hsx = $("select[name='all_manufacturer']").val();
        var xx = $("select[name='all_provenance']").val();
        var sort = $("select[name='no_sort_price']").val();
        var input_val = $("input[name='input_search']").val();
        var curr = $('.show_tr_tb').val();

        if (nvt == "" && hsx == "" && xx == "" && sort == "" && input_val == "" && curr == 10) {
            window.location.href = "/danh-sach-vat-tu-thiet-bi.html?dis=" + curr + '&page=' + page;
        } else {
            window.location.href = "/danh-sach-vat-tu-thiet-bi.html?nvt=" + nvt + "&hsx=" + hsx + "&xx=" + xx + "&sort=" + sort + "&input=" + input_val + "&dis=" + curr + "&page=" + page;
        }
    });

    $(document).keyup(function(e) {
        if ($("input[name='input_search']").is(":focus") && (e.keyCode == 13)) {
            $(".icon_search_equipment_id").click();
        }
    });

    function id_xoa(id) {
        var id_vt = $(id).parent().parent().parent().attr('data-id');
        var id_cty = <?= $id_cty ?>;
        var id_nguoi_xoa = <?= $id_nguoi_xoa ?>;
        var name = $(id).parent().parent().parent().find('td:nth-child(3)').text();
        $('.delete_materials_equipment').show();
        $('.delete_materials_equipment strong').text(name);
        $('.delete_materials_equipment .button_accp').on("click", function(){
            $.ajax({
                url: '../ajax/delete_danh_sach_vat_tu.php',
                type: 'POST',
                data: {
                    id_vt: id_vt,
                    id_cty: id_cty,
                    id_nguoi_xoa: id_nguoi_xoa
                },
                success: function(data) {
                    if(data == ""){
                        $('.popup_delete_materials_equipment_success strong').text(name);
                    }else{
                        alert("Xóa thất bại!");
                    }
                }
            })
        });
        $('.button_close,.btn_close2').click(function(){
            $('.delete_materials_equipment .button_accp').off();
        });
    }

    $(".button_export_excel").click(function() {
        window.location.href = '../Excel/danh_sach_vat_tu.php';
    });

    function display(select){
    var curr = $(select).val();
    var page = 1;
    var nvt = $("select[name='all_equipment_supplies']").val();
    var hsx = $("select[name='all_manufacturer']").val();
    var xx = $("select[name='all_provenance']").val();
    var sort = $("select[name='no_sort_price']").val();
    var input_val = $("input[name='input_search']").val();
    var curr = $('.show_tr_tb').val();

    if (nvt == "" && hsx == "" && xx == "" && sort == "" && input_val == "" && curr == 10) {
        window.location.href = "/danh-sach-vat-tu-thiet-bi.html?dis=" + curr + '&page=' + page;
    } else {
        window.location.href = "/danh-sach-vat-tu-thiet-bi.html?nvt=" + nvt + "&hsx=" + hsx + "&xx=" + xx + "&sort=" + sort + "&input=" + input_val + "&dis=" + curr + "&page=" + page;
    }
}

</script>

</html>