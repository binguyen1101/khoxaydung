<?php include("config.php"); ?>
<!DOCTYPE html>
<html lang="vi">

<head>
    <title>Nhóm vật tư thiết bị</title>
    <meta charset="UTF-8">
    <meta name="robots" content="noindex,nofollow" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="../css/select2.min.css?v=<?= $ver ?>">
    <link rel="stylesheet" type="text/css" href="../css/style.css?v=<?= $ver ?>">
    <link rel="stylesheet" type="text/css" href="../css/style_h.css?v=<?= $ver ?>">
    <link rel="stylesheet" type="text/css" href="../css/style_n.css?v=<?= $ver ?>">
</head>

<body class="seclec2_radius">
    <div class="box_right gr_dv gr_dv_add">
        <?php include('../includes/sidebar.php');  ?>
        <div class="box_right_ct">
            <div class="block_gr_dv">
                <div class="block_change block_gr_dv">
                    <form action="" method="post" id="f_gr_dv_add">
                        <div class="head_wh d_flex space_b align_c">
                            <div class="head_tab d_flex space_b align_c">
                                <div class="icon_header open_sidebar_w">
                                    <span class="icon_header_tbl"></span>
                                    <span class="icon_header_tbl"></span>
                                    <span class="icon_header_tbl"></span>
                                </div>
                                <?php include("../includes/header.php") ; ?>
                            </div>
                            <p class="color_grey line_16 font_s14">
                                <a href="/nhom-vat-tu-thiet-bi-detail.html" class="cursor_p">
                                    <img src="../images/back_item_g.png" alt="">
                                </a>
                                &nbsp Thông tin vật tư thiết bị / Nhóm vật tư thiết bị / Nhóm vật tư 1 / Thêm vật tư
                            </p>
                            <?php include('../includes/header.php');  ?>
                        </div>
                        <div class="add_ex_wh add_gr_dv" style="display: block;">
                            <div class="tit_info_rq back_blue">
                                <p class="color_white font_s16 line_h19 font_w700">Thêm mới vật tư thiết bị</p>
                            </div>
                            <div class="ct_add_ex_wh">
                                <div class="box_add_ex_wh d_flex space_b">
                                    <div class="l_add_ex">
                                        <div class="d_flex flex_column mb_15">
                                            <p class="color_grey font_s15 line_h18 font_w500">Mã vật tư thiết bị</p>
                                            <input class="color_grey3 font_s14 line_h17 font_w400" type="text"
                                                value="VT-0000" disabled="disabled">
                                        </div>
                                        <div class="name_full_dv d_flex flex_column mb_15">
                                            <p class="color_grey font_s15 line_h18 font_w500">Tên đầy đủ thiết bị vật tư
                                                <span style="color: red;">*</span>
                                            </p>
                                            <input class="color_grey3 font_s14 line_h17 font_w400" type="text"
                                                placeholder="Nhập tên vật tư thiết bị" name="name_full_dv">
                                        </div>
                                        <div class="d_flex flex_column mb_15">
                                            <p class="color_grey font_s15 line_h18 font_w500">Nhóm vật tư thiết bị</p>
                                            <input class="color_grey3 font_s14 line_h17 font_w400" type="text"
                                                value="Nhóm vật tư 1" disabled="disabled">
                                        </div>
                                        <div class="box_add_unit d_flex flex_column mb_15">
                                            <div class="d_flex space_b">
                                                <p class="color_grey font_s15 line_h18 font_w500">Đơn vị tính<span
                                                        style="color: red;">*</span>
                                                </p>
                                                <p
                                                    class="add_unit color_blue font_s15 line_h18 font_w400 d_flex cursor_p">
                                                    <span
                                                        class="icon_add_unit font_s16 d_flex align_c flex_center"><span>+</span></span>&nbsp;Thêm
                                                    đơn vị tính
                                                </p>
                                            </div>
                                            <select name="select_add_unit" id=""
                                                class="select_add_unit color_grey3 font_s14 line_h17 font_w400"
                                                style="width: 100%;">
                                                <option value=""></option>
                                                <option value="1">mi li mét</option>
                                                <option value="2">xen ti mét</option>
                                                <option value="3">đề xi mét</option>
                                                <option value="4">mét</option>
                                                <option value="5">ki lô mét</option>
                                                <option value="6">dặm</option>
                                            </select>
                                        </div>
                                        <div class="d_flex flex_column mb_15 position_r">
                                            <p class="color_grey font_s15 line_h18 font_w500">Đơn giá</p>
                                            <input class="color_grey3 font_s14 line_h17 font_w400" type="text"
                                                placeholder="Nhập đơn giá">
                                            <span class="color_grey3 font_s14 line_h17 font_w400 position_a"
                                                style="right: 15px; bottom: 10px;">VNĐ</span>
                                        </div>
                                        <div class="d_flex flex_column mb_15">
                                            <p class="color_grey font_s15 line_h18 font_w500">Hãng sản xuất</p>
                                            <select name="" id=""
                                                class="select_brand color_grey3 font_s14 line_h17 font_w400"
                                                style="width: 100%;">
                                                <option value=""></option>
                                                <option value="1">Tân Á</option>
                                                <option value="2">Tân Á</option>
                                                <option value="3">Tân Á</option>
                                                <option value="4">Tân Á</option>
                                                <option value="5">Tân Á</option>
                                                <option value="6">Tân Á</option>
                                            </select>
                                        </div>
                                        <div class="d_flex flex_column mb_15">
                                            <p class="color_grey font_s15 line_h18 font_w500">Xuất xứ</p>
                                            <select name="" id=""
                                                class="select_origin color_grey3 font_s14 line_h17 font_w400"
                                                style="width: 100%;">
                                                <option value=""></option>
                                                <option value="2">Việt Nam</option>
                                                <option value="3">Việt Nam</option>
                                                <option value="4">Việt Nam</option>
                                                <option value="5">Việt Nam</option>
                                                <option value="6">Việt Nam</option>
                                            </select>
                                        </div>
                                        <div class="d_flex flex_column mb_15">
                                            <p class="color_grey font_s15 line_h18 font_w500">Người tạo</p>
                                            <input class="color_grey3 font_s14 line_h17 font_w400" type="text"
                                                value="Nguyễn Văn Nam" disabled="disabled">
                                        </div>
                                        <div class="d_flex flex_column mb_15">
                                            <p class="color_grey font_s15 line_h18 font_w500">Ngày tạo</p>
                                            <input class="color_grey3 font_s14 line_h17 font_w400" type="date"
                                                disabled="disabled">
                                        </div>
                                        <div class="d_flex flex_column display_none">
                                            <p class="color_grey font_s15 line_h18 font_w500">Mô tả vật tư thiết bị</p>
                                            <div id="editor_mobile" style="height: 258px">
                                                <p>Nhập nội dung</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="r_add_ex">
                                        <div class="d_flex flex_column">
                                            <div id="r_add_img" class="upload_equipment_supplies">
                                                <p class=" font_s15 line_h18 font_w500 color_grey">Hình ảnh thiết bị
                                                    vật tư</p>
                                                <label for="input_file_chat"
                                                    class="input_file_img1 input_file_img position_r">
                                                    <div class="upload_file_img d_flex align_c flex_center">
                                                        <picture>
                                                            <img class="d_flex align_c margin_a"
                                                                src="../images/camera_b.png" alt="">
                                                            <p class="font_s14 line_h16 color_blue text_a_c">Tải lên
                                                                hình
                                                                ảnh</p>
                                                            <input type="file" id="input_file_chat"
                                                                class="display_none">
                                                        </picture>
                                                    </div>
                                                </label>
                                                <div class="upload_logo_vehicle_done position_r display_none">
                                                    <img class="ready_upload_logo" src="../images/add_logo.svg" alt="">
                                                    <label for="upload_logo">
                                                        <img class="add_logo position_a" src="../images/add_photo.png"
                                                            alt="">
                                                        <input type="file" id="upload_logo" class=" display_none"
                                                            accept=".png, .jpg, .jpeg">
                                                    </label>
                                                    <img class="del_logo position_a" src="../images/close_b.png" alt="">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="d_flex flex_column">
                                            <p class="color_grey font_s15 line_h18 font_w500">Mô tả vật tư thiết bị</p>
                                            <div id="editor" style="height: 258px">
                                                <p>Nhập nội dung</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="btn_cf_rq d_flex flex_center" style="display: flex;">
                            <button type="button"
                                class="btn_cancel back_w color_blue font_s15 line_h18 font_w500 cursor_p">
                                <a href="/nhom-vat-tu-thiet-bi-detail.html"></a>
                                Hủy
                            </button>
                            <button type="submit"
                                class="btn_save back_blue color_white font_s15 line_h18 font_w500 cursor_p">Lưu</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    <?php include('../includes/popup_overview.php');  ?>
        <?php include('../includes/popup_nhom-vat-tu-thiet-bi.php') ?>
</body>
<script type="text/javascript" src="../js/jquery-3.4.1.min.js"></script>
<script type="text/javascript" src="../js/jquery.validate.min.js"></script>
<script type="text/javascript" src="../js/select2.min.js"></script>
<script type="text/javascript" src="../js/js_h.js"></script>
<script type="text/javascript" src="../js/validate_h.js"></script>
<script type="text/javascript" src="../js/js_k.js"></script>
<script type="text/javascript" src="../js/js_n.js"></script>
<script src="//cdn.ckeditor.com/4.17.1/standard/ckeditor.js"></script>
<script>
CKEDITOR.replace('editor');
if ($(window).width() <= 768) {
    $('#editor').parent().hide();
}
CKEDITOR.replace('editor_mobile');
</script>
<script>
    $('.active4').each(function () {
        if ($(this).hasClass('active4')) {
            $(this).parent().addClass('show');
            $(this).parent().parent().find('.item_sidebar_cha').addClass('active');
            $(this).find('a').addClass('active');
        }
    });
</script>

</html>