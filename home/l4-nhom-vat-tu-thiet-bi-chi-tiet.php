<?php

include("config1.php");
if(!in_array(1,$ro_nhom_vt)){
    header("Location: /tong-quan.html");
}

$id = getValue('id','int','GET','');
isset($_GET['page']) ? $page = $_GET['page'] : $page = 1;
isset($_GET['hsx']) ? $hsx = $_GET['hsx'] : $hsx = "";
isset($_GET['xx']) ? $xx = $_GET['xx'] : $xx = "";
isset($_GET['sort']) ? $sort = $_GET['sort'] : $sort = "";
isset($_GET['input']) ? $ip = $_GET['input'] : $ip = "";

if (isset($_COOKIE['user']) && $_COOKIE['user'] != "" && $_COOKIE['role'] == 2) {
    $token = $_COOKIE['acc_token'];
    $curl = curl_init();
    $data = array();
    curl_setopt($curl, CURLOPT_POST, 1);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
    curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/user_info_employee.php');
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));
    $response = curl_exec($curl);
    curl_close($curl);
    $data_tt = json_decode($response, true);
    $tt_user = $data_tt['data']['user_info_result'];

}else if (isset($_COOKIE['user']) && $_COOKIE['user'] != "" && $_COOKIE['role'] == 1) {
    $token = $_COOKIE['acc_token'];
    $curl = curl_init();
    $data = array();
    curl_setopt($curl, CURLOPT_POST, 1);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
    curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/user_info_company.php');
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));
    $response = curl_exec($curl);
    curl_close($curl);
    $data_tt = json_decode($response, true);
    $tt_user = $data_tt['data']['user_info_result'];
}
    
$id_cty = $tt_user['com_id'];

$sql_ten_nhom = new db_query("SELECT `nvt_id`, `nvt_name` FROM `nhom-vat-tu-thiet-bi` WHERE `nvt_id` = $id AND `nvt_check` = 1 AND `nvt_id_ct` = $id_cty");
$ten_nhom_vat_tu = mysql_fetch_assoc($sql_ten_nhom->result)['nvt_name'];

$sql_hang_san_xuat = new db_query("SELECT `hsx_id`, `hsx_name` FROM `hang-san-xuat` WHERE `hsx_check` = 1 AND `hsx_id_ct` = $id_cty");
$sql_xuat_xu = new db_query("SELECT `xx_id`, `xx_name` FROM `xuat-xu` WHERE 1");
?>

<!DOCTYPE html>
<html lang="vi">

<head>
<title>Nhóm vật tư thiết bị</title>
<meta charset="UTF-8">
<meta name="robots" content="noindex,nofollow" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" type="text/css" href="../css/select2.min.css?v=<?= $ver ?>">
<link rel="stylesheet" type="text/css" href="../css/style.css?v=<?= $ver ?>">
<link rel="stylesheet" type="text/css" href="../css/style_h.css?v=<?= $ver ?>">
<link rel="stylesheet" type="text/css" href="../css/style_n.css?v=<?= $ver ?>">
</head>

<body>
<div class="box_right gr_dv_detail">
    <?php include('../includes/sidebar.php');  ?>
    <div class="box_right_ct">
    <div class="block_change block_gr_dv">
        <div class="head_wh d_flex space_b align_c">
        <div class="head_tab d_flex space_b align_c">
            <div class="icon_header open_sidebar_w">
            <span class="icon_header_tbl"></span>
            <span class="icon_header_tbl"></span>
            <span class="icon_header_tbl"></span>
            </div>
            <?php include("../includes/header.php") ; ?>
        </div>
        <p class="color_grey line_16 font_s14">
            <a href="/nhom-vat-tu-thiet-bi.html" class="cursor_p">
            <img src="../images/back_item_g.png" alt="">
            </a>
            &nbsp Thông tin vật tư thiết bị / Nhóm vật tư thiết bị / <?=$ten_nhom_vat_tu?>
        </p>
        <?php include('../includes/header.php');  ?>
        </div>
        <div class="sort_gr_dv">
        <div class="list_sort d_flex flex_start flex_w">
            <div class="item_sort">
            <select class="select_sort_invt0 color_grey font_s14 line_h17 font_w400" name="hang_san_xuat" id=""
                style="width: 100%;">
                <option value="">Tất cả hãng sản xuất</option>
                <?php while($row_hsx = mysql_fetch_assoc($sql_hang_san_xuat->result)){?>
                    <option value="<?=$row_hsx['hsx_id'];?>" <?= ($hsx == $row_hsx['hsx_id']) ? "selected" : "" ?>><?=$row_hsx['hsx_name'];?></option>
                <?php } ?>
            </select>
            </div>
            
            <div class="item_sort">
            <select class="select_sort_invt1 color_grey font_s14 line_h17 font_w400" name="xuat_xu" id=""
                style="width: 100%;">
                <option value="">Tất cả xuất xứ</option>
                <?php while($row_xx = mysql_fetch_assoc($sql_xuat_xu->result)){?>
                    <option value="<?=$row_xx['xx_id'];?>" <?= ($xx == $row_xx['xx_id']) ? "selected" : "" ?>><?=$row_xx['xx_name'];?></option>
                <?php } ?>
            </select>
            </div>
            <div class="item_sort">
            <select class="select_sort_invt2 color_grey font_s14 line_h17 font_w400" name="sap_xep" id=""
                style="width: 100%;">
                <option value="">Không sắp xếp đơn giá</option>
                <option value="1" <?= ($sort == 1) ? "selected" : "" ?>>Đơn giá tăng dần</option>
                <option value="2" <?= ($sort == 2) ? "selected" : "" ?>>Đơn giá giảm dần</option>
            </select>
            </div>
        </div>
        </div>
        <div class="operation_wh d_flex space_b">
            <div class="search_wh d_flex space_b" style="display: block;">
                <div class="input_sr_wh">
                <div class="box_input_sr position_r">
                    <input type="text" name="input_search" placeholder="Tìm kiếm theo mã, tên vật tư thiết bị" value="<?= ($ip != "") ? $ip : "" ?>">
                    <span class="icon_sr_wh"></span>
                </div>
                </div>
            </div>
            <?php if(in_array(2,$ro_nhom_vt)){?>
            <div class="export_wh d_flex space_b align_c">
                <button class="btn_px d_flex align_c">
                <a href="/nhom-vat-tu-thiet-bi-them-moi-vat-tu-<?= $id?>.html">
                </a>
                <img src="../images/img_px.png" alt="">
                <p class="color_white font_s15 line_h18 font_w500">Thêm mới</p>
                </button>
            </div>
            <?php }?>
        </div>
        <!-- Nhóm vật tư thiết bị -->
        <div class="detail_wh" data-page="<?= $page ?>" data-hsx="<?= $hsx ?>" data-xx="<?= $xx ?>" data-sort="<?= $sort ?>" data-ip="<?= $ip ?>">
        
        </div>
    </div>
    </div>
</div>
    <?php include('../includes/popup_overview.php');  ?>
    <?php include('../includes/popup_h.php');  ?>
</body>

<script type="text/javascript" src="../js/jquery-3.4.1.min.js"></script>
<script type="text/javascript" src="../js/jquery.validate.min.js"></script>
<script type="text/javascript" src="../js/select2.min.js"></script>
<script type="text/javascript" src="../js/js_h.js"></script>
<script type="text/javascript" src="../js/validate_h.js"></script>
<script type="text/javascript" src="../js/js_k.js"></script>
<script type="text/javascript" src="../js/js_n.js"></script>
<script>
$('.active4').each(function () {
    if ($(this).hasClass('active4')) {
    $(this).parent().addClass('show');
    $(this).parent().parent().find('.item_sidebar_cha').addClass('active');
    $(this).find('a').addClass('active');
    }
});

var page = 1;
var hsx = $(".detail_wh").attr("data-hsx");
var xx = $(".detail_wh").attr("data-xx");
var sort = $(".detail_wh").attr("data-sort");
var input_val = $(".detail_wh").attr("data-ip");
var id_nhom = <?= json_encode($id); ?>;

$.ajax({
    url: "../render/tb_nhom_vat_tu_chi_tiet.php",
    type: "POST",
    data:{
        id_nhom: id_nhom,
        page: page,
        hsx: hsx,
        xx: xx,
        sort: sort,
        input_val: input_val,
    },
    success: function(data){
        $(".detail_wh").append(data);
    }
});

$('.select_sort_invt0, .select_sort_invt1, .select_sort_invt2').change(function(){
    var hsx = $("select[name='hang_san_xuat']").val();
    var xx = $("select[name='xuat_xu']").val();
    var sort = $("select[name='sap_xep']").val();
    var input_val = $("input[name='input_search']").val();

    if(hsx == "" && xx == "" && sort == "" && input_val == ""){
            window.location.href = "/nhom-vat-tu-thiet-bi-chi-tiet-" + id_nhom + ".html"
    }else{
            window.location.href = "/nhom-vat-tu-thiet-bi-chi-tiet-" + id_nhom + ".html?hsx=" + hsx + "&xx=" + xx + "&sort=" + sort + "&input=" + input_val  + "&page=" + page;
    }
    $("input[name='input_search'").val('');
});



$(".icon_sr_wh").click(function(){
    var hsx = $("select[name='hang_san_xuat']").val();
    var xx = $("select[name='xuat_xu']").val();
    var sort = $("select[name='sap_xep']").val();
    var input_val = $("input[name='input_search']").val();
    if(hsx == "" && xx == "" && sort == "" && input_val == ""){
        window.location.href = "/nhom-vat-tu-thiet-bi-chi-tiet-" + id_nhom + ".html"
    }else{
        window.location.href = "/nhom-vat-tu-thiet-bi-chi-tiet-" + id_nhom + ".html?hsx=" + hsx + "&xx=" + xx + "&sort=" + sort + "&input=" + input_val + "&page=" + page;
    }
});

$(document).keyup(function (e) {
    if ($("input[name='input_search']").is(":focus") && (e.keyCode == 13)) {
        var hsx = $("select[name='hang_san_xuat']").val();
        var xx = $("select[name='xuat_xu']").val();
        var sort = $("select[name='sap_xep']").val();
        var input_val = $("input[name='input_search']").val();
        if(hsx == "" && xx == "" && sort == "" && input_val == ""){
            window.location.href = "/nhom-vat-tu-thiet-bi-chi-tiet-" + id_nhom + ".html"
        }else{
            window.location.href = "/nhom-vat-tu-thiet-bi-chi-tiet-" + id_nhom + ".html?hsx=" + hsx + "&xx=" + xx + "&sort=" + sort + "&input=" + input_val + "&page=" + page;
        }
    }
});

</script>
</html>