<?php include("config.php"); ?>
<!DOCTYPE html>
<html lang="vi">

<head>
  <title>Nhóm vật tư thiết bị</title>
  <meta charset="UTF-8">
  <meta name="robots" content="noindex,nofollow" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" type="text/css" href="../css/select2.min.css?v=<?= $ver ?>">
  <link rel="stylesheet" type="text/css" href="../css/style.css?v=<?= $ver ?>">
  <link rel="stylesheet" type="text/css" href="../css/style_h.css?v=<?= $ver ?>">
  <link rel="stylesheet" type="text/css" href="../css/style_n.css?v=<?= $ver ?>">
</head>

<body>
  <div class="box_right gr_dv_detail">
    <?php include('../includes/sidebar.php');  ?>
    <div class="box_right_ct">
      <div class="block_change block_gr_dv">
        <div class="head_wh d_flex space_b align_c">
          <div class="head_tab d_flex space_b align_c">
            <div class="icon_header open_sidebar_w">
              <span class="icon_header_tbl"></span>
              <span class="icon_header_tbl"></span>
              <span class="icon_header_tbl"></span>
            </div>
            <?php include("../includes/header.php") ; ?>
          </div>
          <p class="color_grey line_16 font_s14">
            <a href="/nhom-vat-tu-thiet-bi.html" class="cursor_p">
              <img src="../images/back_item_g.png" alt="">
            </a>
            &nbsp Thông tin vật tư thiết bị / Nhóm vật tư thiết bị / Nhóm vật tư 1
          </p>
          <?php include('../includes/header.php');  ?>
        </div>
        <div class="sort_gr_dv">
          <div class="list_sort d_flex flex_start flex_w">
            <div class="item_sort">
              <select class="select_sort_invt color_grey font_s14 line_h17 font_w400" name="" id=""
                style="width: 100%;">
                <option value="">Tất cả hãng sản xuất</option>
                <option value="">Tân Á</option>
                <option value="">Hòa Phát</option>
                <option value="">Hòa Phát</option>
                <option value="">Hòa Phát</option>
                <option value="">Hòa Phát</option>
                <option value="">Hòa Phát</option>
              </select>
            </div>
            <div class="item_sort">
              <select class="select_sort_invt color_grey font_s14 line_h17 font_w400" name="" id=""
                style="width: 100%;">
                <option value="">Tất cả xuất xứ</option>
                <option value="">Việt Nam</option>
                <option value="">Trung Quốc</option>
                <option value="">Trung Quốc</option>
                <option value="">Trung Quốc</option>
                <option value="">Trung Quốc</option>
                <option value="">Trung Quốc</option>
              </select>
            </div>
            <div class="item_sort">
              <select class="select_sort_invt color_grey font_s14 line_h17 font_w400" name="" id=""
                style="width: 100%;">
                <option value="">Không sắp xếp đơn giá</option>
                <option value="">Đơn giá tăng dần</option>
                <option value="">Đơn giá giảm dần</option>
              </select>
            </div>
          </div>
        </div>
        <div class="operation_wh d_flex space_b">
          <div class="search_wh d_flex space_b" style="display: block;">
            <div class="input_sr_wh">
              <div class="box_input_sr position_r">
                <input type="text" placeholder="Tìm kiếm theo mã, tên vật tư thiết bị">
                <span class="icon_sr_wh"></span>
              </div>
            </div>
          </div>
          <div class="export_wh d_flex space_b align_c">
            <button class="btn_px d_flex align_c">
              <a href="/nhom-vat-tu-thiet-bi-add.html">
              </a>
              <img src="../images/img_px.png" alt="">
              <p class="color_white font_s15 line_h18 font_w500">Thêm mới</p>
            </button>
          </div>
        </div>
        <!-- Nhóm vật tư thiết bị -->
        <div class="tb_operation_wh position_r d_flex align_c">
          <div class="table_vt_scr">
            <div class="table_ds_vt">
              <table style="width: 1711px;">
                <tr class="tit_tbl color_white font_s16 line_h19 font_w500 back_blue">
                  <th>STT<span class="span_tbody"></span></th>
                  <th>Mã vật tư thiết bị<span class="span_tbody"></span></th>
                  <th>Tên đầy đủ vật tư thiết bị<span class="span_tbody"></span></th>
                  <th>Đơn vị tính<span class="span_tbody"></span></th>
                  <th>Hãng sản xuất<span class="span_tbody"></span></th>
                  <th>Xuất xứ<span class="span_tbody"></span></th>
                  <th>Mô tả<span class="span_tbody"></span></th>
                  <th>Đơn giá (VNĐ)<span class="span_tbody"></span></th>
                  <th style="width: 136px;">Chức năng</th>
                </tr>
                <tr class="color_grey font_s14 line_h17 font_w400">
                  <td>1</td>
                  <td>VT-0000</td>
                  <td style="text-align: left;"><a href="/danh-sach-vat-tu-thiet-bi-detail.html" class="color_blue font_w500">Ống nhựa</a></td>
                  <td>Chai</td>
                  <td style="text-align: left;">Việt Hà</td>
                  <td>Việt Nam</td>
                  <td style="text-align: left;">Mô tả 1</td>
                  <td style="text-align: right;">10.000.000</td>
                  <td>
                    <a class="color_blue font_s14 line_h17 font_w500" href="#">
                      <img src="../images/edit_tb.png" alt="">Sửa
                    </a>
                    <span style="border-left: 1px solid #4C5BD4; margin: auto 5px;"></span>
                    <a class="color_red font_s14 line_h17 font_w500" href="#">
                      <img src="../images/del_tb.png" alt="">Xóa
                    </a>
                  </td>
                </tr>
                <tr class="color_grey font_s14 line_h17 font_w400">
                  <td>1</td>
                  <td>VT-0000</td>
                  <td style="text-align: left;"><a href="#" class="color_blue font_w500">Ống nhựa</a></td>
                  <td>Chai</td>
                  <td style="text-align: left;">Việt Hà</td>
                  <td>Việt Nam</td>
                  <td style="text-align: left;">Mô tả 1</td>
                  <td style="text-align: right;">1.000.000</td>
                  <td>
                    <a class="color_blue font_s14 line_h17 font_w500" href="#">
                      <img src="../images/edit_tb.png" alt="">Sửa
                    </a>
                    <span style="border-left: 1px solid #4C5BD4; margin: auto 5px;"></span>
                    <a class="color_red font_s14 line_h17 font_w500" href="#">
                      <img src="../images/del_tb.png" alt="">Xóa
                    </a>
                  </td>
                </tr>
                <tr class="color_grey font_s14 line_h17 font_w400">
                  <td>1</td>
                  <td>VT-0000</td>
                  <td style="text-align: left;"><a href="#" class="color_blue font_w500">Ống nhựa</a></td>
                  <td>Chai</td>
                  <td style="text-align: left;">Việt Hà</td>
                  <td>Việt Nam</td>
                  <td style="text-align: left;">Mô tả 1</td>
                  <td style="text-align: right;">100.000</td>
                  <td>
                    <a class="color_blue font_s14 line_h17 font_w500" href="#">
                      <img src="../images/edit_tb.png" alt="">Sửa
                    </a>
                    <span style="border-left: 1px solid #4C5BD4; margin: auto 5px;"></span>
                    <a class="color_red font_s14 line_h17 font_w500" href="#">
                      <img src="../images/del_tb.png" alt="">Xóa
                    </a>
                  </td>
                </tr>
                <tr class="color_grey font_s14 line_h17 font_w400">
                  <td>1</td>
                  <td>VT-0000</td>
                  <td style="text-align: left;"><a href="#" class="color_blue font_w500">Ống nhựa</a></td>
                  <td>Chai</td>
                  <td style="text-align: left;">Việt Hà</td>
                  <td>Việt Nam</td>
                  <td style="text-align: left;">Mô tả 1</td>
                  <td style="text-align: right;">10.000</td>
                  <td>
                    <a class="color_blue font_s14 line_h17 font_w500" href="#">
                      <img src="../images/edit_tb.png" alt="">Sửa
                    </a>
                    <span style="border-left: 1px solid #4C5BD4; margin: auto 5px;"></span>
                    <a class="color_red font_s14 line_h17 font_w500" href="#">
                      <img src="../images/del_tb.png" alt="">Xóa
                    </a>
                  </td>
                </tr>
                <tr class="color_grey font_s14 line_h17 font_w400">
                  <td>1</td>
                  <td>VT-0000</td>
                  <td style="text-align: left;"><a href="#" class="color_blue font_w500">Ống nhựa</a></td>
                  <td>Chai</td>
                  <td style="text-align: left;">Việt Hà</td>
                  <td>Việt Nam</td>
                  <td style="text-align: left;">Mô tả 1</td>
                  <td style="text-align: right;">100.000.000</td>
                  <td>
                    <a class="color_blue font_s14 line_h17 font_w500" href="#">
                      <img src="../images/edit_tb.png" alt="">Sửa
                    </a>
                    <span style="border-left: 1px solid #4C5BD4; margin: auto 5px;"></span>
                    <a class="color_red font_s14 line_h17 font_w500" href="#">
                      <img src="../images/del_tb.png" alt="">Xóa
                    </a>
                  </td>
                </tr>
                <tr class="color_grey font_s14 line_h17 font_w400">
                  <td>1</td>
                  <td>VT-0000</td>
                  <td style="text-align: left;"><a href="#" class="color_blue font_w500">Ống nhựa</a></td>
                  <td>Chai</td>
                  <td style="text-align: left;">Việt Hà</td>
                  <td>Việt Nam</td>
                  <td style="text-align: left;">Mô tả 1</td>
                  <td style="text-align: right;">1.000.000.000</td>
                  <td>
                    <a class="color_blue font_s14 line_h17 font_w500" href="#">
                      <img src="../images/edit_tb.png" alt="">Sửa
                    </a>
                    <span style="border-left: 1px solid #4C5BD4; margin: auto 5px;"></span>
                    <a class="color_red font_s14 line_h17 font_w500" href="#">
                      <img src="../images/del_tb.png" alt="">Xóa
                    </a>
                  </td>
                </tr>
                <tr class="color_grey font_s14 line_h17 font_w400">
                  <td>1</td>
                  <td>VT-0000</td>
                  <td style="text-align: left;"><a href="#" class="color_blue font_w500">Ống nhựa</a></td>
                  <td>Chai</td>
                  <td style="text-align: left;">Việt Hà</td>
                  <td>Việt Nam</td>
                  <td style="text-align: left;">Mô tả 1</td>
                  <td style="text-align: right;">10.000.000</td>
                  <td>
                    <a class="color_blue font_s14 line_h17 font_w500" href="#">
                      <img src="../images/edit_tb.png" alt="">Sửa
                    </a>
                    <span style="border-left: 1px solid #4C5BD4; margin: auto 5px;"></span>
                    <a class="color_red font_s14 line_h17 font_w500" href="#">
                      <img src="../images/del_tb.png" alt="">Xóa
                    </a>
                  </td>
                </tr>
                <tr class="color_grey font_s14 line_h17 font_w400">
                  <td>1</td>
                  <td>VT-0000</td>
                  <td style="text-align: left;"><a href="#" class="color_blue font_w500">Ống nhựa</a></td>
                  <td>Chai</td>
                  <td style="text-align: left;">Việt Hà</td>
                  <td>Việt Nam</td>
                  <td style="text-align: left;">Mô tả 1</td>
                  <td style="text-align: right;">10.000.000</td>
                  <td>
                    <a class="color_blue font_s14 line_h17 font_w500" href="#">
                      <img src="../images/edit_tb.png" alt="">Sửa
                    </a>
                    <span style="border-left: 1px solid #4C5BD4; margin: auto 5px;"></span>
                    <a class="color_red font_s14 line_h17 font_w500" href="#">
                      <img src="../images/del_tb.png" alt="">Xóa
                    </a>
                  </td>
                </tr>
                <tr class="color_grey font_s14 line_h17 font_w400">
                  <td>1</td>
                  <td>VT-0000</td>
                  <td style="text-align: left;"><a href="#" class="color_blue font_w500">Ống nhựa</a></td>
                  <td>Chai</td>
                  <td style="text-align: left;">Việt Hà</td>
                  <td>Việt Nam</td>
                  <td style="text-align: left;">Mô tả 1</td>
                  <td style="text-align: right;">10.000.000</td>
                  <td>
                    <a class="color_blue font_s14 line_h17 font_w500" href="#">
                      <img src="../images/edit_tb.png" alt="">Sửa
                    </a>
                    <span style="border-left: 1px solid #4C5BD4; margin: auto 5px;"></span>
                    <a class="color_red font_s14 line_h17 font_w500" href="#">
                      <img src="../images/del_tb.png" alt="">Xóa
                    </a>
                  </td>
                </tr>
                <tr class="color_grey font_s14 line_h17 font_w400">
                  <td>1</td>
                  <td>VT-0000</td>
                  <td style="text-align: left;"><a href="#" class="color_blue font_w500">Ống nhựa</a></td>
                  <td>Chai</td>
                  <td style="text-align: left;">Việt Hà</td>
                  <td>Việt Nam</td>
                  <td style="text-align: left;">Mô tả 1</td>
                  <td style="text-align: right;">10.000.000</td>
                  <td>
                    <a class="color_blue font_s14 line_h17 font_w500" href="#">
                      <img src="../images/edit_tb.png" alt="">Sửa
                    </a>
                    <span style="border-left: 1px solid #4C5BD4; margin: auto 5px;"></span>
                    <a class="color_red font_s14 line_h17 font_w500" href="#">
                      <img src="../images/del_tb.png" alt="">Xóa
                    </a>
                  </td>
                </tr>
              </table>
            </div>
          </div>
          <div class="pre_q d_flex align_c flex_center position_a">
            <span class="pre_arrow"></span>
          </div>
          <div class="next_q d_flex align_c flex_center position_a">
            <span class="next_arrow"></span>
          </div>
        </div>
        <div class="w_navigation d_flex space_b align_c">
          <div class="l_nav d_flex align_c">
            <p class="color_grey font_s14 line_h17 font_w400">Hiển thị:</p>
            <select name="" id="" class="show_tr_tb color_grey font_s14 line_h17 font_w400">
              <option value="">10</option>
              <option value="">20</option>
              <option value="">30</option>
              <option value="">40</option>
            </select>
          </div>
          <div class="r_nav">
            <ul class="d_flex font_s13 line_h15 font_wN">
              <li><a href="#" class="pre_paginition_detail">
                  <</a>
              </li>
              <li><a href="#" class="r_paginition_detail paging_detail">1</a></li>
              <li><a href="#" class="r_paginition_detail">2</a></li>
              <li><a href="#" class="r_paginition_detail">3</a></li>
              <li><a href="#" class="r_paginition_detail">4</a></li>
              <li><a href="#" class="r_paginition_detail">5</a></li>
              <li><a href="#" class="next_paginition_detail">></a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
    <?php include('../includes/popup_overview.php');  ?>
</body>

<script type="text/javascript" src="../js/jquery-3.4.1.min.js"></script>
<script type="text/javascript" src="../js/jquery.validate.min.js"></script>
<script type="text/javascript" src="../js/select2.min.js"></script>
<script type="text/javascript" src="../js/js_h.js"></script>
<script type="text/javascript" src="../js/validate_h.js"></script>
<script type="text/javascript" src="../js/js_k.js"></script>
<script type="text/javascript" src="../js/js_n.js"></script>
<script>
  $('.active4').each(function () {
    if ($(this).hasClass('active4')) {
      $(this).parent().addClass('show');
      $(this).parent().parent().find('.item_sidebar_cha').addClass('active');
      $(this).find('a').addClass('active');
    }
  });
</script>

</html>