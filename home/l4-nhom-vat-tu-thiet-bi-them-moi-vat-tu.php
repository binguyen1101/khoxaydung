<?php
include("config1.php");

if (!in_array(2, $ro_nhom_vt)) {
    header("Location: /nhom-vat-tu-thiet-bi.html");
}

$id = getValue('id', 'int', 'GET', '');

if (isset($_COOKIE['user']) && $_COOKIE['user'] != "" && $_COOKIE['role'] == 1) {
    $token = $_COOKIE['acc_token'];
    $curl = curl_init();
    $data = array();
    curl_setopt($curl, CURLOPT_POST, 1);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
    curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/user_info_company.php');
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));

    $response = curl_exec($curl);
    curl_close($curl);
    $data_tt = json_decode($response, true);
    $tt_user = $data_tt['data']['user_info_result'];
    $user_id = $_SESSION['com_id'];
    $user_name = $_SESSION['com_name'];
}

if (isset($_COOKIE['user']) && $_COOKIE['user'] != "" && $_COOKIE['role'] == 2) {
    $token = $_COOKIE['acc_token'];
    $curl = curl_init();
    $data = array();
    curl_setopt($curl, CURLOPT_POST, 1);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
    curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/user_info_employee.php');
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));
    $response = curl_exec($curl);
    curl_close($curl);
    $data_tt = json_decode($response, true);
    $tt_user = $data_tt['data']['user_info_result'];
    $user_id = $_SESSION['ep_id'];
    $user_name = $_SESSION['ep_name'];
}

$id_cty = $tt_user['com_id'];
$date = date('Y-m-d', time());

if (isset($_SESSION['quyen']) && $_SESSION['quyen'] == 1) {
    $curl = curl_init();
    $token = $_COOKIE['acc_token'];
    curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/list_all_employee_of_company.php');
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));
    $response = curl_exec($curl);
    curl_close($curl);

    $data_list = json_decode($response, true);

    $data_list_nv = $data_list['data']['items'];
    $count = count($data_list_nv);
    $newArr = [];
    for ($i = 0; $i < count($data_list_nv); $i++) {
        $value = $data_list_nv[$i];
        $newArr[$value["ep_id"]] = $value;
    }
} elseif (isset($_SESSION['quyen']) && ($_SESSION['quyen'] == 2)) {
    $curl = curl_init();
    $token = $_COOKIE['acc_token'];
    curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/list_all_my_partner.php?get_all=true');
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));
    $response = curl_exec($curl);
    curl_close($curl);
    $data_list = json_decode($response, true);
    $data_list_nv = $data_list['data']['items'];
    $count = count($data_list_nv);
    $newArr = [];
    for ($i = 0; $i < count($data_list_nv); $i++) {
        $value = $data_list_nv[$i];
        $newArr[$value["ep_id"]] = $value;
    }
}
$sql_ten_nhom = new db_query("SELECT `nvt_id`, `nvt_name` FROM `nhom-vat-tu-thiet-bi` WHERE `nvt_id` = $id AND `nvt_check` = 1 AND `nvt_id_ct` = $id_cty");
$ten_nhom_vat_tu = mysql_fetch_assoc($sql_ten_nhom->result)['nvt_name'];

$sql_don_vi_tinh = new db_query("SELECT `dvt_id`, `dvt_name` FROM `don-vi-tinh` WHERE `dvt_check` = 1 AND `dvt_id_ct` = $id_cty");
$sql_hang_san_xuat = new db_query("SELECT `hsx_id`, `hsx_name` FROM `hang-san-xuat` WHERE `hsx_check` = 1 AND `hsx_id_ct` = $id_cty");
$sql_xuat_xu = new db_query("SELECT `xx_id`, `xx_name` FROM `xuat-xu` WHERE 1");
?>
<!DOCTYPE html>
<html lang="vi">

<head>
    <title>Nhóm vật tư thiết bị</title>
    <meta charset="UTF-8">
    <meta name="robots" content="noindex,nofollow" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="../css/select2.min.css?v=<?= $ver ?>">
    <link rel="stylesheet" type="text/css" href="../css/style.css?v=<?= $ver ?>">
    <link rel="stylesheet" type="text/css" href="../css/style_h.css?v=<?= $ver ?>">
    <link rel="stylesheet" type="text/css" href="../css/style_n.css?v=<?= $ver ?>">
</head>

<body class="seclec2_radius">
    <div class="box_right gr_dv gr_dv_add">
        <?php include('../includes/sidebar.php');  ?>
        <div class="box_right_ct">
            <div class="block_gr_dv">
                <div class="block_change block_gr_dv">
                    <form action="" method="post" id="f_gr_dv_add">
                        <div class="head_wh d_flex space_b align_c">
                            <div class="head_tab d_flex space_b align_c">
                                <div class="icon_header open_sidebar_w">
                                    <span class="icon_header_tbl"></span>
                                    <span class="icon_header_tbl"></span>
                                    <span class="icon_header_tbl"></span>
                                </div>
                                <?php include("../includes/header.php"); ?>
                            </div>
                            <p class="color_grey line_16 font_s14">
                                <a href="/nhom-vat-tu-thiet-bi-chi-tiet-<?= $id ?>.html" class="cursor_p">
                                    <img src="../images/back_item_g.png" alt="">
                                </a>
                                &nbsp Thông tin vật tư thiết bị / Nhóm vật tư thiết bị / <?= $ten_nhom_vat_tu ?> / Thêm vật tư
                            </p>
                            <?php include('../includes/header.php');  ?>
                        </div>
                        <div class="add_ex_wh add_gr_dv" style="display: block;">
                            <div class="tit_info_rq back_blue">
                                <p class="color_white font_s16 line_h19 font_w700">Thêm mới vật tư thiết bị</p>
                            </div>
                            <div class="ct_add_ex_wh">
                                <div class="box_add_ex_wh d_flex space_b">
                                    <div class="l_add_ex">
                                        <!-- <div class="d_flex flex_column mb_15">
                                            <p class="color_grey font_s15 line_h18 font_w500">Mã vật tư thiết bị</p>
                                            <input class="color_grey3 font_s14 line_h17 font_w400" type="text"
                                                value="VT-0000" disabled="disabled">
                                        </div> -->
                                        <div class="name_full_dv d_flex flex_column mb_15">
                                            <p class="color_grey font_s15 line_h18 font_w500">Tên đầy đủ thiết bị vật tư
                                                <span style="color: red;">*</span>
                                            </p>
                                            <input class="color_grey3 font_s14 line_h17 font_w400" type="text" placeholder="Nhập tên vật tư thiết bị" name="name_full_dv">
                                        </div>
                                        <div class="d_flex flex_column mb_15">
                                            <p class="color_grey font_s15 line_h18 font_w500">Nhóm vật tư thiết bị</p>
                                            <input class="color_grey3 font_s14 line_h17 font_w400" type="text" name="group_dv" value="<?= $ten_nhom_vat_tu ?>" disabled="disabled">
                                        </div>
                                        <div class="box_add_unit d_flex flex_column mb_15">
                                            <div class="d_flex space_b">
                                                <p class="color_grey font_s15 line_h18 font_w500">Đơn vị tính<span style="color: red;">*</span>
                                                </p>
                                                <p class="add_unit color_blue font_s15 line_h18 font_w400 d_flex cursor_p">
                                                    <span class="icon_add_unit font_s16 d_flex align_c flex_center"><span>+</span></span>&nbsp;Thêm
                                                    đơn vị tính
                                                </p>
                                            </div>
                                            <select name="select_add_unit" id="" class="select_add_unit color_grey3 font_s14 line_h17 font_w400" style="width: 100%;">
                                                <option value=""></option>
                                                <?php while ($row_dvt = mysql_fetch_assoc($sql_don_vi_tinh->result)) { ?>
                                                    <option value="<?= $row_dvt['dvt_id']; ?>"><?= $row_dvt['dvt_name']; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                        <div class="d_flex flex_column mb_15 position_r">
                                            <p class="color_grey font_s15 line_h18 font_w500">Đơn giá</p>
                                            <input class="color_grey3 font_s14 line_h17 font_w400" type="text" name="price" placeholder="Nhập đơn giá">
                                            <span class="color_grey3 font_s14 line_h17 font_w400 position_a" style="right: 15px; bottom: 10px;">VNĐ</span>
                                        </div>
                                        <div class="d_flex flex_column mb_15">
                                            <p class="color_grey font_s15 line_h18 font_w500">Hãng sản xuất</p>
                                            <select name="select_brand" id="" class="select_brand color_grey3 font_s14 line_h17 font_w400" style="width: 100%;">
                                                <option value=""></option>
                                                <?php while ($row_hsx = mysql_fetch_assoc($sql_hang_san_xuat->result)) { ?>
                                                    <option value="<?= $row_hsx['hsx_id']; ?>"><?= $row_hsx['hsx_name']; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                        <div class="d_flex flex_column mb_15">
                                            <p class="color_grey font_s15 line_h18 font_w500">Xuất xứ</p>
                                            <select name="select_origin" id="" class="select_origin color_grey3 font_s14 line_h17 font_w400" style="width: 100%;">
                                                <option value=""></option>
                                                <?php while ($row_xx = mysql_fetch_assoc($sql_xuat_xu->result)) { ?>
                                                    <option value="<?= $row_xx['xx_id']; ?>"><?= $row_xx['xx_name']; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                        <div class="d_flex flex_column mb_15">
                                            <p class="color_grey font_s15 line_h18 font_w500">Người tạo</p>
                                            <input class="color_grey3 font_s14 line_h17 font_w400" type="text" name="user_create" data='<?= $user_id ?>' value="<?= $user_name ?>" disabled=" disabled">
                                        </div>
                                        <div class="d_flex flex_column mb_15">
                                            <p class="color_grey font_s15 line_h18 font_w500">Ngày tạo</p>
                                            <input class="color_grey3 font_s14 line_h17 font_w400" type="date" name="date_create" value="<?= $date ?>" disabled="disabled">
                                        </div>
                                        <div class="d_flex flex_column display_none">
                                            <p class="color_grey font_s15 line_h18 font_w500">Mô tả vật tư thiết bị</p>
                                            <div id="editor_mobile" style="height: 258px">
                                                <p>Nhập nội dung</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="r_add_ex">
                                        <div class="d_flex flex_column">
                                            <div id="r_add_img" class="upload_equipment_supplies">
                                                <p class=" font_s15 line_h18 font_w500 color_grey">Hình ảnh thiết bị
                                                    vật tư</p>
                                                <label for="input_file_chat" class="input_file_img1 input_file_img position_r">
                                                    <div class="upload_file_img d_flex align_c flex_center">
                                                        <picture>
                                                            <img class="d_flex align_c margin_a" src="../images/camera_b.png" alt="">
                                                            <p class="font_s14 line_h16 color_blue text_a_c">Tải lên
                                                                hình
                                                                ảnh</p>
                                                            <input type="file" id="input_file_chat" class="" hidden>
                                                        </picture>
                                                    </div>
                                                </label>
                                                <div class="upload_logo_vehicle_done position_r display_none">
                                                    <img class="ready_upload_logo" src="" alt="">
                                                    <label for="upload_logo">
                                                        <img class="add_logo position_a" src="" alt="">
                                                        <input type="file" id="upload_logo" class=" display_none" accept=".png, .jpg, .jpeg">
                                                    </label>
                                                    <img class="del_logo position_a cursor_p" src="../images/close_b.png" alt="">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="d_flex flex_column">
                                            <p class="color_grey font_s15 line_h18 font_w500">Mô tả vật tư thiết bị</p>
                                            <div id="editor" style="height: 258px">
                                                <p>Nhập nội dung</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="btn_cf_rq d_flex flex_center" style="display: flex;">
                            <button type="button" class="btn_cancel back_w color_blue font_s15 line_h18 font_w500 cursor_p">
                                <a href="/nhom-vat-tu-thiet-bi-chi-tiet-<?= $id ?>.html"></a>
                                Hủy
                            </button>
                            <button type="button" class="btn_save back_blue color_white font_s15 line_h18 font_w500 cursor_p">Lưu</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- <input type="file" name="file_test"> -->
        <?php include('../includes/popup_overview.php');  ?>
        <?php include('../includes/popup_nhom-vat-tu-thiet-bi.php') ?>
</body>
<script type="text/javascript" src="../js/jquery-3.4.1.min.js"></script>
<script type="text/javascript" src="../js/jquery.validate.min.js"></script>
<script type="text/javascript" src="../js/select2.min.js"></script>
<script type="text/javascript" src="../js/js_h.js"></script>
<script type="text/javascript" src="../js/validate_h.js"></script>
<script type="text/javascript" src="../js/js_k.js"></script>
<script type="text/javascript" src="../js/js_n.js"></script>
<script src="//cdn.ckeditor.com/4.17.1/standard/ckeditor.js"></script>
<script>
    CKEDITOR.replace('editor');
    if ($(window).width() <= 768) {
        $('#editor').parent().hide();
    }
    CKEDITOR.replace('editor_mobile');
</script>
<script>
    $('.active4').each(function() {
        if ($(this).hasClass('active4')) {
            $(this).parent().addClass('show');
            $(this).parent().parent().find('.item_sidebar_cha').addClass('active');
            $(this).find('a').addClass('active');
        }
    });

    var id_ct = <?= json_encode($id_cty); ?>;

    $('.gr_dv.gr_dv_add .btn_save').click(function() {
        var dsvt_name = $("input[name='name_full_dv']").val();
        var dsvt_nhomVatTuThietBi = <?= json_encode($id); ?>;
        var dsvt_donViTinh = $("select[name='select_add_unit']").val();
        var dsvt_donGia = $("input[name='price']").val();
        var dsvt_hangSanXuat = $("select[name='select_brand']").val();
        var dsvt_xuatXu = $("select[name='select_origin']").val();
        var dsvt_userCreateId = $("input[name='user_create']").attr('data');
        var dsvt_dateCreate = $("input[name='date_create']").val();
        var dsvt_description = CKEDITOR.instances.editor.getData();
        var role = "<?= $_COOKIE['role'] ?>"

        var file_data = $('#input_file_chat').prop('files')[0];

        var form_valid = $("#f_gr_dv_add");
        form_valid.validate({
            errorPlacement: function(error, element) {
                error.appendTo(element.parents(".name_full_dv"));
                error.appendTo(element.parents(".box_add_unit"));
                error.wrap("<span class='error'>");
            },
            rules: {
                name_full_dv: "required",
                select_add_unit: "required",
            },
            messages: {
                name_full_dv: "Vui lòng nhập tên dầy đủ thiết bị vật tư.",
                select_add_unit: "Vui lòng chọn đơn vị tính",
            },
        });

        if (form_valid.valid() === true) {
            var fd = new FormData();
            fd.append('file', file_data);
            fd.append('id_ct', id_ct);
            fd.append('dsvt_name', dsvt_name);
            fd.append('dsvt_nhomVatTuThietBi', dsvt_nhomVatTuThietBi);
            fd.append('dsvt_donViTinh', dsvt_donViTinh);
            fd.append('dsvt_donGia', dsvt_donGia);
            fd.append('dsvt_hangSanXuat', dsvt_hangSanXuat);
            fd.append('dsvt_xuatXu', dsvt_xuatXu);
            fd.append('dsvt_userCreateId', dsvt_userCreateId);
            fd.append('dsvt_dateCreate', dsvt_dateCreate);
            fd.append('dsvt_description', dsvt_description);
            fd.append('role', role);
            // $.ajax({
            //     url: "../ajax/add_danh_sach_vat_tu.php",
            //     type: "POST",
            //     contentType: false,
            //     processData: false,
            //     data: fd,
            //     success: function(data){
            //         alert(data);
            //     }
            // })
            $.ajax({
                url: "../ajax/add_danh_sach_vat_tu.php",
                type: "POST",
                // dataType: 'Json',
                contentType: false,
                processData: false,
                data: fd,
                success: function(data) {
                    $('#popup_add_notif_succ').show();
                    var text = $('#popup_add_notif_succ .p_add_succ').text('');
                    var text_new = '';
                    var name = $("input[name='name_full_dv']").val();
                    text_new += 'Thêm mới vật tư thiết bị';
                    text_new += '<strong>';
                    text_new += '&nbsp' + name;
                    text_new += '</strong>';
                    text_new += '&nbspthành công!';
                    text.append(text_new);
                }
            });
        }
    });

    $('.btn_close').click(function() {
        window.location.href = "/nhom-vat-tu-thiet-bi-chi-tiet-<?= $id ?>.html";
    });

    $('.select_add_unit,.select_wh_ex,.select_wh_in').change(function() {
        $(this).parent().find('span.error').remove();
    });
</script>

</html>