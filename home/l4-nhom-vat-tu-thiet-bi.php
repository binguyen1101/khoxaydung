<?php 
    include("config1.php");

    if(!in_array(1,$ro_nhom_vt)){
        header("Location: /tong-quan.html");
    }

    isset($_GET['page']) ? $page = $_GET['page'] : $page = 1;

    isset($_GET['input']) ? $ip = $_GET['input'] : $ip = "";

    isset($_GET['dis']) ? $dis = $_GET['dis'] : $dis = 10;

 ?>
<!DOCTYPE html>
<html lang="vi">

<head>
    <title>Nhóm vật tư thiết bị</title>
    <meta charset="UTF-8">
    <meta name="robots" content="noindex,nofollow" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="../css/select2.min.css?v=<?= $ver ?>">
    <link rel="stylesheet" type="text/css" href="../css/style.css?v=<?= $ver ?>">
    <link rel="stylesheet" type="text/css" href="../css/style_h.css?v=<?= $ver ?>">
    <link rel="stylesheet" type="text/css" href="../css/style_n.css?v=<?= $ver ?>">
</head>

<body>
    <div class="box_right gr_dv">
        <?php include('../includes/sidebar.php');  ?>
        <div class="box_right_ct">
            <div class="block_change block_gr_dv">
                <div class="head_wh d_flex space_b align_c">
                    <div class="head_tab d_flex space_b align_c">
                        <div class="icon_header open_sidebar_w">
                            <span class="icon_header_tbl"></span>
                            <span class="icon_header_tbl"></span>
                            <span class="icon_header_tbl"></span>
                        </div>
                        <?php include("../includes/header.php") ; ?>
                    </div>
                    <p class="color_grey font_s14 line_h17 font_w400">Thông tin vật tư thiết bị / Nhóm vật tư thiết bị
                    </p>
                    <?php include('../includes/header.php');  ?>
                </div>
                <div class="operation_wh d_flex space_b">
                    <div class="search_wh d_flex space_b" style="display: block;">
                        <div class="input_sr_wh">
                            <div class="box_input_sr position_r">
                                <input type="text" name="input_search" value="<?= ($ip != "") ? $ip : "" ?>" placeholder="Tìm kiếm theo mã, tên nhóm vật tư thiết bị">
                                <span class="icon_sr_wh"></span>
                            </div>
                        </div>
                    </div>
                    <div class="export_wh d_flex space_b align_c">
                        <?php if(in_array(2,$ro_nhom_vt)){?>
                            <button class="btn_px d_flex align_c cursor_p">
                                <img src="../images/img_px.png" alt="">
                                <p class="color_white font_s15 line_h18 font_w500">Thêm mới</p>
                            </button>
                        <?php }?>
                        <button class="btn_ex d_flex align_c cursor_p">
                            <img src="../images/export.png" alt="">
                            <p class="color_white font_s15 line_h18 font_w500">Xuất excel</p>
                        </button>
                        <div class="hd_ex d_flex align_c cursor_p">
                            <img src="../images/img_hd.png" alt="">
                            <p class="color_blue font_s15 line_h18 font_w500">Hướng dẫn</p>
                        </div>
                    </div>
                </div>
                <!-- Nhóm vật tư thiết bị -->
                <div class="ct_block_gr">
                    <div class="detail_wh" style="display: block;" data-page="<?= $page ?>" data-ip ="<?= $ip ?>" data-dis ="<?= $dis ?>">
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php include('../includes/popup_overview.php');  ?>
    <?php include('../includes/popup_nhom-vat-tu-thiet-bi.php') ?>
    <?php include('../includes/ghi_chu.php');  ?>
</body>
<script type="text/javascript" src="../js/jquery-3.4.1.min.js"></script>
<script type="text/javascript" src="../js/jquery.validate.min.js"></script>
<script type="text/javascript" src="../js/select2.min.js"></script>
<script type="text/javascript" src="../js/js_h.js"></script>
<script type="text/javascript" src="../js/validate_h.js"></script>
<script type="text/javascript" src="../js/js_k.js"></script>
<script type="text/javascript" src="../js/js_n.js"></script>
<script type="text/javascript" src="../js/nhom_vat_tu.js"></script>
</html>