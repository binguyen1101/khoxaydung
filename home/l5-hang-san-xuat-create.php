<?php

include("config1.php");
if (!in_array(2, $ro_hsx)) {
  header("Location: /hang-san-xuat.html");
}

$id = getValue('id', 'int', 'GET', '');

if (isset($_COOKIE['user']) && $_COOKIE['user'] != "" && $_COOKIE['role'] == 1) {
  $token = $_COOKIE['acc_token'];
  $curl = curl_init();
  $data = array();
  curl_setopt($curl, CURLOPT_POST, 1);
  curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
  curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/user_info_company.php');
  curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
  curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));

  $response = curl_exec($curl);
  curl_close($curl);
  $data_tt = json_decode($response, true);
  $tt_user = $data_tt['data']['user_info_result'];
  $user_id = $_SESSION['com_id'];
  $user_name = $_SESSION['com_name'];
}
if (isset($_COOKIE['user']) && $_COOKIE['user'] != "" && $_COOKIE['role'] == 2) {
  $token = $_COOKIE['acc_token'];
  $curl = curl_init();
  $data = array();
  curl_setopt($curl, CURLOPT_POST, 1);
  curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
  curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/user_info_employee.php');
  curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
  curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));
  $response = curl_exec($curl);
  curl_close($curl);
  $data_tt = json_decode($response, true);
  $tt_user = $data_tt['data']['user_info_result'];
  $user_id = $_SESSION['ep_id'];
  $user_name = $_SESSION['ep_name'];
}

$id_cty = $tt_user['com_id'];
$date = date('Y-m-d', time());

if (isset($_SESSION['quyen']) && $_SESSION['quyen'] == 1) {
  $curl = curl_init();
  $token = $_COOKIE['acc_token'];
  curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/list_all_employee_of_company.php');
  curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
  curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));
  $response = curl_exec($curl);
  curl_close($curl);

  $data_list = json_decode($response, true);

  $data_list_nv = $data_list['data']['items'];
  $count = count($data_list_nv);
  $newArr = [];
  for ($i = 0; $i < count($data_list_nv); $i++) {
    $value = $data_list_nv[$i];
    $newArr[$value["ep_id"]] = $value;
  }
} elseif (isset($_SESSION['quyen']) && ($_SESSION['quyen'] == 2)) {
  $curl = curl_init();
  $token = $_COOKIE['acc_token'];
  curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/list_all_my_partner.php?get_all=true');
  curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
  curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));
  $response = curl_exec($curl);
  curl_close($curl);
  $data_list = json_decode($response, true);
  $data_list_nv = $data_list['data']['items'];
  $count = count($data_list_nv);
  $newArr = [];
  for ($i = 0; $i < count($data_list_nv); $i++) {
    $value = $data_list_nv[$i];
    $newArr[$value["ep_id"]] = $value;
  }
}
$sql_hang_san_xuat = new db_query("SELECT `hsx_id`, `hsx_name` FROM `hang-san-xuat` WHERE `hsx_id` = $id AND `hsx_check` = 1 AND `hsx_id_ct` = $id_cty");
$ten_hang_san_xuat = mysql_fetch_assoc($sql_hang_san_xuat->result)['hsx_name'];

$sql_nhom_vat_tu = new db_query("SELECT `nvt_id`, `nvt_name` FROM `nhom-vat-tu-thiet-bi` WHERE `nvt_check` = 1 AND `nvt_id_ct` = $id_cty");
$sql_don_vi_tinh = new db_query("SELECT `dvt_id`, `dvt_name` FROM `don-vi-tinh` WHERE `dvt_check` = 1 AND `dvt_id_ct` = $id_cty");
$sql_xuat_xu = new db_query("SELECT `xx_id`, `xx_name` FROM `xuat-xu` WHERE 1");

?>

<!DOCTYPE html>
<html lang="vi">

<head>
  <title>Hãng sản xuất</title>
  <meta charset="UTF-8">
  <meta name="robots" content="noindex,nofollow" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" type="text/css" href="../css/select2.min.css?v=<?= $ver ?>">
  <link rel="stylesheet" type="text/css" href="../css/style.css?v=<?= $ver ?>">
  <link rel="stylesheet" type="text/css" href="../css/style_h.css?v=<?= $ver ?>">
  <link rel="stylesheet" type="text/css" href="../css/style_n.css?v=<?= $ver ?>">
</head>

<body class="seclec2_radius">
  <div class="main_wrapper_all">
    <div class="wapper_all">
      <?php include('../includes/sidebar.php');  ?>
    </div>
    <div class="main_overview" id="main_overview">
      <div class="header_menu_overview d_flex align_c space_b">
        <div class="text_link_header_back" style="display: block;">
          <div class=" d_flex align_c">
            <a href="/hang-san-xuat.html" class="cursor_p">
              <img src="../images/back_item_g.png" alt="">
            </a>
            <p class="color_grey line_16 font_s14 ml_10">Thông tin vật tư thiết bị / Hãng sản xuất /
              <?= $ten_hang_san_xuat ?> / Thêm vật tư</p>
          </div>
        </div>
        <img class="open_sidebar_w" src="../images/open_sidebar_w.png" alt="" onclick="toggle('main_sidebar')">
        <?php include('../includes/header.php');  ?>
      </div>
      <div class="body_equipment_supplies">
        <div class="text_link_body_back" style="display: none;">
          <div class=" d_flex align_c mb_15">
            <a href="/danh-sach-vat-tu-thiet-bi.html" class="cursor_p">
              <img src="../images/back_item_g.png" alt="">
            </a>
            <p class="color_grey line_16 font_s14 ml_10">Thông tin vật tư thiết bị / Hãng sản xuất /
              Tân Á / Thêm vật tư</p>
          </div>
        </div>
        <form action="" method="POST" role="form" id="create_manufacturer" name="create_manufacturer">
          <div class="body_equipment_supplies_create">
            <div class="header_body_b color_white line_h19 font_s16 font_wB">
              Thêm mới vật tư thiết bị
            </div>
            <div class="d_flex flex_w" style="flex-direction: row;">
              <div id="add_infomation" class="input_create_equipment_supplies mt_20">
                <!-- <div>
                  <p class="font_s15 line_h18 font_w500 color_grey mb_5">Mã vật tư thiết bị</p>
                  <input class="input_value_grey font_14 line_h16" disabled type="text" placeholder="VT-0000">
                </div> -->
                <div class="fullname_equipment">
                  <div class="d_flex">
                    <p class="font_s15 line_h18 font_w500 color_grey mb_5 ">Tên đầy đủ thiết bị vật tư</p>
                    <span class="color_red alert_red">*</span>
                  </div>
                  <input class="input_value font_14 line_h16" name="name_equipment" id="name_equipment" type="text" placeholder="Nhập tên vật tư thiết bị">
                </div>
                <div class="mt_20">
                  <div class="d_flex align_c space_b">
                    <p class="font_s15 line_h18 font_w500 color_grey mb_5 ">Nhóm vật tư thiết bị</p>
                    <div class="d_flex align_c cursor_p" onclick="openAndHide('','add_new_materials_equipment')">
                      <img src="../images/add_circle_b.png" alt="">
                      <p class="color_blue line_h16 font_s14 ml_5">Thêm nhóm</p>
                    </div>
                  </div>
                  <div class="select_create">
                    <select class="select_create group_materials_equipment" name="group_materials_equipment">
                      <option value="">Chọn nhóm vật tư thiết bị</option>
                      <?php while ($row_nvt = mysql_fetch_assoc($sql_nhom_vat_tu->result)) { ?>
                        <option value="<?= $row_nvt['nvt_id']; ?>"><?= $row_nvt['nvt_name']; ?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>
                <div class="mt_20 unit_equipment">
                  <div class="d_flex align_c space_b">
                    <div class="d_flex">
                      <p class="font_s15 line_h18 font_w500 color_grey mb_5 ">Đơn vị tính</p>
                      <span class="color_red alert_red">*</span>
                    </div>
                    <div class="d_flex align_c btn_add_unit cursor_p">
                      <img src="../images/add_circle_b.png" alt="">
                      <p class="color_blue line_h16 font_s14 ml_5">Thêm đơn vị tính</p>
                    </div>
                  </div>
                  <div class="select_create">
                    <select class="select_create select_unit" name="select_unit" id="select_unit">
                      <option value="">Chọn đơn vị tính</option>
                      <?php while ($row_dvt = mysql_fetch_assoc($sql_don_vi_tinh->result)) { ?>
                        <option value="<?= $row_dvt['dvt_id']; ?>"><?= $row_dvt['dvt_name']; ?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>
                <div class="mt_20">
                  <div class="d_flex">
                    <p class="font_s15 line_h18 font_w500 color_grey mb_5 ">Đơn giá</p>
                  </div>
                  <div class="position_r">
                    <input class="input_value font_14 line_h16" name="don_gia" type="text" placeholder="Nhập đơn giá">
                    <span class="position_a text_unit_price font_s14 line_h16 color_grey">VND</span>
                  </div>
                </div>
              </div>
              <div id="add_equip" class="input_create_equipment_supplies mb_20">
                <div class="mt_20">
                  <div class="">
                    <p class="font_s15 line_h18 font_w500 color_grey mb_5 ">Hãng sản xuất</p>
                    <input class="input_value_grey font_14 line_h16" disabled type="text" value="<?= $ten_hang_san_xuat ?>">
                  </div>
                </div>
                <div class="mt_20">
                  <div class="d_flex align_c space_b">
                    <p class="font_s15 line_h18 font_w500 color_grey mb_5 ">Xuất xứ</p>
                  </div>
                  <div class="select_create">
                    <select class="select_create select_origin" name="select_origin">
                      <option value="">Chọn xuất xứ</option>
                      <?php while ($row_xx = mysql_fetch_assoc($sql_xuat_xu->result)) { ?>
                        <option value="<?= $row_xx['xx_id']; ?>"><?= $row_xx['xx_name']; ?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>
                <div class="mt_20">
                  <p class="font_s15 line_h18 font_w500 color_grey mb_5">Người tạo</p>
                  <input class="input_value_grey font_14 line_h16 " name="user_create" disabled type="text" data='<?= $user_id ?>' value="<?= $user_name ?>">
                </div>
                <div class=" mt_20">
                  <p class="font_s15 line_h18 font_w500 color_grey mb_5">Ngày tạo</p>
                  <input class="input_value_grey font_14 line_h16 " name="date_create" disabled type="date" value="<?= $date ?>">
                </div>
              </div>
              <div id="upload_equips" class="upload_equipment_supplies mt_20 ">
                <p class=" font_s15 line_h18 font_w500 color_grey mb_5">Hình ảnh thiết bị vật tư</p>
                <label for="input_file_chat" class="input_file_img1 input_file_img position_r">
                  <div class="upload_file_img d_flex align_c flex_center" style="padding-top: 0 !important;">
                    <picture>
                      <img class="d_flex align_c margin_a" src="../images/camera_b.png" alt="">
                      <p class="font_s14 line_h16 color_blue text_a_c">Tải lên
                        hình
                        ảnh</p>
                      <input type="file" id="input_file_chat" class="" hidden>
                    </picture>
                  </div>
                </label>
                <div class="upload_logo_vehicle_done position_r display_none">
                  <img class="ready_upload_logo" src="" alt="">
                  <label for="upload_logo">
                    <img class="add_logo position_a" src="" alt="">
                    <input type="file" id="upload_logo" class=" display_none" accept=".png, .jpg, .jpeg">
                  </label>
                  <img class="del_logo position_a cursor_p" src="../images/close_b.png" alt="">
                </div>
              </div>
              <div id="details_equip" class="upload_equipment_supplies mb_20">
                <p class="mt_20 font_s15 line_h18 font_w500 color_grey mb_5">Mô tả vật tư thiết bị</p>
                <div id="editor" style="height: 258px">
                  <p>Nhập nội dung</p>
                </div>
              </div>
            </div>
          </div>
          <div class="d_flex align_c flex_center button_add_new_equipment_supplies">
            <button class="button_close color_blue font_s15 line_h18 font_w500"><a href="/hang-san-xuat-chi-tiet-<?= $id ?>.html"></a>Hủy</button>
            <button class="button_accp color_white font_s15 line_h18 font_w500" type="button">Lưu</button>
            <!-- onclick="openAndHide('','popup_cancel_manufacturer')"
              onclick="openAndHide('','create_new_equipment_materials')" -->
          </div>
        </form>
      </div>
    </div>
    <?php include('../includes/popup_overview.php');  ?>
    <?php include('../includes/popup_h.php');  ?>
    <?php include('../includes/popup_don-vi-tinh.php');  ?>
  </div>
</body>
<script type="text/javascript" src="../js/jquery-3.4.1.min.js"></script>
<script type="text/javascript" src="../js/jquery.validate.min.js"></script>
<script type="text/javascript" src="../js/select2.min.js"></script>
<script type="text/javascript" src="../js/js_h.js"></script>
<script type="text/javascript" src="../js/validate_h.js"></script>
<script type="text/javascript" src="../js/js_k.js"></script>
<script type="text/javascript" src="../js/js_n.js"></script>
<script src="//cdn.ckeditor.com/4.17.1/standard/ckeditor.js"></script>
<script>
  CKEDITOR.replace('editor');
</script>
<script>
  $('.active5').each(function() {
    if ($(this).hasClass('active5')) {
      $(this).parent().addClass('show');
      $(this).parent().parent().find('.item_sidebar_cha').addClass('active');
      $(this).find('a').addClass('active');
    }
  });

  var id_ct = <?= json_encode($id_cty); ?>;
  $('.button_accp').click(function() {
    var dsvt_name = $("input[name='name_equipment']").val();
    var dsvt_nhomVatTuThietBi = $("select[name='group_materials_equipment']").val();
    var dsvt_donViTinh = $("select[name='select_unit']").val();
    var dsvt_donGia = $("input[name='don_gia']").val();
    var dsvt_hangSanXuat = <?= $id ?>;
    var dsvt_xuatXu = $("select[name='select_origin']").val();
    var dsvt_userCreateId = $("input[name='user_create']").attr('data');
    var dsvt_dateCreate = $("input[name='date_create']").val();
    var dsvt_description = CKEDITOR.instances.editor.getData();
    var file_data = $('#input_file_chat').prop('files')[0];
    var role = "<?= $_COOKIE['role'] ?>"

    var form_valid = $("#create_manufacturer");
    form_valid.validate({
      errorPlacement: function(error, element) {
        error.appendTo(element.parents(".fullname_equipment"));
        error.appendTo(element.parents(".unit_equipment"));
        error.wrap("<span class='error'>");
      },
      rules: {
        name_equipment: "required",
        select_unit: "required",
      },
      messages: {
        name_equipment: "Vui lòng nhập tên dầy đủ thiết bị vật tư.",
        select_unit: "Vui lòng chọn đơn vị tính",
      },
    });

    if (form_valid.valid() === true) {
      var fd = new FormData();
      fd.append('file', file_data);
      fd.append('id_ct', id_ct);
      fd.append('dsvt_name', dsvt_name);
      fd.append('dsvt_nhomVatTuThietBi', dsvt_nhomVatTuThietBi);
      fd.append('dsvt_donViTinh', dsvt_donViTinh);
      fd.append('dsvt_donGia', dsvt_donGia);
      fd.append('dsvt_hangSanXuat', dsvt_hangSanXuat);
      fd.append('dsvt_xuatXu', dsvt_xuatXu);
      fd.append('dsvt_userCreateId', dsvt_userCreateId);
      fd.append('dsvt_dateCreate', dsvt_dateCreate);
      fd.append('dsvt_description', dsvt_description);
      fd.append('role', role);

      $.ajax({
        url: "../ajax/add_danh_sach_vat_tu.php",
        type: "POST",
        // dataType: 'Json',
        contentType: false,
        processData: false,
        data: fd,
        success: function(data) {
          $('#popup_add_notif_succ').show();
          var text = $('#popup_add_notif_succ .p_add_succ').text('');
          var text_new = '';
          var name = $("input[name='name_equipment']").val();
          text_new += 'Thêm mới vật tư thiết bị';
          text_new += '<strong>';
          text_new += '&nbsp' + name;
          text_new += '</strong>';
          text_new += '&nbspthành công!';
          text.append(text_new);
        }
      });
    }
  });

  $('.btn_close').click(function() {
    window.location.href = "/hang-san-xuat-chi-tiet-<?= $id ?>.html";
  });
</script>

</html>