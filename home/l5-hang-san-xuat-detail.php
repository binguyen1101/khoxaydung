<?php
include("config1.php");

$id = getValue('id', 'int', 'GET', '');
isset($_GET['page']) ? $page = $_GET['page'] : $page = 1;
isset($_GET['nvt']) ? $nvt = $_GET['nvt'] : $nvt = "";
isset($_GET['xx']) ? $xx = $_GET['xx'] : $xx = "";
isset($_GET['sort']) ? $sort = $_GET['sort'] : $sort = "";
isset($_GET['input']) ? $ip = $_GET['input'] : $ip = "";

if (isset($_COOKIE['user']) && $_COOKIE['user'] != "" && $_COOKIE['role'] == 1) {
    $token = $_COOKIE['acc_token'];
    $curl = curl_init();
    $data = array();
    curl_setopt($curl, CURLOPT_POST, 1);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
    curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/user_info_company.php');
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));

    $response = curl_exec($curl);
    curl_close($curl);
    $data_tt = json_decode($response, true);
    $tt_user = $data_tt['data']['user_info_result'];
    $user_id = $_SESSION['com_id'];
    $user_name = $_SESSION['com_name'];
}

if (isset($_COOKIE['user']) && $_COOKIE['user'] != "" && $_COOKIE['role'] == 2) {
    $token = $_COOKIE['acc_token'];
    $curl = curl_init();
    $data = array();
    curl_setopt($curl, CURLOPT_POST, 1);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
    curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/user_info_employee.php');
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));
    $response = curl_exec($curl);
    curl_close($curl);
    $data_tt = json_decode($response, true);
    $tt_user = $data_tt['data']['user_info_result'];
    $user_id = $_SESSION['ep_id'];
    $user_name = $_SESSION['ep_name'];
}

$id_cty = $tt_user['com_id'];

$sql_hang = new db_query("SELECT `hsx_id`, `hsx_name` FROM `hang-san-xuat` WHERE `hsx_id` = $id AND `hsx_check` = 1 AND `hsx_id_ct` = $id_cty");
$ten_hang_san_xuat = mysql_fetch_assoc($sql_hang->result)['hsx_name'];

$sql_nhom_vat_tu = new db_query("SELECT `nvt_id`, `nvt_name` FROM `nhom-vat-tu-thiet-bi` WHERE `nvt_check` = 1 AND `nvt_id_ct` = $id_cty");
$sql_xuat_xu = new db_query("SELECT `xx_id`, `xx_name` FROM `xuat-xu` WHERE 1");

?>
<!DOCTYPE html>
<html lang="vi">

<head>
    <title>Hãng sản xuất</title>
    <meta charset="UTF-8">
    <meta name="robots" content="noindex,nofollow" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="../css/select2.min.css?v=<?= $ver ?>">
    <link rel="stylesheet" type="text/css" href="../css/style.css?v=<?= $ver ?>">
    <link rel="stylesheet" type="text/css" href="../css/style_h.css?v=<?= $ver ?>">
    <link rel="stylesheet" type="text/css" href="../css/style_n.css?v=<?= $ver ?>">
</head>

<body>
    <div class="main_wrapper_all">
        <div class="wapper_all">
            <?php include('../includes/sidebar.php');  ?>
        </div>
        <div class="main_overview" id="main_overview">
            <div class="header_menu_overview d_flex align_c space_b">
                <div class="text_link_header_back" style="display: block;">
                    <div class=" d_flex align_c">
                        <a href="/hang-san-xuat.html" class="cursor_p">
                            <img src="../images/back_item_g.png" alt="">
                        </a>
                        <p class="color_grey line_16 font_s14 ml_10">Thông tin vật tư thiết bị / Hãng sản xuất /
                            <?= $ten_hang_san_xuat ?>
                        </p>
                    </div>
                </div>
                <img class="open_sidebar_w" src="../images/open_sidebar_w.png" alt="" onclick="toggle('main_sidebar')">
                <?php include('../includes/header.php');  ?>
            </div>
            <div class="body_equipment_supplies">
                <div class="text_link_body_back" style="display: none;">
                    <div class=" d_flex align_c mb_15">
                        <a href="/hang-san-xuat.html" class="cursor_p">
                            <img src="../images/back_item_g.png" alt="">
                        </a>
                        <p class="color_grey line_16 font_s14 ml_10">Thông tin vật tư thiết bị / Hãng sản xuất /
                            <?= $ten_hang_san_xuat ?></p>
                    </div>
                </div>
                <div>
                    <div class="d_flex align_c space_b">
                        <div class="d_flex align_c">
                            <div class="d_flex align_c" id="block16">
                                <div class="mr_15" id="block14">
                                    <select class="all_equipment_supplies" name="all_equipment_supplies">
                                        <option value="">Tất cả nhóm vật tư thiết bị</option>
                                        <?php while ($row_nvt = mysql_fetch_assoc($sql_nhom_vat_tu->result)) { ?>
                                            <option value="<?= $row_nvt['nvt_id']; ?>" <?= ($nvt == $row_nvt['nvt_id']) ? "selected" : "" ?>>
                                                <?= $row_nvt['nvt_name']; ?>
                                            </option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="d_flex align_c" id="block15">
                                    <div class="mr_15">
                                        <select class="all_provenance" name="all_provenance">
                                            <option value="">Tất cả xuất xứ</option>
                                            <?php while ($row_xx = mysql_fetch_assoc($sql_xuat_xu->result)) { ?>
                                                <option value="<?= $row_xx['xx_id']; ?>" <?= ($xx == $row_xx['xx_id'])
                                                                                                ? "selected" : "" ?>>
                                                    <?= $row_xx['xx_name']; ?>
                                                </option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <div>
                                        <select class="no_sort_price" name="no_sort_price">
                                            <option value="">Không sắp xếp đơn giá</option>
                                            <option value="1" <?= ($sort == 1) ? "selected" : "" ?>>Đơn giá tăng dần</option>
                                            <option value="2" <?= ($sort == 2) ? "selected" : "" ?>>Đơn giá giảm dần</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="d_flex align_c space_b padding_tb20" id="block17">
                        <div class="position_r" id="block18">
                            <input class="search_equipment_id d_flex align_c space_b" name="input_search" type="text" placeholder="Tìm kiếm theo mã, tên vật tư thiết bị" value="<?= ($ip != "") ? $ip : "" ?>">
                            <img class="position_a icon_search_equipment_id cursor_p" src="../images/icon-sr.png" alt="">
                        </div>
                        <?php if (in_array(2, $ro_hsx)) { ?>
                            <div class="d_flex align_c" id="block19">
                                <a href="/hang-san-xuat-them-moi-vat-tu-<?= $id ?>.html">
                                    <button class="d_flex align_c button_add_new cursor_p">
                                        <img class="mr_10 img_button_add_new" src="../images/add_new.png" alt="">
                                        <p class="color_white line_18 font_s15 font_w500">Thêm mới</p>
                                    </button>
                                </a>
                            </div>
                        <?php } ?>
                    </div>
                </div>
                <div class="ct_block_gr">
                    <div class="detail_wh" style="display: block;" data-page="<?= $page ?>" data-nvt="<?= $nvt ?>" data-xx="<?= $xx ?>" data-sort="<?= $sort ?>" data-ip="<?= $ip ?>">

                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php include('../includes/popup_overview.php');  ?>
    </div>
</body>
<script type="text/javascript" src="../js/jquery-3.4.1.min.js"></script>
<script type="text/javascript" src="../js/jquery.validate.min.js"></script>
<script type="text/javascript" src="../js/select2.min.js"></script>
<script type="text/javascript" src="../js/js_h.js"></script>
<script type="text/javascript" src="../js/validate_h.js"></script>
<script type="text/javascript" src="../js/js_k.js"></script>
<script type="text/javascript" src="../js/js_n.js"></script>
<script>
    $('.active5').each(function() {
        if ($(this).hasClass('active5')) {
            $(this).parent().addClass('show');
            $(this).parent().parent().find('.item_sidebar_cha').addClass('active');
            $(this).find('a').addClass('active');
        }
    });

    var page = 1;
    var nvt = $(".detail_wh").attr("data-nvt");
    var xx = $(".detail_wh").attr("data-xx");
    var sort = $(".detail_wh").attr("data-sort");
    var input_val = $(".detail_wh").attr("data-ip");
    var id_hsx = <?= json_encode($id); ?>;

    $.ajax({
        url: "../render/tb_hang_san_xuat_detail.php",
        type: "POST",
        data: {
            id_hsx: id_hsx,
            page: page,
            nvt: nvt,
            xx: xx,
            sort: sort,
            input_val: input_val,
        },
        success: function(data) {
            $(".detail_wh").append(data);
        }
    });

    $('.all_equipment_supplies, .all_provenance, .no_sort_price').change(function() {
        var nvt = $("select[name='all_equipment_supplies']").val();
        var xx = $("select[name='all_provenance']").val();
        var sort = $("select[name='no_sort_price']").val();
        var input_val = $("input[name='input_search']").val();

        if (nvt == "" && xx == "" && sort == "" && input_val == "") {
            window.location.href = "/hang-san-xuat-chi-tiet-" + id_hsx + ".html"
        } else {
            window.location.href = "/hang-san-xuat-chi-tiet-" + id_hsx + ".html?nvt=" + nvt + "&xx=" + xx + "&sort=" + sort + "&input=" + input_val + "&page=" + page;
        }
        $("input[name='input_search'").val('');
    });



    $(".icon_search_equipment_id").click(function() {
        var nvt = $("select[name='all_equipment_supplies']").val();
        var xx = $("select[name='all_provenance']").val();
        var sort = $("select[name='no_sort_price']").val();
        var input_val = $("input[name='input_search']").val();
        if (nvt == "" && xx == "" && sort == "" && input_val == "") {
            window.location.href = "/hang-san-xuat-chi-tiet-" + id_hsx + ".html"
        } else {
            window.location.href = "/hang-san-xuat-chi-tiet-" + id_hsx + ".html?nvt=" + nvt + "&xx=" + xx + "&sort=" + sort + "&input=" + input_val + "&page=" + page;
        }
    });

    $(document).keyup(function(e) {
        if ($("input[name='input_search']").is(":focus") && (e.keyCode == 13)) {
            var nvt = $("select[name='all_equipment_supplies']").val();
            var xx = $("select[name='all_provenance']").val();
            var sort = $("select[name='no_sort_price']").val();
            var input_val = $("input[name='input_search']").val();
            if (nvt == "" && xx == "" && sort == "" && input_val == "") {
                window.location.href = "/hang-san-xuat-chi-tiet-" + id_hsx + ".html"
            } else {
                window.location.href = "/hang-san-xuat-chi-tiet-" + id_hsx + ".html?nvt=" + nvt + "&xx=" + xx + "&sort=" + sort + "&input=" + input_val + "&page=" + page;
            }
        }
    });
</script>

</html>