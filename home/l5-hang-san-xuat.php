<?php 
  include("config1.php");

  if(!in_array(1,$ro_hsx)){
    header("Location: /tong-quan.html");
  }

  isset($_GET['page']) ? $page = $_GET['page'] : $page = 1;

  isset($_GET['input']) ? $ip = $_GET['input'] : $ip = "";

  isset($_GET['dis']) ? $dis = $_GET['dis'] : $dis = 10;

?>
<!DOCTYPE html>
<html lang="vi">

<head>
  <title>Hãng sản xuất</title>
  <meta charset="UTF-8">
  <meta name="robots" content="noindex,nofollow" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" type="text/css" href="../css/select2.min.css?v=<?= $ver ?>">
  <link rel="stylesheet" type="text/css" href="../css/style.css?v=<?= $ver ?>">
  <link rel="stylesheet" type="text/css" href="../css/style_h.css?v=<?= $ver ?>">
  <link rel="stylesheet" type="text/css" href="../css/style_n.css?v=<?= $ver ?>">
</head>

<body>
  <div class="main_wrapper_all">
    <div class="wapper_all">
      <?php include('../includes/sidebar.php');  ?>
    </div>
    <div class="main_overview" id="main_overview">
      <div class="header_menu_overview d_flex align_c space_b">
        <p class="color_grey line_16 font_s14 text_link_page" style="display: block;">Thông tin vật tư thiết bị / Hãng
          sản xuất</p>
        <img class="open_sidebar_w" src="../images/open_sidebar_w.png" alt="" onclick="toggle('main_sidebar')">
        <?php include('../includes/header.php');  ?>
      </div>
      <div class="body_equipment_supplies">
        <p class="color_grey line_16 font_s14 text_link_page_re mb_15" style="display: none;">Thông tin vật tư thiết bị
          / Hãng sản xuất</p>
        <div>
          <div class="d_flex space_b mb_20" id="block13">
              <div class="position_r" id="block09">
                <input class="search_equipment_id d_flex align_c space_b" type="text" name="input_search"
                  placeholder="Tìm kiếm theo mã, tên vật tư thiết bị" value="<?= ($ip != "") ? $ip : "" ?>">
                <img class="position_a icon_search_equipment_id" src="../images/icon-sr.png" alt="">
              </div>
            <div class="d_flex align_c" id="block10">
              <?php if(in_array(2,$ro_hsx)){?>
                <div id="block12">
                  <button class="d_flex align_c button_add_new cursor_p" onclick="openAndHide('','add_new_manufacturer')">
                    <img class="mr_10 img_button_add_new" src="../images/add_new.png" alt="">
                    <p class="color_white line_18 font_s15 font_w500">Thêm mới</p>
                  </button>
                </div>
              <?php }?>
              <div class="d_flex align_c" id="block11">
                <button class="d_flex align_c button_export_excel ml_15 cursor_p">
                  <img class="mr_10 img_button_export_excel" src="../images/export_excel.png" alt="">
                  <p class="color_white line_18 font_s15 font_w500">Xuất excel</p>
                </button>
                <div class="d_flex align_c ml_15">
                  <img src="../images/img_hd.png" alt="">
                  <p class="padding_l5 color_blue line_18 font_s15 font_w500">Hướng dẫn</p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="ct_block_gr">
          <div class="detail_wh" data-page="<?= $page ?>" data-ip="<?= $ip ?>" data-dis ="<?= $dis ?>">
            <!--  -->
          </div>
        </div>
        <?php include('../includes/popup_overview.php');  ?>
        <?php include('../includes/popup_h.php');  ?>
        <?php include('../includes/popup_don-vi-tinh.php');  ?>
        <?php include('../includes/ghi_chu.php');  ?>
      </div>
</body>
<script type="text/javascript" src="../js/jquery-3.4.1.min.js"></script>
<script type="text/javascript" src="../js/jquery.validate.min.js"></script>
<script type="text/javascript" src="../js/select2.min.js"></script>
<script type="text/javascript" src="../js/js_h.js"></script>
<script type="text/javascript" src="../js/validate_h.js"></script>
<script type="text/javascript" src="../js/js_k.js"></script>
<script type="text/javascript" src="../js/js_n.js"></script>

<script>

    var input_val = $(".detail_wh").attr("data-ip");
    var page = $(".detail_wh").attr("data-page");
    var curr = $(".detail_wh").attr("data-dis");

    $.ajax({
    url: "../render/tb_hang_san_xuat.php",
    type: "POST",
    data:{
        input_val: input_val,
        page: page,
        curr: curr
    },
    success: function(data){
        $(".detail_wh").html(data);
    }
    });

  $(".icon_search_equipment_id").click(function(){
    var input_val = $("input[name='input_search'").val();
    var page = 1;
    var curr = $('.show_tr_tb').val();
    if(input_val == "" && curr == 10){
        window.location.href = "/hang-san-xuat.html?dis=" + curr + '&page=' + page;
    }else{
        window.location.href = "/hang-san-xuat.html?input=" + input_val + "&dis=" + curr + "&page=" + page;
    }
});

$(document).keyup(function (e) {
    if ($("input[name='input_search']").is(":focus") && (e.keyCode == 13)) {
      $(".icon_search_equipment_id").click();
    }
});

$(".button_export_excel").click(function() {
    window.location.href = '../Excel/hang_san_xuat.php';
});

function display(select){
    var curr = $(select).val();
    var page = 1;
    var input_val = $("input[name='input_search']").val();
    if(input_val == "" && curr == 10){
        window.location.href = "/hang-san-xuat.html?dis=" + curr + '&page=' + page;
    }else{
        window.location.href = "/hang-san-xuat.html?input=" + input_val + "&dis=" + curr + "&page=" + page;
    }
    $("input[name='input_search'").val('');
}


</script>

</html>