<?php
include("config1.php");

if (!in_array(1, $ro_ton_kho)) {
	header("Location: /tong-quan.html");
}

$id_kho = getValue('id', 'int', 'GET', '');
isset($_GET['id']) ? $id = $_GET['id'] : $id = "";
isset($_GET['page']) ? $page = $_GET['page'] : $page = 1;
isset($_GET['nvt']) ? $nvt = $_GET['nvt'] : $nvt = "";
isset($_GET['hsx']) ? $hsx = $_GET['hsx'] : $hsx = "";
isset($_GET['xx']) ? $xx = $_GET['xx'] : $xx = "";
isset($_GET['sort']) ? $sort = $_GET['sort'] : $sort = "";
isset($_GET['input']) ? $ip = $_GET['input'] : $ip = "";

if (isset($_COOKIE['user']) && $_COOKIE['user'] != "" && $_COOKIE['role'] == 1) {
	$token = $_COOKIE['acc_token'];
	$curl = curl_init();
	$data = array();
	curl_setopt($curl, CURLOPT_POST, 1);
	curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
	curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/user_info_company.php');
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
	curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));

	$response = curl_exec($curl);
	curl_close($curl);
	$data_tt = json_decode($response, true);
	$tt_user = $data_tt['data']['user_info_result'];
	// $id_nguoi_xoa = $_SESSION['com_id'];
}
if (isset($_COOKIE['user']) && $_COOKIE['user'] != "" && $_COOKIE['role'] == 2) {
	$token = $_COOKIE['acc_token'];
	$curl = curl_init();
	$data = array();
	curl_setopt($curl, CURLOPT_POST, 1);
	curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
	curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/user_info_employee.php');
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
	curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));
	$response = curl_exec($curl);
	curl_close($curl);
	$data_tt = json_decode($response, true);
	$tt_user = $data_tt['data']['user_info_result'];
}

$id_cty = $tt_user['com_id'];

$tenkho = new db_query("SELECT `kho_id`, `kho_name` FROM `kho` WHERE `kho_id` = '$id_kho' ");
$item_n = mysql_fetch_assoc($tenkho->result);
$ten_kho = $item_n['kho_name'];

$sql_nhom_vat_tu = new db_query("SELECT `nvt_id`, `nvt_name` FROM `nhom-vat-tu-thiet-bi` WHERE `nvt_check` = 1 AND `nvt_id_ct` = $id_cty");
$sql_hang_san_xuat = new db_query("SELECT `hsx_id`, `hsx_name` FROM `hang-san-xuat` WHERE `hsx_check` = 1 AND `hsx_id_ct` = $id_cty");
$sql_xuat_xu = new db_query("SELECT `xx_id`, `xx_name` FROM `xuat-xu` WHERE 1");

?>
<!DOCTYPE html>
<html lang="vi">

<head>
	<title>Thông tin kho</title>
	<meta charset="UTF-8">
	<meta name="robots" content="noindex,nofollow" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" type="text/css" href="../css/select2.min.css?v=<?= $ver ?>">
	<link rel="stylesheet" type="text/css" href="../css/style.css?v=<?= $ver ?>">
	<link rel="stylesheet" type="text/css" href="../css/style_h.css?v=<?= $ver ?>">
	<link rel="stylesheet" type="text/css" href="../css/style_n.css?v=<?= $ver ?>">
	<link rel="stylesheet" type="text/css" href="../css/style_k.css?v=<?= $ver ?>">
</head>

<body>
	<div class="main_wrapper_all ">
		<div class="wapper_all">
			<?php include('../includes/sidebar.php');  ?>
		</div>
		<div class="main_overview" id="main_overview">
			<div class="header_menu_overview d_flex align_c space_b">
				<p class="color_grey line_16 font_s14 text_link_page" style="display: block;">
					<a href="/danh-sach-kho.html" class="cursor_p">
						<img src="../images/back_item_g.png" alt="">
					</a>Tồn kho / <?= $ten_kho ?>
				</p>
				<img class="open_sidebar_w" src="../images/open_sidebar_w.png" alt="" onclick="toggle('main_sidebar')" style="display: none;">
				<?php include('../includes/header.php');  ?>
			</div>
			<div class="main_list_wh">
				<div class="body_equipment_supplies list_wh">
					<div class="head_wh d_flex space_b align_c title_wh" style="display: none;">
						<div class=" d_flex align_c">
							<a href="/danh-sach-kho.html" class="cursor_p">
								<img src="../images/back_item_g.png" alt="">
							</a>
							<p class="color_grey line_16 font_s14 ml_10">
								Tồn kho / <?= $ten_kho ?>
							</p>
						</div>
					</div>
					<div class="d_flex align_c space_b mb_20 header_list_wh">
						<div class="d_flex align_c option_wh_list">
							<div class="mr_15">
								<select class="all_meterial_wh " name="all_meterial_wh">
									<option value="">Tất cả nhóm vật tư thiết bị</option>
									<?php while ($row_nvt = mysql_fetch_assoc($sql_nhom_vat_tu->result)) { ?>
										<option value="<?= $row_nvt['nvt_id']; ?>" <?= ($nvt == $row_nvt['nvt_id']) ? "selected" : "" ?>>
											<?= $row_nvt['nvt_name']; ?>
										</option>
									<?php } ?>
								</select>
							</div>
							<div class="mr_15">
								<select class="all_manufacturer" name="all_manufacturer">
									<option value="">Tất cả hãng sản xuất</option>
									<?php while ($row_hsx = mysql_fetch_assoc($sql_hang_san_xuat->result)) { ?>
										<option value="<?= $row_hsx['hsx_id']; ?>" <?= ($hsx == $row_hsx['hsx_id']) ? "selected" : "" ?>><?= $row_hsx['hsx_name']; ?></option>
									<?php } ?>
								</select>
							</div>
							<div class="mr_15">
								<select class="all_provenance" name="all_provenance">
									<option value="">Tất cả xuất xứ</option>
									<?php while ($row_xx = mysql_fetch_assoc($sql_xuat_xu->result)) { ?>
										<option value="<?= $row_xx['xx_id']; ?>" <?= ($xx == $row_xx['xx_id']) ? "selected" : "" ?>><?= $row_xx['xx_name']; ?></option>
									<?php } ?>
								</select>
							</div>
							<select class="no_sort_quantily" name="no_sort_quanily">
								<option value="">Không sắp xếp số lượng</option>
								<option value="1" <?= ($sort == 1) ? "selected" : "" ?>>Số lượng tăng dần</option>
								<option value="2" <?= ($sort == 2) ? "selected" : "" ?>>Số lượng giảm dần</option>
							</select>
						</div>
						<div class="btn_hd">
							<div class="d_flex align_c ">
								<img src="../images/img_hd.png" alt="">
								<p class="padding_l5 color_blue line_18 font_s15 font_w500">Hướng dẫn</p>
							</div>
						</div>
					</div>
					<div class="d_flex align_c space_b mb_20 operation_wh_375">
						<div class="position_r btn_search">
							<input class="search_equipment_id d_flex align_c space_b" name="input_search" type="text" placeholder="Tìm kiếm theo mã, tên vật tư thiết bị" value="<?= ($ip != "") ? $ip : "" ?>">
							<img class="position_a icon_search_equipment_id" src="../images/icon-sr.png" alt="">
						</div>
						<div class="d_flex align_c btn_wh_list_375">
							<div class=" export_wh">
								<button class="btn_ex d_flex align_c cursor_p" data="<?= $id_kho ?>">
									<img src="../images/export.png" alt="">
									<p class="color_white font_s15 line_h18 font_w500">Xuất excel</p>
								</button>
							</div>
							<div class="btn_hd_375">
								<div class="d_flex align_c ">
									<img src="../images/img_hd.png" alt="">
									<p class="padding_l5 color_blue line_18 font_s15 font_w500">Hướng dẫn</p>
								</div>
							</div>
						</div>
					</div>
					<div class="wh_info" data-page="<?= $page ?>" data-nvt="<?= $nvt ?>" data-hsx="<?= $hsx ?>" data-xx="<?= $xx ?>" data-sort="<?= $sort ?>" data-ip="<?= $ip ?>">

					</div>
				</div>

				<?php include('../includes/popup_danh-sach-kho.php');  ?>
				<?php include('../includes/popup_overview.php');  ?>
			</div>
		</div>
	</div>

</body>
<script type="text/javascript" src="../js/jquery-3.4.1.min.js"></script>
<script type="text/javascript" src="../js/jquery.validate.min.js"></script>
<script type="text/javascript" src="../js/select2.min.js"></script>
<script type="text/javascript" src="../js/js_h.js"></script>
<script type="text/javascript" src="../js/validate_h.js"></script>
<script type="text/javascript" src="../js/js_n.js"></script>
<script type="text/javascript" src="../js/js_k.js"></script>
<script>
	$('.active7').each(function() {
		if ($(this).hasClass('active7')) {
			$(this).find('a').addClass('active');
		}
	});

	var page = $(".wh_info").attr("data-page");
	var nvt = $(".wh_info").attr("data-nvt");
	var hsx = $(".wh_info").attr("data-hsx");
	var xx = $(".wh_info").attr("data-xx");
	var sort = $(".wh_info").attr("data-sort");
	var input_val = $(".wh_info").attr("data-ip");
	var id_kho = <?= $id_kho ?>;

	$.ajax({
		url: '../render/tb_danh_sach_kho_chi_tiet.php',
		type: 'POST',
		data: {
			id_kho: id_kho,
			page: page,
			nvt: nvt,
			hsx: hsx,
			xx: xx,
			sort: sort,
			input_val: input_val,
		},
		success: function(data) {
			$(".wh_info").append(data);
		}
	})

	$('.all_meterial_wh, .all_manufacturer, .all_provenance, .no_sort_quantily').change(function() {
		var page = 1;
		var nvt = $("select[name='all_meterial_wh']").val();
		var hsx = $("select[name='all_manufacturer']").val();
		var xx = $("select[name='all_provenance']").val();
		var sort = $("select[name='no_sort_quanily']").val();
		var input_val = $("input[name='input_search']").val();

		if (nvt == "" && hsx == "" && xx == "" && sort == "" && input_val == "") {
			window.location.href = "/danh-sach-kho-chi-tiet-<?= $id_kho ?>.html"
		} else {
			window.location.href = "/danh-sach-kho-chi-tiet-<?= $id_kho ?>.html?nvt=" + nvt + "&hsx=" + hsx + "&xx=" + xx + "&sort=" + sort + "&input=" + input_val + "&page=" + page;
		}
		$("input[name='input_search'").val('');
	});

	$(".icon_search_equipment_id").click(function() {
		var page = 1;
		var nvt = $("select[name='all_meterial_wh']").val();
		var hsx = $("select[name='all_manufacturer']").val();
		var xx = $("select[name='all_provenance']").val();
		var sort = $("select[name='no_sort_quanily']").val();
		var input_val = $("input[name='input_search']").val();

		if (nvt == "" && hsx == "" && xx == "" && sort == "" && input_val == "") {
			window.location.href = "/danh-sach-kho-chi-tiet-<?= $id_kho ?>.html"
		} else {
			window.location.href = "/danh-sach-kho-chi-tiet-<?= $id_kho ?>.html?nvt=" + nvt + "&hsx=" + hsx + "&xx=" + xx + "&sort=" + sort + "&input=" + input_val + "&page=" + page;
		}
		$("input[name='input_search'").val('');
	});

	$(".btn_ex").click(function() {
		var id_kho = $(this).attr("data");
		window.location.href = '../Excel/ton_kho.php?id=' + id_kho;
	});

	$(document).keyup(function(e) {
		if ($("input[name='input_search']").is(":focus") && (e.keyCode == 13)) {
			$(".icon_search_equipment_id").click();
		}
	});

	function id_thiet_lap(id) {
		var id = $(id).attr("data");
		var id_kho = $('.popup_add_wh_set').attr("data-kho");
		$.ajax({
			url: '../render/thiet_lap_kho.php',
			type: 'POST',
			data: {
				id: id,
				id_kho: id_kho,
			},
			success: function(data) {
				$(".popup_add_wh_set .content_popup").html(data);
				$('.popup_add_wh_set').show();
			}
		})
	};

	function sua_thiet_lap(_this) {
		var id = $(_this).attr("data-id");
		var id_kho = $('.popup_add_wh_set').attr("data-kho");
		var so_luong_toi_thieu = $("input[name='sltt']").val();
		var so_luong_toi_da = $("input[name='sltd']").val();

		$.ajax({
			url: '../ajax/thiet_lap_ton_kho.php',
			type: 'POST',
			data: {
				id: id,
				id_kho: id_kho,
				so_luong_toi_thieu: so_luong_toi_thieu,
				so_luong_toi_da: so_luong_toi_da,
			},
			success: function(data) {
				if (data != "") {
					$('.popup_set_wh_succ').show();
				} else {
					alert("Fail!!!");
				}
			}
		})
	}
</script>

</html>