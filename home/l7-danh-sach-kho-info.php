<?php include("config.php"); ?>
<!DOCTYPE html>
<html lang="vi">

<head>
	<title>Thông tin kho</title>
	<meta charset="UTF-8">
	<meta name="robots" content="noindex,nofollow" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" type="text/css" href="../css/select2.min.css?v=<?= $ver ?>">
	<link rel="stylesheet" type="text/css" href="../css/style.css?v=<?= $ver ?>">
	<link rel="stylesheet" type="text/css" href="../css/style_h.css?v=<?= $ver ?>">
	<link rel="stylesheet" type="text/css" href="../css/style_n.css?v=<?= $ver ?>">
	<link rel="stylesheet" type="text/css" href="../css/style_k.css?v=<?= $ver ?>">
</head>

<body>
	<div class="main_wrapper_all ">
		<div class="wapper_all">
			<?php include('../includes/sidebar.php');  ?>
		</div>
		<div class="main_overview" id="main_overview">
			<div class="header_menu_overview d_flex align_c space_b">
				<p class="color_grey line_16 font_s14 text_link_page" style="display: block;">
					<a href="/danh-sach-kho.html" class="cursor_p">
						<img src="../images/back_item_g.png" alt="">
					</a>Tồn kho / Danh sách kho
				</p>
				<img class="open_sidebar_w" src="../images/open_sidebar_w.png" alt="" onclick="toggle('main_sidebar')" style="display: none;">
				<?php include('../includes/header.php');  ?>
			</div>
			<div class="main_list_wh">
				<div class="body_equipment_supplies list_wh">
					<div class="head_wh d_flex space_b align_c title_wh" style="display: none;">
						<div class=" d_flex align_c">
							<a href="/danh-sach-kho.html" class="cursor_p">
								<img src="../images/back_item_g.png" alt="">
							</a>
							<p class="color_grey line_16 font_s14 ml_10">
								Tồn kho / Kho ngã tư sở
							</p>
						</div>
					</div>
					<div class="d_flex align_c space_b mb_20 header_list_wh">
						<div class="d_flex align_c option_wh_list">
							<div class="mr_15">
								<select class="all_meterial_wh " name="all_meterial_wh">
									<option></option>
									<option value="k1">Nhóm vật tư 1</option>
									<option value="k2">Nhóm vật tư 1</option>
									<option value="k3">Nhóm vật tư 1</option>
									<option value="k4">Nhóm vật tư 1</option>
									<option value="k5">Nhóm vật tư 1</option>
								</select>
							</div>
							<div class="mr_15">
								<select class="all_manufacturer" name="all_manufacturer">
									<option></option>
									<option value="k1">Tân Á</option>
									<option value="k2">Hòa Phát</option>
									<option value="k3">Hòa Phát</option>
									<option value="k4">Hòa Phát</option>
									<option value="k5">Hòa Phát</option>
									<option value="k6">Hòa Phát</option>
								</select>
							</div>
							<div class="mr_15">
								<select class="all_provenance" name="all_provenance">
									<option></option>
									<option value="k1">Việt Nam</option>
									<option value="k2">Trung Quốc</option>
									<option value="k3">Trung Quốc</option>
									<option value="k4">Trung Quốc</option>
									<option value="k5">Trung Quốc</option>
								</select>
							</div>
							<select class="no_sort_quantily" name="no_sort_quanily">
								<option value="1">Không sắp xếp số lượng</option>
								<option value="2">Số lượng tăng dần</option>
								<option value="2">Số lượng giảm dần</option>
							</select>
						</div>
						<div class="btn_hd">
							<div class="d_flex align_c ">
								<img src="../images/img_hd.png" alt="">
								<p class="padding_l5 color_blue line_18 font_s15 font_w500">Hướng dẫn</p>
							</div>
						</div>
					</div>
					<div class="d_flex align_c space_b mb_20 operation_wh_375">
						<div class="position_r btn_search">
							<input class="search_equipment_id d_flex align_c space_b" type="text" placeholder="Tìm kiếm theo mã, tên vật tư thiết bị">
							<img class="position_a icon_search_equipment_id" src="../images/icon-sr.png" alt="">
						</div>
						<div class="d_flex align_c btn_wh_list_375">
							<div class=" export_wh">
								<button class="btn_ex d_flex align_c cursor_p">
									<img src="../images/export.png" alt="">
									<p class="color_white font_s15 line_h18 font_w500">Xuất excel</p>
								</button>
							</div>
							<div class="btn_hd_375">
								<div class="d_flex align_c ">
									<img src="../images/img_hd.png" alt="">
									<p class="padding_l5 color_blue line_18 font_s15 font_w500">Hướng dẫn</p>
								</div>
							</div>
						</div>
					</div>
					<div class="position_r d_flex align_c">
						<div class="main_table mb_15 table_vt_scr">
							<table class="table table_add_stoke_info" style="width: 1601px;">
								<tr>
									<th>
										STT
										<span class="span_tbody"></span>
									</th>
									<th>Mã vật tư thiết bị
										<span class="span_tbody"></span>
									</th>
									<th>Tên đầy đủ vật tư thiết bị
										<span class="span_tbody"></span>
									</th>
									<th>Nhóm vật tư thiết bị
										<span class="span_tbody"></span>
									</th>
									<th>Số lượng
										<span class="span_tbody"></span>
									</th>
									<th>Đơn vị tính
										<span class="span_tbody"></span>
									</th>
									<th>Hãng sản xuất
										<span class="span_tbody"></span>
									</th>
									<th>Xuất xứ
										<span class="span_tbody"></span>
									</th>
									<th>Chức năng
										<span class="span_tbody"></span>
									</th>
								</tr>
								<tr class="font_s14 line_16 color_grey">
									<td>1</td>
									<td>VT-0000</td>
									<td style="text-align: left;">
										<a class="color_blue line_16 font_s14 font_w500" href="/danh-sach-vat-tu-thiet-bi-detail.html">Ống
											nhựa</a>
									</td>
									<td style="text-align: left;">Nhóm vật tư 1</td>
									<td>1000</td>
									<td>Chai</td>
									<td style="text-align: left;">Việt Hà</td>
									<td>Việt Nam</td>
									<td>
										<div class="d_flex align_c p_set_ft" id="p_set_ft">
											<img src="../images/pen_blu.png" alt="" style="margin-right:0px;">
											<p class="font_s14 line_h16 color_blue font_w500 cursor_p">Thiết lập tồn kho</p>
										</div>
									</td>
								</tr>
								<tr class="font_s14 line_16 color_grey">
									<td>1</td>
									<td>VT-0000</td>
									<td style="text-align: left;">
										<a class="color_blue line_16 font_s14 font_w500" href="">Ống
											nhựa</a>
									</td>
									<td style="text-align: left;">Nhóm vật tư 1</td>
									<td>1000</td>
									<td>Chai</td>
									<td style="text-align: left;">Việt Hà</td>
									<td>Việt Nam</td>
									<td>
										<p class="d_flex align_c">
											<img src="../images/pen_blu.png" alt="" style="margin-right:0px;">
											<a href="" class="font_s14 line_h16 color_blue font_w500" class="a_set_ft">Thiết lập tồn kho</a>
										</p>
									</td>
								</tr>
								<tr class="font_s14 line_16 color_grey">
									<td>1</td>
									<td>VT-0000</td>
									<td style="text-align: left;">
										<a class="color_blue line_16 font_s14 font_w500" href="danh-sach-vat-tu-thiet-bi-detail.html">Ống
											nhựa</a>
									</td>
									<td style="text-align: left;">Nhóm vật tư 1</td>
									<td>1000</td>
									<td>Chai</td>
									<td style="text-align: left;">Việt Hà</td>
									<td>Việt Nam</td>
									<td>
										<p class="d_flex align_c">
											<img src="../images/pen_blu.png" alt="" style="margin-right:0px;">
											<a href="" class="font_s14 line_h16 color_blue font_w500">Thiết lập tồn kho</a>
										</p>
									</td>
								</tr>
							</table>
						</div>
						<div class="pre_q d_flex align_c flex_center position_a">
							<span class="pre_arrow"></span>
						</div>
						<div class="next_q d_flex align_c flex_center position_a">
							<span class="next_arrow"></span>
						</div>
					</div>
				</div>
				<div class="d_flex align_c space_b">
					<div class="d_flex align_c">
						<p class="line_h16 font_s14 color_grey">Hiển thị:</p>
						<div class="d_flex align_c space_b list_choonse_item ml_10 position_r cursor_p">
							<div class="d_flex align_c" onclick="toggle('list_item_choonse_show')">
								<p class="line_h16 font_s14 color_grey">40</p>
								<img class="ml_10" src="../images/list_item.png" alt="">
							</div>
							<div class="list_item_choonse_show position_a" style="display: none;">
								<p class="item_choonse_show text_a_c font_s14 line_16 color_grey">10</p>
								<p class="item_choonse_show text_a_c font_s14 line_16 color_grey">20</p>
								<p class="item_choonse_show text_a_c font_s14 line_16 color_grey">40</p>
								<p class="item_choonse_show text_a_c font_s14 line_16 color_grey">80</p>
								<p class="item_choonse_show text_a_c font_s14 line_16 color_grey">100</p>
							</div>
						</div>
					</div>
					<div class="d_flex align_c">
						<div class="item_pagination cursor_p">
							<img src="../images/back_item_g.png" alt="">
						</div>
						<p class="item_pagination ml_15 font_s13 line_h15 color_grey3 number_item_pagination cursor_p">1</p>
						<p class="item_pagination ml_10 font_s13 line_h15 color_grey3 number_item_pagination cursor_p">2</p>
						<p class="item_pagination ml_10 font_s13 line_h15 color_grey3 number_item_pagination cursor_p">3</p>
						<p class="item_pagination ml_10 font_s13 line_h15 color_grey3 number_item_pagination cursor_p">4</p>
						<p class="item_pagination ml_10 font_s13 line_h15 color_grey3 number_item_pagination cursor_p">5</p>
						<div class="item_pagination  ml_15  next_item_g cursor_p">
							<img src="../images/next_item_g.png" alt="">
						</div>
					</div>
				</div>
				<?php include('../includes/popup_danh-sach-kho.php');  ?>
				<?php include('../includes/popup_overview.php');  ?>
			</div>
		</div>
	</div>

</body>
<script type="text/javascript" src="../js/jquery-3.4.1.min.js"></script>
<script type="text/javascript" src="../js/jquery.validate.min.js"></script>
<script type="text/javascript" src="../js/select2.min.js"></script>
<script type="text/javascript" src="../js/js_h.js"></script>
<script type="text/javascript" src="../js/validate_h.js"></script>
<script type="text/javascript" src="../js/js_n.js"></script>
<script type="text/javascript" src="../js/js_k.js"></script>
<script>
	$('.active7').each(function() {
		if ($(this).hasClass('active7')) {
			$(this).find('a').addClass('active');
		}
	});
</script>

</html>