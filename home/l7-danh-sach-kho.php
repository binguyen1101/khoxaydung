<?php 
	include("config1.php"); 

	if(!in_array(1,$ro_ton_kho)){
		header("Location: /tong-quan.html");
	  }

	isset($_GET['page']) ? $page = $_GET['page'] : $page = 1;
	isset($_GET['input']) ? $ip = $_GET['input'] : $ip = "";
	isset($_GET['dis']) ? $dis = $_GET['dis'] : $dis = 10;

	if(isset($_COOKIE['acc_token']) && isset($_COOKIE['rf_token']) && isset($_COOKIE['role']) && $_COOKIE['role'] == 2){
        $curl = curl_init();
        $token = $_COOKIE['acc_token'];
        curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/list_all_my_partner.php?get_all=true');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer '.$token));
        $response = curl_exec($curl);
        curl_close($curl);

        $data_list = json_decode($response,true);
        $data_list_nv =$data_list['data']['items'];
    }else{
        $curl = curl_init();
        $token = $_COOKIE['acc_token'];
        curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/list_all_employee_of_company.php');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer '.$token));
        $response = curl_exec($curl);
        curl_close($curl);

        $data_list = json_decode($response,true);
        $data_list_nv =$data_list['data']['items'];
    }

	if (isset($_COOKIE['user']) && $_COOKIE['user'] != "" && $_COOKIE['role'] == 1) {
		$token = $_COOKIE['acc_token'];
		$curl = curl_init();
		$data = array();
		curl_setopt($curl, CURLOPT_POST, 1);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
		curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/user_info_company.php');
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));
	
		$response = curl_exec($curl);
		curl_close($curl);
		$data_tt = json_decode($response, true);
		$tt_user = $data_tt['data']['user_info_result'];
	} else if (isset($_COOKIE['user']) && $_COOKIE['user'] != "" && $_COOKIE['role'] == 2) {
		$token = $_COOKIE['acc_token'];
		$curl = curl_init();
		$data = array();
		curl_setopt($curl, CURLOPT_POST, 1);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
		curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/user_info_employee.php');
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));
		$response = curl_exec($curl);
		curl_close($curl);
		$data_tt = json_decode($response, true);
		$tt_user = $data_tt['data']['user_info_result'];
	}
    $id_cty = $tt_user['com_id'];

	// API công trình
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_POST, 1);
    curl_setopt($curl, CURLOPT_URL, 'https://phanmemquanlycongtrinh.timviec365.vn/api/congtrinh.php');
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($curl, CURLOPT_POSTFIELDS, [
        'id_com' => $id_cty,
    ]);
    $response = curl_exec($curl);
    curl_close($curl);
    $data_list = json_decode($response, true);
    $list_phieu_vt = $data_list['data']['items'];
    $list_phieu_vt1 = [];
    for ($i = 0; $i < count($list_phieu_vt); $i++) {
        $ctr = $list_phieu_vt[$i];
        $list_phieu_vt1[$ctr["ctr_id"]] = $ctr;
    }
	
?>
<!DOCTYPE html>
<html lang="vi">

<head>
	<title>Tồn kho</title>
	<meta charset="UTF-8">
	<meta name="robots" content="noindex,nofollow" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" type="text/css" href="../css/select2.min.css?v<=<?= $ver ?>">
	<link rel="stylesheet" type="text/css" href="../css/style.css?v=<?= $ver ?>">
	<link rel="stylesheet" type="text/css" href="../css/style_h.css?v=<?= $ver ?>">
	<link rel="stylesheet" type="text/css" href="../css/style_n.css?v=<?= $ver ?>">
	<link rel="stylesheet" type="text/css" href="../css/style_k.css?v<=<?= $ver ?>">
</head>

<body class="seclec2_radius">
	<div class="main_wrapper_all">
		<div class="wapper_all">
			<?php include('../includes/sidebar.php');  ?>
		</div>
		<div class="main_overview" id="main_overview">
			<div class="header_menu_overview d_flex align_c space_b">
				<p class="color_grey line_16 font_s14 text_link_page" style="display: block;">Tồn kho</p>
				<img class="open_sidebar_w" src="../images/open_sidebar_w.png" alt="" onclick="toggle('main_sidebar')" style="display: none;">
				<?php include('../includes/header.php');  ?>
			</div>
			<div class="wh_main">
				<p class="font_w400 font_s14 line_h17 color_grey title_wh" style="display:none;">Tồn kho</p>
				<div class="d_flex align_c space_b header_kho">
					<div class="search_wh d_flex space_b" style="display: block;">
						<div class="input_sr_wh position_r">
							<input type="text" name="input_search" value="<?= ($ip != "") ? $ip : "" ?>" placeholder="Tìm kiếm theo mã, tên kho">
							<span class="icon_sr_wh"></span>
						</div>
					</div>
					<div class="export_wh d_flex space_b align_c">
	  					<?php if(in_array(2,$ro_ton_kho)){ ?>
							<button class="btn_px d_flex align_c">
								<img src="../images/img_px.png" alt="">
								<p class="color_white font_s15 line_h18 font_w500">Thêm mới</p>
							</button>
						<?php }?>
						<button class="btn_ex d_flex align_c cursor_p">
							<img src="../images/export.png" alt="">
							<p class="color_white font_s15 line_h18 font_w500">Xuất excel</p>
						</button>
						<div class="hd_ex d_flex align_c cursor_p">
							<img src="../images/img_hd.png" alt="">
							<p class="color_blue font_s15 line_h18 font_w500">Hướng dẫn</p>
						</div>
					</div>
				</div>
				<div class="main_dsk" data-idct="<?= $id_cty?>" data-page="<?= $page ?>" data-ip ="<?= $ip ?>" data-dis ="<?= $dis ?>">
					
				</div>
				<?php include('../includes/popup_danh-sach-kho.php');  ?>
			</div>
		</div>
	</div>
	<?php include('../includes/popup_overview.php');  ?>
</body>

<script type="text/javascript" src="../js/jquery-3.4.1.min.js"></script>
<script type="text/javascript" src="../js/jquery.validate.min.js"></script>
<script type="text/javascript" src="../js/select2.min.js"></script>
<script type="text/javascript" src="../js/js_h.js"></script>
<script type="text/javascript" src="../js/validate_h.js"></script>
<script type="text/javascript" src="../js/js_k.js"></script>
<script type="text/javascript" src="../js/js_n.js"></script>
<script>
	// CKEDITOR.replace('editor');
</script>
<script>
	$('.active2').each(function() {
		if ($(this).hasClass('active2')) {
			$(this).parent().addClass('da_show');
		}
	});
	var page = $(".main_dsk").attr("data-page");
	var input_val = $(".main_dsk").attr("data-ip");
	var id_cty = $(".main_dsk").attr("data-idct");
	var curr = $(".main_dsk").attr("data-dis");

	$.ajax({
		url: '../render/list_kho.php',
		type: 'POST',
		data:{
			input_val: input_val,
        	page: page,
			curr: curr
		},
		success: function(data){
			$(".main_dsk").append(data);
		}
	});
	// them moi kho
	$(".wh_main .btn_px").click(function(){
		$('.popup_add_wh').show();
	});
	
	$(".btn_add_kho").click(function(){
		var form = $('.popup_add_wh #f_add_wh');
		form.validate({
			errorPlacement: function(error, element) {
				error.appendTo(element.parents(".popup_add_wh .name_wh"));
				error.appendTo(element.parents(".popup_add_wh .address_wh"));
				error.appendTo(element.parents(".popup_add_wh .staff_wh"));
				error.wrap("<span class='error'>");
    		},
			rules: {
				name_wh: "required",
				address_wh: "required",
				staff_wh: "required",
				},
			messages: {
				name_wh: "Vui lòng nhập tên kho",
				address_wh: "Vui lòng nhập địa chỉ kho",
				staff_wh: "Vui lòng nhập nhân viên quản lí kho",
				},
		});
		if(form.valid()===true){
			var kho_name = $(".popup_add_wh input[name='name_wh']").val();
			var kho_diaChi = $(".popup_add_wh input[name ='address_wh']").val();
			var kho_moTa = $(".popup_add_wh textarea[name='description_wh']").val();
			var kho_congTrinh = $(".popup_add_wh select[name ='wh_construction']").val();
			var kho_staff_mn = $(".popup_add_wh select[name = 'staff_wh']").val();
			var id_cty = <?= $id_cty?>;

			$.ajax({
				url: '../ajax/addnew_kho.php',
				method:'POST',
				data:{
					id_cty: id_cty,
					kho_name:kho_name,
					kho_diaChi:kho_diaChi,
					kho_moTa:kho_moTa,
					kho_congTrinh:kho_congTrinh,
					kho_staff_mn:kho_staff_mn,
				},
				success:function(data){
					if(data == ""){
						$('#popup_add_wh_succ .tenKho').html(kho_name);						
						$('#popup_add_wh_succ').show();
					}else if(data != ""){
						alert(data);
					}
					
				}
			})
		}
	});

	function click_edit(_this){
		var id = $(_this).parent().parent().parent().attr('data-id');
		$('.popup_edit_wh').attr('data-id',id);
		var id_cty = $(".main_dsk").attr("data-idct");
		$.ajax({
			url: '../render/popup_sua_kho.php',
			type: 'POST',
			data:{
				id_cty: id_cty,
				id: id,
			},
			success: function(data){
				$('.popup_edit_wh #f_add_wh').html(data);
				$('.popup_edit_wh').show();
			}
		})
	}

	function xoa(_this){
		var id_kho = $(_this).parent().parent().parent().attr('data-id');
		var id_cty = $(".main_dsk").attr("data-idct");
		var text = "Bạn có chắc muốn xóa kho?";
		if (confirm(text) == true) {
			$.ajax({
				url: '../ajax/xoa_kho.php',
				type: 'POST',
				data:{
					id_cty: id_cty,
					id_kho: id_kho,
				},
				success: function(data){
					if(data == ""){
						alert("Xóa kho thành công!");
						window.location.reload();
					}else{
						alert("Xóa kho thất bại!");
					}
				}
			});
		}
	}

	function sua(){
		var id_kho = $('.popup_edit_wh').attr('data-id');
		var form = $('.popup_edit_wh #f_add_wh');
		form.validate({
			errorPlacement: function(error, element) {
				error.appendTo(element.parents(".popup_edit_wh .name_wh"));
				error.appendTo(element.parents(".popup_edit_wh .address_wh"));
				error.appendTo(element.parents(".popup_edit_wh .staff_wh"));
				error.wrap("<span class='error'>");
    		},
			rules: {
				name_wh: "required",
				address_wh: "required",
				staff_wh: "required",
				},
			messages: {
				name_wh: "Vui lòng nhập tên kho",
				address_wh: "Vui lòng nhập địa chỉ kho",
				staff_wh: "Vui lòng nhập nhân viên quản lí kho",
				},
		});
		if(form.valid()===true){
			var kho_name = $(".popup_edit_wh input[name='name_wh']").val();
			var kho_diaChi = $(".popup_edit_wh input[name ='address_wh']").val();
			var kho_moTa = $(".popup_edit_wh textarea[name='description_wh']").val();
			var kho_congTrinh = $(".popup_edit_wh select[name ='wh_construction']").val();
			var kho_staff_mn = $(".popup_edit_wh select[name = 'staff_wh']").val();
			var id_cty = $(".main_dsk").attr("data-idct");

			$.ajax({
				url: '../ajax/edit_kho.php',
				method:'POST',
				data:{
					id_kho:id_kho,
					id_cty: id_cty,
					kho_name:kho_name,
					kho_diaChi:kho_diaChi,
					kho_moTa:kho_moTa,
					kho_congTrinh:kho_congTrinh,
					kho_staff_mn:kho_staff_mn,
				},
				success:function(data){
					if(data == ""){
						$('#popup_edit_wh_succ .tenKho').html(kho_name);						
						$('#popup_edit_wh_succ').show();
					}else if(data != ""){
						alert(data);
					}
					
				}
			})
		}
	}

	$(".icon_sr_wh").click(function(){
		var input_val = $("input[name='input_search'").val();
		var page = 1;
		var curr = $('.show_tr_tb').val();
		
		if(input_val == "" && curr == 10){
			window.location.href = "/danh-sach-kho.html?dis=" + curr + '&page=' + page;
		}else{
			window.location.href = "/danh-sach-kho.html?input=" + input_val + "&dis=" + curr + '&page=' + page;
		}
	});

	$(document).keyup(function (e) {
		if ($("input[name='input_search']").is(":focus") && (e.keyCode == 13)) {
			$(".icon_sr_wh").click();
		}
	});

	function close_popup(){
		$('.popup_edit_wh').hide();
	}

	$(".btn_ex").click(function() {
		window.location.href = '../Excel/danh_sach_kho.php';
	});

	function display(select){
    var curr = $(select).val();
    var page = 1;
    var input_val = $("input[name='input_search']").val();
    if(input_val == "" && curr == 10){
		window.location.href = "/danh-sach-kho.html?dis=" + curr + '&page=' + page;
	}else{
		window.location.href = "/danh-sach-kho.html?input=" + input_val + "&dis=" + curr + '&page=' + page;
	}
    $("input[name='input_search'").val('');
}

	
	
</script>

</html>