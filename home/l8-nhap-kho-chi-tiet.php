<?php
    include("config1.php");

    if(!in_array(1,$ro_nhap_kho)){
        header("Location: /tong-quan.html");
    }

    $id_phieu = getValue('id', 'int', 'GET', '');


    $chi_tiet_phieunhap = new db_query("SELECT * FROM `kho-cho-xu-li` 
    WHERE`kcxl_soPhieu` = 'PNK' AND `kcxl_id` = $id_phieu AND `kcxl_check`='1'
    ");
    $item = mysql_fetch_assoc($chi_tiet_phieunhap->result);
    $id_dh = $item['kcxl_donHang'];
    $id_pyc = $item['kcxl_phieuYeuCau'];


    $kho_nhap = new db_query("SELECT `kho_name`,`kcxl_ngayNhapKho` FROM `kho-cho-xu-li`
        LEFT JOIN `kho` ON `kho-cho-xu-li`.`kcxl_khoNhap` = `kho`.`kho_id`
        WHERE `kcxl_soPhieu` = 'PNK' AND `kcxl_id` = $id_phieu
        ");
    $nhap = mysql_fetch_assoc($kho_nhap->result);

    $kho_xuat = new db_query("SELECT `kho_name`,`kcxl_ngayXuatKho`
        FROM `kho-cho-xu-li`
        LEFT JOIN `kho` ON `kho-cho-xu-li`.`kcxl_khoXuat` = `kho`.`kho_id`
        WHERE `kcxl_soPhieu` = 'PNK' AND `kcxl_id` = $id_phieu
        ");
    $xuat = mysql_fetch_assoc($kho_xuat->result);

    if(isset($_COOKIE['acc_token']) && isset($_COOKIE['rf_token']) && isset($_COOKIE['role']) && $_COOKIE['role'] == 2){
        $curl = curl_init();
        $token = $_COOKIE['acc_token'];
        curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/list_all_my_partner.php?get_all=true');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer '.$token));
        $response = curl_exec($curl);
        curl_close($curl);

        $data_list = json_decode($response,true);
        $data_list_nv =$data_list['data']['items'];
    }else{
        $curl = curl_init();
        $token = $_COOKIE['acc_token'];
        curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/list_all_employee_of_company.php');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer '.$token));
        $response = curl_exec($curl);
        curl_close($curl);

        $data_list = json_decode($response,true);
        $data_list_nv =$data_list['data']['items'];

    }
    $count = count($data_list_nv);

    $user = [];
    for ($i = 0; $i < count($data_list_nv); $i++){
        $nv = $data_list_nv[$i];
        $user[$nv["ep_id"]] = $nv;
    }

    if (isset($_COOKIE['user']) && $_COOKIE['user'] != "" && $_COOKIE['role'] == 2) {
		$token = $_COOKIE['acc_token'];
		$curl = curl_init();
		$data = array();
		curl_setopt($curl, CURLOPT_POST, 1);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
		curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/user_info_employee.php');
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));
		$response = curl_exec($curl);
		curl_close($curl);
		$data_tt = json_decode($response, true);
		$tt_user = $data_tt['data']['user_info_result'];
        $id_nguoi_lg = $_SESSION['ep_id'];

	}else if (isset($_COOKIE['user']) && $_COOKIE['user'] != "" && $_COOKIE['role'] == 1) {
        $token = $_COOKIE['acc_token'];
        $curl = curl_init();
        $data = array();
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/user_info_company.php');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));
        $response = curl_exec($curl);
        curl_close($curl);
        $data_tt = json_decode($response, true);
        $tt_user = $data_tt['data']['user_info_result'];
        $id_nguoi_lg = 0;
    }

    $id_cty = $tt_user['com_id'];

    // API vật tư đơn hàng
    $curl_vtdh = curl_init();
    curl_setopt($curl_vtdh, CURLOPT_POST, 1);
    curl_setopt($curl_vtdh, CURLOPT_URL, 'https://phanmemquanlycungung.timviec365.vn/api/vat_tu_dh.php');
    curl_setopt($curl_vtdh, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl_vtdh, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($curl_vtdh, CURLOPT_POSTFIELDS, [
        'com_id' => $id_cty,
        'dh_id' => $item['kcxl_donHang'],
    ]);
    $response_vtdh = curl_exec($curl_vtdh);
    curl_close($curl_vtdh);
    $data_list_vtdh = json_decode($response_vtdh, true);
    $list_vtdh = $data_list_vtdh['data']['items'];

    $list_vtdh1 = [];
    for ($j = 0; $j < count($list_vtdh); $j++) {
        $list_vtdh_p = $list_vtdh[$j];
        $list_vtdh1[$list_vtdh_p["id_vat_tu"]] = $list_vtdh_p;
    }

    // API vật tư theo phiếu yêu cầu
    $curl_vt_yc = curl_init();
    curl_setopt($curl_vt_yc, CURLOPT_POST, 1);
    curl_setopt($curl_vt_yc, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl_vt_yc, CURLOPT_URL, "https://phanmemquanlycungung.timviec365.vn/api/vat_tu_yc.php");
    curl_setopt($curl_vt_yc, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($curl_vt_yc, CURLOPT_POSTFIELDS, [
        'id_phieu' => $id_pyc,
    ]);
    $response_vt_yc = curl_exec($curl_vt_yc);
    curl_close($curl_vt_yc);
    $emp_json = json_decode($response_vt_yc, true);
    $emp_arr = $emp_json['data']['items'];
    $emp_vt_yc = [];
    for ($i = 0; $i < count($emp_arr); $i++) {
        $vt_yc = $emp_arr[$i];
        $emp_vt_yc[$vt_yc["id_vat_tu"]] = $vt_yc;
    }

    // API công trình
    $curl_ct = curl_init();
    curl_setopt($curl_ct, CURLOPT_POST, 1);
    curl_setopt($curl_ct, CURLOPT_URL, 'https://phanmemquanlycongtrinh.timviec365.vn/api/congtrinh.php');
    curl_setopt($curl_ct, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl_ct, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($curl_ct, CURLOPT_POSTFIELDS, [
        'id_com' => $id_cty,
    ]);
    $response_ct = curl_exec($curl_ct);
    curl_close($curl_ct);
    $data_list_ct = json_decode($response_ct, true);
    $list_ct = $data_list_ct['data']['items'];
    $list_ct_arr = [];
    for ($i = 0; $i < count($list_ct); $i++) {
        $ctr = $list_ct[$i];
        $list_ct_arr[$ctr["ctr_id"]] = $ctr;
    }

    ?>
<!DOCTYPE html>
<html lang="vi">

<head>
    <title>Chi tiết phiếu nhập kho</title>
    <meta charset="UTF-8">
    <meta name="robots" content="noindex,nofollow" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="../css/select2.min.css?= $ver ?>">
    <link rel="stylesheet" type="text/css" href="../css/style.css?v=<?= $ver ?>">
    <link rel="stylesheet" type="text/css" href="../css/style_h.css?v=<?= $ver ?>">
    <link rel="stylesheet" type="text/css" href="../css/style_n.css?v=<?= $ver ?>">
    <link rel="stylesheet" type="text/css" href="../css/style_k.css?v<=<?= $ver ?>">
</head>

<body class="seclec2_radius">
    <div class="main_wrapper_all">
        <div class="wrapper_all">
            <?php include('../includes/sidebar.php');  ?>
        </div>
        <div class="main_overview import_wh_detail" data-id="<?=$id_phieu?>">
            <div class="header_menu_overview d_flex align_c space_b">
                <p class="color_grey line_16 font_s14 text_link_page align_c" style="display: block;">
                    <a href="/nhap-kho.html" class="cursor_p">
                        <img src="../images/back_item_g.png" alt="">
                    </a>Nhập kho / chi tiết phiếu nhập kho
                </p>
                <img class="open_sidebar_w" src="../images/open_sidebar_w.png" alt="" onclick="toggle('main_sidebar')" style="display: none;">
                <?php include('../includes/header.php');  ?>
            </div>
            <div class="main_import_wh_detail import_wh_detail_1">
                <div class="head_wh d_flex space_b align_c title_wh" style="display: none;">
                    <div class=" d_flex align_c ">
                        <a href="/nhap-kho.html" class="cursor_p">
                            <img src="../images/back_item_g.png" alt="">
                        </a>
                        <p class="color_grey line_16 font_s14 ml_10">
                            Nhập kho / Chi tiết phiếu nhập kho
                        </p>
                    </div>
                </div>
                <div class="operation_wh d_flex space_b flex_end">                    
                        <div class="export_wh d_flex space_b align_c ">
                            <?php if($item['kcxl_trangThai'] == 1 && in_array(5,$ro_nhap_kho)){?>
                            <button class="btn_tc d_flex flex_center align_c">
                                <img src="../images/btn_t.png" alt="">
                                <p class="color_white font_s15 line_h18 font_w500">Từ chối</p>
                            </button>
                            <? }?>
                            <?php if(($item['kcxl_trangThai'] == 1 || $item['kcxl_trangThai'] == 2) && in_array(5,$ro_nhap_kho)){?>
                            <button class="btn_dt d_flex flex_center align_c">
                                <img src="../images/check__w.png" alt="">
                                <p class="color_white font_s15 line_h18 font_w500">Duyệt</p>
                            </button>
                            <? }?>
                            <?php if(($item['kcxl_trangThai'] == 3 && $id_nguoi_lg == $item['kcxl_nguoiNhan']) || ($item['kcxl_trangThai'] == 3 && $_SESSION['quyen'] == '1')){?>
                                <button class="btn_ht d_flex flex_center align_c" style="width:142px;">
                                    <img src="../images/check__w.png" alt="">
                                    <p class="color_white font_s15 line_h18 font_w500">Hoàn thành</p>
                                </button>
                            <?} ?>
                            <?php if(($item['kcxl_trangThai'] == 1 || $item['kcxl_trangThai'] == 2 || $item['kcxl_trangThai'] == 3 ||$item['kcxl_trangThai'] == 4) && in_array(4,$ro_nhap_kho)){?>
                            <button class="btn_del d_flex flex_center align_c">
                                <p class="color_blue font_s15 line_h18 font_w500">Xóa phiếu</p>
                            </button>
                            <? }?>
                            <?php if(($item['kcxl_trangThai'] == 1|| $item['kcxl_trangThai'] == 2) && in_array(3,$ro_nhap_kho)){?>
                                <button class="btn_px d_flex flex_center align_c">
                                    <a href="/nhap-kho-chinh-sua-<?=$id_phieu?>.html"></a>
                                    <img src="../images/edit_w.png" alt="">
                                    <p class="color_white font_s15 line_h18 font_w500">Chỉnh sửa</p>
                                </button>
                            <?php }elseif(($item['kcxl_trangThai'] == 1 || $item['kcxl_trangThai'] == 2 || $item['kcxl_trangThai'] == 3) && in_array(3,$ro_nhap_kho) && $_SESSION['quyen'] == '1'){?>
                            <button class="btn_px d_flex flex_center align_c">
                                <a href="/nhap-kho-chinh-sua-<?=$id_phieu?>.html"></a>
                                <img src="../images/edit_w.png" alt="">
                                <p class="color_white font_s15 line_h18 font_w500">Chỉnh sửa</p>
                            </button>
                            <? }?>
                            <button class="btn_ex d_flex flex_center align_c" data = "<?= $id_phieu ?>">
                                <img src="../images/export.png" alt="">
                                <p class="color_white font_s15 line_h18 font_w500">Xuất excel</p>
                            </button>
                        </div>                    
                </div>

                <!-- thông tin phiếu nhập kho trạng thái chờ duyệt1 -->
                <div class="info_rq info_input_order" style="display : block;">
                    <div class="imp_bill" style="display: block;">
                        <div class="tit_info_rq back_blue">
                            <p class="color_white font_s16 line_h19 font_w700">Thông tin phiếu nhập kho</p>
                        </div>
                        <div class="body_info_rq">
                            <!-- trạng thái chờ duyệt 1 -->

                            <div class="list_info_input_order" style="display:block;">
                                <div class="item_body_info_rq d_flex align_c">
                                    <p class="tit_it color_grey font_s14 line_h17 font_w400">Số phiếu:</p>
                                    <p class="ten_phieu color_grey font_s14 line_h17 font_w500">PNK - <?= $item['kcxl_id'] ?></p>
                                </div>
                                <div class=" imp_wh_bb" style="display:block;">
                                    <div class="item_body_info_rq d_flex align_c">
                                        <p class="tit_it color_grey font_s14 line_h17 font_w400">Hình thức nhập kho:</p>
                                        <p class="color_grey font_s14 line_h17 font_w500">
                                            <?= hinh_thuc_nhap($item['kcxl_hinhThuc'])?>
                                        </p>
                                    </div>
                                </div>
                                <div class="item_body_info_rq d_flex align_c">
                                    <p class="tit_it color_grey font_s14 line_h17 font_w400">Người tạo:</p>
                                    <?php 
                                    $nguoi_tao = $item['kcxl_nguoiTao'];
                                    $user_id = $user[$nguoi_tao];
                                    if($nguoi_tao != 0){
                                        $ten_nguoi_tao = $user_id['ep_name'];
                                        $anh_nguoi_tao = $user_id['ep_image'];
                                        if($anh_nguoi_tao == ""){
                                            $anh0 = '../images/ava_ad.png';
                                        }else{
                                            $anh0 = 'https://chamcong.24hpay.vn/upload/employee/'.$anh_nguoi_tao;
                                        }
                                    }else{
                                        $ten_nguoi_tao = $tt_user['com_name'];
                                        $anh0 = 'https://chamcong.24hpay.vn/upload/company/logo/' . $tt_user['com_logo'];
                                    }
                                    ?>
                                    <div class="d_flex flex_start align_c">
                                        <img src="<?= $anh0; ?>" alt="" class="<?= ($anh0 == 'https://chamcong.24hpay.vn/upload/company/logo/') ? 'display_none' : '' ?>">
                                        <p class="color_grey font_s14 line_h17 font_w500"><?= $ten_nguoi_tao; ?></p>
                                    </div>
                                </div>
                                <div class="item_body_info_rq d_flex align_c">
                                    <p class="tit_it color_grey font_s14 line_h17 font_w400">Ngày tạo:</p>
                                    <p class="color_grey font_s14 line_h17 font_w500"><?= date('d/m/Y', strtotime($item['kcxl_ngayTao'])); ?></p>
                                </div>
                                <div class="imp_wh_bb">
                                    <? if($item['kcxl_hinhThuc']=="NK1"||$item['kcxl_hinhThuc']=="NK2"||$item['kcxl_hinhThuc']=="NK3"||$item['kcxl_hinhThuc']=="NK4"){?>                                    
                                    <div class="item_body_info_rq d_flex align_c ">
                                        <p class="tit_it color_grey font_s14 line_h17 font_w400">
                                            <?php $loaiPhieu = $item['kcxl_hinhThuc'];
                                                switch($loaiPhieu){
                                                    case "NK1":
                                                        $loaiPhieu = "Đơn hàng:";
                                                        echo $loaiPhieu;
                                                        break;
                                                    case "NK2":
                                                        $loaiPhieu = "Phiếu điều chuyển kho:";
                                                        echo $loaiPhieu;
                                                        break;
                                                    case "NK3":
                                                        $loaiPhieu = "Công trình:";
                                                        echo $loaiPhieu;
                                                        break;
                                                    case "NK4":
                                                        $loaiPhieu = "Phiếu yêu cầu:";
                                                        echo $loaiPhieu;
                                                        break;
                                                }
                                            ?>
                                        </p>
                                        <?php if($item['kcxl_hinhThuc']=="NK1") {?>
                                            <p class="color_grey font_s14 line_h17 font_w500">ĐH - <?=$item['kcxl_donHang']?></p>
                                        <?}?>
                                        <?php if($item['kcxl_hinhThuc']=="NK2") {?>
                                            <p class="color_grey font_s14 line_h17 font_w500">ĐCK - <?=$item['kcxl_phieuDieuChuyenKho']?></p>
                                        <?}?>
                                        <?php if($item['kcxl_hinhThuc']=="NK3") {?>
                                            <p class="color_grey font_s14 line_h17 font_w500"><?=$list_ct_arr[$item['kcxl_congTrinh']]['ctr_name']?></p>
                                        <?}?>
                                        <?php if($item['kcxl_hinhThuc']=="NK4") {?>
                                            <p class="color_grey font_s14 line_h17 font_w500">PYC - <?=$item['kcxl_phieuYeuCau']?></p>
                                        <?}?>
                                    </div>
                                    <? }?>                               
                                    <div class="item_body_info_rq d_flex align_c">
                                        <p class="tit_it color_grey font_s14 line_h17 font_w400">Trạng thái:</p>
                                        <p class="<?= trang_thai_color($item['kcxl_trangThai']); ?> font_s14 line_h17 font_w500"><?= trang_thai($item['kcxl_trangThai']); ?></p>
                                    </div>
                                    <?php if ($item['kcxl_trangThai'] == 2) { ?>
                                        <div class="item_body_info_rq d_flex align_c">
                                            <p class="tit_it color_grey font_s14 line_h17 font_w400">Lí do từ chối:</p>
                                            <p class="color_grey font_s14 line_h17 font_w500"><?= $item['kcxl_liDoTuChoi']; ?></p>
                                        </div>
                                    <?php } ?>
                                    <? if ($item['kcxl_hinhThuc'] == 'NK1') { ?>
                                        <div class="item_body_info_rq d_flex align_c imp_wh_bb">
                                            <p class="tit_it color_grey font_s14 line_h17 font_w400">Nhà cung cấp:</p>
                                            <p class="color_grey font_s14 line_h17 font_w500"><?= $item['kcxl_nhaCungCap'] ?></p>
                                        </div>
                                        <div class="item_body_info_rq d_flex align_c">
                                            <p class="tit_it color_grey font_s14 line_h17 font_w400">Người giao hàng:</p>
                                            <p class="color_grey font_s14 line_h17 font_w500"><?= $item['kcxl_nguoiThucHien'] ?></p>
                                        </div>
                                        <?php 
                                            $id_nguoi_nhan = $item['kcxl_nguoiNhan'];
                                            $ten_nguoi_nhan = $user[$id_nguoi_nhan]['ep_name'];
                                            $phong_ban_nguoi_nhan = $user[$id_nguoi_nhan]['dep_name'];
                                            $anh_nguoi_nhan = $user[$id_nguoi_nhan]['ep_image'];
                                            if($anh_nguoi_nhan == ""){
                                                $anh1 = '../images/ava_ad.png';
                                            }else{
                                                $anh1 = 'https://chamcong.24hpay.vn/upload/employee/'.$anh_nguoi_nhan;
                                            }
                                        ?>
                                        <div class="item_body_info_rq d_flex align_c">
                                            <p class="tit_it color_grey font_s14 line_h17 font_w400">Người nhận:</p>
                                            <div class="d_flex flex_start align_c">
                                                <img src="<?= $anh1 ?>" alt="">
                                                <p class="color_grey font_s14 line_h17 font_w500"><?= $ten_nguoi_nhan ?></p>
                                            </div>
                                        </div>
                                                <div class="item_body_info_rq d_flex align_c">
                                                    <p class="tit_it color_grey font_s14 line_h17 font_w400">Phòng ban:</p>
                                                    <p class="color_grey font_s14 line_h17 font_w500"><?= explode('-',$phong_ban_nguoi_nhan)[0]; ?></p>
                                                </div>
                                                <div class="item_body_info_rq d_flex align_c">
                                                    <p class="tit_it color_grey font_s14 line_h17 font_w400 kho_nhap" data-id="<?=$item['kcxl_khoNhap']?>">Kho nhập:</p>
                                                    <p class="color_grey font_s14 line_h17 font_w500"><?= $nhap['kho_name'] ?></p>
                                                </div>
                                                <div class="item_body_info_rq d_flex align_c">
                                                    <p class="tit_it color_grey font_s14 line_h17 font_w400">Ngày nhập kho:</p>
                                                    <p class="color_grey font_s14 line_h17 font_w500"><?= $item['kcxl_ngayNhapKho'] ?></p>
                                                </div>
                                            <? } ?>
                                </div>
                                <?php if($item['kcxl_hinhThuc']=='NK2'){?>
                                    <div class="imp_wh_dc" >
                                        <div class="item_body_info_rq d_flex align_c">
                                            <p class="tit_it color_grey font_s14 line_h17 font_w400">Kho xuất:</p>
                                            <p class="color_grey font_s14 line_h17 font_w500"><?= $xuat['kho_name'] ?></p>
                                        </div>
                                        <div class="item_body_info_rq d_flex align_c ">
                                            <p class="tit_it color_grey font_s14 line_h17 font_w400 kho_nhap" data-id="<?=$item['kcxl_khoNhap']?>">Kho nhập:</p>
                                            <p class="color_grey font_s14 line_h17 font_w500"><?= $nhap['kho_name'] ?></p>
                                        </div>
                                        <?php 
                                            $id_nguoi_giao = $item['kcxl_nguoiThucHien'];
                                            $ten_nguoi_giao = $user[$id_nguoi_giao]['ep_name'];
                                            $phong_ban_nguoi_giao = $user[$id_nguoi_giao]['dep_name'];
                                            $anh_nguoi_giao = $user[$id_nguoi_giao]['ep_image'];
                                            if($anh_nguoi_giao == ""){
                                                $anh = '../images/ava_ad.png';
                                            }else{
                                                $anh = 'https://chamcong.24hpay.vn/upload/employee/'.$anh_nguoi_giao;
                                            }
                                        ?>
                                            <div class="item_body_info_rq d_flex align_c">
                                                <p class="tit_it color_grey font_s14 line_h17 font_w400">Người giao:</p>
                                                <div class="d_flex flex_start align_c">
                                                    <img src="<?= $anh ?>" alt="">
                                                    <p class="color_grey font_s14 line_h17 font_w500"><?= $ten_nguoi_giao ?></p>
                                                </div>
                                            </div>                                    
                                            <div class="item_ct_info_rq d_flex align_c">
                                                <p class="tit_it color_grey font_s14 line_h17 font_w400">Phòng ban:</p>
                                                <p class="color_grey font_s14 line_h17 font_w500"><?= explode('-',$phong_ban_nguoi_giao)[0]; ?></p>
                                            </div>
                                        <?php 
                                            $id_nguoi_nhan = $item['kcxl_nguoiNhan'];
                                            $ten_nguoi_nhan = $user[$id_nguoi_nhan]['ep_name'];
                                            $phong_ban_nguoi_nhan = $user[$id_nguoi_nhan]['dep_name'];
                                            $anh_nguoi_nhan = $user[$id_nguoi_nhan]['ep_image'];
                                            if($anh_nguoi_nhan == ""){
                                                $anh1 = '../images/ava_ad.png';
                                            }else{
                                                $anh1 = 'https://chamcong.24hpay.vn/upload/employee/'.$anh_nguoi_nhan;
                                            }
                                        ?>
                                            <div class="item_body_info_rq d_flex align_c">
                                                <p class="tit_it color_grey font_s14 line_h17 font_w400">Người nhận:</p>
                                                <div class="d_flex flex_start align_c">
                                                    <img src="<?= $anh1 ?>" alt="">
                                                    <p class="color_grey font_s14 line_h17 font_w500"><?= $ten_nguoi_nhan ?></p>
                                                </div>
                                            </div>                                    
                                            <div class="item_ct_info_rq d_flex align_c">
                                                <p class="tit_it color_grey font_s14 line_h17 font_w400">Phòng ban:</p>
                                                <p class="color_grey font_s14 line_h17 font_w500"><?= explode('-',$phong_ban_nguoi_nhan)[0]; ?></p>
                                            </div>
                                        <div class="item_body_info_rq d_flex align_c">
                                            <p class="tit_it color_grey font_s14 line_h17 font_w400">Ngày yêu cầu hoàn thành:</p>
                                            <p class="color_grey font_s14 line_h17 font_w500"><?=$item['kcxl_ngayYeuCauHoanThanh']?></p>
                                        </div>
                                        <div class="item_body_info_rq d_flex align_c">
                                            <p class="tit_it color_grey font_s14 line_h17 font_w400">Ngày nhập kho:</p>
                                            <p class="color_grey font_s14 line_h17 font_w500"><?=$item['kcxl_ngayNhapKho']?></p>
                                        </div>
                                    </div> 
                                <?php }?>
                                <?php if($item['kcxl_hinhThuc']=='NK3') {?>                           
                                    <div class="imp_wh_back" >                                    
                                        <?php 
                                            $id_nguoi_giao = $item['kcxl_nguoiThucHien'];
                                            $ten_nguoi_giao = $user[$id_nguoi_giao]['ep_name'];
                                            $phong_ban_nguoi_giao = $user[$id_nguoi_giao]['dep_name'];
                                            $anh_nguoi_giao = $user[$id_nguoi_giao]['ep_image'];
                                            if($anh_nguoi_giao == ""){
                                                $anh = '../images/ava_ad.png';
                                            }else{
                                                $anh = 'https://chamcong.24hpay.vn/upload/employee/'.$anh_nguoi_giao;
                                            }
                                        ?>
                                            <div class="item_body_info_rq d_flex align_c">
                                                <p class="tit_it color_grey font_s14 line_h17 font_w400">Người giao:</p>
                                                <div class="d_flex flex_start align_c">
                                                    <img src="<?= $anh ?>" alt="">
                                                    <p class="color_grey font_s14 line_h17 font_w500"><?= $ten_nguoi_giao ?></p>
                                                </div>
                                            </div>                                    
                                            <div class="item_ct_info_rq d_flex align_c">
                                                <p class="tit_it color_grey font_s14 line_h17 font_w400">Phòng ban:</p>
                                                <p class="color_grey font_s14 line_h17 font_w500"><?= explode('-',$phong_ban_nguoi_giao)[0]; ?></p>
                                            </div>
                                            <?php 
                                                $id_nguoi_nhan = $item['kcxl_nguoiNhan'];
                                                $ten_nguoi_nhan = $user[$id_nguoi_nhan]['ep_name'];
                                                $phong_ban_nguoi_nhan = $user[$id_nguoi_nhan]['dep_name'];
                                                $anh_nguoi_nhan = $user[$id_nguoi_nhan]['ep_image'];
                                                if($anh_nguoi_nhan == ""){
                                                    $anh1 = '../images/ava_ad.png';
                                                }else{
                                                    $anh1 = 'https://chamcong.24hpay.vn/upload/employee/'.$anh_nguoi_nhan;
                                                }
                                            ?>
                                            <div class="item_body_info_rq d_flex align_c">
                                                <p class="tit_it color_grey font_s14 line_h17 font_w400">Người nhận:</p>
                                                <div class="d_flex flex_start align_c">
                                                    <img src="<?= $anh1 ?>" alt="">
                                                    <p class="color_grey font_s14 line_h17 font_w500"><?= $ten_nguoi_nhan ?></p>
                                                </div>
                                            </div>                                    
                                            <div class="item_ct_info_rq d_flex align_c">
                                                <p class="tit_it color_grey font_s14 line_h17 font_w400">Phòng ban:</p>
                                                <p class="color_grey font_s14 line_h17 font_w500"><?= explode('-',$phong_ban_nguoi_nhan)[0]; ?></p>
                                            </div>
                                        <div class="item_body_info_rq d_flex align_c">
                                            <p class="tit_it color_grey font_s14 line_h17 font_w400 kho_nhap" data-id="<?=$item['kcxl_khoNhap']?>">Kho nhập:</p>
                                            <p class="color_grey font_s14 line_h17 font_w500"><?= $nhap['kho_name'] ?></p>
                                        </div>
                                        <div class="item_body_info_rq d_flex align_c">
                                            <p class="tit_it color_grey font_s14 line_h17 font_w400">Ngày nhập kho:</p>
                                            <p class="color_grey font_s14 line_h17 font_w500"><?= $item['kcxl_ngayNhapKho'] ?></p>
                                        </div>
                                    </div>                                
                                <? }?>
                                <?php if($item['kcxl_hinhThuc']=='NK4'){?>
                                    <div class="imp_wh_supply" >                                    
                                    <?php 
                                        $id_nguoi_giao = $item['kcxl_nguoiThucHien'];
                                        $ten_nguoi_giao = $user[$id_nguoi_giao]['ep_name'];
                                        $phong_ban_nguoi_giao = $user[$id_nguoi_giao]['dep_name'];
                                        $anh_nguoi_giao = $user[$id_nguoi_giao]['ep_image'];
                                        if($anh_nguoi_giao == ""){
                                            $anh = '../images/ava_ad.png';
                                        }else{
                                            $anh = 'https://chamcong.24hpay.vn/upload/employee/'.$anh_nguoi_giao;
                                        }
                                    ?>
                                        <div class="item_body_info_rq d_flex align_c">
                                            <p class="tit_it color_grey font_s14 line_h17 font_w400">Người giao:</p>
                                            <div class="d_flex flex_start align_c">
                                                <img src="<?= $anh ?>" alt="">
                                                <p class="color_grey font_s14 line_h17 font_w500"><?= $ten_nguoi_giao ?></p>
                                            </div>
                                        </div>                                    
                                        <div class="item_ct_info_rq d_flex align_c">
                                            <p class="tit_it color_grey font_s14 line_h17 font_w400">Phòng ban:</p>
                                            <p class="color_grey font_s14 line_h17 font_w500"><?= explode('-',$phong_ban_nguoi_giao)[0]; ?></p>
                                        </div>
                                        <?php 
                                            $id_nguoi_nhan = $item['kcxl_nguoiNhan'];
                                            if($nguoi_tao != 0){
                                                $ten_nguoi_nhan = $user[$id_nguoi_nhan]['ep_name'];
                                                $phong_ban_nguoi_nhan = $user[$id_nguoi_nhan]['dep_name'];
                                                $anh_nguoi_nhan = $user[$id_nguoi_nhan]['ep_image'];
                                                if($anh_nguoi_nhan == ""){
                                                    $anh1 = '../images/ava_ad.png';
                                                }else{
                                                    $anh1 = 'https://chamcong.24hpay.vn/upload/employee/'.$anh_nguoi_nhan;
                                                }
                                            }else{
                                                $ten_nguoi_nhan = $tt_user['com_name'];
                                                $anh1 = 'https://chamcong.24hpay.vn/upload/company/logo/' . $tt_user['com_logo'];
                                            }
                                        ?>
                                        <div class="item_body_info_rq d_flex align_c">
                                            <p class="tit_it color_grey font_s14 line_h17 font_w400">Người nhận:</p>
                                            <div class="d_flex flex_start align_c">
                                                <img src="<?= $anh1 ?>" alt="" class="<?= ($anh1 == 'https://chamcong.24hpay.vn/upload/company/logo/') ? 'display_none' : '' ?>">
                                                <p class="color_grey font_s14 line_h17 font_w500"><?= $ten_nguoi_nhan ?></p>
                                            </div>
                                        </div>                                    
                                        <div class="item_ct_info_rq d_flex align_c">
                                            <p class="tit_it color_grey font_s14 line_h17 font_w400">Phòng ban:</p>
                                            <p class="color_grey font_s14 line_h17 font_w500"><?= explode('-',$phong_ban_nguoi_nhan)[0]; ?></p>
                                        </div>
                                    <div class="item_body_info_rq d_flex align_c">
                                        <p class="tit_it color_grey font_s14 line_h17 font_w400 kho_nhap" data-id="<?=$item['kcxl_khoNhap']?>">Kho nhập:</p>
                                        <p class="color_grey font_s14 line_h17 font_w500"><?= $nhap['kho_name'] ?></p>
                                    </div>
                                    <div class="item_body_info_rq d_flex align_c">
                                        <p class="tit_it color_grey font_s14 line_h17 font_w400">Ngày nhập kho:</p>
                                        <p class="color_grey font_s14 line_h17 font_w500"><?= $item['kcxl_ngayNhapKho'] ?></p>
                                    </div>
                                </div>
                                <? } ?>
                                <?php if($item['kcxl_hinhThuc']=='NK5'){?>
                                    <div class="imp_wh_df" >
                                        <div class="item_body_info_rq d_flex align_c">
                                            <p class="tit_it color_grey font_s14 line_h17 font_w400">Người giao:</p>
                                            <div class="d_flex flex_start align_c">
                                                <p class="color_grey font_s14 line_h17 font_w500"><?= $item['kcxl_nguoiThucHien']; ?></p>
                                            </div>
                                        </div>              
                                            <?php 
                                                $id_nguoi_nhan = $item['kcxl_nguoiNhan'];
                                                $ten_nguoi_nhan = $user[$id_nguoi_nhan]['ep_name'];
                                                $phong_ban_nguoi_nhan = $user[$id_nguoi_nhan]['dep_name'];
                                                $anh_nguoi_nhan = $user[$id_nguoi_nhan]['ep_image'];
                                                if($anh_nguoi_nhan == ""){
                                                    $anh1 = '../images/ava_ad.png';
                                                }else{
                                                    $anh1 = 'https://chamcong.24hpay.vn/upload/employee/'.$anh_nguoi_nhan;
                                                }
                                            ?>
                                            <div class="item_body_info_rq d_flex align_c">
                                                <p class="tit_it color_grey font_s14 line_h17 font_w400">Người nhận hàng:</p>
                                                <div class="d_flex flex_start align_c">
                                                    <img src="<?= $anh1 ?>" alt="">
                                                    <p class="color_grey font_s14 line_h17 font_w500"><?= $ten_nguoi_nhan ?></p>
                                                </div>
                                            </div>                                    
                                            <div class="item_ct_info_rq d_flex align_c">
                                                <p class="tit_it color_grey font_s14 line_h17 font_w400">Phòng ban:</p>
                                                <p class="color_grey font_s14 line_h17 font_w500"><?= explode('-',$phong_ban_nguoi_nhan)[0]; ?></p>
                                            </div>
                                        <div class="item_body_info_rq d_flex align_c">
                                            <p class="tit_it color_grey font_s14 line_h17 font_w400 kho_nhap" data-id="<?=$item['kcxl_khoNhap']?>">Kho nhập:</p>
                                            <p class="color_grey font_s14 line_h17 font_w500"><?= $nhap['kho_name'] ?></p>
                                        </div>
                                        <div class="item_body_info_rq d_flex align_c">
                                            <p class="tit_it color_grey font_s14 line_h17 font_w400">Ngày nhập kho:</p>
                                            <p class="color_grey font_s14 line_h17 font_w500"><?= $item['kcxl_ngayNhapKho'] ?></p>
                                        </div>
                                    </div>
                                <? }?>
                                <div class="item_ct_info_rq d_flex align_s">
                                    <p class="tit_it color_grey font_s14 line_h17 font_w400">Ghi chú:</p>
                                    <p class="color_grey font_s14 line_h17 font_w500"><?= $item['kcxl_ghi_chu']; ?></p>
                                </div>
                                <?php if($item['kcxl_trangThai'] == 4){?>
                                    <div>
                                        <?php 
                                            $id_nguoi_duyet = $item['kcxl_nguoiDuyet'];
                                            if($id_nguoi_duyet != 0){
                                                $ten_nguoi_duyet = $user[$id_nguoi_duyet]['ep_name'];
                                                $phong_ban_nguoi_duyet = $user[$id_nguoi_duyet]['dep_name'];
                                                $anh_nguoi_duyet = $user[$id_nguoi_duyet]['ep_image'];
                                                if($anh_nguoi_duyet == ""){
                                                    $anh2 = '../images/ava_ad.png';
                                                }else{
                                                    $anh2 = 'https://chamcong.24hpay.vn/upload/employee/'.$anh_nguoi_duyet;
                                                }
                                            }else{
                                                $ten_nguoi_duyet = $tt_user['com_name'];
                                                $anh2 = 'https://chamcong.24hpay.vn/upload/company/logo/' . $tt_user['com_logo'];
                                            }
                                        ?>
                                        <div class="item_body_info_rq d_flex align_c">
                                            <p class="tit_it color_grey font_s14 line_h17 font_w400">Người duyệt:</p>
                                            <div class="d_flex flex_start align_c">
                                                <img src="<?= $anh2 ?>" alt="" class="<?= ($anh2 == 'https://chamcong.24hpay.vn/upload/company/logo/') ? 'display_none' : '' ?>">
                                                <p class="color_grey font_s14 line_h17 font_w500"><?= $ten_nguoi_duyet ?></p>
                                            </div>
                                            </div>
                                            <div class="item_body_info_rq d_flex align_c">
                                                <p class="tit_it color_grey font_s14 line_h17 font_w400">Ngày duyệt:</p>
                                                <p class="color_grey font_s14 line_h17 font_w500"><?= (strtotime($item['kcxl_ngayDuyet']) > 0) ? date('d/m/Y',strtotime($item['kcxl_ngayDuyet'])) : "" ?></p>
                                            </div>
                                        </div>                    
                                    </div>
                                    <div>
                                        <?php 
                                            $id_nguoi_ht = $item['kcxl_nguoiHoanThanh'];
                                            if($id_nguoi_ht != 0){
                                                $ten_nguoi_ht = $user[$id_nguoi_ht]['ep_name'];
                                                $phong_ban_nguoi_ht = $user[$id_nguoi_ht]['dep_name'];
                                                $anh_nguoi_ht = $user[$id_nguoi_ht]['ep_image'];
                                                if($anh_nguoi_ht == ""){
                                                    $anh3 = '../images/ava_ad.png';
                                                }else{
                                                    $anh3 = 'https://chamcong.24hpay.vn/upload/employee/'.$anh_nguoi_ht;
                                                }
                                            }else{
                                                $ten_nguoi_ht = $tt_user['com_name'];
                                                $anh3 = 'https://chamcong.24hpay.vn/upload/company/logo/' . $tt_user['com_logo'];
                                            }
                                        ?>
                                        <div class="item_body_info_rq d_flex align_c">
                                            <p class="tit_it color_grey font_s14 line_h17 font_w400">Người hoàn thành:</p>
                                            <div class="d_flex flex_start align_c">
                                                <img src="<?=$anh3?>" alt="" class="<?= ($anh3 == 'https://chamcong.24hpay.vn/upload/company/logo/') ? 'display_none' : '' ?>">
                                                <p class="color_grey font_s14 line_h17 font_w500"><?=$ten_nguoi_ht?></p>
                                            </div>
                                        </div>
                                        <div class="item_body_info_rq d_flex align_c">
                                            <p class="tit_it color_grey font_s14 line_h17 font_w400">Ngày hoàn thành:</p>
                                            <p class="color_grey font_s14 line_h17 font_w500"><?= (strtotime($item['kcxl_ngayHoanThanh']) > 0) ? date('d/m/Y',strtotime($item['kcxl_ngayHoanThanh'])) : "" ?></p>
                                        </div>
                                    </div>
                                <?}?>
                                <?php if($item['kcxl_trangThai'] == 3){?>
                                    <div>
                                        <?php 
                                            $id_nguoi_duyet = $item['kcxl_nguoiDuyet'];
                                            if($id_nguoi_duyet != 0){
                                                $ten_nguoi_duyet = $user[$id_nguoi_duyet]['ep_name'];
                                                $phong_ban_nguoi_duyet = $user[$id_nguoi_duyet]['dep_name'];
                                                $anh_nguoi_duyet = $user[$id_nguoi_duyet]['ep_image'];
                                                if($anh_nguoi_duyet == ""){
                                                    $anh2 = '../images/ava_ad.png';
                                                }else{
                                                    $anh2 = 'https://chamcong.24hpay.vn/upload/employee/'.$anh_nguoi_duyet;
                                                }
                                            }else{
                                                $ten_nguoi_duyet = $tt_user['com_name'];
                                                $anh2 = 'https://chamcong.24hpay.vn/upload/company/logo/' . $tt_user['com_logo'];
                                            }
                                        ?>
                                        <div class="item_body_info_rq d_flex align_c">
                                            <p class="tit_it color_grey font_s14 line_h17 font_w400">Người duyệt:</p>
                                            <div class="d_flex flex_start align_c">
                                                <img src="<?= $anh2 ?>" alt="" class="<?= ($anh2 == 'https://chamcong.24hpay.vn/upload/company/logo/') ? 'display_none' : '' ?>">
                                                <p class="color_grey font_s14 line_h17 font_w500"><?= $ten_nguoi_duyet ?></p>
                                            </div>
                                        </div>
                                        <div class="item_body_info_rq d_flex align_c">
                                            <p class="tit_it color_grey font_s14 line_h17 font_w400">Ngày duyệt:</p>
                                            <p class="color_grey font_s14 line_h17 font_w500"><?= (strtotime($item['kcxl_ngayDuyet']) > 0) ? date('d/m/Y',strtotime($item['kcxl_ngayDuyet'])) : "" ?></p>
                                        </div>
                                    </div>                                    
                                <?}?>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- bảng danh sách vật tư -->
                <p class="tit_table_vt color_blue font_s16 line_h19 font_w700"> Danh sách vật tư</p>
                <!-- bảng bb -->
                <?php if($item['kcxl_hinhThuc']== 'NK1'){?>
                    <?php  $vattu_nhap1 = new db_query("SELECT `dsvt_id`,`slvt_idPhieu`, `slvt_maPhieu`, 
                        `dsvt_maVatTuThietBi`, `dsvt_name`, `dvt_name`, `hsx_name`, `xx_name`, `slvt_soLuongNhapKho`,`dsvt_donGia`
                        FROM `danh-sach-vat-tu`
                        LEFT JOIN `don-vi-tinh` ON `dsvt_donViTinh` =`dvt_id`
                        LEFT JOIN `hang-san-xuat` ON `dsvt_hangSanXuat` = `hsx_id`
                        LEFT JOIN `xuat-xu` ON `dsvt_xuatXu` = `xx_id`
                        LEFT JOIN `so-luong-vat-tu` ON `dsvt_id` = `slvt_maVatTuThietBi`
                        LEFT JOIN `kho-cho-xu-li` ON `slvt_idPhieu` = `kcxl_id`
                        WHERE `dsvt_check` = '1' AND `slvt_idPhieu` = '$id_phieu' AND `dsvt_id_ct` = $id_cty
                        ORDER BY `dsvt_id` ASC "); 
                        $st =1;
                    ?>
                    <div class="imp_wh_bb" >
                        <div class="position_r d_flex align_c">
                            <div class="main_table table_vt_scr">
                                <table class="table table_list_meterial_input_info" style="width:1689px;">
                                    <tr>
                                        <th>STT
                                            <span class="span_thread"></span>
                                        </th>
                                        <th>Mã vật tư
                                            <span class="span_thread"></span>
                                        </th>
                                        <th>Tên đầy đủ vật tư thiết bị
                                            <span class="span_thread"></span>
                                        </th>
                                        <th>Đơn vị tính
                                            <span class="span_thread"></span>
                                        </th>
                                        <th> Hãng sản xuất
                                            <span class="span_thread"></span>
                                        </th>
                                        <th> Xuất xứ
                                            <span class="span_thread"></span>
                                        </th>
                                        <th> Số lượng theo đơn hàng
                                            <span class="span_thread"></span>
                                        </th>
                                        <th> Số lượng thực tế nhập kho
                                            <span class="span_thread"></span>
                                        </th>
                                        <th>Đơn giá (VNĐ)
                                            <span class="span_thread"></span>
                                        </th>
                                        <th>Thành tiền (VNĐ)
                                            <span class="span_thread"></span>
                                        </th>
                                    </tr>
                                    <? while($vt_nhap1 = mysql_fetch_assoc($vattu_nhap1 -> result)){?>
                                    <tr class="color_grey font_s14 line_h17 font_w400" data-id="<?=$vt_nhap1['dsvt_id']?>">
                                        <td><?= $st++ ?></td>
                                        <td>VT - <?= $vt_nhap1['dsvt_id'] ?></td>
                                        <td class="color_blue font_s14 line_h17 font_w500" style="text-align: left;"><?= $vt_nhap1['dsvt_name']; ?></td>
                                        <td><?= $vt_nhap1['dvt_name']; ?></td>
                                        <td><?= $vt_nhap1['hsx_name']; ?></td>
                                        <td><?= $vt_nhap1['xx_name']; ?></td>
                                        <?php $id_vtu = $vt_nhap1['dsvt_id'];?>
                                        <td><?= $list_vtdh1[$id_vtu]['so_luong_ky_nay']?></td>
                                        <td class="sl_nhap"><?= $vt_nhap1['slvt_soLuongNhapKho'] ?></td>
                                        <td><?= number_format($vt_nhap1['dsvt_donGia'],0,'','.'); ?></td>
                                        <td><?= number_format($vt_nhap1['dsvt_donGia']*$vt_nhap1['slvt_soLuongNhapKho'],0,'','.')?></td>
                                    </tr>
                                    <?}?>
                                </table>
                            </div>
                            <div class="pre_q d_flex align_c flex_center position_a">
                                <span class="pre_arrow"></span>
                            </div>
                            <div class="next_q d_flex align_c flex_center position_a">
                                <span class="next_arrow"></span>
                            </div>
                        </div>
                    </div>
                <?}?>
                <!-- bảng nhập -->
                <?php if($item['kcxl_hinhThuc']=='NK2'){?>
                    <?php 
                        $check_dc =  $item['kcxl_phieuDieuChuyenKho'];
                        $vattu_nhap2 = new db_query("SELECT `dsvt_id`,`slvt_idPhieu`, `slvt_maPhieu`, `slvt_checkDieuChuyen`,
                        `dsvt_maVatTuThietBi`, `dsvt_name`, `dvt_name`, `hsx_name`, `xx_name`, `slvt_soLuongNhapKho`,`dsvt_donGia`,`slvt_soLuongDieuChuyen`
                        FROM `danh-sach-vat-tu`
                        LEFT JOIN `don-vi-tinh` ON `dsvt_donViTinh` =`dvt_id`
                        LEFT JOIN `hang-san-xuat` ON `dsvt_hangSanXuat` = `hsx_id`
                        LEFT JOIN `xuat-xu` ON `dsvt_xuatXu` = `xx_id`
                        LEFT JOIN `so-luong-vat-tu` ON `dsvt_id` = `slvt_maVatTuThietBi`
                        LEFT JOIN `kho-cho-xu-li` ON `slvt_idPhieu` = `kcxl_id`
                        WHERE `dsvt_check` = '1' AND `slvt_idPhieu` = '$id_phieu' AND `slvt_checkDieuChuyen` = '$check_dc' AND `dsvt_id_ct` = $id_cty
                        ORDER BY `dsvt_id` ASC"); 
                        $st = 1;
                    ?>
                <div class="imp_wh_dc" >
                    <div class="position_r align_c d_flex">
                        <div class="main_table table_vt_scr">
                            <table class="table table_list_meterial-2" style="width: 1672px;">
                                <tr>
                                    <th>STT
                                        <span class="span_thread"></span>
                                    </th>
                                    <th>Mã vật tư
                                        <span class="span_thread"></span>
                                    </th>
                                    <th>Tên đầy đủ vật tư thiết bị
                                        <span class="span_thread"></span>
                                    </th>
                                    <th>Đơn vị tính
                                        <span class="span_thread"></span>
                                    </th>
                                    <th> Hãng sản xuất
                                        <span class="span_thread"></span>
                                    </th>
                                    <th> Xuất xứ
                                        <span class="span_thread"></span>
                                    </th>
                                    <th> Số lượng điều chuyển
                                        <span class="span_thread"></span>
                                    </th>
                                    <th> Số lượng thực tế nhập kho
                                        <span class="span_thread"></span>
                                    </th>
                                    <th>Đơn giá (VNĐ)
                                        <span class="span_thread"></span>
                                    </th>
                                    <th>Thành tiền (VNĐ)
                                        <span class="span_thread"></span>
                                    </th>
                                </tr>
                                <? while($vt_nhap2 = mysql_fetch_assoc($vattu_nhap2 -> result)){?>
                                <tr class="color_grey font_s14 line_h17 font_w400" data-id="<?=$vt_nhap2['dsvt_id']?>">
                                    <td><?= $st++; ?></td>
                                    <td>VT - <?= $vt_nhap2['dsvt_id'] ?></td>
                                    <td class="color_blue font_s14 line_h17 font_w500"><?= $vt_nhap2['dsvt_name']; ?></td>
                                    <td><?= $vt_nhap2['dvt_name']; ?></td>
                                    <td><?= $vt_nhap2['hsx_name']; ?></td>
                                    <td><?= $vt_nhap2['xx_name']; ?></td>
                                    <?php
                                        $idvt_dc = $vt_nhap2['dsvt_id'];
                                        $sl_dc = mysql_fetch_assoc((new db_query("SELECT `slvt_soLuongDieuChuyen` FROM `danh-sach-vat-tu`
                                        LEFT JOIN `so-luong-vat-tu` ON `dsvt_id` = `slvt_maVatTuThietBi`
                                        LEFT JOIN `kho-cho-xu-li` ON `slvt_idPhieu` = `kcxl_id`
                                        WHERE `kcxl_check` = '1' AND `slvt_idPhieu` = '$check_dc' AND `dsvt_id` = '$idvt_dc' AND `kcxl_id_ct` = $id_cty
                                        "))->result)['slvt_soLuongDieuChuyen'];
                                    ?>
                                    <td style="background: #FFFFFF;"><?= $sl_dc ?></td>
                                    <td class="sl_nhap"><?= $vt_nhap2['slvt_soLuongNhapKho']; ?></td>
                                    <td><?= number_format($vt_nhap2['dsvt_donGia'],0,'','.'); ?></td>
                                    <td><?= number_format($vt_nhap2['dsvt_donGia'] * $vt_nhap2['slvt_soLuongNhapKho'],0,'','.')?></td>
                                </tr>
                                <? }?>
                            </table>
                        </div>
                        <div class="pre_q d_flex align_c flex_center position_a">
                            <span class="pre_arrow"></span>
                        </div>
                        <div class="next_q d_flex align_c flex_center position_a">
                            <span class="next_arrow"></span>
                        </div>
                    </div>
                </div>
                <? } ?>
                <!-- bảng trả lại từ thi công -->
                <?php if($item['kcxl_hinhThuc']=='NK3'){?>
                <?php $vattu_nhap3 = new db_query("SELECT `dsvt_id`,`slvt_idPhieu`, `slvt_maPhieu`, 
                        `dsvt_maVatTuThietBi`, `dsvt_name`, `dvt_name`, `hsx_name`, `xx_name`, `slvt_soLuongNhapKho`,`dsvt_donGia` FROM `danh-sach-vat-tu`
                        LEFT JOIN `don-vi-tinh` ON `dsvt_donViTinh` =`dvt_id`
                        LEFT JOIN `hang-san-xuat` ON `dsvt_hangSanXuat` = `hsx_id`
                        LEFT JOIN `xuat-xu` ON `dsvt_xuatXu` = `xx_id`
                        LEFT JOIN `so-luong-vat-tu` ON `dsvt_id` = `slvt_maVatTuThietBi`
                        LEFT JOIN `kho-cho-xu-li` ON `slvt_idPhieu` = `kcxl_id`
                        WHERE `dsvt_check` = '1' AND `slvt_idPhieu` = '$id_phieu' AND `dsvt_id_ct` = $id_cty
                        ORDER BY `dsvt_id` ASC ");
                        $stt = 1;
                ?>
                <div class="imp_wh_back" >
                    <div class="position_r align_c d_flex">
                        <div class="main_table table_vt_scr">
                            <table class="table table_list_meterial-3" style="width: 1432px;">
                                <tr>
                                    <th>STT
                                        <span class="span_thread"></span>
                                    </th>
                                    <th>Mã vật tư
                                        <span class="span_thread"></span>
                                    </th>
                                    <th>Tên đầy đủ vật tư thiết bị
                                        <span class="span_thread"></span>
                                    </th>
                                    <th>Đơn vị tính
                                        <span class="span_thread"></span>
                                    </th>
                                    <th> Hãng sản xuất
                                        <span class="span_thread"></span>
                                    </th>
                                    <th> Xuất xứ
                                        <span class="span_thread"></span>
                                    </th>
                                    <th> Số lượng nhập kho
                                        <span class="span_thread"></span>
                                    </th>
                                    <th>Đơn giá (VNĐ)
                                        <span class="span_thread"></span>
                                    </th>
                                    <th>Thành tiền (VNĐ)
                                        <span class="span_thread"></span>
                                    </th>
                                </tr>
                                <?php while($vt_nhap3 = mysql_fetch_assoc($vattu_nhap3 -> result)){?>
                                <tr class="color_grey font_s14 line_h17 font_w400" data-id="<?=$vt_nhap3['dsvt_id']?>">
                                    <td>
                                        <?= $stt++ ?>
                                    </td>
                                    <td class="color_grey font_s14 line_h17 font_w400" style="background: #FFFFFF;">VT - <?=$vt_nhap3['dsvt_id']?>
                                    </td>
                                    <td class="color_blue font_s14 line_h17 font_w500"><?= $vt_nhap3['dsvt_name']; ?></td>
                                    <td style="background: #FFFFFF;"><?= $vt_nhap3['dvt_name']; ?></td>
                                    <td style="background: #FFFFFF;"><?= $vt_nhap3['hsx_name']; ?></td>
                                    <td style="background: #FFFFFF;"><?= $vt_nhap3['xx_name']; ?></td>
                                    <td class="sl_nhap"><?= $vt_nhap3['slvt_soLuongNhapKho']; ?></td>
                                    <td><?= number_format($vt_nhap3['dsvt_donGia'],0,'','.'); ?></td>
                                    <td><?= number_format($vt_nhap3['dsvt_donGia'] * $vt_nhap3['slvt_soLuongNhapKho'],0,'','.')?></td>
                                </tr>
                                <?}?>
                                <!-- <tr class="color_blue font_s14 line_h17 font_w500">
                                    <td colspan="2">
                                        <div class=" d_flex align_c cursor_p">
                                            <p class="add_vt d_flex flex_center align_c position_r"><span class="color_white font_s20 position_a">+</span></p>
                                            <p class="">Thêm vật tư</p>
                                        </div>
                                    </td>
                                </tr> -->
                            </table>
                        </div>
                        <div class="pre_q d_flex align_c flex_center position_a">
                            <span class="pre_arrow"></span>
                        </div>
                        <div class="next_q d_flex align_c flex_center position_a">
                            <span class="next_arrow"></span>
                        </div>
                    </div>
                </div>
                <? }?>
                <!-- bảng theo yêu cầu vật tư -->
                <?php if($item['kcxl_hinhThuc']=='NK4'){?>
                <?php $vattu4 = new db_query("SELECT `kcxl_id`, `kcxl_soPhieu`, `kcxl_trangThai`, `dsvt_id`,`slvt_idPhieu`, `slvt_maPhieu`, 
                        `dsvt_maVatTuThietBi`, `dsvt_name`, `dvt_name`, `hsx_name`, `xx_name`, `slvt_soLuongNhapKho`,`dsvt_donGia` FROM `danh-sach-vat-tu`
                        LEFT JOIN `don-vi-tinh` ON `dsvt_donViTinh` =`dvt_id`
                        LEFT JOIN `hang-san-xuat` ON `dsvt_hangSanXuat` = `hsx_id`
                        LEFT JOIN `xuat-xu` ON `dsvt_xuatXu` = `xx_id`
                        LEFT JOIN `so-luong-vat-tu` ON `dsvt_id` = `slvt_maVatTuThietBi`
                        LEFT JOIN `kho-cho-xu-li` ON `slvt_idPhieu` = `kcxl_id`
                        WHERE `dsvt_check` = '1' AND `slvt_idPhieu` = '$id_phieu' AND `dsvt_id_ct` = $id_cty
                        ORDER BY `dsvt_id` ASC ");
                        $stt =1; 
                    ?> 
                <div class="imp_wh_supply" >
                    <div class="position_r align_c d_flex">
                        <div class="main_table table_vt_scr">
                            <table class="table table_list_meterial-5" style="width: 1855px;">
                                <tr>
                                    <th>STT
                                        <span class="span_thread"></span>
                                    </th>
                                    <th>Mã vật tư
                                        <span class="span_thread"></span>
                                    </th>
                                    <th>Tên đầy đủ vật tư thiết bị
                                        <span class="span_thread"></span>
                                    </th>
                                    <th>Đơn vị tính
                                        <span class="span_thread"></span>
                                    </th>
                                    <th> Hãng sản xuất
                                        <span class="span_thread"></span>
                                    </th>
                                    <th> Xuất xứ
                                        <span class="span_thread"></span>
                                    </th>
                                    <th> Số lượng theo yêu cầu
                                        <span class="span_thread"></span>
                                    </th>
                                    <th> Số lượng được duyệt
                                        <span class="span_thread"></span>
                                    </th>
                                    <th> Số lượng thực tế nhập kho
                                        <span class="span_thread"></span>
                                    </th>
                                    <th>Đơn giá (VNĐ)
                                        <span class="span_thread"></span>
                                    </th>
                                    <th>Thành tiền (VNĐ)
                                    </th>
                                </tr>
                                <?php while($vt_nhap4 = mysql_fetch_assoc($vattu4 -> result)){?>
                                <tr class="color_grey font_s14 line_h17 font_w400" data-id="<?=$vt_nhap4['dsvt_id']?>">
                                    <td>
                                        <?= $stt++ ?>
                                    </td>
                                    <td>VT - <?= $vt_nhap4['dsvt_id']?></td>
                                    <td class="font_s14 line_h17 color_blue font_w500" style="text-align: left;"><?= $vt_nhap4['dsvt_name']; ?></td>
                                    <td><?= $vt_nhap4['dvt_name']; ?></td>
                                    <td><?= $vt_nhap4['hsx_name']; ?></td>
                                    <td><?= $vt_nhap4['xx_name']; ?></td>
                                    <?php $id_vtu = $vt_nhap4['dsvt_id'];?>
                                    <td style="background: #FFFFFF;"><?= $emp_vt_yc[$id_vtu]['so_luong_yc_duyet']?></td>
                                    <td style="background: #FFFFFF;"><?= $emp_vt_yc[$id_vtu]['so_luong_duyet']?></td>
                                    <td class="sl_nhap"><?=$vt_nhap4['slvt_soLuongNhapKho']?></td>
                                    <td><?= number_format($vt_nhap4['dsvt_donGia'],0,'','.'); ?></td>
                                    <td><?= number_format($vt_nhap4['dsvt_donGia'] * $vt_nhap4['slvt_soLuongNhapKho'],0,'','.')?></td>
                                </tr>
                                <?}?>
                            </table>
                        </div>
                        <div class="pre_q d_flex align_c flex_center position_a">
                            <span class="pre_arrow"></span>
                        </div>
                        <div class="next_q d_flex align_c flex_center position_a">
                            <span class="next_arrow"></span>
                        </div>

                    </div>
                </div>
                <? }?>
                <!-- bảng nhập khác -->
                <?php if($item['kcxl_hinhThuc']=='NK5'){?>
                <?php $vattu5 = new db_query("SELECT `kcxl_id`, `kcxl_soPhieu`, `kcxl_trangThai`, `dsvt_id`,`slvt_idPhieu`, `slvt_maPhieu`, 
                        `dsvt_maVatTuThietBi`, `dsvt_name`, `dvt_name`, `hsx_name`, `xx_name`, `slvt_soLuongNhapKho`,`dsvt_donGia` FROM `danh-sach-vat-tu`
                        LEFT JOIN `don-vi-tinh` ON `dsvt_donViTinh` =`dvt_id`
                        LEFT JOIN `hang-san-xuat` ON `dsvt_hangSanXuat` = `hsx_id`
                        LEFT JOIN `xuat-xu` ON `dsvt_xuatXu` = `xx_id`
                        LEFT JOIN `so-luong-vat-tu` ON `dsvt_id` = `slvt_maVatTuThietBi`
                        LEFT JOIN `kho-cho-xu-li` ON `slvt_idPhieu` = `kcxl_id`
                        WHERE `dsvt_check` = '1' AND `slvt_idPhieu` = '$id_phieu'
                        ORDER BY `dsvt_id` ASC");
                        $tt = 1;
                ?>
                <div class="imp_wh_df">
                    <div class="position_r align_c d_flex">
                        <div class="main_table table_vt_scr">
                            <table class="table table_list_meterial-3" style="width: 1432px;">
                                <tr>
                                    <th>STT
                                        <span class="span_thread"></span>
                                    </th>
                                    <th>Mã vật tư
                                        <span class="span_thread"></span>
                                    </th>
                                    <th>Tên đầy đủ vật tư thiết bị
                                        <span class="span_thread"></span>
                                    </th>
                                    <th>Đơn vị tính
                                        <span class="span_thread"></span>
                                    </th>
                                    <th> Hãng sản xuất
                                        <span class="span_thread"></span>
                                    </th>
                                    <th> Xuất xứ
                                        <span class="span_thread"></span>
                                    </th>
                                    <th> Số lượng nhập kho
                                        <span class="span_thread"></span>
                                    </th>
                                    <th>Đơn giá (VNĐ)
                                        <span class="span_thread"></span>
                                    </th>
                                    <th>Thành tiền (VNĐ)
                                        <span class="span_thread"></span>
                                    </th>
                                </tr>
                                <? while($vt_nhap5 = mysql_fetch_assoc($vattu5 ->result)){?>
                                <tr class="color_grey font_s14 line_h17 font_w400" data-id="<?=$vt_nhap5['dsvt_id']?>">
                                    <td>
                                       <?= $tt++; ?>
                                    </td>
                                    <td style="background: #FFFFFF;">VT - <?= $vt_nhap5['dsvt_id']?></td>
                                    <td>
                                    <?= $vt_nhap5['dsvt_name']; ?>
                                    </td>
                                    <td style="background: #FFFFFF;"><?= $vt_nhap5['dvt_name']; ?></td>
                                    <td style="background: #FFFFFF;"><?= $vt_nhap5['hsx_name']; ?></td>
                                    <td style="background: #FFFFFF;"><?= $vt_nhap5['xx_name']; ?></td>
                                    <td class="sl_nhap"><?= $vt_nhap5['slvt_soLuongNhapKho']; ?></td>
                                    <td><?= number_format($vt_nhap5['dsvt_donGia'],0,'','.'); ?></td>
                                    <td><?= number_format($vt_nhap5['dsvt_donGia'] * $vt_nhap5['slvt_soLuongNhapKho'],0,'','.')?></td>
                                </tr>
                                <?}?>
                                <!-- <tr class="color_blue font_s14 line_h17 font_w500">
                                    <td colspan="2">
                                        <div class=" d_flex align_c cursor_p">
                                            <p class="add_vt d_flex flex_center align_c position_r"><span class="color_white font_s20 position_a">+</span></p>
                                            <p class="">Thêm vật tư</p>
                                        </div>
                                    </td>
                                </tr> -->
                            </table>
                        </div>
                        <div class="pre_q d_flex align_c flex_center position_a">
                            <span class="pre_arrow"></span>
                        </div>
                        <div class="next_q d_flex align_c flex_center position_a">
                            <span class="next_arrow"></span>
                        </div>
                    </div>
                </div>
                <? }?>
            </div>
            <?php include('../includes/popup_overview.php');  ?>
            <?php include('../includes/popup_nhap-kho.php') ?>

        </div>
</body>
<script type="text/javascript" src="../js/jquery-3.4.1.min.js"></script>
<script type="text/javascript" src="../js/jquery.validate.min.js"></script>
<script type="text/javascript" src="../js/select2.min.js"></script>
<script type="text/javascript" src="../js/js_h.js"></script>
<script type="text/javascript" src="../js/validate_h.js"></script>
<script type="text/javascript" src="../js/js_k.js"></script>
<script type="text/javascript" src="../js/js_n.js"></script>
<script type="text/javascript" src="../js/nhap_kho_chi_tiet.js"></script>

<script>
    var nguoi_tc = <?= json_encode($id_nguoi_lg) ?>;
    var nguoi_duyet = <?= json_encode($id_nguoi_lg) ?>;
    var nguoi_xoa = <?= json_encode($id_nguoi_lg) ?>;
    var nguoi_ht = <?= json_encode($id_nguoi_lg) ?>;

</script>

</html>