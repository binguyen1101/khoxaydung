<?php
    include("config1.php");

    if(!in_array(3,$ro_nhap_kho)){
        header("Location: /tong-quan.html");
    }
  
    $id_phieu = getValue("id","int","GET","");

    if (isset($_SESSION['quyen']) && $_SESSION['quyen'] == 1) {
        $curl = curl_init();
        $token = $_COOKIE['acc_token'];
        curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/list_all_employee_of_company.php');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));
        $response = curl_exec($curl);
        curl_close($curl);

        $data_list = json_decode($response, true);
        $data_list_nv = $data_list['data']['items'];
        $count = count($data_list_nv);
        $user_id = $_SESSION['com_id'];
    } elseif (isset($_SESSION['quyen']) && ($_SESSION['quyen'] == 2)) {
        $curl = curl_init();
        $token = $_COOKIE['acc_token'];
        curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/list_all_my_partner.php?get_all=true');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));
        $response = curl_exec($curl);
        curl_close($curl);
        $data_list = json_decode($response, true);
        $data_list_nv = $data_list['data']['items'];
        $count = count($data_list_nv);
        $user_id = $_SESSION['ep_id'];
    }
    $newArr = [];
    for ($i = 0; $i < count($data_list_nv); $i++) {
        $value = $data_list_nv[$i];
        $newArr[$value["ep_id"]] = $value;
    }

    if (isset($_COOKIE['user']) && $_COOKIE['user'] != "" && $_COOKIE['role'] == 1) {
		$token = $_COOKIE['acc_token'];
		$curl = curl_init();
		$data = array();
		curl_setopt($curl, CURLOPT_POST, 1);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
		curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/user_info_company.php');
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));
	
		$response = curl_exec($curl);
		curl_close($curl);
		$data_tt = json_decode($response, true);
		$tt_user = $data_tt['data']['user_info_result'];
	} else if (isset($_COOKIE['user']) && $_COOKIE['user'] != "" && $_COOKIE['role'] == 2) {
		$token = $_COOKIE['acc_token'];
		$curl = curl_init();
		$data = array();
		curl_setopt($curl, CURLOPT_POST, 1);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
		curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/user_info_employee.php');
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));
		$response = curl_exec($curl);
		curl_close($curl);
		$data_tt = json_decode($response, true);
		$tt_user = $data_tt['data']['user_info_result'];
	}
    $id_cty = $tt_user['com_id'];
    $date = date('Y-m-d', time());

    $item = new db_query("SELECT `kcxl_id`, `kcxl_soPhieu`, `kcxl_hinhThuc`, `kcxl_nguoiTao`, `kcxl_ngayTao`, `kcxl_donHang`, `kcxl_trangThai`, 
    `kcxl_ngayHoanThanh`, `kcxl_nhaCungCap`, `kcxl_nguoiThucHien`, `kcxl_phongBanNguoiGiao`, `kcxl_nguoiNhan`, `kcxl_phieuDieuChuyenKho`,
    `kcxl_phongBanNguoiNhan`, `kcxl_khoNhap`, `kcxl_ngayNhapKho`, `kcxl_khoXuat`, `kcxl_ngayYeuCauHoanThanh`, `kcxl_congTrinh`, 
    `kcxl_phieuYeuCau`, `kcxl_ghi_chu` 
    FROM `kho-cho-xu-li`
    WHERE `kcxl_id` = $id_phieu AND `kcxl_soPhieu` = 'PNK' AND `kcxl_check` = 1");

    $info_item = mysql_fetch_assoc($item->result);
    $id_nguoi_giao = $info_item['kcxl_nguoiThucHien'];
    $id_nguoi_nhan = $info_item['kcxl_nguoiNhan'];
    $id_cong_trinh = $info_item['kcxl_congTrinh'];
    

    $select_khoNhap = new db_query("SELECT `kho_id`, `kho_name` FROM `kho` WHERE `kho_id_ct` = $id_cty");

    // Nhập khác
    $kho2 = new db_query("SELECT `kho_id`, `kho_maKho`, `kho_name` FROM `kho` WHERE `kho_id_ct` = $id_cty");

    //  Nhập theo yêu cầu vật tư
    $kho4 = new db_query("SELECT `kho_id`, `kho_maKho`, `kho_name` FROM `kho` WHERE `kho_id_ct` = $id_cty");
    
    // 
    $kho3 = new db_query("SELECT `kho_id`, `kho_maKho`, `kho_name` FROM `kho` WHERE `kho_id_ct` = $id_cty");
    $arr_kho3 = [];
    while ($row = mysql_fetch_assoc($kho3->result)) {
        $arr_kho3[$row["kho_id"]] = $row;
    }

    $pdk = new db_query("SELECT `kcxl`.`kcxl_id`, `kcxl`.`kcxl_nguoiTao`, `kcxl`.`kcxl_ngayTao`, `kcxl`.`kcxl_nguoiThucHien`, `kcxl`.`kcxl_phongBanNguoiGiao`, `kcxl`.`kcxl_nguoiNhan`, `kcxl`.`kcxl_phongBanNguoiNhan`, `kcxl`.`kcxl_khoNhap`, `kcxl`.`kcxl_khoXuat`, `kcxl`.`kcxl_ngayYeuCauHoanThanh`, `khoNhap`.`kho_name` as kho_nhap_name,`khoXuat`.`kho_name`as kho_xuat_name FROM `kho-cho-xu-li` kcxl
    LEFT JOIN `kho` `khoNhap` on `kcxl`.`kcxl_khoNhap` = `khoNhap`.`kho_id`       
    LEFT JOIN `kho` `khoXuat` on `kcxl`.`kcxl_khoXuat` = `khoXuat`.`kho_id`
    WHERE `kcxl_check` = 1 AND `kcxl_id_ct` = $id_cty AND `kcxl_trangThai` = 7 AND `kcxl_soPhieu` = 'ĐCK'");


    $arr_pdk = [];
    while ($row = mysql_fetch_assoc($pdk->result)) {
        $arr_pdk[$row["kcxl_id"]] = $row;
    }
    // echo "<pre>";
    // print_r($arr_pdk);
    // echo "</pre>";
    // die();


    $dsvt_tb = new db_query("SELECT `dsvt_id`, `dsvt_name`, `dsvt_donViTinh`, `dsvt_donGia`, `dsvt_hangSanXuat`, `dsvt_xuatXu`,  `dvt_name`, `hsx_name`, `xx_name` FROM `danh-sach-vat-tu`
    LEFT JOIN `don-vi-tinh` ON `dsvt_donViTinh` = `dvt_id`
    LEFT JOIN `hang-san-xuat` ON `dsvt_hangSanXuat` = `hsx_id`
    LEFT JOIN `xuat-xu` ON `dsvt_xuatXu` = `xx_id`
    WHERE `dsvt_check` = 1 AND `dsvt_id_ct`= $id_cty");
    $arr_dsvt_tb = [];
    while ($row_dsvt = mysql_fetch_assoc($dsvt_tb->result)) {
        $arr_dsvt_tb[$row_dsvt["dsvt_id"]] = $row_dsvt;
    }

    // echo "<pre>";
    // print_r($newArr[5628]['ep_name']);
    // echo "</pre>";
    // die();

    // API phiếu yêu cầu vật tư
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_URL, "https://phanmemquanlycungung.timviec365.vn/api/phieu_yc_vat_tu.php");
    curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($curl, CURLOPT_POSTFIELDS, [
        'id_com' => $id_cty,
    ]);
    $response = curl_exec($curl);
    curl_close($curl);
    $emp0 = json_decode($response, true);
    $emp = $emp0['data']['items'];
    $emp1 = [];
    for ($i = 0; $i < count($emp); $i++) {
        $pyc = $emp[$i];
        $emp1[$pyc["id"]] = $pyc;
    }

    // API vật tư theo phiếu yêu cầu
    $curl_vt_yc = curl_init();
    curl_setopt($curl_vt_yc, CURLOPT_POST, 1);
    curl_setopt($curl_vt_yc, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl_vt_yc, CURLOPT_URL, "https://phanmemquanlycungung.timviec365.vn/api/vat_tu_yc.php");
    curl_setopt($curl_vt_yc, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($curl_vt_yc, CURLOPT_POSTFIELDS, [
        'id_phieu' => $info_item['kcxl_phieuYeuCau'],
    ]);
    $response_vt_yc = curl_exec($curl_vt_yc);
    curl_close($curl_vt_yc);
    $emp_json = json_decode($response_vt_yc, true);
    $emp_arr = $emp_json['data']['items'];
    $emp_vt_yc = [];
    for ($i = 0; $i < count($emp_arr); $i++) {
        $vt_yc = $emp_arr[$i];
        $emp_vt_yc[$vt_yc["id"]] = $vt_yc;
    }

    // API công trình
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_POST, 1);
    curl_setopt($curl, CURLOPT_URL, 'https://phanmemquanlycongtrinh.timviec365.vn/api/congtrinh.php');
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($curl, CURLOPT_POSTFIELDS, [
        'id_com' => $id_cty,
    ]);
    $response = curl_exec($curl);
    curl_close($curl);
    $data_list = json_decode($response, true);
    $list_phieu_vt = $data_list['data']['items'];
    $list_phieu_vt1 = [];
    for ($i = 0; $i < count($list_phieu_vt); $i++) {
        $ctr = $list_phieu_vt[$i];
        $list_phieu_vt1[$ctr["ctr_id"]] = $ctr;
    }

    // API vật tư đơn hàng
    $curl_vtdh = curl_init();
    curl_setopt($curl_vtdh, CURLOPT_POST, 1);
    curl_setopt($curl_vtdh, CURLOPT_URL, 'https://phanmemquanlycungung.timviec365.vn/api/vat_tu_dh.php');
    curl_setopt($curl_vtdh, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl_vtdh, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($curl_vtdh, CURLOPT_POSTFIELDS, [
        'com_id' => $id_cty,
        'dh_id' => $info_item['kcxl_donHang'],
    ]);
    $response_vtdh = curl_exec($curl_vtdh);
    curl_close($curl_vtdh);
    $data_list_vtdh = json_decode($response_vtdh, true);
    $list_vtdh = $data_list_vtdh['data']['items'];

    $list_vtdh1 = [];
    for ($j = 0; $j < count($list_vtdh); $j++) {
        $list_vtdh_p = $list_vtdh[$j];
        $list_vtdh1[$list_vtdh_p["id"]] = $list_vtdh_p;
    }

    // API đơn hàng
    $curl_dh = curl_init();
    curl_setopt($curl_dh, CURLOPT_POST, 1);
    curl_setopt($curl_dh, CURLOPT_URL, 'https://phanmemquanlycungung.timviec365.vn/api/don_hang.php');
    curl_setopt($curl_dh, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl_dh, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($curl_dh, CURLOPT_POSTFIELDS, [
        'com_id' => $id_cty,
        'phan_loai' => 1,
    ]);
    $response_dh = curl_exec($curl_dh);
    curl_close($curl_dh);
    $data_list_dh = json_decode($response_dh, true);
    $list_dh = $data_list_dh['data']['items'];

    $list_dh1 = [];
    for ($j = 0; $j < count($list_dh); $j++) {
        $list_dh_p = $list_dh[$j];
        $list_dh1[$list_dh_p["id"]] = $list_dh_p;
    }

    $oninput = "this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');";

?>
<!DOCTYPE html>
<html lang="vi">

<head>
    <title>kho vật tư xây dựng</title>
    <meta charset="UTF-8">
    <meta name="robots" content="noindex,nofollow" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="../css/select2.min.css?v<=<?= $ver ?>">
    <link rel="stylesheet" type="text/css" href="../css/style.css?v=<?= $ver ?>">
    <link rel="stylesheet" type="text/css" href="../css/style_h.css?v=<?= $ver ?>">
    <link rel="stylesheet" type="text/css" href="../css/style_n.css?v=<?= $ver ?>">
    <link rel="stylesheet" type="text/css" href="../css/style_k.css?v<=<?= $ver ?>">

</head>


<body class="seclec2_radius">
    <div class="main_wrapper_all">
        <div class="wapper_all">
            <?php include('../includes/sidebar.php');  ?>
        </div>
        <div class="main_overview" id="main_overview">
            <div class="header_menu_overview d_flex align_c space_b">
                <p class="color_grey line_16 font_s14 text_link_page" style="display: block;"><a href="/nhap-kho.html" class="cursor_p">
                        <img src="../images/back_item_g.png" alt="">
                    </a>Nhập kho / Chỉnh sửa</p>
                <img class="open_sidebar_w" src="../images/open_sidebar_w.png" alt="" onclick="toggle('main_sidebar')" style="display: none;">
                <?php include('../includes/header.php');  ?>
            </div>
            <form method="post" action="" id="f_import_wh_create">
                <div class="top_in_wh_create">
                    <div class="head_wh d_flex space_b align_c title_wh" style="display: none;">
                        <div class=" d_flex align_c ">
                            <a href="/nhap-kho.html" class="cursor_p">
                                <img src="../images/back_item_g.png" alt="">
                            </a>
                            <p class="color_grey line_16 font_s14 ml_10">&npsbNhập kho / Chỉnh sửa
                            </p>
                        </div>
                    </div>
                    <div class="body_import_warehouse_create">
                        <div class="header_body_b color_white line_h19 font_s16 font_wB">
                            Chỉnh sửa phiếu nhập kho
                        </div>

                        <!-- phiếu nhập kho -->
                        <div class="body_input_infor_vote " id="" style="display:block;">
                            <div class="d_flex align_c space_b wh768">
                                <div class="d_flex flex_column width_create mb_15">
                                    <p class="font_s15 line_h18 font_w500 color_grey mb_5">Số phiếu</p>
                                    <input class="input_value_grey font_14 line_h16 so_phieu" name="so_phieu" type="text" value="PNK - <?=$info_item['kcxl_id']?>" readonly>
                                </div>
                                <div class="d_flex flex_column width_create mb_15">
                                    <p class="font_s15 line_h18 font_w500 color_grey">Hình thức nhập kho</p>
                                    <div class="select_create">
                                        <select class="select_create select_import_form " name="select_import_form" id="select_import_form" onchange="change_ht(this)">
                                            <option value="NK1" <?=($info_item['kcxl_hinhThuc'] == "NK1") ? "selected" : "" ?>>Nhập theo biên bản giao hàng</option>
                                            <option value="NK2" <?=($info_item['kcxl_hinhThuc'] == "NK2") ? "selected" : "" ?>>Nhập điều chuyển</option>
                                            <option value="NK3" <?=($info_item['kcxl_hinhThuc'] == "NK3") ? "selected" : "" ?>>Nhập trả lại từ thi công</option>
                                            <option value="NK4" <?=($info_item['kcxl_hinhThuc'] == "NK4") ? "selected" : "" ?>>Nhập theo yêu cầu vật tư</option>
                                            <option value="NK5" <?=($info_item['kcxl_hinhThuc'] == "NK5") ? "selected" : "" ?>>Nhập khác</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="d_flex align_c space_b wh768">
                                <div class="d_flex flex_column width_create mb_15">
                                    <p class="font_s15 line_h18 font_w500 color_grey mb_5">Người tạo</p>
                                    <?php $id_ngtao = $info_item['kcxl_nguoiTao']; 
                                        if($id_ngtao != 0){
                                            $name_ngtao = $newArr[$id_ngtao]['ep_name'];
                                        }else{
                                            $name_ngtao = $tt_user['com_name'];
                                        }
                                    ?>
                                    <?php if($_SESSION['quyen'] == 2){ ?>
                                        <input class="input_value_grey font_14 line_h16 color_grey nguoi_tao" readonly type="text" value="<?= ($newArr[$id_ngtao]['ep_id'] == $info_item['kcxl_nguoiTao']) ? "$name_ngtao" : "" ?>" data="<?= $info_item['kcxl_nguoiTao'] ?>" data-ct="<?= $id_cty ?>">
                                    <?}else{?>
                                        <input class="input_value_grey font_14 line_h16 color_grey nguoi_tao" readonly type="text" value="<?= $name_ngtao ?>" data="0" data-ct="<?=$id_cty?>">
                                    <?}?>
                                </div>
                                <div class="d_flex flex_column width_create mb_15">
                                    <p class="font_s15 line_h18 font_w500 color_grey mb_5">Ngày tạo</p>
                                    <input class="input_value_grey font_14 line_h16 ngay_tao" type="date" name="ngay_tao" readonly value="<?= $info_item['kcxl_ngayTao'] ?>">
                                </div>
                            </div>
                            <div>
                                <!-- Nhập theo biên bản giao hàng -->
                                <div class="detail_input_wh_1 display_none" <?php if($info_item['kcxl_hinhThuc'] == "NK1"){?>style="display: block;"<?php }?>>
                                    <div class="d_flex align_c space_b wh768">
                                        <div class="d_flex flex_column order_import_wh_cre width_create mb_15">
                                            <div class="d_flex ">
                                                <p class="font_s15 line_h18 font_w500 color_grey mb_5">Đơn hàng</p>
                                                <span class="color_red alert_red">*</span>
                                            </div>
                                            <select name="order_import_wh_cre" class="font_14 line_h16 font_w400 choice_order" style="width: 100%;">
                                                <option value="">Chọn đơn hàng</option>
                                                <?php foreach($list_dh1 as $don_hang){?>
                                                <option value="<?= $don_hang['id']?>"<?= ($don_hang['id'] == $info_item['kcxl_donHang']) ? "selected" : "" ?>>ĐH - <?=$don_hang['id'];?></option>
                                                <?php }?>
                                            </select>
                                        </div>
                                        <div class="d_flex align_c width_create mb_15">
                                            <div class="w_100 seclec2_radius">
                                                <p class="font_s15 line_h18 font_w500 color_grey">Trạng thái</p>
                                                <div class="select_create mt_5">
                                                    <select class="select_create select_status" name="select_status" <?= ($_SESSION['quyen'] == '2') ? "disabled" : "" ?> onchange="change_trangthai(this)">
                                                        <option value="1" <?= $info_item['kcxl_trangThai'] == 1 ? "selected" : "" ?>>Khởi tạo</option>
                                                        <option value="2" <?= $info_item['kcxl_trangThai'] == 2 ? "selected" : "" ?>>Từ chối</option>
                                                        <?php if($_SESSION['quyen'] == '1'){?>
                                                            <option value="3" <?= $info_item['kcxl_trangThai'] == 3 ? "selected" : "" ?>>Đã duyệt chờ nhập kho</option>
                                                        <?php }?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="w_50 ml_10 date_accses" style="display:none">
                                                <div class="d_flex">
                                                    <p class="font_s15 line_h18 font_w500 color_grey">Ngày hoàn thành</p>
                                                    <span class="color_red alert_red">*</span>
                                                </div>
                                                <div class="select_create">
                                                    <input class="input_value_w" type="date" name="ngay_hoanThanh">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="d_flex align_c space_b wh768">
                                        <div class="d_flex flex_column width_create mb_15">
                                            <p class="font_s15 line_h18 font_w500 color_grey mb_5">Nhà cung cấp</p>
                                            <input class="input_value_grey color_grey font_14 line_h16" value="<?= $info_item['kcxl_nhaCungCap']?>" name="nha_cung_cap" type="text" placeholder="Hiển thị tên nhà cung cấp từ đơn hàng" readonly>
                                            <!-- <input class="input_value_grey font_14 line_h16 " type="text" placeholder="Hiển thị tên nhà cung cấp từ đơn hàng" readonly> -->
                                        </div>
                                        <div class="d_flex flex_column width_create mb_15">
                                            <p class="font_s15 line_h18 font_w500 color_grey mb_5">Người giao hàng</p>
                                            <input class=" font_14 line_h16" type="text" value="<?= $info_item['kcxl_nguoiThucHien']?>" placeholder="Nhập tên người giao hàng" name="nguoiGiao">
                                        </div>
                                    </div>
                                    <div class="d_flex align_c space_b wh768">
                                        <div class="d_flex flex_column width_create mb_15">
                                            <p class="font_s15 line_h18 font_w500 color_grey mb_5">Người nhận</p>
                                            <input class="input_value_grey color_grey font_14 line_h16" name="nguoi_nhan" type="text" value="<?= $newArr[$id_nguoi_nhan]['ep_name']?>" placeholder="Hiển thị tên người nhận từ đơn hàng" readonly>
                                            <input class="input_value_grey color_grey font_14 line_h16 " value="<?=$info_item['kcxl_nguoiNhan']?>" name="id_nguoi_nhan" type="hidden" readonly>
                                        </div>
                                        <div class="d_flex flex_column width_create mb_15">
                                            <p class="font_s15 line_h18 font_w500 color_grey mb_5">Phòng ban</p>
                                            <input class="input_value_grey color_grey font_14 line_h16" type="text" value="<?= $newArr[$id_nguoi_nhan]['dep_name']?>" name="phong_ban" placeholder="Hiển thị phòng ban người nhận từ đơn hàng" readonly>
                                            <input class="input_value_grey color_grey font_14 line_h16 " name="id_pb_nguoi_nhan" value="<?= $newArr[$id_nguoi_nhan]['dep_id']?>" type="hidden" name="phong_ban" readonly>
                                        </div>
                                    </div>
                                    <div class="d_flex align_c space_b wh768">
                                        <div class="d_flex flex_column wh_imp width_create mb_15 kho_nhap">
                                            <p class="color_grey font_s15 line_h18 font_w500">Kho nhập<span style="color: red;">*</span></p>
                                            <select name="wh_imp" id="" class="select_wh_in color_grey font_s14 line_h17 font_w400" style="width:100%;">
                                                <option value="">Chọn kho</option>
                                                <? while ($item = mysql_fetch_assoc($select_khoNhap->result)) { ?>
                                                    <option value="<?= $item['kho_id'] ?>" <?=($item['kho_id'] == $info_item['kcxl_khoNhap']) ? "selected" : "" ?>><?= $item['kho_name'] ?></option>
                                                <? } ?>
                                            </select>
                                        </div>
                                        <div class="d_flex flex_column width_create mb_15 ngay_nhap">
                                            <div class="d_flex align_c">
                                                <p class="font_s15 line_h18 font_w500 color_grey mb_5">Ngày nhập kho</p>
                                                <span class="color_red alert_red">*</span>
                                            </div>
                                            <input class=" font_14 line_h16" type="date" value="<?=$info_item['kcxl_ngayNhapKho'];?>" name="ngayNhapKho">
                                        </div>
                                    </div>
                                </div>

                                <!-- Nhập điều chuyển -->
                                <div class="detail_input_wh_2 display_none" <?php if($info_item['kcxl_hinhThuc'] == "NK2"){?>style="display: block;"<?php }?>>
                                    <div class="d_flex space_b wh768">
                                        <div class="d_flex flex_column wh_transfer_note seclec2_radius width_create mb_15">
                                            <div class="d_flex">
                                                <p class="font_s15 line_h18 font_w500 color_grey mb_5">Phiếu điều chuyển kho</p>
                                                <span class="color_red alert_red">*</span>
                                            </div>
                                            <select class="select_create select_transfer_slip" name="select_transfer_slip" id="select_transfer_slip">
                                                <option value="">Chọn phiếu điều chuyển</option>
                                                <? foreach ($arr_pdk as $key => $value) { ?>
                                                    <option value="<?= $value['kcxl_id'] ?>" <?= ($value['kcxl_id'] == $info_item['kcxl_phieuDieuChuyenKho']) ? "selected" : "" ?>><?= 'ĐCK' . ' ' . '-' . ' ' . $value['kcxl_id'] . ' ' . 'chuyển từ' . ' ' . $value['kho_xuat_name'] . ' ' . 'đến' . ' ' . $value['kho_nhap_name'] ?></option>
                                                <? } ?>
                                            </select>
                                        </div>
                                        <div class="d_flex width_create mb_15">
                                            <div class="w_100 seclec2_radius">
                                                <p class="font_s15 line_h18 font_w500 color_grey">Trạng thái</p>
                                                <div class="select_create mt_5">
                                                    <select class="select_create select_status" name="select_status_2" id="select_status_2" <?= ($_SESSION['quyen'] == '2') ? "disabled" : "" ?> onchange="change_trangthai(this)">
                                                        <option value="1" <?= $info_item['kcxl_trangThai'] == 1 ? "selected" : "" ?>>Khởi tạo</option>
                                                        <option value="2" <?= $info_item['kcxl_trangThai'] == 2 ? "selected" : "" ?>>Từ chối</option>
                                                        <?php if($_SESSION['quyen'] == '1'){?>
                                                            <option value="3" <?= $info_item['kcxl_trangThai'] == 3 ? "selected" : "" ?>>Đã duyệt chờ nhập kho</option>
                                                        <?php }?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="w_50 ml_10 date_accses" style="display:none">
                                                <div class="date_accses_2">
                                                    <div class="d_flex">
                                                        <p class="font_s15 line_h18 font_w500 color_grey">Ngày hoàn thành</p>
                                                        <span class="color_red alert_red">*</span>
                                                    </div>
                                                    <div class="select_create">
                                                        <input class="input_value_w ngay_ht_2" type="date" name="ngay_ht_2" id="ngay_ht_2">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="dieuChuyen">
                                        <div class="d_flex align_c space_b wh768">
                                            <?php $id_phieu_dc = $info_item['kcxl_phieuDieuChuyenKho']; ?>
                                            <div class="d_flex flex_column width_create mb_15">
                                                <p class="font_s15 line_h18 font_w500 color_grey mb_5">Kho xuất</p>
                                                <input class="input_value_grey font_14 line_h16 kho_xuat_2" type="text" value="<?=$arr_pdk[$id_phieu_dc]['kho_xuat_name']?>" placeholder="Hiển thị kho xuất theo phiếu điều chuyển kho" readonly>
                                                <input class="input_value_grey font_14 line_h16 id_kho_xuat_2" value="<?=$info_item['kcxl_khoXuat']?>" type="hidden" readonly>
                                            </div>
                                            <div class="d_flex flex_column width_create mb_15">
                                                <p class="font_s15 line_h18 font_w500 color_grey mb_5">Kho nhập</p>
                                                <input class=" font_14 line_h16 kho_nhap_2" type="text" value="<?=$arr_pdk[$id_phieu_dc]['kho_nhap_name']?>" placeholder="Hiển thị kho nhập theo phiếu điều chuyển kho" readonly>
                                                <input class=" font_14 line_h16 id_kho_nhap_2" type="hidden" value="<?=$info_item['kcxl_khoNhap']?>" readonly>
                                            </div>
                                        </div>
                                        <div class="d_flex align_c space_b wh768">
                                            <div class="d_flex flex_column width_create mb_15">
                                                <p class="font_s15 line_h18 font_w500 color_grey mb_5">Người giao</p>
                                                <input class="input_value_grey font_14 line_h16 nguoi_giao_2" type="text" value="<?= $newArr[$id_nguoi_giao]['ep_name']?>" placeholder="Hiển thị tên người giao từ phiếu điều chuyển kho" readonly>
                                                <input class="input_value_grey font_14 line_h16 id_nguoi_giao_2" type="hidden" value="<?=$info_item['kcxl_nguoiThucHien']?>" readonly>
                                            </div>
                                            <div class="d_flex flex_column width_create mb_15">
                                                <p class="font_s15 line_h18 font_w500 color_grey mb_5">Phòng ban</p>
                                                <input class="input_value_grey font_14 line_h16 phong_ban_giao_2" type="text" value="<?= $newArr[$id_nguoi_giao]['dep_name']?>" placeholder="Hiển thị phòng ban người giao từ phiếu điều chuyển kho" readonly>
                                                <input class="input_value_grey font_14 line_h16 id_phong_ban_giao_2" type="hidden" value="<?=$info_item['kcxl_phongBanNguoiGiao']?>" readonly>
                                            </div>
                                        </div>
                                        <div class="d_flex align_c space_b wh768">
                                            <div class="d_flex flex_column width_create mb_15">
                                                <p class="font_s15 line_h18 font_w500 color_grey mb_5">Người nhận</p>
                                                <input class="input_value_grey font_14 line_h16 nguoi_nhan_2" type="text" value="<?= $newArr[$id_nguoi_nhan]['ep_name']?>" placeholder="Hiển thị tên người nhận từ phiếu điều chuyển kho" readonly>
                                                <input class="input_value_grey font_14 line_h16 id_nguoi_nhan_2" type="hidden" value="<?=$info_item['kcxl_nguoiNhan']?>" readonly>
                                            </div>
                                            <div class="d_flex flex_column width_create mb_15">
                                                <p class="font_s15 line_h18 font_w500 color_grey mb_5">Phòng ban</p>
                                                <input class="input_value_grey font_14 line_h16 phong_ban_nhan_2" type="text" value="<?= $newArr[$id_nguoi_nhan]['dep_name']?>" placeholder="Hiển thị phòng ban người nhận từ phiếu điều chuyển kho" readonly>
                                                <input class="input_value_grey font_14 line_h16 id_phong_ban_nhan_2" type="hidden" value="<?=$info_item['kcxl_phongBanNguoiNhan']?>" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="d_flex space_b wh768">
                                        <div class="ngay_ycHoanThanh d_flex flex_column width_create mb_15">
                                            <div class="">
                                                <p class="font_s15 line_h18 font_w500 color_grey mb_5">Ngày yêu cầu hoàn thành</p>
                                                <input class="input_value_grey font_14 line_h16 date_yc_ht_2" type="date" value="<?=$info_item['kcxl_ngayYeuCauHoanThanh']?>" readonly>
                                            </div>
                                        </div>
                                        <div class="d_flex flex_column width_create mb_15">
                                            <div class="data_nhap_2">
                                                <div class="d_flex align_c">
                                                    <p class="font_s15 line_h18 font_w500 color_grey mb_5 date_nhap_2">Ngày nhập kho</p>
                                                    <span class="color_red alert_red">*</span>
                                                </div>
                                                <input class=" font_14 line_h16 ngay_nhap_2" style="width: 100%" name="ngay_nhap_2" id="ngay_nhap_2" type="date" value="<?=$info_item['kcxl_ngayNhapKho']?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!-- Nhập trả lại từ thi công -->
                                <div class="detail_input_wh_3 display_none" <?php if($info_item['kcxl_hinhThuc'] == "NK3"){
                                    $kho = new db_query("SELECT `kho_id`, `kho_maKho`, `kho_name` FROM `kho` WHERE `kho_congTrinh` = $id_cong_trinh AND `kho_id_ct` = $id_cty");
                                    ?>style="display: block;"<?php }?>>
                                    <div class="d_flex space_b wh768">
                                        <div class="d_flex flex_column choice_construction seclec2_radius width_create mb_15">
                                            <div class="d_flex">
                                                <p class="font_s15 line_h18 font_w500 color_grey mb_5">Chọn công trình</p>
                                                <span class="color_red alert_red">*</span>
                                            </div>
                                            <select class="font_14 line_h16 select_construction " name="select_construction" id="select_construction">
                                                <option value="">Chọn công trình</option>
                                                <? foreach ($list_phieu_vt1 as $key => $value) { ?>
                                                    <option value="<?= $value['ctr_id'] ?>" <?= ($value['ctr_id'] == $info_item['kcxl_congTrinh'])  ? "selected" : "" ?>><?= $value['ctr_name'] ?></option>
                                                <? } ?>
                                            </select>
                                        </div>
                                        <div class="d_flex width_create mb_15">
                                            <div class="w_100 ">
                                                <p class="font_s15 line_h18 font_w500 color_grey">Trạng thái</p>
                                                <div class="select_create mt_5">
                                                    <select class="select_create select_status" name="select_status_3" <?= ($_SESSION['quyen'] == '2') ? "disabled" : "" ?> id="select_status_3" onchange="change_trangthai(this)">
                                                        <option value="1" <?= $info_item['kcxl_trangThai'] == 1 ? "selected" : "" ?>>Khởi tạo</option>
                                                        <option value="2" <?= $info_item['kcxl_trangThai'] == 2 ? "selected" : "" ?>>Từ chối</option>
                                                        <?php if($_SESSION['quyen'] == '1'){?>
                                                            <option value="3" <?= $info_item['kcxl_trangThai'] == 3 ? "selected" : "" ?>>Đã duyệt chờ nhập kho</option>
                                                        <?php }?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="w_50 ml_10 date_accses" style="display:none">
                                                <div class="tb_date_ht_3">
                                                    <div class="d_flex">
                                                        <p class="font_s15 line_h18 font_w500 color_grey">Ngày hoàn thành</p>
                                                        <span class="color_red alert_red">*</span>
                                                    </div>
                                                    <div class="select_create   ">
                                                        <input class="input_value_w date_ht_3" type="date" name="date_ht_3" id="data_ht_3">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="d_flex space_b wh768">
                                        <div class="d_flex flex_column mb_15 width_create mb_15">
                                            <div class="d_flex space_b">
                                                <p class="color_grey font_s15 line_h18 font_w500">Người giao hàng</p>
                                                <a href="#" class="color_blue font_s15 line_h18 font_w400 d_flex align_c"><span class="font_s24">+</span>&nbsp;Thêm nhân viên</a>
                                            </div>
                                            <select name="select_shipper_wh_3" id="select_shipper_wh_3" class="select_shipper_wh color_grey font_s14 line_h17 font_w400" style="width: 100%;">
                                                <option value="">Chọn người giao hàng</option>
                                                <? foreach ($data_list_nv as $item) { ?>
                                                    <option value="<?= $item['ep_id'] ?>" <?= ($item['ep_id'] == $info_item['kcxl_nguoiThucHien']) ? "selected" : "" ?>><?= $item['ep_name']."&nbsp-&nbsp".$item['dep_name']; ?></option>
                                                <? } ?>
                                            </select>
                                        </div>
                                        <div class="d_flex flex_column width_create p_nguoiGiao mb_15">
                                            <div class="">
                                                <p class="font_s15 line_h18 font_w500 color_grey mb_5">Phòng ban</p>
                                                <input class="input_value_grey font_14 line_h16 phong_giao_3" value="<?= $newArr[$id_nguoi_giao]['dep_name']?>" type="text" placeholder="Hiển thị phòng ban người giao" readonly>
                                                <input class="input_value_grey font_14 line_h16 id_phong_giao_3" value="<?= $newArr[$id_nguoi_giao]['dep_id']?>" type="hidden" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="d_flex space_b wh768">
                                        <div class="d_flex flex_column mb_15 width_create">
                                            <div class="d_flex space_b">
                                                <p class="color_grey font_s15 line_h18 font_w500">Người nhận</p>
                                                <a href="#" class="color_blue font_s15 line_h18 font_w400 d_flex align_c"><span class="font_s24">+</span>&nbsp;Thêm nhân viên</a>
                                            </div>
                                            <select name="select_receiver_wh_3" id="select_receiver_wh_3" class="select_receiver_wh color_grey font_s14 line_h17 font_w400 nguoiNhan_thiCong" style="width: 100%;">
                                                <option value="">Chọn người nhận</option>
                                                <? foreach ($data_list_nv as $item) { ?>
                                                    <option value="<?= $item['ep_id'] ?>" <?= ($item['ep_id'] == $info_item['kcxl_nguoiNhan']) ? "selected" : "" ?>><?= $item['ep_name']."&nbsp-&nbsp".$item['dep_name']; ?></option>
                                                <? } ?>
                                            </select>
                                        </div>
                                        <div class="d_flex flex_column width_create p_nguoiNhan mb_15">
                                            <div class="">
                                                <p class="font_s15 line_h18 font_w500 color_grey mb_5">Phòng ban</p>
                                                <input class="input_value_grey font_14 line_h16 phong_ban_nhan_3" value="<?= $newArr[$id_nguoi_nhan]['dep_name']?>" type="text" placeholder="Hiển thị phòng ban người nhận" readonly>
                                                <input class="input_value_grey font_14 line_h16 id_phong_ban_nhan_3" value="<?=$info_item['kcxl_phongBanNguoiNhan']?>" type="hidden" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="d_flex space_b wh768">
                                        <div class="d_flex flex_column wh_imp width_create mb_15">
                                            <div class="tb_kn_3">
                                                <p class="color_grey font_s15 line_h18 font_w500">Kho nhập<span style="color: red;">*</span></p>
                                                <select name="select_wh_in_3" id="select_wh_in_3" class="select_wh_in color_grey font_s14 line_h17 font_w400" style="width:100%;">
                                                    <option value=""></option>
                                                    <? while ($row = mysql_fetch_assoc($kho->result)) { ?>
                                                        <option value="<?= $row['kho_id'] ?>" <?= ($row['kho_id'] == $info_item['kcxl_khoNhap']) ? "selected" : "" ?>><?= $row['kho_name'] ?></option>
                                                    <? } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="d_flex flex_column width_create mb_15">
                                            <div class="tb_date_nk_3">
                                                <div class="d_flex align_c">
                                                    <p class="font_s15 line_h18 font_w500 color_grey mb_5">Ngày nhập kho</p>
                                                    <span class="color_red alert_red">*</span>
                                                </div>
                                                <input class=" font_14 line_h16 ngay_nk_3" style="width:100%" name="ngay_nk_3" id="ngay_nk_3" type="date" value="<?=$info_item['kcxl_ngayNhapKho']?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!-- Nhập khác-->
                                <div class="detail_input_wh_4 display_none" <?php if($info_item['kcxl_hinhThuc'] == "NK5"){?>style="display: block;"<?php }?>>
                                    <div class="d_flex space_b wh768">
                                        <div class="d_flex width_create mb_15">
                                            <div class="w_100 seclec2_radius">
                                                <p class="font_s15 line_h18 font_w500 color_grey">Trạng thái</p>
                                                <div class="select_create mt_5">
                                                    <select class="select_create select_status" name="select_status_4" id="select_status_4" <?= ($_SESSION['quyen'] == '2') ? "disabled" : "" ?> onchange="change_trangthai(this)">
                                                        <option value="1" <?= $info_item['kcxl_trangThai'] == 1 ? "selected" : "" ?>>Khởi tạo</option>
                                                        <option value="2" <?= $info_item['kcxl_trangThai'] == 2 ? "selected" : "" ?>>Từ chối</option>
                                                        <?php if($_SESSION['quyen'] == '1'){?>
                                                            <option value="3" <?= $info_item['kcxl_trangThai'] == 3 ? "selected" : "" ?>>Đã duyệt chờ nhập kho</option>
                                                        <?php }?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="w_50 ml_10 date_accses" style="display:none">
                                                <div class="date_accses_4">
                                                    <div class="d_flex">
                                                        <p class="font_s15 line_h18 font_w500 color_grey">Ngày hoàn thành</p>
                                                        <span class="color_red alert_red">*</span>
                                                    </div>
                                                    <div class="select_create">
                                                        <input class="input_value_w ngay_ht_4" type="date" name="ngay_ht_4" id="ngay_ht_4">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="d_flex flex_column width_create mb_15">
                                            <div class="d_flex align_c space_b">
                                                <p class="font_s15 line_h18 font_w500 color_grey">Người giao</p>
                                            </div>
                                            <input class="nguoi_giao_5 font_14 line_h16" value=<?= $info_item['kcxl_nguoiThucHien']?> type="text" placeholder="Nhập tên người giao">
                                        </div>
                                    </div>
                                    <div class="d_flex space_b wh768">
                                        <div class="d_flex flex_column width_create mb_15">
                                            <div class="d_flex align_c space_b">
                                                <p class="font_s15 line_h18 font_w500 color_grey">Người nhận</p>
                                                <a class="color_blue font_s15 line_h18 font_w400 d_flex align_c" href="#">
                                                    <span class="font_s24">+</span>
                                                    Thêm nhân viên
                                                </a>
                                            </div>
                                            <select name="select_staff_4" id="select_staff_4" class="color_grey font_s14 line_h17font_w400 select_staff" style="width: 100%;">
                                                <option value=""></option>
                                                <? foreach ($data_list_nv as $item) { ?>
                                                    <option value="<?= $item['ep_id'] ?>" <?= ($item['ep_id'] == $info_item['kcxl_nguoiNhan']) ? "selected" : "" ?>><?= $item['ep_name']?></option>
                                                <? } ?>
                                            </select>
                                        </div>
                                        <div class="p_nhan_nhapKhac_nhapkho d_flex flex_column width_create mb_15">
                                            <div class="">
                                                <p class="font_s15 line_h18 font_w500 color_grey mb_5">Phòng ban</p>
                                                <input class="input_value_grey font_14 line_h16 phong_giao_4" type="text" value="<?= $newArr[$id_nguoi_nhan]['dep_name']?>" placeholder="Hiển thị phòng ban người nhận." readonly>
                                                <input class="input_value_grey font_14 line_h16 id_phong_giao_4" type="hidden" value="<?=$info_item['kcxl_phongBanNguoiNhan']?>" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="d_flex space_b wh768">
                                        <div class="d_flex flex_column wh_imp width_create mb_15">
                                            <div class="tb_kn_4">
                                                <p class="color_grey font_s15 line_h18 font_w500">Kho nhập<span style="color: red;">*</span></p>
                                                <select name="select_wh_in_4" id="select_wh_in_4" class="select_wh_in color_grey font_s14 line_h17 font_w400" style="width:100%;">
                                                    <option value=""></option>
                                                    <? while ($khoNhap4 = mysql_fetch_assoc($kho2->result)) { ?>
                                                        <option value="<?= $khoNhap4['kho_id'] ?>" <?= ($khoNhap4['kho_id'] == $info_item['kcxl_khoNhap']) ? "selected" : "" ?>><?= $khoNhap4['kho_name'] ?></option>
                                                    <? } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="d_flex flex_column date_imp_wh width_create mb_15">
                                            <div class="tb_date_imp_wh_4">
                                                <div class="d_flex align_c">
                                                    <p class="font_s15 line_h18 font_w500 color_grey mb_5">Ngày nhập kho</p>
                                                    <span class="color_red alert_red">*</span>
                                                </div>
                                                <input style="width:100%" name="date_imp_wh_4" id="date_imp_wh_4" class="date_imp_wh_4 font_14 line_h16" type="date" value="<?= $info_item['kcxl_ngayNhapKho']?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!-- Nhập theo yêu cầu vật tư -->
                                <div class="detail_input_wh_5 display_none" <?php if($info_item['kcxl_hinhThuc'] == "NK4"){?>style="display: block;"<?php }?>>
                                    <div class="d_flex space_b wh768">
                                        <div class="d_flex flex_column rq_note width_create mb_15">
                                            <div class="tb_rq_note_5">
                                                <div class="d_flex">
                                                    <p class="font_s15 line_h18 font_w500 color_grey mb_5">Phiếu yêu cầu</p>
                                                    <span class="color_red alert_red">*</span>
                                                </div>
                                                <select class="font_14 line_h16 font_w400 rq_note_5" name="rq_note_5" id="rq_note_5">
                                                    <option value=""></option>
                                                    <? foreach ($emp1 as $key => $value) { ?>
                                                        <option value="<?= $value['id'] ?>" <?= ($value['id'] == $info_item['kcxl_phieuYeuCau']) ? "selected" : ""?>>YC - <?= $value['id'] ?></option>
                                                    <? } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="d_flex width_create mb_15">
                                            <div class="w_100 ">
                                                <p class="font_s15 line_h18 font_w500 color_grey" style="width:100%">Trạng thái</p>
                                                <div class="select_create mt_5">
                                                    <select class="select_create select_status" name="select_status_5" id="select_status_5" <?= ($_SESSION['quyen'] == '2') ? "disabled" : "" ?> onchange="change_trangthai(this)">
                                                        <option value="1" <?= $info_item['kcxl_trangThai'] == 1 ? "selected" : "" ?>>Khởi tạo</option>
                                                        <option value="2" <?= $info_item['kcxl_trangThai'] == 2 ? "selected" : "" ?>>Từ chối</option>
                                                        <?php if($_SESSION['quyen'] == '1'){?>
                                                            <option value="3" <?= $info_item['kcxl_trangThai'] == 3 ? "selected" : "" ?>>Đã duyệt chờ nhập kho</option>
                                                        <?php }?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="w_50 ml_10 date_accses" style="display:none">
                                                <div class="tb_date_ht_5">
                                                    <div class="d_flex">
                                                        <p class="font_s15 line_h18 font_w500 color_grey">Ngày hoàn thành</p>
                                                        <span class="color_red alert_red">*</span>
                                                    </div>
                                                    <div class="select_create">
                                                        <input class="input_value_w date_ht_5" type="date" name="date_ht_5" id="date_ht_5">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="d_flex space_b wh768">
                                        <div class="d_flex flex_column width_create mb_15">
                                            <div class="d_flex align_c space_b">
                                                <p class="font_s15 line_h18 font_w500 color_grey">Người giao hàng</p>
                                                <a class="color_blue font_s15 line_h18 font_w400 d_flex align_c" href="#">
                                                    <span class="font_s24">+</span>
                                                    Thêm nhân viên
                                                </a>
                                            </div>
                                            <select name="select_shipper_dis_5" id="select_shipper_dis_5" class="color_grey font_s14 line_h17font_w400 select_shipper_dis" style="width: 100%">
                                                <option value="">Chọn người giao hàng</option>
                                                <? foreach ($data_list_nv as $item) { ?>
                                                    <option value="<?= $item['ep_id'] ?>" <?= ($item['ep_id'] == $info_item['kcxl_nguoiThucHien']) ? "selected" : "" ?>><?= $item['ep_name'] . ' ' . '-' . ' ' . $item['dep_name'] ?></option>
                                                <? } ?>
                                            </select>
                                        </div>
                                        <div class="p_giao_ycvt d_flex flex_column width_create mb_15">
                                            <div class="">
                                                <p class="font_s15 line_h18 font_w500 color_grey mb_5">Phòng ban</p>
                                                <input class=" font_14 line_h16 phong_giao_5" style="width:100%;" value="<?= $newArr[$id_nguoi_giao]['dep_name']?>" type="text" placeholder="Hiển thị phòng ban người giao hàng" readonly>
                                                <input class=" font_14 line_h16 id_phong_giao_5" style="width:100%;" value="<?=$info_item['kcxl_phongBanNguoiGiao']?>" type="hidden" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="d_flex align_c space_b wh768">
                                        <div class="d_flex flex_column width_create mb_15">
                                            <p class="font_s15 line_h18 font_w500 color_grey">Người Nhận</p>
                                            <?php $id_nguoi_nhan = $info_item['kcxl_nguoiNhan']; 
                                                if($id_nguoi_nhan != 0){
                                                    $name_nguoi_nhan = $newArr[$id_nguoi_nhan]['ep_name'];
                                                }else{
                                                    $name_nguoi_nhan = $tt_user['com_name'];
                                                }
                                            ?>
                                            <?php if($_SESSION['quyen'] == 2){ ?>
                                                <input class="input_value_grey font_14 line_h16 nguoi_nhan_5" type="text" value="<?= $name_nguoi_nhan ?>" placeholder="Hiển thị tên người nhận từ phiếu yêu cầu." readonly>
                                                <input class="input_value_grey font_14 line_h16 id_nguoi_nhan_5" type="hidden" value="<?=$info_item['kcxl_nguoiNhan']?>" readonly>
                                            <?}else{?>
                                                <input class="input_value_grey font_14 line_h16 nguoi_nhan_5" type="text" value="<?= $name_nguoi_nhan ?>" placeholder="Hiển thị tên người nhận từ phiếu yêu cầu." readonly>
                                                <input class="input_value_grey font_14 line_h16 id_nguoi_nhan_5" type="hidden" value="0" readonly>
                                            <?}?>
                                            </div>
                                        <div class="d_flex flex_column width_create mb_15">
                                            <p class="font_s15 line_h18 font_w500 color_grey mb_5">Phòng ban</p>
                                            <input class="input_value_grey font_14 line_h16 phong_nhan_5" type="text" value="<?= $newArr[$id_nguoi_nhan]['dep_name']?>" placeholder="Hiển thị phòng ban người nhận từ phiếu yêu cầu." readonly>
                                            <input class="input_value_grey font_14 line_h16 id_phong_nhan_5" type="hidden" value="<?=$info_item['kcxl_phongBanNguoiNhan']?>" readonly>
                                        </div>
                                    </div>
                                    <div class="d_flex space_b wh768">
                                        <div class="d_flex flex_column wh_imp width_create mb_15">
                                            <div class="tb_select_wh_in_5">
                                                <p class="color_grey font_s15 line_h18 font_w500">Kho nhập<span style="color: red;">*</span></p>
                                                <select name="select_wh_in_5" id="select_wh_in_5" class="select_wh_in color_grey font_s14 line_h17 font_w400" style="width:100%;">
                                                    <option value=""></option>
                                                    <? while ($khoNhap5 = mysql_fetch_assoc($kho4->result)) { ?>
                                                        <option value="<?= $khoNhap5['kho_id'] ?>" <?= ($khoNhap5['kho_id'] == $info_item['kcxl_khoNhap']) ? "selected" : ""?>><?= $khoNhap5['kho_name'] ?></option>
                                                    <? } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="d_flex flex_column width_create mb_15">
                                            <div class="tb_date_nk_5">
                                                <div class="d_flex align_c">
                                                    <p class="font_s15 line_h18 font_w500 color_grey mb_5">Ngày nhập kho</p>
                                                    <span class="color_red alert_red">*</span>
                                                </div>
                                                <input style="width:100%" class=" font_14 line_h16 date_nk_5" name="date_nk_5" id="date_nk_5" type="date" value="<?=$info_item['kcxl_ngayNhapKho']?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="flex_column d_flex note_import_vote_infor">
                                <p class="font_s15 line_h18 font_w500 color_grey mb_5">Ghi chú</p>
                                <textarea name="ghi_chu" id="ghi_chu" rows="5" placeholder="Nhập nội dung"><?= $info_item['kcxl_ghi_chu']; ?></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- danh sách vật tư theo biên bản giao hàng -->
                    <div class="table_input_wh table_input_wh_1 mt_15 display_none" id="table_input_wh_1" <?php if($info_item['kcxl_hinhThuc'] == "NK1"){?>style="display: block;"<?php }?>>
                        <p class="tit_table_vt color_blue font_s16 line_h19 font_w700"> Danh sách vật tư</p>
                        <div class="position_r d_flex align_c">
                            <div class="main_tabl table_vt_scr" onscroll="table_scroll(this)">
                                <table class="table table_list_meterial" style="width:1689px;">
                                    <tr>
                                        <th>STT
                                            <span class="span_thread"></span>
                                        </th>
                                        <th>Mã vật tư
                                            <span class="span_thread"></span>
                                        </th>
                                        <th>Tên đầy đủ vật tư thiết bị
                                            <span class="span_thread"></span>
                                        </th>
                                        <th>Đơn vị tính
                                            <span class="span_thread"></span>
                                        </th>
                                        <th> Hãng sản xuất
                                            <span class="span_thread"></span>
                                        </th>
                                        <th> Xuất xứ
                                            <span class="span_thread"></span>
                                        </th>
                                        <th> Số lượng theo đơn hàng
                                            <span class="span_thread"></span>
                                        </th>
                                        <th> Số lượng thực tế nhập kho
                                            <span class="span_thread"></span>
                                        </th>
                                        <th>Đơn giá (VNĐ)
                                            <span class="span_thread"></span>
                                        </th>
                                        <th>Thành tiền (VNĐ)
                                        </th>
                                    </tr>
                                    <?php $stt = 1; foreach($list_vtdh1 as $dsvt){
                                        $id_dsvt = $dsvt['id_vat_tu'];
                                        $sql_dsvt = new db_query("SELECT `dvt_name`, `hsx_name`, `xx_name`, `dsvt_donGia`, `dsvt_name` FROM `danh-sach-vat-tu`
                                            LEFT JOIN `don-vi-tinh` ON `dsvt_donViTinh` = `dvt_id` AND `dvt_check` = 1
                                            LEFT JOIN `hang-san-xuat` ON `dsvt_hangSanXuat` = `hsx_id` AND `hsx_check` = 1
                                            LEFT JOIN `xuat-xu` ON `dsvt_xuatXu` = `xx_id`
                                            WHERE `dsvt_id` = $id_dsvt AND `dsvt_check` = 1 AND `dsvt_id_ct` = $id_cty
                                        ");
                                        $info_dsvt = mysql_fetch_assoc($sql_dsvt->result);

                                        $sql_sl = new db_query("SELECT `slvt_soLuongNhapKho` FROM `so-luong-vat-tu` WHERE `slvt_maVatTuThietBi` = $id_dsvt AND `slvt_idPhieu` = $id_phieu AND `slvt_id_ct` = $id_cty");
                                        $sl = mysql_fetch_assoc($sql_sl->result)['slvt_soLuongNhapKho'];
                                        ?>
                                    <tr class="color_grey font_s14 line_h17 font_w400 table_3" data-id="<?=$id_dsvt?>">
                                        <td class="font_s14 line_h17 color_grey font_w400"><?= $stt++; ?></td>
                                        <td class="font_s14 line_h17 color_grey font_w400">VT -  <?= $id_dsvt; ?></td>
                                        <td class="font_s14 line_h17 color_blue font_w500" style="text-align: left;"><?= $info_dsvt['dsvt_name']; ?></td>
                                        <td class="font_s14 line_h17 color_grey font_w400"><?=$info_dsvt['dvt_name']?></td>
                                        <td class="font_s14 line_h17 color_grey font_w400" style="text-align: left;"><?=$info_dsvt['hsx_name']?></td>
                                        <td class="font_s14 line_h17 color_grey font_w400"><?=$info_dsvt['xx_name']?></td>
                                        <td class="font_s14 line_h17 color_grey font_w400" style="text-align: right;background:#EEEEEE;"><?=$dsvt['so_luong_ky_nay']?></td>
                                        <td class="font_s14 line_h17 color_grey font_w400" style="text-align: right;"><input class="nhap_so_luong" value="<?=$sl?>" placeholder="Nhập số lượng" oninput="<?=$oninput?>" onkeyup=tong_vt(this)></td>
                                        <td class="font_s14 line_h17 color_grey font_w400 don_gia_3" style="text-align: right;"><?= number_format($info_dsvt['dsvt_donGia'],0,'','.'); ?></td>
                                        <td class="font_s14 line_h17 color_grey font_w400 thanh_tien_3" style="text-align: right;"><?= number_format($sl*$info_dsvt['dsvt_donGia'],0,'','.'); ?></td>
                                    </tr>
                                    <?php } ?>
                                </table>
                            </div>
                            <div class="pre_q d_flex align_c flex_center position_a display_none" onclick="pre_q(this)">
                                <span class="pre_arrow"></span>
                            </div>
                            <div class="next_q d_flex align_c flex_center position_a" onclick="next_q(this)">
                                <span class="next_arrow"></span>
                            </div>
                        </div>
                    </div>
                <!-- danh sách vật tư điều chuyển -->
                    <div class="table_input_wh table_input_wh_2 mt_15 display_none" id="table_input_wh_2" <?php if($info_item['kcxl_hinhThuc'] == "NK2"){?>style="display: block;"<?php }?>>
                        <p class="tit_table_vt color_blue font_s16 line_h19 font_w700"> Danh sách vật tư</p>
                        <div class="position_r d_flex align_c">
                            <div class="table_vt_scr">
                                <table class="table table_2">
                                    <tr class="color_white font_s16 line_h19 font_w500">
                                        <th>STT
                                            <span class="span_tbody"></span>
                                        </th>
                                        <th>Mã vật tư thiết bị
                                            <span class="span_tbody"></span>
                                        </th>
                                        <th>Tên đầy đủ vật tư thiết bị
                                            <span class="span_tbody"></span>
                                        </th>
                                        <th>Đơn vị tính
                                            <span class="span_tbody"></span>
                                        </th>
                                        <th>Hãng sản xuất
                                            <span class="span_tbody"></span>
                                        </th>
                                        <th>Xuất xứ
                                            <span class="span_tbody"></span>
                                        </th>
                                        <th>Số lượng điều chuyển
                                            <span class="span_tbody"></span>
                                        </th>
                                        <th>Số lượng thực tế nhập kho
                                            <span class="span_tbody"></span>
                                        </th>
                                        <th>Đơn giá (VNĐ)
                                            <span class="span_tbody"></span>
                                        </th>
                                        <th>Thành tiền (VNĐ)
                                        </th>
                                    </tr>
                                    <tbody class="data_table_2">
                                    <?php 
                                        $vattu_nhap2 = new db_query("SELECT `dsvt_id`,`slvt_idPhieu`, `slvt_maPhieu`, `slvt_checkDieuChuyen`,
                                        `dsvt_maVatTuThietBi`, `dsvt_name`, `dvt_name`, `hsx_name`, `xx_name`, `slvt_soLuongNhapKho`,`dsvt_donGia`,`slvt_soLuongDieuChuyen`
                                        FROM `danh-sach-vat-tu`
                                        LEFT JOIN `don-vi-tinh` ON `dsvt_donViTinh` =`dvt_id`
                                        LEFT JOIN `hang-san-xuat` ON `dsvt_hangSanXuat` = `hsx_id`
                                        LEFT JOIN `xuat-xu` ON `dsvt_xuatXu` = `xx_id`
                                        LEFT JOIN `so-luong-vat-tu` ON `dsvt_id` = `slvt_maVatTuThietBi`
                                        LEFT JOIN `kho-cho-xu-li` ON `slvt_idPhieu` = `kcxl_id`
                                        WHERE `dsvt_check` = '1' AND `slvt_idPhieu` = '$id_phieu' AND `slvt_checkDieuChuyen` = '$id_phieu_dc' AND `dsvt_id_ct` = $id_cty
                                        ORDER BY `dsvt_id` ASC"); 
                                        $st = 1;
                                    ?>
                                    <? while($vt_nhap2 = mysql_fetch_assoc($vattu_nhap2 -> result)){?>
                                        <tr class="color_grey font_s14 line_h17 font_w400 table_3 delete_3" data-id ="<?=$vt_nhap2['dsvt_id']?>">
                                            <td><?= $st++; ?></td>
                                            <td>VT - <?= $vt_nhap2['dsvt_id'] ?></td>
                                            <td class="color_blue font_s14 line_h17 font_w500"><?= $vt_nhap2['dsvt_name']; ?></td>
                                            <td><?= $vt_nhap2['dvt_name']; ?></td>
                                            <td><?= $vt_nhap2['hsx_name']; ?></td>
                                            <td><?= $vt_nhap2['xx_name']; ?></td>
                                            <?php
                                                $idvt_dc = $vt_nhap2['dsvt_id'];
                                                $sl_dc = mysql_fetch_assoc((new db_query("SELECT `slvt_soLuongDieuChuyen` FROM `danh-sach-vat-tu`
                                                LEFT JOIN `so-luong-vat-tu` ON `dsvt_id` = `slvt_maVatTuThietBi`
                                                LEFT JOIN `kho-cho-xu-li` ON `slvt_idPhieu` = `kcxl_id`
                                                WHERE `kcxl_check` = '1' AND `slvt_idPhieu` = '$id_phieu_dc' AND `dsvt_id` = '$idvt_dc' AND `kcxl_id_ct` = $id_cty
                                                "))->result)['slvt_soLuongDieuChuyen'];
                                            ?>
                                            <td style="background: #EEEEEE;"><?= $sl_dc ?></td>
                                            
                                            <td><input type="text" value="<?= $vt_nhap2['slvt_soLuongNhapKho']; ?>" class="nhap_so_luong_1 delete_3 color_grey font_s14 line_h17 font_w400" placeholder="Nhập số lượng" onkeyup="tong_vt(this)"></td>
                                            <td class="don_gia_3 delete_3"><?= number_format($vt_nhap2['dsvt_donGia'],0,'','.'); ?></td>
                                            <td class="thanh_tien_3 delete_3"><?= number_format($vt_nhap2['dsvt_donGia'] * $vt_nhap2['slvt_soLuongNhapKho'],0,'','.')?></td>
                                        </tr>
                                    <? }?>
                                    </tbody>
                                </table>
                            </div>
                            <div class="pre_q d_flex align_c flex_center position_a">
                                <span class="pre_arrow"></span>
                            </div>
                            <div class="next_q d_flex align_c flex_center position_a">
                                <span class="next_arrow"></span>
                            </div>
                        </div>
                    </div>
                <!-- danh sách vật tư trả lại từ thi công -->
                    <?php
                        $id_kho_edit = $info_item['kcxl_khoNhap'];

                        $list_dsvt = new db_query("SELECT `dsvt_id`, `dsvt_maVatTuThietBi`, `dsvt_name`, `hsx_name`, `dvt_name`, `xx_name`, `dsvt_donGia`, `slvt_soLuongNhapKho`
                        FROM `danh-sach-vat-tu`
                        LEFT JOIN `hang-san-xuat` on `dsvt_hangSanXuat` = `hsx_id` AND `hsx_check` = 1    
                        LEFT JOIN `don-vi-tinh` on `dsvt_donViTinh` = `dvt_id` AND `dvt_check` = 1 
                        LEFT JOIN `xuat-xu` on `dsvt_xuatXu` = `xx_id`
                        LEFT JOIN `so-luong-vat-tu` on `slvt_maVatTuThietBi` = `dsvt_id`
                        LEFT JOIN `kho-cho-xu-li` on `kcxl_id` = `kcxl_soPhieu`
                        WHERE `dsvt_check` = 1 AND `slvt_idPhieu` = $id_phieu AND `dsvt_id_ct` = $id_cty");

                        $list_all = new db_query("SELECT `dsvt_id`, `dsvt_name`, `dsvt_kho`, `hsx_name`, `dvt_name`, `xx_name`,`dsvt_donGia` FROM `danh-sach-vat-tu`
                        LEFT JOIN `hang-san-xuat` on `dsvt_hangSanXuat` = `hsx_id`       
                        LEFT JOIN `don-vi-tinh` on `dsvt_donViTinh` = `dvt_id`
                        LEFT JOIN `xuat-xu` on `dsvt_xuatXu` = `xx_id`
                        WHERE `dsvt_check` = 1 AND `dsvt_id_ct` = $id_cty");
                        $responsive = [];
                        while (($selected = mysql_fetch_assoc($list_all->result))) {
                            $check_kho = explode(',',$selected['dsvt_kho']);
                            if(in_array($id_kho_edit,$check_kho)){
                                $item['id'] = $selected['dsvt_id'];
                                $item['ten_vt'] = $selected['dsvt_name'];
                                $item['ten_hang'] = $selected['hsx_name'];
                                $item['ten_dvt'] = $selected['dvt_name'];
                                $item['ten_xx'] = $selected['xx_name'];
                                $item['don_gia'] = $selected['dsvt_donGia'];
                                $responsive[$selected['dsvt_id']] = $item;
                            }
                        }
                    ?>
                    <div class="table_input_wh table_input_wh_3 mt_15 display_none" id="table_input_wh_3" <?php if($info_item['kcxl_hinhThuc'] == "NK3"){?>style="display: block;"<?php }?>>
                        <p class="tit_table_vt color_blue font_s16 line_h19 font_w700"> Danh sách vật tư</p>
                        <div class="position_r align_c d_flex">
                            <div class="main_table table_vt_scr" onscroll="table_scroll(this)">
                                <table class="table table_list_meterial-3" style="width: 1432px;">
                                    <tr>
                                        <th>
                                            <span class="span_thread"></span>
                                        </th>
                                        <th>Mã vật tư
                                            <span class="span_thread"></span>
                                        </th>
                                        <th>Tên đầy đủ vật tư thiết bị
                                            <span class="span_thread"></span>
                                        </th>
                                        <th>Đơn vị tính
                                            <span class="span_thread"></span>
                                        </th>
                                        <th> Hãng sản xuất
                                            <span class="span_thread"></span>
                                        </th>
                                        <th> Xuất xứ
                                            <span class="span_thread"></span>
                                        </th>
                                        <th> Số lượng nhập kho
                                            <span class="span_thread"></span>
                                        </th>
                                        <th>Đơn giá (VNĐ)
                                            <span class="span_thread"></span>
                                        </th>
                                        <th>Thành tiền (VNĐ)
                                            <span class="span_thread"></span>
                                        </th>
                                    </tr>
                                    <?php $stt = 1; while($row_dsvt_tc = mysql_fetch_assoc($list_dsvt->result)) {?>
                                        <tr class="color_grey font_s14 line_h17 font_w400 table_3 delete_3" data-id="<?= $row_dsvt_tc['dsvt_id']?>">
                                            <td onclick="deleteRow(this)"><img class="cursor_p delete_3" src="../images/del_row.png" alt="" style="margin: 0; width: 16px; height: 16px;"></td>
                                            <td class="ma_vat_tu_3 delete_3" style="background-color: #EEEEEE;">VT - <?= $row_dsvt_tc['dsvt_id'] ?></td>
                                            <td class="color_blue font_w500 delete_3" style="text-align: left;">
                                                <select class="select_tb color_grey3 font_s14 line_h17 font_w400 select_tb_3 delete_3" style="width: 100%" onchange="change_vt(this)">
                                                    <option value=""></option>
                                                    <? foreach ($responsive as $item) { ?>
                                                        <option value="<?= $item['id']; ?>" <?= ($item['id'] == $row_dsvt_tc['dsvt_id']) ? "selected" : ""?>><?= $item['ten_vt'] ?></option>
                                                    <? } ?>
                                                </select>
                                            </td>
                                            <td class="don_vi_tinh_3 delete_3" style="background-color: #EEEEEE;"><?= $row_dsvt_tc['dvt_name']; ?></td>
                                            <td class="hang_san_xuat_3 delete_3" style="text-align: left; background-color: #EEEEEE;"><?= $row_dsvt_tc['hsx_name']; ?></td>
                                            <td class="xuat_xu_3 delete_3"  style="background-color: #EEEEEE;"><?= $row_dsvt_tc['xx_name']; ?></td>
                                            <td><input type="text" value="<?= $row_dsvt_tc['slvt_soLuongNhapKho']; ?>" class="nhap_sl_3 delete_3 color_grey font_s14 line_h17 font_w400" placeholder="Nhập số lượng" onkeyup="tong_vt(this)"></td>
                                            <td class="don_gia_3 delete_3" style="background-color: #EEEEEE;"><?= number_format($row_dsvt_tc['dsvt_donGia'],0,'','.'); ?></td>
                                            <td class="thanh_tien_3 delete_3" style="background-color: #EEEEEE;"><?= (number_format($row_dsvt_tc['slvt_soLuongNhapKho']*$row_dsvt_tc['dsvt_donGia'],0,'','.')); ?></td>
                                        </tr>
                                    <?php } ?>
                                    <tbody id="data_table_3"></tbody>
                                    <tr class="color_blue font_s14 line_h17 font_w500 ">
                                        <td colspan="2">
                                            <div class=" d_flex align_c cursor_p add_row_wh">
                                                <p class="add_vt d_flex flex_center align_c position_r"><span class="color_white font_s20 position_a">+</span></p>
                                                <p class="">Thêm vật tư</p>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="pre_q d_flex align_c flex_center position_a display_none" onclick="pre_q(this)">
                                <span class="pre_arrow"></span>
                            </div>
                            <div class="next_q d_flex align_c flex_center position_a" onclick="next_q(this)">
                                <span class="next_arrow"></span>
                            </div>
                        </div>
                    </div>
                <!-- danh sách vật tư nhập khác -->
                    <div class="table_input_wh table_input_wh_4 mt_15 display_none" id="table_input_wh_4" <?php if($info_item['kcxl_hinhThuc'] == "NK5"){?>style="display: block;"<?php }?>>
                        <p class="tit_table_vt color_blue font_s16 line_h19 font_w700"> Danh sách vật tư</p>
                        <div class="position_r align_c d_flex">
                            <div class="main_table table_vt_scr">
                                <table class="table table_list_meterial-3" style="width: 1432px;">
                                    <tr>
                                        <th>
                                            <span class="span_thread"></span>
                                        </th>
                                        <th>Mã vật tư
                                            <span class="span_thread"></span>
                                        </th>
                                        <th>Tên đầy đủ vật tư thiết bị
                                            <span class="span_thread"></span>
                                        </th>
                                        <th>Đơn vị tính
                                            <span class="span_thread"></span>
                                        </th>
                                        <th> Hãng sản xuất
                                            <span class="span_thread"></span>
                                        </th>
                                        <th> Xuất xứ
                                            <span class="span_thread"></span>
                                        </th>
                                        <th> Số lượng nhập kho
                                            <span class="span_thread"></span>
                                        </th>
                                        <th>Đơn giá (VNĐ)
                                            <span class="span_thread"></span>
                                        </th>
                                        <th>Thành tiền (VNĐ)
                                            <span class="span_thread"></span>
                                        </th>
                                    </tr>
                                    <?php 
                                       $id_kho_edit = $info_item['kcxl_khoNhap'];

                                       $list_dsvt = new db_query("SELECT `dsvt_id`, `dsvt_maVatTuThietBi`, `dsvt_name`, `hsx_name`, `dvt_name`, `xx_name`, `dsvt_donGia`, `slvt_soLuongNhapKho`
                                       FROM `danh-sach-vat-tu`
                                       LEFT JOIN `hang-san-xuat` on `dsvt_hangSanXuat` = `hsx_id` AND `hsx_check` = 1       
                                       LEFT JOIN `don-vi-tinh` on `dsvt_donViTinh` = `dvt_id` AND `dvt_check` = 1
                                       LEFT JOIN `xuat-xu` on `dsvt_xuatXu` = `xx_id`
                                       LEFT JOIN `so-luong-vat-tu` on `slvt_maVatTuThietBi` = `dsvt_id`
                                       LEFT JOIN `kho-cho-xu-li` on `kcxl_id` = `kcxl_soPhieu`
                                       WHERE `dsvt_check` = 1 AND `slvt_idPhieu` = $id_phieu AND `dsvt_id_ct` = $id_cty");
               
                                       $list_all = new db_query("SELECT `dsvt_id`, `dsvt_name`, `dsvt_kho`, `hsx_name`, `dvt_name`, `xx_name`,`dsvt_donGia` FROM `danh-sach-vat-tu`
                                       LEFT JOIN `hang-san-xuat` on `dsvt_hangSanXuat` = `hsx_id`       
                                       LEFT JOIN `don-vi-tinh` on `dsvt_donViTinh` = `dvt_id`
                                       LEFT JOIN `xuat-xu` on `dsvt_xuatXu` = `xx_id`
                                       WHERE `dsvt_check` = 1 AND `dsvt_id_ct` = $id_cty");
                                       $responsive = [];
                                       while (($selected = mysql_fetch_assoc($list_all->result))) {
                                            $check_kho = explode(',',$selected['dsvt_kho']);
                                            if(in_array($id_kho_edit,$check_kho)){
                                                $item['id'] = $selected['dsvt_id'];
                                                $item['ten_vt'] = $selected['dsvt_name'];
                                                $item['ten_hang'] = $selected['hsx_name'];
                                                $item['ten_dvt'] = $selected['dvt_name'];
                                                $item['ten_xx'] = $selected['xx_name'];
                                                $item['don_gia'] = $selected['dsvt_donGia'];
                                                $responsive[$selected['dsvt_id']] = $item;
                                            }
                                       }
                                       ?>
                                    <?php $stt = 1; while($row_dsvt_tc = mysql_fetch_assoc($list_dsvt->result)) {?>
                                        <tr class="color_grey font_s14 line_h17 font_w400 table_3 delete_3">
                                            <td onclick="deleteRow(this)"><img class="cursor_p delete_3" src="../images/del_row.png" alt="" style="margin: 0; width: 16px; height: 16px;"></td>
                                            <td class="ma_vat_tu_3 delete_3" style="background-color: #EEEEEE;">VT - <?= $row_dsvt_tc['dsvt_id'] ?></td>
                                            <td class="color_blue font_w500 delete_3" style="text-align: left;">
                                                <select class="select_tb color_grey3 font_s14 line_h17 font_w400 select_tb_3 delete_3" style="width: 100%" onchange="change_vt(this)">
                                                    <option value=""></option>
                                                    <? foreach ($responsive as $item) { ?>
                                                        <option value="<?= $item['id']; ?>" <?= ($item['id'] == $row_dsvt_tc['dsvt_id']) ? "selected" : ""?>><?= $item['ten_vt'] ?></option>
                                                    <? } ?>
                                                </select>
                                            </td>
                                            <td class="don_vi_tinh_3 delete_3" style="background-color: #EEEEEE;"><?= $row_dsvt_tc['dvt_name']; ?></td>
                                            <td class="hang_san_xuat_3 delete_3" style="text-align: left; background-color: #EEEEEE;"><?= $row_dsvt_tc['hsx_name']; ?></td>
                                            <td class="xuat_xu_3 delete_3"  style="background-color: #EEEEEE;"><?= $row_dsvt_tc['xx_name']; ?></td>
                                            <td><input type="text" value="<?= $row_dsvt_tc['slvt_soLuongNhapKho']; ?>" class="nhap_sl_4 delete_3 color_grey font_s14 line_h17 font_w400" placeholder="Nhập số lượng" onkeyup="tong_vt(this)"></td>
                                            <td class="don_gia_3 delete_3" style="background-color: #EEEEEE;"><?= number_format($row_dsvt_tc['dsvt_donGia'],0,'','.'); ?></td>
                                            <td class="thanh_tien_3 delete_3" style="background-color: #EEEEEE;"><?= (number_format($row_dsvt_tc['slvt_soLuongNhapKho']*$row_dsvt_tc['dsvt_donGia'],0,'','.')); ?></td>
                                        </tr>
                                    <?php } ?>
                                    <tbody id="data_table_4"></tbody>
                                    <tr class="color_blue font_s14 line_h17 font_w500 ">
                                        <td colspan="2">
                                            <div class=" d_flex align_c cursor_p add_row_wh_4">
                                                <p class="add_vt d_flex flex_center align_c position_r"><span class="color_white font_s20 position_a">+</span></p>
                                                <p class="">Thêm vật tư</p>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="pre_q d_flex align_c flex_center position_a">
                                <span class="pre_arrow"></span>
                            </div>
                            <div class="next_q d_flex align_c flex_center position_a">
                                <span class="next_arrow"></span>
                            </div>
                        </div>
                    </div>
                 <!-- danh sách vật tư nhập theo yêu cầu vật tư -->
                    <div class="table_input_wh table_input_wh_5 mt_15 display_none" id="table_input_wh_5" <?php if($info_item['kcxl_hinhThuc'] == "NK4"){?>style="display: block;"<?php }?>>
                        <p class="tit_table_vt color_blue font_s16 line_h19 font_w700"> Danh sách vật tư</p>
                        <div class="position_r align_c d_flex">
                            <div class="main_table main_table_yc table_vt_scr">
                                <table class="table table_list_meterial-5" style="width: 1855px;">
                                    <tr>
                                        <th>STT
                                            <span class="span_thread"></span>
                                        </th>
                                        <th>Mã vật tư
                                            <span class="span_thread"></span>
                                        </th>
                                        <th>Tên đầy đủ vật tư thiết bị
                                            <span class="span_thread"></span>
                                        </th>
                                        <th>Đơn vị tính
                                            <span class="span_thread"></span>
                                        </th>
                                        <th> Hãng sản xuất
                                            <span class="span_thread"></span>
                                        </th>
                                        <th> Xuất xứ
                                            <span class="span_thread"></span>
                                        </th>
                                        <th> Số lượng theo yêu cầu
                                            <span class="span_thread"></span>
                                        </th>
                                        <th> Số lượng được duyệt
                                            <span class="span_thread"></span>
                                        </th>
                                        <th> Số lượng thực tế nhập kho
                                            <span class="span_thread"></span>
                                        </th>
                                        <th>Đơn giá (VNĐ)
                                            <span class="span_thread"></span>
                                        </th>
                                        <th>Thành tiền (VNĐ)
                                            <span class="span_thread"></span>
                                        </th>
                                    </tr>
                                    <?php $stt = 1; foreach($emp_vt_yc as $dsvt){
                                        $id_dsvt = $dsvt['id_vat_tu'];
                                        $sql_dsvt = new db_query("SELECT `dvt_name`, `hsx_name`, `xx_name`, `dsvt_donGia`, `dsvt_name` FROM `danh-sach-vat-tu`
                                            LEFT JOIN `don-vi-tinh` ON `dsvt_donViTinh` = `dvt_id` AND `dvt_check` = 1
                                            LEFT JOIN `hang-san-xuat` ON `dsvt_hangSanXuat` = `hsx_id` AND `hsx_check` = 1
                                            LEFT JOIN `xuat-xu` ON `dsvt_xuatXu` = `xx_id`
                                            WHERE `dsvt_id` = $id_dsvt AND `dsvt_check` = 1 AND `dsvt_id_ct` = $id_cty
                                        ");
                                        $info_dsvt = mysql_fetch_assoc($sql_dsvt->result);

                                        $sql_sl = new db_query("SELECT `slvt_soLuongNhapKho` FROM `so-luong-vat-tu` WHERE `slvt_maVatTuThietBi` = $id_dsvt AND `slvt_idPhieu` = $id_phieu AND `slvt_id_ct` = $id_cty");
                                        
                                        $sl = mysql_fetch_assoc($sql_sl->result)['slvt_soLuongNhapKho'];
                                        ?>
                                    <tr class="color_grey font_s14 line_h17 font_w400 table_3" data-id="<?=$id_dsvt?>">
                                        <td class="font_s14 line_h17 color_grey font_w400"><?= $stt++; ?></td>
                                        <td class="font_s14 line_h17 color_grey font_w400">VT -  <?= $id_dsvt; ?></td>
                                        <td class="font_s14 line_h17 color_blue font_w500" style="text-align: left;"><?= $info_dsvt['dsvt_name']; ?></td>
                                        <td class="font_s14 line_h17 color_grey font_w400"><?=$info_dsvt['dvt_name']?></td>
                                        <td class="font_s14 line_h17 color_grey font_w400" style="text-align: left;"><?=$info_dsvt['hsx_name']?></td>
                                        <td class="font_s14 line_h17 color_grey font_w400"><?=$info_dsvt['xx_name']?></td>
                                        <td class="font_s14 line_h17 color_grey font_w400" style="text-align: right;background:#EEEEEE;"><?=$dsvt['so_luong_yc_duyet']?></td>
                                        <td class="font_s14 line_h17 color_grey font_w400" style="text-align: right;background:#EEEEEE;"><?=$dsvt['so_luong_duyet']?></td>
                                        <td class="font_s14 line_h17 color_grey font_w400" style="text-align: right;"><input class="sl_vl_yc nhap_so_luong" value="<?=$sl?>" placeholder="Nhập số lượng" oninput="<?=$oninput?>" onkeyup=tong_vt(this)></td>
                                        <td class="font_s14 line_h17 color_grey font_w400 don_gia_3" style="text-align: right;"><?= number_format($info_dsvt['dsvt_donGia'],0,'','.'); ?></td>
                                        <td class="font_s14 line_h17 color_grey font_w400 thanh_tien_3" style="text-align: right;"><?= number_format($sl*$info_dsvt['dsvt_donGia'],0,'','.'); ?></td>
                                    </tr>
                                    <?php } ?>
                                </table>
                            </div>
                            <div class="pre_q d_flex align_c flex_center position_a">
                                <span class="pre_arrow"></span>
                            </div>
                            <div class="next_q d_flex align_c flex_center position_a">
                                <span class="next_arrow"></span>
                            </div>
                        </div>

                    </div>

                <div class="btn_cf_rq d_flex flex_center" style="display: flex;">
                    <button type="button" class="btn_cancel back_w color_blue font_s15 line_h18 font_w500">
                        <a href="nhap-kho.html"></a>
                        Hủy</button>
                    <button type="button" class="btn_save btn_nhapKhoCreate back_blue color_white font_s15 line_h18 font_w500">Lưu</button>
                </div>
            </form>
        </div>
        <?php include('../includes/popup_nhap-kho.php');  ?>
        
    </div>
    <?php include('../includes/popup_overview.php');  ?>
</body>
<script type="text/javascript" src="../js/jquery-3.4.1.min.js"></script>
<script type="text/javascript" src="../js/jquery.validate.min.js"></script>
<script type="text/javascript" src="../js/reset_validate.js"></script>
<script type="text/javascript" src="../js/select2.min.js"></script>
<script type="text/javascript" src="../js/js_h.js"></script>
<script type="text/javascript" src="../js/validate_h.js"></script>
<script type="text/javascript" src="../js/js_k.js"></script>
<script type="text/javascript" src="../js/js_n.js"></script>
<script>
    $('.active8').each(function() {
        if ($(this).hasClass('active8')) {
            $(this).find('a').addClass('active');
        }
    });
</script>
<script>
    var ctr_name_ct = <?= json_encode($list_phieu_vt1) ?>;
    var data = <?= json_encode($newArr) ?>;
    var pdc = <?= json_encode($arr_pdk) ?>;
    var dh = <?= json_encode($list_dh1) ?>;
    var pyc = <?= json_encode($emp1) ?>;
    var arr_dsvt_tb = <?= json_encode($arr_dsvt_tb) ?>;
    var arr_kho3 = <?= json_encode($arr_kho3) ?>;
    var id_phieu = <?= json_encode($id_phieu) ?>;
    var tt_user = <?= json_encode($tt_user) ?>;
</script>
<script type="text/javascript" src="../js/nhap_kho_chinh_sua.js"></script>

</html>