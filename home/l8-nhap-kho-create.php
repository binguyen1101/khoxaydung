<?php
    include("config1.php");
    if(!in_array(2,$ro_nhap_kho)){
        header("Location: /tong-quan.html");
    }

    if (isset($_COOKIE['user']) && $_COOKIE['user'] != "" && $_COOKIE['role'] == 1) {
		$token = $_COOKIE['acc_token'];
		$curl = curl_init();
		$data = array();
		curl_setopt($curl, CURLOPT_POST, 1);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
		curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/user_info_company.php');
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));
	
		$response = curl_exec($curl);
		curl_close($curl);
		$data_tt = json_decode($response, true);
		$tt_user = $data_tt['data']['user_info_result'];
	} else if (isset($_COOKIE['user']) && $_COOKIE['user'] != "" && $_COOKIE['role'] == 2) {
		$token = $_COOKIE['acc_token'];
		$curl = curl_init();
		$data = array();
		curl_setopt($curl, CURLOPT_POST, 1);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
		curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/user_info_employee.php');
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));
		$response = curl_exec($curl);
		curl_close($curl);
		$data_tt = json_decode($response, true);
		$tt_user = $data_tt['data']['user_info_result'];
	}
    $id_cty = $tt_user['com_id'];
    $date = date('Y-m-d', time());

    $item = new db_query("SELECT `kcxl_id`, `kcxl_hinhThuc`, `kcxl_nguoiTao`, `kcxl_ngayTao`, `kcxl_trangThai`, `kcxl_khoXuat`, `kcxl_ngayXuatKho`, `kcxl_ghi_chu`, `kho_name` FROM `kho-cho-xu-li`
    LEFT JOIN `kho` ON `kcxl_khoXuat` = `kho_id` WHERE `kcxl_soPhieu` = 'PNK' and `kcxl_check` = '1'");

    $select_khoNhap = new db_query("SELECT `kho_id`, `kho_maKho`, `kho_name` FROM `kho` WHERE `kho_id_ct` = $id_cty");

    // nhập khác
    $kho2 = new db_query("SELECT `kho_id`, `kho_maKho`, `kho_name` FROM `kho` WHERE `kho_id_ct` = $id_cty");

    //  Nhập theo yêu cầu vật tư
    $kho4 = new db_query("SELECT `kho_id`, `kho_maKho`, `kho_name` FROM `kho` WHERE `kho_id_ct` = $id_cty");
    
    // 
    $kho3 = new db_query("SELECT `kho_id`, `kho_maKho`, `kho_name` FROM `kho` WHERE `kho_id_ct` = $id_cty");
    $arr_kho3 = [];
    while ($row = mysql_fetch_assoc($kho3->result)) {
        $arr_kho3[$row["kho_id"]] = $row;
    }

    $pdk = new db_query("SELECT `kcxl`.`kcxl_id`, `kcxl`.`kcxl_nguoiTao`, `kcxl`.`kcxl_ngayTao`, `kcxl`.`kcxl_nguoiThucHien`, `kcxl`.`kcxl_phongBanNguoiGiao`, `kcxl`.`kcxl_nguoiNhan`, `kcxl`.`kcxl_phongBanNguoiNhan`, `kcxl`.`kcxl_khoNhap`, `kcxl`.`kcxl_khoXuat`, `kcxl`.`kcxl_ngayYeuCauHoanThanh`, `khoNhap`.`kho_name` as kho_nhap_name,`khoXuat`.`kho_name`as kho_xuat_name FROM `kho-cho-xu-li` kcxl
    LEFT JOIN `kho` `khoNhap` on `kcxl`.`kcxl_khoNhap` = `khoNhap`.`kho_id`       
    LEFT JOIN `kho` `khoXuat` on `kcxl`.`kcxl_khoXuat` = `khoXuat`.`kho_id`
    WHERE `kcxl_check` = 1 AND `kcxl_id_ct` = $id_cty AND `kcxl_trangThai` = 7 AND `kcxl_soPhieu` = 'ĐCK'");


    $arr_pdk = [];
    while ($row = mysql_fetch_assoc($pdk->result)) {
        $arr_pdk[$row["kcxl_id"]] = $row;
    }


    $dsvt_tb = new db_query("SELECT `dsvt_id`, `dsvt_name`, `dsvt_donViTinh`, `dsvt_donGia`, `dsvt_hangSanXuat`, `dsvt_xuatXu`,  `dvt_name`, `hsx_name`, `xx_name` FROM `danh-sach-vat-tu`
    LEFT JOIN `don-vi-tinh` ON `dsvt_donViTinh` = `dvt_id`
    LEFT JOIN `hang-san-xuat` ON `dsvt_hangSanXuat` = `hsx_id`
    LEFT JOIN `xuat-xu` ON `dsvt_xuatXu` = `xx_id`
    WHERE `dsvt_check` = 1 AND `dsvt_id_ct`= $id_cty");
    $arr_dsvt_tb = [];
    while ($row_dsvt = mysql_fetch_assoc($dsvt_tb->result)) {
        $arr_dsvt_tb[$row_dsvt["dsvt_id"]] = $row_dsvt;
    }

    if (isset($_SESSION['quyen']) && $_SESSION['quyen'] == 1) {
        $curl = curl_init();
        $token = $_COOKIE['acc_token'];
        curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/list_all_employee_of_company.php');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));
        $response = curl_exec($curl);
        curl_close($curl);

        $data_list = json_decode($response, true);
        $data_list_nv = $data_list['data']['items'];
        $count = count($data_list_nv);
        $user_id = $_SESSION['com_id'];

    } elseif (isset($_SESSION['quyen']) && ($_SESSION['quyen'] == 2)) {
        $curl = curl_init();
        $token = $_COOKIE['acc_token'];
        curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/list_all_my_partner.php?get_all=true');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));
        $response = curl_exec($curl);
        curl_close($curl);
        $data_list = json_decode($response, true);
        $data_list_nv = $data_list['data']['items'];
        $count = count($data_list_nv);
        $user_id = $_SESSION['ep_id'];
        
    }
    $newArr = [];
    for ($i = 0; $i < count($data_list_nv); $i++) {
        $value = $data_list_nv[$i];
        $newArr[$value["ep_id"]] = $value;
    }

    // API phiếu yêu cầu vật tư
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_URL, "https://phanmemquanlycungung.timviec365.vn/api/phieu_yc_vat_tu.php");
    curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($curl, CURLOPT_POSTFIELDS, [
        'id_com' => $id_cty,
    ]);
    $response = curl_exec($curl);
    curl_close($curl);
    $emp0 = json_decode($response, true);
    $emp = $emp0['data']['items'];
    $emp1 = [];
    for ($i = 0; $i < count($emp); $i++) {
        $pyc = $emp[$i];
        $emp1[$pyc["id"]] = $pyc;
    }

    // API công trình
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_POST, 1);
    curl_setopt($curl, CURLOPT_URL, 'https://phanmemquanlycongtrinh.timviec365.vn/api/congtrinh.php');
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($curl, CURLOPT_POSTFIELDS, [
        'id_com' => $id_cty,
    ]);
    $response = curl_exec($curl);
    curl_close($curl);
    $data_list = json_decode($response, true);
    $list_phieu_vt = $data_list['data']['items'];
    $list_phieu_vt1 = [];
    for ($i = 0; $i < count($list_phieu_vt); $i++) {
        $ctr = $list_phieu_vt[$i];
        $list_phieu_vt1[$ctr["ctr_id"]] = $ctr;
    }

    // API đơn hàng
    $curl_dh = curl_init();
    curl_setopt($curl_dh, CURLOPT_POST, 1);
    curl_setopt($curl_dh, CURLOPT_URL, 'https://phanmemquanlycungung.timviec365.vn/api/don_hang.php');
    curl_setopt($curl_dh, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl_dh, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($curl_dh, CURLOPT_POSTFIELDS, [
        'com_id' => $id_cty,
        'phan_loai' => 1,
    ]);
    $response_dh = curl_exec($curl_dh);
    curl_close($curl_dh);
    $data_list_dh = json_decode($response_dh, true);
    $list_dh = $data_list_dh['data']['items'];

    $list_dh1 = [];
    for ($j = 0; $j < count($list_dh); $j++) {
        $list_dh_p = $list_dh[$j];
        $list_dh1[$list_dh_p["id"]] = $list_dh_p;
    }
?>
<!DOCTYPE html>
<html lang="vi">

<head>
    <title>kho vật tư xây dựng</title>
    <meta charset="UTF-8">
    <meta name="robots" content="noindex,nofollow" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="../css/select2.min.css?v<=<?= $ver ?>">
    <link rel="stylesheet" type="text/css" href="../css/style.css?v=<?= $ver ?>">
    <link rel="stylesheet" type="text/css" href="../css/style_h.css?v=<?= $ver ?>">
    <link rel="stylesheet" type="text/css" href="../css/style_n.css?v=<?= $ver ?>">
    <link rel="stylesheet" type="text/css" href="../css/style_k.css?v<=<?= $ver ?>">

</head>


<body class="seclec2_radius">
    <div class="main_wrapper_all">
        <div class="wapper_all">
            <?php include('../includes/sidebar.php');  ?>
        </div>
        <div class="main_overview" id="main_overview">
            <div class="header_menu_overview d_flex align_c space_b">
                <p class="color_grey line_16 font_s14 text_link_page" style="display: block;"><a href="/nhap-kho.html" class="cursor_p">
                        <img src="../images/back_item_g.png" alt="">
                    </a>Nhập kho / Thêm mới</p>
                <img class="open_sidebar_w" src="../images/open_sidebar_w.png" alt="" onclick="toggle('main_sidebar')" style="display: none;">
                <?php include('../includes/header.php');  ?>
            </div>
            <form method="post" action="" id="f_import_wh_create">
                <div class="top_in_wh_create">
                    <div class="head_wh d_flex space_b align_c title_wh" style="display: none;">
                        <div class=" d_flex align_c ">
                            <a href="/nhap-kho.html" class="cursor_p">
                                <img src="../images/back_item_g.png" alt="">
                            </a>
                            <p class="color_grey line_16 font_s14 ml_10">
                                Nhập kho / Thêm mới
                            </p>
                        </div>
                    </div>
                    <div class="body_import_warehouse_create">
                        <div class="header_body_b color_white line_h19 font_s16 font_wB">
                            Thêm mới phiếu nhập kho
                        </div>

                        <!-- phiếu nhập kho -->
                        <div class="body_input_infor_vote " id="" style="display:block;">
                            <div class="d_flex align_c space_b wh768">
                                <div class="d_flex flex_column width_create mb_15">
                                    <p class="font_s15 line_h18 font_w500 color_grey mb_5">Số phiếu</p>
                                    <input class="input_value_grey font_14 line_h16" type="text" placeholder="Hệ thống tự thiết lập" readonly>
                                </div>
                                <div class="d_flex flex_column width_create mb_15">
                                    <p class="font_s15 line_h18 font_w500 color_grey">Hình thức nhập kho</p>
                                    <div class="select_create">
                                        <select class="select_create select_import_form" name="select_import_form" id="select_import_form" onchange="change_ht(this)">
                                            <option value="NK1">Nhập theo biên bản giao hàng</option>
                                            <option value="NK2">Nhập điều chuyển</option>
                                            <option value="NK3">Nhập trả lại từ thi công</option>
                                            <option value="NK4">Nhập theo yêu cầu vật tư</option>
                                            <option value="NK5">Nhập khác</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="d_flex align_c space_b wh768">
                                <div class="d_flex flex_column width_create mb_15">
                                    <p class="font_s15 line_h18 font_w500 color_grey mb_5">Người tạo</p>
                                    <?php if($_SESSION['quyen'] == 2){ ?>
                                        <input class="input_value_grey font_14 line_h16 color_grey nguoi_tao" readonly type="text" value="<?= $newArr[$user_id]['ep_name'] ?>" data="<?= $newArr[$user_id]['ep_id'] ?>" data-ct="<?=$id_cty?>">
                                    <?}else{?>
                                        <input class="input_value_grey font_14 line_h16 color_grey nguoi_tao" readonly type="text" value="<?= $tt_user['com_name'] ?>" data="0" data-ct="<?=$id_cty?>">
                                    <?}?>
                                </div>
                                <div class="d_flex flex_column width_create mb_15">
                                    <p class="font_s15 line_h18 font_w500 color_grey mb_5">Ngày tạo</p>
                                    <input class="input_value_grey font_14 line_h16 ngay_tao" type="date" name="ngay_tao" readonly value="<?= $date ?>">
                                </div>
                            </div>
                            <div>
                                <!-- Nhập theo biên bản giao hàng -->
                                <div class="detail_input_wh_1" style="display: block;">
                                    <div class="d_flex align_c space_b wh768">
                                        <div class="d_flex flex_column order_import_wh_cre width_create mb_15">
                                            <div class="d_flex ">
                                                <p class="font_s15 line_h18 font_w500 color_grey mb_5">Đơn hàng</p>
                                                <span class="color_red alert_red">*</span>
                                            </div>
                                            <select name="order_import_wh_cre" class="font_14 line_h16 font_w400 choice_order" style="width: 100%;">
                                                <option value="">Chọn đơn hàng</option>
                                                <?php foreach($list_dh as $don_hang){?>
                                                <option value="<?=$don_hang['id'];?>">ĐH - <?=$don_hang['id'];?></option>
                                                <?php }?>
                                            </select>
                                        </div>
                                        <div class="d_flex align_c width_create mb_15">
                                            <div class="w_100 seclec2_radius">
                                                <p class="font_s15 line_h18 font_w500 color_grey">Trạng thái</p>
                                                <div class="select_create mt_5">
                                                    <select class="select_create select_status" name="select_status" onchange="change_trangthai(this)">
                                                        <option value="1">Khởi tạo</option>
                                                        <?= ($_SESSION['quyen'] == '1' || in_array(5,$ro_nhap_kho)) ? "<option value='4'>Hoàn thành</option>" : ""?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="w_50 ml_10 date_accses" style="display:none">
                                                <div class="d_flex">
                                                    <p class="font_s15 line_h18 font_w500 color_grey">Ngày hoàn thành</p>
                                                    <span class="color_red alert_red">*</span>
                                                </div>
                                                <div class="select_create">
                                                    <input class="input_value_w" type="date" name="ngay_hoanThanh">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="d_flex align_c space_b wh768">
                                        <div class="d_flex flex_column width_create mb_15">
                                            <p class="font_s15 line_h18 font_w500 color_grey mb_5">Nhà cung cấp</p>
                                            <input class="input_value_grey color_grey font_14 line_h16" name="nha_cung_cap" type="text" placeholder="Hiển thị tên nhà cung cấp từ đơn hàng" readonly>
                                            <!-- <input class="input_value_grey font_14 line_h16 " type="text" placeholder="Hiển thị tên nhà cung cấp từ đơn hàng" readonly> -->
                                        </div>
                                        <div class="d_flex flex_column width_create mb_15">
                                            <p class="font_s15 line_h18 font_w500 color_grey mb_5">Người giao hàng</p>
                                            <input class=" font_14 line_h16" type="text" placeholder="Nhập tên người giao hàng" name="nguoiGiao">
                                        </div>
                                    </div>
                                    <div class="d_flex align_c space_b wh768">
                                        <div class="d_flex flex_column width_create mb_15">
                                            <p class="font_s15 line_h18 font_w500 color_grey mb_5">Người nhận</p>
                                            <input class="input_value_grey color_grey font_14 line_h16 " name="nguoi_nhan" type="text" placeholder="Hiển thị tên người nhận từ đơn hàng" readonly>
                                            <input class="input_value_grey color_grey font_14 line_h16 " name="id_nguoi_nhan" type="hidden" readonly>
                                        </div>
                                        <div class="d_flex flex_column width_create mb_15">
                                            <p class="font_s15 line_h18 font_w500 color_grey mb_5">Phòng ban</p>
                                            <input class="input_value_grey color_grey font_14 line_h16" type="text" name="phong_ban" placeholder="Hiển thị phòng ban người nhận từ đơn hàng" readonly>
                                            <input class="input_value_grey color_grey font_14 line_h16 " name="id_pb_nguoi_nhan" type="hidden" name="phong_ban" readonly>
                                        </div>
                                    </div>
                                    <div class="d_flex align_c space_b wh768">
                                        <div class="d_flex flex_column wh_imp width_create mb_15 kho_nhap">
                                            <p class="color_grey font_s15 line_h18 font_w500">Kho nhập<span style="color: red;">*</span></p>
                                            <select name="wh_imp" id="" class="select_wh_in color_grey font_s14 line_h17 font_w400" style="width:100%;">
                                                <option value="">Chọn kho</option>
                                                <? while ($item = mysql_fetch_assoc($select_khoNhap->result)) { ?>
                                                    <option value="<?= $item['kho_id'] ?>"><?= $item['kho_name'] ?></option>
                                                <? } ?>
                                            </select>
                                        </div>
                                        <div class="d_flex flex_column width_create mb_15 ngay_nhap">
                                            <div class="d_flex align_c">
                                                <p class="font_s15 line_h18 font_w500 color_grey mb_5">Ngày nhập kho</p>
                                                <span class="color_red alert_red">*</span>
                                            </div>
                                            <input class=" font_14 line_h16" type="date" value="" name="ngayNhapKho">
                                        </div>
                                    </div>
                                </div>

                                <!-- Nhập điều chuyển -->
                                <div class="detail_input_wh_2" style="display: none;">
                                    <div class="d_flex space_b wh768">
                                        <div class="d_flex flex_column wh_transfer_note seclec2_radius width_create mb_15">
                                            <div class="d_flex">
                                                <p class="font_s15 line_h18 font_w500 color_grey mb_5">Phiếu điều chuyển kho</p>
                                                <span class="color_red alert_red">*</span>
                                            </div>
                                            <select class="select_create select_transfer_slip" name="select_transfer_slip" id="select_transfer_slip">
                                                <option value="">Chọn phiếu điều chuyển</option>
                                                <? foreach ($arr_pdk as $key => $value) { ?>
                                                    <option value="<?= $value['kcxl_id'] ?>"><?= 'ĐCK' . ' ' . '-' . ' ' . $value['kcxl_id'] . ' ' . 'chuyển từ' . ' ' . $value['kho_xuat_name'] . ' ' . 'đến' . ' ' . $value['kho_nhap_name'] ?></option>
                                                <? } ?>
                                            </select>
                                        </div>
                                        <div class="d_flex width_create mb_15">
                                            <div class="w_100 seclec2_radius">
                                                <p class="font_s15 line_h18 font_w500 color_grey">Trạng thái</p>
                                                <div class="select_create mt_5">
                                                    <select class="select_create select_status" name="select_status_2" id="select_status_2" onchange="change_trangthai(this)">
                                                        <option value="1">Khởi tạo</option>
                                                        <?= ($_SESSION['quyen'] == '1' || in_array(5,$ro_nhap_kho)) ? "<option value='4'>Hoàn thành</option>" : ""?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="w_50 ml_10 date_accses" style="display:none">
                                                <div class="date_accses_2">
                                                    <div class="d_flex">
                                                        <p class="font_s15 line_h18 font_w500 color_grey">Ngày hoàn thành</p>
                                                        <span class="color_red alert_red">*</span>
                                                    </div>
                                                    <div class="select_create">
                                                        <input class="input_value_w ngay_ht_2" type="date" name="ngay_ht_2" id="ngay_ht_2">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="dieuChuyen">
                                        <div class="d_flex align_c space_b wh768">
                                            <div class="d_flex flex_column width_create mb_15">
                                                <p class="font_s15 line_h18 font_w500 color_grey mb_5">Kho xuất</p>
                                                <input class="input_value_grey font_14 line_h16 kho_xuat_2" type="text" placeholder="Hiển thị kho xuất theo phiếu điều chuyển kho" readonly>
                                                <input class="input_value_grey font_14 line_h16 id_kho_xuat_2" type="hidden" readonly>
                                            </div>
                                            <div class="d_flex flex_column width_create mb_15">
                                                <p class="font_s15 line_h18 font_w500 color_grey mb_5">Kho nhập</p>
                                                <input class=" font_14 line_h16 kho_nhap_2" type="text" placeholder="Hiển thị kho nhập theo phiếu điều chuyển kho" readonly>
                                                <input class=" font_14 line_h16 id_kho_nhap_2" type="hidden" readonly>
                                            </div>
                                        </div>
                                        <div class="d_flex align_c space_b wh768">
                                            <div class="d_flex flex_column width_create mb_15">
                                                <p class="font_s15 line_h18 font_w500 color_grey mb_5">Người giao</p>
                                                <input class="input_value_grey font_14 line_h16 nguoi_giao_2" type="text" placeholder="Hiển thị tên người giao từ phiếu điều chuyển kho" readonly>
                                                <input class="input_value_grey font_14 line_h16 id_nguoi_giao_2" type="hidden" readonly>
                                            </div>
                                            <div class="d_flex flex_column width_create mb_15">
                                                <p class="font_s15 line_h18 font_w500 color_grey mb_5">Phòng ban</p>
                                                <input class="input_value_grey font_14 line_h16 phong_ban_giao_2" type="text" placeholder="Hiển thị phòng ban người giao từ phiếu điều chuyển kho" readonly>
                                                <input class="input_value_grey font_14 line_h16 id_phong_ban_giao_2" type="hidden" readonly>
                                            </div>
                                        </div>
                                        <div class="d_flex align_c space_b wh768">
                                            <div class="d_flex flex_column width_create mb_15">
                                                <p class="font_s15 line_h18 font_w500 color_grey mb_5">Người nhận</p>
                                                <input class="input_value_grey font_14 line_h16 nguoi_nhan_2" type="text" placeholder="Hiển thị tên người nhận từ phiếu điều chuyển kho" readonly>
                                                <input class="input_value_grey font_14 line_h16 id_nguoi_nhan_2" type="hidden" readonly>
                                            </div>
                                            <div class="d_flex flex_column width_create mb_15">
                                                <p class="font_s15 line_h18 font_w500 color_grey mb_5">Phòng ban</p>
                                                <input class="input_value_grey font_14 line_h16 phong_ban_nhan_2" type="text" placeholder="Hiển thị phòng ban người nhận từ phiếu điều chuyển kho" readonly>
                                                <input class="input_value_grey font_14 line_h16 id_phong_ban_nhan_2" type="hidden" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="d_flex space_b wh768">
                                        <div class="ngay_ycHoanThanh d_flex flex_column width_create mb_15">
                                            <div class="">
                                                <p class="font_s15 line_h18 font_w500 color_grey mb_5">Ngày yêu cầu hoàn thành</p>
                                                <input class="input_value_grey font_14 line_h16 date_yc_ht_2" type="date" placeholder="" readonly>
                                            </div>
                                        </div>
                                        <div class="d_flex flex_column width_create">
                                            <div class="data_nhap_2">
                                                <div class="d_flex align_c">
                                                    <p class="font_s15 line_h18 font_w500 color_grey mb_5 date_nhap_2">Ngày nhập kho</p>
                                                    <span class="color_red alert_red">*</span>
                                                </div>
                                                <input class=" font_14 line_h16 ngay_nhap_2" style="width: 100%" name="ngay_nhap_2" id="ngay_nhap_2" type="date" value="">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!-- Nhập trả lại từ thi công -->
                                <div class="detail_input_wh_3" style="display:none;">
                                    <div class="d_flex space_b wh768">
                                        <div class="d_flex flex_column choice_construction seclec2_radius width_create mb_15">
                                            <div class="d_flex">
                                                <p class="font_s15 line_h18 font_w500 color_grey mb_5">Chọn công trình</p>
                                                <span class="color_red alert_red">*</span>
                                            </div>
                                            <select class="font_14 line_h16 select_construction " name="select_construction" id="select_construction">
                                                <option value="">Chọn công trình</option>
                                                <? foreach ($list_phieu_vt1 as $key => $value) { ?>
                                                    <option value="<?= $value['ctr_id'] ?>"><?= $value['ctr_name'] ?></option>
                                                <? } ?>
                                            </select>
                                        </div>
                                        <div class="d_flex width_create mb_15">
                                            <div class="w_100 ">
                                                <p class="font_s15 line_h18 font_w500 color_grey">Trạng thái</p>
                                                <div class="select_create mt_5">
                                                    <select class="select_create select_status" name="select_status_3" id="select_status_3" onchange="change_trangthai(this)">
                                                        <option value="1">Khởi tạo</option>
                                                        <?= ($_SESSION['quyen'] == '1' || in_array(5,$ro_nhap_kho)) ? "<option value='4'>Hoàn thành</option>" : ""?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="w_50 ml_10 date_accses" style="display:none">
                                                <div class="tb_date_ht_3">
                                                    <div class="d_flex">
                                                        <p class="font_s15 line_h18 font_w500 color_grey">Ngày hoàn thành</p>
                                                        <span class="color_red alert_red">*</span>
                                                    </div>
                                                    <div class="select_create   ">
                                                        <input class="input_value_w date_ht_3" type="date" name="date_ht_3" id="data_ht_3">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="d_flex  space_b wh768">
                                        <div class="d_flex flex_column mb_15 width_create mb_15">
                                            <div class="d_flex space_b">
                                                <p class="color_grey font_s15 line_h18 font_w500">Người giao hàng</p>
                                                <a href="#" class="color_blue font_s15 line_h18 font_w400 d_flex align_c"><span class="font_s24">+</span>&nbsp;Thêm nhân viên</a>
                                            </div>
                                            <select name="select_shipper_wh_3" id="select_shipper_wh_3" class="select_shipper_wh color_grey font_s14 line_h17 font_w400" style="width: 100%;">
                                                <option value="">Chọn người giao hàng</option>
                                                <? foreach ($data_list_nv as $item) { ?>
                                                    <option value="<?= $item['ep_id'] ?>"><?= $item['ep_name']."&nbsp-&nbsp".$item['dep_name']; ?></option>
                                                <? } ?>
                                            </select>
                                        </div>
                                        <div class="d_flex flex_column width_create mb_15 p_nguoiGiao">
                                            <div class="">
                                                <p class="font_s15 line_h18 font_w500 color_grey mb_5">Phòng ban</p>
                                                <input class="input_value_grey font_14 line_h16 phong_giao_3" type="text" placeholder="Hiển thị phòng ban người giao" readonly>
                                                <input class="input_value_grey font_14 line_h16 id_phong_giao_3" type="hidden" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="d_flex space_b wh768">
                                        <div class="d_flex flex_column mb_15 width_create mb_15">
                                            <div class="d_flex space_b">
                                                <p class="color_grey font_s15 line_h18 font_w500">Người nhận</p>
                                                <a href="#" class="color_blue font_s15 line_h18 font_w400 d_flex align_c"><span class="font_s24">+</span>&nbsp;Thêm nhân viên</a>
                                            </div>
                                            <select name="select_receiver_wh_3" id="select_receiver_wh_3" class="select_receiver_wh color_grey font_s14 line_h17 font_w400 nguoiNhan_thiCong" style="width: 100%;">
                                                <option value="">Chọn người nhận</option>
                                                <? foreach ($data_list_nv as $item) { ?>
                                                    <option value="<?= $item['ep_id'] ?>"><?= $item['ep_name']."&nbsp-&nbsp".$item['dep_name']; ?></option>
                                                <? } ?>
                                            </select>
                                        </div>
                                        <div class="d_flex flex_column width_create mb_15 p_nguoiNhan">
                                            <div class="">
                                                <p class="font_s15 line_h18 font_w500 color_grey mb_5">Phòng ban</p>
                                                <input class="input_value_grey font_14 line_h16 phong_ban_nhan_3" type="text" placeholder="Hiển thị phòng ban người nhận" readonly>
                                                <input class="input_value_grey font_14 line_h16 id_phong_ban_nhan_3" type="hidden" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="d_flex space_b wh768">
                                        <div class="d_flex flex_column mb_15 wh_imp width_create mb_15">
                                            <div class="tb_kn_3">
                                                <p class="color_grey font_s15 line_h18 font_w500">Kho nhập<span style="color: red;">*</span></p>
                                                <select name="select_wh_in_3" id="select_wh_in_3" class="select_wh_in color_grey font_s14 line_h17 font_w400" style="width:100%;">
                                                    <option value=""></option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="d_flex flex_column width_create mb_15">
                                            <div class="tb_date_nk_3">
                                                <div class="d_flex align_c">
                                                    <p class="font_s15 line_h18 font_w500 color_grey mb_5">Ngày nhập kho</p>
                                                    <span class="color_red alert_red">*</span>
                                                </div>
                                                <input class=" font_14 line_h16 ngay_nk_3" style="width:100%" name="ngay_nk_3" id="ngay_nk_3" type="date" value="">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!-- Nhập khác-->
                                <div class="detail_input_wh_4" style="display: none;">
                                    <div class="d_flex space_b wh768">
                                        <div class="d_flex width_create mb_15">
                                            <div class="w_100 seclec2_radius ">
                                                <p class="font_s15 line_h18 font_w500 color_grey">Trạng thái</p>
                                                <div class="select_create mt_5">
                                                    <select class="select_create select_status" name="select_status_4" id="select_status_4" onchange="change_trangthai(this)">
                                                        <option value="1">Khởi tạo</option>
                                                        <?= ($_SESSION['quyen'] == '1' || in_array(5,$ro_nhap_kho)) ? "<option value='4'>Hoàn thành</option>" : ""?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="w_50 ml_10 date_accses" style="display:none">
                                                <div class="date_accses_4">
                                                    <div class="d_flex">
                                                        <p class="font_s15 line_h18 font_w500 color_grey">Ngày hoàn thành</p>
                                                        <span class="color_red alert_red">*</span>
                                                    </div>
                                                    <div class="select_create">
                                                        <input class="input_value_w ngay_ht_4" type="date" name="ngay_ht_4" id="ngay_ht_4">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="d_flex flex_column width_create mb_15">
                                            <div class="d_flex align_c space_b">
                                                <p class="font_s15 line_h18 font_w500 color_grey">Người giao</p>
                                            </div>
                                            <input class="nguoi_giao_5 font_14 line_h16" type="text" placeholder="Nhập tên người giao">
                                        </div>
                                    </div>
                                    <div class="d_flex space_b wh768">
                                        <div class="d_flex flex_column width_create mb_15">
                                            <div class="d_flex align_c space_b">
                                                <p class="font_s15 line_h18 font_w500 color_grey">Người nhận</p>
                                                <a class="color_blue font_s15 line_h18 font_w400 d_flex align_c" href="#">
                                                    <span class="font_s24">+</span>
                                                    Thêm nhân viên
                                                </a>
                                            </div>
                                            <select name="select_staff_4" id="select_staff_4" class="color_grey font_s14 line_h17font_w400 select_staff">
                                                <option value=""></option>
                                                <? foreach ($data_list_nv as $item) { ?>
                                                    <option value="<?= $item['ep_id'] ?>"><?= $item['ep_name'] . ' ' . '-' . ' ' . $item['dep_name'] ?></option>
                                                <? } ?>
                                            </select>
                                        </div>
                                        <div class="p_nhan_nhapKhac_nhapkho d_flex flex_column width_create mb_15">
                                            <div class="">
                                                <p class="font_s15 line_h18 font_w500 color_grey mb_5">Phòng ban</p>
                                                <input class="input_value_grey font_14 line_h16 phong_giao_4" type="text" placeholder="Hiển thị phòng ban người nhận" readonly>
                                                <input class="input_value_grey font_14 line_h16 id_phong_giao_4" type="hidden" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="d_flex space_b wh768">
                                        <div class="d_flex flex_column wh_imp width_create mb_15">
                                            <div class="tb_kn_4">
                                                <p class="color_grey font_s15 line_h18 font_w500">Kho nhập<span style="color: red;">*</span></p>
                                                <select name="select_wh_in_4" id="select_wh_in_4" class="select_wh_in color_grey font_s14 line_h17 font_w400" style="width:100%;">
                                                    <option value=""></option>
                                                    <? while ($khoNhap4 = mysql_fetch_assoc($kho2->result)) { ?>
                                                        <option value="<?= $khoNhap4['kho_id'] ?>"><?= $khoNhap4['kho_name'] ?></option>
                                                    <? } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="d_flex flex_column date_imp_wh width_create mb_15">
                                            <div class="tb_date_imp_wh_4">
                                                <div class="d_flex align_c">
                                                    <p class="font_s15 line_h18 font_w500 color_grey mb_5">Ngày nhập kho</p>
                                                    <span class="color_red alert_red">*</span>
                                                </div>
                                                <input style="width:100%" name="date_imp_wh_4" id="date_imp_wh_4" class="date_imp_wh_4 font_14 line_h16" type="date" value="">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!-- Nhập theo yêu cầu vật tư -->
                                <div class="detail_input_wh_5" style="display: none;">
                                    <div class="d_flex space_b wh768">
                                        <div class="d_flex flex_column rq_note width_create mb_15">
                                            <div class="tb_rq_note_5">
                                                <div class="d_flex">
                                                    <p class="font_s15 line_h18 font_w500 color_grey mb_5">Phiếu yêu cầu</p>
                                                    <span class="color_red alert_red">*</span>
                                                </div>
                                                <select class="font_14 line_h16 font_w400 rq_note_5" name="rq_note_5" id="rq_note_5">
                                                    <option value=""></option>
                                                    <? foreach ($emp1 as $key => $value) { ?>
                                                        <option value="<?= $value['id'] ?>">YC - <?= $value['id'] ?></option>
                                                    <? } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="d_flex width_create mb_15">
                                            <div class="w_100 ">
                                                <p class="font_s15 line_h18 font_w500 color_grey" style="width:100%">Trạng thái</p>
                                                <div class="select_create mt_5">
                                                    <select class="select_create select_status" name="select_status_5" id="select_status_5" onchange="change_trangthai(this)">
                                                        <option value="1">Khởi tạo</option>
                                                        <?= ($_SESSION['quyen'] == '1' || in_array(5,$ro_nhap_kho)) ? "<option value='4'>Hoàn thành</option>" : ""?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="w_50 ml_10 date_accses" style="display:none">
                                                <div class="tb_date_ht_5">
                                                    <div class="d_flex">
                                                        <p class="font_s15 line_h18 font_w500 color_grey">Ngày hoàn thành</p>
                                                        <span class="color_red alert_red">*</span>
                                                    </div>
                                                    <div class="select_create">
                                                        <input class="input_value_w date_ht_5" type="date" name="date_ht_5" id="date_ht_5">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="d_flex space_b wh768">
                                        <div class="d_flex flex_column width_create mb_15">
                                            <div class="d_flex align_c space_b">
                                                <p class="font_s15 line_h18 font_w500 color_grey">Người giao hàng</p>
                                                <a class="color_blue font_s15 line_h18 font_w400 d_flex align_c" href="#">
                                                    <span class="font_s24">+</span>
                                                    Thêm nhân viên
                                                </a>
                                            </div>
                                            <select name="select_shipper_dis_5" id="select_shipper_dis_5" class="color_grey font_s14 line_h17font_w400 select_shipper_dis">
                                                <option value="">Chọn người giao hàng</option>
                                                <? foreach ($data_list_nv as $item) { ?>
                                                    <option value="<?= $item['ep_id'] ?>"><?= $item['ep_name'] . ' ' . '-' . ' ' . $item['dep_name'] ?></option>
                                                <? } ?>
                                            </select>
                                        </div>
                                        <div class="p_giao_ycvt d_flex flex_column width_create mb_15">
                                            <div class="">
                                                <p class="font_s15 line_h18 font_w500 color_grey mb_5">Phòng ban</p>
                                                <input class=" font_14 line_h16 phong_giao_5" style="width:100%;" type="text" placeholder="Hiển thị phòng ban người giao hàng" readonly>
                                                <input class=" font_14 line_h16 id_phong_giao_5" style="width:100%;" type="hidden" placeholder="Hiển thị phòng ban người giao hàng" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="d_flex align_c space_b wh768">
                                        <div class="d_flex flex_column width_create mb_15">
                                            <p class="font_s15 line_h18 font_w500 color_grey">Người Nhận</p>
                                            <input class="input_value_grey font_14 line_h16 nguoi_nhan_5" type="text" placeholder="Hiển thị tên người nhận từ phiếu yêu cầu" readonly>
                                            <input class="input_value_grey font_14 line_h16 id_nguoi_nhan_5" type="hidden" readonly>
                                        </div>
                                        <div class="d_flex flex_column width_create mb_15">
                                            <p class="font_s15 line_h18 font_w500 color_grey mb_5">Phòng ban</p>
                                            <input class="input_value_grey font_14 line_h16 phong_nhan_5" type="text" placeholder="Hiển thị phòng ban người nhận từ phiếu yêu cầu" readonly>
                                            <input class="input_value_grey font_14 line_h16 id_phong_nhan_5" type="hidden" readonly>
                                        </div>
                                    </div>
                                    <div class="d_flex space_b wh768">
                                        <div class="d_flex flex_column wh_imp width_create mb_15">
                                            <div class="tb_select_wh_in_5">
                                                <p class="color_grey font_s15 line_h18 font_w500">Kho nhập<span style="color: red;">*</span></p>
                                                <select name="select_wh_in_5" id="select_wh_in_5" class="select_wh_in color_grey font_s14 line_h17 font_w400" style="width:100%;">
                                                    <option value=""></option>
                                                    <? while ($khoNhap5 = mysql_fetch_assoc($kho4->result)) { ?>
                                                        <option value="<?= $khoNhap5['kho_id'] ?>"><?= $khoNhap5['kho_name'] ?></option>
                                                    <? } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="d_flex flex_column width_create mb_15">
                                            <div class="tb_date_nk_5">
                                                <div class="d_flex align_c">
                                                    <p class="font_s15 line_h18 font_w500 color_grey mb_5">Ngày nhập kho</p>
                                                    <span class="color_red alert_red">*</span>
                                                </div>
                                                <input style="width:100%" class=" font_14 line_h16 date_nk_5" name="date_nk_5" id="date_nk_5" type="date" value="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="flex_column d_flex note_import_vote_infor">
                                <p class="font_s15 line_h18 font_w500 color_grey mb_5">Ghi chú</p>
                                <textarea name="ghi_chu" id="ghi_chu" rows="5" placeholder="Nhập nội dung"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- danh sách vật tư theo biên bản giao hàng -->
                <div class="table_input_wh table_input_wh_1 mt_15" id="table_input_wh_1">
                    <p class="tit_table_vt color_blue font_s16 line_h19 font_w700"> Danh sách vật tư</p>
                    <div class="position_r d_flex align_c">
                        <div class="main_tabl table_vt_scr" onscroll="table_scroll(this)">
                            <table class="table table_list_meterial" style="width:1689px;">
                                <tr>
                                    <th>STT
                                        <span class="span_thread"></span>
                                    </th>
                                    <th>Mã vật tư
                                        <span class="span_thread"></span>
                                    </th>
                                    <th>Tên đầy đủ vật tư thiết bị
                                        <span class="span_thread"></span>
                                    </th>
                                    <th>Đơn vị tính
                                        <span class="span_thread"></span>
                                    </th>
                                    <th> Hãng sản xuất
                                        <span class="span_thread"></span>
                                    </th>
                                    <th> Xuất xứ
                                        <span class="span_thread"></span>
                                    </th>
                                    <th> Số lượng theo đơn hàng
                                        <span class="span_thread"></span>
                                    </th>
                                    <th> Số lượng thực tế nhập kho
                                        <span class="span_thread"></span>
                                    </th>
                                    <th>Đơn giá (VNĐ)
                                        <span class="span_thread"></span>
                                    </th>
                                    <th>Thành tiền (VNĐ)
                                    </th>
                                </tr>
                            </table>
                        </div>
                        <div class="pre_q d_flex align_c flex_center position_a display_none" onclick="pre_q(this)">
                            <span class="pre_arrow"></span>
                        </div>
                        <div class="next_q d_flex align_c flex_center position_a" onclick="next_q(this)">
                            <span class="next_arrow"></span>
                        </div>
                    </div>
                </div>
                <!-- danh sách vật tư điều chuyển -->
                <div style="display:none" class="table_input_wh table_input_wh_2 mt_15" id="table_input_wh_2">
                    <p class="tit_table_vt color_blue font_s16 line_h19 font_w700"> Danh sách vật tư</p>
                    <div class="position_r d_flex align_c">
                        <div class="table_vt_scr" onscroll="table_scroll(this)">
                            <table class="table table_2">
                                <tr class="color_white font_s16 line_h19 font_w500">
                                    <th>STT
                                        <span class="span_tbody"></span>
                                    </th>
                                    <th>Mã vật tư thiết bị
                                        <span class="span_tbody"></span>
                                    </th>
                                    <th>Tên đầy đủ vật tư thiết bị
                                        <span class="span_tbody"></span>
                                    </th>
                                    <th>Đơn vị tính
                                        <span class="span_tbody"></span>
                                    </th>
                                    <th>Hãng sản xuất
                                        <span class="span_tbody"></span>
                                    </th>
                                    <th>Xuất xứ
                                        <span class="span_tbody"></span>
                                    </th>
                                    <th>Số lượng điều chuyển
                                        <span class="span_tbody"></span>
                                    </th>
                                    <th>Số lượng thực tế nhập kho
                                        <span class="span_tbody"></span>
                                    </th>
                                    <th>Đơn giá (VNĐ)
                                        <span class="span_tbody"></span>
                                    </th>
                                    <th>Thành tiền (VNĐ)
                                    </th>
                                </tr>
                                <tbody class="data_table_2">

                                </tbody>
                            </table>
                        </div>
                        <div class="pre_q d_flex align_c flex_center position_a display_none" onclick="pre_q(this)">
                            <span class="pre_arrow"></span>
                        </div>
                        <div class="next_q d_flex align_c flex_center position_a" onclick="next_q(this)">
                            <span class="next_arrow"></span>
                        </div>
                    </div>
                </div>
                <!-- danh sách vật tư trả lại từ thi công -->
                <div style="display:none ;" class="table_input_wh table_input_wh_3 mt_15" id="table_input_wh_3">
                    <p class="tit_table_vt color_blue font_s16 line_h19 font_w700"> Danh sách vật tư</p>
                    <div class="position_r align_c d_flex">
                        <div class="main_table table_vt_scr" onscroll="table_scroll(this)">
                            <table class="table table_list_meterial-3" style="width: 1432px;">
                                <tr>
                                    <th>
                                        <span class="span_thread"></span>
                                    </th>
                                    <th>Mã vật tư
                                        <span class="span_thread"></span>
                                    </th>
                                    <th>Tên đầy đủ vật tư thiết bị
                                        <span class="span_thread"></span>
                                    </th>
                                    <th>Đơn vị tính
                                        <span class="span_thread"></span>
                                    </th>
                                    <th> Hãng sản xuất
                                        <span class="span_thread"></span>
                                    </th>
                                    <th> Xuất xứ
                                        <span class="span_thread"></span>
                                    </th>
                                    <th> Số lượng nhập kho
                                        <span class="span_thread"></span>
                                    </th>
                                    <th>Đơn giá (VNĐ)
                                        <span class="span_thread"></span>
                                    </th>
                                    <th>Thành tiền (VNĐ)
                                        <span class="span_thread"></span>
                                    </th>
                                </tr>
                                <tbody id="data_table_3"></tbody>
                                <tr class="color_blue font_s14 line_h17 font_w500 ">
                                    <td colspan="2">
                                        <div class=" d_flex align_c cursor_p add_row_wh">
                                            <p class="add_vt d_flex flex_center align_c position_r"><span class="color_white font_s20 position_a">+</span></p>
                                            <p class="">Thêm vật tư</p>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="pre_q d_flex align_c flex_center position_a display_none" onclick="pre_q(this)">
                            <span class="pre_arrow"></span>
                        </div>
                        <div class="next_q d_flex align_c flex_center position_a" onclick="next_q(this)">
                            <span class="next_arrow"></span>
                        </div>
                    </div>
                </div>
                <!-- danh sách vật tư nhập khác -->
                <div style="display:none ;" class="table_input_wh table_input_wh_4 mt_15" id="table_input_wh_4">
                    <p class="tit_table_vt color_blue font_s16 line_h19 font_w700"> Danh sách vật tư</p>
                    <div class="position_r align_c d_flex">
                        <div class="main_table table_vt_scr" onscroll="table_scroll(this)">
                            <table class="table table_list_meterial-3" style="width: 1432px;">
                                <tr>
                                    <th>
                                        <span class="span_thread"></span>
                                    </th>
                                    <th>Mã vật tư
                                        <span class="span_thread"></span>
                                    </th>
                                    <th>Tên đầy đủ vật tư thiết bị
                                        <span class="span_thread"></span>
                                    </th>
                                    <th>Đơn vị tính
                                        <span class="span_thread"></span>
                                    </th>
                                    <th> Hãng sản xuất
                                        <span class="span_thread"></span>
                                    </th>
                                    <th> Xuất xứ
                                        <span class="span_thread"></span>
                                    </th>
                                    <th> Số lượng nhập kho
                                        <span class="span_thread"></span>
                                    </th>
                                    <th>Đơn giá (VNĐ)
                                        <span class="span_thread"></span>
                                    </th>
                                    <th>Thành tiền (VNĐ)
                                        <span class="span_thread"></span>
                                    </th>
                                </tr>
                                <tbody id="data_table_4"></tbody>
                                <tr class="color_blue font_s14 line_h17 font_w500 ">
                                    <td colspan="2">
                                        <div class=" d_flex align_c cursor_p add_row_wh_4">
                                            <p class="add_vt d_flex flex_center align_c position_r"><span class="color_white font_s20 position_a">+</span></p>
                                            <p class="">Thêm vật tư</p>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="pre_q d_flex align_c flex_center position_a display_none" onclick="pre_q(this)">
                            <span class="pre_arrow"></span>
                        </div>
                        <div class="next_q d_flex align_c flex_center position_a" onclick="next_q(this)">
                            <span class="next_arrow"></span>
                        </div>
                    </div>
                </div>
                <!-- danh sách vật tư nhập theo yêu cầu vật tư -->
                <div style="display:none;" class="table_input_wh table_input_wh_5 mt_15" id="table_input_wh_5">
                    <p class="tit_table_vt color_blue font_s16 line_h19 font_w700"> Danh sách vật tư</p>
                    <div class="position_r align_c d_flex">
                        <div class="main_table table_vt_scr" onscroll="table_scroll(this)">
                            <table class="table table_list_meterial-5" style="width: 1855px;">
                                <tr>
                                    <th>STT
                                        <span class="span_thread"></span>
                                    </th>
                                    <th>Mã vật tư
                                        <span class="span_thread"></span>
                                    </th>
                                    <th>Tên đầy đủ vật tư thiết bị
                                        <span class="span_thread"></span>
                                    </th>
                                    <th>Đơn vị tính
                                        <span class="span_thread"></span>
                                    </th>
                                    <th> Hãng sản xuất
                                        <span class="span_thread"></span>
                                    </th>
                                    <th> Xuất xứ
                                        <span class="span_thread"></span>
                                    </th>
                                    <th> Số lượng theo yêu cầu
                                        <span class="span_thread"></span>
                                    </th>
                                    <th> Số lượng được duyệt
                                        <span class="span_thread"></span>
                                    </th>
                                    <th> Số lượng thực tế nhập kho
                                        <span class="span_thread"></span>
                                    </th>
                                    <th>Đơn giá (VNĐ)
                                        <span class="span_thread"></span>
                                    </th>
                                    <th>Thành tiền (VNĐ)
                                        <span class="span_thread"></span>
                                    </th>
                                </tr>
                                <tbody class="data_table_1" id="dom-table">
                                </tbody>
                            </table>
                        </div>
                        <div class="pre_q d_flex align_c flex_center position_a display_none" onclick="pre_q(this)">
                            <span class="pre_arrow"></span>
                        </div>
                        <div class="next_q d_flex align_c flex_center position_a" onclick="next_q(this)">
                            <span class="next_arrow"></span>
                        </div>
                    </div>

                </div>

                <div class="btn_cf_rq d_flex flex_center" style="display: flex;">
                    <button type="button" class="btn_cancel back_w color_blue font_s15 line_h18 font_w500">
                        <a href="nhap-kho.html"></a>
                        Hủy</button>
                    <button type="button" class="btn_save btn_nhapKhoCreate back_blue color_white font_s15 line_h18 font_w500">Lưu</button>
                </div>
            </form>
        </div>
        <?php include('../includes/popup_nhap-kho.php');  ?>
        
    </div>
    <?php include('../includes/popup_overview.php');  ?>
</body>
<script type="text/javascript" src="../js/jquery-3.4.1.min.js"></script>
<script type="text/javascript" src="../js/jquery.validate.min.js"></script>
<script type="text/javascript" src="../js/reset_validate.js"></script>
<script type="text/javascript" src="../js/select2.min.js"></script>
<script type="text/javascript" src="../js/js_h.js"></script>
<script type="text/javascript" src="../js/validate_h.js"></script>
<script type="text/javascript" src="../js/js_k.js"></script>
<script type="text/javascript" src="../js/js_n.js"></script>
<script>
    var ctr_name_ct = <?= json_encode($list_phieu_vt1) ?>;
    var data = <?= json_encode($newArr) ?>;
    var pdc = <?= json_encode($arr_pdk) ?>;
    var dh = <?= json_encode($list_dh1) ?>;
    var pyc = <?= json_encode($emp1) ?>;
    var arr_dsvt_tb = <?= json_encode($arr_dsvt_tb) ?>;
    var arr_kho3 = <?= json_encode($arr_kho3) ?>;
    var tt_user = <?= json_encode($tt_user) ?>;
</script>
<script type="text/javascript" src="../js/nhap_kho_them_moi.js"></script>

</html>