<?php include("config.php"); ?>
<!DOCTYPE html>
<html lang="vi">

<head>
    <title>Chi tiết phiếu nhập kho</title>
    <meta charset="UTF-8">
    <meta name="robots" content="noindex,nofollow" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="../css/select2.min.css?= $ver ?>">
    <link rel="stylesheet" type="text/css" href="../css/style.css?v=<?= $ver ?>">
    <link rel="stylesheet" type="text/css" href="../css/style_h.css?v=<?= $ver ?>">
    <link rel="stylesheet" type="text/css" href="../css/style_n.css?v=<?= $ver ?>">
    <link rel="stylesheet" type="text/css" href="../css/style_k.css?v<=<?= $ver ?>">
</head>

<body class="seclec2_radius">
    <div class="main_wrapper_all">
        <div class="wrapper_all">
            <?php include('../includes/sidebar.php');  ?>
        </div>
        <div class="main_overview import_wh_detail">
            <div class="header_menu_overview d_flex align_c space_b">
                <p class="color_grey line_16 font_s14 text_link_page" style="display: block;"><a href="/nhap-kho.html" class="cursor_p">
                        <img src="../images/back_item_g.png" alt="">
                    </a>Nhập kho / chi tiết phiếu nhập kho</p>
                <img class="open_sidebar_w" src="../images/open_sidebar_w.png" alt="" onclick="toggle('main_sidebar')" style="display: none;">
                <?php include('../includes/header.php');  ?>
            </div>
            <div class="main_import_wh_detail import_wh_detail_2">
                <div class="head_wh d_flex space_b align_c title_wh" style="display: none;">
                    <div class=" d_flex align_c ">
                        <a href="/nhap-kho.html" class="cursor_p">
                            <img src="../images/back_item_g.png" alt="">
                        </a>
                        <p class="color_grey line_16 font_s14 ml_10">
                            Nhập kho / Chi tiết phiếu nhập kho
                        </p>
                    </div>
                </div>
                <div class="operation_wh d_flex space_b flex_end">
                    <div class="export_wh d_flex space_b align_c">

                        <button class="btn_del d_flex flex_center align_c">
                            <p class="color_blue font_s15 line_h18 font_w500">Xóa phiếu</p>
                        </button>

                        <button class="btn_ex d_flex flex_center align_c">
                            <img src="../images/export.png" alt="">
                            <p class="color_white font_s15 line_h18 font_w500">Xuất excel</p>
                        </button>
                    </div>
                </div>
                <!-- thông tin phiếu nhập kho trạng thái chờ duyệt1 -->
                <div class="info_rq info_input_order" style="display : block;">
                    <div class="imp_bill" style="display: block;">
                        <div class="tit_info_rq back_blue">
                            <p class="color_white font_s16 line_h19 font_w700">Thông tin phiếu nhập kho</p>
                        </div>
                        <div class="body_info_rq">
                            <!-- trạng thái chờ duyệt 1 -->
                            <div class="list_info_input_order">
                                <div class="item_body_info_rq d_flex align_c">
                                    <p class="tit_it color_grey font_s14 line_h17 font_w400">Số phiếu:</p>
                                    <p class="color_grey font_s14 line_h17 font_w500">ĐCK-0000</p>
                                </div>
                                <div class=" imp_wh_bb" style="display:block;">
                                    <div class="item_body_info_rq d_flex align_c">
                                        <p class="tit_it color_grey font_s14 line_h17 font_w400">Hình thức nhập kho:</p>
                                        <p class="color_grey font_s14 line_h17 font_w500">Nhập theo biên bản giao hàng</p>
                                    </div>
                                </div>
                                <div class="imp_wh_dc" style="display: none;">
                                    <div class="item_body_info_rq d_flex align_c ">
                                        <p class="tit_it color_grey font_s14 line_h17 font_w400">Hình thức nhập kho:</p>
                                        <p class="color_grey font_s14 line_h17 font_w500">Nhập điều chuyển</p>
                                    </div>
                                </div>
                                <div class="imp_wh_back" style="display: none;">
                                    <div class="item_body_info_rq d_flex align_c ">
                                        <p class="tit_it color_grey font_s14 line_h17 font_w400">Hình thức nhập kho:</p>
                                        <p class="color_grey font_s14 line_h17 font_w500">Nhập trả lại từ thi công</p>
                                    </div>
                                </div>
                                <div class="imp_wh_supply" style="display: none;">
                                    <div class="item_body_info_rq d_flex align_c ">
                                        <p class="tit_it color_grey font_s14 line_h17 font_w400">Hình thức nhập kho:</p>
                                        <p class="color_grey font_s14 line_h17 font_w500">Nhập theo yêu cầu cung ứng</p>
                                    </div>
                                </div>
                                <div class="imp_wh_df" style="display: none;">
                                    <div class="item_body_info_rq d_flex align_c ">
                                        <p class="tit_it color_grey font_s14 line_h17 font_w400">Hình thức nhập kho:</p>
                                        <p class="color_grey font_s14 line_h17 font_w500">Nhập khác</p>
                                    </div>
                                </div>
                                <div class="item_body_info_rq d_flex align_c">
                                    <p class="tit_it color_grey font_s14 line_h17 font_w400">Người tạo:</p>
                                    <div class="d_flex flex_start align_c">
                                        <img src="../images/ava_ad.png" alt="">
                                        <p class="color_grey font_s14 line_h17 font_w500">Nguyễn Văn Nam</p>
                                    </div>
                                </div>
                                <div class="item_body_info_rq d_flex align_c">
                                    <p class="tit_it color_grey font_s14 line_h17 font_w400">Ngày tạo:</p>
                                    <p class="color_grey font_s14 line_h17 font_w500">10/10/2020</p>
                                </div>
                                <div class="imp_wh_bb" style="display: block;">
                                    <div class="item_body_info_rq d_flex align_c ">
                                        <p class="tit_it color_grey font_s14 line_h17 font_w400">Đơn hàng:</p>
                                        <p class="color_grey font_s14 line_h17 font_w500">ĐH-0000</p>
                                    </div>
                                    <div class="item_body_info_rq d_flex align_c">
                                        <p class="tit_it color_grey font_s14 line_h17 font_w400">Trạng thái:</p>
                                        <p class="color_green font_s14 line_h17 font_w500 wait_add_wh">Hoàn thành - đã nhập kho</p>
                                    </div>
                                    <div class="item_body_info_rq d_flex align_c imp_wh_bb">
                                        <p class="tit_it color_grey font_s14 line_h17 font_w400">Nhà cung cấp:</p>
                                        <p class="color_grey font_s14 line_h17 font_w500">Việt Nam</p>
                                    </div>
                                    <div class="item_body_info_rq d_flex align_c">
                                        <p class="tit_it color_grey font_s14 line_h17 font_w400">Người giao hàng:</p>
                                        <p class="color_grey font_s14 line_h17 font_w500">Nguyễn Thị Hà</p>
                                    </div>
                                    <div class="item_body_info_rq d_flex align_c">
                                        <p class="tit_it color_grey font_s14 line_h17 font_w400">Người nhận:</p>
                                        <div class="d_flex flex_start align_c">
                                            <img src="../images/ava_ad.png" alt="">
                                            <p class="color_grey font_s14 line_h17 font_w500">Nguyễn Văn Nam</p>
                                        </div>
                                    </div>
                                    <div class="item_body_info_rq d_flex align_c">
                                        <p class="tit_it color_grey font_s14 line_h17 font_w400">Phòng ban:</p>
                                        <p class="color_grey font_s14 line_h17 font_w500">Phòng nhân sự</p>
                                    </div>
                                    <div class="item_body_info_rq d_flex align_c">
                                        <p class="tit_it color_grey font_s14 line_h17 font_w400">Kho nhập:</p>
                                        <p class="color_grey font_s14 line_h17 font_w500">Kho ngã tư sở</p>
                                    </div>
                                    <div class="item_body_info_rq d_flex align_c">
                                        <p class="tit_it color_grey font_s14 line_h17 font_w400">Ngày nhập kho:</p>
                                        <p class="color_grey font_s14 line_h17 font_w500">10/10/2020</p>
                                    </div>
                                </div>
                                <div class="imp_wh_dc" style="display: none;">
                                    <div class="item_body_info_rq d_flex align_c">
                                        <p class="tit_it color_grey font_s14 line_h17 font_w400">Phiếu điều chuyển:</p>
                                        <p class="color_grey font_s14 line_h17 font_w500">ĐH-0000</p>
                                    </div>
                                    <div class="item_body_info_rq d_flex align_c">
                                        <p class="tit_it color_grey font_s14 line_h17 font_w400">Trạng thái:</p>
                                        <p class="color_green font_s14 line_h17 font_w500 wait_add_wh">Hoàn thành - Đã nhập kho</p>
                                    </div>
                                    <div class="item_body_info_rq d_flex align_c">
                                        <p class="tit_it color_grey font_s14 line_h17 font_w400">Kho xuất:</p>
                                        <p class="color_grey font_s14 line_h17 font_w500">Kho 1</p>
                                    </div>
                                    <div class="item_body_info_rq d_flex align_c ">
                                        <p class="tit_it color_grey font_s14 line_h17 font_w400">Kho nhập:</p>
                                        <p class="color_grey font_s14 line_h17 font_w500">Kho 2</p>
                                    </div>
                                    <div class="item_body_info_rq d_flex align_c">
                                        <p class="tit_it color_grey font_s14 line_h17 font_w400">Người giao:</p>
                                        <div class="d_flex flex_start align_c">
                                            <img src="../images/ava_ad.png" alt="">
                                            <p class="color_grey font_s14 line_h17 font_w500">Nguyễn Thị Hà</p>
                                        </div>
                                    </div>
                                    <div class="item_body_info_rq d_flex align_c">
                                        <p class="tit_it color_grey font_s14 line_h17 font_w400">Phòng ban:</p>
                                        <p class="color_grey font_s14 line_h17 font_w500">Phòng nhân sự</p>
                                    </div>
                                    <div class="item_body_info_rq d_flex align_c">
                                        <p class="tit_it color_grey font_s14 line_h17 font_w400">Người nhận:</p>
                                        <div class="d_flex flex_start align_c">
                                            <img src="../images/ava_ad.png" alt="">
                                            <p class="color_grey font_s14 line_h17 font_w500">Nguyễn Văn Nam</p>
                                        </div>
                                    </div>
                                    <div class="item_body_info_rq d_flex align_c">
                                        <p class="tit_it color_grey font_s14 line_h17 font_w400">Phòng ban:</p>
                                        <p class="color_grey font_s14 line_h17 font_w500">Phòng nhân sự</p>
                                    </div>
                                    <div class="item_body_info_rq d_flex align_c">
                                        <p class="tit_it color_grey font_s14 line_h17 font_w400">Ngày yêu cầu:</p>
                                        <p class="color_grey font_s14 line_h17 font_w500">Kho ngã tư sở</p>
                                    </div>
                                    <div class="item_body_info_rq d_flex align_c">
                                        <p class="tit_it color_grey font_s14 line_h17 font_w400">Ngày nhập kho:</p>
                                        <p class="color_grey font_s14 line_h17 font_w500">10/10/2020</p>
                                    </div>
                                </div>
                                <div class="imp_wh_back" style="display: none;">
                                    <div class="item_body_info_rq d_flex align_c">
                                        <p class="tit_it color_grey font_s14 line_h17 font_w400">Công trình:</p>
                                        <p class="color_grey font_s14 line_h17 font_w500 wait_add_wh">Công trình 1</p>
                                    </div>
                                    <div class="item_body_info_rq d_flex align_c">
                                        <p class="tit_it color_grey font_s14 line_h17 font_w400">Trạng thái:</p>
                                        <p class="color_green font_s14 line_h17 font_w500 wait_add_wh">Hoàn thành - đã nhập kho</p>
                                    </div>
                                    <div class="item_body_info_rq d_flex align_c">
                                        <p class="tit_it color_grey font_s14 line_h17 font_w400">Người giao:</p>
                                        <div class="d_flex flex_start align_c">
                                            <img src="../images/ava_ad.png" alt="">
                                            <p class="color_grey font_s14 line_h17 font_w500">Nguyễn Thị Hà</p>
                                        </div>
                                    </div>
                                    <div class="item_body_info_rq d_flex align_c">
                                        <p class="tit_it color_grey font_s14 line_h17 font_w400">Phòng ban:</p>
                                        <p class="color_grey font_s14 line_h17 font_w500">Phòng nhân sự</p>
                                    </div>
                                    <div class="item_body_info_rq d_flex align_c">
                                        <p class="tit_it color_grey font_s14 line_h17 font_w400">Người nhận:</p>
                                        <div class="d_flex flex_start align_c">
                                            <img src="../images/ava_ad.png" alt="">
                                            <p class="color_grey font_s14 line_h17 font_w500">Nguyễn Văn Nam</p>
                                        </div>
                                    </div>
                                    <div class="item_body_info_rq d_flex align_c">
                                        <p class="tit_it color_grey font_s14 line_h17 font_w400">Phòng ban:</p>
                                        <p class="color_grey font_s14 line_h17 font_w500">Phòng nhân sự</p>
                                    </div>
                                    <div class="item_body_info_rq d_flex align_c">
                                        <p class="tit_it color_grey font_s14 line_h17 font_w400">Kho nhập:</p>
                                        <p class="color_grey font_s14 line_h17 font_w500">Kho ngã tư sở</p>
                                    </div>
                                    <div class="item_body_info_rq d_flex align_c">
                                        <p class="tit_it color_grey font_s14 line_h17 font_w400">Ngày nhập kho:</p>
                                        <p class="color_grey font_s14 line_h17 font_w500">10/10/2020</p>
                                    </div>
                                </div>
                                <div class="imp_wh_supply" style="display: none;">
                                    <div class="item_body_info_rq d_flex align_c">
                                        <p class="tit_it color_grey font_s14 line_h17 font_w400">Phiếu yêu cầu cung ứng:</p>
                                        <p class="color_grey font_s14 line_h17 font_w500 wait_add_wh">PYC-0000</p>
                                    </div>
                                    <div class="item_body_info_rq d_flex align_c">
                                        <p class="tit_it color_grey font_s14 line_h17 font_w400">Trạng thái:</p>
                                        <p class="color_green font_s14 line_h17 font_w500 wait_add_wh">Hoàn thành - đã nhập kho</p>
                                    </div>
                                    <div class="item_body_info_rq d_flex align_c">
                                        <p class="tit_it color_grey font_s14 line_h17 font_w400">Người giao:</p>
                                        <div class="d_flex flex_start align_c">
                                            <img src="../images/ava_ad.png" alt="">
                                            <p class="color_grey font_s14 line_h17 font_w500">Nguyễn Thị Hà</p>
                                        </div>
                                    </div>
                                    <div class="item_body_info_rq d_flex align_c">
                                        <p class="tit_it color_grey font_s14 line_h17 font_w400">Phòng ban:</p>
                                        <p class="color_grey font_s14 line_h17 font_w500">Phòng nhân sự</p>
                                    </div>
                                    <div class="item_body_info_rq d_flex align_c">
                                        <p class="tit_it color_grey font_s14 line_h17 font_w400">Người nhận:</p>
                                        <div class="d_flex flex_start align_c">
                                            <img src="../images/ava_ad.png" alt="">
                                            <p class="color_grey font_s14 line_h17 font_w500">Nguyễn Văn Nam</p>
                                        </div>
                                    </div>
                                    <div class="item_body_info_rq d_flex align_c">
                                        <p class="tit_it color_grey font_s14 line_h17 font_w400">Phòng ban:</p>
                                        <p class="color_grey font_s14 line_h17 font_w500">Phòng nhân sự</p>
                                    </div>
                                    <div class="item_body_info_rq d_flex align_c">
                                        <p class="tit_it color_grey font_s14 line_h17 font_w400">Kho nhập:</p>
                                        <p class="color_grey font_s14 line_h17 font_w500">Kho ngã tư sở</p>
                                    </div>
                                    <div class="item_body_info_rq d_flex align_c">
                                        <p class="tit_it color_grey font_s14 line_h17 font_w400">Ngày nhập kho:</p>
                                        <p class="color_grey font_s14 line_h17 font_w500">10/10/2020</p>
                                    </div>
                                </div>
                                <div class="imp_wh_df" style="display: none;">
                                    <div class="item_body_info_rq d_flex align_c">
                                        <p class="tit_it color_grey font_s14 line_h17 font_w400">Trạng thái:</p>
                                        <p class="color_green font_s14 line_h17 font_w500 wait_add_wh">Hoàn thành - Đã nhập kho</p>
                                    </div>
                                    <div class="item_body_info_rq d_flex align_c">
                                        <p class="tit_it color_grey font_s14 line_h17 font_w400">Người giao:</p>
                                        <div class="d_flex flex_start align_c">
                                            <img src="../images/ava_ad.png" alt="">
                                            <p class="color_grey font_s14 line_h17 font_w500">Nguyễn Thị Hà</p>
                                        </div>
                                    </div>
                                    <div class="item_body_info_rq d_flex align_c">
                                        <p class="tit_it color_grey font_s14 line_h17 font_w400">Người nhận:</p>
                                        <div class="d_flex flex_start align_c">
                                            <img src="../images/ava_ad.png" alt="">
                                            <p class="color_grey font_s14 line_h17 font_w500">Nguyễn Văn Nam</p>
                                        </div>
                                    </div>
                                    <div class="item_body_info_rq d_flex align_c">
                                        <p class="tit_it color_grey font_s14 line_h17 font_w400">Phòng ban:</p>
                                        <p class="color_grey font_s14 line_h17 font_w500">Phòng nhân sự</p>
                                    </div>
                                    <div class="item_body_info_rq d_flex align_c">
                                        <p class="tit_it color_grey font_s14 line_h17 font_w400">Kho nhập:</p>
                                        <p class="color_grey font_s14 line_h17 font_w500">Kho ngã tư sở</p>
                                    </div>
                                    <div class="item_body_info_rq d_flex align_c">
                                        <p class="tit_it color_grey font_s14 line_h17 font_w400">Ngày nhập kho:</p>
                                        <p class="color_grey font_s14 line_h17 font_w500">10/10/2020</p>
                                    </div>
                                </div>
                                <div class="item_ct_info_rq d_flex align_s">
                                    <p class="tit_it color_grey font_s14 line_h17 font_w400">Ghi chú:</p>
                                    <p class="color_grey font_s14 line_h17 font_w500">Là năng lực thực hiện các công việc, biến kiến thức thành hành động. Thông thường kỹ năng được chia thành các cấp độ chính như: bắt chước (quan sát và hành vi khuôn mẫu), ứng dụng (thực hiện một số hành động bằng cách làm theo hướng dẫn), vận dụng (chính xác hơn với mỗi hoàn cảnh), vận dụng sáng tạo (trở thành phản xạ tự nhiên).</p>
                                </div>
                                <div>
                                    <div class="item_body_info_rq d_flex align_c">
                                        <p class="tit_it color_grey font_s14 line_h17 font_w400">Người duyệt:</p>
                                        <div class="d_flex flex_start align_c">
                                            <img src="../images/ava_ad.png" alt="">
                                            <p class="color_grey font_s14 line_h17 font_w500">Nguyễn Văn Nam</p>
                                        </div>
                                    </div>
                                    <div class="item_body_info_rq d_flex align_c">
                                        <p class="tit_it color_grey font_s14 line_h17 font_w400">Ngày duyệt:</p>
                                        <p class="color_grey font_s14 line_h17 font_w500">10/10/2020</p>
                                    </div>
                                </div>
                                <div>
                                    <div class="item_body_info_rq d_flex align_c">
                                        <p class="tit_it color_grey font_s14 line_h17 font_w400">Người hoàn thành:</p>
                                        <div class="d_flex flex_start align_c">
                                            <img src="../images/ava_ad.png" alt="">
                                            <p class="color_grey font_s14 line_h17 font_w500">Nguyễn Văn Nam</p>
                                        </div>
                                    </div>
                                    <div class="item_body_info_rq d_flex align_c">
                                        <p class="tit_it color_grey font_s14 line_h17 font_w400">Ngày hoàn thành:</p>
                                        <p class="color_grey font_s14 line_h17 font_w500">10/10/2020</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- bảng danh sách vật tư -->
                <p class="tit_table_vt color_blue font_s16 line_h19 font_w700"> Danh sách vật tư</p>
                <!-- bảng bb -->
                <div class="imp_wh_bb" style="display: block;">
                    <div class="position_r d_flex align_c">
                        <div class="main_table table_vt_scr">
                            <table class="table table_list_meterial_input_info" style="width:1689px;">
                                <tr>
                                    <th>STT
                                        <span class="span_thread"></span>
                                    </th>
                                    <th>Mã vật tư
                                        <span class="span_thread"></span>
                                    </th>
                                    <th>Tên đầy đủ vật tư thiết bị
                                        <span class="span_thread"></span>
                                    </th>
                                    <th>Đơn vị tính
                                        <span class="span_thread"></span>
                                    </th>
                                    <th> Hãng sản xuất
                                        <span class="span_thread"></span>
                                    </th>
                                    <th> Xuất xứ
                                        <span class="span_thread"></span>
                                    </th>
                                    <th> Số lượng theo đơn hàng
                                        <span class="span_thread"></span>
                                    </th>
                                    <th> Số lượng thực tế nhập kho
                                        <span class="span_thread"></span>
                                    </th>
                                    <th>Đơn giá (VNĐ)
                                        <span class="span_thread"></span>
                                    </th>
                                    <th>Thành tiền (VNĐ)
                                        <span class="span_thread"></span>
                                    </th>
                                </tr>
                                <tr class="color_grey font_s14 line_h17 font_w400">
                                    <td>1</td>
                                    <td>VT-0000</td>
                                    <td class="color_blue font_s14 line_h17 font_w500">Ống nhựa</td>
                                    <td>Chai</td>
                                    <td>Việt Hà</td>
                                    <td>Việt Nam</td>
                                    <td>100</td>
                                    <td>100</td>
                                    <td>10.000.000</td>
                                    <td>10.000.000</td>
                                </tr>
                            </table>
                        </div>
                        <div class="pre_q d_flex align_c flex_center position_a">
                            <span class="pre_arrow"></span>
                        </div>
                        <div class="next_q d_flex align_c flex_center position_a">
                            <span class="next_arrow"></span>
                        </div>
                    </div>
                </div>
                <!-- bảng điều chuyển -->
                <div class="imp_wh_dc" style="display:none;">
                    <div class="position_r align_c d_flex">
                        <div class="main_table table_vt_scr">
                            <table class="table table_list_meterial-2" style="width: 1672px;">
                                <tr>
                                    <th>STT
                                        <span class="span_thread"></span>
                                    </th>
                                    <th>Mã vật tư
                                        <span class="span_thread"></span>
                                    </th>
                                    <th>Tên đầy đủ vật tư thiết bị
                                        <span class="span_thread"></span>
                                    </th>
                                    <th>Đơn vị tính
                                        <span class="span_thread"></span>
                                    </th>
                                    <th> Hãng sản xuất
                                        <span class="span_thread"></span>
                                    </th>
                                    <th> Xuất xứ
                                        <span class="span_thread"></span>
                                    </th>
                                    <th> Số lượng điều chuyển
                                        <span class="span_thread"></span>
                                    </th>
                                    <th> Số lượng thực tế nhập kho
                                        <span class="span_thread"></span>
                                    </th>
                                    <th>Đơn giá (VNĐ)
                                        <span class="span_thread"></span>
                                    </th>
                                    <th>Thành tiền (VNĐ)
                                        <span class="span_thread"></span>
                                    </th>
                                </tr>
                                <tr class="color_grey font_s14 line_h17 font_w400">
                                    <td>1</td>
                                    <td>VT-0000</td>
                                    <td class="color_blue font_s14 line_h17 font_w500">Ống nhựa</td>
                                    <td>Chai</td>
                                    <td>Việt Hà</td>
                                    <td>Việt Nam</td>
                                    <td>100</td>
                                    <td>100</td>
                                    <td>10.000.000</td>
                                    <td>10.000.000</td>
                                </tr>
                            </table>
                        </div>
                        <div class="pre_q d_flex align_c flex_center position_a">
                            <span class="pre_arrow"></span>
                        </div>
                        <div class="next_q d_flex align_c flex_center position_a">
                            <span class="next_arrow"></span>
                        </div>
                    </div>
                </div>
                <!-- bảng trả lại từ thi công -->
                <div class="imp_wh_back" style="display: none;">
                    <div class="position_r align_c d_flex">
                        <div class="main_table table_vt_scr">
                            <table class="table table_list_meterial-3" style="width: 1432px;">
                                <tr>
                                    <th>
                                        <span class="span_thread"></span>
                                    </th>
                                    <th>Mã vật tư
                                        <span class="span_thread"></span>
                                    </th>
                                    <th>Tên đầy đủ vật tư thiết bị
                                        <span class="span_thread"></span>
                                    </th>
                                    <th>Đơn vị tính
                                        <span class="span_thread"></span>
                                    </th>
                                    <th> Hãng sản xuất
                                        <span class="span_thread"></span>
                                    </th>
                                    <th> Xuất xứ
                                        <span class="span_thread"></span>
                                    </th>
                                    <th> Số lượng nhập kho
                                        <span class="span_thread"></span>
                                    </th>
                                    <th>Đơn giá (VNĐ)
                                        <span class="span_thread"></span>
                                    </th>
                                    <th>Thành tiền (VNĐ)
                                        <span class="span_thread"></span>
                                    </th>
                                </tr>

                                <tr>
                                    <td>
                                        <img src="/images/del_row.png" alt="" style="margin:0;width:16px;height:16px;">
                                    </td>
                                    <td class="color_grey font_s14 line_h17 font_w400">VT-0000</td>
                                    <td>
                                        <div class="position_r">
                                            <select class="select_tb color_blue font_s14 line_h16 font_w500" style="width: 100%">
                                                <option value="" selected hidden disabled>Chọn vật tư</option>
                                                <option value="1">Ống nhựa</option>
                                                <option value="2">Vật tư 1</option>
                                                <option value="3">Vật tư 1</option>
                                                <option value="4">Vật tư 1</option>
                                                <option value="5">Vật tư 1</option>
                                                <option value="6">Vật tư 1</option>
                                                <option value="7">Vật tư 1</option>
                                            </select>
                                        </div>
                                    </td>
                                    <td class="color_grey font_s14 line_h17 font_w400">Chai</td>
                                    <td class="color_grey font_s14 line_h17 font_w400">Việt Hà</td>
                                    <td class="color_grey font_s14 line_h17 font_w400">Việt Nam</td>
                                    <td class="color_grey font_s14 line_h17 font_w400">Nhập số lượng</td>
                                    <td class="color_grey font_s14 line_h17 font_w400">10.000.000</td>
                                    <td class="color_grey font_s14 line_h17 font_w400">10.000.000</td>
                                </tr>
                                <tr class="color_blue font_s14 line_h17 font_w500">
                                    <td colspan="2">
                                        <div class=" d_flex align_c cursor_p">
                                            <p class="add_vt d_flex flex_center align_c position_r"><span class="color_white font_s20 position_a">+</span></p>
                                            <p class="">Thêm vật tư</p>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="pre_q d_flex align_c flex_center position_a">
                            <span class="pre_arrow"></span>
                        </div>
                        <div class="next_q d_flex align_c flex_center position_a">
                            <span class="next_arrow"></span>
                        </div>
                    </div>
                </div>
                <!-- bảng theo yêu cầu vật tư -->
                <div class="imp_wh_supply" style="display: none;">
                    <div class="position_r align_c d_flex">
                        <div class="main_table table_vt_scr">
                            <table class="table table_list_meterial-5" style="width: 1855px;">
                                <tr>
                                    <th>STT
                                        <span class="span_thread"></span>
                                    </th>
                                    <th>Mã vật tư
                                        <span class="span_thread"></span>
                                    </th>
                                    <th>Tên đầy đủ vật tư thiết bị
                                        <span class="span_thread"></span>
                                    </th>
                                    <th>Đơn vị tính
                                        <span class="span_thread"></span>
                                    </th>
                                    <th> Hãng sản xuất
                                        <span class="span_thread"></span>
                                    </th>
                                    <th> Xuất xứ
                                        <span class="span_thread"></span>
                                    </th>
                                    <th> Số lượng theo yêu cầu
                                        <span class="span_thread"></span>
                                    </th>
                                    <th> Số lượng được duyệt
                                        <span class="span_thread"></span>
                                    </th>
                                    <th> Số lượng thực tế nhập kho
                                        <span class="span_thread"></span>
                                    </th>
                                    <th>Đơn giá (VNĐ)
                                        <span class="span_thread"></span>
                                    </th>
                                    <th>Thành tiền (VNĐ)
                                        <span class="span_thread"></span>
                                    </th>
                                </tr>
                                <tr class="color_grey font_s14 line_h17 font_w400">
                                    <td>
                                        1
                                    </td>
                                    <td>VT-0000</td>
                                    <td class="font_s14 line_h17 color_blue font_w500" style="text-align: left;">Ống nhựa</td>
                                    <td>Chai</td>
                                    <td>Việt Hà</td>
                                    <td>Việt Nam</td>
                                    <td>100</td>
                                    <td>100</td>
                                    <td>Nhập số lượng</td>
                                    <td>10.000.000</td>
                                    <td>10.000.000</td>
                                </tr>
                            </table>
                        </div>
                        <div class="pre_q d_flex align_c flex_center position_a">
                            <span class="pre_arrow"></span>
                        </div>
                        <div class="next_q d_flex align_c flex_center position_a">
                            <span class="next_arrow"></span>
                        </div>

                    </div>
                </div>
                <!-- bảng nhập khác -->
                <div class="imp_wh_df" style="display: none;">
                    <div class="position_r align_c d_flex">
                        <div class="main_table table_vt_scr">
                            <table class="table table_list_meterial-3" style="width: 1432px;">
                                <tr>
                                    <th>
                                        <span class="span_thread"></span>
                                    </th>
                                    <th>Mã vật tư
                                        <span class="span_thread"></span>
                                    </th>
                                    <th>Tên đầy đủ vật tư thiết bị
                                        <span class="span_thread"></span>
                                    </th>
                                    <th>Đơn vị tính
                                        <span class="span_thread"></span>
                                    </th>
                                    <th> Hãng sản xuất
                                        <span class="span_thread"></span>
                                    </th>
                                    <th> Xuất xứ
                                        <span class="span_thread"></span>
                                    </th>
                                    <th> Số lượng nhập kho
                                        <span class="span_thread"></span>
                                    </th>
                                    <th>Đơn giá (VNĐ)
                                        <span class="span_thread"></span>
                                    </th>
                                    <th>Thành tiền (VNĐ)
                                        <span class="span_thread"></span>
                                    </th>
                                </tr>
                                <tr class="color_grey font_s14 line_h17 font_w400">
                                    <td>
                                        <img src="/images/del_row.png" alt="" style="margin:0;width:16px;height:16px;">
                                    </td>
                                    <td>VT-0000</td>
                                    <td>
                                        <select class="select_tb color_grey3 font_s14 line_h17 font_w400" style="width: 100%">
                                            <option value="" selected hidden disabled>Chọn vật tư</option>
                                            <option value="1">Ống nhựa</option>
                                            <option value="2">Vật tư 1</option>
                                            <option value="3">Vật tư 1</option>
                                            <option value="4">Vật tư 1</option>
                                            <option value="5">Vật tư 1</option>
                                            <option value="6">Vật tư 1</option>
                                            <option value="7">Vật tư 1</option>
                                        </select>
                                    </td>
                                    <td>Chai</td>
                                    <td>Việt Hà</td>
                                    <td>Việt Nam</td>
                                    <td>Nhập số lượng</td>
                                    <td>10.000.000</td>
                                    <td>10.000.000</td>
                                </tr>
                                <tr class="color_blue font_s14 line_h17 font_w500">
                                    <td colspan="2">
                                        <div class=" d_flex align_c cursor_p">
                                            <p class="add_vt d_flex flex_center align_c position_r"><span class="color_white font_s20 position_a">+</span></p>
                                            <p class="">Thêm vật tư</p>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="pre_q d_flex align_c flex_center position_a">
                            <span class="pre_arrow"></span>
                        </div>
                        <div class="next_q d_flex align_c flex_center position_a">
                            <span class="next_arrow"></span>
                        </div>
                    </div>
                </div>
            </div>
            <?php include('../includes/popup_nhap-kho.php');  ?>
            <?php include('../includes/popup_overview.php');  ?>
        </div>
</body>
<script type="text/javascript" src="../js/jquery-3.4.1.min.js"></script>
<script type="text/javascript" src="../js/jquery.validate.min.js"></script>
<script type="text/javascript" src="../js/select2.min.js"></script>
<script type="text/javascript" src="../js/js_h.js"></script>
<script type="text/javascript" src="../js/validate_h.js"></script>
<script type="text/javascript" src="../js/js_k.js"></script>
<script type="text/javascript" src="../js/js_n.js"></script>

<script>
    $('.active8').each(function() {
        if ($(this).hasClass('active8')) {
            $(this).find('a').addClass('active');
        }
    });
</script>

</html>