<?php 

    include("config1.php"); 

    if(!in_array(1,$ro_nhap_kho)){
        header("Location: /tong-quan.html");
    }

    isset($_GET['page']) ? $page = $_GET['page'] : $page = 1;
    isset($_GET['all_k']) ? $all_k = $_GET['all_k'] : $all_k = "";
    isset($_GET['th']) ? $th = $_GET['th'] : $th = "";
    isset($_GET['ht']) ? $ht = $_GET['ht'] : $ht = "";

    isset($_GET['ngts']) ? $ngts = $_GET['ngts'] : $ngts = "";
    isset($_GET['ngte']) ? $ngte = $_GET['ngte'] : $ngte = "";

    isset($_GET['ngns']) ? $ngns = $_GET['ngns'] : $ngns = "";
    isset($_GET['ngne']) ? $ngne = $_GET['ngne'] : $ngne = "";

    isset($_GET['input']) ? $ip = $_GET['input'] : $ip = "";

    isset($_GET['dis']) ? $dis = $_GET['dis'] : $dis = 10;


    if (isset($_COOKIE['user']) && $_COOKIE['user'] != "" && $_COOKIE['role'] == 2) {
		$token = $_COOKIE['acc_token'];
		$curl = curl_init();
		$data = array();
		curl_setopt($curl, CURLOPT_POST, 1);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
		curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/user_info_employee.php');
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));
		$response = curl_exec($curl);
		curl_close($curl);
		$data_tt = json_decode($response, true);
		$tt_user = $data_tt['data']['user_info_result'];

	}else if (isset($_COOKIE['user']) && $_COOKIE['user'] != "" && $_COOKIE['role'] == 1) {
        $token = $_COOKIE['acc_token'];
        $curl = curl_init();
        $data = array();
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/user_info_company.php');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));
        $response = curl_exec($curl);
        curl_close($curl);
        $data_tt = json_decode($response, true);
        $tt_user = $data_tt['data']['user_info_result'];
    }
    
    $id_cty = $tt_user['com_id'];

    $all_kho = new db_query("SELECT `kho_id`, `kho_name`
    FROM `kho`
    WHERE `kho_id_ct` = $id_cty
    ORDER BY `kho_id` DESC
    ");

    

?>

<!DOCTYPE html>
<html lang="vi">

<head>
    <title>Nhập kho</title>
    <meta charset="UTF-8">
    <meta name="robots" content="noindex,nofollow" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="../css/select2.min.css?= $ver ?>">
    <link rel="stylesheet" type="text/css" href="../css/style.css?v=<?= $ver ?>">
    <link rel="stylesheet" type="text/css" href="../css/style_h.css?v=<?= $ver ?>">
    <link rel="stylesheet" type="text/css" href="../css/style_n.css?v=<?= $ver ?>">
    <link rel="stylesheet" type="text/css" href="../css/style_k.css?v<=<?= $ver ?>">
</head>

<body>
    <div class="main_wrapper_all">
        <div class="wrapper_all">
            <?php include('../includes/sidebar.php');  ?>
        </div>
        <div class="main_overview">
            <div class="header_menu_overview d_flex align_c space_b">
                <p class="color_grey line_16 font_s14 text_link_page" style="display: block;">Nhập kho</p>
                <img class="open_sidebar_w" src="../images/open_sidebar_w.png" alt="" onclick="toggle('main_sidebar')" style="display: none;">
                <?php include('../includes/header.php');  ?>
            </div>
            <div class="import_wh_main">
                <p class="font_w400 font_s14 line_h17 color_grey title_wh" style="display:none;">Nhập kho</p>
                <div class="filter_dtl mt_20 d_flex align_c">
                    <div class="all_kho mr_20">
                        <select class="select_all_kho" name="all_kho">
                            <option value="">Tất cả các kho</option>
                        <?php while($row_all_kho = mysql_fetch_assoc($all_kho->result)) {?>
                            <option value="<?= $row_all_kho['kho_id']; ?>" <?= ($all_k == $row_all_kho['kho_id']) ? "selected" : "" ?>><?= $row_all_kho['kho_name']; ?></option>
                        <? } ?>
                        </select>
                    </div>
                    <div class="date_cr d_flex space_b">
                        <p class="color_grey font_s14 line_h17 font_w400">Ngày tạo: <span class="date_start" id="date_cr_start" data="<?= $ngts ?>"><?= ($ngts != "") ?  $ngts : "yyyy/mm/dd" ?></span> -
                            <span class="date_end" id="date_cr_end" data="<?= $ngte ?>"><?= ($ngte != "") ?  $ngte : "yyyy/mm/dd" ?></span>
                        </p>
                        <img class="cursor_p" src="../images/date.png" alt="">
                    </div>
                    <div class="date_in d_flex space_b">
                        <p class="color_grey font_s14 line_h17 font_w400">Ngày nhập: <span class="date_start" id="date_in_start" data="<?= $ngns ?>"><?= ($ngns != "") ?  $ngns : "yyyy/mm/dd" ?></span> -
                            <span class="date_end" id="date_in_end" data="<?= $ngne ?>"><?= ($ngne != "") ?  $ngne : "yyyy/mm/dd" ?></span>
                        </p>
                        <img class="cursor_p" src="../images/date.png" alt="">
                    </div>

                    <div class="all_kho_export mr_20">
                        <select class="select_all_form_in" name="all_in">
                            <option value="">Tất cả hình thức nhập kho</option>
                            <option value="NK1" <?= ($ht == "NK1") ? "selected" : "" ?>>Nhập theo biên bản giao hàng</option>
                            <option value="NK2" <?= ($ht == "NK2") ? "selected" : "" ?>>Nhập điều chuyển</option>
                            <option value="NK3" <?= ($ht == "NK3") ? "selected" : "" ?>>Nhập trả lại từ thi công</option>
                            <option value="NK4" <?= ($ht == "NK4") ? "selected" : "" ?>>Nhập theo yêu cầu vật tư</option>
                            <option value="NK5" <?= ($ht == "NK5") ? "selected" : "" ?>>Nhập khác</option>
                        </select>
                    </div>
                    <div class="all_kho_status_export mr_20">
                        <select class="select_all_in_note_status" name="note_status" style="width:252px;">
                            <option value="">Tất cả trạng thái phiếu nhập kho</option>
                            <option value="1" <?= ($th == 1) ? "selected" : "" ?>>Chờ duyệt</option>
                            <option value="2" <?= ($th == 2) ? "selected" : "" ?>>Từ chối</option>
                            <option value="3" <?= ($th == 3) ? "selected" : "" ?>>Đã duyệt - Chờ nhập kho</option>
                            <option value="4" <?= ($th == 4) ? "selected" : "" ?>>Hoàn thành</option>
                        </select>
                        <div class="hd_ex1">
                            <div class="hd_ex d_flex align_c cursor_p ">
                                <img src="../images/img_hd.png" alt="">
                                <p class="color_blue font_s15 line_h18 font_w500">Hướng dẫn</p>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="operation_wh d_flex space_b" style="display: flex;">
                    <div class="search_wh d_flex space_b">
                        <div class="input_sr_wh position_r">
                            <input type="text" placeholder="Tìm kiếm theo số phiếu" name ="input_search" value="<?= ($ip != "") ? $ip : "" ?>">
                            <span class="icon_sr_wh"></span>
                        </div>
                    </div>
                    <div class="export_wh d_flex space_b align_c">
                        <?php if(in_array(2,$ro_nhap_kho)){ ?>
                            <button class="btn_px d_flex align_c">
                                <a href="/nhap-kho-create.html"></a>
                                <img src="../images/img_px.png" alt="">
                                <p class="color_white font_s15 line_h18 font_w500">Thêm mới</p>
                            </button>
                        <?php }?>
                        <button class="btn_ex d_flex align_c cursor_p">
                            <img src="../images/export.png" alt="">
                            <p class="color_white font_s15 line_h18 font_w500">Xuất excel</p>
                        </button>
                        <div class="hd_ex d_flex align_c cursor_p">
                            <img src="../images/img_hd.png" alt="">
                            <p class="color_blue font_s15 line_h18 font_w500">Hướng dẫn</p>
                        </div>
                    </div>
                </div>
                <div class="main_nhapKho" data-page="<?= $page ?>" data-k="<?= $all_k ?>" data-th="<?= $th ?>" data-ht="<?= $ht ?>" data-ngts="<?= $ngts?>" data-ngte="<?= $ngte?>" data-ngns="<?= $ngns?>" data-ngne="<?= $ngne?>" data-ip ="<?= $ip ?>" data-dis ="<?= $dis ?>">
                    <!--  -->
                </div>
                <?php include('../includes/popup_nhap-kho.php');  ?>
            </div>
        </div>
    </div>
    <?php include('../includes/popup_overview.php');  ?>
</body>
<script type="text/javascript" src="../js/jquery-3.4.1.min.js"></script>
<script type="text/javascript" src="../js/jquery.validate.min.js"></script>
<script type="text/javascript" src="../js/select2.min.js"></script>
<script type="text/javascript" src="../js/js_h.js"></script>
<script type="text/javascript" src="../js/validate_h.js"></script>
<script type="text/javascript" src="../js/js_k.js"></script>
<script type="text/javascript" src="../js/js_n.js"></script>
<script type="text/javascript" src="../js/nhap_kho.js"></script>

</html>