<?php
include("config1.php");

if (!in_array(2, $ro_xuat_kho)) {
  header("Location: /tong-quan.html");
}
$stt = 1;
if (isset($_COOKIE['user']) && $_COOKIE['user'] != "" && $_COOKIE['role'] == 1) {
  $token = $_COOKIE['acc_token'];
  $curl = curl_init();
  $data = array();
  curl_setopt($curl, CURLOPT_POST, 1);
  curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
  curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/user_info_company.php');
  curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
  curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));

  $response = curl_exec($curl);
  curl_close($curl);
  $data_tt = json_decode($response, true);
  $tt_user = $data_tt['data']['user_info_result'];
  $user_id = $_SESSION['com_id'];
  // $newArr[$value["com_id"]] = $value;
} else if (isset($_COOKIE['user']) && $_COOKIE['user'] != "" && $_COOKIE['role'] == 2) {
  $token = $_COOKIE['acc_token'];
  $curl = curl_init();
  $data = array();
  curl_setopt($curl, CURLOPT_POST, 1);
  curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
  curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/user_info_employee.php');
  curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
  curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));
  $response = curl_exec($curl);
  curl_close($curl);
  $data_tt = json_decode($response, true);
  $tt_user = $data_tt['data']['user_info_result'];
  $user_id = $_SESSION['ep_id'];
}
// echo "<pre>";
// print_r($tt_user);
// echo "</pre>";
// die();
$id_cty = $tt_user['com_id'];
$date = date('Y-m-d', time());
$stt = 1;

$item = new db_query("SELECT `kcxl_id`, `kcxl_hinhThuc`, `kcxl_nguoiTao`, `kcxl_ngayTao`, `kcxl_trangThai`, `kcxl_khoXuat`, `kcxl_ngayXuatKho`, `kcxl_ghi_chu`, `kho_name` FROM `kho-cho-xu-li`
		INNER JOIN `kho` ON `kcxl_khoXuat` = `kho_id` WHERE `kcxl_soPhieu` = 'PXK' AND `kcxl_check` = '1' AND `kcxl_id_ct` = $id_cty");

$kho = new db_query("SELECT `kho_id`, `kho_maKho`, `kho_name` FROM `kho` WHERE `kho_id_ct` = $id_cty");
$kho2 = new db_query("SELECT `kho_id`, `kho_maKho`, `kho_name` FROM `kho` WHERE `kho_id_ct` = $id_cty");
$kho3 = new db_query("SELECT `kho_id`, `kho_maKho`, `kho_name` FROM `kho` WHERE `kho_id_ct` = $id_cty");
$kho4 = new db_query("SELECT `kho_id`, `kho_maKho`, `kho_name` FROM `kho` WHERE `kho_id_ct` = $id_cty");
$arr_kho3 = [];
while ($row = mysql_fetch_assoc($kho3->result)) {
  $arr_kho3[$row["kho_id"]] = $row;
}
$pdk = new db_query("SELECT `kcxl`.`kcxl_id`, `kcxl`.`kcxl_nguoiTao`, `kcxl`.`kcxl_ngayTao`, `kcxl`.`kcxl_nguoiThucHien`, `kcxl`.`kcxl_phongBanNguoiGiao`, `kcxl`.`kcxl_nguoiNhan`, `kcxl`.`kcxl_phongBanNguoiNhan`, `kcxl`.`kcxl_khoNhap`, `kcxl`.`kcxl_khoXuat`, `kcxl`.`kcxl_ngayYeuCauHoanThanh`, `khoNhap`.`kho_name` as kho_nhap_name,`khoXuat`.`kho_name`as kho_xuat_name FROM `kho-cho-xu-li` kcxl
		LEFT JOIN `kho` `khoNhap` on `kcxl`.`kcxl_khoNhap` = `khoNhap`.`kho_id`       
		LEFT JOIN `kho` `khoXuat` on `kcxl`.`kcxl_khoXuat` = `khoXuat`.`kho_id`
		WHERE `kcxl`.`kcxl_check` = 1 AND `kcxl`.`kcxl_id_ct` = $id_cty AND `kcxl`.`kcxl_trangThai` = 7 AND `kcxl`.`kcxl_soPhieu` = 'ĐCK'");
$arr_pdk = [];
while ($row = mysql_fetch_assoc($pdk->result)) {
  $arr_pdk[$row["kcxl_id"]] = $row;
}


$dsvt_tb = new db_query("SELECT `dsvt_id`, `dsvt_name`, `dsvt_donViTinh`, `dsvt_donGia`, `dsvt_hangSanXuat`, `dsvt_xuatXu`,  `dvt_name`, `hsx_name`, `xx_name` FROM `danh-sach-vat-tu`
		LEFT JOIN `don-vi-tinh` ON `dsvt_donViTinh` = `dvt_id` AND `dvt_check` = 1
		LEFT JOIN `hang-san-xuat` ON `dsvt_hangSanXuat` = `hsx_id` AND `hsx_check` = 1
		LEFT JOIN `xuat-xu` ON `dsvt_xuatXu` = `xx_id`
		WHERE `dsvt_check` = 1 AND `dsvt_id_ct`= $id_cty");
$arr_dsvt_tb = [];
while ($row_dsvt = mysql_fetch_assoc($dsvt_tb->result)) {
  $arr_dsvt_tb[$row_dsvt["dsvt_id"]] = $row_dsvt;
}

if (isset($_SESSION['quyen']) && $_SESSION['quyen'] == 1) {
  $curl = curl_init();
  $token = $_COOKIE['acc_token'];
  curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/list_all_employee_of_company.php');
  curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
  curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));
  $response = curl_exec($curl);
  curl_close($curl);

  $data_list = json_decode($response, true);

  $data_list_nv = $data_list['data']['items'];
} elseif (isset($_SESSION['quyen']) && ($_SESSION['quyen'] == 2)) {
  $curl = curl_init();
  $token = $_COOKIE['acc_token'];
  curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/list_all_my_partner.php?get_all=true');
  curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
  curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));
  $response = curl_exec($curl);
  curl_close($curl);
  $data_list = json_decode($response, true);
  $data_list_nv = $data_list['data']['items'];
}
$newArr = [];
for ($i = 0; $i < count($data_list_nv); $i++) {
  $value = $data_list_nv[$i];
  $newArr[$value["ep_id"]] = $value;
}


$curl = curl_init();
curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($curl, CURLOPT_URL, "https://phanmemquanlycungung.timviec365.vn/api/phieu_yc_vat_tu.php");
curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
curl_setopt($curl, CURLOPT_POSTFIELDS, [
  'id_com' => $id_cty,
]);
$response = curl_exec($curl);
curl_close($curl);
$emp0 = json_decode($response, true);
$emp = $emp0['data']['items'];
$emp1 = [];
for ($i = 0; $i < count($emp); $i++) {
  $pyc = $emp[$i];
  $emp1[$pyc["id"]] = $pyc;
}

$curl = curl_init();
curl_setopt($curl, CURLOPT_POST, 1);
curl_setopt($curl, CURLOPT_URL, 'https://phanmemquanlycongtrinh.timviec365.vn/api/congtrinh.php');
curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
curl_setopt($curl, CURLOPT_POSTFIELDS, [
  'id_com' => $id_cty,
]);
$response = curl_exec($curl);
curl_close($curl);
$data_list = json_decode($response, true);
$list_phieu_vt = $data_list['data']['items'];
$list_phieu_vt1 = [];
for ($i = 0; $i < count($list_phieu_vt); $i++) {
  $ctr = $list_phieu_vt[$i];
  $list_phieu_vt1[$ctr["ctr_id"]] = $ctr;
}


$curl_dh = curl_init();
curl_setopt($curl_dh, CURLOPT_POST, 1);
curl_setopt($curl_dh, CURLOPT_URL, 'https://phanmemquanlycungung.timviec365.vn/api/don_hang.php');
curl_setopt($curl_dh, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($curl_dh, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
curl_setopt($curl_dh, CURLOPT_POSTFIELDS, [
  'com_id' => $id_cty,
  'phan_loai' => 2,
]);
$response_dh = curl_exec($curl_dh);
curl_close($curl_dh);
$data_list_dh = json_decode($response_dh, true);
$list_dh = $data_list_dh['data']['items'];
$data_dh_2 = [];
for ($i = 0; $i < count($list_dh); $i++) {
  $dh = $list_dh[$i];
  $data_dh_2[$dh["id"]] = $dh;
}

// 	echo "<pre>";
// print_r($data_dh_2); 
// echo "</pre>";
// die();
?>

<!DOCTYPE html>
<html lang="vi">

<head>
  <title>Xuất kho</title>
  <meta charset="UTF-8">
  <meta name="robots" content="noindex,nofollow" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" type="text/css" href="../css/select2.min.css?v=<?= $ver ?>">

  <link rel="stylesheet" type="text/css" href="../css/style.css?v=<?= $ver ?>">
  <link rel="stylesheet" type="text/css" href="../css/style_h.css?v=<?= $ver ?>">
  <link rel="stylesheet" type="text/css" href="../css/style_n.css?v=<?= $ver ?>">
</head>

<body class="seclec2_radius">
  <div class="main_wrapper_all">
    <div class="wapper_all">
      <?php include('../includes/sidebar.php');  ?>
    </div>
    <div class="main_overview" id="main_overview">
      <div class="header_menu_overview d_flex align_c space_b">
        <div class="text_link_header_back" style="display: block;">
          <div class=" d_flex align_c">
            <a href="/xuat-kho.html" class="cursor_p">
              <img src="../images/back_item_g.png" alt="">
            </a>
            <p class="color_grey line_16 font_s14 ml_10">Xuất kho / Thêm mới </p>
          </div>
        </div>
        <img class="open_sidebar_w" src="../images/open_sidebar_w.png" alt="" onclick="toggle('main_sidebar')">
        <?php include('../includes/header.php');  ?>
      </div>
      <div class="body_equipment_supplies">
        <div class="text_link_body_back" style="display: none;">
          <div class=" d_flex align_c mb_15">
            <a href="/xuat-kho.html" class="cursor_p">
              <img src="../images/back_item_g.png" alt="">
            </a>
            <p class="color_grey line_16 font_s14 ml_10">Thông tin vật tư thiết bị / Danh sá ch vật tư thiết bị /3
              Thêm mới</p>
          </div>
        </div>
        <form action="" method="POST" name="create_export_kho" id="create_export_kho">
          <div>
            <div class="body_equipment_supplies_create ">
              <div class="header_body_b color_white line_h19 font_s16 font_wB">
                Thêm mới phiếu xuất kho
              </div>
              <div class="body_create_export_kho">
                <div class="d_flex align_c" id="block32">
                  <div class="width_create_choose mr_10">
                    <p class="font_s15 line_h18 font_w500 color_grey">Số phiếu</p>
                    <input class="input_value_grey font_14 line_h16 mt_5 color_grey" placeholder="Hệ thống tự thiết lập" readonly type="text">
                  </div>
                  <div class="width_create_choose ml_10">
                    <p class="font_s15 line_h18 font_w500 color_grey">Hình thức xuất kho</p>
                    <div class="select_create mt_5">
                      <select class="select_create select_export_form" name="select_export_form" id="select_export_form">
                        <option value="XK1">Xuất thi công</option>
                        <option value="XK2">Xuất điều chuyển</option>
                        <option value="XK3">Xuất theo yêu cầu vật tư</option>
                        <option value="XK4">Xuất theo đơn hàng bán vật tư</option>
                        <option value="XK5">Xuất khác</option>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="d_flex align_c mt_15" id="block32">
                  <div class="width_create_choose mr_10">
                    <p class="font_s15 line_h18 font_w500 color_grey">Người tạo</p>
                    <?php if ($_COOKIE['role'] == 2) { ?>
                      <input class="input_value_grey font_14 line_h16 mt_5 color_grey nguoi_tao" readonly type="text" value="<?= $newArr[$user_id]['ep_name'] ?>" data="<?= $newArr[$user_id]['ep_id'] ?>">
                    <? } ?>
                    <?php if ($_COOKIE['role'] == 1) { ?>
                      <input class="input_value_grey font_14 line_h16 mt_5 color_grey nguoi_tao" readonly type="text" value="<?= $tt_user['com_name'] ?>" data="0">
                    <? } ?>
                  </div>
                  <div class="width_create_choose ml_10" id="block33">
                    <p class="font_s15 line_h18 font_w500 color_grey">Ngày tạo</p>
                    <input class="input_value_grey font_14 line_h16 mt_5 color_grey ngay_tao" readonly type="text" data="<?= $date ?>" value="<?= $date ?>">
                  </div>
                </div>

                <div class="selected_construction_export" style="display: block;">
                  <div class="d_flex align_c mt_15" id="block32">
                    <div class="width_create_choose mr_10 request_form ">
                      <div class="d_flex">
                        <p class="font_s15 line_h18 font_w500 color_grey">Phiếu yêu cầu</p>
                        <span class="color_red alert_red">*</span>
                      </div>
                      <div class="select_create select_create_1 mt_5">
                        <select class="select_create select_request_form" name="select_request_form" id="select_request_form">
                          <option></option>
                          <? foreach ($emp1 as $key => $value) { ?>
                            <option value="<?= $key ?>"><?= 'PYC' . ' ' . '-' . ' ' . $value['id'] ?></option>
                          <? } ?>
                        </select>
                      </div>
                    </div>
                    <div class="width_create_choose ml_10" id="block33">
                      <div class="d_flex align_c" id="block28">
                        <div class="w_100 " id="block34">
                          <p class="font_s15 line_h18 font_w500 color_grey">Trạng thái</p>
                          <div class="select_create mt_5">
                            <select class="select_create select_status" name="select_status1" id="select_status1">
                              <option value="1">Khởi tạo</option>
                              <?= ($_SESSION['quyen'] == '1' || in_array(5, $ro_xuat_kho)) ? "<option value='2'>Hoàn thành</option>" : "" ?>
                            </select>
                          </div>
                        </div>
                        <div class="w_50 ml_10 date_accses" id="block35" style="display:none">
                          <div class="d_flex">
                            <p class="font_s15 line_h18 font_w500 color_grey">Ngày hoàn thành</p>
                            <span class="color_red alert_red">*</span>
                          </div>
                          <div class="select_create1 mt_5 select_create">
                            <input class="input_value_w" name="date_accses_export1" id="date_accses_export1" type="date">
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="d_flex align_c mt_15" id="block32">
                    <div class="width_create_choose mr_10">
                      <p class="font_s15 line_h18 font_w500 color_grey">Công trình</p>
                      <input class="input_value_grey font_14 line_h16 mt_5 cong_trinh" readonly type="text" placeholder="Hiện tên công trình từ phiếu yêu cầu">
                      <input class="input_value_grey font_14 line_h16 mt_5 id_cong_trinh" readonly type="hidden" placeholder="Hiện tên công trình từ phiếu yêu cầu">
                    </div>
                    <div class="width_create_choose ml_10" id="block33">
                      <p class="font_s15 line_h18 font_w500 color_grey">Ngày yêu cầu hoàn thành</p>
                      <input class="input_value_grey font_14 line_h16 mt_5 ngay_ht_yc" readonly type="date" placeholder="Hiện ngày yêu cầu theo phiếu yêu cầu">
                    </div>
                  </div>
                  <div class="d_flex align_c mt_15" id="block32">
                    <div class="width_create_choose mr_10">
                      <p class="font_s15 line_h18 font_w500 color_grey">Người giao hàng</p>
                      <div class="select_create mt_5">
                        <select class="select_create select_deliver" name="select_deliver" id="select_deliver">
                          <option value="">Chọn người giao</option>
                          <? foreach ($newArr as $key => $value) { ?>
                            <option value="<?= $key ?>"><?= $value['ep_name'] . ' ' . '-' . ' ' . $value['dep_name'] ?></option>
                          <? } ?>
                        </select>
                      </div>
                    </div>
                    <div class="width_create_choose ml_10" id="block33">
                      <p class="font_s15 line_h18 font_w500 color_grey">Phòng ban</p>
                      <input class="input_value_grey font_14 line_h16 mt_5 phong_ban_giao" readonly type="text" placeholder="Hiển thị phòng ban người giao">
                      <input class="input_value_grey font_14 line_h16 mt_5 id_phong_ban_giao" readonly type="hidden" placeholder="Hiển thị phòng ban người giao">
                    </div>
                  </div>
                  <div class="d_flex align_c mt_15" id="block32">
                    <div class="width_create_choose mr_10">
                      <p class="font_s15 line_h18 font_w500 color_grey">Người nhận</p>
                      <input class="input_value_grey font_14 line_h16 mt_5 nguoi_nhan" readonly type="text" placeholder="Hiển thị tên người nhận từ đơn hàng">
                      <input class="input_value_grey font_14 line_h16 mt_5 id_nguoi_nhan" readonly type="hidden" placeholder="Hiển thị tên người nhận từ đơn hàng">
                    </div>
                    <div class="width_create_choose ml_10" id="block33">
                      <p class="font_s15 line_h18 font_w500 color_grey">Phòng ban</p>
                      <input class="input_value_grey font_14 line_h16 mt_5 phong_ban_nhan" readonly type="text" placeholder="Hiển thị phòng ban người nhận từ đơn hàng">
                      <input class="input_value_grey font_14 line_h16 mt_5 id_phong_ban_nhan" readonly type="hidden" placeholder="Hiển thị phòng ban người nhận từ đơn hàng">
                    </div>
                  </div>
                  <div class="d_flex align_c mt_15" id="block32">
                    <div class="width_create_choose mr_10 export_kho">
                      <div class="d_flex">
                        <p class="font_s15 line_h18 font_w500 color_grey">Kho xuất</p>
                        <span class="color_red alert_red">*</span>
                      </div>
                      <div class="select_create">
                        <select class="select_create select_export_kho" name="select_export_kho1" id="select_export_kho1">
                          <option value="">Chọn kho xuất</option>
                          <? while (($selected = mysql_fetch_assoc($kho2->result))) { ?>
                            <option value="<?= $selected['kho_id'] ?>"><?= $selected['kho_name'] ?></option>
                          <? } ?>
                        </select>
                      </div>
                    </div>
                    <div class="width_create_choose ml_10 date_export_kho" id="block33">
                      <div class="d_flex">
                        <p class="font_s15 line_h18 font_w500 color_grey">Ngày xuất kho</p>
                        <span class="color_red alert_red">*</span>
                      </div>
                      <input class="input_value_w" name="selected_date_export_kho" id="selected_date_export_kho" type="date">
                    </div>
                  </div>
                </div>

                <div class="selected_export_transfer" style="display: none;">
                  <div class="d_flex mt_15" id="block32">
                    <div class="width_create_choose mr_10 transfer_slip">
                      <div class="d_flex">
                        <p class="font_s15 line_h18 font_w500 color_grey">Phiếu điều chuyển</p>
                        <span class="color_red alert_red">*</span>
                      </div>
                      <div class="select_create select_create2 mt_5">
                        <select class="select_create select_transfer_slip" name="select_transfer_slip" id="select_transfer_slip">
                          <option value="">Chọn phiếu điều chuyển</option>
                          <? foreach ($arr_pdk as $key => $value) { ?>
                            <option value="<?= $value['kcxl_id'] ?>"><?= 'ĐCK' . ' ' . '-' . ' ' . $value['kcxl_id'] . ' ' . 'chuyển từ' . ' ' . $value['kho_xuat_name'] . ' ' . 'đến' . ' ' . $value['kho_nhap_name'] ?></option>
                          <? } ?>
                        </select>
                      </div>
                    </div>
                    <div class="width_create_choose ml_10" id="block33">
                      <div class="d_flex align_c" id="block28">
                        <div class="w_100 " id="block34">
                          <p class="font_s15 line_h18 font_w500 color_grey">Trạng thái</p>
                          <div class="select_create mt_5">
                            <select class="select_create select_status" name="select_status2" id="select_status2">
                              <option value="1">Khởi tạo</option>
                              <?= ($_SESSION['quyen'] == '1' || in_array(5, $ro_xuat_kho)) ? "<option value='2'>Hoàn thành</option>" : "" ?>
                            </select>
                          </div>
                        </div>
                        <div class="w_50 ml_10 date_accses" id="block35" style="display:none">
                          <div class="d_flex">
                            <p class="font_s15 line_h18 font_w500 color_grey">Ngày hoàn thành</p>
                            <span class="color_red alert_red">*</span>
                          </div>
                          <div class="select_create mt_5 date_accses_2">
                            <input class="input_value_w" type="date" name="date_accses_export_2" id="date_accses_export_2">
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="d_flex mt_15 " id="block32">
                    <div class="width_create_choose mr_10">
                      <p class="font_s15 line_h18 font_w500 color_grey">Kho xuất</p>
                      <input class="input_value_grey font_14 line_h16 mt_5 kho_xuat_1" readonly type="text" placeholder="Hiển thị kho xuất từ phiếu điều chuyển">
                      <input class="input_value_grey font_14 line_h16 mt_5 id_kho_xuat_1" name="id_kho_xuat_1" readonly type="hidden" placeholder="Hiển thị kho xuất từ phiếu điều chuyển">
                    </div>
                    <div class="width_create_choose ml_10 date_xuat_2" id="block33">
                      <div class="d_flex">
                        <p class="font_s15 line_h18 font_w500 color_grey">Ngày xuất kho</p>
                        <span class="color_red alert_red">*</span>
                      </div>
                      <div class="">
                        <input class="input_value_w mt_5 " name="name_date_xuat_2" id="name_date_xuat_2" type="date">
                      </div>
                    </div>
                  </div>
                  <div class="d_flex align_c mt_15" id="block32">
                    <div class="width_create_choose mr_10">
                      <p class="font_s15 line_h18 font_w500 color_grey">Kho nhập</p>
                      <input class="input_value_grey font_14 line_h16 mt_5 kho_nhap_1" readonly type="text" placeholder="Hiển thị kho nhập từ phiếu điều chuyển">
                      <input class="input_value_grey font_14 line_h16 mt_5 id_kho_nhap_1" name="id_kho_nhap_1" readonly type="hidden" placeholder="Hiển thị kho nhập từ phiếu điều chuyển">
                    </div>
                    <div class="width_create_choose ml_10" id="block33">
                      <div class="d_flex">
                        <p class="font_s15 line_h18 font_w500 color_grey">Ngày yêu cầu hoàn thành điều chuyển</p>
                      </div>
                      <input class="input_value_grey mt_5 ngay_yeu_cau_1" name="ngay_yeu_cau_1" readonly type="date" placeholder="Hiển thị ngày yêu cầu hoàn thành điều chuyển">
                    </div>
                  </div>
                  <div class="d_flex align_c mt_15" id="block32">
                    <div class="width_create_choose mr_10">
                      <p class="font_s15 line_h18 font_w500 color_grey">Người giao hàng</p>
                      <input class="input_value_grey font_14 line_h16 mt_5 nguoi_giao_1" readonly type="text" placeholder="Hiển thị tên người giao hàng từ phiếu điều chuyển">
                      <input class="input_value_grey font_14 line_h16 mt_5 id_nguoi_giao_1" name="id_nguoi_giao_1" readonly type="hidden" placeholder="Hiển thị tên người giao hàng từ phiếu điều chuyển">
                    </div>
                    <div class="width_create_choose ml_10" id="block33">
                      <div class="d_flex">
                        <p class="font_s15 line_h18 font_w500 color_grey">Phòng ban</p>
                      </div>
                      <input class="input_value_grey mt_5 phong_ban_giao_1" readonly type="text" placeholder="Hiển thị phòng ban người giao">
                      <input class="input_value_grey mt_5 id_phong_ban_giao_1" readonly type="hidden" placeholder="Hiển thị phòng ban người giao">
                    </div>
                  </div>
                  <div class="d_flex align_c mt_15" id="block32">
                    <div class="width_create_choose mr_10">
                      <p class="font_s15 line_h18 font_w500 color_grey">Người nhận</p>
                      <input class="input_value_grey font_14 line_h16 mt_5 nguoi_nhan_1" readonly type="text" placeholder="Hiển thị tên người nhận từ phiếu điều chuyển">
                      <input class="input_value_grey font_14 line_h16 mt_5 id_nguoi_nhan_1" readonly type="hidden" placeholder="Hiển thị tên người nhận từ phiếu điều chuyển">
                    </div>
                    <div class="width_create_choose ml_10" id="block33">
                      <div class="d_flex">
                        <p class="font_s15 line_h18 font_w500 color_grey">Phòng ban</p>
                      </div>
                      <input class="input_value_grey mt_5 phong_ban_nhan_1" readonly type="text" placeholder="Hiển thị phòng ban người nhận">
                      <input class="input_value_grey mt_5 id_phong_ban_nhan_1" readonly type="hidden" placeholder="Hiển thị phòng ban người nhận">
                    </div>
                  </div>
                </div>

                <div class="selected_export_on_demand" style="display: none;">
                  <div class="d_flex align_c mt_15" id="block32">
                    <div class="width_create_choose mr_10">
                      <div class="d_flex">
                        <p class="font_s15 line_h18 font_w500 color_grey">Phiếu yêu cầu</p>
                        <span class="color_red alert_red">*</span>
                      </div>
                      <div class="select_create request_form1 mt_5">
                        <select class="select_create select_request_form1" name="select_request_form1" id="select_request_form1">
                          <option></option>
                          <? foreach ($emp1 as $key => $value) { ?>
                            <option value="<?= $key ?>"><?= 'PYC' . ' ' . '-' . ' ' . $value['id'] ?></option>
                          <? } ?>
                        </select>
                      </div>
                    </div>
                    <div class="width_create_choose ml_10" id="block33">
                      <div class="d_flex align_c" id="block28">
                        <div class="w_100 " id="block34">
                          <p class="font_s15 line_h18 font_w500 color_grey">Trạng thái</p>
                          <div class="select_create mt_5">
                            <select class="select_create select_status" name="select_status3" id="select_status3">
                              <option value="1">Khởi tạo</option>
                              <?= ($_SESSION['quyen'] == '1' || in_array(5, $ro_xuat_kho)) ? "<option value='2'>Hoàn thành</option>" : "" ?>
                            </select>
                          </div>
                        </div>
                        <div class="w_50 ml_10 date_accses" id="block35" style="display:none">
                          <div class="d_flex">
                            <p class="font_s15 line_h18 font_w500 color_grey">Ngày hoàn thành</p>
                            <span class="color_red alert_red">*</span>
                          </div>
                          <div class="select_create mt_5 date_accses_3">
                            <input class="input_value_w" type="date" name="date_accses_export_3" id="date_accses_export_3">
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="d_flex mt_15" id="block32">
                    <div class="width_create_choose mr_10">
                      <p class="font_s15 line_h18 font_w500 color_grey">Kho xuất</p>
                      <input class="input_value_grey mt_5 kho_xuat_3" readonly type="text" placeholder="Hiển thị kho xuất theo phiếu yêu cầu">
                      <input class="input_value_grey mt_5 id_kho_xuat_3" readonly type="hidden" placeholder="Hiển thị kho xuất theo phiếu yêu cầu">
                    </div>
                    <div class="width_create_choose ml_10 date_xuat_3" id="block33">
                      <div class="d_flex">
                        <p class="font_s15 line_h18 font_w500 color_grey">Ngày xuất kho</p>
                        <span class="color_red alert_red">*</span>
                      </div>
                      <input class="input_value_w mt_5 ngay_xuat_3" id="ngay_xuat_3" name="ngay_xuat_3" type="date">
                    </div>
                  </div>
                  <div class="d_flex align_c mt_15" id="block32">
                    <div class="width_create_choose mr_10">
                      <!-- <p class="font_s15 line_h18 font_w500 color_grey">Kho nhập</p>
 											<input class="input_value_grey mt_5" readonly type="date" placeholder="Hiển thị kho nhập theo phiếu yêu cầu"> -->
                    </div>
                    <div class="width_create_choose ml_10" id="block33">
                      <p class="font_s15 line_h18 font_w500 color_grey">Ngày yêu cầu hoàn thành</p>
                      <input class="input_value_grey mt_5 date_yc_3" readonly type="date" placeholder="Hiển thị ngày yêu cầu hoàn thành cung ứng">
                    </div>
                  </div>
                  <div class="d_flex align_c mt_15" id="block32">
                    <div class="width_create_choose mr_10">
                      <p class="font_s15 line_h18 font_w500 color_grey">Người giao hàng</p>
                      <div class="select_create mt_5">
                        <select class="select_create select_deliver1" name="select_deliver1">
                          <option value="">Chọn người giao</option>
                          <? foreach ($newArr as $key => $value) { ?>
                            <option value="<?= $key ?>"><?= $value['ep_name'] . ' ' . '-' . ' ' . $value['dep_name'] ?></option>
                            <? } ?>>
                        </select>
                      </div>
                    </div>
                    <div class="width_create_choose ml_10" id="block33">
                      <p class="font_s15 line_h18 font_w500 color_grey">Phòng ban</p>
                      <input class="input_value_grey mt_5 phong_ban_giao1" readonly type="text" placeholder="Hiển thị phòng ban người giao">
                      <input class="input_value_grey mt_5 id_phong_ban_giao1" readonly type="hidden" placeholder="Hiển thị phòng ban người giao">
                    </div>
                  </div>
                  <div class="d_flex align_c mt_15" id="block32">
                    <div class="width_create_choose mr_10">
                      <p class="font_s15 line_h18 font_w500 color_grey">Người nhận</p>
                      <div class="select_create mt_5">
                        <select class="select_create select_receiver1" name="select_receiver1">
                          <option value="">Chọn người nhận</option>
                          <? foreach ($newArr as $key => $value) { ?>
                            <option value="<?= $key ?>"><?= $value['ep_name'] . ' ' . '-' . ' ' . $value['dep_name'] ?></option>
                            <? } ?>>
                        </select>
                      </div>
                    </div>
                    <div class="width_create_choose ml_10" id="block33">
                      <p class="font_s15 line_h18 font_w500 color_grey">Phòng ban</p>
                      <input class="input_value_grey mt_5 phong_ban_nhan1" readonly type="text" placeholder="Hiển thị phòng ban người nhận">
                      <input class="input_value_grey mt_5 id_phong_ban_nhan1" readonly type="hidden" placeholder="Hiển thị phòng ban người nhận">
                    </div>
                  </div>
                </div>

                <div class="selected_export_by_order" style="display: none;">
                  <div class="d_flex align_c mt_15" id="block32">
                    <div class="width_create_choose mr_10">
                      <div class="d_flex">
                        <p class="font_s15 line_h18 font_w500 color_grey">Đơn hàng</p>
                        <span class="color_red alert_red">*</span>
                      </div>
                      <div class="select_create choose_select_order mt_5">
                        <select class="select_create select_order" name="select_order" id="select_order">
                          <option>
                          <option></option>
                          <? foreach ($data_dh_2 as $key => $value) { ?>
                            <option value="<?= $value['id'] ?>"><?= 'ĐH' . ' ' . '-' . ' ' . $value['id'] ?></option>
                          <? } ?>
                          </option>
                        </select>
                      </div>
                    </div>
                    <div class="width_create_choose ml_10" id="block33">
                      <div class="d_flex align_c" id="block28">
                        <div class="w_100 " id="block34">
                          <p class="font_s15 line_h18 font_w500 color_grey">Trạng thái</p>
                          <div class="select_create mt_5">
                            <select class="select_create select_status" name="select_status4" id="select_status4">
                              <option value="1">Khởi tạo</option>
                              <?= ($_SESSION['quyen'] == '1' || in_array(5, $ro_xuat_kho)) ? "<option value='2'>Hoàn thành</option>" : "" ?>
                            </select>
                          </div>
                        </div>
                        <div class="w_50 ml_10 date_accses " id="block35" style="display:none">
                          <div class="d_flex">
                            <p class="font_s15 line_h18 font_w500 color_grey">Ngày hoàn thành</p>
                            <span class="color_red alert_red">*</span>
                          </div>
                          <div class="select_create mt_5 date_accses_4">
                            <input class="input_value_w" type="date" name="date_accses_export_4" id="date_accses_export_4">
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="d_flex align_c mt_15" id="block32">
                    <div class="width_create_choose mr_10 choose_select_export_kho_2">
                      <div class="d_flex">
                        <p class="font_s15 line_h18 font_w500 color_grey">Kho xuất</p>
                        <span class="color_red alert_red">*</span>
                      </div>
                      <div class="select_create">
                        <select class="select_create select_export_kho" name="select_export_kho2" id="select_export_kho2">
                          <option value="">Chọn kho xuất</option>
                          <? while (($selected = mysql_fetch_assoc($kho4->result))) { ?>
                            <option value="<?= $selected['kho_id'] ?>"><?= $selected['kho_name'] ?></option>
                          <? } ?>
                        </select>
                      </div>
                    </div>
                    <div class="width_create_choose ml_10 select_date_xuat_4" id="block33">
                      <div class="d_flex">
                        <p class="font_s15 line_h18 font_w500 color_grey">Ngày xuất kho</p>
                        <span class="color_red alert_red">*</span>
                      </div>
                      <input class="input_value_w mt_5 date_xuat_4" type="date" name="date_xuat_4" id="date_xuat_4">
                    </div>
                  </div>
                  <div class="d_flex align_c mt_15" id="block32">
                    <div class="width_create_choose mr_10">
                      <p class="font_s15 line_h18 font_w500 color_grey">Thời hạn hoàn thành</p>
                      <input class="input_value_grey mt_5 time_ht" readonly type="text" placeholder="Hiển thị ngày hoàn thành từ đơn hàng">
                    </div>
                    <div class="width_create_choose ml_10">
                    </div>
                  </div>
                  <div class="d_flex align_c mt_15" id="block32">
                    <div class="width_create_choose mr_10">
                      <div class="d_flex align_c space_b">
                        <div class="d_flex">
                          <p class="font_s15 line_h18 font_w500 color_grey">Người xuất kho</p>
                          <span class="color_red alert_red">*</span>
                        </div>
                        <div class="d_flex align_c">
                          <img src="../images/create.png" alt="">
                          <p class="font_s14 line_h16 color_blue ml_5">Thêm nhân viên</p>
                        </div>
                      </div>
                      <div class="select_create mt_5 choose_select_human_export">
                        <select class="select_create select_human_export" name="select_human_export" id="select_human_export">
                          <option value="">Chọn người xuất kho</option>
                          <? foreach ($newArr as $key => $value) { ?>
                            <option value="<?= $key ?>"><?= $value['ep_name'] . ' ' . '-' . ' ' . $value['dep_name'] ?></option>
                            <? } ?>>
                        </select>
                      </div>
                    </div>
                    <div class="width_create_choose ml_10" id="block33">
                      <p class="font_s15 line_h18 font_w500 color_grey">Phòng ban</p>
                      <input class="input_value_grey mt_5 ten_pb_4" readonly type="text" placeholder="Hiển thị phòng ban người giao">
                      <input class="input_value_grey mt_5 id_pb_4" readonly type="hidden" placeholder="Hiển thị phòng ban người giao">
                    </div>
                  </div>
                </div>

                <div class="selected_other_export" style="display: none;">
                  <div class="d_flex align_c mt_15" id="block32">
                    <div class="width_create_choose mr_10">
                      <div class="d_flex align_c" id="block28">
                        <div class="w_100" id="block34">
                          <p class="font_s15 line_h18 font_w500 color_grey">Trạng thái</p>
                          <div class="select_create mt_5">
                            <select class="select_create select_status" name="select_status5" id="select_status5">
                              <option value="1">Khởi tạo</option>
                              <?= ($_SESSION['quyen'] == '1' || in_array(5, $ro_xuat_kho)) ? "<option value='2'>Hoàn thành</option>" : "" ?>
                            </select>
                          </div>
                        </div>
                        <div class="w_50 ml_10 date_accses" id="block35" style="display:none">
                          <div class="d_flex">
                            <p class="font_s15 line_h18 font_w500 color_grey">Ngày hoàn thành</p>
                            <span class="color_red alert_red">*</span>
                          </div>
                          <div class="select_create mt_5 date_accses_5">
                            <input class="input_value_w" type="date" name="date_accses_export_5" id="date_accses_export_5">
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="width_create_choose ml_10">
                    </div>
                  </div>
                  <div class="d_flex mt_15" id="block32">
                    <div class="width_create_choose mr_10">
                      <div class="d_flex">
                        <p class="font_s15 line_h18 font_w500 color_grey">Kho xuất</p>
                        <span class="color_red alert_red">*</span>
                      </div>
                      <div class="select_create mt_5 choose_select_export_kho_5">
                        <select class="select_create select_export_kho_5" name="select_export_kho_5" id="select_export_kho_5">
                          <option value="">Chọn kho xuất</option>
                          <? while (($selected = mysql_fetch_assoc($kho->result))) { ?>
                            <option value="<?= $selected['kho_id'] ?>"><?= $selected['kho_name'] ?></option>
                          <? } ?>
                        </select>
                      </div>
                    </div>
                    <div class="width_create_choose ml_10" id="block33">
                      <div class="select_date_xuat_6">
                        <div class="d_flex">
                          <p class="font_s15 line_h18 font_w500 color_grey">Ngày xuất kho</p>
                          <span class="color_red alert_red">*</span>
                        </div>
                        <div class="tb_date_xuat_6">
                          <input class="input_value_w mt_5 date_xuat_6" type="date" name="date_xuat_6">
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="d_flex align_c mt_15" id="block32">
                    <div class="width_create_choose mr_10">
                      <div class="d_flex align_c space_b">
                        <p class="font_s15 line_h18 font_w500 color_grey">Người giao hàng</p>
                        <div class="d_flex align_c">
                          <img src="../images/create.png" alt="">
                          <p class="font_s14 line_h16 color_blue ml_5">Thêm nhân viên</p>
                        </div>
                      </div>
                      <div class="select_create mt_5">
                        <select class="select_create select_deliver" name="select_deliver_5" id="select_deliver_5">
                          <option value="">Chọn người giao</option>
                          <? foreach ($newArr as $key => $value) { ?>
                            <option value="<?= $key ?>"><?= $value['ep_name'] . ' ' . '-' . ' ' . $value['dep_name'] ?></option>
                          <? } ?>
                        </select>
                      </div>
                    </div>
                    <div class="width_create_choose ml_10" id="block33">
                      <p class="font_s15 line_h18 font_w500 color_grey">Phòng ban</p>
                      <input class="input_value_grey mt_5 ten_pbg_5" readonly type="text" placeholder="Hiển thị phòng ban người giao">
                      <input class="input_value_grey mt_5 id_pbg_5" readonly type="hidden" placeholder="Hiển thị phòng ban người giao">
                    </div>
                  </div>
                  <div class="d_flex align_c mt_15" id="block32">
                    <div class="width_create_choose mr_10">
                      <div class="d_flex align_c space_b">
                        <p class="font_s15 line_h18 font_w500 color_grey">Người nhận</p>
                        <div class="d_flex align_c">
                          <img src="../images/create.png" alt="">
                          <p class="font_s14 line_h16 color_blue ml_5">Thêm nhân viên</p>
                        </div>
                      </div>
                      <div class="select_create mt_5">
                        <select class="select_create select_receiver" name="select_receiver_5" id="select_receiver_5" style="width: 100%">
                          <option value="">Chọn người giao</option>
                          <? foreach ($newArr as $key => $value) { ?>
                            <option value="<?= $key ?>"><?= $value['ep_name'] . ' ' . '-' . ' ' . $value['dep_name'] ?></option>
                          <? } ?>
                        </select>
                      </div>
                    </div>
                    <div class="width_create_choose ml_10" id="block33">
                      <p class="font_s15 line_h18 font_w500 color_grey">Phòng ban</p>
                      <input class="input_value_grey mt_5 ten_pbn_5" readonly type="text" placeholder="Hiển thị phòng ban người nhận">
                      <input class="input_value_grey mt_5 id_pbn_5" readonly type="hidden" placeholder="Hiển thị phòng ban người nhận">
                    </div>
                  </div>
                </div>

                <div class="d_flex flex_column mt_15">
                  <p class="color_grey font_s15 line_h18 font_w500">Ghi chú</p>
                  <textarea class="mt_5 note" name="description" id="description" rows="5" placeholder="Nhập nội dung"></textarea>
                </div>
              </div>
            </div>
          </div>
          <p class="mt_20 color_blue line_h19 font_s16 font_wB">Danh sách vật tư</p>
          <div class="main_table mt_20">

            <div class="table_construction_export" style="display: block;">
              <div class="position_r d_flex align_c">
                <div class="table_vt_scr">
                  <table class="table table_1" id="table_1">
                    <tr class="color_white font_s16 line_h19 font_w500">
                      <th>STT
                        <span class="span_tbody"></span>
                      </th>
                      <th>Mã vật tư thiết bị
                        <span class="span_tbody"></span>
                      </th>
                      <th>Tên đầy đủ vật tư thiết bị
                        <span class="span_tbody"></span>
                      </th>
                      <th>Đơn vị tính
                        <span class="span_tbody"></span>
                      </th>
                      <th>Hãng sản xuất
                        <span class="span_tbody"></span>
                      </th>
                      <th>Xuất xứ
                        <span class="span_tbody"></span>
                      </th>
                      <th>Số lượng yêu cầu
                        <span class="span_tbody"></span>
                      </th>
                      <th>Số lượng được duyệt
                        <span class="span_tbody"></span>
                      </th>
                      <th>Số lượng thực tế xuất kho
                        <span class="span_tbody"></span>
                      </th>
                      <th>Đơn giá (VNĐ)
                        <span class="span_tbody"></span>
                      </th>
                      <th>Thành tiền (VNĐ)
                      </th>
                    </tr>
                    <tbody class="data_table_1" id="dom-table">
                    </tbody>
                  </table>
                </div>
                <div class="pre_q d_flex align_c flex_center position_a">
                  <span class="pre_arrow"></span>
                </div>
                <div class="next_q d_flex align_c flex_center position_a">
                  <span class="next_arrow"></span>
                </div>
              </div>
            </div>

            <div class="table_export_transfer" style="display: none;">
              <div class="position_r d_flex align_c">
                <div class="table_vt_scr">
                  <table class="table table_2">
                    <tr class="color_white font_s16 line_h19 font_w500">
                      <th>STT
                        <span class="span_tbody"></span>
                      </th>
                      <th>Mã vật tư thiết bị
                        <span class="span_tbody"></span>
                      </th>
                      <th>Tên đầy đủ vật tư thiết bị
                        <span class="span_tbody"></span>
                      </th>
                      <th>Đơn vị tính
                        <span class="span_tbody"></span>
                      </th>
                      <th>Hãng sản xuất
                        <span class="span_tbody"></span>
                      </th>
                      <th>Xuất xứ
                        <span class="span_tbody"></span>
                      </th>
                      <th>Số lượng yêu cầu
                        <span class="span_tbody"></span>
                      </th>
                      <th>Số lượng thực tế xuất kho
                        <span class="span_tbody"></span>
                      </th>
                      <th>Đơn giá (VNĐ)
                        <span class="span_tbody"></span>
                      </th>
                      <th>Thành tiền (VNĐ)
                      </th>
                    </tr>
                    <tbody class="data_table_2">

                    </tbody>
                  </table>
                </div>
                <div class="pre_q d_flex align_c flex_center position_a">
                  <span class="pre_arrow"></span>
                </div>
                <div class="next_q d_flex align_c flex_center position_a">
                  <span class="next_arrow"></span>
                </div>
              </div>
            </div>

            <div class="table_export_by_order" style="display: none;">
              <div class="position_r d_flex align_c">
                <div class="table_vt_scr">
                  <table class="table table_3">
                    <tr class="color_white font_s16 line_h19 font_w500">
                      <th>
                        <span class="span_tbody"></span>
                      </th>
                      <th>Mã vật tư thiết bị
                        <span class="span_tbody"></span>
                      </th>
                      <th>Tên đầy đủ vật tư thiết bị
                        <span class="span_tbody"></span>
                      </th>
                      <th>Đơn vị tính
                        <span class="span_tbody"></span>
                      </th>
                      <th>Hãng sản xuất
                        <span class="span_tbody"></span>
                      </th>
                      <th>Xuất xứ
                        <span class="span_tbody"></span>
                      </th>
                      <th>Số lượng xuất kho
                        <span class="span_tbody"></span>
                      </th>
                      <th>Đơn giá (VNĐ)
                        <span class="span_tbody"></span>
                      </th>
                      <th>Thành tiền (VNĐ)
                      </th>
                    </tr>
                    <tbody class="table_khac">

                    </tbody>
                    <tr>
                      <td colspan="11">
                        <div class="d_flex align_c add_row">
                          <img src="../images/add_vt.png" alt="">
                          <p class="color_blue font_s14 line_h16 font_w500">Thêm vật tư</p>
                        </div>
                      </td>
                    </tr>
                  </table>
                </div>
                <div class="pre_q d_flex align_c flex_center position_a">
                  <span class="pre_arrow"></span>
                </div>
                <div class="next_q d_flex align_c flex_center position_a">
                  <span class="next_arrow"></span>
                </div>
              </div>
            </div>
          </div>
          <div class="btn_cf_rq d_flex flex_center" style="display: flex;">
            <button class="btn_cancel back_w color_blue font_s15 line_h18 font_w500" onclick="openAndHide('','popup_cancel_export_note')">Hủy</button>
            <button class="btn_save back_blue color_white font_s15 line_h18 font_w500" type="button">Lưu</button>
            <!-- onclick="openAndHide('','add_new_export_note_success')" -->
          </div>
        </form>
      </div>
    </div>
    <?php include('../includes/popup_overview.php');  ?>
    <?php include('../includes/popup_h.php');  ?>
  </div>
</body>
<script type="text/javascript" src="../js/jquery-3.4.1.min.js"></script>
<script type="text/javascript" src="../js/jquery.validate.min.js"></script>
<script type="text/javascript" src="../js/reset_validate.js"></script>
<script type="text/javascript" src="../js/select2.min.js"></script>
<script type="text/javascript" src="../js/js_h.js"></script>
<script type="text/javascript" src="../js/validate_h.js"></script>
<script type="text/javascript" src="../js/js_k.js"></script>
<script type="text/javascript" src="../js/js_n.js"></script>
<script>
  var ctr_name_ct = <?= json_encode($list_phieu_vt1) ?>;
  var data = <?= json_encode($newArr) ?>;
  var pdc = <?= json_encode($arr_pdk) ?>;
  var pyc = <?= json_encode($emp1) ?>;
  var arr_dsvt_tb = <?= json_encode($arr_dsvt_tb) ?>; 
  var arr_kho3 = <?= json_encode($arr_kho3) ?>;
  var data_dh_2 = <?= json_encode($data_dh_2) ?>;
  var id_ct = <?= json_encode($id_cty) ?>;
  var tt_user = <?= json_encode($tt_user) ?>;
</script>

<script type="text/javascript" src="../js/xuat_kho_them_moi.js"></script>
<script>
  $('.active9').each(function() {
    if ($(this).hasClass('active9')) {
      $(this).find('a').addClass('active');
    }
  });
</script>

</html>