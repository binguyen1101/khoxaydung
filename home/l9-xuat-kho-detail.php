<?php
include("config1.php");

if (!in_array(1, $ro_xuat_kho)) {
  header("Location: /tong-quan.html");
}

$id_px = isset($_GET['id']) ? $id = $_GET['id'] : $id = "";

if (isset($_COOKIE['user']) && $_COOKIE['user'] != "" && $_COOKIE['role'] == 1) {
  $token = $_COOKIE['acc_token'];
  $curl = curl_init();
  $data = array();
  curl_setopt($curl, CURLOPT_POST, 1);
  curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
  curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/user_info_company.php');
  curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
  curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));

  $response = curl_exec($curl);
  curl_close($curl);
  $data_tt = json_decode($response, true);
  $tt_user = $data_tt['data']['user_info_result'];
  $user_id = $_SESSION['com_id'];
}

if (isset($_COOKIE['user']) && $_COOKIE['user'] != "" && $_COOKIE['role'] == 2) {
  $token = $_COOKIE['acc_token'];
  $curl = curl_init();
  $data = array();
  curl_setopt($curl, CURLOPT_POST, 1);
  curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
  curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/user_info_employee.php');
  curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
  curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));
  $response = curl_exec($curl);
  curl_close($curl);
  $data_tt = json_decode($response, true);
  $tt_user = $data_tt['data']['user_info_result'];
  $user_id = $_SESSION['ep_id'];
}
$id_cty = $tt_user['com_id'];
$date = date('Y-m-d', time());
$stt = 1;

$item = new db_query("SELECT `kcxl_id`, `kcxl_hinhThuc`, `kcxl_nguoiTao`, `kcxl_ngayTao`, `kcxl_donHang`, `kcxl_trangThai`, `kcxl_ngayHoanThanh`, 
  `kcxl_nhaCungCap`, `kcxl_nguoiDuyet`, `kcxl_ngayDuyet`, `kcxl_nguoiThucHien`, `kcxl_phongBanNguoiGiao`, `kcxl_nguoiNhan`, `kcxl_phongBanNguoiNhan`, 
  `kcxl_khoNhap`, `kcxl_ngayNhapKho`,  `kcxl_khoXuat`, `kcxl_ngayYeuCauHoanThanh`, `kcxl_congTrinh`, `kcxl_phieuYeuCau`, `kcxl_ngayXuatKho`, `kcxl_thoiGianHoanThanh`, 
  `kcxl_ngayThucHienDieuChuyen`,  `kcxl_nguoiTuChoi`, `kcxl_liDoTuChoi`, `kcxl_ghi_chu`, `kcxl_nguoiXoa`, `kcxl_ngayXoa`, `kcxl_check`, `kcxl_timeCurrentDay` , `kho_name`, `kcxl_nguoiHoanThanh`, `kcxl_phieuDieuChuyenKho`
  FROM `kho-cho-xu-li`
  LEFT JOIN `kho` ON `kcxl_khoXuat` = `kho_id`
   WHERE `kcxl_check` = '1' AND `kcxl_soPhieu` = 'PXK' AND `kcxl_id` = $id_px AND `kcxl_id_ct` = $id_cty");

$info_item = mysql_fetch_assoc($item->result);


$stt = 1;
if (isset($_SESSION['quyen']) && $_SESSION['quyen'] == 1) {
  $curl = curl_init();
  $token = $_COOKIE['acc_token'];
  curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/list_all_employee_of_company.php');
  curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
  curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));
  $response = curl_exec($curl);
  curl_close($curl);

  $data_list = json_decode($response, true);

  $data_list_nv = $data_list['data']['items'];
} elseif (isset($_SESSION['quyen']) && ($_SESSION['quyen'] == 2)) {
  $curl = curl_init();
  $token = $_COOKIE['acc_token'];
  curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/list_all_my_partner.php?get_all=true');
  curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
  curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));
  $response = curl_exec($curl);
  curl_close($curl);
  $data_list = json_decode($response, true);
  $data_list_nv = $data_list['data']['items'];
}
$count = count($data_list_nv);
$newArr = [];
$arr_pdk;
for ($i = 0; $i < count($data_list_nv); $i++) {
  $value = $data_list_nv[$i];
  $newArr[$value["ep_id"]] = $value;
}

$curl_vt_yc = curl_init();
curl_setopt($curl_vt_yc, CURLOPT_POST, 1);
curl_setopt($curl_vt_yc, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($curl_vt_yc, CURLOPT_URL, "https://phanmemquanlycungung.timviec365.vn/api/vat_tu_yc.php");
curl_setopt($curl_vt_yc, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
curl_setopt($curl_vt_yc, CURLOPT_POSTFIELDS, [
  'id_phieu' => $info_item['kcxl_phieuYeuCau'],
]);
$response_vt_yc = curl_exec($curl_vt_yc);
curl_close($curl_vt_yc);
$emp_json = json_decode($response_vt_yc, true);
$emp_arr = $emp_json['data']['items'];
$emp_vt_yc = [];
for ($i = 0; $i < count($emp_arr); $i++) {
  $vt_yc = $emp_arr[$i];
  $emp_vt_yc[$vt_yc["id_vat_tu"]] = $vt_yc;
}

$curl_ct = curl_init();
curl_setopt($curl_ct, CURLOPT_POST, 1);
curl_setopt($curl_ct, CURLOPT_URL, 'https://phanmemquanlycongtrinh.timviec365.vn/api/congtrinh.php');
curl_setopt($curl_ct, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($curl_ct, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
curl_setopt($curl_ct, CURLOPT_POSTFIELDS, [
  'id_com' => $id_cty,
]);
$response_ct = curl_exec($curl_ct);
curl_close($curl_ct);
$data_list_ct = json_decode($response_ct, true);
$list_ct = $data_list_ct['data']['items'];
$list_ct_arr = [];
for ($i = 0; $i < count($list_ct); $i++) {
  $ctr = $list_ct[$i];
  $list_ct_arr[$ctr["ctr_id"]] = $ctr;
}


// echo "<pre>";
// print_r($list_phieu_vt1);
// echo "</pre>";
// die();

$kho_nhap = new db_query("SELECT `kho_name`,`kcxl_ngayNhapKho` FROM `kho-cho-xu-li`
LEFT JOIN `kho` ON `kho-cho-xu-li`.`kcxl_khoNhap` = `kho`.`kho_id`
WHERE `kcxl_soPhieu` = 'PXK' AND `kcxl_id` = $id_px
");
$nhap = mysql_fetch_assoc($kho_nhap->result);

$kho_xuat = new db_query("SELECT `kho_name`,`kcxl_ngayXuatKho`
FROM `kho-cho-xu-li`
LEFT JOIN `kho` ON `kho-cho-xu-li`.`kcxl_khoXuat` = `kho`.`kho_id`
WHERE `kcxl_soPhieu` = 'PXK' AND `kcxl_id` = $id_px
");
$xuat = mysql_fetch_assoc($kho_xuat->result);

?>
<!DOCTYPE html>
<html lang="vi">

<head>
  <title>Xuất kho</title>
  <meta charset="UTF-8">
  <meta name="robots" content="noindex,nofollow" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" type="text/css" href="../css/style.css?v=<?= $ver ?>">
  <link rel="stylesheet" type="text/css" href="../css/style_h.css?v=<?= $ver ?>">
  <link rel="stylesheet" type="text/css" href="../css/style_n.css?v=<?= $ver ?>">
</head>

<body>
  <div class="main_wrapper_all">
    <div class="wapper_all">
      <?php include('../includes/sidebar.php');  ?>
    </div>
    <div class="main_overview" id="main_overview">
      <div class="header_menu_overview d_flex align_c space_b">
        <div class="text_link_header_back" style="display: block;">
          <div class=" d_flex align_c">
            <a href="/xuat-kho.html" class="cursor_p">
              <img src="../images/back_item_g.png" alt="">
            </a>
            <p class="color_grey line_16 font_s14 ml_10">Xuất kho / Chi tiết phiếu xuất kho</p>
          </div>
        </div>
        <img class="open_sidebar_w" src="../images/open_sidebar_w.png" alt="" onclick="toggle('main_sidebar')">
        <?php include('../includes/header.php');  ?>
      </div>
      <div class="body_equipment_supplies">
        <div class="text_link_body_back" style="display: none;">
          <div class=" d_flex align_c mb_15">
            <a href="/xuat-kho.html" class="cursor_p">
              <img src="../images/back_item_g.png" alt="">
            </a>
            <p class="color_grey line_16 font_s14 ml_10">Thông tin vật tư thiết bị / Danh sá ch vật tư thiết bị /
              Thêm mới</p>
          </div>
        </div>
        <div class="d_flex align_c space_b mb_30">
          <div>
          </div>
          <div class="d_flex align_c " id="block38">
            <?php if ($info_item['kcxl_trangThai'] == 1 && in_array(5, $ro_xuat_kho)) { ?>
              <div id="block37">
                <button class="button_refuse ml_15 d_flex align_c">
                  <img class="mr_10 img_button_refuse" src="../images/close_w.png" alt="">
                  <p class="color_white font_s15 line_h18 font_w500">Từ chối</p>
                </button>
                <!-- onclick="openAndHide('','refuse_export_note')" -->
              </div>
            <? } ?>
            <div class="d_flex align_c" id="block36">
              <div class="d_flex align_c" id="block39">
                <?php if (($info_item['kcxl_trangThai'] == 1 || $info_item['kcxl_trangThai'] == 2) && in_array(5, $ro_xuat_kho)) { ?>
                  <button class="button_browse  ml_15 d_flex align_c xxxxx">
                    <img class="mr_10 img_button_browse" src="../images/check_w.png" alt="">
                    <p class="color_white font_s15 line_h18 font_w500">Duyệt</p>
                  </button>
                <?php } ?>
                <?php if ($info_item['kcxl_trangThai'] == 5) { ?>
                  <button class="button_complete">
                    <div class=" ml_15 d_flex align_c">
                      <img class="mr_10 img_button_complete" src="../images/check_w.png" alt="">
                      <p class="color_white font_s15 line_h18 font_w500">Hoàn thành</p>
                    </div>
                  </button>
                  <!-- onclick="openAndHide('','complete_export_note')" -->
                <?php } ?>
                <?php if (in_array(4, $ro_xuat_kho)) { ?>
                  <button class="button_delete ml_15">
                    <p class="color_blue font_s15 line_h18 font_w500">Xóa
                      phiếu
                    </p>
                  </button>
                <?php } ?>
              </div>
              <div class="d_flex align_c" id="block40">
                <?php if (($info_item['kcxl_trangThai'] == 1 || $info_item['kcxl_trangThai'] == 2) && in_array(3, $ro_xuat_kho)) { ?>
                  <a href="/xuat-kho-chinh-sua-<?= $info_item['kcxl_id']; ?>.html">
                    <button class="d_flex align_c button_add_new cursor_p ml_15">
                      <img class="mr_10 img_button_add_new" src="../images/pen_edit.png" alt="">
                      <p class="color_white line_18 font_s15 font_w500">Chỉnh sửa</p>
                    </button>
                  </a>
                <?php } elseif (($info_item['kcxl_trangThai'] == 1 || $info_item['kcxl_trangThai'] == 2 || $info_item['kcxl_trangThai'] == 5 || $info_item['kcxl_trangThai'] == 6) && in_array(3, $ro_xuat_kho) && $_SESSION['quyen'] == '1') { ?>
                  <a href="/xuat-kho-chinh-sua-<?= $info_item['kcxl_id']; ?>.html">
                    <button class="d_flex align_c button_add_new cursor_p ml_15">
                      <img class="mr_10 img_button_add_new" src="../images/pen_edit.png" alt="">
                      <p class="color_white line_18 font_s15 font_w500">Chỉnh sửa</p>
                    </button>
                  </a>
                <?php } ?>
                <button class="d_flex align_c button_export_excel ml_15 cursor_p" data="<?= $id_px ?>">
                  <img class="mr_10 img_button_export_excel" src="../images/export_excel.png" alt="">
                  <p class="color_white line_18 font_s15 font_w500">Xuất excel</p>
                </button>
              </div>
            </div>
          </div>
        </div>
        <div class="body_equipment_supplies_create">
          <div class="header_body_b color_white line_h19 font_s16 font_wB">
            Thông tin phiếu xuất kho
          </div>
          <div>
            <div class="d_flex srcoll_body_equipment_supplies_detail">
              <div class="export_kho_detail">
                <?php if ($info_item['kcxl_hinhThuc'] == "XK1") { ?>
                  <div class="table_selected_construction_export srcoll_equipment_supplies_detail ">
                    <div class="d_flex align_c">
                      <p class="title_detail font_s14 line_16 color_grey">Số phiếu:</p>
                      <p class="description_detail font_s14 line_16 color_grey font_w500">PXK - <?= $info_item['kcxl_id'] ?></p>
                    </div>
                    <div class="d_flex align_c boder_detail_top mt_17">
                      <p class="title_detail font_s14 line_16 color_grey">Hình thức xuất kho:</p>
                      <p class="description_detail font_s14 line_16 color_grey font_w500">Xuất thi
                        công</p>
                    </div>
                    <div class="d_flex align_c boder_detail_top mt_17">
                      <?php
                      $nguoi_tao = $info_item['kcxl_nguoiTao'];
                      $user_tao = $newArr[$nguoi_tao];
                      if ($nguoi_tao != 0) {
                        $ten_nguoi_tao = $user_tao['ep_name'];
                        $anh_nguoi_tao = $user_tao['ep_image'];
                        if ($anh_nguoi_tao == "") {
                          $anh0 = '../images/ava_ad.png';
                        } else {
                          $anh0 = 'https://chamcong.24hpay.vn/upload/employee/' . $anh_nguoi_tao;
                        }
                      } else {
                        $ten_nguoi_tao = $tt_user['com_name'];
                        $anh0 = 'https://chamcong.24hpay.vn/upload/company/logo/' . $tt_user['com_logo'];
                      }
                      ?>
                      <p class="title_detail font_s14 line_16 color_grey">Người tạo:</p>
                      <div class="d_flex flex_center align_c">
                        <img class="ava_human_table" src="<?= $anh0 ?>" alt="">
                        <p class=" font_s14 line_16 color_grey font_w500"><?= $ten_nguoi_tao ?></p>
                      </div>
                    </div>
                    <div class="d_flex align_c boder_detail_top mt_17">
                      <p class="title_detail font_s14 line_16 color_grey">Ngày tạo:</p>
                      <p class="description_detail font_s14 line_16 color_grey font_w500"><?= $info_item['kcxl_ngayTao'] ?></p>
                    </div>
                    <div class="d_flex align_c boder_detail_top mt_17">
                      <p class="title_detail font_s14 line_16 color_grey">Phiếu yêu cầu:</p>
                      <p class="description_detail font_s14 line_16 color_grey font_w500">PYC - <?= $info_item['kcxl_phieuYeuCau'] ?>
                      </p>
                    </div>
                    <div class="d_flex align_c boder_detail_top mt_17">
                      <p class="title_detail font_s14 line_16 color_grey">Trạng thái:</p>
                      <p class="description_detail pending font_s14 line_16 color_grey font_w500 display_none" <?php if ($info_item['kcxl_trangThai'] == 1) { ?>style="display: block;" <?php } ?>>Chờ duyệt</p>
                      <p class="description_detail approved_export font_s14 line_16 color_green_tb font_w500 display_none" <?php if ($info_item['kcxl_trangThai'] == 6) { ?>style="display: block;" <?php } ?>>Hoàn thành</p>
                      <p class="description_detail refuse font_s14 line_16 color_org font_w500 display_none" <?php if ($info_item['kcxl_trangThai'] == 2) { ?>style="display: block;" <?php } ?>>Từ
                        chối</p>
                      <p class="description_detail approved_wait_export font_s14 line_16 color_blue font_w500 display_none" <?php if ($info_item['kcxl_trangThai'] == 5) { ?>style="display: block;" <?php } ?>>Đã duyệt - Chờ xuất kho</p>
                    </div>
                    <div class="d_flex align_c boder_detail_top mt_17 reason_for_refusal display_none" <?php if ($info_item['kcxl_trangThai'] == 2) { ?>style="display: flex;" <?php } ?>>
                      <p class="title_detail font_s14 line_16 color_grey">Lý do từ chối:</p>
                      <p class="description_detail font_s14 line_16 color_grey font_w500"><?= $info_item['kcxl_liDoTuChoi'] ?>
                      </p>
                    </div>
                    <div class="d_flex align_c boder_detail_top mt_17">
                      <p class="title_detail font_s14 line_16 color_grey">Ngày yêu cầu hoàn thành:</p>
                      <p class="description_detail font_s14 line_16 color_grey font_w500"><?= $info_item['kcxl_ngayYeuCauHoanThanh'] ?></p>
                    </div>
                    <div class="d_flex align_c boder_detail_top mt_17">
                      <p class="title_detail font_s14 line_16 color_grey">Công trình:</p>
                      <p class="description_detail font_s14 line_16 color_grey font_w500"><?= $list_ct_arr[trim($info_item['kcxl_congTrinh'])]['ctr_name'] ?></p>
                    </div>
                    <div class="d_flex align_c boder_detail_top mt_17">
                      <p class="title_detail font_s14 line_16 color_grey">Người giao hàng:</p>
                      <p class="description_detail font_s14 line_16 color_grey font_w500"><?= $newArr[$info_item['kcxl_nguoiThucHien']]['ep_name'] ?>
                      </p>
                    </div>
                    <div class="d_flex align_c boder_detail_top mt_17">
                      <p class="title_detail font_s14 line_16 color_grey">Phòng ban:</p>
                      <p class="description_detail font_s14 line_16 color_grey font_w500"><?= $newArr[$info_item['kcxl_nguoiThucHien']]['dep_name'] ?>
                      </p>
                    </div>
                    <div class="d_flex align_c boder_detail_top mt_17">
                      <?php
                      $nguoi_nhan = $info_item['kcxl_nguoiNhan'];
                      $user_nhan = $newArr[$nguoi_nhan];
                      if ($nguoi_nhan != 0) {
                        $ten_nguoi_nhan = $user_nhan['ep_name'];
                        $anh_nguoi_nhan = $user_nhan['ep_image'];
                        if ($anh_nguoi_nhan == "") {
                          $anh1 = '../images/ava_ad.png';
                        } else {
                          $anh1 = 'https://chamcong.24hpay.vn/upload/employee/' . $anh_nguoi_nhan;
                        }
                      } else {
                        $ten_nguoi_nhan = $tt_user['com_name'];
                        $anh1 = 'https://chamcong.24hpay.vn/upload/company/logo/' . $tt_user['com_logo'];
                      }
                      ?>
                      <p class="title_detail font_s14 line_16 color_grey">Người nhận:</p>
                      <div class="d_flex flex_center align_c">
                        <img class="ava_human_table" src="<?= $anh1 ?>" alt="">
                        <p class=" font_s14 line_16 color_grey font_w500"><?= $ten_nguoi_nhan ?></p>
                      </div>
                    </div>
                    <div class="d_flex align_c boder_detail_top mt_17">
                      <p class="title_detail font_s14 line_16 color_grey">Phòng ban:</p>
                      <p class="description_detail font_s14 line_16 color_grey font_w500"><?= $newArr[$info_item['kcxl_nguoiNhan']]['dep_name'] ?>
                      </p>
                    </div>
                    <div class="d_flex align_c boder_detail_top mt_17">
                      <p class="title_detail font_s14 line_16 color_grey">Kho xuất:</p>
                      <p class="description_detail font_s14 line_16 color_grey font_w500"><?= $info_item['kho_name'] ?>
                      </p>
                    </div>
                    <div class="d_flex align_c boder_detail_top mt_17">
                      <p class="title_detail font_s14 line_16 color_grey">Ngày xuất kho:</p>
                      <p class="description_detail font_s14 line_16 color_grey font_w500"><?= $info_item['kcxl_ngayXuatKho'] ?>
                      </p>
                    </div>
                    <div class="d_flex boder_detail_top mt_17">
                      <p class="title_detail font_s14 line_16 color_grey">Ghi chú:</p>
                      <p class="description_detail font_s14 line_16 color_grey font_w500"><?= $info_item['kcxl_ghi_chu'] ?></p>
                    </div>
                    <div class="d_flex align_c boder_detail_top mt_17 human_approved display_none" <?php if ($info_item['kcxl_trangThai'] == 2 || $info_item['kcxl_trangThai'] == 5 || $info_item['kcxl_trangThai'] == 6) { ?>style="display: flex;" <?php } ?>>
                      <?php
                      $nguoi_duyet = $info_item['kcxl_nguoiDuyet'];
                      $user_duyet = $newArr[$nguoi_duyet];
                      if ($nguoi_duyet != 0) {
                        $ten_nguoi_duyet = $user_duyet['ep_name'];
                        $anh_nguoi_duyet = $user_duyet['ep_image'];
                        if ($anh_nguoi_duyet == "") {
                          $anh3 = '../images/ava_ad.png';
                        } else {
                          $anh3 = 'https://chamcong.24hpay.vn/upload/employee/' . $anh_nguoi_duyet;
                        }
                      } else {
                        $ten_nguoi_duyet = $tt_user['com_name'];
                        $anh3 = 'https://chamcong.24hpay.vn/upload/company/logo/' . $tt_user['com_logo'];
                      }
                      ?>
                      <p class="title_detail font_s14 line_16 color_grey">Người duyệt:</p>
                      <div class="d_flex flex_center align_c">
                        <img class="ava_human_table" src="<?= $anh3 ?>" alt="">
                        <p class=" font_s14 line_16 color_grey font_w500"><?= $ten_nguoi_duyet ?></p>
                      </div>
                    </div>
                    <div class="d_flex align_c boder_detail_top mt_17 approval_date display_none" <?php if ($info_item['kcxl_trangThai'] == 6 || $info_item['kcxl_trangThai'] == 2 || $info_item['kcxl_trangThai'] == 5) { ?>style="display: flex;" <?php } ?>>
                      <p class="title_detail font_s14 line_16 color_grey">Ngày duyệt:</p>
                      <p class="description_detail font_s14 line_16 color_grey font_w500">
                        <?= $info_item['kcxl_ngayDuyet'] ?>
                      </p>
                    </div>
                    <div class="d_flex align_c boder_detail_top mt_17 human_complete display_none" <?php if ($info_item['kcxl_trangThai'] == 6) { ?>style="display: flex;" <?php } ?>>
                      <?php
                      $anh_nguoi_kiemke =  $newArr[$info_item['kcxl_nguoiHoanThanh']]['ep_image'];
                      if ($anh_nguoi_kiemke == "") {
                        $anh = '../images/ava_ad.png';
                      } else {
                        $anh = 'https://chamcong.24hpay.vn/upload/employee/' . $newArr[$info_item['kcxl_nguoiHoanThanh']]['ep_image'];
                      }
                      ?>
                      <?php
                      $nguoi_ht = $info_item['kcxl_nguoiHoanThanh'];
                      $user_ht = $newArr[$nguoi_ht];
                      if ($nguoi_ht != 0) {
                        $ten_nguoi_ht = $user_ht['ep_name'];
                        $anh_nguoi_ht = $user_ht['ep_image'];
                        if ($anh_nguoi_ht == "") {
                          $anh4 = '../images/ava_ad.png';
                        } else {
                          $anh4 = 'https://chamcong.24hpay.vn/upload/employee/' . $anh_nguoi_ht;
                        }
                      } else {
                        $ten_nguoi_ht = $tt_user['com_name'];
                        $anh4 = 'https://chamcong.24hpay.vn/upload/company/logo/' . $tt_user['com_logo'];
                      }
                      ?>
                      <p class="title_detail font_s14 line_16 color_grey">Người hoàn thành:</p>
                      <div class="d_flex flex_center align_c">
                        <img class="ava_human_table" src="<?= $anh4 ?>" alt="">
                        <p class=" font_s14 line_16 color_grey font_w500"><?= $ten_nguoi_ht ?></p>
                      </div>
                    </div>
                    <div class="d_flex align_c boder_detail_top mt_17 data_complete display_none" <?php if ($info_item['kcxl_trangThai'] == 6) { ?>style="display: flex;" <?php } ?>>
                      <p class="title_detail font_s14 line_16 color_grey">Ngày hoàn thành:</p>
                      <p class="description_detail font_s14 line_16 color_grey font_w500"><?= $info_item['kcxl_ngayHoanThanh'] ?>
                      </p>
                    </div>
                  </div>
                <? } ?>

                <?php if ($info_item['kcxl_hinhThuc'] == "XK2") { ?>
                  <div class="table_selected_export_transfer srcoll_equipment_supplies_detail ">
                    <div class="d_flex align_c">
                      <p class="title_detail_export_transfer font_s14 line_16 color_grey">Số phiếu:</p>
                      <p class="description_detail font_s14 line_16 color_grey font_w500">PXK - <?= $info_item['kcxl_id'] ?></p>
                    </div>
                    <div class="d_flex align_c boder_detail_top mt_17">
                      <p class="title_detail_export_transfer font_s14 line_16 color_grey">Hình thức xuất kho:</p>
                      <p class="description_detail font_s14 line_16 color_grey font_w500">Xuất điều chuyển</p>
                    </div>
                    <div class="d_flex align_c boder_detail_top mt_17">
                      <?php
                      $nguoi_tao = $info_item['kcxl_nguoiTao'];
                      $user_tao = $newArr[$nguoi_tao];
                      if ($nguoi_tao != 0) {
                        $ten_nguoi_tao = $user_tao['ep_name'];
                        $anh_nguoi_tao = $user_tao['ep_image'];
                        if ($anh_nguoi_tao == "") {
                          $anh0 = '../images/ava_ad.png';
                        } else {
                          $anh0 = 'https://chamcong.24hpay.vn/upload/employee/' . $anh_nguoi_tao;
                        }
                      } else {
                        $ten_nguoi_tao = $tt_user['com_name'];
                        $anh0 = 'https://chamcong.24hpay.vn/upload/company/logo/' . $tt_user['com_logo'];
                      }
                      ?>
                      <p class="title_detail_export_transfer font_s14 line_16 color_grey" style="width: 270px">Người tạo:</p>
                      <div class="d_flex flex_center align_c">

                        <img class="ava_human_table" src="<?= $anh0 ?>" alt="">
                        <p class=" font_s14 line_16 color_grey font_w500"><?= $ten_nguoi_tao ?></p>
                      </div>
                    </div>
                    <div class="d_flex align_c boder_detail_top mt_17">
                      <p class="title_detail_export_transfer font_s14 line_16 color_grey">Ngày tạo:</p>
                      <p class="description_detail font_s14 line_16 color_grey font_w500"><?= $info_item['kcxl_ngayTao'] ?></p>
                    </div>
                    <div class="d_flex align_c boder_detail_top mt_17">
                      <p class="title_detail_export_transfer font_s14 line_16 color_grey">Phiếu điều chuyển:</p>
                      <p class="description_detail font_s14 line_16 color_grey font_w500">ĐCK - <?= $info_item['kcxl_phieuDieuChuyenKho'] ?></p>
                    </div>
                    <div class="d_flex align_c boder_detail_top mt_17">
                      <p class="title_detail_export_transfer1 font_s14 line_16 color_grey">Trạng thái:</p>
                      <p class="pending font_s14 line_16 color_grey font_w500 display_none" <?php if ($info_item['kcxl_trangThai'] == 1) { ?>style="display: block;" <?php } ?>>Chờ duyệt</p>
                      <p class="approved_export font_s14 line_16 color_green_tb font_w500 display_none" <?php if ($info_item['kcxl_trangThai'] == 6) { ?>style="display: block;" <?php } ?>>Hoàn thành</p>
                      <p class="refuse font_s14 line_16 color_org font_w500 display_none" <?php if ($info_item['kcxl_trangThai'] == 2) { ?>style="display: block;" <?php } ?>>Từ
                        chối</p>
                      <p class="approved_wait_export font_s14 line_16 color_blue font_w500 display_none" <?php if ($info_item['kcxl_trangThai'] == 5) { ?>style="display: block;" <?php } ?>>Đã duyệt - Chờ xuất kho</p>
                    </div>
                    <div class="d_flex align_c boder_detail_top mt_17 reason_for_refusal display_none" <?php if ($info_item['kcxl_trangThai'] == 2) { ?>style="display: flex;" <?php } ?>>
                      <p class="title_detail_export_transfer font_s14 line_16 color_grey">Lý do từ chối:</p>
                      <p class="description_detail font_s14 line_16 color_grey font_w500"><?= $info_item['kcxl_liDoTuChoi'] ?>
                      </p>
                    </div>
                    <div class="d_flex align_c boder_detail_top mt_17">
                      <p class="title_detail_export_transfer font_s14 line_16 color_grey">Kho xuất:</p>
                      <p class="description_detail font_s14 line_16 color_grey font_w500"><?= $xuat['kho_name'] ?></p>
                    </div>
                    <div class="d_flex align_c boder_detail_top mt_17">
                      <p class="title_detail_export_transfer font_s14 line_16 color_grey">Ngày xuất:</p>
                      <p class="description_detail font_s14 line_16 color_grey font_w500"><?= $info_item['kcxl_ngayXuatKho'] ?></p>
                    </div>
                    <div class="d_flex align_c boder_detail_top mt_17">
                      <p class="title_detail_export_transfer font_s14 line_16 color_grey">Kho nhập:</p>
                      <p class="description_detail font_s14 line_16 color_grey font_w500"><?= $nhap['kho_name'] ?></p>
                    </div>
                    <div class="d_flex align_c boder_detail_top mt_17">
                      <p class="title_detail_export_transfer font_s14 line_16 color_grey">Ngày yêu cầu hoàn thành điều
                        chuyển:</p>
                      <p class="description_detail font_s14 line_16 color_grey font_w500"><?= $info_item['kcxl_ngayYeuCauHoanThanh'] ?></p>
                    </div>
                    <div class="d_flex align_c boder_detail_top mt_17">
                      <p class="title_detail_export_transfer font_s14 line_16 color_grey">Người giao hàng:</p>
                      <p class="description_detail font_s14 line_16 color_grey font_w500"><?= $newArr[$info_item['kcxl_nguoiThucHien']]['ep_name'] ?></p>
                    </div>
                    <div class="d_flex align_c boder_detail_top mt_17">
                      <p class="title_detail_export_transfer font_s14 line_16 color_grey">Phòng ban:</p>
                      <p class="description_detail font_s14 line_16 color_grey font_w500"><?= $newArr[$info_item['kcxl_nguoiThucHien']]['dep_name'] ?></p>
                    </div>
                    <div class="d_flex align_c boder_detail_top mt_17">
                      <?php
                      $nguoi_nhan = $info_item['kcxl_nguoiNhan'];
                      $user_nhan = $newArr[$nguoi_nhan];
                      if ($nguoi_nhan != 0) {
                        $ten_nguoi_nhan = $user_nhan['ep_name'];
                        $anh_nguoi_nhan = $user_nhan['ep_image'];
                        if ($anh_nguoi_nhan == "") {
                          $anh1 = '../images/ava_ad.png';
                        } else {
                          $anh1 = 'https://chamcong.24hpay.vn/upload/employee/' . $anh_nguoi_nhan;
                        }
                      } else {
                        $ten_nguoi_nhan = $tt_user['com_name'];
                        $anh1 = 'https://chamcong.24hpay.vn/upload/company/logo/' . $tt_user['com_logo'];
                      }
                      ?>
                      <p class="title_detail_export_transfer font_s14 line_16 color_grey" style="width: 270px">Người nhận:</p>
                      <div class="d_flex flex_center align_c">
                        <img class="ava_human_table" src="<?= $anh1 ?>" alt="">
                        <p class=" font_s14 line_16 color_grey font_w500"><?= $ten_nguoi_nhan ?></p>
                      </div>
                    </div>
                    <div class="d_flex align_c boder_detail_top mt_17">
                      <p class="title_detail_export_transfer font_s14 line_16 color_grey">Phòng ban:</p>
                      <p class="description_detail font_s14 line_16 color_grey font_w500"><?= $newArr[$info_item['kcxl_nguoiNhan']]['dep_name'] ?></p>
                    </div>
                    <div class="d_flex align_c boder_detail_top mt_17">
                      <p class="title_detail_export_transfer font_s14 line_16 color_grey">Ghi chú:</p>
                      <p class="description_detail font_s14 line_16 color_grey font_w500"><?= $info_item['kcxl_ghi_chu'] ?></p>
                    </div>
                    <div class="d_flex align_c boder_detail_top mt_17 human_approved display_none" <?php if ($info_item['kcxl_trangThai'] == 2 || $info_item['kcxl_trangThai'] == 5 || $info_item['kcxl_trangThai'] == 6) { ?>style="display: flex;" <?php } ?>>
                      <?php
                      $nguoi_duyet = $info_item['kcxl_nguoiDuyet'];
                      $user_duyet = $newArr[$nguoi_duyet];
                      if ($nguoi_duyet != 0) {
                        $ten_nguoi_duyet = $user_duyet['ep_name'];
                        $anh_nguoi_duyet = $user_duyet['ep_image'];
                        if ($anh_nguoi_duyet == "") {
                          $anh3 = '../images/ava_ad.png';
                        } else {
                          $anh3 = 'https://chamcong.24hpay.vn/upload/employee/' . $anh_nguoi_duyet;
                        }
                      } else {
                        $ten_nguoi_duyet = $tt_user['com_name'];
                        $anh3 = 'https://chamcong.24hpay.vn/upload/company/logo/' . $tt_user['com_logo'];
                      }
                      ?>
                      <p class="title_detail_export_transfer font_s14 line_16 color_grey" style="width: 270px">Người duyệt:</p>
                      <div class="d_flex flex_center align_c">
                        <img class="ava_human_table" src="<?= $anh3 ?>" alt="">
                        <p class=" font_s14 line_16 color_grey font_w500"><?= $ten_nguoi_duyet ?></p>
                      </div>
                    </div>
                    <div class="d_flex align_c boder_detail_top mt_17 approval_date display_none" <?php if ($info_item['kcxl_trangThai'] == 6 || $info_item['kcxl_trangThai'] == 2 || $info_item['kcxl_trangThai'] == 5) { ?>style="display: flex;" <?php } ?>>
                      <p class="title_detail_export_transfer font_s14 line_16 color_grey">Ngày duyệt:</p>
                      <p class="description_detail font_s14 line_16 color_grey font_w500"> <?= $info_item['kcxl_ngayDuyet'] ?>
                      </p>
                    </div>
                    <div class="d_flex align_c boder_detail_top mt_17 human_complete display_none" <?php if ($info_item['kcxl_trangThai'] == 6) { ?>style="display: flex;" <?php } ?>>
                      <?php
                      $nguoi_ht = $info_item['kcxl_nguoiHoanThanh'];
                      $user_ht = $newArr[$nguoi_ht];
                      if ($nguoi_ht != 0) {
                        $ten_nguoi_ht = $user_ht['ep_name'];
                        $anh_nguoi_ht = $user_ht['ep_image'];
                        if ($anh_nguoi_ht == "") {
                          $anh4 = '../images/ava_ad.png';
                        } else {
                          $anh4 = 'https://chamcong.24hpay.vn/upload/employee/' . $anh_nguoi_ht;
                        }
                      } else {
                        $ten_nguoi_ht = $tt_user['com_name'];
                        $anh4 = 'https://chamcong.24hpay.vn/upload/company/logo/' . $tt_user['com_logo'];
                      }
                      ?>
                      <p class="title_detail font_s14 line_16 color_grey" style="width: 270px">Người hoàn thành:</p>
                      <div class="d_flex flex_center align_c">
                        <img class="ava_human_table" src="<?= $anh4 ?>" alt="">
                        <p class=" font_s14 line_16 color_grey font_w500"><?= $ten_nguoi_ht ?></p>
                      </div>
                    </div>
                    <div class="d_flex align_c boder_detail_top mt_17 data_complete display_none" <?php if ($info_item['kcxl_trangThai'] == 6) { ?>style="display: flex;" <?php } ?>>
                      <p class="title_detail_export_transfer font_s14 line_16 color_grey">Ngày hoàn thành:</p>
                      <p class="description_detail font_s14 line_16 color_grey font_w500"><?= $info_item['kcxl_ngayHoanThanh'] ?>
                      </p>
                    </div>
                  </div>
                <? } ?>

                <?php if ($info_item['kcxl_hinhThuc'] == "XK3") { ?>
                  <div class="table_selected_export_on_demand srcoll_equipment_supplies_detail ">
                    <div class="d_flex align_c">
                      <p class="title_detail font_s14 line_16 color_grey">Số phiếu:</p>
                      <p class="description_detail font_s14 line_16 color_grey font_w500">PXK - <?= $info_item['kcxl_id'] ?></p>
                    </div>
                    <div class="d_flex align_c boder_detail_top mt_17">
                      <p class="title_detail font_s14 line_16 color_grey">Hình thức xuất kho:</p>
                      <p class="description_detail font_s14 line_16 color_grey font_w500">Xuất theo yêu cầu vật tư</p>
                    </div>
                    <div class="d_flex align_c boder_detail_top mt_17">
                      <?php
                      $nguoi_tao = $info_item['kcxl_nguoiTao'];
                      $user_tao = $newArr[$nguoi_tao];
                      if ($nguoi_tao != 0) {
                        $ten_nguoi_tao = $user_tao['ep_name'];
                        $anh_nguoi_tao = $user_tao['ep_image'];
                        if ($anh_nguoi_tao == "") {
                          $anh0 = '../images/ava_ad.png';
                        } else {
                          $anh0 = 'https://chamcong.24hpay.vn/upload/employee/' . $anh_nguoi_tao;
                        }
                      } else {
                        $ten_nguoi_tao = $tt_user['com_name'];
                        $anh0 = 'https://chamcong.24hpay.vn/upload/company/logo/' . $tt_user['com_logo'];
                      }
                      ?>
                      <p class="title_detail font_s14 line_16 color_grey">Người tạo:</p>
                      <div class="d_flex flex_center align_c">
                        <img class="ava_human_table" src="<?= $anh0 ?>" alt="">
                        <p class=" font_s14 line_16 color_grey font_w500"><?= $ten_nguoi_tao ?></p>
                      </div>
                    </div>
                    <div class="d_flex align_c boder_detail_top mt_17">
                      <p class="title_detail font_s14 line_16 color_grey">Ngày tạo:</p>
                      <p class="description_detail font_s14 line_16 color_grey font_w500"><?= $info_item['kcxl_ngayTao'] ?></p>
                    </div>
                    <div class="d_flex align_c boder_detail_top mt_17">
                      <p class="title_detail font_s14 line_16 color_grey">Phiếu yêu cầu:</p>
                      <p class="description_detail font_s14 line_16 color_grey font_w500">PYC - <?= $info_item['kcxl_phieuYeuCau'] ?>
                      </p>
                    </div>
                    <div class="d_flex align_c boder_detail_top mt_17">
                      <p class="title_detail font_s14 line_16 color_grey">Trạng thái:</p>
                      <p class="description_detail pending font_s14 line_16 color_grey font_w500 display_none" <?php if ($info_item['kcxl_trangThai'] == 1) { ?>style="display: block;" <?php } ?>>Chờ duyệt</p>
                      <p class="description_detail approved_export font_s14 line_16 color_green_tb font_w500 display_none" <?php if ($info_item['kcxl_trangThai'] == 6) { ?>style="display: block;" <?php } ?>>Hoàn thành</p>
                      <p class="description_detail refuse font_s14 line_16 color_org font_w500 display_none" <?php if ($info_item['kcxl_trangThai'] == 2) { ?>style="display: block;" <?php } ?>>Từ
                        chối</p>
                      <p class="description_detail approved_wait_export font_s14 line_16 color_blue font_w500 display_none" <?php if ($info_item['kcxl_trangThai'] == 5) { ?>style="display: block;" <?php } ?>>Đã duyệt - Chờ xuất kho</p>
                    </div>
                    <div class="d_flex align_c boder_detail_top mt_17 reason_for_refusal display_none" <?php if ($info_item['kcxl_trangThai'] == 2) { ?>style="display: flex;" <?php } ?>>
                      <p class="title_detail font_s14 line_16 color_grey">Lý do từ chối:</p>
                      <p class="description_detail font_s14 line_16 color_grey font_w500"><?= $info_item['kcxl_liDoTuChoi'] ?>
                      </p>
                    </div>
                    <div class="d_flex align_c boder_detail_top mt_17">
                      <p class="title_detail font_s14 line_16 color_grey">Kho xuất:</p>
                      <p class="description_detail font_s14 line_16 color_grey font_w500"><?= $xuat['kho_name'] ?></p>
                    </div>
                    <div class="d_flex align_c boder_detail_top mt_17">
                      <p class="title_detail font_s14 line_16 color_grey">Ngày xuất:</p>
                      <p class="description_detail font_s14 line_16 color_grey font_w500"><?= $info_item['kcxl_ngayXuatKho'] ?></p>
                    </div>
                    <div class="d_flex align_c boder_detail_top mt_17">
                      <p class="title_detail font_s14 line_16 color_grey">Ngày yêu cầu hoàn thành:</p>
                      <p class="description_detail font_s14 line_16 color_grey font_w500"><?= $info_item['kcxl_ngayYeuCauHoanThanh'] ?>
                      </p>
                    </div>
                    <div class="d_flex align_c boder_detail_top mt_17">
                      <p class="title_detail font_s14 line_16 color_grey">Người giao hàng:</p>
                      <p class="description_detail font_s14 line_16 color_grey font_w500"><?= $newArr[$info_item['kcxl_nguoiThucHien']]['ep_name'] ?>
                      </p>
                    </div>
                    <div class="d_flex align_c boder_detail_top mt_17">
                      <?php
                      $nguoi_nhan = $info_item['kcxl_nguoiNhan'];
                      $user_nhan = $newArr[$nguoi_nhan];
                      if ($nguoi_nhan != 0) {
                        $ten_nguoi_nhan = $user_nhan['ep_name'];
                        $anh_nguoi_nhan = $user_nhan['ep_image'];
                        if ($anh_nguoi_nhan == "") {
                          $anh1 = '../images/ava_ad.png';
                        } else {
                          $anh1 = 'https://chamcong.24hpay.vn/upload/employee/' . $anh_nguoi_nhan;
                        }
                      } else {
                        $ten_nguoi_nhan = $tt_user['com_name'];
                        $anh1 = 'https://chamcong.24hpay.vn/upload/company/logo/' . $tt_user['com_logo'];
                      }
                      ?>
                      <p class="title_detail font_s14 line_16 color_grey">Người nhận:</p>
                      <div class="d_flex flex_center align_c">
                        <img class="ava_human_table" src="<?= $anh1 ?>" alt="">
                        <p class=" font_s14 line_16 color_grey font_w500"><?= $ten_nguoi_nhan ?></p>
                      </div>
                    </div>
                    <div class="d_flex align_c boder_detail_top mt_17">
                      <p class="title_detail font_s14 line_16 color_grey">Phòng ban:</p>
                      <p class="description_detail font_s14 line_16 color_grey font_w500"><?= $newArr[$info_item['kcxl_nguoiNhan']]['dep_name'] ?>
                      </p>
                    </div>
                    <div class="d_flex boder_detail_top mt_17">
                      <p class="title_detail font_s14 line_16 color_grey">Ghi chú:</p>
                      <p class="description_detail font_s14 line_16 color_grey font_w500"><?= $info_item['kcxl_id'] ?></p>
                    </div>
                    <div class="d_flex align_c boder_detail_top mt_17 human_approved display_none" <?php if ($info_item['kcxl_trangThai'] == 2 || $info_item['kcxl_trangThai'] == 5 || $info_item['kcxl_trangThai'] == 6) { ?>style="display: flex;" <?php } ?>>
                      <?php
                      $nguoi_duyet = $info_item['kcxl_nguoiDuyet'];
                      $user_duyet = $newArr[$nguoi_duyet];
                      if ($nguoi_duyet != 0) {
                        $ten_nguoi_duyet = $user_duyet['ep_name'];
                        $anh_nguoi_duyet = $user_duyet['ep_image'];
                        if ($anh_nguoi_duyet == "") {
                          $anh3 = '../images/ava_ad.png';
                        } else {
                          $anh3 = 'https://chamcong.24hpay.vn/upload/employee/' . $anh_nguoi_duyet;
                        }
                      } else {
                        $ten_nguoi_duyet = $tt_user['com_name'];
                        $anh3 = 'https://chamcong.24hpay.vn/upload/company/logo/' . $tt_user['com_logo'];
                      }
                      ?>
                      <p class="title_detail font_s14 line_16 color_grey">Người duyệt:</p>
                      <div class="d_flex flex_center align_c">
                        <img class="ava_human_table" src="<?= $anh3 ?>" alt="">
                        <p class=" font_s14 line_16 color_grey font_w500"><?= $ten_nguoi_duyet ?></p>
                      </div>
                    </div>
                    <div class="d_flex align_c boder_detail_top mt_17 approval_date display_none" <?php if ($info_item['kcxl_trangThai'] == 2 || $info_item['kcxl_trangThai'] == 5 || $info_item['kcxl_trangThai'] == 6) { ?>style="display: flex;" <?php } ?>>
                      <p class="title_detail font_s14 line_16 color_grey">Ngày duyệt:</p>
                      <p class="description_detail font_s14 line_16 color_grey font_w500"> <?= $info_item['kcxl_ngayDuyet'] ?>
                      </p>
                    </div>
                    <div class="d_flex align_c boder_detail_top mt_17 human_complete display_none" <?php if ($info_item['kcxl_trangThai'] == 6) { ?>style="display: flex;" <?php } ?>>
                    <?php
                      $nguoi_ht = $info_item['kcxl_nguoiHoanThanh'];
                      $user_ht = $newArr[$nguoi_ht];
                      if ($nguoi_ht != 0) {
                        $ten_nguoi_ht = $user_ht['ep_name'];
                        $anh_nguoi_ht = $user_ht['ep_image'];
                        if ($anh_nguoi_ht == "") {
                          $anh4 = '../images/ava_ad.png';
                        } else {
                          $anh4 = 'https://chamcong.24hpay.vn/upload/employee/' . $anh_nguoi_ht;
                        }
                      } else {
                        $ten_nguoi_ht = $tt_user['com_name'];
                        $anh4 = 'https://chamcong.24hpay.vn/upload/company/logo/' . $tt_user['com_logo'];
                      }
                      ?>
                      <p class="title_detail font_s14 line_16 color_grey">Người hoàn thành:</p>
                      <div class="d_flex flex_center align_c">
                        <img class="ava_human_table" src="<?= $anh4 ?>" alt="">
                        <p class=" font_s14 line_16 color_grey font_w500"><?= $ten_nguoi_ht ?></p>
                      </div>
                    </div>
                    <div class="d_flex align_c boder_detail_top mt_17 data_complete display_none" <?php if ($info_item['kcxl_trangThai'] == 6) { ?>style="display: flex;" <?php } ?>>
                      <p class="title_detail font_s14 line_16 color_grey">Ngày hoàn thành:</p>
                      <p class="description_detail font_s14 line_16 color_grey font_w500"><?= $info_item['kcxl_ngayHoanThanh'] ?>
                      </p>
                    </div>
                  </div>
                <? } ?>

                <?php if ($info_item['kcxl_hinhThuc'] == "XK4") { ?>
                  <div class="table_selected_export_by_order srcoll_equipment_supplies_detail display_none">
                    <div class="d_flex align_c">
                      <p class="title_detail font_s14 line_16 color_grey">Số phiếu:</p>
                      <p class="description_detail font_s14 line_16 color_grey font_w500">PXK - <?= $info_item['kcxl_id'] ?></p>
                    </div>
                    <div class="d_flex align_c boder_detail_top mt_17">
                      <p class="title_detail font_s14 line_16 color_grey">Hình thức xuất kho:</p>
                      <p class="description_detail font_s14 line_16 color_grey font_w500">Xuất theo đơn hàng bán vật tư</p>
                    </div>
                    <div class="d_flex align_c boder_detail_top mt_17">
                      <?php
                      $nguoi_tao = $info_item['kcxl_nguoiTao'];
                      $user_tao = $newArr[$nguoi_tao];
                      if ($nguoi_tao != 0) {
                        $ten_nguoi_tao = $user_tao['ep_name'];
                        $anh_nguoi_tao = $user_tao['ep_image'];
                        if ($anh_nguoi_tao == "") {
                          $anh0 = '../images/ava_ad.png';
                        } else {
                          $anh0 = 'https://chamcong.24hpay.vn/upload/employee/' . $anh_nguoi_tao;
                        }
                      } else {
                        $ten_nguoi_tao = $tt_user['com_name'];
                        $anh0 = 'https://chamcong.24hpay.vn/upload/company/logo/' . $tt_user['com_logo'];
                      }
                      ?>
                      <p class="title_detail font_s14 line_16 color_grey">Người tạo:</p>
                      <div class="d_flex flex_center align_c">
                        <img class="ava_human_table" src="<?= $anh0 ?>" alt="">
                        <p class=" font_s14 line_16 color_grey font_w500"><?= $ten_nguoi_tao ?></p>
                      </div>
                    </div>
                    <div class="d_flex align_c boder_detail_top mt_17">
                      <p class="title_detail font_s14 line_16 color_grey">Ngày tạo:</p>
                      <p class="description_detail font_s14 line_16 color_grey font_w500"><?= $info_item['kcxl_ngayTao'] ?></p>
                    </div>
                    <div class="d_flex align_c boder_detail_top mt_17">
                      <p class="title_detail font_s14 line_16 color_grey">Trạng thái:</p>
                      <p class="description_detail pending font_s14 line_16 color_grey font_w500 display_none" <?php if ($info_item['kcxl_trangThai'] == 1) { ?>style="display: block;" <?php } ?>>Chờ duyệt</p>
                      <p class="description_detail approved_export font_s14 line_16 color_green_tb font_w500 display_none" <?php if ($info_item['kcxl_trangThai'] == 6) { ?>style="display: block;" <?php } ?>>Hoàn thành</p>
                      <p class="description_detail refuse font_s14 line_16 color_org font_w500 display_none" <?php if ($info_item['kcxl_trangThai'] == 2) { ?>style="display: block;" <?php } ?>>Từ
                        chối</p>
                      <p class="description_detail approved_wait_export font_s14 line_16 color_blue font_w500 display_none" <?php if ($info_item['kcxl_trangThai'] == 5) { ?>style="display: block;" <?php } ?>>Đã duyệt - Chờ xuất kho</p>
                    </div>
                    <div class="d_flex align_c boder_detail_top mt_17 reason_for_refusal display_none" <?php if ($info_item['kcxl_trangThai'] == 2) { ?>style="display: flex;" <?php } ?>>
                      <p class="title_detail font_s14 line_16 color_grey">Lý do từ chối:</p>
                      <p class="description_detail font_s14 line_16 color_grey font_w500"><?= $info_item['kcxl_liDoTuChoi'] ?>
                      </p>
                    </div>
                    <div class="d_flex align_c boder_detail_top mt_17">
                      <p class="title_detail font_s14 line_16 color_grey">Kho xuất:</p>
                      <p class="description_detail font_s14 line_16 color_grey font_w500"><?= $info_item['kho_name'] ?></p>
                    </div>
                    <div class="d_flex align_c boder_detail_top mt_17">
                      <p class="title_detail font_s14 line_16 color_grey">Ngày xuất:</p>
                      <p class="description_detail font_s14 line_16 color_grey font_w500"><?= $info_item['kcxl_ngayXuatKho'] ?></p>
                    </div>
                    <div class="d_flex align_c boder_detail_top mt_17">
                      <p class="title_detail font_s14 line_16 color_grey">Người xuất kho:</p>
                      <p class="description_detail font_s14 line_16 color_grey font_w500"><?= $newArr[$info_item['kcxl_nguoiThucHien']]['ep_name'] ?>
                      </p>
                    </div>
                    <!-- <div class="d_flex align_c boder_detail_top mt_17">
                    <p class="title_detail font_s14 line_16 color_grey">Ngày yêu cầu hoàn thành:</p>
                    <p class="description_detail font_s14 line_16 color_grey font_w500"><?= $info_item['kcxl_ngayYeuCauHoanThanh'] ?>
                    </p>
                  </div> -->
                    <div class="d_flex align_c boder_detail_top mt_17">
                      <p class="title_detail font_s14 line_16 color_grey">Phòng ban:</p>
                      <p class="description_detail font_s14 line_16 color_grey font_w500"><?= $newArr[$info_item['kcxl_nguoiThucHien']]['dep_name'] ?>
                      </p>
                    </div>
                    <div class="d_flex align_c boder_detail_top mt_17">
                      <p class="title_detail font_s14 line_16 color_grey">Thời hạn hoàn thành:</p>
                      <p class="description_detail font_s14 line_16 color_grey font_w500"><?= $info_item['kcxl_ngayYeuCauHoanThanh'] ?>
                      </p>
                    </div>
                    <div class="d_flex boder_detail_top mt_17">
                      <p class="title_detail font_s14 line_16 color_grey">Ghi chú:</p>
                      <p class="description_detail font_s14 line_16 color_grey font_w500"><?= $info_item['kcxl_ghi_chu'] ?></p>
                    </div>
                    <div class="d_flex align_c boder_detail_top mt_17 human_approved display_none" <?php if ($info_item['kcxl_trangThai'] == 2 || $info_item['kcxl_trangThai'] == 5 || $info_item['kcxl_trangThai'] == 6) { ?>style="display: flex;" <?php } ?>>
                      <?php
                      $nguoi_duyet = $info_item['kcxl_nguoiDuyet'];
                      $user_duyet = $newArr[$nguoi_duyet];
                      if ($nguoi_duyet != 0) {
                        $ten_nguoi_duyet = $user_duyet['ep_name'];
                        $anh_nguoi_duyet = $user_duyet['ep_image'];
                        if ($anh_nguoi_duyet == "") {
                          $anh3 = '../images/ava_ad.png';
                        } else {
                          $anh3 = 'https://chamcong.24hpay.vn/upload/employee/' . $anh_nguoi_duyet;
                        }
                      } else {
                        $ten_nguoi_duyet = $tt_user['com_name'];
                        $anh3 = 'https://chamcong.24hpay.vn/upload/company/logo/' . $tt_user['com_logo'];
                      }
                      ?>
                      <p class=".title_detail font_s14 line_16 color_grey" style="width: 200px">Người duyệt:</p>
                      <div class="d_flex flex_center align_c">
                        <img class="ava_human_table" src="<?= $anh3 ?>" alt="">
                        <p class=" font_s14 line_16 color_grey font_w500"><?= $ten_nguoi_duyet ?></p>
                      </div>
                    </div>
                    <div class="d_flex align_c boder_detail_top mt_17 approval_date display_none" <?php if ($info_item['kcxl_trangThai'] == 2 || $info_item['kcxl_trangThai'] == 5 || $info_item['kcxl_trangThai'] == 6) { ?>style="display: flex;" <?php } ?>>
                      <p class="title_detail font_s14 line_16 color_grey">Ngày duyệt:</p>
                      <p class="description_detail font_s14 line_16 color_grey font_w500"><?= $info_item['kcxl_ngayDuyet'] ?>
                      </p>
                    </div>
                    <div class="d_flex align_c boder_detail_top mt_17 human_complete display_none" <?php if ($info_item['kcxl_trangThai'] == 6) { ?>style="display: flex;" <?php } ?>>
                    <?php
                      $nguoi_ht = $info_item['kcxl_nguoiHoanThanh'];
                      $user_ht = $newArr[$nguoi_ht];
                      if ($nguoi_ht != 0) {
                        $ten_nguoi_ht = $user_ht['ep_name'];
                        $anh_nguoi_ht = $user_ht['ep_image'];
                        if ($anh_nguoi_ht == "") {
                          $anh4 = '../images/ava_ad.png';
                        } else {
                          $anh4 = 'https://chamcong.24hpay.vn/upload/employee/' . $anh_nguoi_ht;
                        }
                      } else {
                        $ten_nguoi_ht = $tt_user['com_name'];
                        $anh4 = 'https://chamcong.24hpay.vn/upload/company/logo/' . $tt_user['com_logo'];
                      }
                      ?>
                      <p class="description_detail font_s14 line_16 color_grey" style="width: 200px">Người hoàn thành:</p>
                      <div class="d_flex flex_center align_c">
                        <img class="ava_human_table" src="<?= $anh4 ?>" alt="">
                        <p class=" font_s14 line_16 color_grey font_w500"><?= $ten_nguoi_ht ?></p>
                      </div>
                    </div>
                    <div class="d_flex align_c boder_detail_top mt_17 data_complete display_none" <?php if ($info_item['kcxl_trangThai'] == 6) { ?>style="display: flex;" <?php } ?>>
                      <p class="title_detail font_s14 line_16 color_grey">Ngày hoàn thành:</p>
                      <p class="description_detail font_s14 line_16 color_grey font_w500"><?= $info_item['kcxl_ngayHoanThanh'] ?>
                      </p>
                    </div>
                  </div>
                <? } ?>

                <?php if ($info_item['kcxl_hinhThuc'] == "XK5") { ?>
                  <div class="table_selected_other_export srcoll_equipment_supplies_detail">
                    <div class="d_flex align_c">
                      <p class="title_detail font_s14 line_16 color_grey">Số phiếu:</p>
                      <p class="description_detail font_s14 line_16 color_grey font_w500">PXK - <?= $info_item['kcxl_id'] ?></p>
                    </div>
                    <div class="d_flex align_c boder_detail_top mt_17">
                      <p class="title_detail font_s14 line_16 color_grey">Hình thức xuất kho:</p>
                      <p class="description_detail font_s14 line_16 color_grey font_w500">Xuất khác</p>
                    </div>
                    <div class="d_flex align_c boder_detail_top mt_17">
                      <?php
                      $nguoi_tao = $info_item['kcxl_nguoiTao'];
                      $user_tao = $newArr[$nguoi_tao];
                      if ($nguoi_tao != 0) {
                        $ten_nguoi_tao = $user_tao['ep_name'];
                        $anh_nguoi_tao = $user_tao['ep_image'];
                        if ($anh_nguoi_tao == "") {
                          $anh0 = '../images/ava_ad.png';
                        } else {
                          $anh0 = 'https://chamcong.24hpay.vn/upload/employee/' . $anh_nguoi_tao;
                        }
                      } else {
                        $ten_nguoi_tao = $tt_user['com_name'];
                        $anh0 = 'https://chamcong.24hpay.vn/upload/company/logo/' . $tt_user['com_logo'];
                      }
                      ?>
                      <p class="title_detail font_s14 line_16 color_grey">Người tạo:</p>
                      <div class="d_flex flex_center align_c">
                        <img class="ava_human_table" src="<?= $anh0 ?>" alt="">
                        <p class=" font_s14 line_16 color_grey font_w500"><?= $ten_nguoi_tao ?></p>
                      </div>
                    </div>
                    <div class="d_flex align_c boder_detail_top mt_17">
                      <p class="title_detail font_s14 line_16 color_grey">Ngày tạo:</p>
                      <p class="description_detail font_s14 line_16 color_grey font_w500"><?= $info_item['kcxl_ngayTao'] ?></p>
                    </div>
                    <div class="d_flex align_c boder_detail_top mt_17">
                      <p class="title_detail font_s14 line_16 color_grey">Trạng thái:</p>
                      <p class="description_detail pending font_s14 line_16 color_grey font_w500 display_none" <?php if ($info_item['kcxl_trangThai'] == 1) { ?>style="display: block;" <?php } ?>>Chờ duyệt</p>
                      <p class="description_detail approved_export font_s14 line_16 color_green_tb font_w500 display_none" <?php if ($info_item['kcxl_trangThai'] == 6) { ?>style="display: block;" <?php } ?>>Hoàn thành</p>
                      <p class="description_detail refuse font_s14 line_16 color_org font_w500 display_none" <?php if ($info_item['kcxl_trangThai'] == 2) { ?>style="display: block;" <?php } ?>>Từ
                        chối</p>
                      <p class="description_detail approved_wait_export font_s14 line_16 color_blue font_w500 display_none" <?php if ($info_item['kcxl_trangThai'] == 5) { ?>style="display: block;" <?php } ?>>Đã duyệt - Chờ xuất kho</p>
                    </div>
                    <div class="d_flex align_c boder_detail_top mt_17 reason_for_refusal display_none" <?php if ($info_item['kcxl_trangThai'] == 2) { ?>style="display: flex;" <?php } ?>>
                      <p class="title_detail font_s14 line_16 color_grey">Lý do từ chối:</p>
                      <p class="description_detail font_s14 line_16 color_grey font_w500"><?= $info_item['kcxl_liDoTuChoi'] ?>
                      </p>
                    </div>
                    <div class="d_flex align_c boder_detail_top mt_17">
                      <p class="title_detail font_s14 line_16 color_grey">Kho xuất:</p>
                      <p class="description_detail font_s14 line_16 color_grey font_w500"><?= $info_item['kho_name'] ?></p>
                    </div>
                    <div class="d_flex align_c boder_detail_top mt_17">
                      <p class="title_detail font_s14 line_16 color_grey">Ngày xuất:</p>
                      <p class="description_detail font_s14 line_16 color_grey font_w500"><?= $info_item['kcxl_ngayXuatKho'] ?></p>
                    </div>
                    <div class="d_flex align_c boder_detail_top mt_17">
                      <p class="title_detail font_s14 line_16 color_grey">Người giao hàng:</p>
                      <p class="description_detail font_s14 line_16 color_grey font_w500"><?= $newArr[$info_item['kcxl_nguoiThucHien']]['ep_name'] ?>
                      </p>
                    </div>
                    <div class="d_flex align_c boder_detail_top mt_17">
                      <p class="title_detail font_s14 line_16 color_grey">Phòng ban:</p>
                      <p class="description_detail font_s14 line_16 color_grey font_w500"><?= $newArr[$info_item['kcxl_nguoiThucHien']]['dep_name'] ?>
                      </p>
                    </div>
                    <div class="d_flex align_c boder_detail_top mt_17">
                      <?php
                      $nguoi_nhan = $info_item['kcxl_nguoiNhan'];
                      $user_nhan = $newArr[$nguoi_nhan];
                      if ($nguoi_nhan != 0) {
                        $ten_nguoi_nhan = $user_nhan['ep_name'];
                        $anh_nguoi_nhan = $user_nhan['ep_image'];
                        if ($anh_nguoi_nhan == "") {
                          $anh1 = '../images/ava_ad.png';
                        } else {
                          $anh1 = 'https://chamcong.24hpay.vn/upload/employee/' . $anh_nguoi_nhan;
                        }
                      } else {
                        $ten_nguoi_nhan = $tt_user['com_name'];
                        $anh1 = 'https://chamcong.24hpay.vn/upload/company/logo/' . $tt_user['com_logo'];
                      }
                      ?>
                      <p class="title_detail font_s14 line_16 color_grey">Người nhận:</p>
                      <div class="d_flex flex_center align_c">
                        <img class="ava_human_table" src="<?= $anh1 ?>" alt="">
                        <p class=" font_s14 line_16 color_grey font_w500"><?= $ten_nguoi_nhan ?></p>
                      </div>
                      </p>
                    </div>
                    <div class="d_flex align_c boder_detail_top mt_17">
                      <p class="title_detail font_s14 line_16 color_grey">Phòng ban:</p>
                      <p class="description_detail font_s14 line_16 color_grey font_w500"><?= $newArr[$info_item['kcxl_nguoiNhan']]['dep_name'] ?>
                      </p>
                    </div>
                    <div class="d_flex boder_detail_top mt_17">
                      <p class="title_detail font_s14 line_16 color_grey">Ghi chú:</p>
                      <p class="description_detail font_s14 line_16 color_grey font_w500"><?= $info_item['kcxl_ghi_chu'] ?></p>
                    </div>
                    <div class="d_flex align_c boder_detail_top mt_17 human_approved display_none" <?php if ($info_item['kcxl_trangThai'] == 6 || $info_item['kcxl_trangThai'] == 2 || $info_item['kcxl_trangThai'] == 5) { ?>style="display: flex;" <?php } ?>>
                      <?php
                      $nguoi_duyet = $info_item['kcxl_nguoiDuyet'];
                      $user_duyet = $newArr[$nguoi_duyet];
                      if ($nguoi_duyet != 0) {
                        $ten_nguoi_duyet = $user_duyet['ep_name'];
                        $anh_nguoi_duyet = $user_duyet['ep_image'];
                        if ($anh_nguoi_duyet == "") {
                          $anh3 = '../images/ava_ad.png';
                        } else {
                          $anh3 = 'https://chamcong.24hpay.vn/upload/employee/' . $anh_nguoi_duyet;
                        }
                      } else {
                        $ten_nguoi_duyet = $tt_user['com_name'];
                        $anh3 = 'https://chamcong.24hpay.vn/upload/company/logo/' . $tt_user['com_logo'];
                      }
                      ?>
                      <p class="title_detail font_s14 line_16 color_grey">Người duyệt:</p>
                      <div class="d_flex flex_center align_c">
                        <img class="ava_human_table" src="<?= $anh3 ?>" alt="">
                        <p class=" font_s14 line_16 color_grey font_w500"><?= $ten_nguoi_duyet ?></p>
                      </div>
                    </div>
                    <div class="d_flex align_c boder_detail_top mt_17 approval_date display_none" <?php if ($info_item['kcxl_trangThai'] == 6 || $info_item['kcxl_trangThai'] == 2 || $info_item['kcxl_trangThai'] == 5) { ?>style="display: flex;" <?php } ?>>
                      <p class="title_detail font_s14 line_16 color_grey">Ngày duyệt:</p>
                      <p class="description_detail font_s14 line_16 color_grey font_w500"> <?= $info_item['kcxl_ngayDuyet'] ?>
                      </p>
                    </div>
                    <div class="d_flex align_c boder_detail_top mt_17 human_complete display_none" <?php if ($info_item['kcxl_trangThai'] == 6) { ?>style="display: flex;" <?php } ?>>
                    <?php
                      $nguoi_ht = $info_item['kcxl_nguoiHoanThanh'];
                      $user_ht = $newArr[$nguoi_ht];
                      if ($nguoi_ht != 0) {
                        $ten_nguoi_ht = $user_ht['ep_name'];
                        $anh_nguoi_ht = $user_ht['ep_image'];
                        if ($anh_nguoi_ht == "") {
                          $anh4 = '../images/ava_ad.png';
                        } else {
                          $anh4 = 'https://chamcong.24hpay.vn/upload/employee/' . $anh_nguoi_ht;
                        }
                      } else {
                        $ten_nguoi_ht = $tt_user['com_name'];
                        $anh4 = 'https://chamcong.24hpay.vn/upload/company/logo/' . $tt_user['com_logo'];
                      }
                      ?>
                      <p class="title_detail font_s14 line_16 color_grey">Người hoàn thành:</p>
                      <div class="d_flex flex_center align_c">
                        <img class="ava_human_table" src="<?= $anh4 ?>" alt="">
                        <p class=" font_s14 line_16 color_grey font_w500"><?= $ten_nguoi_ht ?></p>
                      </div>
                    </div>
                    <div class="d_flex align_c boder_detail_top mt_17 data_complete display_none" <?php if ($info_item['kcxl_trangThai'] == 6) { ?>style="display: flex;" <?php } ?>>
                      <p class="title_detail font_s14 line_16 color_grey">Ngày hoàn thành:</p>
                      <p class="description_detail font_s14 line_16 color_grey font_w500"><?= $info_item['kcxl_ngayHoanThanh'] ?>
                      </p>
                    </div>
                  </div>
                <? } ?>

              </div>
            </div>
          </div>
        </div>
        <p class="mt_20">Danh sách vật tư</p>
        <div class="main_table mt_20">

          <?php if ($info_item['kcxl_hinhThuc'] == "XK1") { ?>
            <div class="table_construction_export">
              <div class="position_r d_flex align_c">
                <div class="table_vt_scr">
                  <table class="table table_1" id="table_1">
                    <tr class="color_white font_s16 line_h19 font_w500">
                      <th>STT
                        <span class="span_tbody"></span>
                      </th>
                      <th>Mã vật tư thiết bị
                        <span class="span_tbody"></span>
                      </th>
                      <th>Tên đầy đủ vật tư thiết bị
                        <span class="span_tbody"></span>
                      </th>
                      <th>Đơn vị tính
                        <span class="span_tbody"></span>
                      </th>
                      <th>Hãng sản xuất
                        <span class="span_tbody"></span>
                      </th>
                      <th>Xuất xứ
                        <span class="span_tbody"></span>
                      </th>
                      <th>Số lượng yêu cầu
                        <span class="span_tbody"></span>
                      </th>
                      <th>Số lượng được duyệt
                        <span class="span_tbody"></span>
                      </th>
                      <th>Số lượng thực tế xuất kho
                        <span class="span_tbody"></span>
                      </th>
                      <th>Đơn giá (VNĐ)
                        <span class="span_tbody"></span>
                      </th>
                      <th>Thành tiền (VNĐ)
                      </th>
                    </tr>
                    <tbody class="data_table_1" id="dom-table">
                      <?php
                      $vt = $value['id_vat_tu'];
                      $vattu_nhap1 = new db_query("SELECT `dsvt_id`, `dsvt_name`, `dvt_name`, `hsx_name`, `xx_name`, `slvt_soLuongXuatKho`,`dsvt_donGia`,`slvt_so_luong_yeu_cau`,`slvt_so_luong_duyet`
                        FROM `danh-sach-vat-tu`
                        LEFT JOIN `don-vi-tinh` ON `dsvt_donViTinh` =`dvt_id` 
                        LEFT JOIN `hang-san-xuat` ON `dsvt_hangSanXuat` = `hsx_id`
                        LEFT JOIN `xuat-xu` ON `dsvt_xuatXu` = `xx_id`
                        LEFT JOIN `so-luong-vat-tu` ON `dsvt_id` = `slvt_maVatTuThietBi`
                        LEFT JOIN `kho-cho-xu-li` ON `slvt_idPhieu` = `kcxl_id`
                        WHERE `dsvt_check` = '1' AND `slvt_idPhieu` = '$id_px' AND `dsvt_id_ct` = '$id_cty '
                        ORDER BY `dsvt_id` ASC ");
                      $stt = 1;
                      while ($info_dsvt = mysql_fetch_assoc($vattu_nhap1->result)) {
                      ?>
                        <tr class="color_grey font_s14 line_h17 font_w400">
                          <td><?= $stt++; ?></td>
                          <td>VT-<?= $info_dsvt['dsvt_id']; ?></td>
                          <td style="text-align: left;">
                            <a href="#" class="color_blue font_w500 id_vt" data="<?= $info_dsvt['dsvt_id']; ?>">
                              <?= $info_dsvt['dsvt_name']; ?>
                            </a>
                          </td>
                          <td><?= $info_dsvt['dvt_name'] ?></td>
                          <td style="text-align: left;"><?= $info_dsvt['hsx_name'] ?></td>
                          <td><?= $info_dsvt['xx_name'] ?></td>
                          <td style="text-align: right;">
                            <?= $info_dsvt['slvt_so_luong_yeu_cau']; ?>
                          </td>
                          <td style="text-align: right;">
                            <?= $info_dsvt['slvt_so_luong_duyet']; ?>
                          </td>
                          <td>
                            <input class="nhap_so_luong" style="border: none; text-align: right;  outline: none" type="text" placeholder="Nhập số lượng" onkeyup="tong_vt(this)" oninput="<?= $oninput ?>" value="<?= $info_dsvt['slvt_soLuongXuatKho'] ?>" readonly>
                          </td>
                          <td style="text-align: right;" class="don_gia_3"><?= $info_dsvt['dsvt_donGia']; ?></td>
                          <td style="text-align: right;" class="thanh_tien_3">
                            <?= (number_format($info_dsvt['slvt_soLuongXuatKho'] * $info_dsvt['dsvt_donGia'], 0, '', '.')); ?>
                          </td>
                        </tr>
                      <?php
                      } ?>
                    </tbody>
                  </table>
                </div>
                <div class="pre_q d_flex align_c flex_center position_a">
                  <span class="pre_arrow"></span>
                </div>
                <div class="next_q d_flex align_c flex_center position_a">
                  <span class="next_arrow"></span>
                </div>
              </div>
            </div>
          <? } ?>

          <?php if ($info_item['kcxl_hinhThuc'] == "XK2") { ?>
            <div class="table_export_transfer ">
              <div class="position_r d_flex align_c">
                <div class="table_vt_scr">
                  <table class="table table_2">
                    <tr class="color_white font_s16 line_h19 font_w500">
                      <th>STT
                        <span class="span_tbody"></span>
                      </th>
                      <th>Mã vật tư thiết bị
                        <span class="span_tbody"></span>
                      </th>
                      <th>Tên đầy đủ vật tư thiết bị
                        <span class="span_tbody"></span>
                      </th>
                      <th>Đơn vị tính
                        <span class="span_tbody"></span>
                      </th>
                      <th>Hãng sản xuất
                        <span class="span_tbody"></span>
                      </th>
                      <th>Xuất xứ
                        <span class="span_tbody"></span>
                      </th>
                      <th>Số lượng yêu cầu
                        <span class="span_tbody"></span>
                      </th>
                      <th>Số lượng thực tế xuất kho
                        <span class="span_tbody"></span>
                      </th>
                      <th>Đơn giá (VNĐ)
                        <span class="span_tbody"></span>
                      </th>
                      <th>Thành tiền (VNĐ)
                      </th>
                    </tr>
                    <tbody class="data_table_2">
                      <?php
                      $vt = $value['id_vat_tu'];
                      $vattu_nhap1 = new db_query("SELECT `dsvt_id`, `dsvt_name`, `dvt_name`, `hsx_name`, `xx_name`, `slvt_soLuongXuatKho`,`dsvt_donGia`,`slvt_so_luong_yeu_cau`,`slvt_so_luong_duyet`
                        FROM `danh-sach-vat-tu`
                        LEFT JOIN `don-vi-tinh` ON `dsvt_donViTinh` =`dvt_id`
                        LEFT JOIN `hang-san-xuat` ON `dsvt_hangSanXuat` = `hsx_id`
                        LEFT JOIN `xuat-xu` ON `dsvt_xuatXu` = `xx_id`
                        LEFT JOIN `so-luong-vat-tu` ON `dsvt_id` = `slvt_maVatTuThietBi`
                        LEFT JOIN `kho-cho-xu-li` ON `slvt_idPhieu` = `kcxl_id`
                        WHERE `dsvt_check` = '1' AND `slvt_idPhieu` = '$id_px' AND `dsvt_id_ct` = '$id_cty '
                        ORDER BY `dsvt_id` ASC ");
                      $stt = 1;
                      while ($info_dsvt = mysql_fetch_assoc($vattu_nhap1->result)) {
                      ?>
                        <tr class="color_grey font_s14 line_h17 font_w400">
                          <td><?= $stt++; ?></td>
                          <td>VT-<?= $info_dsvt['dsvt_id']; ?></td>
                          <td style="text-align: left;">
                            <a href="#" class="color_blue font_w500 id_vt" data="<?= $info_dsvt['dsvt_id']; ?>">
                              <?= $info_dsvt['dsvt_name']; ?>
                            </a>
                          </td>
                          <td><?= $info_dsvt['dvt_name'] ?></td>
                          <td style="text-align: left;"><?= $info_dsvt['hsx_name'] ?></td>
                          <td><?= $info_dsvt['xx_name'] ?></td>
                          <td style="text-align: right;">
                            <?= $info_dsvt['slvt_so_luong_yeu_cau']; ?>
                          </td>
                          <!-- <td style="text-align: right;">
                        <?= $info_dsvt['slvt_so_luong_duyet']; ?>
                      </td> -->
                          <td>
                            <input class="nhap_so_luong" style="border: none; text-align: right;  outline: none" type="text" placeholder="Nhập số lượng" onkeyup="tong_vt(this)" oninput="<?= $oninput ?>" value="<?= $info_dsvt['slvt_soLuongXuatKho'] ?>" readonly>
                          </td>
                          <td style="text-align: right;" class="don_gia_3"><?= $info_dsvt['dsvt_donGia']; ?></td>
                          <td style="text-align: right;" class="thanh_tien_3">
                            <?= (number_format($info_dsvt['slvt_soLuongXuatKho'] * $info_dsvt['dsvt_donGia'], 0, '', '.')); ?>
                          </td>
                        </tr>
                      <?php
                      } ?>
                    </tbody>
                  </table>
                </div>
                <div class="pre_q d_flex align_c flex_center position_a">
                  <span class="pre_arrow"></span>
                </div>
                <div class="next_q d_flex align_c flex_center position_a">
                  <span class="next_arrow"></span>
                </div>
              </div>
            </div>
          <? } ?>

          <?php if ($info_item['kcxl_hinhThuc'] == "XK3") { ?>
            <div class="table_export_on_request ">
              <div class="position_r d_flex align_c">
                <div class="table_vt_scr">
                  <table class="table table_1" id="table_1">
                    <tr class="color_white font_s16 line_h19 font_w500">
                      <th>STT
                        <span class="span_tbody"></span>
                      </th>
                      <th>Mã vật tư thiết bị
                        <span class="span_tbody"></span>
                      </th>
                      <th>Tên đầy đủ vật tư thiết bị
                        <span class="span_tbody"></span>
                      </th>
                      <th>Đơn vị tính
                        <span class="span_tbody"></span>
                      </th>
                      <th>Hãng sản xuất
                        <span class="span_tbody"></span>
                      </th>
                      <th>Xuất xứ
                        <span class="span_tbody"></span>
                      </th>
                      <th>Số lượng yêu cầu
                        <span class="span_tbody"></span>
                      </th>
                      <th>Số lượng được duyệt
                        <span class="span_tbody"></span>
                      </th>
                      <th>Số lượng thực tế xuất kho
                        <span class="span_tbody"></span>
                      </th>
                      <th>Đơn giá (VNĐ)
                        <span class="span_tbody"></span>
                      </th>
                      <th>Thành tiền (VNĐ)
                      </th>
                    </tr>
                    <tbody class="data_table_2" id="dom-table1">
                      <?php
                      $vt = $value['id_vat_tu'];
                      $vattu_nhap1 = new db_query("SELECT `dsvt_id`, `dsvt_name`, `dvt_name`, `hsx_name`, `xx_name`, `slvt_soLuongXuatKho`,`dsvt_donGia`,`slvt_so_luong_yeu_cau`,`slvt_so_luong_duyet`
                        FROM `danh-sach-vat-tu`
                        LEFT JOIN `don-vi-tinh` ON `dsvt_donViTinh` =`dvt_id`
                        LEFT JOIN `hang-san-xuat` ON `dsvt_hangSanXuat` = `hsx_id`
                        LEFT JOIN `xuat-xu` ON `dsvt_xuatXu` = `xx_id`
                        LEFT JOIN `so-luong-vat-tu` ON `dsvt_id` = `slvt_maVatTuThietBi`
                        LEFT JOIN `kho-cho-xu-li` ON `slvt_idPhieu` = `kcxl_id`
                        WHERE `dsvt_check` = '1' AND `slvt_idPhieu` = '$id_px' AND `dsvt_id_ct` = '$id_cty '
                        ORDER BY `dsvt_id` ASC ");
                      $stt = 1;
                      while ($info_dsvt = mysql_fetch_assoc($vattu_nhap1->result)) {
                      ?>
                        <tr class="color_grey font_s14 line_h17 font_w400">
                          <td><?= $stt++; ?></td>
                          <td>VT-<?= $info_dsvt['dsvt_id']; ?></td>
                          <td style="text-align: left;">
                            <a href="#" class="color_blue font_w500 id_vt" data="<?= $info_dsvt['dsvt_id']; ?>">
                              <?= $info_dsvt['dsvt_name']; ?>
                            </a>
                          </td>
                          <td><?= $info_dsvt['dvt_name'] ?></td>
                          <td style="text-align: left;"><?= $info_dsvt['hsx_name'] ?></td>
                          <td><?= $info_dsvt['xx_name'] ?></td>
                          <td style="text-align: right;">
                            <?= $info_dsvt['slvt_so_luong_yeu_cau']; ?>
                          </td>
                          <td style="text-align: right;">
                            <?= $info_dsvt['slvt_so_luong_duyet']; ?>
                          </td>
                          <td>
                            <input class="nhap_so_luong" style="border: none; text-align: right;  outline: none" type="text" placeholder="Nhập số lượng" onkeyup="tong_vt(this)" oninput="<?= $oninput ?>" value="<?= $info_dsvt['slvt_soLuongXuatKho'] ?>" readonly>
                          </td>
                          <td style="text-align: right;" class="don_gia_3"><?= $info_dsvt['dsvt_donGia']; ?></td>
                          <td style="text-align: right;" class="thanh_tien_3">
                            <?= (number_format($info_dsvt['slvt_soLuongXuatKho'] * $info_dsvt['dsvt_donGia'], 0, '', '.')); ?>
                          </td>
                        </tr>
                      <?php
                      } ?>
                    </tbody>
                  </table>
                </div>
                <div class="pre_q d_flex align_c flex_center position_a">
                  <span class="pre_arrow"></span>
                </div>
                <div class="next_q d_flex align_c flex_center position_a">
                  <span class="next_arrow"></span>
                </div>
              </div>
            </div>
          <? } ?>

          <?php if ($info_item['kcxl_hinhThuc'] == "XK4") { ?>
            <div class="table_made_to_order ">
              <div class="position_r d_flex align_c">
                <div class="table_vt_scr">
                  <table class="table table_2">
                    <tr class="color_white font_s16 line_h19 font_w500">
                      <th>STT
                        <span class="span_tbody"></span>
                      </th>
                      <th>Mã vật tư thiết bị
                        <span class="span_tbody"></span>
                      </th>
                      <th>Tên đầy đủ vật tư thiết bị
                        <span class="span_tbody"></span>
                      </th>
                      <th>Đơn vị tính
                        <span class="span_tbody"></span>
                      </th>
                      <th>Hãng sản xuất
                        <span class="span_tbody"></span>
                      </th>
                      <th>Xuất xứ
                        <span class="span_tbody"></span>
                      </th>
                      <th>Số lượng yêu cầu
                        <span class="span_tbody"></span>
                      </th>
                      <th>Số lượng thực tế xuất kho
                        <span class="span_tbody"></span>
                      </th>
                      <th>Đơn giá (VNĐ)
                        <span class="span_tbody"></span>
                      </th>
                      <th>Thành tiền (VNĐ)
                      </th>
                    </tr>
                    <?php
                    $vt = $value['id_vat_tu'];
                    $vattu_nhap1 = new db_query("SELECT `dsvt_id`, `dsvt_name`, `dvt_name`, `hsx_name`, `xx_name`, `slvt_soLuongXuatKho`,`dsvt_donGia`,`slvt_so_luong_yeu_cau`,`slvt_so_luong_duyet`
                        FROM `danh-sach-vat-tu`
                        LEFT JOIN `don-vi-tinh` ON `dsvt_donViTinh` =`dvt_id`
                        LEFT JOIN `hang-san-xuat` ON `dsvt_hangSanXuat` = `hsx_id`
                        LEFT JOIN `xuat-xu` ON `dsvt_xuatXu` = `xx_id`
                        LEFT JOIN `so-luong-vat-tu` ON `dsvt_id` = `slvt_maVatTuThietBi`
                        LEFT JOIN `kho-cho-xu-li` ON `slvt_idPhieu` = `kcxl_id`
                        WHERE `dsvt_check` = '1' AND `slvt_idPhieu` = '$id_px' AND `dsvt_id_ct` = '$id_cty '
                        ORDER BY `dsvt_id` ASC ");
                    $stt = 1;
                    while ($info_dsvt = mysql_fetch_assoc($vattu_nhap1->result)) {
                    ?>
                      <tr class="color_grey font_s14 line_h17 font_w400">
                        <td><?= $stt++; ?></td>
                        <td>VT-<?= $info_dsvt['dsvt_id']; ?></td>
                        <td style="text-align: left;">
                          <a href="#" class="color_blue font_w500 id_vt" data="<?= $info_dsvt['dsvt_id']; ?>">
                            <?= $info_dsvt['dsvt_name']; ?>
                          </a>
                        </td>
                        <td><?= $info_dsvt['dvt_name'] ?></td>
                        <td style="text-align: left;"><?= $info_dsvt['hsx_name'] ?></td>
                        <td><?= $info_dsvt['xx_name'] ?></td>
                        <td style="text-align: right;">
                          <?= $info_dsvt['slvt_so_luong_yeu_cau']; ?>
                        </td>
                        <td>
                          <input class="nhap_so_luong" style="border: none; text-align: right;  outline: none" type="text" placeholder="Nhập số lượng" onkeyup="tong_vt(this)" oninput="<?= $oninput ?>" value="<?= $info_dsvt['slvt_soLuongXuatKho'] ?>" readonly>
                        </td>
                        <td style="text-align: right;" class="don_gia_3"><?= $info_dsvt['dsvt_donGia']; ?></td>
                        <td style="text-align: right;" class="thanh_tien_3">
                          <?= (number_format($info_dsvt['slvt_soLuongXuatKho'] * $info_dsvt['dsvt_donGia'], 0, '', '.')); ?>
                        </td>
                      </tr>
                    <?php
                    } ?>
                  </table>
                </div>
                <div class="pre_q d_flex align_c flex_center position_a">
                  <span class="pre_arrow"></span>
                </div>
                <div class="next_q d_flex align_c flex_center position_a">
                  <span class="next_arrow"></span>
                </div>
              </div>
            </div>
          <? } ?>

          <?php if ($info_item['kcxl_hinhThuc'] == "XK5") { ?>
            <div class="table_export_by_order ">
              <div class="position_r d_flex align_c">
                <div class="table_vt_scr">
                  <table class="table table_3">
                    <tr class="color_white font_s16 line_h19 font_w500">
                      <th>
                        <span class="span_tbody"></span>
                      </th>
                      <th>Mã vật tư thiết bị
                        <span class="span_tbody"></span>
                      </th>
                      <th>Tên đầy đủ vật tư thiết bị
                        <span class="span_tbody"></span>
                      </th>
                      <th>Đơn vị tính
                        <span class="span_tbody"></span>
                      </th>
                      <th>Hãng sản xuất
                        <span class="span_tbody"></span>
                      </th>
                      <th>Xuất xứ
                        <span class="span_tbody"></span>
                      </th>
                      <th>Số lượng xuất kho
                        <span class="span_tbody"></span>
                      </th>
                      <th>Đơn giá (VNĐ)
                        <span class="span_tbody"></span>
                      </th>
                      <th>Thành tiền (VNĐ)
                      </th>
                    </tr>
                    <tbody class="table_khac">
                      <?php
                      $vt = $value['id_vat_tu'];
                      $vattu_nhap1 = new db_query("SELECT `dsvt_id`, `dsvt_name`, `dvt_name`, `hsx_name`, `xx_name`, `slvt_soLuongXuatKho`,`dsvt_donGia`,`slvt_so_luong_yeu_cau`,`slvt_so_luong_duyet`
                        FROM `danh-sach-vat-tu`
                        LEFT JOIN `don-vi-tinh` ON `dsvt_donViTinh` =`dvt_id`
                        LEFT JOIN `hang-san-xuat` ON `dsvt_hangSanXuat` = `hsx_id`
                        LEFT JOIN `xuat-xu` ON `dsvt_xuatXu` = `xx_id`
                        LEFT JOIN `so-luong-vat-tu` ON `dsvt_id` = `slvt_maVatTuThietBi`
                        LEFT JOIN `kho-cho-xu-li` ON `slvt_idPhieu` = `kcxl_id`
                        WHERE `dsvt_check` = '1' AND `slvt_idPhieu` = '$id_px' AND `dsvt_id_ct` = '$id_cty '
                        ORDER BY `dsvt_id` ASC ");
                      $stt = 1;
                      while ($info_dsvt = mysql_fetch_assoc($vattu_nhap1->result)) {
                      ?>
                        <tr class="color_grey font_s14 line_h17 font_w400">
                          <td><?= $stt++; ?></td>
                          <td>VT-<?= $info_dsvt['dsvt_id']; ?></td>
                          <td style="text-align: left;">
                            <a href="#" class="color_blue font_w500 id_vt" data="<?= $info_dsvt['dsvt_id']; ?>">
                              <?= $info_dsvt['dsvt_name']; ?>
                            </a>
                          </td>
                          <td><?= $info_dsvt['dvt_name'] ?></td>
                          <td style="text-align: left;"><?= $info_dsvt['hsx_name'] ?></td>
                          <td><?= $info_dsvt['xx_name'] ?></td>
                          <td>
                            <input class="nhap_so_luong" style="border: none; text-align: right;  outline: none" type="text" placeholder="Nhập số lượng" onkeyup="tong_vt(this)" oninput="<?= $oninput ?>" value="<?= $info_dsvt['slvt_soLuongXuatKho'] ?>" readonly>
                          </td>
                          <td style="text-align: right;" class="don_gia_3"><?= $info_dsvt['dsvt_donGia']; ?></td>
                          <td style="text-align: right;" class="thanh_tien_3">
                            <?= (number_format($info_dsvt['slvt_soLuongXuatKho'] * $info_dsvt['dsvt_donGia'], 0, '', '.')); ?>
                          </td>
                        </tr>
                      <?php
                      } ?>
                    </tbody>
                    <!-- <tr>
                    <td colspan="11">
                      <div class="d_flex align_c add_row">
                        <img src="../images/add_vt.png" alt="">
                        <p class="color_blue font_s14 line_h16 font_w500">Thêm vật tư</p>
                      </div>
                    </td>
                  </tr> -->
                  </table>
                </div>
                <div class="pre_q d_flex align_c flex_center position_a">
                  <span class="pre_arrow"></span>
                </div>
                <div class="next_q d_flex align_c flex_center position_a">
                  <span class="next_arrow"></span>
                </div>
              </div>
            </div>
          <? } ?>
        </div>
      </div>
    </div>
  </div>
  <?php include('../includes/popup_overview.php');  ?>
  <?php include('../includes/popup_h.php');  ?>
</body>
<script type="text/javascript" src="../js/jquery-3.4.1.min.js"></script>
<script type="text/javascript" src="../js/jquery.validate.min.js"></script>
<script type="text/javascript" src="../js/select2.min.js"></script>
<script type="text/javascript" src="../js/js_h.js"></script>
<script type="text/javascript" src="../js/validate_h.js"></script>
<script type="text/javascript" src="../js/js_k.js"></script>
<script type="text/javascript" src="../js/js_n.js"></script>

<script>
  $('.active9').each(function() {
    if ($(this).hasClass('active9')) {
      $(this).find('a').addClass('active');
    }
  });

  $('.button_browse').click(function() {
    var id = "<?= $id_px ?>"
    $('.check_export_note').show();
    $('.check_export_note .text_tb').text('')
    var text = 'Bạn có chắc chắn muốn duyệt phiếu xuất kho<br><strong>PKK-' + id + '</strong>?'
    $('.check_export_note .text_tb').append(text)
  })

  $('.button_accp_duyet').click(function() {
    var value = '5';
    var id = "<?= $id_px ?>"
    var id_cty = "<?= $id_cty ?>"
    var trangThai = "<?= $info_item['kcxl_trangThai'] ?>"
    var kcxl_nguoiDuyet = "<?= $user_id ?>"


    $.ajax({
      url: '../ajax/edit_detail_pxk.php',
      type: 'POST',
      // dataType: 'Json',
      data: {
        id_pxk: id,
        value: value,
        id_cty: id_cty,
        trangThai: trangThai,
        kcxl_nguoiDuyet: kcxl_nguoiDuyet
      },
      success: function(response) {
        var text = $('#check_export_note_success .p_add_succ').text('');
        var text_new = '';
        // var name = $("input[name='name_full_dv']").val();
        text_new += 'Duyệt phiếu xuất kho PXK-';
        text_new += '<strong>';
        text_new += '&nbsp' + id;
        text_new += '</strong>';
        text_new += '&nbspthành công!';
        text.append(text_new);
        $('#check_export_note').hide();
        $('#check_export_note_success').show();
      }
    })
  });

  $('.button_complete').click(function() {
    var id = "<?= $id_px ?>"
    $('.complete_export_note').show();
    $('.complete_export_note .text_tb').text('')
    var text = 'Bạn có chắc chắn muốn hoàn thành phiếu xuất kho<br><strong>PKK-' + id + '</strong>?'
    $('.complete_export_note .text_tb').append(text)
  })

  $('.button_accp_hoan_thanh').click(function() {
    var listTable = [];
    $(".nhap_so_luong").each(function() {
      var data = $(this).parent().parent().find('.id_vt').attr('data');
      var soluong = $(this).val();
      listTable.push({
        'id': data,
        'soluong': soluong,
      });
    });
    var value = '6';
    var id = "<?= $id_px ?>"
    var id_cty = "<?= $id_cty ?>"
    var kcxl_nguoiHoanThanh = "<?= $user_id ?>"
    var id_kho = "<?= $info_item['kcxl_khoXuat'] ?>"
    $.ajax({
      url: '../ajax/hoan_thanh_pxk.php',
      type: 'POST',
      // dataType: 'Json',
      data: {
        id_pxk: id,
        value: value,
        id_cty: id_cty,
        // trangThai: trangThai,
        kcxl_nguoiHoanThanh: kcxl_nguoiHoanThanh,
        listTable: listTable,
        id_kho: id_kho,
      },
      success: function(response) {
        var text = $('#check_export_note_success .p_add_succ').text('');
        var text_new = '';
        // var name = $("input[name='name_full_dv']").val();
        text_new += 'Hoàn thành phiếu xuất kho PXK-';
        text_new += '<strong>';
        text_new += '&nbsp' + id;
        text_new += '</strong>';
        text_new += '&nbspthành công!';
        text.append(text_new);
        $('#complete_export_note').hide();
        $('#check_export_note_success').show();
      }
    })
  });

  $('.button_delete').click(function() {
    var id = "<?= $id_px ?>"
    $('.delete_export_note').show();
    $('.delete_export_note .text_tb').text('')
    var text = 'Bạn có chắc chắn muốn xóa phiếu xuất kho<br><strong>PKK-' + id + '</strong>?'
    $('.delete_export_note .text_tb').append(text)
  })

  $('.button_accp_delete').click(function() {
    var value = '13';
    var id = "<?= $id_px ?>"
    var id_cty = "<?= $id_cty ?>"

    $.ajax({
      url: '../ajax/edit_detail_pxk.php',
      type: 'POST',
      // dataType: 'Json',
      data: {
        id_pxk: id,
        value: value,
        id_cty: id_cty,
      },
      success: function(response) {
        var text = $('#delete_export_note_success .p_add_succ').text('');
        var text_new = '';
        text_new += 'Xóa phiếu xuất kho PXK-';
        text_new += '<strong>';
        text_new += '&nbsp' + id;
        text_new += '</strong>';
        text_new += '&nbspthành công!';
        text.append(text_new);
        $('#delete_export_note').hide();
        $('#delete_export_note_success').show();
      }
    })
  });

  $('.button_refuse').click(function() {
    $('#refuse_export_note').show();
    var id = "<?= $id_px ?>"
    // console.log(id)
    $('.refuse_export_note .button_accp ').attr('data', id)
  })

  $('.btn_tuchoi').click(function() {
    var form_vali = $('#f_deny_ivt');
    form_vali.validate({
      errorPlacement: function(error, element) {
        error.appendTo(element.parents(".vali_li_do"));
        error.wrap("<span class='error'>");
      },
      rules: {
        validate_li_do: "required",
      },
      messages: {
        validate_li_do: "Nhập lí do từ chối."
      }
    });

    if (form_vali.valid() === true) {
      var value = '2';
      var id_phieu = $('.refuse_export_note .btn_tuchoi ').attr('data')
      var id_cty = "<?= $id_cty ?>"
      var kcxl_liDoTuChoi = $('textarea[name="validate_li_do"]').val()
      var kcxl_nguoiDuyet = "<?= $user_id ?>"
      console.log(id_phieu)
      console.log(kcxl_liDoTuChoi)
      $.ajax({
        url: '../ajax/edit_detail_pxk.php',
        type: 'POST',
        data: {
          id_pxk: id_phieu,
          value: value,
          id_cty: id_cty,
          kcxl_liDoTuChoi: kcxl_liDoTuChoi,
          kcxl_nguoiDuyet: kcxl_nguoiDuyet
        },
        success: function(data) {
          var text = $('#check_export_note_success .p_add_succ').text('');
          var text_new = '';
          text_new += 'Từ chối phiếu xuất kho PXK-';
          text_new += '<strong>';
          text_new += '&nbsp' + id_phieu;
          text_new += '</strong>';
          text_new += '&nbspthành công!';
          text.append(text_new);
          $('#refuse_export_note').hide();
          $('#check_export_note_success').show();
        }
      });
    }
  })

  $('.on_reload').click(function() {
    window.location.reload();
  })

  $('.button_export_excel').click(function() {
    var id_phieu = $(this).attr("data");
    window.location.href = '../Excel/chi_tiet_phieu_xuat.php?id=' + id_phieu;
  })
</script>

</html>