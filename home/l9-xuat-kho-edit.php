	<?php include("config1.php");

	$id_phieu = getValue("id", "int", "GET", "");
	// echo "<pre>";
	// print_r($id_phieu);
	// echo "</pre>";
	// die();

	if (isset($_SESSION['quyen']) && $_SESSION['quyen'] == 1) {
		$curl = curl_init();
		$token = $_COOKIE['acc_token'];
		curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/list_all_employee_of_company.php');
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));
		$response = curl_exec($curl);
		curl_close($curl);

		$data_list = json_decode($response, true);
		$data_list_nv = $data_list['data']['items'];
		$count = count($data_list_nv);
		$user_id = $_SESSION['com_id'];
	} elseif (isset($_SESSION['quyen']) && ($_SESSION['quyen'] == 2)) {
		$curl = curl_init();
		$token = $_COOKIE['acc_token'];
		curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/list_all_my_partner.php?get_all=true');
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));
		$response = curl_exec($curl);
		curl_close($curl);
		$data_list = json_decode($response, true);
		$data_list_nv = $data_list['data']['items'];
		$count = count($data_list_nv);
		$user_id = $_SESSION['ep_id'];
	}
	$newArr = [];
	for ($i = 0; $i < count($data_list_nv); $i++) {
		$value = $data_list_nv[$i];
		$newArr[$value["ep_id"]] = $value;
	}

	if (isset($_COOKIE['user']) && $_COOKIE['user'] != "" && $_COOKIE['role'] == 1) {
		$token = $_COOKIE['acc_token'];
		$curl = curl_init();
		$data = array();
		curl_setopt($curl, CURLOPT_POST, 1);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
		curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/user_info_company.php');
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));

		$response = curl_exec($curl);
		curl_close($curl);
		$data_tt = json_decode($response, true);
		$tt_user = $data_tt['data']['user_info_result'];
	} else if (isset($_COOKIE['user']) && $_COOKIE['user'] != "" && $_COOKIE['role'] == 2) {
		$token = $_COOKIE['acc_token'];
		$curl = curl_init();
		$data = array();
		curl_setopt($curl, CURLOPT_POST, 1);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
		curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/user_info_employee.php');
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));
		$response = curl_exec($curl);
		curl_close($curl);
		$data_tt = json_decode($response, true);
		$tt_user = $data_tt['data']['user_info_result'];
	}
	$id_cty = $tt_user['com_id'];
	$date = date('Y-m-d', time());
	$stt = 1;

	$item = new db_query("SELECT `kcxl_id`, `kcxl_soPhieu`, `kcxl_hinhThuc`, `kcxl_nguoiTao`, `kcxl_ngayTao`, `kcxl_donHang`, `kcxl_trangThai`, 
	`kcxl_ngayHoanThanh`, `kcxl_nhaCungCap`, `kcxl_nguoiThucHien`, `kcxl_phongBanNguoiGiao`, `kcxl_nguoiNhan`, `kcxl_phieuDieuChuyenKho`,
	`kcxl_phongBanNguoiNhan`, `kcxl_khoNhap`, `kcxl_ngayXuatKho`, `kcxl_khoXuat`, `kcxl_ngayYeuCauHoanThanh`, `kcxl_congTrinh`, 
	`kcxl_phieuYeuCau`, `kcxl_ghi_chu` 
	FROM `kho-cho-xu-li`
	WHERE `kcxl_id` = $id_phieu AND `kcxl_soPhieu` = 'PXK' AND `kcxl_check` = 1");

	$info_item = mysql_fetch_assoc($item->result);
	$id_nguoi_giao = $info_item['kcxl_nguoiThucHien'];
	$id_nguoi_nhan = $info_item['kcxl_nguoiNhan'];
	$id_kho_Xuat_Name = $info_item['kcxl_khoXuat'];
	$id_kho_Nhap_Name = $info_item['kcxl_khoNhap'];
	$cong_trinh1 = $info_item['kcxl_congTrinh'];


	$dsvt_tb = new db_query("SELECT `dsvt_id`, `dsvt_name`, `dsvt_donViTinh`, `dsvt_donGia`, `dsvt_hangSanXuat`, `dsvt_xuatXu`,  `dvt_name`, `hsx_name`, `xx_name` FROM `danh-sach-vat-tu`
	LEFT JOIN `don-vi-tinh` ON `dsvt_donViTinh` = `dvt_id` AND `dvt_check` = 1 
	LEFT JOIN `hang-san-xuat` ON `dsvt_hangSanXuat` = `hsx_id`  AND `hsx_check` = 1 
	LEFT JOIN `xuat-xu` ON `dsvt_xuatXu` = `xx_id`
	WHERE `dsvt_check` = 1 AND `dsvt_id_ct`= $id_cty");
	$arr_dsvt_tb = [];
	while ($row_dsvt = mysql_fetch_assoc($dsvt_tb->result)) {
		$arr_dsvt_tb[$row_dsvt["dsvt_id"]] = $row_dsvt;
	}
	$kho4 = new db_query("SELECT `kho_id`, `kho_maKho`, `kho_name` FROM `kho` WHERE `kho_id_ct` = $id_cty");
	$kho2 = new db_query("SELECT `kho_id`, `kho_maKho`, `kho_name` FROM `kho` WHERE `kho_id_ct` = $id_cty");
	$kho = new db_query("SELECT `kho_id`, `kho_maKho`, `kho_name` FROM `kho` WHERE `kho_id_ct` = $id_cty");
	$kho3 = new db_query("SELECT `kho_id`, `kho_maKho`, `kho_name` FROM `kho` WHERE `kho_id_ct` = $id_cty");
	$arr_kho3 = [];
	while ($row = mysql_fetch_assoc($kho3->result)) {
		$arr_kho3[$row["kho_id"]] = $row;
	}
	$pdk = new db_query("SELECT `kcxl`.`kcxl_id`, `kcxl`.`kcxl_nguoiTao`, `kcxl`.`kcxl_ngayTao`, `kcxl`.`kcxl_nguoiThucHien`, `kcxl`.`kcxl_phongBanNguoiGiao`, `kcxl`.`kcxl_nguoiNhan`, `kcxl`.`kcxl_phongBanNguoiNhan`, `kcxl`.`kcxl_khoNhap`, `kcxl`.`kcxl_khoXuat`, `kcxl`.`kcxl_ngayYeuCauHoanThanh`, `khoNhap`.`kho_name` as kho_nhap_name,`khoXuat`.`kho_name`as kho_xuat_name FROM `kho-cho-xu-li` kcxl
    LEFT JOIN `kho` `khoNhap` on `kcxl`.`kcxl_khoNhap` = `khoNhap`.`kho_id`       
    LEFT JOIN `kho` `khoXuat` on `kcxl`.`kcxl_khoXuat` = `khoXuat`.`kho_id`
    WHERE `kcxl_check` = 1 AND `kcxl_id_ct` = $id_cty AND `kcxl_trangThai` = 7 AND `kcxl_soPhieu` = 'ĐCK' ");


	$arr_pdk = [];
	while ($row = mysql_fetch_assoc($pdk->result)) {
		$arr_pdk[$row["kcxl_id"]] = $row;
	}

	$kho_Xuat_Name = new db_query("SELECT `kho_id`, `kho_maKho`, `kho_name` FROM `kho` WHERE `kho_id_ct` = $id_cty AND `kho_id` = $id_kho_Xuat_Name");
	$info_Kho_Xuat_Name = mysql_fetch_assoc($kho_Xuat_Name->result);
	if ($id_kho_Nhap_Name != '') {
		$kho_Nhap_Name = new db_query("SELECT `kho_id`, `kho_maKho`, `kho_name` FROM `kho` WHERE `kho_id_ct` = $id_cty AND `kho_id` = $id_kho_Nhap_Name");
		$info_Kho_Nhap_Name = mysql_fetch_assoc($kho_Nhap_Name->result);
	}

	$curl = curl_init();
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($curl, CURLOPT_URL, "https://phanmemquanlycungung.timviec365.vn/api/phieu_yc_vat_tu.php");
	curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
	curl_setopt($curl, CURLOPT_POSTFIELDS, [
		'id_com' => $id_cty,
	]);
	$response = curl_exec($curl);
	curl_close($curl);
	$emp0 = json_decode($response, true);
	$emp = $emp0['data']['items'];
	$emp1 = [];
	for ($i = 0; $i < count($emp); $i++) {
		$pyc = $emp[$i];
		$emp1[$pyc["id"]] = $pyc;
	}

	// API vật tư theo phiếu yêu cầu
	$curl_vt_yc = curl_init();
	curl_setopt($curl_vt_yc, CURLOPT_POST, 1);
	curl_setopt($curl_vt_yc, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($curl_vt_yc, CURLOPT_URL, "https://phanmemquanlycungung.timviec365.vn/api/vat_tu_yc.php");
	curl_setopt($curl_vt_yc, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
	curl_setopt($curl_vt_yc, CURLOPT_POSTFIELDS, [
		'id_phieu' => $info_item['kcxl_phieuYeuCau'],
	]);
	$response_vt_yc = curl_exec($curl_vt_yc);
	curl_close($curl_vt_yc);
	$emp_json = json_decode($response_vt_yc, true);
	$emp_arr = $emp_json['data']['items'];
	$emp_vt_yc = [];
	for ($i = 0; $i < count($emp_arr); $i++) {
		$vt_yc = $emp_arr[$i];
		$emp_vt_yc[$vt_yc["id"]] = $vt_yc;
	}

	$curl = curl_init();
	curl_setopt($curl, CURLOPT_POST, 1);
	curl_setopt($curl, CURLOPT_URL, 'https://phanmemquanlycongtrinh.timviec365.vn/api/congtrinh.php');
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
	curl_setopt($curl, CURLOPT_POSTFIELDS, [
		'id_com' => $id_cty,
	]);
	$response = curl_exec($curl);
	curl_close($curl);
	$data_list = json_decode($response, true);
	$list_phieu_vt = $data_list['data']['items'];
	$list_phieu_vt1 = [];
	for ($j = 0; $j < count($list_phieu_vt); $j++) {
		$ctr = $list_phieu_vt[$j];
		$list_phieu_vt1[$ctr['ctr_id']] = $ctr;
	}

	// echo "<pre>";
	// print_r($list_phieu_vt1[trim($info_item['kcxl_ngayXuatKho'])]);
	// echo "</pre>";
	// die();
	$curl_dh = curl_init();
	curl_setopt($curl_dh, CURLOPT_POST, 1);
	curl_setopt($curl_dh, CURLOPT_URL, 'https://phanmemquanlycungung.timviec365.vn/api/don_hang.php');
	curl_setopt($curl_dh, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($curl_dh, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
	curl_setopt($curl_dh, CURLOPT_POSTFIELDS, [
		'com_id' => $id_cty,
		'phan_loai' => 2,
	]);
	$response_dh = curl_exec($curl_dh);
	curl_close($curl_dh);
	$data_list_dh = json_decode($response_dh, true);
	$list_dh = $data_list_dh['data']['items'];
	$data_dh_2 = [];
	for ($i = 0; $i < count($list_dh); $i++) {
		$dh = $list_dh[$i];
		$data_dh_2[$dh["id"]] = $dh;
	}

	// echo "<pre>";
	// print_r($data_dh_2);
	// echo "</pre>";
	// die();
	// API vật tư đơn hàng
	$curl_vtdh = curl_init();
	curl_setopt($curl_vtdh, CURLOPT_POST, 1);
	curl_setopt($curl_vtdh, CURLOPT_URL, 'https://phanmemquanlycungung.timviec365.vn/api/vat_tu_dh.php');
	curl_setopt($curl_vtdh, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($curl_vtdh, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
	curl_setopt($curl_vtdh, CURLOPT_POSTFIELDS, [
		'com_id' => $id_cty,
		'dh_id' =>  $info_item['kcxl_donHang'],
	]);
	$response_vtdh = curl_exec($curl_vtdh);
	curl_close($curl_vtdh);
	$data_list_vtdh = json_decode($response_vtdh, true);
	$list_vtdh = $data_list_vtdh['data']['items'];

	$list_vtdh1 = [];
	for ($j = 0; $j < count($list_vtdh); $j++) {
		$list_vtdh_p = $list_vtdh[$j];
		$list_vtdh1[$list_vtdh_p["id"]] = $list_vtdh_p;
	}

	// echo "<pre>";
	// print_r($list_phieu_vt1[5]['ctr_name']);
	// echo "</pre>";
	// die();
	?>

	<!DOCTYPE html>
	<html lang="vi">

	<head>
		<title>Xuất kho</title>
		<meta charset="UTF-8">
		<meta name="robots" content="noindex,nofollow" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="stylesheet" type="text/css" href="../css/select2.min.css?v=<?= $ver ?>">

		<link rel="stylesheet" type="text/css" href="../css/style.css?v=<?= $ver ?>">
		<link rel="stylesheet" type="text/css" href="../css/style_h.css?v=<?= $ver ?>">
		<link rel="stylesheet" type="text/css" href="../css/style_n.css?v=<?= $ver ?>">
	</head>

	<body class="seclec2_radius">
		<div class="main_wrapper_all">
			<div class="wapper_all">
				<?php include('../includes/sidebar.php');  ?>
			</div>
			<div class="main_overview" id="main_overview">
				<div class="header_menu_overview d_flex align_c space_b">
					<div class="text_link_header_back" style="display: block;">
						<div class=" d_flex align_c">
							<a href="/xuat-kho.html" class="cursor_p">
								<img src="../images/back_item_g.png" alt="">
							</a>
							<p class="color_grey line_16 font_s14 ml_10">Xuất kho / Chỉnh sửa </p>
						</div>
					</div>
					<img class="open_sidebar_w" src="../images/open_sidebar_w.png" alt="" onclick="toggle('main_sidebar')">
					<?php include('../includes/header.php');  ?>
				</div>
				<div class="body_equipment_supplies">
					<div class="text_link_body_back" style="display: none;">
						<div class=" d_flex align_c mb_15">
							<a href="/xuat-kho.html" class="cursor_p">
								<img src="../images/back_item_g.png" alt="">
							</a>
							<p class="color_grey line_16 font_s14 ml_10">Thông tin vật tư thiết bị / Danh sá ch vật tư thiết bị /
								Chỉnh sửa</p>
						</div>
					</div>
					<form action="" method="POST" name="create_export_kho" id="create_export_kho">
						<div>
							<div class="body_equipment_supplies_create ">
								<div class="header_body_b color_white line_h19 font_s16 font_wB">
									Thêm mới phiếu xuất kho
								</div>
								<div class="body_create_export_kho">
									<div class="d_flex align_c" id="block32">
										<div class="width_create_choose mr_10">
											<p class="font_s15 line_h18 font_w500 color_grey mb_5">Số phiếu</p>
											<input class="input_value_grey font_14 line_h16" type="text" value="PXK - <?= $info_item['kcxl_id'] ?>" readonly>
										</div>
										<div class="width_create_choose ml_10" id="">
											<p class="font_s15 line_h18 font_w500 color_grey">Hình thức xuất kho</p>
											<div class="select_create mt_5">
												<select class="select_create select_export_form" name="select_export_form" id="select_export_form">
													<option value="XK1" <?= ($info_item['kcxl_hinhThuc'] == "XK1") ? "selected" : "" ?>>Xuất thi công</option>
													<option value="XK2" <?= ($info_item['kcxl_hinhThuc'] == "XK2") ? "selected" : "" ?>>Xuất điều chuyển</option>
													<option value="XK3" <?= ($info_item['kcxl_hinhThuc'] == "XK3") ? "selected" : "" ?>>Xuất theo yêu cầu vật tư</option>
													<option value="XK4" <?= ($info_item['kcxl_hinhThuc'] == "XK4") ? "selected" : "" ?>>Xuất theo đơn hàng bán vật tư</option>
													<option value="" <?= ($info_item['kcxl_hinhThuc'] == "") ? "selected" : "" ?>>Xuất khác</option>
												</select>
											</div>
										</div>
									</div>
									<div class="d_flex align_c mt_15" id="block32">
										<div class="width_create_choose mr_10">
											<p class="font_s15 line_h18 font_w500 color_grey">Người tạo</p>
											<?php if ($info_item['kcxl_nguoiTao'] != 0) { ?>
												<input class="input_value_grey font_14 line_h16 mt_5 color_grey nguoi_tao" readonly type="text" value="<?= $newArr[$info_item['kcxl_nguoiTao']]['ep_name'] ?>" data="<?= $info_item['kcxl_nguoiTao'] ?>">
											<? } ?>
											<?php if ($info_item['kcxl_nguoiTao'] == 0) { ?>
												<input class="input_value_grey font_14 line_h16 mt_5 color_grey nguoi_tao" readonly type="text" value="<?= $tt_user['com_name'] ?>" data="0">
											<? } ?>
										</div>
										<div class="width_create_choose ml_10" id="block33">
											<p class="font_s15 line_h18 font_w500 color_grey">Ngày tạo</p>
											<input class="input_value_grey font_14 line_h16 mt_5 color_grey ngay_tao" readonly type="text" value="<?= $info_item['kcxl_ngayTao'] ?>">
										</div>
									</div>

									<div class="selected_construction_export display_none" <?php if ($info_item['kcxl_hinhThuc'] == "XK1") { ?>style="display: block;" <?php } ?>>
										<div class="d_flex align_c mt_15" id="block32">
											<div class="width_create_choose mr_10 request_form ">
												<div class="d_flex">
													<p class="font_s15 line_h18 font_w500 color_grey">Phiếu yêu cầu</p>
													<span class="color_red alert_red">*</span>
												</div>
												<div class="select_create select_create_1 mt_5">
													<select class="select_create select_request_form" name="select_request_form" id="select_request_form">
														<option></option>
														<? foreach ($emp1 as $key => $value) { ?>
															<option value="<?= $key ?>" <?= ($key == $info_item['kcxl_phieuYeuCau']) ? "selected" : "" ?>><?= 'PYC' . ' ' . '-' . ' ' . $value['id'] ?></option>
														<? } ?>
													</select>
												</div>
											</div>
											<div class="width_create_choose ml_10" id="block33">
												<div class="d_flex align_c" id="block28">
													<div class="w_100 " id="block34">
														<p class="font_s15 line_h18 font_w500 color_grey">Trạng thái</p>
														<div class="select_create mt_5">
															<select class="select_create select_status" name="select_status1" id="select_status1" disabled>
																<option value="1" <?= $info_item['kcxl_trangThai'] == 1 ? "selected" : "" ?>>Khởi tạo</option>
																<option value="3" <?= $info_item['kcxl_trangThai'] == 2 ? "selected" : "" ?>>Từ chối</option>
															</select>
														</div>
													</div>
													<div class="w_50 ml_10 date_accses" id="block35" style="display:none">
														<div class="d_flex">
															<p class="font_s15 line_h18 font_w500 color_grey">Ngày hoàn thành</p>
															<span class="color_red alert_red">*</span>
														</div>
														<div class="select_create1 mt_5 select_create">
															<input class="input_value_w" name="date_accses_export1" id="date_accses_export1" type="date">
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="d_flex align_c mt_15" id="block32">
											<div class="width_create_choose mr_10">
												<p class="font_s15 line_h18 font_w500 color_grey">Công trình</p>
												<input class="input_value_grey font_14 line_h16 mt_5 cong_trinh" readonly type="text" placeholder="Hiện tên công trình từ phiếu yêu cầu" value="<?= $list_phieu_vt1[trim($info_item['kcxl_congTrinh'])]['ctr_name'] ?>">
												<input class="input_value_grey font_14 line_h16 mt_5 id_cong_trinh" readonly type="hidden" placeholder="Hiện tên công trình từ phiếu yêu cầu" value="<?= $list_phieu_vt1[trim($info_item['kcxl_congTrinh'])]['ctr_id'] ?>">
											</div>
											<div class="width_create_choose ml_10" id="block33">
												<p class="font_s15 line_h18 font_w500 color_grey">Ngày yêu cầu hoàn thành</p>
												<input class="input_value_grey font_14 line_h16 mt_5 ngay_ht_yc" readonly type="date" placeholder="Hiện ngày yêu cầu theo phiếu yêu cầu" value="<?= $info_item['kcxl_ngayYeuCauHoanThanh'] ?>">
											</div>
										</div>
										<div class="d_flex align_c mt_15" id="block32">
											<div class="width_create_choose mr_10">
												<p class="font_s15 line_h18 font_w500 color_grey">Người giao hàng</p>
												<div class="select_create mt_5">
													<select class="select_create select_deliver" name="select_deliver">
														<option value="">Chọn người giao</option>
														<? foreach ($newArr as $key => $value) { ?>
															<option value="<?= $key ?>" <?= ($key == $info_item['kcxl_nguoiThucHien']) ? "selected" : "" ?>><?= $value['ep_name'] . ' ' . '-' . ' ' . $value['dep_name'] ?></option>
														<? } ?>
													</select>
												</div>
											</div>
											<div class="width_create_choose ml_10" id="block33">
												<p class="font_s15 line_h18 font_w500 color_grey">Phòng ban</p>
												<input class="input_value_grey font_14 line_h16 mt_5 phong_ban_giao" readonly type="text" placeholder="Hiển thị phòng ban người giao" value="<?= $newArr[$id_nguoi_giao]['dep_name'] ?>">
												<input class="input_value_grey font_14 line_h16 mt_5 id_phong_ban_giao" readonly type="hidden" placeholder="Hiển thị phòng ban người giao" value="<?= $newArr[$id_nguoi_giao]['dep_id'] ?>">
											</div>
										</div>
										<div class="d_flex align_c mt_15" id="block32">
											<div class="width_create_choose mr_10">
												<p class="font_s15 line_h18 font_w500 color_grey">Người nhận</p>
												<?php if ($id_nguoi_nhan != 0) { ?>
													<input class="input_value_grey font_14 line_h16 mt_5 nguoi_nhan" readonly type="text" placeholder="Hiển thị tên người nhận từ đơn hàng" value="<?= $newArr[$id_nguoi_nhan]['ep_name'] ?>">
													<input class="input_value_grey font_14 line_h16 mt_5 id_nguoi_nhan" readonly type="hidden" placeholder="Hiển thị tên người nhận từ đơn hàng" value="<?= $id_nguoi_nhan ?>">
												<? } ?>
												<?php if ($id_nguoi_nhan == 0) { ?>
													<input class="input_value_grey font_14 line_h16 mt_5 color_grey nguoi_nhan" readonly type="text" value="<?= $tt_user['com_name'] ?>" data="0">
													<input class="input_value_grey font_14 line_h16 mt_5 color_grey id_nguoi_nhan" readonly type="hidden" value="0" data="0">
												<? } ?>
											</div>
											<div class="width_create_choose ml_10" id="block33">
												<p class="font_s15 line_h18 font_w500 color_grey">Phòng ban</p>
												<input class="input_value_grey font_14 line_h16 mt_5 phong_ban_nhan" readonly type="text" placeholder="Hiển thị phòng ban người nhận từ đơn hàng" value="<?= $newArr[$id_nguoi_nhan]['dep_name'] ?>">
												<input class="input_value_grey font_14 line_h16 mt_5 id_phong_ban_nhan" readonly type="hidden" placeholder="Hiển thị phòng ban người nhận từ đơn hàng" value="<?= $newArr[$id_nguoi_nhan]['dep_id'] ?>">
											</div>
										</div>
										<div class="d_flex align_c mt_15" id="block32">
											<div class="width_create_choose mr_10 export_kho">
												<div class="d_flex">
													<p class="font_s15 line_h18 font_w500 color_grey">Kho xuất</p>
													<span class="color_red alert_red">*</span>
												</div>
												<div class="select_create">
													<select class="select_create select_export_kho" name="select_export_kho1" id="select_export_kho1">
														<option value="">Chọn kho xuất</option>
														<? while (($selected = mysql_fetch_assoc($kho2->result))) { ?>
															<option value="<?= $selected['kho_id'] ?>" <?= ($selected['kho_id'] == $info_item['kcxl_khoXuat']) ? "selected" : "" ?>><?= $selected['kho_name'] ?></option>
														<? } ?>
													</select>
												</div>
											</div>
											<div class="width_create_choose ml_10 date_export_kho" id="block33">
												<div class="d_flex">
													<p class="font_s15 line_h18 font_w500 color_grey">Ngày xuất kho</p>
													<span class="color_red alert_red">*</span>
												</div>
												<input class="input_value_w" name="selected_date_export_kho" id="selected_date_export_kho" type="date" value="<?= $info_item['kcxl_ngayXuatKho'] ?>">
											</div>
										</div>
									</div>

									<div class="selected_export_transfer display_none" <?php if ($info_item['kcxl_hinhThuc'] == "XK2") { ?>style="display: block;" <?php } ?>>
										<div class="d_flex mt_15" id="block32">
											<div class="width_create_choose mr_10 transfer_slip">
												<div class="d_flex">
													<p class="font_s15 line_h18 font_w500 color_grey">Phiếu điều chuyển</p>
													<span class="color_red alert_red">*</span>
												</div>
												<div class="select_create select_create2 mt_5">
													<select class="select_create select_transfer_slip" name="select_transfer_slip" id="select_transfer_slip">
														<option value="">Chọn phiếu điều chuyển</option>
														<? foreach ($arr_pdk as $key => $value) { ?>
															<option value="<?= $value['kcxl_id'] ?>" <?= ($value['kcxl_id'] == $info_item['kcxl_phieuDieuChuyenKho']) ? "selected" : "" ?>><?= 'ĐCK' . ' ' . '-' . ' ' . $value['kcxl_id'] . ' ' . 'chuyển từ' . ' ' . $value['kho_xuat_name'] . ' ' . 'đến' . ' ' . $value['kho_nhap_name'] ?></option>
														<? } ?>
													</select>
												</div>
											</div>
											<div class="width_create_choose ml_10" id="block33">
												<div class="d_flex align_c" id="block28">
													<div class="w_100 " id="block34">
														<p class="font_s15 line_h18 font_w500 color_grey">Trạng thái</p>
														<div class="select_create mt_5">
															<select class="select_create select_status" name="select_status2" id="select_status2" disabled>
																<option value="1" <?= $info_item['kcxl_trangThai'] == 1 ? "selected" : "" ?>>Khởi tạo</option>
																<option value="3" <?= $info_item['kcxl_trangThai'] == 2 ? "selected" : "" ?>>Từ chối</option>
															</select>
														</div>
													</div>
													<div class="w_50 ml_10 date_accses" id="block35" style="display:none">
														<div class="d_flex">
															<p class="font_s15 line_h18 font_w500 color_grey">Ngày hoàn thành</p>
															<span class="color_red alert_red">*</span>
														</div>
														<div class="select_create mt_5 date_accses_2">
															<input class="input_value_w" type="date" name="date_accses_export_2" id="date_accses_export_2">
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="d_flex mt_15 " id="block32">
											<div class="width_create_choose mr_10">
												<p class="font_s15 line_h18 font_w500 color_grey">Kho xuất</p>
												<input class="input_value_grey font_14 line_h16 mt_5 kho_xuat_1" readonly type="text" placeholder="Hiển thị kho xuất từ phiếu điều chuyển" value="<?= $info_Kho_Xuat_Name['kho_name'] ?>">
												<input class="input_value_grey font_14 line_h16 mt_5 id_kho_xuat_1" name="id_kho_xuat_1" readonly type="hidden" placeholder="Hiển thị kho xuất từ phiếu điều chuyển" value="<?= $info_item['kcxl_khoXuat'] ?>">
											</div>
											<div class="width_create_choose ml_10 date_xuat_2" id="block33">
												<div class="d_flex">
													<p class="font_s15 line_h18 font_w500 color_grey">Ngày xuất kho</p>
													<span class="color_red alert_red">*</span>
												</div>
												<div class="">
													<input class="input_value_w mt_5 " name="name_date_xuat_2" id="name_date_xuat_2" type="date" value="<?= $info_item['kcxl_ngayXuatKho'] ?>">
												</div>
											</div>
										</div>
										<div class="d_flex align_c mt_15" id="block32">
											<div class="width_create_choose mr_10">
												<p class="font_s15 line_h18 font_w500 color_grey">Kho nhập</p>
												<input class="input_value_grey font_14 line_h16 mt_5 kho_nhap_1" readonly type="text" placeholder="Hiển thị kho nhập từ phiếu điều chuyển" value="<?= $info_Kho_Nhap_Name['kho_name'] ?>">
												<input class="input_value_grey font_14 line_h16 mt_5 id_kho_nhap_1" name="id_kho_nhap_1" readonly type="hidden" placeholder="Hiển thị kho nhập từ phiếu điều chuyển" value="<?= $info_item['kcxl_khoNhap'] ?>">
											</div>
											<div class="width_create_choose ml_10" id="block33">
												<div class="d_flex">
													<p class="font_s15 line_h18 font_w500 color_grey">Ngày yêu cầu hoàn thành điều chuyển</p>
												</div>
												<input class="input_value_grey mt_5 ngay_yeu_cau_1" name="ngay_yeu_cau_1" readonly type="date" placeholder="Hiển thị ngày yêu cầu hoàn thành điều chuyển" value="<?= $info_item['kcxl_ngayYeuCauHoanThanh'] ?>">
											</div>
										</div>
										<div class="d_flex align_c mt_15" id="block32">
											<div class="width_create_choose mr_10">
												<p class="font_s15 line_h18 font_w500 color_grey">Người giao hàng</p>
												<input class="input_value_grey font_14 line_h16 mt_5 nguoi_giao_1" readonly type="text" placeholder="Hiển thị tên người giao hàng từ phiếu điều chuyển" value="<?= $newArr[$id_nguoi_giao]['ep_name'] ?>">
												<input class=" input_value_grey font_14 line_h16 mt_5 id_nguoi_giao_1" name="id_nguoi_giao_1" readonly type="hidden" placeholder="Hiển thị tên người giao hàng từ phiếu điều chuyển" value="<?= $info_item['kcxl_phongBanNguoiGiao'] ?>">
											</div>
											<div class="width_create_choose ml_10" id="block33">
												<div class="d_flex">
													<p class="font_s15 line_h18 font_w500 color_grey">Phòng ban</p>
												</div>
												<input class="input_value_grey mt_5 phong_ban_giao_1" readonly type="text" placeholder="Hiển thị phòng ban người giao" value="<?= $newArr[$id_nguoi_giao]['dep_name'] ?>">
												<input class="input_value_grey mt_5 id_phong_ban_giao_1" readonly type="hidden" placeholder="Hiển thị phòng ban người giao" value="<?= $newArr[$id_nguoi_giao]['dep_id'] ?>">
											</div>
										</div>
										<div class="d_flex align_c mt_15" id="block32">
											<div class="width_create_choose mr_10">
												<p class="font_s15 line_h18 font_w500 color_grey">Người nhận</p>
												<input class="input_value_grey font_14 line_h16 mt_5 nguoi_nhan_1" readonly type="text" placeholder="Hiển thị tên người nhận từ phiếu điều chuyển" value="<?= $newArr[$id_nguoi_nhan]['ep_name'] ?>">
												<input class="input_value_grey font_14 line_h16 mt_5 id_nguoi_nhan_1" readonly type="hidden" placeholder="Hiển thị tên người nhận từ phiếu điều chuyển" value="<?= $newArr[$id_nguoi_nhan]['ep_id'] ?>">
											</div>
											<div class="width_create_choose ml_10" id="block33">
												<div class="d_flex">
													<p class="font_s15 line_h18 font_w500 color_grey">Phòng ban</p>
												</div>
												<input class="input_value_grey mt_5 phong_ban_nhan_1" readonly type="text" placeholder="Hiển thị phòng ban người nhận" value="<?= $newArr[$id_nguoi_nhan]['dep_name'] ?>">
												<input class="input_value_grey mt_5 id_phong_ban_nhan_1" readonly type="hidden" placeholder="Hiển thị phòng ban người nhận" value="<?= $newArr[$id_nguoi_nhan]['dep_id'] ?>">
											</div>
										</div>
									</div>

									<div class="selected_export_on_demand display_none" <?php if ($info_item['kcxl_hinhThuc'] == "XK3") { ?>style="display: block;" <?php } ?>>
										<div class="d_flex align_c mt_15" id="block32">
											<div class="width_create_choose mr_10">
												<div class="d_flex">
													<p class="font_s15 line_h18 font_w500 color_grey">Phiếu yêu cầu</p>
													<span class="color_red alert_red">*</span>
												</div>
												<div class="select_create request_form1 mt_5">
													<select class="select_create select_request_form1" name="select_request_form1" id="select_request_form1">
														<option></option>
														<? foreach ($emp1 as $key => $value) { ?>
															<option value="<?= $key ?>" <?= ($key == $info_item['kcxl_phieuYeuCau']) ? "selected" : "" ?>><?= 'PYC' . ' ' . '-' . ' ' . $value['id'] ?></option>
														<? } ?>
													</select>
													<!-- cái chỗ chọn phiếu yêu cầu đây -->
												</div>
											</div>
											<div class="width_create_choose ml_10" id="block33">
												<div class="d_flex align_c" id="block28">
													<div class="w_100 " id="block34">
														<p class="font_s15 line_h18 font_w500 color_grey">Trạng thái</p>
														<div class="select_create mt_5">
															<select class="select_create select_status" name="select_status3" id="select_status3" disabled>
																<option value="1" <?= $info_item['kcxl_trangThai'] == 1 ? "selected" : "" ?>>Khởi tạo</option>
																<option value="3" <?= $info_item['kcxl_trangThai'] == 2 ? "selected" : "" ?>>Từ chối</option>
															</select>
														</div>
													</div>
													<div class="w_50 ml_10 date_accses" id="block35" style="display:none">
														<div class="d_flex">
															<p class="font_s15 line_h18 font_w500 color_grey">Ngày hoàn thành</p>
															<span class="color_red alert_red">*</span>
														</div>
														<div class="select_create mt_5 date_accses_3">
															<input class="input_value_w" type="date" name="date_accses_export_3" id="date_accses_export_3" value="<?= $info_item['kcxl_kcxl_ngayHoanThanh'] ?>">
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="d_flex mt_15" id="block32">
											<div class="width_create_choose mr_10">
												<p class="font_s15 line_h18 font_w500 color_grey">Kho xuất</p>
												<input class="input_value_grey mt_5 kho_xuat_3" readonly type="text" placeholder="Hiển thị kho xuất theo phiếu yêu cầu" value="<?= $info_Kho_Xuat_Name['kho_name'] ?>">
												<input class="input_value_grey mt_5 id_kho_xuat_3" name="id_kho_xuat_3" readonly type="hidden" placeholder="Hiển thị kho xuất theo phiếu yêu cầu" value="<?= $info_item['kcxl_khoXuat'] ?>">
											</div>
											<div class="width_create_choose ml_10 date_xuat_3" id="block33">
												<div class="d_flex">
													<p class="font_s15 line_h18 font_w500 color_grey">Ngày xuất kho</p>
													<span class="color_red alert_red">*</span>
												</div>
												<input class="input_value_w mt_5 ngay_xuat_3" id="ngay_xuat_3" name="ngay_xuat_3" type="date" value="<?= $info_item['kcxl_ngayXuatKho'] ?>">
											</div>
										</div>
										<div class="d_flex align_c mt_15" id="block32">
											<div class="width_create_choose mr_10">
												<!-- <p class="font_s15 line_h18 font_w500 color_grey">Kho nhập</p>
 											<input class="input_value_grey mt_5" readonly type="date" placeholder="Hiển thị kho nhập theo phiếu yêu cầu"> -->
											</div>
											<div class="width_create_choose ml_10" id="block33">
												<p class="font_s15 line_h18 font_w500 color_grey">Ngày yêu cầu hoàn thành</p>
												<input class="input_value_grey mt_5 date_yc_3" readonly type="date" placeholder="Hiển thị ngày yêu cầu hoàn thành cung ứng" value="<?= $info_item['kcxl_ngayYeuCauHoanThanh'] ?>">
											</div>
										</div>
										<div class="d_flex align_c mt_15" id="block32">
											<div class="width_create_choose mr_10">
												<p class="font_s15 line_h18 font_w500 color_grey">Người giao hàng</p>
												<div class="select_create mt_5">
													<select class="select_create select_deliver1" name="select_deliver1">
														<option value="">Chọn người giao</option>
														<? foreach ($newArr as $key => $value) { ?>
															<option value="<?= $key ?>" <?= ($key == $info_item['kcxl_nguoiThucHien']) ? "selected" : "" ?>><?= $value['ep_name'] . ' ' . '-' . ' ' . $value['dep_name'] ?></option>
															<? } ?>>
													</select>
												</div>
											</div>
											<div class="width_create_choose ml_10" id="block33">
												<p class="font_s15 line_h18 font_w500 color_grey">Phòng ban</p>
												<input class="input_value_grey mt_5 phong_ban_giao1" readonly type="text" placeholder="Hiển thị phòng ban người giao" value="<?= $newArr[$id_nguoi_giao]['dep_name'] ?>">
												<input class="input_value_grey mt_5 id_phong_ban_giao1" readonly type="hidden" placeholder="Hiển thị phòng ban người giao" value="<?= $newArr[$id_nguoi_giao]['dep_id'] ?>">
											</div>
										</div>
										<div class="d_flex align_c mt_15" id="block32">
											<div class="width_create_choose mr_10">
												<p class="font_s15 line_h18 font_w500 color_grey">Người nhận</p>
												<div class="select_create mt_5">
													<select class="select_create select_receiver1" name="select_receiver1">
														<option value="">Chọn người nhận</option>
														<? foreach ($newArr as $key => $value) { ?>
															<option value="<?= $key ?>" <?= ($key == $info_item['kcxl_nguoiNhan']) ? "selected" : "" ?>><?= $value['ep_name'] . ' ' . '-' . ' ' . $value['dep_name'] ?></option>
															<? } ?>>
													</select>
												</div>
											</div>
											<div class="width_create_choose ml_10" id="block33">
												<p class="font_s15 line_h18 font_w500 color_grey">Phòng ban</p>
												<input class="input_value_grey mt_5 phong_ban_nhan1" readonly type="text" placeholder="Hiển thị phòng ban người nhận" value="<?= $newArr[$id_nguoi_nhan]['dep_name'] ?>">
												<input class="input_value_grey mt_5 id_phong_ban_nhan1" readonly type="hidden" placeholder="Hiển thị phòng ban người nhận" value="<?= $newArr[$id_nguoi_nhan]['dep_id'] ?>">
											</div>
										</div>
									</div>

									<div class="selected_export_by_order display_none" <?php if ($info_item['kcxl_hinhThuc'] == "XK4") { ?>style="display: block;" <?php } ?>>
										<div class="d_flex align_c mt_15" id="block32">
											<div class="width_create_choose mr_10">
												<div class="d_flex">
													<p class="font_s15 line_h18 font_w500 color_grey">Đơn hàng</p>
													<span class="color_red alert_red">*</span>
												</div>
												<div class="select_create choose_select_order mt_5">
													<select class="select_create select_order" name="select_order" id="select_order">
														<option></option>
														<? foreach ($data_dh_2 as $key => $value) { ?>
															<option value="<?= $value['id'] ?>" <?= ($value['id'] == $info_item['kcxl_donHang']) ? "selected" : "" ?>><?= 'ĐH' . ' ' . '-' . ' ' . $value['id'] ?></option>
														<? } ?>
													</select>
												</div>
											</div>
											<div class="width_create_choose ml_10" id="block33">
												<div class="d_flex align_c" id="block28">
													<div class="w_100 " id="block34">
														<p class="font_s15 line_h18 font_w500 color_grey">Trạng thái</p>
														<div class="select_create mt_5">
															<select class="select_create select_status" name="select_status4" id="select_status4" disabled>
																<option value="1" <?= $info_item['kcxl_trangThai'] == 1 ? "selected" : "" ?>>Khởi tạo</option>
																<option value="3" <?= $info_item['kcxl_trangThai'] == 2 ? "selected" : "" ?>>Từ chối</option>
															</select>
														</div>
													</div>
													<div class="w_50 ml_10 date_accses " id="block35" style="display:none">
														<div class="d_flex">
															<p class="font_s15 line_h18 font_w500 color_grey">Ngày hoàn thành</p>
															<span class="color_red alert_red">*</span>
														</div>
														<div class="select_create mt_5 date_accses_4">
															<input class="input_value_w" type="date" name="date_accses_export_4" id="date_accses_export_4" value="<?= $info_item['kcxl_ngayHoanThanh'] ?>">
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="d_flex align_c mt_15" id="block32">
											<div class="width_create_choose mr_10 choose_select_export_kho_2">
												<div class="d_flex">
													<p class="font_s15 line_h18 font_w500 color_grey">Kho xuất</p>
													<span class="color_red alert_red">*</span>
												</div>
												<div class="select_create">
													<select class="select_create select_export_kho" name="select_export_kho2" id="select_export_kho2">
														<? while ($item = mysql_fetch_assoc($kho4->result)) { ?>
															<option value="<?= $item['kho_id'] ?>" <?= ($item['kho_id'] == $info_item['kcxl_khoNhap']) ? "selected" : "" ?>><?= $item['kho_name'] ?></option>
														<? } ?>
													</select>
												</div>
											</div>
											<div class="width_create_choose ml_10 select_date_xuat_4" id="block33">
												<div class="d_flex">
													<p class="font_s15 line_h18 font_w500 color_grey">Ngày xuất kho</p>
													<span class="color_red alert_red">*</span>
												</div>
												<input class="input_value_w mt_5 date_xuat_4" type="date" name="date_xuat_4" id="date_xuat_4" value="<?= $info_item['kcxl_ngayXuatKho']; ?>">
											</div>
										</div>
										<div class="d_flex align_c mt_15" id="block32">
											<div class="width_create_choose mr_10">
												<p class="font_s15 line_h18 font_w500 color_grey">Thời hạn hoàn thành</p>
												<input class="input_value_grey mt_5 time_ht" readonly type="text" placeholder="Hiển thị ngày hoàn thành từ đơn hàng" value="<?= $info_item['kcxl_ngayYeuCauHoanThanh']; ?>">
											</div>
											<div class="width_create_choose ml_10">
											</div>
										</div>
										<div class="d_flex align_c mt_15" id="block32">
											<div class="width_create_choose mr_10">
												<div class="d_flex align_c space_b">
													<div class="d_flex">
														<p class="font_s15 line_h18 font_w500 color_grey">Người xuất kho</p>
														<span class="color_red alert_red">*</span>
													</div>
													<div class="d_flex align_c">
														<img src="../images/create.png" alt="">
														<p class="font_s14 line_h16 color_blue ml_5">Thêm nhân viên</p>
													</div>
												</div>
												<div class="select_create mt_5 choose_select_human_export">
													<select class="select_create select_human_export" name="select_human_export" id="select_human_export">
														<option value="">Chọn người xuất kho</option>
														<? foreach ($data_list_nv as $item) { ?>
															<option value="<?= $item['ep_id'] ?>" <?= ($item['ep_id'] == $info_item['kcxl_nguoiThucHien']) ? "selected" : "" ?>><?= $item['ep_name'] . "&nbsp-&nbsp" . $item['dep_name']; ?></option>
														<? } ?>
													</select>
												</div>
											</div>
											<div class="width_create_choose ml_10" id="block33">
												<p class="font_s15 line_h18 font_w500 color_grey">Phòng ban</p>
												<input class="input_value_grey mt_5 ten_pb_4" readonly type="text" placeholder="Hiển thị phòng ban người giao" value="<?= $newArr[$id_nguoi_giao]['dep_name'] ?>">
												<input class="input_value_grey mt_5 id_pb_4" readonly type="hidden" placeholder="Hiển thị phòng ban người giao" value="<?= $info_item['kcxl_phongBanNguoiGiao'] ?>">
											</div>
										</div>
									</div>

									<div class="selected_other_export display_none" <?php if ($info_item['kcxl_hinhThuc'] == "	XK5") { ?>style="display: block;" <?php } ?>>
										<div class="d_flex align_c mt_15" id="block32">
											<div class="width_create_choose mr_10">
												<div class="d_flex align_c" id="block28">
													<div class="w_100" id="block34">
														<p class="font_s15 line_h18 font_w500 color_grey">Trạng thái</p>
														<div class="select_create mt_5">
															<select class="select_create select_status" name="select_status5" id="select_status5" disabled>
																<option value="1" <?= $info_item['kcxl_trangThai'] == 1 ? "selected" : "" ?>>Khởi tạo</option>
																<option value="3" <?= $info_item['kcxl_trangThai'] == 2 ? "selected" : "" ?>>Từ chối</option>
															</select>
														</div>
													</div>
													<div class="w_50 ml_10 date_accses" id="block35" style="display:none">
														<div class="d_flex">
															<p class="font_s15 line_h18 font_w500 color_grey">Ngày hoàn thành</p>
															<span class="color_red alert_red">*</span>
														</div>
														<div class="select_create mt_5 date_accses_4">
															<input class="input_value_w" type="date" name="date_accses_export_4" id="date_accses_export_4">
														</div>
													</div>
												</div>
											</div>
											<div class="width_create_choose ml_10">
											</div>
										</div>
										<div class="d_flex mt_15" id="block32">
											<div class="width_create_choose mr_10">
												<div class="d_flex">
													<p class="font_s15 line_h18 font_w500 color_grey">Kho xuất</p>
													<span class="color_red alert_red">*</span>
												</div>
												<div class="select_create mt_5 choose_select_export_kho_5">
													<select class="select_create select_export_kho_5" name="select_export_kho_5" id="select_export_kho_5">
														<option value="">Chọn kho xuất</option>
														<? while (($selected = mysql_fetch_assoc($kho->result))) { ?>
															<option value="<?= $selected['kho_id'] ?>" <?= ($selected['kho_id'] == $info_item['kcxl_khoXuat']) ? "selected" : "" ?>><?= $selected['kho_name'] ?></option>
														<? } ?>
													</select>
												</div>
											</div>
											<div class="width_create_choose ml_10" id="block33">
												<div class="select_date_xuat_6">
													<div class="d_flex">
														<p class="font_s15 line_h18 font_w500 color_grey">Ngày xuất kho</p>
														<span class="color_red alert_red">*</span>
													</div>
													<div>
														<input class="input_value_w mt_5 date_xuat_6" type="date" name="date_xuat_6" value="<?= $info_item['kcxl_ngayXuatKho'] ?>">
													</div>
												</div>
											</div>
										</div>
										<div class="d_flex align_c mt_15" id="block32">
											<div class="width_create_choose mr_10">
												<div class="d_flex align_c space_b">
													<p class="font_s15 line_h18 font_w500 color_grey">Người giao hàng</p>
													<div class="d_flex align_c">
														<img src="../images/create.png" alt="">
														<p class="font_s14 line_h16 color_blue ml_5">Thêm nhân viên</p>
													</div>
												</div>
												<div class="select_create mt_5">
													<select class="select_create select_deliver" name="select_deliver_5" id="select_deliver_5">
														<option value="">Chọn người giao</option>
														<? foreach ($newArr as $key => $value) { ?>
															<option value="<?= $key ?>" <?= ($key == $info_item['kcxl_nguoiThucHien']) ? "selected" : "" ?>><?= $value['ep_name'] . ' ' . '-' . ' ' . $value['dep_name'] ?></option>
														<? } ?>
													</select>
												</div>
											</div>
											<div class="width_create_choose ml_10" id="block33">
												<p class="font_s15 line_h18 font_w500 color_grey">Phòng ban</p>
												<input class="input_value_grey mt_5 ten_pbg_5" readonly type="text" placeholder="Hiển thị phòng ban người giao" value="<?= $newArr[$id_nguoi_giao]['dep_name'] ?>">
												<input class="input_value_grey mt_5 id_pbg_5" readonly type="hidden" placeholder="Hiển thị phòng ban người giao" value="<?= $newArr[$id_nguoi_giao]['dep_id'] ?>">
											</div>
										</div>
										<div class="d_flex align_c mt_15" id="block32">
											<div class="width_create_choose mr_10">
												<div class="d_flex align_c space_b">
													<p class="font_s15 line_h18 font_w500 color_grey">Người nhận</p>
													<div class="d_flex align_c">
														<img src="../images/create.png" alt="">
														<p class="font_s14 line_h16 color_blue ml_5">Thêm nhân viên</p>
													</div>
												</div>
												<div class="select_create mt_5">
													<select class="select_create select_receiver" name="select_receiver_5" id="select_receiver_5" style="width: 100%">
														<option value="">Chọn người giao</option>
														<? foreach ($newArr as $key => $value) { ?>
															<option value="<?= $key ?>" <?= ($key == $info_item['kcxl_nguoiNhan']) ? "selected" : "" ?>><?= $value['ep_name'] . ' ' . '-' . ' ' . $value['dep_name'] ?></option>
														<? } ?>
													</select>
												</div>
											</div>
											<div class="width_create_choose ml_10" id="block33">
												<p class="font_s15 line_h18 font_w500 color_grey">Phòng ban</p>
												<input class="input_value_grey mt_5 ten_pbn_5" readonly type="text" placeholder="Hiển thị phòng ban người nhận" value="<?= $newArr[$id_nguoi_nhan]['dep_name'] ?>">
												<input class="input_value_grey mt_5 id_pbn_5" readonly type="hidden" placeholder="Hiển thị phòng ban người nhận" value="<?= $newArr[$id_nguoi_nhan]['dep_id'] ?>">
											</div>
										</div>
									</div>

									<div class="d_flex flex_column mt_15">
										<p class="color_grey font_s15 line_h18 font_w500">Ghi chú</p>
										<textarea class="mt_5 note" name="description" id="description" rows="5" placeholder="Nhập nội dung"><?= $info_item['kcxl_ghi_chu']; ?></textarea>
									</div>
								</div>
							</div>
						</div>
						<p class="mt_20 color_blue line_h19 font_s16 font_wB">Danh sách vật tư</p>
						<div class="main_table mt_20">

							<div class="table_construction_export display_none" <?php if ($info_item['kcxl_hinhThuc'] == "XK1") { ?>style="display: block;" <?php } ?>>
								<div class="position_r d_flex align_c">
									<div class="table_vt_scr">
										<table class="table table_1" id="table_1">
											<tr class="color_white font_s16 line_h19 font_w500">
												<th>STT
													<span class="span_tbody"></span>
												</th>
												<th>Mã vật tư thiết bị
													<span class="span_tbody"></span>
												</th>
												<th>Tên đầy đủ vật tư thiết bị
													<span class="span_tbody"></span>
												</th>
												<th>Đơn vị tính
													<span class="span_tbody"></span>
												</th>
												<th>Hãng sản xuất
													<span class="span_tbody"></span>
												</th>
												<th>Xuất xứ
													<span class="span_tbody"></span>
												</th>
												<th>Số lượng yêu cầu
													<span class="span_tbody"></span>
												</th>
												<th>Số lượng được duyệt
													<span class="span_tbody"></span>
												</th>
												<th>Số lượng thực tế xuất kho
													<span class="span_tbody"></span>
												</th>
												<th>Đơn giá (VNĐ)
													<span class="span_tbody"></span>
												</th>
												<th>Thành tiền (VNĐ)
												</th>
											</tr>
											<tbody class="data_table_1" id="dom-table">
												<?php $stt = 1;
												foreach ($emp_vt_yc as $dsvt) {
													$id_dsvt = $dsvt['id_vat_tu'];
													$sql_dsvt = new db_query("SELECT `dvt_name`, `hsx_name`, `xx_name`, `dsvt_donGia`, `dsvt_name` FROM `danh-sach-vat-tu`
                                            LEFT JOIN `don-vi-tinh` ON `dsvt_donViTinh` = `dvt_id` AND `dvt_check` = 1
                                            LEFT JOIN `hang-san-xuat` ON `dsvt_hangSanXuat` = `hsx_id`  AND `hsx_check` = 1
                                            LEFT JOIN `xuat-xu` ON `dsvt_xuatXu` = `xx_id`
                                            WHERE `dsvt_id` = $id_dsvt  AND `dsvt_check` = 1 AND `dsvt_id_ct` = $id_cty
                                        ");
													$info_dsvt = mysql_fetch_assoc($sql_dsvt->result);

													$sql_sl = new db_query("SELECT `slvt_soLuongXuatKho` FROM `so-luong-vat-tu` WHERE `slvt_maVatTuThietBi` = $id_dsvt AND `slvt_idPhieu` = $id_phieu AND `slvt_id_ct` = $id_cty");

													$sl = mysql_fetch_assoc($sql_sl->result)['slvt_soLuongXuatKho'];
												?>
													<tr>
														<td><?= $stt++; ?></td>
														<td>VT-<?= $id_dsvt; ?></td>
														<td style="text-align: left;">
															<a href="#" class="color_blue font_w500 id_vt" data="<?= $id_dsvt; ?>">
																<?= $info_dsvt['dsvt_name']; ?>
															</a>
														</td>
														<td><?= $info_dsvt['dvt_name'] ?></td>
														<td style="text-align: left;"><?= $info_dsvt['hsx_name'] ?></td>
														<td><?= $info_dsvt['xx_name'] ?></td>
														<td style="text-align: right; background: #EEEEEE;">
															<?= $dsvt['so_luong_yc_duyet'] ?>
														</td>
														<td style="text-align: right; background: #EEEEEE;">
															<?= $dsvt['so_luong_duyet'] ?>
														</td>
														<td>
															<input class="nhap_so_luong" style="border: none; text-align: right;  outline: none" type="text" placeholder="Nhập số lượng" onkeyup="tong_vt(this)" oninput="<?= $oninput ?>" value="<?= $sl ?>">
														</td>
														<td style="text-align: right;" class="don_gia_3"><?= $info_dsvt['dsvt_donGia']; ?></td>
														<td style="text-align: right;" class="thanh_tien_3">
															<?= (number_format($sl * $info_dsvt['dsvt_donGia'], 0, '', '.')); ?>
														</td>
													</tr>
												<?php } ?>
											</tbody>
										</table>
									</div>
									<div class="pre_q d_flex align_c flex_center position_a">
										<span class="pre_arrow"></span>
									</div>
									<div class="next_q d_flex align_c flex_center position_a">
										<span class="next_arrow"></span>
									</div>
								</div>
							</div>

							<div class="table_export_transfer display_none" <?php if ($info_item['kcxl_hinhThuc'] == "XK2") { ?>style="display: block;" <?php } ?>>
								<div class="position_r d_flex align_c">
									<div class="table_vt_scr">
										<table class="table table_2">
											<tr class="color_white font_s16 line_h19 font_w500">
												<th>STT
													<span class="span_tbody"></span>
												</th>
												<th>Mã vật tư thiết bị
													<span class="span_tbody"></span>
												</th>
												<th>Tên đầy đủ vật tư thiết bị
													<span class="span_tbody"></span>
												</th>
												<th>Đơn vị tính
													<span class="span_tbody"></span>
												</th>
												<th>Hãng sản xuất
													<span class="span_tbody"></span>
												</th>
												<th>Xuất xứ
													<span class="span_tbody"></span>
												</th>
												<th>Số lượng yêu cầu
													<span class="span_tbody"></span>
												</th>
												<th>Số lượng thực tế xuất kho
													<span class="span_tbody"></span>
												</th>
												<th>Đơn giá (VNĐ)
													<span class="span_tbody"></span>
												</th>
												<th>Thành tiền (VNĐ)
												</th>
											</tr>
											<tbody class="data_table_2">
												<?php $stt = 1;
												if ($info_item['kcxl_phieuDieuChuyenKho'] != '') {
													$id_pdc_sl = $info_item['kcxl_phieuDieuChuyenKho'];
													$list_dsvt = new db_query("SELECT `dsvt_id`, `dsvt_maVatTuThietBi`, `dsvt_name`, `hsx_name`, `dvt_name`, `xx_name`, `dsvt_donGia`, `slvt_soLuongXuatKho`, `slvt_checkDieuChuyen`
																							FROM `danh-sach-vat-tu`
																							LEFT JOIN `hang-san-xuat` on `dsvt_hangSanXuat` = `hsx_id`   AND `hsx_check` = 1     
																							LEFT JOIN `don-vi-tinh` on `dsvt_donViTinh` = `dvt_id`  AND `dvt_check` = 1
																							LEFT JOIN `xuat-xu` on `dsvt_xuatXu` = `xx_id`
																							LEFT JOIN `so-luong-vat-tu` on  `dsvt_id` = `slvt_maVatTuThietBi`
																							WHERE `dsvt_check` = 1 AND `slvt_idPhieu` = $id_phieu AND `dsvt_id_ct` = $id_cty
																					");
												}
												while ($info_dsvt_2 = mysql_fetch_assoc($list_dsvt->result)) {
													$id_pdc = $info_dsvt_2['slvt_checkDieuChuyen'];
													$sl_yc_dc = new db_query("SELECT `slvt_soLuongDieuChuyen` FROM `so-luong-vat-tu` WHERE `slvt_idPhieu` = $id_pdc AND `slvt_id_ct` = $id_cty
											");
													$info_sl_dc = mysql_fetch_assoc($sl_yc_dc->result);
												?>
													<tr class="color_grey font_s14 line_h16 font_w400">
														<td><?= $stt++; ?></td>
														<td>VT - <?= $info_dsvt_2['dsvt_id']; ?></td>
														<td style="text-align: left;">
															<a href="#" class="color_blue font_w500 id_vt" data="<?= $info_dsvt_2['dsvt_id']; ?>">
																<?= $info_dsvt_2['dsvt_name']; ?>
															</a>
														</td>
														<td><?= $info_dsvt_2['dvt_name'] ?></td>
														<td style="text-align: left;"><?= $info_dsvt_2['hsx_name'] ?></td>
														<td><?= $info_dsvt_2['xx_name'] ?></td>
														<td style="text-align: right; background: #EEEEEE;">
															<?= $info_sl_dc['slvt_soLuongDieuChuyen'] ?>
														</td>
														<td>
															<input class="nhap_so_luong" style="border: none; text-align: right;  outline: none" type="text" placeholder="Nhập số lượng" onkeyup="tong_vt(this)" oninput="<?= $oninput ?>" value="<?= $info_dsvt_2['slvt_soLuongXuatKho'] ?>">
														</td>
														<td style="text-align: right;" class="don_gia_3"><?= $info_dsvt_2['dsvt_donGia']; ?></td>
														<td style="text-align: right;" class="thanh_tien_3">
															<?= (number_format($info_dsvt_2['slvt_soLuongXuatKho'] * $info_dsvt_2['dsvt_donGia'], 0, '', '.')); ?>
														</td>
													</tr>
												<?php } ?>
											</tbody>
										</table>
									</div>
									<div class="pre_q d_flex align_c flex_center position_a">
										<span class="pre_arrow"></span>
									</div>
									<div class="next_q d_flex align_c flex_center position_a">
										<span class="next_arrow"></span>
									</div>
								</div>
							</div>

							<div class="table_export_on_request display_none" <?php if ($info_item['kcxl_hinhThuc'] == "XK3") { ?>style="display: block;" <?php } ?>>
								<div class="position_r d_flex align_c">
									<div class="table_vt_scr">
										<table class="table table_3" id="table_1">
											<tr class="color_white font_s16 line_h19 font_w500">
												<th>STT
													<span class="span_tbody"></span>
												</th>
												<th>Mã vật tư thiết bị
													<span class="span_tbody"></span>
												</th>
												<th>Tên đầy đủ vật tư thiết bị
													<span class="span_tbody"></span>
												</th>
												<th>Đơn vị tính
													<span class="span_tbody"></span>
												</th>
												<th>Hãng sản xuất
													<span class="span_tbody"></span>
												</th>
												<th>Xuất xứ
													<span class="span_tbody"></span>
												</th>
												<th>Số lượng yêu cầu
													<span class="span_tbody"></span>
												</th>
												<th>Số lượng được duyệt
													<span class="span_tbody"></span>
												</th>
												<th>Số lượng thực tế xuất kho
													<span class="span_tbody"></span>
												</th>
												<th>Đơn giá (VNĐ)
													<span class="span_tbody"></span>
												</th>
												<th>Thành tiền (VNĐ)
												</th>
											</tr>
											<tbody class="data_table_3" id="dom-table1">
												<?php $stt = 1;
												foreach ($emp_vt_yc as $dsvt) {
													$id_dsvt = $dsvt['id_vat_tu'];
													$sql_dsvt = new db_query("SELECT `dvt_name`, `hsx_name`, `xx_name`, `dsvt_donGia`, `dsvt_name` FROM `danh-sach-vat-tu`
                                            LEFT JOIN `don-vi-tinh` ON `dsvt_donViTinh` = `dvt_id`  AND `dvt_check` = 1
                                            LEFT JOIN `hang-san-xuat` ON `dsvt_hangSanXuat` = `hsx_id`  AND `hsx_check` = 1
                                            LEFT JOIN `xuat-xu` ON `dsvt_xuatXu` = `xx_id`
                                            WHERE `dsvt_id` = $id_dsvt  AND `dsvt_check` = 1 AND `dsvt_id_ct` = $id_cty
                                        ");
													$info_dsvt = mysql_fetch_assoc($sql_dsvt->result);

													$sql_sl = new db_query("SELECT `slvt_soLuongXuatKho` FROM `so-luong-vat-tu` WHERE `slvt_maVatTuThietBi` = $id_dsvt AND `slvt_idPhieu` = $id_phieu AND `slvt_id_ct` = $id_cty");

													$sl = mysql_fetch_assoc($sql_sl->result)['slvt_soLuongXuatKho'];
												?>
													<td><?= $stt++; ?></td>
													<td>VT-<?= $id_dsvt; ?></td>
													<td style="text-align: left;">
														<a href="#" class="color_blue font_w500 id_vt" data="<?= $id_dsvt; ?>">
															<?= $info_dsvt['dsvt_name']; ?>
														</a>
													</td>
													<td><?= $info_dsvt['dvt_name'] ?></td>
													<td style="text-align: left;"><?= $info_dsvt['hsx_name'] ?></td>
													<td><?= $info_dsvt['xx_name'] ?></td>
													<td style="text-align: right; background: #EEEEEE;">
														<?= $dsvt['so_luong_yc_duyet'] ?>
													</td>
													<td style="text-align: right; background: #EEEEEE;">
														<?= $dsvt['so_luong_duyet'] ?>
													</td>
													<td>
														<input class="nhap_so_luong" style="border: none; text-align: right;  outline: none" type="text" placeholder="Nhập số lượng" onkeyup="tong_vt(this)" oninput="<?= $oninput ?>" value="<?= $sl ?>">
													</td>
													<td style="text-align: right;" class="don_gia_3"><?= $info_dsvt['dsvt_donGia']; ?></td>
													<td style="text-align: right;" class="thanh_tien_3">
														<?= (number_format($sl * $info_dsvt['dsvt_donGia'], 0, '', '.')); ?>
													</td>
													</tr>
												<?php } ?>
											</tbody>
										</table>
									</div>
									<div class="pre_q d_flex align_c flex_center position_a">
										<span class="pre_arrow"></span>
									</div>
									<div class="next_q d_flex align_c flex_center position_a">
										<span class="next_arrow"></span>
									</div>
								</div>
							</div>

							<div class="table_made_to_order display_none" <?php if ($info_item['kcxl_hinhThuc'] == "XK4") { ?>style="display: block;" <?php } ?>>
								<div class="position_r d_flex align_c">
									<div class="table_vt_scr">
										<table class="table table_2">
											<tr class="color_white font_s16 line_h19 font_w500">
												<th>STT
													<span class="span_tbody"></span>
												</th>
												<th>Mã vật tư thiết bị
													<span class="span_tbody"></span>
												</th>
												<th>Tên đầy đủ vật tư thiết bị
													<span class="span_tbody"></span>
												</th>
												<th>Đơn vị tính
													<span class="span_tbody"></span>
												</th>
												<th>Hãng sản xuất
													<span class="span_tbody"></span>
												</th>
												<th>Xuất xứ
													<span class="span_tbody"></span>
												</th>
												<th>Số lượng yêu cầu
													<span class="span_tbody"></span>
												</th>
												<th>Số lượng thực tế xuất kho
													<span class="span_tbody"></span>
												</th>
												<th>Đơn giá (VNĐ)
													<span class="span_tbody"></span>
												</th>
												<th>Thành tiền (VNĐ)
												</th>
											</tr>
											<tbody class="table_4">
												<?php $stt = 1;
												foreach ($list_vtdh1 as $dsvt) {
													$id_dsvt = $dsvt['id_vat_tu'];
													$sql_dsvt = new db_query("SELECT `dvt_name`, `hsx_name`, `xx_name`, `dsvt_donGia`, `dsvt_name` FROM `danh-sach-vat-tu`
                                            LEFT JOIN `don-vi-tinh` ON `dsvt_donViTinh` = `dvt_id`  AND `dvt_check` = 1
                                            LEFT JOIN `hang-san-xuat` ON `dsvt_hangSanXuat` = `hsx_id` AND `hsx_check` = 1
                                            LEFT JOIN `xuat-xu` ON `dsvt_xuatXu` = `xx_id`
                                            WHERE `dsvt_id` = $id_dsvt  AND `dsvt_check` = 1 AND `dsvt_id_ct` = $id_cty
                                        ");
													$info_dsvt = mysql_fetch_assoc($sql_dsvt->result);

													$sql_sl = new db_query("SELECT `slvt_soLuongXuatKho` FROM `so-luong-vat-tu` WHERE `slvt_maVatTuThietBi` = $id_dsvt AND `slvt_idPhieu` = $id_phieu AND `slvt_id_ct` = $id_cty");
													$sl = mysql_fetch_assoc($sql_sl->result)['slvt_soLuongXuatKho'];
												?>
													<tr class="color_grey font_s14 line_h16 font_w400">
														<td><?= $stt++; ?></td>
														<td>VT - <?= $id_dsvt; ?></td>
														<td style="text-align: left;">
															<a href="#" class="color_blue font_w500 id_vt" data="<?= $id_dsvt; ?>">
																<?= $info_dsvt['dsvt_name']; ?>
															</a>
														</td>
														<td><?= $info_dsvt['dvt_name'] ?></td>
														<td style="text-align: left;"><?= $info_dsvt['hsx_name'] ?></td>
														<td><?= $info_dsvt['xx_name'] ?></td>
														<td style="text-align: right; background: #EEEEEE;">
															<?= $dsvt['so_luong_ky_nay'] ?>
														</td>
														<td>
															<input class="nhap_so_luong" style="border: none; text-align: right;  outline: none" type="text" placeholder="Nhập số lượng" onkeyup="tong_vt(this)" oninput="<?= $oninput ?>" value="<?= $sl ?>">
														</td>
														<td style="text-align: right;" class="don_gia_3"><?= $info_dsvt['dsvt_donGia']; ?></td>
														<td style="text-align: right;" class="thanh_tien_3">
															<?= (number_format($sl * $info_dsvt['dsvt_donGia'], 0, '', '.')); ?>
														</td>
													</tr>
												<?php } ?>
											</tbody>
										</table>
									</div>
									<div class="pre_q d_flex align_c flex_center position_a">
										<span class="pre_arrow"></span>
									</div>
									<div class="next_q d_flex align_c flex_center position_a">
										<span class="next_arrow"></span>
									</div>
								</div>
							</div>

							<div class="table_export_by_order display_none" <?php if ($info_item['kcxl_hinhThuc'] == "XK5") { ?>style="display: block;" <?php } ?>>
								<div class="position_r d_flex align_c">
									<div class="table_vt_scr">
										<table class="table table_3">
											<tr class="color_white font_s16 line_h19 font_w500">
												<th>
													<span class="span_tbody"></span>
												</th>
												<th>Mã vật tư thiết bị
													<span class="span_tbody"></span>
												</th>
												<th>Tên đầy đủ vật tư thiết bị
													<span class="span_tbody"></span>
												</th>
												<th>Đơn vị tính
													<span class="span_tbody"></span>
												</th>
												<th>Hãng sản xuất
													<span class="span_tbody"></span>
												</th>
												<th>Xuất xứ
													<span class="span_tbody"></span>
												</th>
												<th>Số lượng xuất kho
													<span class="span_tbody"></span>
												</th>
												<th>Đơn giá (VNĐ)
													<span class="span_tbody"></span>
												</th>
												<th>Thành tiền (VNĐ)
												</th>
											</tr>
											<tbody class="table_khac">
												<?php
												$id_kho_edit = $info_item['kcxl_khoXuat'];

												$list_dsvt = new db_query("SELECT `dsvt_id`, `dsvt_maVatTuThietBi`, `dsvt_name`, `hsx_name`, `dvt_name`, `xx_name`, `dsvt_donGia`, `slvt_soLuongXuatKho`
                                       FROM `danh-sach-vat-tu`
                                       LEFT JOIN `hang-san-xuat` on `dsvt_hangSanXuat` = `hsx_id`   AND `hsx_check` = 1     
                                       LEFT JOIN `don-vi-tinh` on `dsvt_donViTinh` = `dvt_id` AND `dvt_check` = 1
                                       LEFT JOIN `xuat-xu` on `dsvt_xuatXu` = `xx_id`
                                       LEFT JOIN `so-luong-vat-tu` on `slvt_maVatTuThietBi` = `dsvt_id`
                                       LEFT JOIN `kho-cho-xu-li` on `kcxl_id` = `kcxl_soPhieu`
                                       WHERE `dsvt_check` = 1 AND `slvt_idPhieu` = $id_phieu AND `dsvt_id_ct` = $id_cty");

												$list_all = new db_query("SELECT `dsvt_id`, `dsvt_name`, `dsvt_kho`, `hsx_name`, `dvt_name`, `xx_name`,`dsvt_donGia` FROM `danh-sach-vat-tu`
                                       LEFT JOIN `hang-san-xuat` on `dsvt_hangSanXuat` = `hsx_id` AND `hsx_check` = 1      
                                       LEFT JOIN `don-vi-tinh` on `dsvt_donViTinh` = `dvt_id` AND `dvt_check` = 1
                                       LEFT JOIN `xuat-xu` on `dsvt_xuatXu` = `xx_id`
                                       WHERE `dsvt_check` = 1 AND `dsvt_id_ct` = $id_cty");
												$responsive = [];
												while (($selected = mysql_fetch_assoc($list_all->result))) {
													$check_kho = explode(',', $selected['dsvt_kho']);
													if (in_array($id_kho_edit, $check_kho)) {
														$item['id'] = $selected['dsvt_id'];
														$item['ten_vt'] = $selected['dsvt_name'];
														$item['ten_hang'] = $selected['hsx_name'];
														$item['ten_dvt'] = $selected['dvt_name'];
														$item['ten_xx'] = $selected['xx_name'];
														$item['don_gia'] = $selected['dsvt_donGia'];
														$responsive[$selected['dsvt_id']] = $item;
													}
												}
												?>
												<?php $stt = 1;
												while ($row_dsvt_tc = mysql_fetch_assoc($list_dsvt->result)) { ?>
													<tr class="color_grey font_s14 line_h17 font_w400 table_3 delete_3">
														<td onclick="deleteRow(this)"><img class="cursor_p delete_3" src="../images/del_row.png" alt="" style="margin: 0; width: 16px; height: 16px;"></td>
														<td class="ma_vat_tu_3 delete_3" style="background-color: #EEEEEE;">VT - <?= $row_dsvt_tc['dsvt_id'] ?></td>
														<td class="color_blue font_w500 delete_3 id_vt" data="<?= $row_dsvt_tc['dsvt_id'] ?>" style="text-align: left;">
															<select class="select_tb color_grey3 font_s14 line_h17 font_w400 select_tb_3 delete_3" onchange="change_value_sl(this)" style="width: 100%">
																<option value=""></option>
																<? foreach ($responsive as $item) { ?>
																	<option value="<?= $item['id']; ?>" <?= ($item['id'] == $row_dsvt_tc['dsvt_id']) ? "selected" : "" ?>><?= $item['ten_vt'] ?></option>
																<? } ?>
															</select>
														</td>
														<td class="don_vi_tinh_3 delete_3" style="background-color: #EEEEEE;"><?= $row_dsvt_tc['dvt_name']; ?></td>
														<td class="hang_san_xuat_3 delete_3" style="text-align: left; background-color: #EEEEEE;"><?= $row_dsvt_tc['hsx_name']; ?></td>
														<td class="xuat_xu_3 delete_3" style="background-color: #EEEEEE;"><?= $row_dsvt_tc['xx_name']; ?></td>
														<td><input style="border:none; outline:none" type="text" value="<?= $row_dsvt_tc['slvt_soLuongXuatKho']; ?>" class="nhap_sl_3 delete_3 color_grey font_s14 line_h17 font_w400" placeholder="Nhập số lượng" onkeyup="tong_vt(this)"></td>
														<td class="don_gia_3 delete_3" style="background-color: #EEEEEE;"><?= number_format($row_dsvt_tc['dsvt_donGia'], 0, '', '.'); ?></td>
														<td class="thanh_tien_3 delete_3" style="background-color: #EEEEEE;"><?= (number_format($row_dsvt_tc['slvt_soLuongXuatKho'] * $row_dsvt_tc['dsvt_donGia'], 0, '', '.')); ?></td>
													</tr>
												<?php } ?>
											</tbody>
											<tr>
												<td colspan="11">
													<div class="d_flex align_c add_row">
														<img src="../images/add_vt.png" alt="">
														<p class="color_blue font_s14 line_h16 font_w500">Thêm vật tư</p>
													</div>
												</td>
											</tr>
										</table>
									</div>
									<div class="pre_q d_flex align_c flex_center position_a">
										<span class="pre_arrow"></span>
									</div>
									<div class="next_q d_flex align_c flex_center position_a">
										<span class="next_arrow"></span>
									</div>
								</div>
							</div>
						</div>
						<div class="btn_cf_rq d_flex flex_center" style="display: flex;">
							<button class="btn_cancel back_w color_blue font_s15 line_h18 font_w500" onclick="openAndHide('','popup_cancel_export_note')">Hủy</button>
							<button class="btn_save back_blue color_white font_s15 line_h18 font_w500" type="button">Lưu</button>
							<!-- onclick="openAndHide('','add_new_export_note_success')" -->
						</div>
					</form>
				</div>
			</div>
			<?php include('../includes/popup_overview.php');  ?>
			<?php include('../includes/popup_h.php');  ?>
		</div>
	</body>
	<script type="text/javascript" src="../js/jquery-3.4.1.min.js"></script>
	<script type="text/javascript" src="../js/jquery.validate.min.js"></script>
	<script type="text/javascript" src="../js/reset_validate.js"></script>
	<script type="text/javascript" src="../js/select2.min.js"></script>
	<script type="text/javascript" src="../js/js_h.js"></script>
	<script type="text/javascript" src="../js/validate_h.js"></script>
	<script type="text/javascript" src="../js/js_k.js"></script>
	<script type="text/javascript" src="../js/js_n.js"></script>
	<script>
		var ctr_name_ct = <?= json_encode($list_phieu_vt1) ?>;
		var data = <?= json_encode($newArr) ?>;
		var pdc = <?= json_encode($arr_pdk) ?>;
		var pyc = <?= json_encode($emp1) ?>;
		var arr_dsvt_tb = <?= json_encode($arr_dsvt_tb) ?>;
		var arr_kho3 = <?= json_encode($arr_kho3) ?>;
		var data_dh_2 = <?= json_encode($data_dh_2) ?>;
		var id_ct_change = <?= json_encode($id_cty) ?>;
		var id_ct = <?= json_encode($id_cty) ?>;
		var tt_user = <?= json_encode($tt_user) ?>;
		var id_phieu = <?= json_encode($id_phieu) ?>;
	</script>
	<script type="text/javascript" src="../js/xuat_kho_edit.js"></script>
	<script>
		$('.active9').each(function() {
			if ($(this).hasClass('active9')) {
				$(this).find('a').addClass('active');
			}
		});
		$('.btn_save').click(function() {
			var id_phieu = <?= json_encode($id_phieu) ?>;
			var form_add = $("#create_export_kho");
			var id_cty = id_ct;
			resetFormValidator(form_add)
			if ($('.select_export_form').val() === 'XK1') {
				var listTable = []
				$(".nhap_so_luong").each(function() {
					var data = $(this).parent().parent().find($('.id_vt')).attr('data')
					var soluong = $(this).val()
					listTable.push({
						'id': data,
						'soluong': soluong,
					})
				})
				var check_empty_arr = [];
				for (var i = 0; i < listTable.length; i++) {
					var check_empty = listTable[i].soluong;
					if (check_empty == "") {
						check_empty_arr.push(listTable[i].soluong);
					}
				}
				var length_soluong_empty = check_empty_arr.length;
				if ($('#select_status1').val() === '1') {
					form_add.validate({
						errorPlacement: function(error, element) {
							error.appendTo(element.parents(".select_create_1"));
							error.appendTo(element.parents(".export_kho"));
							error.appendTo(element.parents(".date_export_kho"));
							error.wrap("<span class='error'>")
						},
						rules: {
							select_request_form: "required",
							select_export_kho1: "required",
							selected_date_export_kho: "required",
						},
						messages: {
							select_request_form: "Vui lòng chọn phiếu yêu cầu",
							select_export_kho1: "Vui lòng chọn kho xuất",
							selected_date_export_kho: "Vui lòng chọn ngày xuất kho",
						},
					});
					if (form_add.valid() === true && length_soluong_empty == 0) {
						var kcxl_hinhThuc = $('select[name="select_export_form"]').val()
						var kcxl_nguoiTao = $('.nguoi_tao').attr('data')
						var kcxl_ngayTao = $('.ngay_tao').attr('data')
						var kcxl_phieuYeuCau = $('select[name="select_request_form"]').val()
						var kcxl_trangThai = $('select[name="select_status1"]').val()
						var kcxl_congTrinh = $('.id_cong_trinh').val()
						var kcxl_ngayYeuCauHoanThanh = $('.ngay_ht_yc').val()
						var kcxl_nguoiThucHien = $('select[name="select_deliver"]').val()
						var kcxl_phongBanNguoiGiao = $('.id_phong_ban_giao').val()
						var kcxl_nguoiNhan = $('.id_nguoi_nhan').val()
						var kcxl_phongBanNguoiNhan = $('.id_phong_ban_nhan').val()
						var kcxl_khoXuat = $('select[name="select_export_kho1"]').val()
						var kcxl_ngayXuatKho = $('input[name = "selected_date_export_kho"]').val()
						var kcxl_ghi_chu = $('textarea[name="description"]').val()
						var listTable = JSON.stringify(listTable);
						id_cty
						$.ajax({
							url: '../ajax/edit_pxk.php',
							type: 'POST',
							// dataType: 'Json',
							data: {
								kcxl_hinhThuc: kcxl_hinhThuc,
								kcxl_nguoiTao: kcxl_nguoiTao,
								kcxl_ngayTao: kcxl_ngayTao,
								kcxl_phieuYeuCau: kcxl_phieuYeuCau,
								kcxl_trangThai: kcxl_trangThai,
								kcxl_congTrinh: kcxl_congTrinh,
								kcxl_ngayYeuCauHoanThanh: kcxl_ngayYeuCauHoanThanh,
								kcxl_nguoiThucHien: kcxl_nguoiThucHien,
								kcxl_phongBanNguoiGiao: kcxl_phongBanNguoiGiao,
								kcxl_nguoiNhan: kcxl_nguoiNhan,
								kcxl_phongBanNguoiNhan: kcxl_phongBanNguoiNhan,
								kcxl_khoXuat: kcxl_khoXuat,
								kcxl_ngayXuatKho: kcxl_ngayXuatKho,
								kcxl_ghi_chu: kcxl_ghi_chu,
								listTable: listTable,
								id_cty: id_cty,
								id_phieu: id_phieu,
							},
							success: function(response) {
								var text = $('#add_new_export_note_success .p_add_succ').text('');
								var text_new = '';
								text_new += 'Sửa phiếu xuất kho thành công!';
								text.append(text_new);
								$('#add_new_export_note_success').show()
							}
						})
					} else {
						alert("Vui lòng nhập đầy đủ thông tin!!!");
					}
				}
				if ($('#select_status1').val() === '2') {
					form_add.validate({
						errorPlacement: function(error, element) {
							error.appendTo(element.parents(".select_create_1"));
							error.appendTo(element.parents(".export_kho"));
							error.appendTo(element.parents(".date_export_kho"));
							error.appendTo(element.parents(".select_create1"));
							error.wrap("<span class='error'>")
						},
						rules: {
							select_request_form: "required",
							select_export_kho1: "required",
							selected_date_export_kho: "required",
							date_accses_export1: "required",
						},
						messages: {
							select_request_form: "Vui lòng chọn phiếu yêu cầu",
							select_export_kho1: "Vui lòng chọn kho xuất",
							selected_date_export_kho: "Vui lòng chọn ngày xuất kho",
							date_accses_export1: "Vui lòng chọn ngày hoàn thành",
						},
					});
					if (form_add.valid() === true && length_soluong_empty == 0) {
						var kcxl_hinhThuc = $('select[name="select_export_form"]').val()
						var kcxl_nguoiTao = $('.nguoi_tao').attr('data')
						var kcxl_ngayTao = $('.ngay_tao').attr('data')
						var kcxl_phieuYeuCau = $('select[name="select_request_form"]').val()
						var kcxl_trangThai = $('select[name="select_status1"]').val()
						var kcxl_congTrinh = $('.id_cong_trinh').val()
						var kcxl_ngayYeuCauHoanThanh = $('.ngay_ht_yc').val()
						var kcxl_nguoiThucHien = $('select[name="select_deliver"]').val()
						var kcxl_phongBanNguoiGiao = $('.id_phong_ban_giao').val()
						var kcxl_nguoiNhan = $('.id_nguoi_nhan').val()
						var kcxl_phongBanNguoiNhan = $('.id_phong_ban_nhan').val()
						var kcxl_khoXuat = $('select[name="select_export_kho1"]').val()
						var kcxl_ngayXuatKho = $('.selected_date_export_kho').val()
						var kcxl_ghi_chu = $('textarea[name="description"]').val()
						var kcxl_ngayHoanThanh = $('input[name="date_accses_export1"]').val()
						var listTable = JSON.stringify(listTable);
						id_cty
						$.ajax({
							url: '../ajax/edit_pxk.php',
							type: 'POST',
							dataType: 'Json',
							data: {
								kcxl_hinhThuc: kcxl_hinhThuc,
								kcxl_nguoiTao: kcxl_nguoiTao,
								kcxl_ngayTao: kcxl_ngayTao,
								kcxl_phieuYeuCau: kcxl_phieuYeuCau,
								kcxl_trangThai: kcxl_trangThai,
								kcxl_congTrinh: kcxl_congTrinh,
								kcxl_ngayYeuCauHoanThanh: kcxl_ngayYeuCauHoanThanh,
								kcxl_nguoiThucHien: kcxl_nguoiThucHien,
								kcxl_phongBanNguoiGiao: kcxl_phongBanNguoiGiao,
								kcxl_nguoiNhan: kcxl_nguoiNhan,
								kcxl_phongBanNguoiNhan: kcxl_phongBanNguoiNhan,
								kcxl_khoXuat: kcxl_khoXuat,
								kcxl_ngayXuatKho: kcxl_ngayXuatKho,
								kcxl_ghi_chu: kcxl_ghi_chu,
								kcxl_ngayHoanThanh: kcxl_ngayHoanThanh,
								listTable: listTable,
								id_cty: id_cty,
								id_phieu: id_phieu,
							},
							success: function(data) {
								var text = $('#add_new_export_note_success .p_add_succ').text('');
								var text_new = '';
								text_new += 'Sửa phiếu xuất kho thành công!';
								text.append(text_new);
								$('#add_new_export_note_success').show()
							}
						})
					} else {
						alert("Vui lòng nhập đầy đủ thông tin!!!");
					}
				}
			}
			if ($('.select_export_form').val() === 'XK2') {
				var listTable = []
				$(".nhap_so_luong_1").each(function() {
					var data = $(this).parent().parent().find($('.id_vt_1')).attr('data_1')
					var soluong = $(this).val()
					listTable.push({
						'id': data,
						'soluong': soluong,
					})
				})
				var check_empty_arr = [];
				for (var i = 0; i < listTable.length; i++) {
					var check_empty = listTable[i].soluong;
					if (check_empty == "") {
						check_empty_arr.push(listTable[i].soluong);
					}
				}
				var length_soluong_empty = check_empty_arr.length;
				console.log(check_empty_arr)
				if ($('#select_status2').val() === '1') {
					form_add.validate({
						errorPlacement: function(error, element) {
							error.appendTo(element.parents(".select_create2"));
							error.appendTo(element.parents(".date_xuat_2"));
							error.wrap("<span class='error'>")
						},
						rules: {
							select_transfer_slip: "required",
							name_date_xuat_2: "required",
						},
						messages: {
							select_transfer_slip: "Vui lòng chọn phiếu điều chuyển",
							name_date_xuat_2: "Vui lòng chọn ngày xuất kho",
						},
					});
					if (form_add.valid() === true && length_soluong_empty == 0) {
						var kcxl_hinhThuc = $('select[name="select_export_form"]').val()
						var kcxl_nguoiTao = $('.nguoi_tao').attr('data')
						var kcxl_ngayTao = $('.ngay_tao').attr('data')
						var kcxl_phieuDieuChuyenKho = $('select[name="select_transfer_slip"]').val()
						var kcxl_trangThai = $('select[name="select_status2"]').val()
						var kcxl_khoXuat = $('input[name="id_kho_xuat_1"]').val()
						var kcxl_ngayXuatKho = $('input[name = "name_date_xuat_2	"]').val()
						var kcxl_khoNhap = $('input[name="id_kho_nhap_1"]').val()
						var kcxl_ngayYeuCauHoanThanh = $('.ngay_yeu_cau_1').val()
						var kcxl_nguoiThucHien = $('.id_nguoi_giao_1').val()
						var kcxl_phongBanNguoiGiao = $('.id_phong_ban_giao_1').val()
						var kcxl_nguoiNhan = $('.id_nguoi_nhan_1').val()
						var kcxl_phongBanNguoiNhan = $('.id_phong_ban_nhan_1').val()
						var kcxl_ghi_chu = $('textarea[name="description"]').val()
						var listTable = JSON.stringify(listTable);
						var id_phieu = id_phieu;
						id_cty
						console.log(listTable)
						$.ajax({
							url: '../ajax/edit_pxk.php',
							type: 'POST',
							// dataType: 'Json',
							data: {
								kcxl_hinhThuc: kcxl_hinhThuc,
								kcxl_nguoiTao: kcxl_nguoiTao,
								kcxl_ngayTao: kcxl_ngayTao,
								kcxl_phieuDieuChuyenKho: kcxl_phieuDieuChuyenKho,
								kcxl_trangThai: kcxl_trangThai,
								kcxl_khoXuat: kcxl_khoXuat,
								kcxl_ngayXuatKho: kcxl_ngayXuatKho,
								kcxl_khoNhap: kcxl_khoNhap,
								kcxl_ngayYeuCauHoanThanh: kcxl_ngayYeuCauHoanThanh,
								kcxl_nguoiThucHien: kcxl_nguoiThucHien,
								kcxl_phongBanNguoiGiao: kcxl_phongBanNguoiGiao,
								kcxl_nguoiNhan: kcxl_nguoiNhan,
								kcxl_phongBanNguoiNhan: kcxl_phongBanNguoiNhan,
								kcxl_ghi_chu: kcxl_ghi_chu,
								listTable: listTable,
								id_cty: id_cty,
								id_phieu: id_phieu,
							},
							success: function(data) {
								var text = $('#add_new_export_note_success .p_add_succ').text('');
								var text_new = '';
								text_new += 'Sửa phiếu xuất kho thành công!';
								text.append(text_new);
								$('#add_new_export_note_success').show()
							}
						})
					} else {
						alert("Vui lòng nhập đầy đủ thông tin!!!");
					}
				}
				if ($('#select_status2').val() === '2') {
					var form_add = $("#create_export_kho");
					form_add.validate({
						errorPlacement: function(error, element) {
							error.appendTo(element.parents(".select_create2"));
							error.appendTo(element.parents(".date_xuat_2"));
							error.appendTo(element.parents(".date_accses_2"));
							error.wrap("<span class='error'>")
						},
						rules: {
							select_transfer_slip: "required",
							name_date_xuat_2: "required",
							date_accses_export_2: "required",
						},
						messages: {
							select_transfer_slip: "Vui lòng chọn phiếu điều chuyển",
							name_date_xuat_2: "Vui lòng chọn ngày xuất kho",
							date_accses_export_2: "Vui lòng chọn ngày xuất kho",
						},
					});
					if (form_add.valid() === true && length_soluong_empty == 0) {
						var kcxl_hinhThuc = $('select[name="select_export_form"]').val()
						var kcxl_nguoiTao = $('.nguoi_tao').attr('data')
						var kcxl_ngayTao = $('.ngay_tao').attr('data')
						var kcxl_phieuDieuChuyenKho = $('select[name="select_transfer_slip"]').val()
						var kcxl_trangThai = $('select[name="select_status2"]').val()
						var kcxl_ngayHoanThanh = $('input[name="date_accses_export_2"]').val()
						var kcxl_khoXuat = $('input[name="id_kho_xuat_1"]').val()
						var kcxl_ngayXuatKho = $('input[name = "name_date_xuat_2	"]').val()
						var kcxl_khoNhap = $('input[name="id_kho_nhap_1"]').val()
						var kcxl_ngayYeuCauHoanThanh = $('.ngay_yeu_cau_1').val()
						var kcxl_nguoiThucHien = $('.id_nguoi_giao_1').val()
						var kcxl_phongBanNguoiGiao = $('.id_phong_ban_giao_1').val()
						var kcxl_nguoiNhan = $('.id_nguoi_nhan_1').val()
						var kcxl_phongBanNguoiNhan = $('.id_phong_ban_nhan_1').val()
						var kcxl_ghi_chu = $('textarea[name="description"]').val()
						var listTable = JSON.stringify(listTable);
						id_cty
						console.log(kcxl_ngayHoanThanh, kcxl_khoXuat, kcxl_ngayXuatKho, kcxl_khoNhap)
						$.ajax({
							url: '../ajax/edit_pxk.php',
							type: 'POST',
							// dataType: 'Json',
							data: {
								kcxl_hinhThuc: kcxl_hinhThuc,
								kcxl_nguoiTao: kcxl_nguoiTao,
								kcxl_ngayTao: kcxl_ngayTao,
								kcxl_phieuDieuChuyenKho: kcxl_phieuDieuChuyenKho,
								kcxl_trangThai: kcxl_trangThai,
								kcxl_khoXuat: kcxl_khoXuat,
								kcxl_ngayXuatKho: kcxl_ngayXuatKho,
								kcxl_khoNhap: kcxl_khoNhap,
								kcxl_ngayYeuCauHoanThanh: kcxl_ngayYeuCauHoanThanh,
								kcxl_nguoiThucHien: kcxl_nguoiThucHien,
								kcxl_phongBanNguoiGiao: kcxl_phongBanNguoiGiao,
								kcxl_nguoiNhan: kcxl_nguoiNhan,
								kcxl_phongBanNguoiNhan: kcxl_phongBanNguoiNhan,
								kcxl_ghi_chu: kcxl_ghi_chu,
								listTable: listTable,
								id_cty: id_cty,
								id_phieu: id_phieu,
							},
							success: function(data) {
								var text = $('#add_new_export_note_success .p_add_succ').text('');
								var text_new = '';
								text_new += 'Sửa phiếu xuất kho thành công!';
								text.append(text_new);
								$('#add_new_export_note_success').show()
							}
						})
					} else {
						alert("Vui lòng nhập đầy đủ thông tin!!!");
					}
				}
			}
			if ($('.select_export_form').val() === 'XK3') {
				var listTable = []
				$(".nhap_so_luong").each(function() {
					var data = $(this).parent().parent().find($('.id_vt')).attr('data')
					var soluong = $(this).val()
					listTable.push({
						'id': data,
						'soluong': soluong,
					})
				})
				var check_empty_arr = [];
				for (var i = 0; i < listTable.length; i++) {
					var check_empty = listTable[i].soluong;
					if (check_empty == "") {
						check_empty_arr.push(listTable[i].soluong);
					}
				}
				var length_soluong_empty = check_empty_arr.length;
				if ($('#select_status3').val() === '1') {
					form_add.validate({
						errorPlacement: function(error, element) {
							error.appendTo(element.parents(".request_form1"));
							error.appendTo(element.parents(".date_xuat_3"));
							error.wrap("<span class='error'>")
						},
						rules: {
							select_request_form1: "required",
							ngay_xuat_3: "required",
						},
						messages: {
							select_request_form1: "Vui lòng chọn phiếu yêu cầu",
							ngay_xuat_3: "Vui lòng chọn ngày xuất kho",
						},
					});
					if (form_add.valid() === true && length_soluong_empty == 0) {
						var kcxl_hinhThuc = $('select[name="select_export_form"]').val()
						var kcxl_nguoiTao = $('.nguoi_tao').attr('data')
						var kcxl_ngayTao = $('.ngay_tao').attr('data')
						var kcxl_phieuYeuCau = $('select[name="select_request_form1"]').val()
						var kcxl_trangThai = $('select[name="select_status3"]').val()
						var kcxl_khoXuat = $('input[name="id_kho_xuat_3"]').val()
						var kcxl_ngayXuatKho = $('input[name = "ngay_xuat_3"]').val()
						var kcxl_ngayYeuCauHoanThanh = $('.date_yc_3').val()
						var kcxl_nguoiThucHien = $('select[name="select_deliver1"]').val()
						var kcxl_phongBanNguoiGiao = $('.id_phong_ban_giao1').val()
						var kcxl_nguoiNhan = $('select[name="select_receiver1"]').val()
						var kcxl_phongBanNguoiNhan = $('.id_phong_ban_nhan1').val()
						var kcxl_ghi_chu = $('textarea[name="description"]').val()
						var listTable = JSON.stringify(listTable);
						var id_phieu = id_phieu;
						id_cty
						$.ajax({
							url: '../ajax/edit_pxk.php',
							type: 'POST',
							// dataType: 'Json',
							data: {
								kcxl_hinhThuc: kcxl_hinhThuc,
								kcxl_nguoiTao: kcxl_nguoiTao,
								kcxl_ngayTao: kcxl_ngayTao,
								kcxl_phieuYeuCau: kcxl_phieuYeuCau,
								kcxl_trangThai: kcxl_trangThai,
								kcxl_congTrinh: kcxl_congTrinh,
								kcxl_ngayYeuCauHoanThanh: kcxl_ngayYeuCauHoanThanh,
								kcxl_nguoiThucHien: kcxl_nguoiThucHien,
								kcxl_phongBanNguoiGiao: kcxl_phongBanNguoiGiao,
								kcxl_nguoiNhan: kcxl_nguoiNhan,
								kcxl_phongBanNguoiNhan: kcxl_phongBanNguoiNhan,
								kcxl_khoXuat: kcxl_khoXuat,
								kcxl_ngayXuatKho: kcxl_ngayXuatKho,
								kcxl_ghi_chu: kcxl_ghi_chu,
								listTable: listTable,
								id_cty: id_cty,
								id_phieu: id_phieu
							},
							success: function(data) {
								var text = $('#add_new_export_note_success .p_add_succ').text('');
								var text_new = '';
								text_new += 'Sửa phiếu xuất kho thành công!';
								text.append(text_new);
								$('#add_new_export_note_success').show()
							}
						})
					} else {
						alert("Vui lòng nhập đầy đủ thông tin!!!");
					}
				}
				if ($('#select_status3').val() === '2') {
					form_add.validate({
						errorPlacement: function(error, element) {
							error.appendTo(element.parents(".request_form1"));
							error.appendTo(element.parents(".date_xuat_3"));
							error.appendTo(element.parents(".date_accses_3"));
							error.wrap("<span class='error'>")
						},
						rules: {
							select_request_form1: "required",
							ngay_xuat_3: "required",
							date_accses_export_3: "required",
						},
						messages: {
							select_request_form1: "Vui lòng chọn phiếu yêu cầu",
							ngay_xuat_3: "Vui lòng chọn ngày xuất kho",
							date_accses_export_3: "Vui lòng chọn ngày xuất kho",
						},
					});
					if (form_add.valid() === true && length_soluong_empty == 0) {
						var kcxl_hinhThuc = $('select[name="select_export_form"]').val()
						var kcxl_nguoiTao = $('.nguoi_tao').attr('data')
						var kcxl_ngayTao = $('.ngay_tao').attr('data')
						var kcxl_phieuYeuCau = $('select[name="select_request_form1"]').val()
						var kcxl_trangThai = $('select[name="select_status3"]').val()
						var kcxl_ngayHoanThanh = $('input[name="date_accses_export_3"]').val()
						var kcxl_khoXuat = $('input[name="id_kho_xuat_3"]').val()
						var kcxl_ngayXuatKho = $('input[name = "ngay_xuat_3"]').val()
						var kcxl_ngayYeuCauHoanThanh = $('.date_yc_3').val()
						var kcxl_nguoiThucHien = $('select[name="select_deliver1"]').val()
						var kcxl_phongBanNguoiGiao = $('.id_phong_ban_giao1').val()
						var kcxl_nguoiNhan = $('select[name="select_receiver1"]').val()
						var kcxl_phongBanNguoiNhan = $('.id_phong_ban_nhan1').val()
						var kcxl_ghi_chu = $('textarea[name="description"]').val()
						var listTable = JSON.stringify(listTable);
						id_cty
						$.ajax({
							url: '../ajax/edit_pxk.php',
							type: 'POST',
							// dataType: 'Json',
							data: {
								kcxl_hinhThuc: kcxl_hinhThuc,
								kcxl_nguoiTao: kcxl_nguoiTao,
								kcxl_ngayTao: kcxl_ngayTao,
								kcxl_phieuYeuCau: kcxl_phieuYeuCau,
								kcxl_trangThai: kcxl_trangThai,
								kcxl_ngayHoanThanh: kcxl_ngayHoanThanh,
								kcxl_congTrinh: kcxl_congTrinh,
								kcxl_ngayYeuCauHoanThanh: kcxl_ngayYeuCauHoanThanh,
								kcxl_nguoiThucHien: kcxl_nguoiThucHien,
								kcxl_phongBanNguoiGiao: kcxl_phongBanNguoiGiao,
								kcxl_nguoiNhan: kcxl_nguoiNhan,
								kcxl_phongBanNguoiNhan: kcxl_phongBanNguoiNhan,
								kcxl_khoXuat: kcxl_khoXuat,
								kcxl_ngayXuatKho: kcxl_ngayXuatKho,
								kcxl_ghi_chu: kcxl_ghi_chu,
								listTable: listTable,
								id_cty: id_cty,
								id_phieu: id_phieu,
							},
							success: function(data) {
								var text = $('#add_new_export_note_success .p_add_succ').text('');
								var text_new = '';
								text_new += 'Sửa phiếu xuất kho thành công!';
								text.append(text_new);
								$('#add_new_export_note_success').show()
							}
						})
					} else {
						alert("Vui lòng nhập đầy đủ thông tin!!!");
					}
				}
			}
			if ($('.select_export_form').val() === 'XK4') {
				var listTable = []
				$(".nhap_so_luong").each(function() {
					var data = $(this).parent().parent().find($('.id_vt')).attr('data')
					var soluong = $(this).val()
					listTable.push({
						'id': data,
						'soluong': soluong,
					})
				})
				var check_empty_arr = [];
				for (var i = 0; i < listTable.length; i++) {
					var check_empty = listTable[i].soluong;
					if (check_empty == "") {
						check_empty_arr.push(listTable[i].soluong);
					}
				}
				var length_soluong_empty = check_empty_arr.length;
				if ($('#select_status4').val() === '1') {
					form_add.validate({
						errorPlacement: function(error, element) {
							error.appendTo(element.parents(".choose_select_order"));
							error.appendTo(element.parents(".choose_select_export_kho_2"));
							error.appendTo(element.parents(".select_date_xuat_4"));
							error.appendTo(element.parents(".choose_select_human_export"));
							error.wrap("<span class='error'>")
						},
						rules: {
							select_order: "required",
							select_export_kho2: "required",
							date_xuat_4: "required",
							select_human_export: "required",
						},
						messages: {
							select_order: "Vui lòng chọn đơn hàng",
							select_export_kho2: "Vui lòng chọn kho xuất",
							date_xuat_4: "Vui lòng chọn ngày xuất kho",
							select_human_export: "Vui lòng chọn người xuất kho",
						},
					});
					if (form_add.valid() === true && length_soluong_empty == 0) {
						var kcxl_hinhThuc = $('select[name="select_export_form"]').val()
						var kcxl_nguoiTao = $('.nguoi_tao').attr('data')
						var kcxl_ngayTao = $('.ngay_tao').attr('data')
						var kcxl_donHang = $('select[name = "select_order"]').val()
						var kcxl_trangThai = $('select[name="select_status4"]').val()
						var kcxl_khoXuat = $('select[name="select_export_kho2"]').val()
						var kcxl_ngayXuatKho = $('input[name = "ngay_xuat_3"]').val()
						var kcxl_ngayYeuCauHoanThanh = $('.time_ht').val()
						var kcxl_nguoiThucHien = $('select[name="select_human_export"]').val()
						var kcxl_phongBanNguoiGiao = $('.id_pb_4').val()
						var kcxl_ghi_chu = $('textarea[name="description"]').val()
						var listTable = JSON.stringify(listTable);
						var id_phieu = id_phieu;
						id_cty
						$.ajax({
							url: '../ajax/edit_pxk.php',
							type: 'POST',
							// dataType: 'Json',
							data: {
								kcxl_hinhThuc: kcxl_hinhThuc,
								kcxl_nguoiTao: kcxl_nguoiTao,
								kcxl_ngayTao: kcxl_ngayTao,
								kcxl_donHang: kcxl_donHang,
								kcxl_trangThai: kcxl_trangThai,
								kcxl_khoXuat: kcxl_khoXuat,
								kcxl_ngayXuatKho: kcxl_ngayXuatKho,
								kcxl_ngayYeuCauHoanThanh: kcxl_ngayYeuCauHoanThanh,
								kcxl_nguoiThucHien: kcxl_nguoiThucHien,
								kcxl_phongBanNguoiGiao: kcxl_phongBanNguoiGiao,
								kcxl_ghi_chu: kcxl_ghi_chu,
								listTable: listTable,
								id_cty: id_cty,
								id_phieu: id_phieu
							},
							success: function(data) {
								var text = $('#add_new_export_note_success .p_add_succ').text('');
								var text_new = '';
								text_new += 'Sửa phiếu xuất kho thành công!';
								text.append(text_new);
								$('#add_new_export_note_success').show()
							}
						})
					} else {
						alert("Vui lòng nhập đầy đủ thông tin!!!");
					}
				}
				if ($('#select_status4').val() === '2') {
					var form_add = $("#create_export_kho");
					form_add.validate({
						errorPlacement: function(error, element) {
							error.appendTo(element.parents(".choose_select_order"));
							error.appendTo(element.parents(".choose_select_export_kho_2"));
							error.appendTo(element.parents(".select_date_xuat_4"));
							error.appendTo(element.parents(".date_accses_4"));
							error.appendTo(element.parents(".choose_select_human_export"));
							error.wrap("<span class='error'>")
						},
						rules: {
							select_order: "required",
							select_export_kho2: "required",
							date_xuat_4: "required",
							date_accses_export_4: "required",
							select_human_export: "required",
						},
						messages: {
							select_order: "Vui lòng chọn đơn hàng",
							select_export_kho2: "Vui lòng chọn kho xuất",
							date_xuat_4: "Vui lòng chọn ngày xuất kho",
							date_accses_export_4: "Vui lòng chọn ngày hoàn thành",
							select_human_export: "Vui lòng chọn người xuất kho",
						},
					});
					if (form_add.valid() === true && length_soluong_empty == 0) {
						var kcxl_hinhThuc = $('select[name="select_export_form"]').val()
						var kcxl_nguoiTao = $('.nguoi_tao').attr('data')
						var kcxl_ngayTao = $('.ngay_tao').attr('data')
						var kcxl_donHang = $('select[name = "select_order"]').val()
						var kcxl_trangThai = $('select[name="select_status4"]').val()
						var kcxl_khoXuat = $('select[name="select_export_kho2"]').val()
						var kcxl_ngayXuatKho = $('input[name = "ngay_xuat_3"]').val()
						var kcxl_ngayHoanThanh = $('input[name = "date_accses_export_4"]').val()
						var kcxl_ngayYeuCauHoanThanh = $('.time_ht').val()
						var kcxl_nguoiThucHien = $('select[name="select_human_export"]').val()
						var kcxl_phongBanNguoiGiao = $('.id_pb_4').val()
						var kcxl_ghi_chu = $('textarea[name="description"]').val()
						var listTable = JSON.stringify(listTable);
						id_cty
						$.ajax({
							url: '../ajax/edit_pxk.php',
							type: 'POST',
							// dataType: 'Json',
							data: {
								kcxl_hinhThuc: kcxl_hinhThuc,
								kcxl_nguoiTao: kcxl_nguoiTao,
								kcxl_ngayTao: kcxl_ngayTao,
								kcxl_donHang: kcxl_donHang,
								kcxl_trangThai: kcxl_trangThai,
								kcxl_ngayHoanThanh: kcxl_ngayHoanThanh,
								kcxl_khoXuat: kcxl_khoXuat,
								kcxl_ngayXuatKho: kcxl_ngayXuatKho,
								kcxl_ngayYeuCauHoanThanh: kcxl_ngayYeuCauHoanThanh,
								kcxl_nguoiThucHien: kcxl_nguoiThucHien,
								kcxl_phongBanNguoiGiao: kcxl_phongBanNguoiGiao,
								kcxl_ghi_chu: kcxl_ghi_chu,
								listTable: listTable,
								id_cty: id_cty,
								id_phieu: id_phieu,
							},
							success: function(data) {
								var text = $('#add_new_export_note_success .p_add_succ').text('');
								var text_new = '';
								text_new += 'Sửa phiếu xuất kho thành công!';
								text.append(text_new);
								$('#add_new_export_note_success').show()
							}
						})
					} else {
						alert("Vui lòng nhập đầy đủ thông tin!!!");
					}
				}
			}
			if ($('.select_export_form').val() === 'XK5') {
				var listTable = []
				$(".nhap_sl_3").each(function() {
					var data = $(this).parent().parent().find($('.id_vt')).attr('data')
					var soluong = $(this).val()
					listTable.push({
						'id': data,
						'soluong': soluong,
					})
				})
				var check_empty_arr = [];
				for (var i = 0; i < listTable.length; i++) {
					var check_empty = listTable[i].soluong;
					if (check_empty == "") {
						check_empty_arr.push(listTable[i].soluong);
					}
				}
				var length_soluong_empty = check_empty_arr.length;
				if ($('#select_status5').val() === '1') {
					form_add.validate({
						errorPlacement: function(error, element) {
							error.appendTo(element.parents(".choose_select_export_kho_5"));
							error.appendTo(element.parents(".select_date_xuat_6"));
							error.wrap("<span class='error'>")
						},
						rules: {
							select_export_kho_5: "required",
							date_xuat_6: "required",
						},
						messages: {
							select_export_kho_5: "Vui lòng chọn kho xuất",
							date_xuat_6: "Vui lòng chọn ngày xuất kho",
						},
					});
					if (form_add.valid() === true && length_soluong_empty == 0) {
						var kcxl_hinhThuc = $('select[name="select_export_form"]').val()
						var kcxl_nguoiTao = $('.nguoi_tao').attr('data')
						var kcxl_ngayTao = $('.ngay_tao').attr('data')
						var kcxl_phieuYeuCau = $('select[name="select_request_form1"]').val()
						var kcxl_trangThai = $('select[name="select_status3"]').val()
						var kcxl_khoXuat = $('input[name="id_kho_xuat_3"]').val()
						var kcxl_ngayXuatKho = $('input[name = "ngay_xuat_3"]').val()
						var kcxl_ngayYeuCauHoanThanh = $('.date_yc_3').val()
						var kcxl_nguoiThucHien = $('select[name="select_deliver1"]').val()
						var kcxl_phongBanNguoiGiao = $('.id_pbg_5').val()
						var kcxl_nguoiNhan = $('select[name="select_receiver1"]').val()
						var kcxl_phongBanNguoiNhan = $('.id_pbn_5').val()
						var kcxl_ghi_chu = $('textarea[name="description"]').val()
						var listTable = JSON.stringify(listTable);
						var id_phieu = id_phieu;
						id_cty
						$.ajax({
							url: '../ajax/edit_pxk.php',
							type: 'POST',
							dataType: 'Json',
							data: {
								kcxl_hinhThuc: kcxl_hinhThuc,
								kcxl_nguoiTao: kcxl_nguoiTao,
								kcxl_ngayTao: kcxl_ngayTao,
								kcxl_trangThai: kcxl_trangThai,
								kcxl_khoXuat: kcxl_khoXuat,
								kcxl_ngayXuatKho: kcxl_ngayXuatKho,
								kcxl_nguoiThucHien: kcxl_nguoiThucHien,
								kcxl_phongBanNguoiGiao: kcxl_phongBanNguoiGiao,
								kcxl_nguoiNhan: kcxl_nguoiNhan,
								kcxl_phongBanNguoiNhan: kcxl_phongBanNguoiNhan,
								kcxl_ghi_chu: kcxl_ghi_chu,
								listTable: listTable,
								id_cty: id_cty,
								id_phieu: id_phieu
							},
							success: function(data) {
								var text = $('#add_new_export_note_success .p_add_succ').text('');
								var text_new = '';
								text_new += 'Sửa phiếu xuất kho thành công!';
								text.append(text_new);
								$('#add_new_export_note_success').show()
							}
						})
					} else {
						alert("Vui lòng nhập đầy đủ thông tin!!!");
					}
				}
				if ($('#select_status5').val() === '2') {
					var form_add = $("#create_export_kho");
					form_add.validate({
						errorPlacement: function(error, element) {
							error.appendTo(element.parents(".choose_select_export_kho_5"));
							error.appendTo(element.parents(".choose_select_export_kho_5"));
							error.appendTo(element.parents(".date_accses_4"));
							error.wrap("<span class='error'>")
						},
						rules: {
							select_export_kho_5: "required",
							date_xuat_6: "required",
							date_accses_export_4: "required",
						},
						messages: {
							select_export_kho_5: "Vui lòng chọn kho xuất",
							date_xuat_6: "Vui lòng chọn ngày xuất kho",
							date_accses_export_4: "Vui lòng chọn ngày hoàn thành",
						},
					});
					if (form_add.valid() === true && length_soluong_empty == 0) {
						$.ajax({
							url: '../ajax/edit_pxk.php',
							type: 'POST',
							dataType: 'Json',
							data: {
								kcxl_hinhThuc: kcxl_hinhThuc,
								kcxl_nguoiTao: kcxl_nguoiTao,
								kcxl_ngayTao: kcxl_ngayTao,
								kcxl_trangThai: kcxl_trangThai,
								kcxl_ngayHoanThanh: kcxl_ngayHoanThanh,
								kcxl_khoXuat: kcxl_khoXuat,
								kcxl_ngayXuatKho: kcxl_ngayXuatKho,
								kcxl_nguoiThucHien: kcxl_nguoiThucHien,
								kcxl_phongBanNguoiGiao: kcxl_phongBanNguoiGiao,
								kcxl_nguoiNhan: kcxl_nguoiNhan,
								kcxl_phongBanNguoiNhan: kcxl_phongBanNguoiNhan,
								kcxl_ghi_chu: kcxl_ghi_chu,
								listTable: listTable,
								id_cty: id_cty,
								id_phieu: id_phieu,
							},
							success: function(data) {
								var text = $('#add_new_export_note_success .p_add_succ').text('');
								var text_new = '';
								text_new += 'Sửa phiếu xuất kho thành công!';
								text.append(text_new);
								$('#add_new_export_note_success').show()
							}
						})
					} else {
						alert("Vui lòng nhập đầy đủ thông tin!!!");
					}
				}
			}
		})
	</script>

	</html>