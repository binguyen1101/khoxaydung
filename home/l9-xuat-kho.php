<?php 
    include("config1.php");

    if(!in_array(1,$ro_xuat_kho)){
      header("Location: /tong-quan.html");
    }
    
    isset($_GET['page']) ? $page = $_GET['page'] : $page = 1;
    isset($_GET['all_k']) ? $all_k = $_GET['all_k'] : $all_k = "";
    isset($_GET['th']) ? $th = $_GET['th'] : $th = "";
    isset($_GET['ht']) ? $ht = $_GET['ht'] : $ht = "";

    isset($_GET['ngts']) ? $ngts = $_GET['ngts'] : $ngts = "";
    isset($_GET['ngte']) ? $ngte = $_GET['ngte'] : $ngte = "";

    isset($_GET['ngxs']) ? $ngxs = $_GET['ngxs'] : $ngxs = "";
    isset($_GET['ngxe']) ? $ngxe = $_GET['ngxe'] : $ngxe = "";

    isset($_GET['input']) ? $ip = $_GET['input'] : $ip = "";

    isset($_GET['dis']) ? $dis = $_GET['dis'] : $dis = 10;


    if (isset($_COOKIE['user']) && $_COOKIE['user'] != "" && $_COOKIE['role'] == 2) {
		$token = $_COOKIE['acc_token'];
		$curl = curl_init();
		$data = array();
		curl_setopt($curl, CURLOPT_POST, 1);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
		curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/user_info_employee.php');
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));
		$response = curl_exec($curl);
		curl_close($curl);
		$data_tt = json_decode($response, true);
		$tt_user = $data_tt['data']['user_info_result'];
	}
    
    $id_cty = $tt_user['com_id'];

    $all_kho = new db_query("SELECT `kho_id`, `kho_name`
    FROM `kho`
    WHERE `kho_id_ct` = $id_cty
    ORDER BY `kho_id` DESC
    ");

?>
<!DOCTYPE html>
<html lang="vi">

<head>
  <title>Xuất kho</title>
  <meta charset="UTF-8">
  <meta name="robots" content="noindex,nofollow" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
  <link rel="stylesheet" type="text/css" href="../css/style.css?v=<?= $ver ?>">
  <link rel="stylesheet" type="text/css" href="../css/style_h.css?v=<?= $ver ?>">
  <link rel="stylesheet" type="text/css" href="../css/style_n.css?v=<?= $ver ?>">
</head>

<body>
  <div class="main_wrapper_all">
    <div class="wapper_all">
      <?php include('../includes/sidebar.php');  ?>
    </div>
    <div class="main_overview" id="main_overview">
      <div class="header_menu_overview d_flex align_c space_b">
        <p class="color_grey line_16 font_s14 text_link_page" style="display: block;">Xuất kho</p>
        <img class="open_sidebar_w" src="../images/open_sidebar_w.png" alt="" onclick="toggle('main_sidebar')">
        <?php include('../includes/header.php');  ?>
      </div>
      <div class="body_equipment_supplies">
        <p class="color_grey line_16 font_s14 text_link_page_re mb_15" style="display: none;">Xuất kho</p>
        <div class="export_kho_pc">
          <div class=" d_flex">
            <div class="all_kho mr_20">
              <select class="select_all_kho" name="all_kho">
                <option value=""> Tất cả các kho</option>
                <?php while($row_all_kho = mysql_fetch_assoc($all_kho->result)) {?>
                    <option value="<?= $row_all_kho['kho_id']; ?>" <?= ($all_k == $row_all_kho['kho_id']) ? "selected" : "" ?>><?= $row_all_kho['kho_name']; ?></option>
                <? } ?>
              </select>
            </div>
            <div class="date_create d_flex space_b mr_20">
              <p class="color_grey font_s14 line_h17 font_w400 align_c d_flex ml_15 mr_12">Ngày tạo:&nbsp<span class="date_start" id="date_cr_start" data="<?= $ngts ?>"><?= ($ngts != "") ?  $ngts : "yyyy/mm/dd" ?></span>
              &nbsp-&nbsp<span class="date_end" id="date_cr_end" data="<?= $ngte ?>"><?= ($ngte != "") ?  $ngte : "yyyy/mm/dd" ?></span></p>
                <img class="icon_date_click cursor_p" style="height: 15px; width: 15px" src="../images/date.png" alt="">
              </div>
            <div class="date_cr_export d_flex space_b">
              <p class="color_grey font_s14 line_h17 font_w400 align_c d_flex ml_15 mr_12">Ngày xuất:&nbsp<span class="date_start" id="date_ex_start" data="<?= $ngxs ?>"><?= ($ngxs != "") ?  $ngxs : "yyyy/mm/dd" ?></span>
              &nbsp-&nbsp<span class="date_end" id="date_ex_end" data="<?= $ngxe ?>"><?= ($ngxe != "") ?  $ngxe : "yyyy/mm/dd" ?></span></p>
                <img class="icon_date_click cursor_p" style="height: 15px; width: 15px" src="../images/date.png" alt="">
              </div>
          </div>
          <div class="d_flex align_c mt_20">
            <div class="all_kho_export mr_20">
              <div>
                <select class="select_all_form_kho" name="all_kho">
                  <option value="">Tất cả hình thức xuất kho</option>
                  <option value="XK1" <?= ($ht == "XK1") ? "selected" : "" ?>>Xuất thi công</option>
                  <option value="XK2" <?= ($ht == "XK2") ? "selected" : "" ?>>Xuất điều chuyển</option>
                  <option value="XK3" <?= ($ht == "XK3") ? "selected" : "" ?>>Xuất theo yêu cầu vật tư</option>
                  <option value="XK4" <?= ($ht == "XK4") ? "selected" : "" ?>>Xuất theo đơn hàng bán vật tư</option>
                  <option value="XK5" <?= ($ht == "XK5") ? "selected" : "" ?>>Xuất khác</option>
                </select>
              </div>
            </div>
            <div class="all_kho_status_export mr_20">
              <select class="select_all_status_kho" name="all_kho">
                <option value="">Tất cả trạng thái phiếu xuất kho</option>
                <option value="1" <?= ($th == 1) ? "selected" : "" ?>>Chờ duyệt</option>
                <option value="2" <?= ($th == 2) ? "selected" : "" ?>>Từ chối</option>
                <option value="5" <?= ($th == 5) ? "selected" : "" ?>>Đã duyệt - Chờ xuất kho</option>
                <option value="6" <?= ($th == 6) ? "selected" : "" ?>>Hoàn thành</option>
              </select>
            </div>
          </div>
          <div class="operation_wh d_flex space_b" style="display: flex;">
            <div class=" d_flex space_b">
              <div class="position_r" id="">
                <input class="search_equipment_id d_flex align_c space_b" type="text"
                  placeholder="Tìm kiếm theo mã, tên vật tư thiết bị"  name ="input_search" value="<?= ($ip != "") ? $ip : "" ?>">
                <img class="position_a icon_search_equipment_id cursor_p" src="../images/icon-sr.png" alt="">
              </div>
            </div>
            <div class="export_wh d_flex space_b align_c">
              <?php if(in_array(2,$ro_xuat_kho)){ ?>
                <a class="mr_15" href="/xuat-kho-create.html">
                  <button class="btn_px d_flex align_c cursor_p">
                    <img src="../images/img_px.png" alt="">
                    <p class="color_white font_s15 line_h18 font_w500">Thêm mới</p>
                  </button>
                </a>
              <?php }?>
              <button class="btn_ex d_flex align_c cursor_p">
                <img src="../images/export.png" alt="">
                <p class="color_white font_s15 line_h18 font_w500">Xuất excel</p>
              </button>
              <div class="hd_ex d_flex align_c">
                <img src="../images/img_hd.png" alt="">
                <p class="color_blue font_s15 line_h18 font_w500">Hướng dẫn</p>
              </div>
            </div>
          </div>
        </div>
        <div class="export_kho_mb_tl" style="display:none">
          <div class=" d_flex" id="block31">
            <div class="all_kho mr_20">
              <select class="select_all_kho" name="all_kho">
              <option value="">Tất cả các kho</option>
                <? while (($selected = mysql_fetch_assoc($kho->result))) {?>
                <option value="<?= $selected['kho_id']?>"><?= $selected['kho_name'] ?></option>
                <?}?>
              </select>
            </div>
            <div class="date_create d_flex space_b mr_20 mt_20">
              <p class="color_grey font_s14 line_h17 font_w400 align_c d_flex ml_15 mr_12">Ngày tạo:&nbsp<span class="date_start" id="date_cr_start" data="<?= $ngts ?>"><?= ($ngts != "") ?  $ngts : "yyyy/mm/dd" ?></span>
              &nbsp-&nbsp<span class="date_end" id="date_cr_end" data="<?= $ngte ?>"><?= ($ngte != "") ?  $ngte : "yyyy/mm/dd" ?></span></p>
              <img class="icon_date_click cursor_p" style="height: 15px; width: 15px" src="../images/date.png" alt="">
            </div>
          </div>
          <div class="d_flex align_c mt_20" id="block31">
            <div class="date_cr_export d_flex space_b">
              <p class="color_grey font_s14 line_h17 font_w400 align_c d_flex ml_15 mr_12">Ngày xuất:&nbsp<span class="date_start" id="date_ex_start" data="<?= $ngxs ?>"><?= ($ngxs != "") ?  $ngxs : "yyyy/mm/dd" ?></span>
              &nbsp-&nbsp<span class="date_end" id="date_ex_end" data="<?= $ngxe ?>"><?= ($ngxe != "") ?  $ngxe : "yyyy/mm/dd" ?></span></p>
              <img class="icon_date_click cursor_p" style="height: 15px; width: 15px" src="../images/date.png" alt="">
            </div>
            <div class="all_kho_export mr_20">
              <select class="select_all_form_kho" name="all_kho">
                <option value="">Tất cả hình thức xuất kho</option>
                <option value="XK1" <?= ($ht == "XK1") ? "selected" : "" ?>>Xuất thi công</option>
                <option value="XK2" <?= ($ht == "XK2") ? "selected" : "" ?>>Xuất điều chuyển</option>
                <option value="XK3" <?= ($ht == "XK3") ? "selected" : "" ?>>Xuất theo yêu cầu vật tư</option>
                <option value="XK4" <?= ($ht == "XK4") ? "selected" : "" ?>>Xuất theo đơn hàng bán vật tư</option>
                <option value="XK5" <?= ($ht == "XK5") ? "selected" : "" ?>>Xuất khác</option>
              </select>
            </div>
          </div>
          <div class="d_flex align_c space_b" id="block31">
            <div class="all_kho_status_export mr_20 mt_15">
              <select class="select_all_status_kho" name="all_kho">
                <option value="">Tất cả trạng thái phiếu xuất kho</option>
                <option value="1" <?= ($th == 1) ? "selected" : "" ?>>Chờ duyệt</option>
                <option value="2" <?= ($th == 2) ? "selected" : "" ?>>Từ chối</option>
                <option value="5" <?= ($th == 5) ? "selected" : "" ?>>Đã duyệt - Chờ xuất kho</option>
                <option value="6" <?= ($th == 6) ? "selected" : "" ?>>Đã duyệt - Đã xuất kho</option>
              </select>
            </div>
            <div class="hd_ex d_flex align_c mt_15 hd_tablet">
              <img src="../images/img_hd.png" alt="">
              <p class="color_blue font_s15 line_h18 font_w500">Hướng dẫn</p>
            </div>
          </div>
          <div class="operation_wh d_flex space_b" style="display: flex;">
            <div class="search_wh d_flex space_b">
              <div class="input_sr_wh position_r">
                <input type="text" placeholder="Tìm kiếm theo số phiếu" name ="input_search" value="<?= ($ip != "") ? $ip : "" ?>">
                <span class="icon_sr_wh"></span>
              </div>
            </div>
            <div class="export_wh d_flex space_b align_c">
              <a class="mr_15" href="/xuat-kho-create.html">
                <button class="btn_px d_flex align_c">
                  <img src="../images/img_px.png" alt="">
                  <p class="color_white font_s15 line_h18 font_w500">Thêm mới</p>
                </button>
              </a>
              <button class="btn_ex d_flex align_c">
                <img src="../images/export.png" alt="">
                <p class="color_white font_s15 line_h18 font_w500">Xuất excel</p>
              </button>
            </div>
          </div>
          <div class="hd_mobie" style="display:none">
            <div class="hd_ex d_flex align_c mt_15">
              <img src="../images/img_hd.png" alt="">
              <p class="color_blue font_s15 line_h18 font_w500">Hướng dẫn</p>
            </div>
          </div>
        </div>
        <div class="detail_wh" style="display: block;" data-page="<?= $page ?>" data-k="<?= $all_k ?>" data-th="<?= $th ?>" data-ht="<?= $ht ?>" data-ngts="<?= $ngts?>" data-ngte="<?= $ngte?>" data-ngxs="<?= $ngxs?>" data-ngxe="<?= $ngxe?>" data-ip ="<?= $ip ?>" data-dis ="<?= $dis ?>">
          <!--  -->
        </div>
      </div>
      <?php include('../includes/popup_overview.php');  ?>
      <?php include("../includes/popup_dieu-chuyen-kho.php"); ?>
      <?php include("../includes/popup_chon_ngay.php"); ?>
    </div>
</body>
<script type="text/javascript" src="../js/jquery-3.4.1.min.js"></script>
<script type="text/javascript" src="../js/jquery.validate.min.js"></script>
<script type="text/javascript" src="../js/select2.min.js"></script>
<script type="text/javascript" src="../js/js_h.js"></script>
<script type="text/javascript" src="../js/validate_h.js"></script>
<script type="text/javascript" src="../js/js_k.js"></script>
<script type="text/javascript" src="../js/js_n.js"></script>
<script type="text/javascript" src="../js/xuat_kho.js"></script>
<script>
</script>

</html>