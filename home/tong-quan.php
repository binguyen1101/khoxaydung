<?php 
  include("config1.php"); 
  $name = $_SESSION['com_name'];
  $date = $date = date('Y-m-d', time());

  if (isset($_COOKIE['user']) && $_COOKIE['user'] != "" && $_COOKIE['role'] == 2) {
		$token = $_COOKIE['acc_token'];
		$curl = curl_init();
		$data = array();
		curl_setopt($curl, CURLOPT_POST, 1);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
		curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/user_info_employee.php');
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));
		$response = curl_exec($curl);
		curl_close($curl);
		$data_tt = json_decode($response, true);
		$tt_user = $data_tt['data']['user_info_result'];
	}
    
    
    $id_cty = $tt_user['com_id'];

    // Kho cho xu li
    $sl_phieu_cho = new db_query("SELECT COUNT(`kcxl_id`) AS sl_pc FROM `kho-cho-xu-li` WHERE `kcxl_check` = 1
    AND NOT `kcxl_trangThai` = 4 
    AND NOT `kcxl_trangThai` = 6 
    AND NOT `kcxl_trangThai` = 7 
    AND NOT `kcxl_trangThai` = 12 
    AND `kcxl_id_ct` = $id_cty ");
    $sl_phieu_cho_hn = mysql_fetch_assoc($sl_phieu_cho->result)['sl_pc'];

    // So luong Vattu da tao hom nay
    $sl_vattu = new db_query("SELECT COUNT(`dsvt_id`) AS sl_vt FROM `danh-sach-vat-tu` WHERE `dsvt_id_ct` = $id_cty AND `dsvt_dateCreate` = '$date' AND `dsvt_check` = 1");
    $sl_vattu_hn = mysql_fetch_assoc($sl_vattu->result)['sl_vt'];

    // So luong PNK da tao hom nay
    $sl_pnk = new db_query("SELECT COUNT(`kcxl_id`) AS sl_pnk FROM `kho-cho-xu-li` WHERE `kcxl_id_ct` = $id_cty AND `kcxl_ngayTao` = '$date' AND `kcxl_soPhieu` = 'PNK' AND `kcxl_check` = 1");
    $sl_pnk_hn = mysql_fetch_assoc($sl_pnk->result)['sl_pnk'];

    // So luong PXK da tao hom nay
    $sl_pxk = new db_query("SELECT COUNT(`kcxl_id`) AS sl_pxk FROM `kho-cho-xu-li` WHERE `kcxl_id_ct` = $id_cty AND `kcxl_ngayTao` = '$date' AND `kcxl_soPhieu` = 'PXK' AND `kcxl_check` = 1");
    $sl_pxk_hn = mysql_fetch_assoc($sl_pxk->result)['sl_pxk'];

    // So luong ĐCK da tao hom nay
    $sl_dck = new db_query("SELECT COUNT(`kcxl_id`) AS sl_dck FROM `kho-cho-xu-li` WHERE `kcxl_id_ct` = $id_cty AND `kcxl_ngayTao` = '$date' AND `kcxl_soPhieu` = 'ĐCK' AND `kcxl_check` = 1");
    $sl_dck_hn = mysql_fetch_assoc($sl_dck->result)['sl_dck'];

    // So luong PKK da tao hom nay
    $sl_pkk = new db_query("SELECT COUNT(`kcxl_id`) AS sl_pkk FROM `kho-cho-xu-li` WHERE `kcxl_id_ct` = $id_cty AND `kcxl_ngayTao` = '$date' AND `kcxl_soPhieu` = 'PKK' AND `kcxl_check` = 1");
    $sl_pkk_hn = mysql_fetch_assoc($sl_pkk->result)['sl_pkk'];

?>
<!DOCTYPE html>
<html lang="vi">

<head>
  <title>kho vật tư xây dựng</title>
  <meta charset="UTF-8">
  <meta name="robots" content="noindex,nofollow" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" type="text/css" href="../css/style.css?v=<?= $ver ?>">
  <link rel="stylesheet" type="text/css" href="../css/style_h.css?v=<?= $ver ?>">
  <link rel="stylesheet" type="text/css" href="../css/style_n.css?v=<?= $ver ?>">
</head>

<body>
  <div class="main_wrapper_all">
    <div class="wapper_all">
      <?php include('../includes/sidebar.php');  ?>
    </div>
    <div class="main_overview" id="main_overview">
      <div class="header_menu_overview d_flex align_c space_b">
        <p class="color_grey line_16 font_s14 text_link_page" style="display: block;">Trang chủ</p>
        <img class="open_sidebar_w" src="../images/open_sidebar_w.png" alt="" onclick="toggle('main_sidebar')"
          style="display: none;">
        <?php include('../includes/header.php');  ?>
      </div>
      <div class="body_tong_quan">
        <p class="color_grey line_16 font_s14 text_link_page_re ml_10 mt_15" style="display: none;">Trang chủ</p>
        <p class="line_h19 font_s16 font_wB color_grey padding_tb20 ml_10">Tổng quan</p>
        <div class="d_flex align_c space_b flex_w flex_start">
          <div class="item_body_overview margin_10">
            <img class="icon_img_tq" src="../images/major_b.png" alt="">
            <p class="padding_tb20 line_h19 font_s16 font_w500 color_grey">Nghiệp vụ kho chờ xử lý</p>
            <div class="d_flex align_c">
              <p class="color_blue line_h16 font_s14"><?=$sl_phieu_cho_hn?></p>
              <p class="padding_l5 line_h16 font_s14 color_grey">Nghiệp vụ</p>
            </div>
            <a class="href_item" href="/nghiep-vu-kho-cho-xu-ly.html"></a>
          </div>
          <div class="item_body_overview margin_10">
            <img class="icon_img_tq" src="../images/information_g.png" alt="">
            <p class="padding_tb20 line_h19 font_s16 font_w500 color_grey">Vật tư thiết bị mới thêm</p>
            <div class="d_flex align_c">
              <p class="color_blue line_h16 font_s14 color_green_l"><?=$sl_vattu_hn?></p>
              <p class="padding_l5 line_h16 font_s14 color_grey">Vật tư thiết bị</p>
            </div>
            <a class="href_item" href="/danh-sach-vat-tu-thiet-bi.html"></a>
          </div>
          <div class="item_body_overview margin_10">
            <img class="icon_img_tq" src="../images/add_v.png" alt="">
            <p class="padding_tb20 line_h19 font_s16 font_w500 color_grey">Nhập kho hôm nay</p>
            <div class="d_flex align_c">
              <p class="color_blue line_h16 font_s14 color_violet"><?= $sl_pnk_hn; ?></p>
              <p class="padding_l5 line_h16 font_s14 color_grey">Phiếu nhập</p>
            </div>
            <a class="href_item" href="/nhap-kho.html?all_k=&th=&ht=&ngts=<?=$date?>&ngte=&ngns=&ngne=&input=&page=1"></a>
          </div>
          <div class="item_body_overview margin_10">
            <img class="icon_img_tq" src="../images/export_g.png" alt="">
            <p class="padding_tb20 line_h19 font_s16 font_w500 color_grey">Xuất kho hôm nay</p>
            <div class="d_flex align_c">
              <p class="color_blue line_h16 font_s14 color_blue_l"><?= $sl_pxk_hn; ?></p>
              <p class="padding_l5 line_h16 font_s14 color_grey">Phiếu xuất</p>
            </div>
            <a class="href_item" href="/xuat-kho.html?all_k=&th=&ht=&ngts=<?=$date?>&ngte=&ngxs=&ngxe=&input=&page=1"></a>
          </div>
          <div class="item_body_overview margin_10">
            <img class="icon_img_tq" src="../images/change_g.png" alt="">
            <p class="padding_tb20 line_h19 font_s16 font_w500 color_grey">Điều chuyển kho hôm nay</p>
            <div class="d_flex align_c">
              <p class="color_blue line_h16 font_s14 color_org"><?= $sl_dck_hn; ?></p>
              <p class="padding_l5 line_h16 font_s14 color_grey">Phiếu điều chuyển</p>
            </div>
            <a class="href_item" href="/dieu-chuyen-kho.html?kx=&kn=&th=&ngts=<?=$date?>&ngte=&ngths=&ngthe=&nghts=&nghte=&input=&page=1"></a>
          </div>
          <div class="item_body_overview margin_10">
            <img class="icon_img_tq" src="../images/list_b.png" alt="">
            <p class="padding_tb20 line_h19 font_s16 font_w500 color_grey">Kiểm kê kho hôm nay</p>
            <div class="d_flex align_c">
              <p class="color_blue line_h16 font_s14 color_blue"><?= $sl_pkk_hn; ?></p>
              <p class="padding_l5 line_h16 font_s14 color_grey">Phiếu kiểm kê</p>
            </div>
            <a class="href_item" href="/kiem-ke-kho.html?all_k=&th=&ngts=<?=$date?>&ngte=&ngths=&ngthe=&ngychts=&ngychte=&nghts=&nghte=&input=&page=1"></a>
          </div>
        </div>
        <p class="line_h19 font_s16 font_wB color_grey padding_tb20 ml_10">Ưu điểm vượt trội của hệ sinh thái Chuyển
          đổi
          số 365 </p>
        <div class="d_flex align_c space_b flex_w flex_start">
          <div class="item_body_overview margin_10">
            <div class="item_advantages">
              <img class="icon_img_tq" src="../images/safe.png" alt="">
              <p class="line_h19 font_s16 font_w500 color_grey padding_tb_15_10">An toàn và bảo mật</p>
              <p class="line_h19 font_s16 color_grey text_a_j">An toàn, bảo mật
                tuyệt đối, dữ liệu được lưu trữ theo mô hình điện thoán đám mây.</p>
            </div>
            <div class="d_flex space_b align_c button_see_details">
              <p class="color_blue line_h19 font_s15 margin_a see_details">Xem chi tiết</p>
              <img class="next_item_img" src="../images/next_item.png" alt="">
              <a href="https://chamcong.timviec365.vn/uu-diem-vuot-troi.html#antoan" class="link_detail"></a>
            </div>
          </div>
          <div class="item_body_overview margin_10">
            <div class="item_advantages">
              <img class="icon_img_tq" src="../images/pc.png" alt="">
              <p class="line_h19 font_s16 font_w500 color_grey padding_tb_15_10">Một nền tảng duy nhất</p>
              <p class="line_h19 font_s16 color_grey description_item_body_overview text_a_j">Tích hợp tất
                cả
                các
                ứng dụng doanh nghiệp của bạn đang cần trên một nền tảng duy nhất.</p>
            </div>
            <div class="d_flex space_b align_c button_see_details">
              <p class="color_blue line_h19 font_s15 margin_a see_details">Xem chi tiết</p>
              <img class="next_item_img" src="../images/next_item.png" alt="">
              <a href="https://chamcong.timviec365.vn/uu-diem-vuot-troi.html#tichhop" class="link_detail"></a>
              <a href="http://"></a>
            </div>
          </div>
          <div class="item_body_overview margin_10">
            <div class="item_advantages">
              <img class="icon_img_tq" src="../images/technology.png" alt="">
              <p class="line_h19 font_s16 font_w500 color_grey padding_tb_15_10">Ứng dụng công nghệ AI</p>
              <p class="line_h19 font_s16 color_grey description_item_body_overview text_a_j">Ứng dụng
                công
                nghệ
                AI tự
                nhận thức. Phân tích hành vi người dùng giải quyết toàn diện bài các toán đối với doanh
                nghiệp
                cụ
                thể.</p>
            </div>
            <div class="d_flex space_b align_c button_see_details">
              <p class="color_blue line_h19 font_s15 margin_a see_details">Xem chi tiết</p>
              <img class="next_item_img" src="../images/next_item.png" alt="">
              <a href="https://chamcong.timviec365.vn/uu-diem-vuot-troi.html#ungdung" class="link_detail"></a>
              <a href="http://"></a>
            </div>
          </div>
          <div class="item_body_overview margin_10">
            <div class="item_advantages">
              <img class="icon_img_tq" src="../images/rocket.png" alt="">
              <p class="line_h19 font_s16 font_w500 color_grey padding_tb_15_10">Giải pháp số 1 Việt Nam
              </p>
              <p class="line_h19 font_s16 color_grey description_item_body_overview text_a_j">Luôn đồng
                hành
                và hỗ
                trợ
                24/7. Phù hợp với tất cả các tập đoàn xuyên quốc gia đến những công ty SME.</p>
            </div>
            <div class="d_flex space_b align_c button_see_details">
              <p class="color_blue line_h19 font_s15 margin_a see_details">Xem chi tiết</p>
              <img class="next_item_img" src="../images/next_item.png" alt="">
              <a href="https://chamcong.timviec365.vn/uu-diem-vuot-troi.html#donghanh" class="link_detail"></a>
              <a href="http://"></a>
            </div>
          </div>
          <div class="item_body_overview margin_10">
            <div class="item_advantages">
              <img class="icon_img_tq" src="../images/money.png" alt="">
              <p class="line_h19 font_s16 font_w500 color_grey padding_tb_15_10">Sử dụng miễn phí trọn đời
              </p>
              <p class="line_h19 font_s16 color_grey description_item_body_overview text_a_j">Miễn phí
                trọn
                đời
                đối
                với tất cả các doanh nghiệp đăng ký trong đại dịch covid-19.</p>
            </div>
            <div class="d_flex space_b align_c button_see_details">
              <p class="color_blue line_h19 font_s15 margin_a see_details">Xem chi tiết</p>
              <img class="next_item_img" src="../images/next_item.png" alt="">
              <a href="https://chamcong.timviec365.vn/uu-diem-vuot-troi.html#mienphi" class="link_detail"></a>
              <a href="http://"></a>
            </div>
          </div>
        </div>
      </div>
    </div>
    <?php include('../includes/popup_overview.php');  ?>
  </div>
</body>
<script type="text/javascript" src="../js/jquery-3.4.1.min.js"></script>
<script type="text/javascript" src="../js/jquery.validate.min.js"></script>
<script type="text/javascript" src="../js/select2.min.js"></script>
<script type="text/javascript" src="../js/js_h.js"></script>
<script type="text/javascript" src="../js/validate_h.js"></script>
<script type="text/javascript" src="../js/js_k.js"></script>
<script type="text/javascript" src="../js/js_n.js"></script>
<script>

</script>

</html>