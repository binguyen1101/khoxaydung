<footer class="footer">
  <div class="footer_box ">
    <div class="footer_left" >
      <div class="box_footer_left">
        <div class="left_ttct">
          <p class="dvcq">Đơn vị chủ quản:</p>
          <p class="name_company">
            Công ty Cổ phần Thanh toán Hưng Hà
          </p>
          <div class="left_ttlh">
            <div class="item_ttlh">
              <div class="img">
                <img src="../images/nha.png" alt="Đia chỉ" />
              </div>
              <div class="text_ttlh">
                <p class="thep_ttlh">
                  Địa chỉ 1: Tầng 4, B50, Lô 6, KĐT Định Công - Hoàng Mai - Hà Nội
                </p>
                <p class="thep_ttlh">
                  Địa chỉ 2: Thôn Thanh Miếu, Xã Việt Hưng, Huyện Văn Lâm, Tỉnh Hưng Yên
                </p>
                  <p class="thep_ttlh">
                  Địa chỉ 3: Số 1 đường Trần Nguyên Đán, Khu Đô Thị Định Công, Hoàng Mai, Hà Nội
                </p>
              </div>
            </div>
            <div class="item_ttlh align_c">
              <div class="img">
                <img src="../images/call.png" alt="Điên thoại" />
              </div>
              <div class="text_ttlh">
                <p class="thep_ttlh">
                  Hotline: 1900633682 - ấn phím 1
                </p>
              </div>
            </div>
            <div class="item_ttlh align_c">
                <div class="img">
                  <img src="../images/thu.png" alt="gmail" />
                </div>
                <div class="text_ttlh">
                  <p class="thep_ttlh">
                    Email hỗ trợ: timviec365.vn@gmail.com
                  </p>
                </div>
            </div>
          </div>
        </div>
        <div class="left_menu_li">
          <div class="box_left_menu_li">
            <div class="item_left_li">
              <div class="img">
                <img src="../images/icon2.png" alt="Về chúng tôi" />
              </div>
              <div class="name_left_li">
                <a rel="nofollow" href="https://timviec365.vn/gioi-thieu-cong-ty.html" target="_blank"></a>
                Về chúng tôi
              </div>
            </div>
            <div class="item_left_li">
              <div class="img">
                <img src="../images/icon2.png" alt="Quy trình giải quyết tranh chấp" />
              </div>
              <div class="name_left_li">
              <a rel="nofollow" href="https://timviec365.vn/giai-quyet-tranh-chap.html" target="_blank"></a>
                Quy trình giải quyết tranh chấp
              </div>
            </div>
            <div class="item_left_li">
              <div class="img">
                <img src="../images/icon2.png" alt="Giới thiệu chung" />
              </div>
              <div class="name_left_li">
              <a rel="nofollow" href="https://timviec365.vn/gioi-thieu-chung.html" target="_blank"></a>
                Giới thiệu chung
              </div>
            </div>
            <div class="item_left_li">
              <div class="img">
                <img src="../images/icon2.png" alt="Thỏa thuận sử dụng" />
              </div>
              <div class="name_left_li">
              <a rel="nofollow" href="https://timviec365.vn/thoa-thuan-su-dung.html" target="_blank"></a>
                Thỏa thuận sử dụng
              </div>
            </div>
            <div class="item_left_li">
              <div class="img">
                <img src="../images/icon2.png" alt="Sơ đồ website" />
              </div>
              <div class="name_left_li">
              <a rel="nofollow" href="https://timviec365.vn/so-do-trang-web.html" target="_blank"></a>
                Sơ đồ website
              </div>
            </div>
            <div class="item_left_li">
              <div class="img">
                <img src="../images/icon2.png" alt="Quy định bảo mật" />
              </div>
              <div class="name_left_li">
              <a rel="nofollow" href="https://timviec365.vn/quy-dinh-bao-mat.html" target="_blank"></a>
                Quy định bảo mật
              </div>
            </div>
            <div class="item_left_li">
              <div class="img_logo">
                  <a rel="nofollow" href="http://online.gov.vn/(X(1)S(vtdc1kxfxfpr01yxjd0qzbzi))/Home/WebDetails/35979?AspxAutoDetectCookieSupport=1"><img src="../images/anh1.png" alt="Đã đăng ký bộ công thương" /></a>
                  <a rel="nofollow" href="https://www.dmca.com/Protection/Status.aspx?ID=5b1070f1-e6fb-4ba4-8283-84c7da8f8398"><img src="../images/anh2.png" alt="DMCA.com Protection Status"/></a>
              </div>
            </div>
          </div>
        </div>
        <!-- <div class="list_img_1024">
          <div class="img_12">
            <div class="img_red_yel">
                <a href="http://online.gov.vn/(X(1)S(vtdc1kxfxfpr01yxjd0qzbzi))/Home/WebDetails/35979?AspxAutoDetectCookieSupport=1"><img src="../images/anh1.png"  /></a>
                <a href="https://www.dmca.com/Protection/Status.aspx?ID=5b1070f1-e6fb-4ba4-8283-84c7da8f8398"><img src="../images/anh2.png" /></a>
            </div>
            <div class="form_btn_downl">
                <a rel="nofollow" href="https://play.google.com/store/apps/details?id=vn.timviec365.myapplication&hl=vi&gl=US"><div class="item_down">
                <img src="../images/down1.png">
                <p>Download app Timviec365</p>
              </div></a>
                <a rel="nofollow" href="https://play.google.com/store/apps/details?id=com.hungha.appcv365&hl=vi&gl=US"><div class="item_down">
                <img src="../images/down2.png">
                <p>Download app CV365</p> 
              </div></a>
            </div>
          </div>
          <div class="img3">
              <img src="../images/phone.png" alt="">
            </div>
        </div> -->
      </div>
    </div>

    <div class="footer_right">
      <div class="box_footer_right">
        <div class="right_code">
          <p class="title_right_code">Tải app để tìm việc làm siêu tốc</p>
          <p class="title_right_code">Tạo CV đẹp với 365+ mẫu CV xin việc</p>
          <div class="box_img_code">
            <div class="item_img_code d_flex align_c flex_column">
              <img src="../images/barcode.png" alt="App Timviec365" />
              <p>App Timviec365</p>
            </div>
            <div class="item_img_code d_flex align_c flex_column">
              <img src="../images/barcode1.png" alt="App CV365" />
              <p>App CV365</p>
            </div>
          </div>
          <div class="form_btn_downl">
              <a rel="nofollow" href="https://play.google.com/store/apps/details?id=vn.timviec365.myapplication&hl=vi&gl=US"><div class="item_down">
              <img src="../images/down1.png" alt="app tìm việc 365">
              <p>Download app Timviec365</p>
            </div></a>
              <a rel="nofollow" href="https://play.google.com/store/apps/details?id=com.hungha.appcv365&hl=vi&gl=US"><div class="item_down">
              <img src="../images/down2.png" alt="app CV365">
              <p>Download app CV365</p> 
            </div></a>
          </div>
        </div>
        <div class="right_img_ct">
          <img src="../images/phone.png" alt="map">
        </div>
      </div>
    </div>
  </div>
  <div class="det_l">
    <div class="det_link">
      <div class="font_weight_bold"><p>Top ngành nghề:</p></div>
      <div class="list_det_link">
        <a href="https://timviec365.vn/viec-lam-ban-hang-c10v0"> Tìm việc làm bán hàng </a>
        <a href="https://timviec365.vn/tim-viec-lam-them.html"> Tìm việc làm thêm </a>
        <a href="https://timviec365.vn/viec-lam-nhan-vien-kinh-doanh-c9v0"> Tìm việc làm kinh doanh </a>
        <a href="https://timviec365.vn/viec-lam-bao-hiem-c66v0"> Tìm việc làm bảo hiểm  </a>
        <a href="https://timviec365.vn/viec-lam-kd-bat-dong-san-c33v0"> Tuyển dụng Kinh Doanh Bất động sản </a>
        <!-- rel="dofollow" target="_blank" -->
      </div>
    </div>
    <div class="det_ic">
      <p>Theo dõi chúng tôi trên mạng xã hội :</p>
      <div class="link_logo">
          <a href="https://www.facebook.com/Timviec365.Vn/"><i class="fa"><img src="../images/fb.png" alt="facebook" /></i></a>
          <a href="https://twitter.com/timviec365vn"><i class="fa"><img src="../images/tw.png" alt="twitter" /></i></a>
          <a href="https://www.youtube.com/channel/UCI6_mZYL8exLuvmtipBFrkg/videos"><i class="fa"><img src="../images/yt.png" alt="youtube" /></i></a>
      </div>
    </div>
  </div>
</footer>
