<div class="d_flex align_c nav_header" id="nav_header">
  <div class="position_r cursor_p" onclick="toggle('')">
    <img class="header_img_b" src="../images/messes.png" alt="" style="display:block">
    <img class="header_img_w display_none" src="../images/messes_w.png" alt="">
    <img class="position_a notifi_sl" src="../images/notifi_sl.png" alt="">
  </div>
  <div class="position_r event_header_overview event_header_overview_nhac_nho cursor_p" onclick="toggle('popup_nhac_nho')">
    <img class="header_img_b" src="../images/warning.png" alt="" style="display:block">
    <img class="header_img_w display_none" src="../images/remind_w.png" alt="">
    <img class="position_a notifi_sl" src="../images/notifi_sl.png" alt="">
  </div>
  <div class="position_r event_header_overview event_header_overview_thong_bao cursor_p" onclick="toggle('popup_thong_bao')">
    <img class="header_img_b" src="../images/notification.png" alt="" style="display:block">
    <img class="header_img_w display_none" src="../images/notification_w.png" alt="">
    <img class="position_a notifi_sl_tq" src="../images/notifi_sl.png" alt="">
  </div>
  <div class="d_flex align_c">
    <div class="d_flex align_c info_header" onclick="toggle('header_popup_item')">
      <? if (isset($_COOKIE['acc_token']) && isset($_COOKIE['role']) && isset($_COOKIE['rf_token'])) {
        if (isset($_COOKIE['user']) && $_COOKIE['user'] != "" && $_COOKIE['role'] == 1) {
          $token = $_COOKIE['acc_token'];
          $curl = curl_init();
          $data = array();
          curl_setopt($curl, CURLOPT_POST, 1);
          curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
          curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/user_info_company.php');
          curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
          curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
          curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));

          $response = curl_exec($curl);
          curl_close($curl);
          $data_tt = json_decode($response, true);
          $tt_user = $data_tt['data']['user_info_result'];
          // print_r($tt_user);

      ?>
          <div class="d_flex user_company" style="display: flex;">
            <? if (isset($tt_user['com_logo']) && $tt_user['com_logo'] != "") { ?>
              <img style="margin-left: 25px; margin-right : 15px;" src="https://chamcong.24hpay.vn/upload/company/logo/<?= $tt_user['com_logo'] ?>" class="avt_nv_dn" alt="ảnh đại diện">
            <? } else { ?>
              <img style="margin-left: 25px; margin-right : 15px;" src="../img/avt4.png" class="avt_nv_dn" alt="ảnh đại diện">
            <? } ?>
            <div class="d_flex align_c">
              <p class="name_header_overview font_s16 line_h28 color_grey"><?= $tt_user['com_name'] ?></p>
              <p> | </p>
              <p class="line_h28 font_s16 font_w500 color_grey id_header_overview">ID: <?= $tt_user['com_id'] ?></p>
            </div>
          </div>
        <? }
        if (isset($_COOKIE['user']) && $_COOKIE['user'] != "" && $_COOKIE['role'] == 2) {
          $token = $_COOKIE['acc_token'];
          $curl = curl_init();
          $data = array();
          curl_setopt($curl, CURLOPT_POST, 1);
          curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
          curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/user_info_employee.php');
          curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
          curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
          curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));

          $response = curl_exec($curl);
          curl_close($curl);
          $data_tt = json_decode($response, true);
          $tt_user = $data_tt['data']['user_info_result'];
        ?>
          <div class="color_grey line_h28 font_s16 user_member display_none" <?php if (isset($_COOKIE['user']) && $_COOKIE['user'] != "" && $_COOKIE['role'] == 2) { ?>style="display: flex;" <?php } ?>>
            <? if (isset($tt_user['ep_image']) && $tt_user['ep_image'] != "") { ?>
              <img style="margin-left: 25px; margin-right : 15px;" src="https://chamcong.24hpay.vn/upload/employee/<?= $tt_user['ep_image'] ?>" class="avt_nv_dn" alt="ảnh đại diện">
            <? } else { ?>
              <img src="../img/avt4.png" class="avt_nv_dn" alt="ảnh đại diện">
            <? } ?>
            <?= $tt_user['ep_name'] ?>
          </div>
      <? }
      }
      ?>
    </div>
    <div class="position_r">
      <img class="list_item_header cursor_p" src="../images/list_item.png" alt="" onclick="toggle('header_popup_item')">
      <img class="img_header display_none" src="../images/ava_w.png" alt="" onclick="toggle('header_popup_item')">
      <div class="position_a backgroud_popup_item_w200 header_popup_item box_s_d" style="display: none; z-index: 3">
        <div class="item_header d_flex align_c cursor_p">
          <img class="img_item_header img_b" src="../images/user_b.png" alt="">
          <img style="display: none;" class="img_item_header img_w" src="../images/user_w.png" alt="">
          <p class="color_grey line_h28 font_s14 title_item_header">Thông tin tài khoản</p>
        </div>
        <div class="item_header d_flex align_c cursor_p">
          <img class="img_item_header img_b" src="../images/pen_b.png" alt="">
          <img style="display: none;" class="img_item_header img_w" src="../images/pen_w.png" alt="">
          <p class="color_grey line_h28 font_s14 title_item_header">Đánh giá</p>
        </div>
        <div class="item_header d_flex align_c cursor_p">
          <img class="img_item_header img_b" src="../images/danger_b.png" alt="">
          <img style="display: none;" class="img_item_header img_w" src="../images/danger_w.png" alt="">
          <p class="color_grey line_h28 font_s14 title_item_header">Báo lỗi</p>
        </div>
        <div class="item_header d_flex align_c cursor_p">
          <img class="img_item_header img_b" src="../images/instruct_b.png" alt="">
          <img style="display: none;" class="img_item_header img_w" src="../images/instruct_w.png" alt="">
          <p class="color_grey line_h28 font_s14 title_item_header">Hướng dẫn</p>
        </div>
        <div class="item_header d_flex align_c cursor_p">
          <img class="img_item_header img_b" src="../images/logout_b.png" alt="">
          <img style="display: none;" class="img_item_header img_w" src="../images/logout_w.png" alt="">
          <p class="color_grey line_h28 font_s14 title_item_header" onclick="openAndHide('header_popup_item','popup_log_out')">Đăng xuất</p>
        </div>
      </div>
    </div>
  </div>
</div>