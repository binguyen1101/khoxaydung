<div class="header_top back_blue d_flex space_b align_c">
    <div class="icon_header cursor_p">
        <div>
            <span class="icon_header_tbl"></span>
            <span class="icon_header_tbl"></span>
            <span class="icon_header_tbl"></span>
        </div>
        <div class="menu_tabl">
            <ul class="menu_resp d_flex flex_column">
                <li>
                    <img src="../images/home.png" alt="">
                    <a href="/trang-chu.html" class="font_s16 line_h19 color_white font_w700">Trang chủ</a>
                </li>
                <li>
                    <img src="../images/instruct.png" alt="">
                    <a href="/trang-huong-dan.html" class="font_s16 line_h19 color_white font_w700">Hướng dẫn</a>
                </li>
                <li>
                    <img src="../images/new.png" alt="">
                    <a href="https://timviec365.vn/blog/c256/quan-ly-kho-hang" class="font_s16 line_h19 color_white font_w700">Tin tức</a>
                </li>
                <li>
                    <img src="../images/register.png" alt="">
                    <a rel="nofollow" href="https://quanlychung.timviec365.vn/lua-chon-dang-ky.html" class="font_s16 line_h19 color_white font_w700">Đăng kí</a>
                </li>
                <li>
                    <img src="../images/log_ing.png" alt="">
                    <a rel="nofollow" href="https://quanlychung.timviec365.vn/lua-chon-dang-nhap.html" class="font_s16 line_h19 color_white font_w700">Đăng nhập</a>
                </li>
            </ul>
        </div>
    </div>
    <div class="logo_web cursor_p">
        <a href="https://timviec365.vn"><img src="../images/logo.png" alt="timviec365.vn"></a>
    </div>
    <div class="menu_header d_flex align_c">
        <ul class="menu_ul d_flex space_b align_c">
            <li>
                <a href="/trang-chu.html" class="active_menu font_s16 line_h19 color_white font_w700">Trang chủ</a>
            </li>
            <li>
                <a href="/trang-huong-dan.html" class="font_s16 line_h19 color_white font_w700">Hướng dẫn</a>
            </li>
            <li>
                <a href="https://timviec365.vn/blog/c256/quan-ly-kho-hang" class="font_s16 line_h19 color_white font_w700">Tin tức</a>
            </li>
        </ul>
        <div class="log_in-out">
            <? if (isset($_COOKIE['acc_token']) && isset($_COOKIE['role']) && isset($_COOKIE['rf_token'])) {
                if (isset($_COOKIE['user']) && $_COOKIE['user'] != "" && $_COOKIE['role'] == 1) {
                    $token = $_COOKIE['acc_token'];
                    $curl = curl_init();
                    $data = array();
                    curl_setopt($curl, CURLOPT_POST, 1);
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                    curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/user_info_company.php');
                    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
                    curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));

                    $response = curl_exec($curl);
                    curl_close($curl);
                    $data_tt = json_decode($response, true);
                    $tt_user = $data_tt['data']['user_info_result'];

            ?>
                    <div class="bg_log_aff" data="<?= $tt_user['com_id'] ?>">
                        <div class="bg_log_img">
                            <? if (isset($tt_user['ep_image']) && $tt_user['ep_image'] != "") { ?>
                                <img src="https://chamcong.24hpay.vn/upload/employee/<?= $tt_user['com_logo'] ?>" class="avt_nv_dn" alt="ảnh đại diện">
                            <? } else { ?>
                                <img src="../img/avt4.png" class="avt_nv_dn" alt="ảnh đại diện">
                            <? } ?>
                            <div class="box_user_name">
                                <a href="https://phanmemquanlykhoxaydung.timviec365.vn/tong-quan.html">
                                    <p class="share_clr_tow share_fsize_tow ml_10 ten_nv_dn" style="color: #ffffff;"><?= $tt_user['com_name'] ?></p>
                                </a>
                            </div>
                        </div>
                    </div>
                <? }
                if (isset($_COOKIE['user']) && $_COOKIE['user'] != "" && $_COOKIE['role'] == 2) {
                    $token = $_COOKIE['acc_token'];
                    $curl = curl_init();
                    $data = array();
                    curl_setopt($curl, CURLOPT_POST, 1);
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                    curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/user_info_employee.php');
                    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
                    curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));

                    $response = curl_exec($curl);
                    curl_close($curl);
                    $data_tt = json_decode($response, true);
                    $tt_user = $data_tt['data']['user_info_result'];

                ?>
                    <div class="bg_log_aff">
                        <div class="bg_log_img d_flex align_c" data="<?= $tt_user['ep_id'] ?>">
                            <? if (isset($tt_user['ep_image']) && $tt_user['ep_image'] != "") { ?>
                                <img src="https://chamcong.24hpay.vn/upload/employee/<?= $tt_user['ep_image'] ?>" class="avt_nv_dn" alt="ảnh đại diện">
                            <? } else { ?>
                                <img src="../img/avt4.png" class="avt_nv_dn" alt="ảnh đại diện">
                            <? } ?>
                            <div class="box_user_name">
                                <a href="https://phanmemquanlykhoxaydung.timviec365.vn/tong-quan.html">
                                    <p class="share_clr_tow share_fsize_tow ml_10 ten_nv_dn" style="color: #ffffff;"><?= $tt_user['ep_name'] ?></p>
                                </a>
                            </div>
                        </div>
                        <!-- <div class="bg_logout">
                            <div class="chd_content">
                                <p class="chuyen_doi">
                                    <a href="quan-ly-trang-chu.html">Quản lý cung ứng</a>
                                </p>
                                <p class="dang_xuat btx_logout share_cursor">
                                    <a>Đăng xuất</a>
                                </p>
                            </div>
                        </div> -->
                    </div>
                <? }
            }
            if (!isset($_COOKIE['acc_token']) && !isset($_COOKIE['role']) && !isset($_COOKIE['rf_token'])) { ?>
                <ul class="menu_log d_flex">
                    <li>
                        <a rel="nofollow" href="https://quanlychung.timviec365.vn/lua-chon-dang-ky.html" class="font_s16 line_h19 color_white font_w700">Đăng
                            kí</a>
                    </li>
                    <span class="font_s16 line_h19 color_white font_w700">&nbsp;/&nbsp;</span>
                    <li>
                        <a rel="nofollow" href="https://quanlychung.timviec365.vn/lua-chon-dang-nhap.html" class="font_s16 line_h19 color_white font_w700">Đăng nhập</a>
                    </li>
                </ul>
            <? } ?>
        </div>
    </div>
</div>
<script>
    var menuActive = $('.menu_ul li');
    menuActive.click(function() {
        $(this).parent().find('a').removeClass('active_menu');
        $(this).find('a').addClass('active_menu');
    });
</script>