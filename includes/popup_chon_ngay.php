<!-- Chọn ngày -->
<div class="popup_show popup_n popup_select_date" id="popup_select_date" style="display: none;">
    <div class="box_popup">
        <div class="box_content">
            <div class="header_box back_blue">
                <div class="tit_head position_r">
                    <p class="color_white font_s16 line_h19 font_w700 text_a_c">Chọn thời gian
                    </p>
                    <p class="color_white font_s16 line_h19 font_w700 text_a_c" style="display: none;">Chỉnh sửa nhóm vật tư thiết bị
                    </p>
                    <img class="close_popup position_a cursor_p" src="../images/close_w.png" alt="" style="top: 0; right: 0;">
                </div>
            </div>
            <div class="content_popup">
                <div class="d_flex flex_column mb_15">
                    <p class="color_grey font_s15 line_h18 font_w500">Từ ngày</p>
                    <div class="position_r">
                        <input type="text" placeholder="Chọn ngày" onfocusin="(this.type='date')" onfocusout="(this.type='text')">    
                        <img class="img_date" src="../images/select_date.png" alt="">
                    </div>
                </div>
                <div class="d_flex flex_column position_r">
                    <p class="color_grey font_s15 line_h18 font_w500">Đến ngày</p>                    
                    <div class="position_r">
                        <input type="text" placeholder="Chọn ngày" onfocusin="(this.type='date')" onfocusout="(this.type='text')">
                        <img class="img_date" src="../images/select_date.png" alt="">
                    </div>
                </div>
                <div class="btn_ct_pp d_flex flex_center">
                    <button class="btn_cancel back_w color_blue font_s15 line_h18 font_w500 cursor_p">Hủy</button>
                    <button class="btn_save back_blue color_white font_s15 line_h18 font_w500 cursor_p">Đồng ý</button>
                </div>
            </div>
        </div>
    </div>
</div>