<!-- popup thêm mới,chỉnh sửa kho -->
<?php if(in_array(2,$ro_ton_kho)){ ?>
    <div class="popup_show popup_n popup_func_dv_add popup_add_wh seclec2_radius" id="popup_add_wh" style="display: none;">
        <div class="box_popup">
            <div class="box_content">
                <div class="header_box back_blue">
                    <div class="tit_head position_r">
                        <p class="color_white font_s16 line_h19 font_w700 text_a_c">Thêm mới kho
                        </p>
                        <img class="close_popup position_a cursor_p" src="../images/close_w.png" alt="" style="top: 0; right: 0;">
                    </div>
                </div>
                <div class="content_popup">
                    <form id="f_add_wh">
                        <div class="d_flex flex_column" style="display: none;">
                            <p class="color_grey font_s15 line_h18 font_w500">Mã kho</p>
                            <input class="color_grey font_s14 line_h17 font_w400" type="text" value="NVT-0000" disabled="disabled">
                        </div>
                        <div class="d_flex flex_column name_wh">
                            <p class="color_grey font_s15 line_h18 font_w500">Tên kho<span style="color: red;">*</span></p>
                            <input name="name_wh" class="color_grey font_s14 line_h17 font_w400" type="text" placeholder="Nhập tên kho">
                        </div>
                        <div class="d_flex flex_column">
                            <p class="color_grey font_s15 line_h18 font_w500 ">Công trình</p>
                            <select class="select_construction" name="wh_construction">
                                <option value="">Chọn công trình</option>
                                <?php foreach($list_phieu_vt1 as $val_ct){ ?>
                                    <option value="<?= $val_ct['ctr_id']?>"><?= $val_ct['ctr_name']?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="d_flex flex_column address_wh">
                            <p class="color_grey font_s15 line_h18 font_w500">Địa chỉ<span style="color: red;">*</span></p>
                            <input name="address_wh" class="color_grey font_s14 line_h17 font_w400" type="text" placeholder="Nhập địa chỉ kho">
                        </div>
                        <div class="d_flex flex_column staff_wh">
                            <p class="color_grey font_s15 line_h18 font_w500">Nhân viên quản lí kho<span style="color: red;">*</span></p>
                            <select class="select_staff" name="staff_wh">
                                <option value=""> Chọn nhân viên</option>
                                <? foreach ($data_list_nv as $item) { ?>
                                    <option value="<?= $item['ep_id'] ?>"><?= $item['ep_name'] ?></option>
                                <? } ?>                            
                            </select>
                        </div>
                        <div class="d_flex flex_column">
                            <p class="color_grey font_s15 line_h18 font_w500">Mô tả </p>
                            <textarea name="description_wh" id="" rows="5" placeholder="Nhập nội dung"></textarea>
                        </div>
                        <div class="btn_ct_pp d_flex flex_center">
                            <button type="button" class="btn_cancel back_w color_blue font_s15 line_h18 font_w500 cursor_p">Hủy</button>
                            <button type="button" class="btn_save btn_add_kho back_blue color_white font_s15 line_h18 font_w500 cursor_p">Đồng ý</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
<?php }?>

<?php if(in_array(3,$ro_ton_kho)){ ?>
    <div class="popup_show popup_n popup_func_dv_edit popup_edit_wh seclec2_radius" id="popup_edit_wh" style="display: none;">
        <div class="box_popup">
            <div class="box_content">
                <div class="header_box back_blue">
                    <div class="tit_head position_r">
                        <p class="color_white font_s16 line_h19 font_w700 text_a_c">Chỉnh sửa kho
                        </p>
                        <img class="close_popup position_a cursor_p" src="../images/close_w.png" alt="" style="top: 0; right: 0;">
                    </div>
                </div>
                <div class="content_popup">
                    <form id="f_add_wh">
                        
                    </form>
                </div>
            </div>
        </div>
    </div>
<?php }?>



<!-- popup thông báo thành công thất bại -->
<div class="popup_show popup_ex_st  popup_add_wh_succ" id="popup_add_wh_succ" style="display: none;">
    <div class="box_popup">
        <div class="box_content d_flex flex_column align_c">
            <img class="img_p_succ" src="../images/st_tc.png" alt="">
            <p class="p_add_succ color_grey font_s15 line_h24 text_a_c" style="display: block;">Thêm mới Kho <strong class="tenKho"></strong>
                thành công!</p>
            <button class="btn_close back_blue color_white font_s15 line_h18 font_w500 cursor_p">Đóng
                <a href="/danh-sach-kho.html"></a>
            </button>
        </div>
    </div>
</div>

<div class="popup_show popup_ex_st  popup_edit_wh_succ" id="popup_edit_wh_succ" style="display: none;">
    <div class="box_popup">
        <div class="box_content d_flex flex_column align_c">
            <img class="img_p_succ" src="../images/st_tc.png" alt="">
            <p class="p_add_succ color_grey font_s15 line_h24 text_a_c" style="display: block;">Chỉnh sửa Kho <strong class="tenKho"></strong>
                thành công!</p>
            <button class="btn_close back_blue color_white font_s15 line_h18 font_w500 cursor_p">Đóng
                <a href="/danh-sach-kho.html"></a>
            </button>
        </div>
    </div>
</div>

<div class="popup_show popup_ex_st popup_ex_lose popup_add_wh_false" id="popup_add_wh_false" style="display: none;">
    <div class="box_popup">
        <div class="box_content d_flex flex_column align_c">
            <img class="img_p_lose" src="../images/st_tb.png" alt="">
            <p class="p_add_lose color_grey font_s15 line_h24 text_a_c">Thêm mới kho <strong class="tenKho"></strong>
                thất bại!</p>
            <button class="btn_close back_blue color_white font_s15 line_h18 font_w500 cursor_p">Đóng</button>
        </div>
    </div>
</div>
<!-- popup thiết lập tồn kho -->
<div class="popup_show popup_n popup_func_dv_add popup_add_wh_set " id="popup_add_wh_set" style="display: none;" data-kho="<?=$id_kho?>">
    <div class="box_popup">
        <div class="box_content">
            <div class="header_box back_blue">
                <div class="tit_head position_r">
                    <p class="color_white font_s16 line_h19 font_w700 text_a_c">Thiết lập tồn kho
                    </p>
                    <img class="close_popup position_a cursor_p" src="../images/close_w.png" alt="" style="top: 0; right: 0;">
                </div>
            </div>
            <div class="content_popup">
               
            </div>
        </div>
    </div>
</div>

<!--  -->
<div class="popup_show popup_ex_st  popup_set_wh_succ" id="popup_set_wh_succ" style="display: none;">
    <div class="box_popup">
        <div class="box_content d_flex flex_column align_c">
            <img class="img_p_succ" src="../images/st_tc.png" alt="">
            <p class="p_add_succ color_grey font_s15 line_h24 text_a_c" style="display: block;">Thiết lập tồn kho <strong class="set"></strong>
                thành công!</p>
            <button class="btn_close back_blue color_white font_s15 line_h18 font_w500 cursor_p">Đóng
                <a href=""></a>
            </button>
        </div>
    </div>
</div>

<div class="popup_show popup_ex_st  popup_set_wh_false" id="popup_set_wh_false" style="display: none;">
    <div class="box_popup">
        <div class="box_content d_flex flex_column align_c">
            <img class="img_p_lose" src="../images/st_tb.png" alt="">
            <p class="p_add_lose color_grey font_s15 line_h24 text_a_c">Thiết lập tồn kho <strong>ống nhựa</strong>
                thất bại!</p>
            <button class="btn_close back_blue color_white font_s15 line_h18 font_w500 cursor_p">Đóng</button>
        </div>
    </div>
</div>

