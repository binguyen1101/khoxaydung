
<!-- Xóa phiếu điều chuyển kho -->
<div class="popup_show popup_n popup_del_tf" id="popup_del_tf" style="display: none;">
    <div class="box_popup">
        <div class="box_content">
            <div class="header_box back_blue">
                <div class="tit_head position_r">
                    <p class="color_white font_s16 line_h19 font_w700 text_a_c">Xóa phiếu điều chuyển kho
                    </p>
                    <img class="close_popup position_a cursor_p" src="../images/close_w.png" alt="" style="top: 0; right: 0;">
                </div>
            </div>
            <div class="content_popup">
                <p class="del_w text_a_c color_grey font_s15 line_h21 font_w400">Bạn có chắc chắn muốn xóa phiếu điều chuyển kho <br><strong>ĐCK-<?=$id_phieu?></strong>?</p>
                <div class="btn_ct_pp d_flex flex_center">
                    <button class="btn_cancel back_w color_blue font_s15 line_h18 font_w500 cursor_p">Hủy</button>
                    <button class="btn_save back_blue color_white font_s15 line_h18 font_w500 cursor_p">Đồng ý</button>
                </div>
            </div>
        </div>
    </div>
</div>

<!--  Xóa vĩnh viễn ----------------------  -->
<div class="popup_show popup_n popup_del_w" id="popup_del_w" data-id="" style="display: none;">
    <div class="box_popup">
        <div class="box_content">
            <div class="header_box back_blue">
                <div class="tit_head position_r">
                    <p class="color_white font_s16 line_h19 font_w700 text_a_c">Xóa vĩnh viễn <span class="tit_deleted">vật tư thiết bị</span>
                    </p>
                    <img class="close_popup position_a cursor_p" src="../images/close_w.png" alt="" style="top: 0; right: 0;">
                </div>
            </div>
            <div class="content_popup">
                <p class="del_w text_a_c color_grey font_s15 line_h21 font_w400">Bạn có chắc chắn muốn xóa vĩnh viễn <span class="tit_deleted">vật tư thiết bị</span> <br><strong>ĐCK-0000</strong>?</p>
                <div class="btn_ct_pp d_flex flex_center">
                    <button class="btn_cancel back_w color_blue font_s15 line_h18 font_w500 cursor_p">Hủy</button>
                    <button class="btn_save back_blue color_white font_s15 line_h18 font_w500 cursor_p">Đồng ý</button>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="popup_show popup_n popup_dels_w" id="popup_dels_w" style="display: none;">
    <div class="box_popup">
        <div class="box_content">
            <div class="header_box back_blue">
                <div class="tit_head position_r">
                    <p class="color_white font_s16 line_h19 font_w700 text_a_c">Xóa vĩnh viễn <span class="tit_deleted">vật tư thiết bị</span>
                    </p>
                    <img class="close_popup position_a cursor_p" src="../images/close_w.png" alt="" style="top: 0; right: 0;">
                </div>
            </div>
            <div class="content_popup">
                <p class="del_w text_a_c color_grey font_s15 line_h21 font_w400">Bạn có chắc chắn muốn xóa vĩnh viễn<br><strong>4</strong> <span class="tit_deleted">vật tư thiết bị</span> đã chọn?</p>
                <div class="btn_ct_pp d_flex flex_center">
                    <button class="btn_cancel back_w color_blue font_s15 line_h18 font_w500 cursor_p">Hủy</button>
                    <button class="btn_save back_blue color_white font_s15 line_h18 font_w500 cursor_p">Đồng ý</button>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Khôi phục phiếu điều chuyển kho -->
<div class="popup_show popup_n popup_rms_w" id="popup_rms_w" style="display: none;">
    <div class="box_popup">
        <div class="box_content">
            <div class="header_box back_blue">
                <div class="tit_head position_r">
                    <p class="color_white font_s16 line_h19 font_w700 text_a_c">Khôi phục <span class="tit_deleted">vật tư thiết bị</span> đã xóa
                    </p>
                    <img class="close_popup position_a cursor_p" src="../images/close_w.png" alt="" style="top: 0; right: 0;">
                </div>
            </div>
            <div class="content_popup">
                <p class="del_w text_a_c color_grey font_s15 line_h21 font_w400">Bạn có chắc chắn muốn khôi phục <strong>4</strong> <span class="tit_deleted">vật tư thiết bị</span> đã chọn?</p>
                <div class="btn_ct_pp d_flex flex_center">
                    <button class="btn_cancel back_w color_blue font_s15 line_h18 font_w500 cursor_p">Hủy</button>
                    <button class="btn_save back_blue color_white font_s15 line_h18 font_w500 cursor_p">Đồng ý</button>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="popup_show popup_n popup_rm_w" id="popup_rm_w" data-id="" style="display: none;">
    <div class="box_popup">
        <div class="box_content">
            <div class="header_box back_blue">
                <div class="tit_head position_r">
                    <p class="color_white font_s16 line_h19 font_w700 text_a_c">Khôi phục <span class="tit_deleted">vật tư thiết bị</span> đã xóa
                    </p>
                    <img class="close_popup position_a cursor_p" src="../images/close_w.png" alt="" style="top: 0; right: 0;">
                </div>
            </div>
            <div class="content_popup">
                <p class="del_w text_a_c color_grey font_s15 line_h21 font_w400">Bạn có chắc chắn muốn khôi phục <span class="tit_deleted">vật tư thiết bị</span> <br><strong>VT-0000</strong>?</p>
                <div class="btn_ct_pp d_flex flex_center">
                    <button class="btn_cancel back_w color_blue font_s15 line_h18 font_w500 cursor_p">Hủy</button>
                    <button class="btn_save back_blue color_white font_s15 line_h18 font_w500 cursor_p">Đồng ý</button>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Duyệt phiếu điều chuyển kho -->
<div class="popup_show popup_n popup_succ_w" id="popup_succ_w" style="display: none;">
    <div class="box_popup">
        <div class="box_content">
            <div class="header_box back_blue">
                <div class="tit_head position_r">
                    <p class="color_white font_s16 line_h19 font_w700 text_a_c">Duyệt phiếu điều chuyển kho
                    </p>
                    <img class="close_popup position_a cursor_p" src="../images/close_w.png" alt="" style="top: 0; right: 0;">
                </div>
            </div>
            <div class="content_popup">
                <p class="del_w text_a_c color_grey font_s15 line_h21 font_w400">Bạn có chắc chắn muốn duyệt phiếu điều chuyển kho<br><strong>ĐCK-<?=$id_phieu?></strong>?</p>
                <div class="btn_ct_pp d_flex flex_center">
                    <button class="btn_cancel back_w color_blue font_s15 line_h18 font_w500">Hủy</button>
                    <button class="btn_save back_blue color_white font_s15 line_h18 font_w500">Đồng ý</button>
                </div>
            </div>
        </div>
    </div>
</div>

<!--Từ chối phiếu điều chuyển kho -->
<div class="popup_show popup_n popup_ref_w" id="popup_ref_w" style="display: none;">
    <div class="box_popup">
        <div class="box_content">
            <div class="header_box back_blue">
                <div class="tit_head position_r">
                    <p class="color_white font_s16 line_h19 font_w700 text_a_c">Từ chối phiếu điều chuyển kho
                    </p>
                    <img class="close_popup position_a cursor_p" src="../images/close_w.png" alt="" style="top: 0; right: 0;">
                </div>
            </div>
            <div class="content_popup">
                <form action="" method="post" id="f_reason_ref">
                    <div class="name_reason_ref d_flex flex_column">
                        <p class="color_grey font_s15 line_h18 font_w500">Lí do từ chối<span style="color: red;">*</span></p>
                        <textarea name="name_reason_ref" id="" rows="5" placeholder="Nhập nội dung"></textarea>
                    </div>
                    <div class="btn_ct_pp d_flex flex_center">
                        <button type="button" class="btn_cancel back_w color_blue font_s15 line_h18 font_w500 cursor_p">Hủy</button>
                        <button type="button" class="btn_save back_blue color_white font_s15 line_h18 font_w500 cursor_p">Đồng ý</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Thông báo thành công, thất bại -->
<div class="popup_show popup_add_notif_succ" id="popup_add_notif_succ" style="display: none;">
    <div class="box_popup">
        <div class="box_content d_flex flex_column align_c">
            <img class="img_p_succ" src="../images/st_tc.png" alt="" style="display: block;">
            <img class="img_f_succ" src="../images/st_tb.png" alt="" style="display: none;">
            <p class="p_add_succ color_grey font_s15 line_h24 text_a_c" style="display: block;">Xóa vĩnh vĩnh vật tư thiết bị <strong>VT-0000</strong> thành công!</p>
            <button class="btn_close back_blue color_white font_s15 line_h18 font_w500 cursor_p">
                Đóng
            </button>
        </div>
    </div>
</div>

<!-- Hủy thêm mới -->
<div class="popup_show popup_n popup_func_del" id="popup_func_del" style="display: none;">
    <div class="box_popup">
        <div class="box_content">
            <div class="header_box back_blue">
                <div class="tit_head position_r">
                    <p class="color_white font_s16 line_h19 font_w700 text_a_c">Hủy thêm mới
                    </p>
                    <img class="close_popup position_a cursor_p" src="../images/close_w.png" alt="" style="top: 0; right: 0;">
                </div>
            </div>
            <div class="content_popup">
                <p class="color_grey font_s15 line_h21 font_w400 text_a_c">Bạn có chắc chắn muốn hủy thêm mới phiếu điều chuyển kho mọi dữ liệu bạn vừa nhập sẽ không được lưu lại?</p>
                <div class="btn_ct_pp d_flex flex_center">
                    <button class="btn_cancel back_w color_blue font_s15 line_h18 font_w500 cursor_p">Hủy</button>
                    <button class="btn_save back_blue color_white font_s15 line_h18 font_w500 cursor_p">
                    <a href="/dieu-chuyen-kho.html"></a>    
                    Đồng ý</button>
                </div>
            </div>
        </div>
    </div>
</div>