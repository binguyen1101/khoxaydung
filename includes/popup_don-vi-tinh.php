
<!-- Thông báo thêm thành công, thất bại -->
<div class="popup_show popup_add_notif_succ" id="popup_add_notif_succ" style="display: none;">
    <div class="box_popup">
        <div class="box_content d_flex flex_column align_c">
            <img class="img_p_succ" src="../images/st_tc.png" alt="" style="display: block;">
            <p class="p_add_succ color_grey font_s15 line_h24 text_a_c" style="display: block;">Thêm mới đơn vị tính&nbsp<strong>Chai</strong>&nbsp<span>thất bại!</span></p>
            <button type="button" class="btn_close back_blue color_white font_s15 line_h18 font_w500 cursor_p">
                <!-- <a href="/don-vi-tinh.html"></a> -->
                Đóng
            </button>
        </div>
    </div>
</div>

<div class="popup_show popup_add_notif_lose" id="popup_add_notif_lose" style="display: none;">
    <div class="box_popup">
        <div class="box_content d_flex flex_column align_c">
            <img class="img_p_lose" src="../images/st_tb.png" alt="" style="display: block;">
            <p class="p_add_succ color_grey font_s15 line_h24 text_a_c" style="display: block;">Thêm mới đơn vị tính<strong>Chai</strong> <span>thất bại!</span></p>
            <button class="btn_close back_blue color_white font_s15 line_h18 font_w500 cursor_p">Đóng</button>
        </div>
    </div>
</div>

<!-- Thêm, sửa đơn vị tính -->
<div class="popup_show popup_n popup_func_dv_add" id="popup_func_dv_add" style="display: none;">
    <div class="box_popup">
        <div class="box_content">
            <div class="header_box back_blue">
                <div class="tit_head position_r">
                    <p class="color_white font_s16 line_h19 font_w700 text_a_c">Thêm mới đơn vị tính
                    </p>
                    <img class="close_popup position_a cursor_p" src="../images/close_w.png" alt="" style="top: 0; right: 0;">
                </div>
            </div>
            <div class="content_popup">
                <form action="" method="post" class="f_func_add">
                    <!-- <div class="d_flex flex_column mb_15">
                        <p class="color_grey font_s15 line_h18 font_w500">Mã đơn vị tính</p>
                        <input class="color_grey font_s14 line_h17 font_w400" type="text" value="ĐVT-0000" disabled="disabled">
                    </div> -->
                    <div class="name_unit_add d_flex flex_column mb_15">
                        <p class="color_grey font_s15 line_h18 font_w500">Tên đơn vị tính<span style="color: red;">*</span></p>
                        <input class="color_grey font_s14 line_h17 font_w400" type="text" placeholder="Nhập tên đơn vị tính" name="name_unit_add">
                    </div>
                    <div class="d_flex flex_column">
                        <p class="color_grey font_s15 line_h18 font_w500">Mô tả đơn vị tính</p>
                        <textarea name="dep_unit_add" id="" rows="5" placeholder="Nhập nội dung"></textarea>
                    </div>
                    <div class="btn_ct_pp d_flex flex_center">
                        <button type="button" class="btn_cancel back_w color_blue font_s15 line_h18 font_w500 cursor_p">Hủy</button>
                        <button type="button" class="btn_save back_blue color_white font_s15 line_h18 font_w500 cursor_p">Đồng ý</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php if(in_array(3, $ro_dvt)){ ?>
    <div class="popup_show popup_n popup_func_dv_edit" id="popup_func_dv_edit" style="display: none;">
        <div class="box_popup">
            <div class="box_content">
                <div class="header_box back_blue">
                    <div class="tit_head position_r">
                        <p class="color_white font_s16 line_h19 font_w700 text_a_c">Chỉnh sửa đơn vị tính
                        </p>
                        <img class="close_popup position_a cursor_p" src="../images/close_w.png" alt="" style="top: 0; right: 0;">
                    </div>
                </div>
                <div class="content_popup">
                    <form action="" method="post" class="f_func_edit">
                        <!-- <div class="d_flex flex_column mb_15">
                            <p class="color_grey font_s15 line_h18 font_w500">Mã đơn vị tính</p>
                            <input class="color_grey font_s14 line_h17 font_w400" type="text" value="ĐVT-0000" disabled="disabled">
                        </div> -->
                        <div class="name_unit_edit d_flex flex_column mb_15">
                            <p class="color_grey font_s15 line_h18 font_w500">Tên đơn vị tính<span style="color: red;">*</span></p>
                            <input class="color_grey font_s14 line_h17 font_w400" type="text" placeholder="Nhập tên đơn vị tính" name="name_unit_edit">
                        </div>
                        <div class="d_flex flex_column">
                            <p class="color_grey font_s15 line_h18 font_w500">Mô tả đơn vị tính</p>
                            <textarea name="dep_unit_edit" id="" rows="5" placeholder="Nhập nội dung"></textarea>
                        </div>
                        <div class="btn_ct_pp d_flex flex_center">
                            <button type="button" class="btn_cancel back_w color_blue font_s15 line_h18 font_w500 cursor_p">Hủy</button>
                            <button type="button" class="btn_save back_blue color_white font_s15 line_h18 font_w500 cursor_p">Đồng ý</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
<? }?>

<!-- Xóa đơn vị tính -->
<div class="popup_show popup_n popup_func_del" id="popup_func_del" style="display: none;">
    <div class="box_popup">
        <div class="box_content">
            <div class="header_box back_blue">
                <div class="tit_head position_r">
                    <p class="color_white font_s16 line_h19 font_w700 text_a_c">Xóa đơn vị tính
                    </p>
                    <p class="color_white font_s16 line_h19 font_w700 text_a_c" style="display: none;">Chỉnh sửa đơn vị tính
                    </p>
                    <img class="close_popup position_a cursor_p" src="../images/close_w.png" alt="" style="top: 0; right: 0;">
                </div>
            </div>
            <div class="content_popup">
                <p class="color_grey font_s15 line_h21 font_w400 text_a_c">Bạn có chắc chắn muốn xóa đơn vị tính<br><strong>Chai</strong>?</p>
                <div class="btn_ct_pp d_flex flex_center">
                    <button class="btn_cancel back_w color_blue font_s15 line_h18 font_w500 cursor_p">Hủy</button>
                    <button class="btn_save back_blue color_white font_s15 line_h18 font_w500 cursor_p">Đồng ý</button>
                </div>
            </div>
        </div>
    </div>
</div>