<div class="popup popup_cancel_equipment_materials" id="popup_cancel_equipment_materials" style="display: none;">
	<div class="main_popup_500">
		<div class="head_popup_blue align_c position_r">
			<p class="font_s16 line_19 font_wB title_header_popup_blue text_a_c color_white">Hủy thêm mới</p>
			<img class="close_header_popup_blue cursor_p position_a btn_close2" src="../images/close_w.png" alt="">
		</div>
		<div class="body_popup_blue_500">
			<p class="font_s15 line_h21 color_grey text_a_c pd_69 pd_lr_77">Bạn có chắc chắn muốn hủy thêm mới vật tư thiết bị
				mọi dữ liệu bạn vừa nhập sẽ không được lưu lại?</p>
			<div class=" d_flex flex_center">
				<button class="button_close back_w color_blue font_s15 line_h18 font_w500 mr_68" onclick="openAndHide('popup_cancel_equipment_materials','')">Hủy</button>
				<button class="button_accp back_blue color_white font_s15 line_h18 font_w500">Đồng ý
					<a href="/danh-sach-vat-tu-thiet-bi.html"></a>
				</button>
			</div>
		</div>
	</div>
</div>

<div class="popup popup_cancel_manufacturer" id="popup_cancel_manufacturer" style="display: none;">
	<div class="main_popup_500">
		<div class="head_popup_blue align_c position_r">
			<p class="font_s16 line_19 font_wB title_header_popup_blue text_a_c color_white">Thêm liên hệ mới</p>
			<img class="close_header_popup_blue cursor_p position_a btn_close2" src="../images/close_w.png" alt="">
		</div>
		<div class="body_popup_blue_500">
			<p class="font_s15 line_h21 color_grey text_a_c pd_69 pd_lr_77">Bạn có chắc chắn muốn hủy thêm mới vật tư thiết bị
				mọi dữ liệu bạn vừa nhập sẽ không được lưu lại?</p>
			<div class=" d_flex flex_center">
				<button class="button_close back_w color_blue font_s15 line_h18 font_w500 mr_68" onclick="openAndHide('popup_cancel_manufacturer','')">Hủy</button>
				<button class="button_accp back_blue color_white font_s15 line_h18 font_w500">Đồng ý
					<a href="/hang-san-xuat-detail.html"></a>
				</button>
			</div>
		</div>
	</div>
</div>

<div class="popup popup_cancel_export_note" id="popup_cancel_export_note" style="display: none;">
	<div class="main_popup_500">
		<div class="head_popup_blue align_c position_r">
			<p class="font_s16 line_19 font_wB title_header_popup_blue text_a_c color_white">Hủy thêm mới</p>
			<img class="close_header_popup_blue cursor_p position_a btn_close2" src="../images/close_w.png" alt="">
		</div>
		<div class="body_popup_blue_500">
			<p class="font_s15 line_h21 color_grey text_a_c pd_69 pd_lr_77">Bạn có chắc chắn muốn hủy thêm mới vật tư thiết bị
				mọi dữ liệu bạn vừa nhập sẽ không được lưu lại?</p>
			<div class=" d_flex flex_center">
				<button class="button_close back_w color_blue font_s15 line_h18 font_w500 mr_68" onclick="openAndHide('popup_cancel_export_note','')">Hủy</button>
				<button class="button_accp back_blue color_white font_s15 line_h18 font_w500">Đồng ý
					<a href="/xuat-kho.html"></a>
				</button>
			</div>
		</div>
	</div>
</div>

<!-- <div class="popup cancel_create_equipment_materials" id="cancel_create_equipment_materials" style="display: none;">
  <div class="margin_a" style="width: 500px">
    <div class="head_popup_blue align_c position_r">
      <p class="font_s16 line_19 font_wB title_header_popup_blue text_a_c color_white">Thêm liên hệ mới</p>
      <img class="close_header_popup_blue cursor_p position_a btn_close2" src="../images/close_w.png" alt="">
    </div>
    <div class="body_popup_blue">
      <p class="font_s15 line_h21 color_grey text_a_c pd_69">Bạn có chắc chắn muốn hủy thêm mới vật tư thiết bị<br>
        mọi dữ liệu bạn vừa nhập sẽ không được lưu lại?</p>
      <div class=" d_flex flex_center">
        <button class="button_close back_w color_blue font_s15 line_h18 font_w500 mr_68"
          onclick="openAndHide('cancel_create_equipment_materials','')">Hủy</button>
        <button class="button_accp back_blue color_white font_s15 line_h18 font_w500">Đồng ý
          <a href="/hang-san-xuat-detail.html"></a>
        </button>
      </div>
    </div>
  </div>
</div> -->

<div class="popup check_export_note" id="check_export_note" style="display: none;">
	<div class="main_popup_500">
		<div class="head_popup_blue align_c position_r">
			<p class="font_s16 line_19 font_wB title_header_popup_blue text_a_c color_white">Duyệt phiếu xuất kho</p>
			<img class="close_header_popup_blue cursor_p position_a btn_close2" src="../images/close_w.png" alt="">
		</div>
		<div class="body_popup_blue_500">
			<p class="text_tb font_s15 line_h21 color_grey text_a_c pd_69">Bạn có chắc chắn muốn duyệt phiếu xuất kho <br>
				<strong>PXK-0000</strong>?
			</p>
			<div class=" d_flex flex_center">
				<button class="button_close back_w color_blue font_s15 line_h18 font_w500 mr_68" onclick="openAndHide('check_export_note','')">Hủy</button>
				<button class="button_accp button_accp_duyet back_blue color_white font_s15 line_h18 font_w500">
					Đồng ý
				</button>
			</div>
		</div>
	</div>
</div>
<!-- onclick="openAndHide('check_export_note','check_export_note_success')" -->

<div class="popup complete_export_note" id="complete_export_note" style="display: none;">
	<div class="main_popup_500">
		<div class="head_popup_blue align_c position_r">
			<p class="font_s16 line_19 font_wB title_header_popup_blue text_a_c color_white">Hoàn thành phiếu xuất kho</p>
			<img class="close_header_popup_blue cursor_p position_a btn_close2" src="../images/close_w.png" alt="">
		</div>
		<div class="body_popup_blue_500">
			<p class="font_s15 line_h21 color_grey text_a_c pd_69 text_tb">
			</p>
			<div class=" d_flex flex_center">
				<button class="button_close back_w color_blue font_s15 line_h18 font_w500 mr_68" onclick="openAndHide('complete_export_note','')">Hủy</button>
				<button class="button_accp back_blue color_white font_s15 line_h18 font_w500 button_accp_hoan_thanh">Đồng ý
				</button>
				<!-- onclick="openAndHide('complete_export_note','complete_export_note_success')" -->
			</div>
		</div>
	</div>
</div>

<div class="popup add_new_materials_equipment" id="add_new_materials_equipment" style="display: none;">
	<div class="main_popup_500">
		<div class="head_popup_blue align_c position_r">
			<p class="font_s16 line_19 font_wB title_header_popup_blue text_a_c color_white">Thêm mới nhóm vật tư thiết bị</p>
			<img class="close_header_popup_blue cursor_p position_a btn_close2" src="../images/close_w.png" alt="">
		</div>
		<div class="body_popup_blue_500">
			<form id="add_materials_equipment">
				<div class="d_flex flex_column">
					<div class="input_materials_equipment">
						<p class="color_grey font_s15 line_h18 font_w500">Tên nhóm vật tư thiết bị<span style="color: red;">*</span></p>
						<input class="mt_5 w_100 boder_ra_input_grey name_materials_equipment" type="text" placeholder="Nhập tên nhóm" name="name_materials_equipment">
					</div>
					<p class="color_grey font_s15 line_h18 font_w500 mt_20">Mô tả nhóm vật tư thiết bị</p>
					<textarea class="mt_5 boder_ra_input_grey description_materials_equipment" name="description_materials_equipment" id="" rows="5" placeholder="Nhập nội dung"></textarea>
					<div class=" d_flex flex_center mt_25">
						<button class="button_close back_w color_blue font_s15 line_h18 font_w500 mr_68" type="button" onclick="openAndHide('add_new_materials_equipment','')">Hủy</button>
						<button class="button_accp them_moi_nvttb back_blue color_white font_s15 line_h18 font_w500" type="button">Đồng ý
						</button>
						<!-- onclick="openAndHide('','add_new_materials_equipment_success')" -->
					</div>
				</div>
			</form>
		</div>
	</div>
</div>

<div class="popup add_new_unit" id="add_new_unit" style="display: none;">
	<div class="main_popup_500">
		<div class="head_popup_blue align_c position_r">
			<p class="font_s16 line_19 font_wB title_header_popup_blue text_a_c color_white">Thêm mới đơn vị tính</p>
			<img class="close_header_popup_blue cursor_p position_a btn_close2" src="../images/close_w.png" alt="" onclick="openAndHide('add_new_unit','')">
		</div>
		<div class="body_popup_blue_500">
			<form id="add_unit">
				<div class="d_flex flex_column">
					<div class="input_unit">
						<p class="color_grey font_s15 line_h18 font_w500">Tên đơn vị tính<span style="color: red;">*</span></p>
						<input class="mt_5 w_100 boder_ra_input_grey name_unit" type="text" placeholder="Nhập tên nhóm" name="name_unit">
					</div>
					<p class="color_grey font_s15 line_h18 font_w500 mt_20">Mô tả đơn vị tính</p>
					<textarea class="mt_5 boder_ra_input_grey description_unit" name="description_unit" id="" rows="5" placeholder="Nhập nội dung"></textarea>
					<div class=" d_flex flex_center mt_25">
						<button class="button_close back_w color_blue font_s15 line_h18 font_w500 mr_68" type="button" onclick="openAndHide('add_new_unit','')">Hủy</button>
						<button class="button_accp them_moi_dvt back_blue color_white font_s15 line_h18 font_w500" type="button">Đồng ý
						</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>

<div class="popup refuse_export_note" id="refuse_export_note" style="display: none;">
	<div class="main_popup_500">
		<div class="head_popup_blue align_c position_r">
			<p class="font_s16 line_19 font_wB title_header_popup_blue text_a_c color_white">Từ chối phiếu xuất kho</p>
			<img class="close_header_popup_blue cursor_p position_a btn_close2" src="../images/close_w.png" alt="">
		</div>
		<div class="body_popup_blue_500">
			<form method="POST" id="f_deny_ivt">
				<div class="d_flex flex_column">
					<div class="d_flex flex_column vali_li_do">
						<p class="color_grey font_s15 line_h18 font_w500 mt_20">Lý do từ chối<span style="color: red;">*</span></p>
						<textarea class="mt_5" class="validate_li_do" name="validate_li_do" id="" rows="5" placeholder="Nhập nội dung"></textarea>
					</div>
					<div class=" d_flex flex_center mt_25">
						<button class="button_close back_w color_blue font_s15 line_h18 font_w500 mr_68" onclick="openAndHide('refuse_export_note','')">Hủy</button>
						<button class="button_accp back_blue color_white font_s15 line_h18 font_w500 btn_tuchoi" type="button">Đồng ý</button>
						<!-- onclick="openAndHide('refuse_export_note','refuse_export_note_success')" -->
					</div>
				</div>
			</form>
		</div>
	</div>
</div>

<?php if (in_array(2, $ro_hsx)) { ?>
	<div class="popup add_new_manufacturer" id="add_new_manufacturer" style="display: none;">
		<div class="main_popup_500">
			<div class="head_popup_blue align_c position_r">
				<p class="font_s16 line_19 font_wB title_header_popup_blue text_a_c color_white">Thêm mới hãng sản xuất</p>
				<img class="close_header_popup_blue cursor_p position_a btn_close2" src="../images/close_w.png" alt="">
			</div>
			<div class="body_popup_blue_500">
				<form action="" method="POST" role="form" id="add_manufacturer" name="add_manufacturer">
					<div class="d_flex flex_column">
						<div class="input_name_manufacturer">
							<p class="color_grey font_s15 line_h18 font_w500">Tên hãng sản xuất<span style="color: red;">*</span>
							</p>
							<input class="mt_5 w_100 boder_ra_input_grey name_manufacturer" type="text" placeholder="Nhập tên hãng sản xuất" name="name_manufacturer">
						</div>
						<p class="color_grey font_s15 line_h18 font_w500 mt_20">Mô tả hãng sản xuất</p>
						<textarea class="mt_5 boder_ra_input_grey description_manufacturer" name="description_manufacturer" id="" rows="5" placeholder="Nhập nội dung"></textarea>
						<div class=" d_flex flex_center mt_25">
							<button class="button_close back_w color_blue font_s15 line_h18 font_w500 mr_68" type="button" onclick="openAndHide('add_new_manufacturer','')">Hủy</button>
							<button class="button_accp back_blue color_white font_s15 line_h18 font_w500 them_moi_hvt" type="button">Đồng ý
							</button>
							<!-- onclick="openAndHide('','add_new_manufacturer_success')" -->
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
<?php } ?>

<?php if (in_array(3, $ro_hsx)) { ?>
	<div class="popup edit_manufacturer" id="edit_manufacturer" style="display: none;">
		<div class="main_popup_500">
			<div class="head_popup_blue align_c position_r">
				<p class="font_s16 line_19 font_wB title_header_popup_blue text_a_c color_white">Chỉnh sửa hãng sản xuất</p>
				<img class="close_header_popup_blue cursor_p position_a btn_close2" src="../images/close_w.png" alt="">
			</div>
			<div class="body_popup_blue_500">
				<form id="edit_full_manufacturer" class="edit_full_manufacturer">

				</form>
			</div>
		</div>
	</div>
<?php } ?>

<!-- popup delete  -->
<div class="popup delete_materials_equipment" id="delete_materials_equipment" style="display: none;">
	<div class="main_popup_500">
		<div class="head_popup_blue align_c position_r">
			<p class="font_s16 line_19 font_wB title_header_popup_blue text_a_c color_white">Xóa vật tư thiết bị</p>
			<img class="close_header_popup_blue cursor_p position_a btn_close2" src="../images/close_w.png" alt="">
		</div>
		<div class="body_popup_blue_500">
			<p class="pd_69 text_a_c">Bạn có chắc chắn muốn xóa vật tư thiết bị <br>
				<strong>kìm</strong>?
			</p>
			<div class=" d_flex flex_center">
				<button class="button_close back_w color_blue font_s15 line_h18 font_w500 mr_68" onclick="openAndHide('delete_materials_equipment','')">Hủy</button>
				<button class="button_accp back_blue color_white font_s15 line_h18 font_w500" onclick="openAndHide('delete_materials_equipment','popup_delete_materials_equipment_success')">Đồng ý</button>
			</div>
		</div>
	</div>
</div>

<div class="popup delete_export_note" id="delete_export_note" style="display: none;">
	<div class="main_popup_500">
		<div class="head_popup_blue align_c position_r">
			<p class="font_s16 line_19 font_wB title_header_popup_blue text_a_c color_white">Xóa phiếu xuất kho</p>
			<img class="close_header_popup_blue cursor_p position_a btn_close2" onclick="openAndHide('delete_export_note','')" src="../images/close_w.png" alt="">
		</div>
		<div class="body_popup_blue_500">
			<p class="pd_69 text_a_c color_grey text_tb">
				<!-- Bạn có chắc chắn muốn xóa phiếu xuất kho <br>
				<strong>PXK-0000</strong>? -->
			</p>
			<div class=" d_flex flex_center">
				<button class="button_close back_w color_blue font_s15 line_h18 font_w500 mr_68" onclick="openAndHide('delete_export_note','')">Hủy</button>
				<button class="button_accp back_blue color_white font_s15 line_h18 font_w500 button_accp_delete">Đồng ý</button>
			</div>
			<!-- onclick="openAndHide('delete_export_note','delete_export_note_success')" -->
		</div>
	</div>
</div>

<div class="popup delete_manufacturer" id="delete_manufacturer" style="display: none;">
	<div class="main_popup_500">
		<div class="head_popup_blue align_c position_r">
			<p class="font_s16 line_19 font_wB title_header_popup_blue text_a_c color_white">Xóa hãng sản xuất</p>
			<img class="close_header_popup_blue cursor_p position_a btn_close2" src="../images/close_w.png" alt="">
		</div>
		<div class="body_popup_blue_500">
			<p class="pd_69 text_a_c">Bạn có chắc chắn muốn xóa hãng sản xuất<br>
				<strong>Tân Á</strong>?
			</p>
			<div class=" d_flex flex_center">
				<button class="button_close back_w color_blue font_s15 line_h18 font_w500 mr_68" onclick="openAndHide('delete_manufacturer','')">Hủy</button>
				<button class="button_accp back_blue color_white font_s15 line_h18 font_w500" onclick="openAndHide('delete_manufacturer','popup_delete_manufacturer_success')">Đồng ý</button>
			</div>
		</div>
	</div>
</div>

<div class="popup delete_link_equipment" id="delete_link_equipment" style="display: none;">
	<div class="main_popup_500">
		<div class="head_popup_blue align_c position_r">
			<p class="font_s16 line_19 font_wB title_header_popup_blue text_a_c color_white">Bỏ liên kết với thiết bị</p>
			<img class="close_header_popup_blue cursor_p position_a btn_close2" src="../images/close_w.png" alt="">
		</div>
		<div class="body_popup_blue_500">
			<p class="pd_69 text_a_c color_grey">Bạn có muốn bỏ liên kết với thiết bị <strong>SAMSUNG S20</strong>?<br>
				Tài khoản sẽ đăng xuất khỏi thiết bị.
			</p>
			<div class=" d_flex flex_center">
				<button class="button_close back_w color_blue font_s15 line_h18 font_w500 mr_68" onclick="openAndHide('delete_link_equipment','')">Hủy</button>
				<button class="button_accp back_blue color_white font_s15 line_h18 font_w500" onclick="openAndHide('delete_link_equipment','delete_link_equipment_succses')">Đồng ý</button>
			</div>
		</div>
	</div>
</div>


<!-- popup succses  -->

<div class="popup popup_ex_st popup_create_equipment_materials" id="popup_create_equipment_materials" style="display: none;">
	<div class="box_popup">
		<div class="box_content d_flex flex_column align_c">
			<img class="img_p_succ" src="../images/st_tc.png" alt="">
			<p class="p_add_succ color_grey font_s15 line_h24 text_a_c">
				Thêm mới vật tư thiết bị <strong>Kìm</strong> thành công!</p>
			<button class="btn_close back_blue color_white font_s15 line_h18 font_w500 cursor_p">Đóng
				<a href="/danh-sach-vat-tu-thiet-bi.html"></a>
			</button>
		</div>
	</div>
</div>

<div class="popup popup_ex_st delete_link_equipment_succses" id="delete_link_equipment_succses" style="display: none;">
	<div class="box_popup">
		<div class="box_content d_flex flex_column align_c">
			<img class="img_p_succ" src="../images/st_tc.png" alt="">
			<p class="p_add_succ color_grey font_s15 line_h24 text_a_c">
				Bỏ liên kết thành công!
			</p>
			<button class="btn_close back_blue color_white font_s15 line_h18 font_w500 cursor_p">Đóng
				<a href="/thong-tin-bao-mat.html"></a>
			</button>
		</div>
	</div>
</div>

<div class="popup popup_ex_st create_new_equipment_materials" id="create_new_equipment_materials" style="display: none;">
	<div class="box_popup">
		<div class="box_content d_flex flex_column align_c">
			<img class="img_p_succ" src="../images/st_tc.png" alt="">
			<p class="p_add_succ color_grey font_s15 line_h24 text_a_c">
				Thêm mới vật tư thiết bị <strong>Kìm</strong> thành công!</p>
			<button class="btn_close back_blue color_white font_s15 line_h18 font_w500 cursor_p">Đóng
				<a href="/hang-san-xuat-detail.html"></a>
			</button>
		</div>
	</div>
</div>

<div class="popup popup_ex_st add_new_manufacturer_success" id="add_new_manufacturer_success" style="display: none;">
	<div class="box_popup">
		<div class="box_content d_flex flex_column align_c">
			<img class="img_p_succ" src="../images/st_tc.png" alt="">
			<p class="p_add_succ color_grey font_s15 line_h24 text_a_c">
				Thêm mới nhóm vật tư thiết bị <strong> Nhóm vật tư 1</strong> thành công!</p>
			<button class="btn_close back_blue color_white font_s15 line_h18 font_w500 cursor_p" onclick="openAndHide('add_new_manufacturer_success','')">Đóng
			</button>
		</div>
	</div>
</div>

<div class="popup popup_ex_st add_new_export_note_success" id="add_new_export_note_success" style="display: none;">
	<div class="box_popup">
		<div class="box_content d_flex flex_column align_c">
			<img class="img_p_succ" src="../images/st_tc.png" alt="">
			<p class="p_add_succ color_grey font_s15 line_h24 text_a_c">
				<!-- Thêm mới phiếu xuất kho <strong>PXK-0000 </strong><br>
				theo đơn hàng thành công! -->
			</p>
			<button class="btn_close back_blue color_white font_s15 line_h18 font_w500 cursor_p">Đóng
				<a href="/xuat-kho.html"></a>
			</button>
		</div>
	</div>
</div>

<div class="popup popup_ex_st edit_manufacturer_success" id="edit_manufacturer_success" style="display: none;">
	<div class="box_popup">
		<div class="box_content d_flex flex_column align_c">
			<img class="img_p_succ" src="../images/st_tc.png" alt="">
			<p class="p_add_succ color_grey font_s15 line_h24 text_a_c">
				Chỉnh sửa hãng sản xuất <strong>Tân Á</strong> thành công!</p>
			<button class="btn_close back_blue color_white font_s15 line_h18 font_w500 cursor_p" onclick="openAndHide('edit_manufacturer_success','')">Đóng
			</button>
		</div>
	</div>
</div>

<div class="popup popup_ex_st edit_equipment_materials_success" id="edit_equipment_materials_success" style="display: none;">
	<div class="box_popup">
		<div class="box_content d_flex flex_column align_c">
			<img class="img_p_succ" src="../images/st_tc.png" alt="">
			<p class="p_add_succ color_grey font_s15 line_h24 text_a_c">
				Chỉnh sửa vật tư thiết bị <strong>Kìm</strong> thành công!</p>
			<button class="btn_close back_blue color_white font_s15 line_h18 font_w500 cursor_p">Đóng
				<a href="/danh-sach-vat-tu-thiet-bi.html"></a>
			</button>
		</div>
	</div>
</div>

<div class="popup popup_ex_st add_new_materials_equipment_success" id="add_new_materials_equipment_success" style="display: none;">
	<div class="box_popup">
		<div class="box_content d_flex flex_column align_c">
			<img class="img_p_succ" src="../images/st_tc.png" alt="">
			<p class="p_add_succ color_grey font_s15 line_h24 text_a_c add_new_succ">

			</p>
			<button class="btn_close back_blue color_white font_s15 line_h18 font_w500 cursor_p">Đóng
			</button>
		</div>
	</div>
</div>

<div class="popup popup_ex_st popup_delete_materials_equipment_success" id="popup_delete_materials_equipment_success" style="display: none;">
	<div class="box_popup">
		<div class="box_content d_flex flex_column align_c">
			<img class="img_p_succ" src="../images/st_tc.png" alt="">
			<p class="p_add_succ color_grey font_s15 line_h24 text_a_c">
				Xóa vật tư thiết bị <strong>Kìm</strong> thành công!</p>
			<button class="btn_close back_blue color_white font_s15 line_h18 font_w500 cursor_p">Đóng
				<a href="/danh-sach-vat-tu-thiet-bi.html"></a>
			</button>
		</div>
	</div>
</div>
<div class="popup popup_ex_st popup_delete_manufacturer_success" id="popup_delete_manufacturer_success" style="display: none;">
	<div class="box_popup">
		<div class="box_content d_flex flex_column align_c">
			<img class="img_p_succ" src="../images/st_tc.png" alt="">
			<p class="p_add_succ color_grey font_s15 line_h24 text_a_c">
				Xóa hãng sản xuất <strong>Tân Á</strong> thành công!</p>
			<button class="btn_close back_blue color_white font_s15 line_h18 font_w500 cursor_p" onclick="openAndHide('popup_delete_manufacturer_success','')">Đóng
			</button>
		</div>
	</div>
</div>

<div class="popup popup_ex_st update_decentralization_success" id="update_decentralization_success" style="display: none;">
	<div class="box_popup">
		<div class="box_content d_flex flex_column align_c">
			<img class="img_p_succ" src="../images/st_tc.png" alt="">
			<p class="p_add_succ color_grey font_s15 line_h24 text_a_c">
				Cập nhật phân quyền thành công thành công!</p>
			<button class="btn_close back_blue color_white font_s15 line_h18 font_w500 cursor_p">Đóng
				<a href="/phan-quyen.html"></a>
			</button>
		</div>
	</div>
</div>

<div class="popup popup_ex_st check_export_note_success" id="check_export_note_success" style="display: none;">
	<div class="box_popup">
		<div class="box_content d_flex flex_column align_c">
			<img class="img_p_succ" src="../images/st_tc.png" alt="">
			<p class="p_add_succ color_grey font_s15 line_h24 text_a_c">
				<!-- Duyệt phiếu xuất kho <strong>PXK-0000</strong> thành công!</p> -->
			</p>
			<button class="btn_close back_blue color_white font_s15 line_h18 font_w500 cursor_p on_reload">Đóng
			</button>
		</div>
	</div>
</div>
<div class="popup popup_ex_st complete_export_note_success" id="complete_export_note_success" style="display: none;">
	<div class="box_popup">
		<div class="box_content d_flex flex_column align_c">
			<img class="img_p_succ" src="../images/st_tc.png" alt="">
			<p class="p_add_succ color_grey font_s15 line_h24 text_a_c">
				Duyệt phiếu xuất kho <strong>PXK-0000</strong> thành công!</p>
			<button class="btn_close back_blue color_white font_s15 line_h18 font_w500 cursor_p" onclick="complete('complete_export_note_success')">Đóng
			</button>
		</div>
	</div>
</div>
<div class="popup popup_ex_st refuse_export_note_success" id="refuse_export_note_success" style="display: none;">
	<div class="box_popup">
		<div class="box_content d_flex flex_column align_c">
			<img class="img_p_succ" src="../images/st_tc.png" alt="">
			<p class="p_add_succ color_grey font_s15 line_h24 text_a_c">
				Từ chối phiếu xuất kho <strong>PXK-0000</strong> thành công!</p>
			<button class="btn_close back_blue color_white font_s15 line_h18 font_w500 cursor_p" onclick="refuse('refuse_export_note_success')">Đóng
			</button>
		</div>
	</div>
</div>
<div class="popup popup_ex_st delete_export_note_success" id="delete_export_note_success" style="display: none;">
	<div class="box_popup">
		<div class="box_content d_flex flex_column align_c">
			<img class="img_p_succ" src="../images/st_tc.png" alt="">
			<p class="p_add_succ color_grey font_s15 line_h24 text_a_c">
				<!-- Xóa phiếu xuất kho <strong>PXK-0000</strong> thành công!</p> -->
			</p>
			<button class="btn_close back_blue color_white font_s15 line_h18 font_w500 cursor_p">Đóng
				<a href="/xuat-kho.html"></a>
			</button>
		</div>
	</div>
</div>

<!-- popup false -->

<div class="popup popup_ex_st" id="" style="display: none;">
	<div class="box_popup">
		<div class="box_content d_flex flex_column align_c">
			<img class="img_p_lose" src="../images/st_tb.png" alt="">
			<p class="p_add_lose color_grey font_s15 line_h24 text_a_c">Thêm mới phiếu xuất kho <strong>PXK-0000</strong><br>
				theo yêu cầu vật tư thất bại!</p>
			<button class="btn_close back_blue color_white font_s15 line_h18 font_w500 cursor_p">Đóng</button>
		</div>
	</div>
</div>

<!-- logout  -->