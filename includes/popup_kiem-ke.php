<!-- xóa phiếu kiểm kê -->
<div class="popup_show popup_n popup_del_kk " id="popup_del_kk" style="display: none;">
    <div class="box_popup">
        <div class="box_content">
            <div class="header_box back_blue">
                <div class="tit_head position_r">
                    <p class="color_white font_s16 line_h19 font_w700 text_a_c">Xóa phiếu kiểm kê
                    </p>
                    <img class="close_popup position_a cursor_p" onclick="openAndHide('popup_del_kk','')" src="../images/close_w.png" alt="" style="top: 0; right: 0;">
                </div>
            </div>
            <div class="content_popup">
                <p class="del_w text_a_c color_grey font_s15 line_h21 font_w400">Bạn có chắc chắn muốn xóa phiếu kiểm kê <br><strong>PKK-0000</strong>?</p>
                <div class="btn_ct_pp d_flex flex_center">
                    <button class="btn_cancel back_w color_blue font_s15 line_h18 font_w500 cursor_p" onclick="openAndHide('popup_del_kk','')">Hủy</button>
                    <button class="btn_save back_blue color_white font_s15 line_h18 font_w500 cursor_p btn_xoa_phieu_kk" type="button" data="">Đồng ý</button>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- popup chọn ngày -->    
<div class="popup_show popup_n popup_select_date" id="popup_select_date" style="display: none;">
    <div class="box_popup">
        <div class="box_content">
            <div class="header_box back_blue">
                <div class="tit_head position_r">
                    <p class="color_white font_s16 line_h19 font_w700 text_a_c">Chọn thời gian
                    </p>
                    <p class="color_white font_s16 line_h19 font_w700 text_a_c" style="display: none;">Chỉnh sửa nhóm vật tư thiết bị
                    </p>
                    <img class="close_popup position_a cursor_p" src="../images/close_w.png" alt="" style="top: 0; right: 0;">
                </div>
            </div>
            <div class="content_popup">
                <div class="d_flex flex_column mb_15">
                    <p class="color_grey font_s15 line_h18 font_w500">Từ ngày</p>
                    <div class="position_r">
                        <input type="text" placeholder="Chọn ngày" onfocusin="(this.type='date')" onfocusout="(this.type='text')">    
                        <img class="img_date" src="../images/select_date.png" alt="">
                    </div>
                </div>
                <div class="d_flex flex_column position_r">
                    <p class="color_grey font_s15 line_h18 font_w500">Đến ngày</p>                    
                    <div class="position_r">
                        <input type="text" placeholder="Chọn ngày" onfocusin="(this.type='date')" onfocusout="(this.type='text')">
                        <img class="img_date" src="../images/select_date.png" alt="">
                    </div>
                </div>
                <div class="btn_ct_pp d_flex flex_center">
                    <button class="btn_cancel back_w color_blue font_s15 line_h18 font_w500 cursor_p">Hủy</button>
                    <button class="btn_save back_blue color_white font_s15 line_h18 font_w500 cursor_p">Đồng ý</button>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- duyệt phiếu kiểm kê -->
<div class="popup_show popup_n popup_acp_kk" id="popup_acp_kk" style="display: none;">
    <div class="box_popup">
        <div class="box_content">
            <div class="header_box back_blue">
                <div class="tit_head position_r">
                    <p class="color_white font_s16 line_h19 font_w700 text_a_c">Duyệt phiếu kiểm kê
                    </p>
                    <img class="close_popup position_a" onclick="openAndHide('popup_acp_kk','')" src="../images/close_w.png" alt="" style="top: 0; right: 0;">
                </div>
            </div>
            <div class="content_popup">
                <p class="del_w text_a_c color_grey font_s15 line_h21 font_w400">

                </p>
                <div class="btn_ct_pp d_flex flex_center">
                    <button class="btn_cancel back_w color_blue font_s15 line_h18 font_w500" onclick="openAndHide('popup_acp_kk','')">Hủy</button>
                    <button class="btn_save back_blue color_white font_s15 line_h18 font_w500 btn_duyet_kk" type="button" data="">Đồng ý</button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- cập nhật -->
<div class="popup_show popup_n popup_update_kk" id="popup_update_kk" style="display: none;">
    <div class="box_popup">
        <div class="box_content">
            <div class="header_box back_blue">
                <div class="tit_head position_r">
                    <p class="color_white font_s16 line_h19 font_w700 text_a_c">Cập nhật phiếu kiểm kê
                    </p>
                    <img class="close_popup position_a" onclick="openAndHide('popup_update_kk','')" src="../images/close_w.png" alt="" style="top: 0; right: 0;">
                </div>
            </div>
            <div class="content_popup">
                <p class="del_w text_a_c color_grey font_s15 line_h21 font_w400"></p>
                <div class="btn_ct_pp d_flex flex_center">
                    <button class="btn_cancel back_w color_blue font_s15 line_h18 font_w500" onclick="openAndHide('popup_update_kk','')">Hủy</button>
                    <button class="btn_save back_blue color_white font_s15 line_h18 font_w500 btn_capnhat_kk" type="button" data="">Đồng ý</button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- từ chối phiếu -->
<div class="popup_show popup_n popup_deny_kk" id="popup_deny_kk" style="display: none;">
    <div class="box_popup">
        <div class="box_content">

            <div class="header_box back_blue">
                <div class="tit_head position_r">
                    <p class="color_white font_s16 line_h19 font_w700 text_a_c">Từ chối phiếu kiểm kê
                    </p>
                    <img class="close_popup position_a cursor_p" onclick="openAndHide('popup_deny_kk','')" src="../images/close_w.png" alt="" style="top: 0; right: 0;">
                </div>
            </div>
            <div class="content_popup">
                <form method="POST" id="f_deny_ivt">
                    <div class="d_flex flex_column vali_li_do">
                        <p class="color_grey font_s15 line_h18 font_w500">Lí do từ chối<span style="color: red;">*</span></p>
                        <textarea class="validate_li_do" name="validate_li_do" id="" rows="5" placeholder="Nhập nội dung"></textarea>
                    </div>
                    <div class="btn_ct_pp d_flex flex_center">
                        <button class="btn_cancel back_w color_blue font_s15 line_h18 font_w500 cursor_p" type="button" onclick="openAndHide('popup_deny_kk','')">Hủy</button>
                        <button class="btn_save back_blue color_white font_s15 line_h18 font_w500 cursor_p btn_tuchoi" type="button" data="">Đồng ý</button>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>
<!-- hoàn tahnhf phiếu kiểm kê -->
<div class="popup_show popup_n popup_comp_kk" id="popup_comp_kk" style="display: none;">
    <div class="box_popup">
        <div class="box_content">
            <div class="header_box back_blue">
                <div class="tit_head position_r">
                    <p class="color_white font_s16 line_h19 font_w700 text_a_c">Hoàn thành kiểm kê
                    </p>
                    <img class="close_popup position_a" onclick="openAndHide('popup_comp_kk','')"    src="../images/close_w.png" alt="" style="top: 0; right: 0;">
                </div>
            </div>
            <div class="content_popup">
                <p class="del_w text_a_c color_grey font_s15 line_h21 font_w400 content_kt_duyet">

                </p>
                <div class="btn_ct_pp d_flex flex_center">
                    <button class="btn_cancel back_w color_blue font_s15 line_h18 font_w500" onclick="openAndHide('popup_comp_kk','')">Hủy</button>
                    <button class="btn_save back_blue color_white font_s15 line_h18 font_w500 xoa_btx" type="button" data="" onclick="update_trangThai('9')">Đồng ý</button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- thông báo thành công -->
<div class="popup_show popup_ex_st popup_add_kk_succ" id="popup_add_kk_succ" style="display: none;">
    <div class="box_popup">
        <div class="box_content d_flex flex_column align_c">
            <img class="img_p_succ" src="../images/st_tc.png" alt="" style="display: block;">
            <p class="p_add_succ color_grey font_s15 line_h24 text_a_c" style="display: block;">Duyệt phiếu kiểm kê
                <strong>PKK-0000</strong> thành công
            </p>
            <button class="btn_close back_blue color_white font_s15 line_h18 font_w500 cursor_p">
                <a href="/kiem-ke-kho-detail3.html"></a>
                Đóng
            </button>
        </div>
    </div>
</div>
<div class="popup_show popup_ex_st popup_del_kk_succ" id="popup_del_kk_succ" style="display: none;">
    <div class="box_popup">
        <div class="box_content d_flex flex_column align_c">
            <img class="img_p_succ" src="../images/st_tb.png" alt="" style="display: block;">
            <p class="p_add_succ color_grey font_s15 line_h24 text_a_c" style="display: block;">Xóa phiếu kiểm kê
                <strong>PKK-0000</strong>thất bại
            </p>
            <button class="btn_close back_blue color_white font_s15 line_h18 font_w500 cursor_p">
                <a href="./kiem-ke-kho.html"></a>
                Đóng
            </button>
        </div>
    </div>
</div>

<div class="popup_show  popup_add_notif_succ" id="popup_add_notif_succ" style="display: none;">
    <div class="box_popup">
        <div class="box_content d_flex flex_column align_c">
            <img class="img_p_succ" src="../images/st_tc.png" alt="" style="display: block;">
            <p class="p_add_succ color_grey font_s15 line_h24 text_a_c" style="display: block;"></p>
            <button class="btn_close back_blue color_white font_s15 line_h18 font_w500 cursor_p">
                <!-- <a href="/kiem-ke-kho.html"></a> -->
                Đóng
            </button>
        </div>
    </div>
</div>

<div class="popup_show  popup_add_notif_succ_kk" id="popup_add_notif_succ_kk" style="display: none;">
    <div class="box_popup">
        <div class="box_content d_flex flex_column align_c">
            <img class="img_p_succ" src="../images/st_tc.png" alt="" style="display: block;">
            <p class="p_add_succ color_grey font_s15 line_h24 text_a_c" style="display: block;">Xóa vĩnh vĩnh vật tư thiết bị <strong>VT-0000</strong> thành công!</p>
            <button class="btn_close back_blue color_white font_s15 line_h18 font_w500 cursor_p">
                <a href="/kiem-ke-kho.html"></a>
                Đóng
            </button>
        </div>
    </div>
</div>