
<!-- Thông báo thành công, thất bại -->
<div class="popup_show popup_ex_st popup_ex_st_succ" id="popup_ex_st_succ" style="display: none;">
    <div class="box_popup">
        <div class="box_content d_flex flex_column align_c">
            <img class="img_p_succ" src="../images/st_tc.png" alt="">
            <p class="p_add_succ color_grey font_s15 line_h24 text_a_c" style="display: block;">Thêm mới phiếu xuất kho <strong>PXK-0000</strong><br>
                theo yêu cầu vật tư thành công!</p>
            <button class="btn_close back_blue color_white font_s15 line_h18 font_w500 cursor_p">Đóng
                <!-- <a href=""></a> -->
                <!-- /nghiep-vu-kho-cho-xu-ly.html -->
            </button>
        </div>
    </div>
</div>

<div class="popup_show popup_ex_st popup_ex_lose" id="popup_ex_lose" style="display: none;">
    <div class="box_popup">
        <div class="box_content d_flex flex_column align_c">
            <img class="img_p_lose" src="../images/st_tb.png" alt="">
            <p class="p_add_lose color_grey font_s15 line_h24 text_a_c">Thêm mới phiếu xuất kho <strong>PXK-0000</strong><br>
                theo yêu cầu vật tư thất bại!</p>
            <button class="btn_close back_blue color_white font_s15 line_h18 font_w500 cursor_p">Đóng</button>
        </div>
    </div>
</div>

<!-- Thông báo hủy thêm mới -->
<div class="popup_show popup_n popup_func_del" id="popup_func_del" style="display: none;">
    <div class="box_popup">
        <div class="box_content">
            <div class="header_box back_blue">
                <div class="tit_head position_r">
                    <p class="color_white font_s16 line_h19 font_w700 text_a_c">Hủy thêm mới
                    </p>
                    <img class="close_popup position_a cursor_p" src="../images/close_w.png" alt="" style="top: 0; right: 0;">
                </div>
            </div>
            <div class="content_popup">
                <p class="color_grey font_s15 line_h21 font_w400 text_a_c">Bạn có chắc chắn muốn hủy thêm mới vật tư thiết bị<br>mọi dữ liệu bạn vừa nhập sẽ không được lưu lại?</p>
                <div class="btn_ct_pp d_flex flex_center">
                    <button class="btn_cancel back_w color_blue font_s15 line_h18 font_w500 cursor_p">Hủy</button>
                    <button class="btn_save back_blue color_white font_s15 line_h18 font_w500 cursor_p">
                    <a href="/nghiep-vu-kho-cho-xu-ly-detail-tf.html"></a>    
                    Đồng ý</button>
                </div>
            </div>
        </div>
    </div>
</div>



