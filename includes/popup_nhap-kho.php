<div class="popup_show popup_show2 popup_add_phieuNhap" id="popup_add_phieuNhap" style="display: none;">
    <div class="box_popup">
        <div class="box_content d_flex flex_column align_c">
            <img class="img_p_succ" src="../images/st_tc.png" alt="" style="display: block;">
            <p class="p_add_succ color_grey font_s15 line_h24 text_a_c" style="display: block;">Thêm mới phiếu nhập kho thành công</p>
            <button class="btn_close back_blue color_white font_s15 line_h18 font_w500 cursor_p">
                <a href=""></a>
                Đóng
            </button>
        </div>
    </div>
</div>
<div class="popup_show popup_n " id="" style="display: none;">
    <div class="box_popup">
        <div class="box_content d_flex flex_column align_c">
            <img class="img_p_succ" src="../images/st_tc.png" alt="" style="display: block;">
            <p class="p_add_succ color_grey font_s15 line_h24 text_a_c" style="display: block;">Thêm mới phiếu nhập kho
                 <strong>PNK</strong> thất bại!</p>
            <button class="btn_close back_blue color_white font_s15 line_h18 font_w500 cursor_p">
                <a href="/nhom-vat-tu-thiet-bi.html"></a>
                Đóng
            </button>
        </div>
    </div>
</div>
<!-- từ chối -->
<div class="popup_show popup_n popup_ref_w" id="popup_ref_w" style="display: none;">
    <div class="box_popup">
        <div class="box_content">
            <div class="header_box back_blue">
                <div class="tit_head position_r">
                    <p class="color_white font_s16 line_h19 font_w700 text_a_c">Từ chối phiếu điều chuyển kho
                    </p>
                    <img class="close_popup position_a cursor_p" src="../images/close_w.png" alt="" style="top: 0; right: 0;">
                </div>
            </div>
            <div class="content_popup">
                <form action="" method="post" id="f_reason_ref">
                    <div class="name_reason_ref d_flex flex_column">
                        <p class="color_grey font_s15 line_h18 font_w500">Lí do từ chối<span style="color: red;">*</span></p>
                        <textarea name="name_reason_ref" id="" rows="5" placeholder="Nhập nội dung"></textarea>
                    </div>
                    <div class="btn_ct_pp d_flex flex_center">
                        <button type="button" class="btn_cancel back_w color_blue font_s15 line_h18 font_w500 cursor_p">Hủy</button>
                        <button type="button" class="btn_save back_blue color_white font_s15 line_h18 font_w500 cursor_p">Đồng ý</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- duyệt phiếu nhập -->
<div class="popup_show popup_n popup_succ_w" id="popup_succ_w" style="display: none;">
    <div class="box_popup">
        <div class="box_content">
            <div class="header_box back_blue">
                <div class="tit_head position_r">
                    <p class="color_white font_s16 line_h19 font_w700 text_a_c">Duyệt phiếu nhập kho
                    </p>
                    <img class="close_popup position_a cursor_p" src="../images/close_w.png" alt="" style="top: 0; right: 0;">
                </div>
            </div>
            <div class="content_popup">
                <p class="del_w text_a_c color_grey font_s15 line_h21 font_w400">Bạn có chắc chắn muốn duyệt phiếu nhập kho<br><strong>PNK-0000</strong>?</p>
                <div class="btn_ct_pp d_flex flex_center">
                    <button class="btn_cancel back_w color_blue font_s15 line_h18 font_w500">Hủy</button>
                    <button class="btn_save back_blue color_white font_s15 line_h18 font_w500">Đồng ý</button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- hoàn thành  -->
<div class="popup_show popup_n popup_ht_import_wh" id="popup_ht_import_wh" style="display: none;">
    <div class="box_popup">
        <div class="box_content">
            <div class="header_box back_blue">
                <div class="tit_head position_r">
                    <p class="color_white font_s16 line_h19 font_w700 text_a_c">Hoàn thành phiếu nhập kho
                    </p>
                    <img class="close_popup position_a cursor_p" src="../images/close_w.png" alt="" style="top: 0; right: 0;">
                </div>
            </div>
            <div class="content_popup">
                <p class="del_w text_a_c color_grey font_s15 line_h21 font_w400">Bạn có chắc chắn muốn hoàn thành phiếu nhập kho<br><strong>PNK-0000</strong>?</p>
                <div class="btn_ct_pp d_flex flex_center">
                    <button class="btn_cancel back_w color_blue font_s15 line_h18 font_w500">Hủy</button>
                    <button class="btn_save back_blue color_white font_s15 line_h18 font_w500">Đồng ý</button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- xóa phiếu nhập -->
<div class="popup_show popup_n popup_del_w" id="popup_del_w" style="display: none;">
    <div class="box_popup">
        <div class="box_content">
            <div class="header_box back_blue">
                <div class="tit_head position_r">
                    <p class="color_white font_s16 line_h19 font_w700 text_a_c">Xóa phiếu nhập kho
                    </p>
                    <img class="close_popup position_a cursor_p" src="../images/close_w.png" alt="" style="top: 0; right: 0;">
                </div>
            </div>
            <div class="content_popup">
                <p class="del_w text_a_c color_grey font_s15 line_h21 font_w400">Bạn có chắc chắn muốn xóa phiếu nhập kho<br><strong>PNK-0000</strong>?</p>
                <div class="btn_ct_pp d_flex flex_center">
                    <button class="btn_cancel back_w color_blue font_s15 line_h18 font_w500 cursor_p">Hủy</button>
                    <button class="btn_save back_blue color_white font_s15 line_h18 font_w500 cursor_p">Đồng ý</button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- popup chon ngay -->
<div class="popup_show popup_n popup_select_date" id="popup_select_date" style="display: none;">
    <div class="box_popup">
        <div class="box_content">
            <div class="header_box back_blue">
                <div class="tit_head position_r">
                    <p class="color_white font_s16 line_h19 font_w700 text_a_c">Chọn thời gian
                    </p>
                    <p class="color_white font_s16 line_h19 font_w700 text_a_c" style="display: none;">Chỉnh sửa nhóm vật tư thiết bị
                    </p>
                    <img class="close_popup position_a cursor_p" src="../images/close_w.png" alt="" style="top: 0; right: 0;">
                </div>
            </div>
            <div class="content_popup">
                <div class="d_flex flex_column mb_15">
                    <p class="color_grey font_s15 line_h18 font_w500">Từ ngày</p>
                    <div class="position_r">
                        <input type="text" placeholder="Chọn ngày" onfocusin="(this.type='date')" onfocusout="(this.type='text')">    
                        <img class="img_date" src="../images/select_date.png" alt="">
                    </div>
                </div>
                <div class="d_flex flex_column position_r">
                    <p class="color_grey font_s15 line_h18 font_w500">Đến ngày</p>                    
                    <div class="position_r">
                        <input type="text" placeholder="Chọn ngày" onfocusin="(this.type='date')" onfocusout="(this.type='text')">
                        <img class="img_date" src="../images/select_date.png" alt="">
                    </div>
                </div>
                <div class="btn_ct_pp d_flex flex_center">
                    <button class="btn_cancel back_w color_blue font_s15 line_h18 font_w500 cursor_p">Hủy</button>
                    <button class="btn_save back_blue color_white font_s15 line_h18 font_w500 cursor_p">Đồng ý</button>
                </div>
            </div>
        </div>
    </div>
</div>



<div class="popup_show  popup_add_notif_succ" id="popup_add_notif_succ" style="display: none;">
    <div class="box_popup">
        <div class="box_content d_flex flex_column align_c">
            <img class="img_p_succ" src="../images/st_tc.png" alt="" style="display: block;">
            <p class="p_add_succ color_grey font_s15 line_h24 text_a_c" style="display: block;">Xóa vĩnh vĩnh vật tư thiết bị <strong>VT-0000</strong> thành công!</p>
            <button class="btn_close back_blue color_white font_s15 line_h18 font_w500 cursor_p">
                <a href="#"></a>
                Đóng
            </button>
        </div>
    </div>
</div>