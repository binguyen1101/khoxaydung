
<!-- Thông báo thêm thành công, thất bại -->
<div class="popup_show popup_add_notif_succ" id="popup_add_notif_succ" style="display: none;">
    <div class="box_popup">
        <div class="box_content d_flex flex_column align_c">
            <img class="img_p_succ" src="../images/st_tc.png" alt="" style="display: block;">
            <p class="p_add_succ color_grey font_s15 line_h24 text_a_c" style="display: block;">Thêm mới nhóm vật tư
                thiết bị <strong>Nhóm vật tư 1</strong> thất bại!</p>
            <button class="btn_close back_blue color_white font_s15 line_h18 font_w500 cursor_p">
                <!-- <a href="/nhom-vat-tu-thiet-bi.html"></a> -->
                Đóng
            </button>
        </div>
    </div>
</div>

<div class="popup_show popup_add_notif_lose" id="popup_add_notif_lose" style="display: none;">
    <div class="box_popup">
        <div class="box_content d_flex flex_column align_c">
            <img class="img_p_lose" src="../images/st_tb.png" alt="" style="display: block;">
            <p class="p_add_succ color_grey font_s15 line_h24 text_a_c" style="display: block;">Thêm mới nhóm vật tư
                thiết bị <strong>Nhóm vật tư 1</strong> thất bại!</p>
            <button class="btn_close back_blue color_white font_s15 line_h18 font_w500 cursor_p">Đóng</button>
        </div>
    </div>
</div>

<!-- Thêm, sửa mới nhóm vật tư thiết bị -->
<?php if(in_array(2,$ro_nhom_vt)){?>
    <div class="popup_show popup_n popup_func_dv_add" id="popup_func_dv_add" style="display: none;">
        <div class="box_popup">
            <div class="box_content">
                <div class="header_box back_blue">
                    <div class="tit_head position_r">
                        <p class="color_white font_s16 line_h19 font_w700 text_a_c">Thêm mới nhóm vật tư thiết bị
                        </p>
                        <img class="close_popup position_a cursor_p" src="../images/close_w.png" alt="" style="top: 0; right: 0;">
                    </div>
                </div>
                <div class="content_popup">
                    <form action="" method="post" class="f_func_add">
                        <div class="name_gr_dv_add d_flex flex_column mb_15">
                            <p class="color_grey font_s15 line_h18 font_w500">Tên nhóm vật tư thiết bị<span style="color: red;">*</span></p>
                            <input class="color_grey font_s14 line_h17 font_w400" type="text" placeholder="Nhập tên nhóm" name="name_gr_dv_add">
                        </div>
                        <div class="d_flex flex_column">
                            <p class="color_grey font_s15 line_h18 font_w500">Mô tả nhóm vật tư thiết bị</p>
                            <textarea name="dep_gr_dv_add" id="" rows="5" placeholder="Nhập nội dung"></textarea>
                        </div>
                        <div class="btn_ct_pp d_flex flex_center">
                            <button type="button" class="btn_cancel back_w color_blue font_s15 line_h18 font_w500 cursor_p">Hủy</button>
                            <button type="button" class="btn_save back_blue color_white font_s15 line_h18 font_w500 cursor_p">Đồng ý</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
<?php }?>

<?php if(in_array(3,$ro_nhom_vt)){?>
    <div class="popup_show popup_n popup_func_dv_edit" id="popup_func_dv_edit" style="display: none;">
        <div class="box_popup">
            <div class="box_content">
                <div class="header_box back_blue">
                    <div class="tit_head position_r">
                        <p class="color_white font_s16 line_h19 font_w700 text_a_c">Chỉnh sửa nhóm vật tư thiết bị
                        </p>
                        <img class="close_popup position_a cursor_p" src="../images/close_w.png" alt="" style="top: 0; right: 0;">
                    </div>
                </div>
                <div class="content_popup">
                    <form action="" method="post" class="f_func_edit">
                        <div class="name_gr_dv_edit d_flex flex_column mb_15">
                            <p class="color_grey font_s15 line_h18 font_w500">Tên nhóm vật tư thiết bị<span style="color: red;">*</span></p>
                            <input class="color_grey font_s14 line_h17 font_w400" type="text" placeholder="Nhập tên nhóm" name="name_gr_dv_edit">
                        </div>
                        <div class="d_flex flex_column">
                            <p class="color_grey font_s15 line_h18 font_w500">Mô tả nhóm vật tư thiết bị</p>
                            <textarea name="dep_gr_dv_edit" id="" rows="5" placeholder="Nhập nội dung"></textarea>
                        </div>
                        <div class="btn_ct_pp d_flex flex_center">
                            <button type="button" class="btn_cancel back_w color_blue font_s15 line_h18 font_w500 cursor_p">Hủy</button>
                            <button type="button" class="btn_save back_blue color_white font_s15 line_h18 font_w500 cursor_p">Đồng ý</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
<?php }?>
<!-- Xóa nhóm vật tư thiết bị -->
<div class="popup_show popup_n popup_func_del" id="popup_func_del" style="display: none;">
    <div class="box_popup">
        <div class="box_content">
            <div class="header_box back_blue">
                <div class="tit_head position_r">
                    <p class="color_white font_s16 line_h19 font_w700 text_a_c">Xóa nhóm vật tư thiết bị
                    </p>
                    <p class="color_white font_s16 line_h19 font_w700 text_a_c" style="display: none;">Chỉnh sửa nhóm vật tư thiết bị
                    </p>
                    <img class="close_popup position_a cursor_p" src="../images/close_w.png" alt="" style="top: 0; right: 0;">
                </div>
            </div>
            <div class="content_popup">
                <p class="color_grey font_s15 line_h21 font_w400 text_a_c">Bạn có chắc chắn muốn xóa nhóm vật tư thiết bị<br><strong>Nhóm vật tư 1</strong>?</p>
                <div class="btn_ct_pp d_flex flex_center">
                    <button class="btn_cancel back_w color_blue font_s15 line_h18 font_w500 cursor_p">Hủy</button>
                    <button class="btn_save back_blue color_white font_s15 line_h18 font_w500 cursor_p">Đồng ý</button>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="popup delete_materials_equipment" id="delete_materials_equipment" style="display: none;">
	<div class="main_popup_500">
		<div class="head_popup_blue align_c position_r">
			<p class="font_s16 line_19 font_wB title_header_popup_blue text_a_c color_white">Xóa vật tư thiết bị</p>
			<img class="close_header_popup_blue cursor_p position_a btn_close2" src="../images/close_w.png" alt="">
		</div>
		<div class="body_popup_blue_500">
			<p class="pd_69 text_a_c">Bạn có chắc chắn muốn xóa vật tư thiết bị <br>
				<strong>kìm</strong>?
			</p>
			<div class=" d_flex flex_center">
				<button class="button_close back_w color_blue font_s15 line_h18 font_w500 mr_68" onclick="openAndHide('delete_materials_equipment','')">Hủy</button>
				<button class="button_accp back_blue color_white font_s15 line_h18 font_w500" onclick="openAndHide('delete_materials_equipment','popup_delete_materials_equipment_success')">Đồng ý</button>
			</div>
		</div>
	</div>
</div>

<div class="popup popup_ex_st popup_delete_materials_equipment_success" id="popup_delete_materials_equipment_success" style="display: none;">
	<div class="box_popup">
		<div class="box_content d_flex flex_column align_c">
			<img class="img_p_succ" src="../images/st_tc.png" alt="">
			<p class="p_add_succ color_grey font_s15 line_h24 text_a_c">
				Xóa vật tư thiết bị <strong>Kìm</strong> thành công!</p>
			<button class="btn_close back_blue color_white font_s15 line_h18 font_w500 cursor_p">Đóng
				<a href="/danh-sach-vat-tu-thiet-bi.html"></a>
			</button>
		</div>
	</div>
</div>