<!-- thông báo logout -->
<div class="popup logout" id="logout" style="display: none;">
	<div class="container_popup d_flex margin_a">
		<div class="main_popup_logout">
			<div class="header_popup_blue d_flex align_c position_r">
				<p class="d_flex margin_a color_white line_h19 font_s16 font_wB">Đăng xuất</p>
				<img class="position_a close_w_popup_blue btn_close cursor_p" src="../images/close_w.png" alt="">
			</div>
			<div class="title_popup_logout text_a_c font_s15 line_h22">
				Bạn có chắc chắn muốn đăng xuất khỏi phần mềm
				Quản lý kho vật tư xây dựng 365?
			</div>
			<div class="d_flex align_c space_b button_popup_logout">
				<button class="button_close color_blue btn_close">Hủy</button>
				<button class="button_accp color_white">Đồng ý</button>
			</div>
		</div>
	</div>
</div>

<!-- nhắc nhở -->

<div class="popup_nhac_nho back_w position_a" id="popup_nhac_nho" style="display: none;z-index: 3">
	<div class="container_popup">
		<div class="main_nhac_nho position_r">
			<div class="header_popup_nhac_nho d_flex space_b align_c">
				<div class="name_popup">
					<p class="font_wB color_grey font_s18">Nhắc nhở</p>
				</div>
				<div class="img_close_popup btn_close2 cursor_p">
					<img src="../images/close_b.png" alt="">
				</div>
			</div>
			<div class="body_popup_nhac_nho">
				<div class='item_prompt active_unread cursor_p'>
					<div class="d_flex ">
						<img class="img_popup_prompt" src="../images/boder_major.png" alt="">
						<div>
							<p class="color_grey line_h24 font_s15 font_w500">[Nghiệp vụ kho chờ xử lý]</p>
							<p class="color_grey line_h20 font_s14">Phiếu nhập kho PNK-0000 được thêm mới bởi Nguyễn Văn
								Nam đang chờ bạn duyệt!</p>
						</div>
					</div>
					<p class="text_a_r time_line_prompt font_s12 line_h14">8:00, 20/05/2021</p>
				</div>
				<div class='item_prompt active_unread cursor_p'>
					<div class="d_flex ">
						<img class="img_popup_prompt" src="../images/boder_major.png" alt="">
						<div>
							<p class="color_grey line_h24 font_s15 font_w500">[Nghiệp vụ kho chờ xử lý]</p>
							<p class="color_grey line_h20 font_s14">Phiếu nhập kho PNK-0000 sắp hết hạn hãy hoàn thành
								ngay! (18:00 - 10/10/2020)</p>
						</div>
					</div>
					<p class="text_a_r time_line_prompt font_s12 line_h14">8:00, 20/05/2021</p>
				</div>
				<div class='item_prompt cursor_p'>
					<div class="d_flex ">
						<img class="img_popup_prompt" src="../images/boder_major.png" alt="">
						<div>
							<p class="color_grey line_h24 font_s15 font_w500">[Nghiệp vụ kho chờ xử lý]</p>
							<p class="color_grey line_h20 font_s14">Phiếu xuất kho PXK-0000 sắp hết hạn hãy hoàn thành
								ngay! (18:00 - 10/10/2020)</p>
						</div>
					</div>
					<p class="text_a_r time_line_prompt font_s12 line_h14">8:00, 20/05/2021</p>
				</div>
				<div class='item_prompt active_unread cursor_p'>
					<div class="d_flex ">
						<img class="img_popup_prompt" src="../images/boder_major.png" alt="">
						<div>
							<p class="color_grey line_h24 font_s15 font_w500">[Nghiệp vụ kho chờ xử lý]</p>
							<p class="color_grey line_h20 font_s14">Phiếu nhập kho PNK-0000 sắp hết hạn hãy hoàn thành
								ngay! (18:00 - 10/10/2020)</p>
						</div>
					</div>
					<p class="text_a_r time_line_prompt font_s12 line_h14">8:00, 20/05/2021</p>
				</div>
			</div>
			<div class="bottom_popup_nhac_nho d_flex space_b align_c position_a">
				<p class="d_flex margin_a color_blue cursor_p">Đánh dấu đã xem tất cả</p>
			</div>
		</div>
	</div>
</div>

<!-- thông báo -->

<div class="popup_thong_bao back_w position_a" id="popup_thong_bao" style="display: none; z-index: 3">
	<div class="container_popup">
		<div class="main_nhac_nho position_r">
			<div class="header_popup_nhac_nho d_flex space_b align_c">
				<div class="name_popup">
					<p class="font_wB color_grey font_s18">Thông báo</p>
				</div>
				<div class="img_close_popup cursor_p btn_close2">
					<img src="../images/close_b.png" alt="">
				</div>
			</div>
			<div class="body_popup_nhac_nho">
				<div class='item_prompt active_unread cursor_p'>
					<div class="d_flex ">
						<img class="img_popup_prompt" src="../images/ava1.png" alt="">
						<div>
							<p class="color_grey line_h24 font_s15 font_w500">[Danh sách vật tư thiết bị]</p>
							<p class="color_grey line_h20 font_s14">Nguyễn Trần Trung Quân vừa thêm mới vật tư thiết bị
								Ống nhựa</p>
						</div>
					</div>
					<p class="text_a_r time_line_prompt font_s12 line_h14">8:00, 20/05/2021</p>
				</div>
				<div class='item_prompt active_unread cursor_p'>
					<div class="d_flex ">
						<img class="img_popup_prompt" src="../images/ava1.png" alt="">
						<div>
							<p class="color_grey line_h24 font_s15 font_w500">[Hãng sản xuất]</p>
							<p class="color_grey line_h20 font_s14">Nguyễn Trần Trung Quân vừa thêm mới hãng sản xuất
								Tiền Phong</p>
						</div>
					</div>
					<p class="text_a_r time_line_prompt font_s12 line_h14">8:00, 20/05/2021</p>
				</div>
				<div class='item_prompt cursor_p'>
					<div class="d_flex ">
						<img class="img_popup_prompt" src="../images/ava1.png" alt="">
						<div>
							<p class="color_grey line_h24 font_s15 font_w500">[Nhóm vật tư thiết bị]</p>
							<p class="color_grey line_h20 font_s14">Nguyễn Trần Trung Quân vừa thêm mới nhóm vật tư
								thiết bị Nhóm vật liệu 1</p>
						</div>
					</div>
					<p class="text_a_r time_line_prompt font_s12 line_h14">8:00, 20/05/2021</p>
				</div>
				<div class='item_prompt active_unread cursor_p'>
					<div class="d_flex ">
						<img class="img_popup_prompt" src="../images/ava1.png" alt="">
						<div>
							<p class="color_grey line_h24 font_s15 font_w500">[Điều chuyển kho]</p>
							<p class="color_grey line_h20 font_s14">Nguyễn Trần Trung Quân vừa thêm mới phiếu điều chuyển kho TCK-0000</p>
						</div>
					</div>
					<p class="text_a_r time_line_prompt font_s12 line_h14">8:00, 20/05/2021</p>
				</div>
			</div>
			<div class="bottom_popup_nhac_nho d_flex space_b align_c position_a">
				<p class="d_flex margin_a color_blue cursor_p">Đánh dấu đã xem tất cả</p>
			</div>
		</div>
	</div>
</div>

<div class="popup popup_log_out" id="popup_log_out" style="display: none;">
  <div class="main_popup_500">
    <div class="head_popup_blue align_c position_r">
      <p class="font_s16 line_19 font_wB title_header_popup_blue text_a_c color_white">Đăng xuất</p>
      <img class="close_header_popup_blue cursor_p position_a btn_close2" src="../images/close_w.png" alt=""
        onclick="openAndHide('popup_log_out','')">
    </div>
    <div class="body_popup_blue_500">
      <p class="pd_69 text_a_c color_grey">Bạn có chắc chắn muốn đăng xuất khỏi phần mềm<br>
        <strong> Quản lý kho vật tư xây dựng 365?</strong>
      </p>
      <div class=" d_flex flex_center">
        <button class="button_close back_w color_blue font_s15 line_h18 font_w500 mr_68"
          onclick="openAndHide('popup_log_out','')">Hủy</button>
        <button class="button_accp back_blue color_white font_s15 line_h18 font_w500">Đồng ý
					<a href="dang-xuat.html"></a>
				</button>
      </div>
    </div>
  </div>
</div>