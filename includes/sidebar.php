<div class="main_sidebar" id="main_sidebar">
  <div class="header_sidebar position_r">
    <img class="" src="../images/timviec365.png" alt="">
    <a class="href_item" href="/tong-quan.html"></a>
  </div>
  <div class="body_sidebar">
    <ul class="ul_sidebar">
      <li class="li_sidebar position_r active1">
        <a href="/tong-quan.html">
          <div class="item_sidebar d_flex flex_start align_c item_sidebar_cha">
            <div class="img_li d_flex space_a align_c">
              <img class="img_b icon_sidebar_hw" id="" src="../images/home_b.png" alt="">
              <img class="img_w icon_sidebar_hw display_none" id="" src="../images/home_w.png" alt="">
            </div>
            <p class="p_item_sidebar font_s16 line_h19 font_w500 color_blue">Trang chủ</p>
          </div>
        </a>
      </li>
      <li class="li_sidebar position_r active2">
        <a href="/nghiep-vu-kho-cho-xu-ly.html">
          <div class="item_sidebar d_flex flex_start align_c item_sidebar_cha">
            <div class="img_li d_flex space_a align_c">
              <img class="img_b icon_sidebar_hw" id="" src="../images/major_b.png" alt="">
              <img class="img_w icon_sidebar_hw display_none" id="" src="../images/major_w.png" alt="">
            </div>
            <p class="p_item_sidebar font_s16 line_h19 font_w500 color_blue">Nghiệp vụ kho chờ xử lý</p>
          </div>
        </a>
      </li>
      <?php if(in_array(1, $ro_vattu) || in_array(1, $ro_nhom_vt) || in_array(1, $ro_dvt) || in_array(1, $ro_hsx)) {?>
        <li class="li_sidebar">
          <div>	
            <div class="item_sidebar item_sidebar_cha d_flex flex_start align_c">
              <div class="img_li d_flex space_a align_c">
                <img class="img_b icon_sidebar_hw" src="../images/information_b.png" alt="">
                <img class="img_w icon_sidebar_hw display_none" src="../images/information_w.png" alt="">
              </div>
              <p class="p_item_sidebar font_s16 line_h19 font_w500 color_blue">Thông tin vật tư thiết bị</p>
            </div>
            <ul class="ul_sidebar_con position_r information_ul_sidebar_con">
              <?php if(in_array(1, $ro_vattu)) {?>
                <li class="li_sidebar_con active3">
                  <a href="/danh-sach-vat-tu-thiet-bi.html" class="">
                    <div class="item_sidebar position_r  d_flex align_c">
                      <div class="backgroud_item_sidebar"></div>
                      <p class="p_item_sidebar font_s16 line_h19 font_w500 color_blue">Danh sách vật tư thiết bị</p>
                    </div>
                  </a>
                </li>
              <?php }?>
              <?php if(in_array(1, $ro_nhom_vt)) {?>
                <li class="li_sidebar_con active4">
                  <a href="/nhom-vat-tu-thiet-bi.html">
                    <div class="item_sidebar position_r  d_flex align_c">
                      <div class="backgroud_item_sidebar"></div>
                      <p class="p_item_sidebar font_s16 line_h19 font_w500 color_blue">Nhóm vật tư thiết bị</p>

                    </div>
                  </a>
                </li>
              <?php }?>
              <?php if(in_array(1, $ro_hsx)) {?>
                <li class="li_sidebar_con active5">
                  <a href="/hang-san-xuat.html">
                    <div class="item_sidebar position_r  d_flex align_c">
                      <div class="backgroud_item_sidebar"></div>
                      <p class="p_item_sidebar font_s16 line_h19 font_w500 color_blue">Hãng sản xuất</p>

                    </div>
                  </a>
                </li>
              <?php }?>
              <?php if(in_array(1, $ro_dvt)) {?>
                <li class="li_sidebar_con active6">
                  <a href="/don-vi-tinh.html">
                    <div class="item_sidebar position_r  d_flex align_c">
                      <div class="backgroud_item_sidebar"></div>
                      <p class="p_item_sidebar font_s16 line_h19 font_w500 color_blue">Đơn vị tính</p>

                    </div>
                  </a>
                </li>
              <?php }?>
            </ul>
          </div>
        </li>
      <?php }?>

      <?php if(in_array(1, $ro_ton_kho)) {?>
        <li class="li_sidebar position_r active7">
          <a href="/danh-sach-kho.html">
            <div class="item_sidebar d_flex flex_start align_c item_sidebar_cha">
              <div class="img_li d_flex space_a align_c">
                <img class="img_b icon_sidebar_hw icon_sidebar_hw" id="" src="../images/inventory_b.png" alt="">
                <img class="img_w icon_sidebar_hw icon_sidebar_hw display_none" id="" src=" ../images/inventory_w.png" alt="">
              </div>
              <p class="p_item_sidebar font_s16 line_h19 font_w500 color_blue">Tồn kho</p>
            </div>
          </a>
        </li>
      <?php }?>

      <?php if(in_array(1, $ro_nhap_kho)) {?>
        <li class="li_sidebar position_r active8">
          <a href="/nhap-kho.html">
            <div class="item_sidebar d_flex flex_start align_c item_sidebar_cha">
              <div class="img_li d_flex space_a align_c">
                <img class="img_b icon_sidebar_hw" id="" src="../images/add_b.png" alt="">
                <img class="img_w icon_sidebar_hw display_none" id="" src="../images/add_w.png" alt="">
              </div>
              <p class="p_item_sidebar font_s16 line_h19 font_w500 color_blue">Nhập kho</p>
            </div>
          </a>
        </li>
      <?php }?>

      <?php if(in_array(1, $ro_xuat_kho)) {?>
        <li class="li_sidebar position_r active9">
          <a href="/xuat-kho.html">
            <div class="item_sidebar d_flex flex_start align_c item_sidebar_cha">
              <div class="img_li d_flex space_a align_c">
                <img class="img_b icon_sidebar_hw" id="" src="../images/export_b.png" alt="">
                <img class="img_w icon_sidebar_hw display_none" id="" src=" ../images/export_w.png" alt="">
              </div>
              <p class="p_item_sidebar font_s16 line_h19 font_w500 color_blue">Xuất kho</p>
            </div>
          </a>
        </li>
      <?php }?>

      <?php if(in_array(1, $ro_dc_kho)) {?>
        <li class="li_sidebar position_r active10">
          <a href="/dieu-chuyen-kho.html">
            <div class="item_sidebar d_flex flex_start align_c item_sidebar_cha">
              <div class="img_li d_flex space_a align_c">
                <img class="img_b icon_sidebar_hw" id="" src="../images/change_b.png" alt="">
                <img class="img_w icon_sidebar_hw display_none" id="" src=" ../images/change_w.png" alt="">
              </div>
              <p class="p_item_sidebar font_s16 line_h19 font_w500 color_blue">Điều chuyển kho</p>
            </div>
          </a>
        </li>
      <?php }?>

      <?php if(in_array(1, $ro_kk_kho)) {?>
        <li class="li_sidebar position_r active11">
          <a href="/kiem-ke-kho.html">
            <div class="item_sidebar d_flex flex_start align_c item_sidebar_cha">
              <div class="img_li d_flex space_a align_c">
                <img class="img_b icon_sidebar_hw" id="" src="../images/list_b.png" alt="">
                <img class="img_w icon_sidebar_hw display_none" id="" src=" ../images/list_w.png" alt="">
              </div>
              <p class="p_item_sidebar font_s16 line_h19 font_w500 color_blue">Kiểm kê</p>
            </div>
          </a>
        </li>
      <?php }?>

      <?php if(in_array(1, $ro_bao_cao)) {?>
        <li class="li_sidebar">
          <div class="item_sidebar item_sidebar_cha d_flex flex_start align_c">
            <div class="img_li d_flex space_a align_c">
              <img class="img_b icon_sidebar_hw" src="../images/char_b.png" alt="">
              <img class="img_w icon_sidebar_hw display_none" src="../images/char_w.png" alt="">
            </div>
            <p class="p_item_sidebar font_s16 line_h19 font_w500 color_blue">Báo cáo</p>
          </div>
          <ul class="ul_sidebar_con position_r report_ul_sidebar_con">
            <li class="li_sidebar_con active12">
              <a href="/bao-cao-nhap-kho.html">
                <div class="item_sidebar position_r  d_flex align_c">
                  <div class="backgroud_item_sidebar"></div>
                  <p class="p_item_sidebar font_s16 line_h19 font_w500 color_blue">Báo cáo nhập kho</p>

                </div>
              </a>
            </li>
            <li class="li_sidebar_con active13">
              <a href="/bao-cao-xuat-kho.html">
                <div class="item_sidebar position_r  d_flex align_c">
                  <div class="backgroud_item_sidebar"></div>
                  <p class="p_item_sidebar font_s16 line_h19 font_w500 color_blue">Báo cáo xuất kho</p>

                </div>
              </a>
            </li>
            <li class="li_sidebar_con active14">
              <a href="/bao-cao-ton-kho.html">
                <div class="item_sidebar position_r  d_flex align_c">
                  <div class="backgroud_item_sidebar"></div>
                  <p class="p_item_sidebar font_s16 line_h19 font_w500 color_blue">Báo cáo tồn kho</p>

                </div>
              </a>
            </li>
          </ul>
        </li>
      <?php }?>

      <?php if(in_array(1, $ro_phan_quyen) || $_SESSION['quyen'] == 1) {?>
        <li class="li_sidebar position_r active15">
          <a href="/phan-quyen.html">
            <div class="item_sidebar d_flex flex_start align_c item_sidebar_cha">
              <div class="img_li d_flex space_a align_c">
                <img class="img_b icon_sidebar_hw" id="" src="../images/decentralization_b.png" alt="">
                <img class="img_w icon_sidebar_hw display_none" id="" src=" ../images/decentralization_w.png" alt="">
              </div>
              <p class="p_item_sidebar font_s16 line_h19 font_w500 color_blue">Phân quyền</p>
            </div>
          </a>
        </li>
        <?php }?>
      <?php if($_SESSION['quyen'] == 2){ ?>
        <li class="li_sidebar position_r active16">
          <a href="/du-lieu-da-xoa-gan-day.html">
            <div class="item_sidebar d_flex flex_start align_c item_sidebar_cha">
              <div class="img_li d_flex space_a align_c">
                <img class="img_b icon_sidebar_hw" id="" src="../images/delete_b.png" alt="">
                <img class="img_w icon_sidebar_hw display_none" id="" src=" ../images/delete_w.png" alt="">
              </div>
              <p class="p_item_sidebar font_s16 line_h19 font_w500 color_blue">Dữ liệu đã xóa gần đây</p>
            </div>
          </a>
        </li>
      <?php } ?>
      <!-- <li class="li_sidebar position_r active17">
        <a href="/cai-dat-chung.html">
          <div class="item_sidebar d_flex flex_start align_c item_sidebar_cha">
            <div class="img_li d_flex space_a align_c">
              <img class="img_b icon_sidebar_hw" id="" src="../images/setting_b.png" alt="">
              <img class="img_w icon_sidebar_hw display_none" id="" src=" ../images/setting_w.png" alt="">
            </div>
            <p class="p_item_sidebar font_s16 line_h19 font_w500 color_blue">Cài đặt</p>
          </div>
        </a>
      </li> -->
      <li class="li_sidebar position_r">
        <a href="#">
          <div class="item_sidebar d_flex flex_start align_c item_sidebar_cha">
            <div class="img_li d_flex space_a align_c">
              <img class="img_b icon_sidebar_hw" id="" src="../images/convert_b.png" alt="">
              <img class="img_w icon_sidebar_hw display_none" id="" src=" ../images/convert_w.png" alt="">
            </div>
            <p class="p_item_sidebar font_s16 line_h19 font_w500 color_blue">Chuyển đổi số 365</p>
          </div>
        </a>
      </li>
    </ul>
  </div>
</div>