<?php include("../home/config.php"); ?>
<!DOCTYPE html>
<html lang="vi">

<head>
    <title>Trang hướng dẫn</title>
    <meta charset="UTF-8">
    <meta name="robots" content="noindex,nofollow" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://timviec365.vn/favicon.ico" rel="shortcut icon">
    <link rel="preload" as="style" href="../css/style.css?v=<?= $ver ?>">
    <link rel="stylesheet" media="all" href="../css/style.css?v=<?= $ver ?>" media="all"
        onload="if (media != 'all')media='all'">
    <link rel="preload" as="style" href="../css/style_n.css?v=<?= $ver ?>">
    <!-- <link rel="preload" as="style" href="../css/style_h.css?v=<?= $ver ?>"> -->
    <link rel="stylesheet" media="all" href="../css/style_n.css?v=<?= $ver ?>" media="all"
        onload="if (media != 'all')media='all'">
    <!-- <link rel="stylesheet" media="all" href="../css/style_h.css?v=<?= $ver ?>" media="all"
        onload="if (media != 'all')media='all'"> -->
</head>

<body>
    <div class="instruct_page" id="instruct_page">
        <div class="main_instruct">
            <div class="box_instruct">
                <div class="header_home">
                    <?php include("../includes/inc_header.php"); ?>
                    <div class="header_body">
                        <div class="box_header_body position_r">
                            <div class="picture_header_body">
                                <!-- <img src="../images/bg_hd.png" alt="" class="picture_header_body"> -->
                                <div class="overlay"></div>
                            </div>
                            <div class="d_ins_b d_flex align_c space_b position_a">
                                <div class="instruct_tit">
                                    <p class="font_s30 line_h42 color_white font_w700">Hướng dẫn sử dụng phần mềm<br>
                                        Quản lý kho vật tư xây dựng 365</p>
                                    <div class="mobi_download d_flex">
                                        <a href="#" class="gg_play_download">
                                            <img src="../images/gg_play_download.png" alt="">
                                        </a>
                                        <a href="#" class="app_store_download">
                                            <img src="../images/app_store_download.png" alt="">
                                        </a>
                                    </div>
                                </div>
                                <div class="video_user_manual">
                                    <iframe width="100%" height="100%" src="https://youtube.com/embed/DRGRquRiWYc">
                                    </iframe>
                                </div>
                            </div>
                            <div class="chat_w_us d_flex align_e flex_column">
                                <picture>
                                    <img src="../images/chat_skype.png" alt="">
                                </picture>
                                <p class="tit_chat_us font_s16 line_h19 color_white font_w500">Hãy chat với chúng tôi
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content_instruct d_flex space_b">
                    <div class="nav_sidebar_ins">
                        <ul class="ul_sidebar_ins">
                            <li class="li_sidebar_ins sidebar_actived">
                                <div class="d_flex flex_start align_c">
                                    <div class="img_li_ins">
                                        <img class="img_b_ins display_none" id="" src="../images/home_b.png" alt="">
                                        <img class="img_w_ins" id="" src="../images/home_w.png" alt="">
                                    </div>
                                    <p class="tit_sidebar_ins font_s16 line_h19 font_w500 color_blue">Trang chủ
                                    </p>
                                </div>
                                <a class="href_item" href="#"></a>
                            </li>
                            <li class="li_sidebar_ins">
                                <div class="d_flex flex_start align_c">
                                    <div class="img_li_ins">
                                        <img class="img_b_ins" id="" src="../images/major_b.png" alt="">
                                        <img class="img_w_ins display_none" id="" src="../images/major_w.png" alt="">
                                    </div>
                                    <p class="tit_sidebar_ins font_s16 line_h19 font_w500 color_blue">Nghiệp vụ kho chờ
                                        xử lý
                                    </p>
                                </div>
                                <a class="href_item" href="#"></a>
                            </li>
                            <li class="li_sidebar_ins">
                                <div class="d_flex flex_start align_c">
                                    <div class="img_li_ins">
                                        <img class="img_b_ins" id="" src="../images/information_b.png" alt="" alt="">
                                        <img class="img_w_ins display_none" id="" src="../images/information_w.png"
                                            alt="" alt="">
                                    </div>
                                    <p class="tit_sidebar_ins font_s16 line_h19 font_w500 color_blue">Thông tin vật tư
                                        thiết bị
                                    </p>
                                </div>
                                <a class="href_item" href="#"></a>
                            </li>
                            <li class="li_sidebar_ins">
                                <div class="d_flex flex_start align_c">
                                    <div class="img_li_ins">
                                        <img class="img_b_ins" id="" src="../images/inventory_b.png" alt="">
                                        <img class="img_w_ins display_none" id="" src="../images/inventory_w.png"
                                            alt="">
                                    </div>
                                    <p class="tit_sidebar_ins font_s16 line_h19 font_w500 color_blue">Tồn kho
                                    </p>
                                </div>
                                <a class="href_item" href="#"></a>
                            </li>
                            <li class="li_sidebar_ins">
                                <div class="d_flex flex_start align_c">
                                    <div class="img_li_ins">
                                        <img class="img_b_ins" id="" src="../images/add_b.png" alt="">
                                        <img class="img_w_ins display_none" id="" src="../images/add_w.png" alt="">
                                    </div>
                                    <p class="tit_sidebar_ins font_s16 line_h19 font_w500 color_blue">Nhập kho
                                    </p>
                                </div>
                                <a class="href_item" href="#"></a>
                            </li>
                            <li class="li_sidebar_ins">
                                <div class="d_flex flex_start align_c">
                                    <div class="img_li_ins">
                                        <img class="img_b_ins" id="" src="../images/export_b.png" alt="">
                                        <img class="img_w_ins display_none" id="" src="../images/export_w.png" alt="">
                                    </div>
                                    <p class="tit_sidebar_ins font_s16 line_h19 font_w500 color_blue">Xuất kho
                                    </p>
                                </div>
                                <a class="href_item" href="#"></a>
                            </li>
                            <li class="li_sidebar_ins">
                                <div class="d_flex flex_start align_c">
                                    <div class="img_li_ins">
                                        <img class="img_b_ins" id="" src="../images/change_b.png" alt="">
                                        <img class="img_w_ins display_none" id="" src="../images/change_w.png" alt="">
                                    </div>
                                    <p class="tit_sidebar_ins font_s16 line_h19 font_w500 color_blue">Điều chuyển kho
                                    </p>
                                </div>
                                <a class="href_item" href="#"></a>
                            </li>
                            <li class="li_sidebar_ins">
                                <div class="d_flex flex_start align_c">
                                    <div class="img_li_ins">
                                        <img class="img_b_ins" id="" src="../images/list_b.png" alt="">
                                        <img class="img_w_ins display_none" id="" src="../images/list_w.png" alt="">
                                    </div>
                                    <p class="tit_sidebar_ins font_s16 line_h19 font_w500 color_blue">Kiểm kê
                                    </p>
                                </div>
                                <a class="href_item" href="#"></a>
                            </li>
                            <li class="li_sidebar_ins">
                                <div class="d_flex flex_start align_c">
                                    <div class="img_li_ins">
                                        <img class="img_b_ins" id="" src="../images/char_b.png" alt="">
                                        <img class="img_w_ins display_none" id="" src="../images/char_w.png" alt="">
                                    </div>
                                    <p class="tit_sidebar_ins font_s16 line_h19 font_w500 color_blue">Báo cáo
                                    </p>
                                </div>
                                <a class="href_item" href="#"></a>
                            </li>
                            <li class="li_sidebar_ins">
                                <div class="d_flex flex_start align_c">
                                    <div class="img_li_ins">
                                        <img class="img_b_ins" id="" src="../images/decentralization_b.png" alt="">
                                        <img class="img_w_ins display_none" id="" src="../images/decentralization_w.png"
                                            alt="">
                                    </div>
                                    <p class="tit_sidebar_ins font_s16 line_h19 font_w500 color_blue">Phân quyền
                                    </p>
                                </div>
                                <a class="href_item" href="#"></a>
                            </li>
                            <li class="li_sidebar_ins">
                                <div class="d_flex flex_start align_c">
                                    <div class="img_li_ins">
                                        <img class="img_b_ins" id="" src="../images/delete_b.png" alt="">
                                        <img class="img_w_ins display_none" id="" src="../images/delete_w.png" alt="">
                                    </div>
                                    <p class="tit_sidebar_ins font_s16 line_h19 font_w500 color_blue">Dữ liệu đã xóa gần
                                        đây
                                    </p>
                                </div>
                                <a class="href_item" href="#"></a>
                            </li>
                            <li class="li_sidebar_ins">
                                <div class="d_flex flex_start align_c">
                                    <div class="img_li_ins">
                                        <img class="img_b_ins" id="" src="../images/setting_b.png" alt="">
                                        <img class="img_w_ins display_none" id="" src="../images/setting_w.png" alt="">
                                    </div>
                                    <p class="tit_sidebar_ins font_s16 line_h19 font_w500 color_blue">Cài đặt
                                    </p>
                                </div>
                                <a class="href_item" href="#"></a>
                            </li>

                        </ul>
                    </div>
                    <div class="nav_sidebar_ins_tbl">
                        <div class="nav_sidebar_selected d_flex flex_start align_c position_r cursor_p">
                            <div class="img_li_ins">
                                <img class="img_b_ins display_none" id="" src="../images/home_b.png" alt="">
                                <img class="img_w_ins" id="" src="../images/home_w.png" alt="">
                            </div>
                            <p class="tit_sidebar_ins font_s16 line_h19 font_w500 color_white">Trang chủ
                            </p>
                        </div>
                        <div class="dropdown">
                            <span class="dropdown_menu1"></span>
                            <span class="dropdown_menu2"></span>
                        </div>
                        <ul class="ul_sidebar_ins position_a" style="display: none;">
                            <li class="li_sidebar_ins sidebar_actived">
                                <div class="d_flex flex_start align_c">
                                    <div class="img_li_ins">
                                        <img class="img_b_ins display_none" id="" src="../images/home_b.png" alt="">
                                        <img class="img_w_ins" id="" src="../images/home_w.png" alt="">
                                    </div>
                                    <p class="tit_sidebar_ins font_s16 line_h19 font_w500 color_blue">Trang chủ
                                    </p>
                                </div>
                                <a class="href_item" href="#"></a>
                            </li>
                            <li class="li_sidebar_ins">
                                <div class="d_flex flex_start align_c">
                                    <div class="img_li_ins">
                                        <img class="img_b_ins" id="" src="../images/major_b.png" alt="">
                                        <img class="img_w_ins display_none" id="" src="../images/major_w.png" alt="">
                                    </div>
                                    <p class="tit_sidebar_ins font_s16 line_h19 font_w500 color_blue">Nghiệp vụ kho chờ
                                        xử lý
                                    </p>
                                </div>
                                <a class="href_item" href="#"></a>
                            </li>
                            <li class="li_sidebar_ins">
                                <div class="d_flex flex_start align_c">
                                    <div class="img_li_ins">
                                        <img class="img_b_ins" id="" src="../images/information_b.png" alt="" alt="">
                                        <img class="img_w_ins display_none" id="" src="../images/information_w.png"
                                            alt="" alt="">
                                    </div>
                                    <p class="tit_sidebar_ins font_s16 line_h19 font_w500 color_blue">Thông tin vật tư
                                        thiết bị
                                    </p>
                                </div>
                                <a class="href_item" href="#"></a>
                            </li>
                            <li class="li_sidebar_ins">
                                <div class="d_flex flex_start align_c">
                                    <div class="img_li_ins">
                                        <img class="img_b_ins" id="" src="../images/inventory_b.png" alt="">
                                        <img class="img_w_ins display_none" id="" src="../images/inventory_w.png"
                                            alt="">
                                    </div>
                                    <p class="tit_sidebar_ins font_s16 line_h19 font_w500 color_blue">Tồn kho
                                    </p>
                                </div>
                                <a class="href_item" href="#"></a>
                            </li>
                            <li class="li_sidebar_ins">
                                <div class="d_flex flex_start align_c">
                                    <div class="img_li_ins">
                                        <img class="img_b_ins" id="" src="../images/add_b.png" alt="">
                                        <img class="img_w_ins display_none" id="" src="../images/add_w.png" alt="">
                                    </div>
                                    <p class="tit_sidebar_ins font_s16 line_h19 font_w500 color_blue">Nhập kho
                                    </p>
                                </div>
                                <a class="href_item" href="#"></a>
                            </li>
                            <li class="li_sidebar_ins">
                                <div class="d_flex flex_start align_c">
                                    <div class="img_li_ins">
                                        <img class="img_b_ins" id="" src="../images/export_b.png" alt="">
                                        <img class="img_w_ins display_none" id="" src="../images/export_w.png" alt="">
                                    </div>
                                    <p class="tit_sidebar_ins font_s16 line_h19 font_w500 color_blue">Xuất kho
                                    </p>
                                </div>
                                <a class="href_item" href="#"></a>
                            </li>
                            <li class="li_sidebar_ins">
                                <div class="d_flex flex_start align_c">
                                    <div class="img_li_ins">
                                        <img class="img_b_ins" id="" src="../images/change_b.png" alt="">
                                        <img class="img_w_ins display_none" id="" src="../images/change_w.png" alt="">
                                    </div>
                                    <p class="tit_sidebar_ins font_s16 line_h19 font_w500 color_blue">Điều chuyển kho
                                    </p>
                                </div>
                                <a class="href_item" href="#"></a>
                            </li>
                            <li class="li_sidebar_ins">
                                <div class="d_flex flex_start align_c">
                                    <div class="img_li_ins">
                                        <img class="img_b_ins" id="" src="../images/list_b.png" alt="">
                                        <img class="img_w_ins display_none" id="" src="../images/list_w.png" alt="">
                                    </div>
                                    <p class="tit_sidebar_ins font_s16 line_h19 font_w500 color_blue">Kiểm kê
                                    </p>
                                </div>
                                <a class="href_item" href="#"></a>
                            </li>
                            <li class="li_sidebar_ins">
                                <div class="d_flex flex_start align_c">
                                    <div class="img_li_ins">
                                        <img class="img_b_ins" id="" src="../images/char_b.png" alt="">
                                        <img class="img_w_ins display_none" id="" src="../images/char_w.png" alt="">
                                    </div>
                                    <p class="tit_sidebar_ins font_s16 line_h19 font_w500 color_blue">Báo cáo
                                    </p>
                                </div>
                                <a class="href_item" href="#"></a>
                            </li>
                            <li class="li_sidebar_ins">
                                <div class="d_flex flex_start align_c">
                                    <div class="img_li_ins">
                                        <img class="img_b_ins" id="" src="../images/decentralization_b.png" alt="">
                                        <img class="img_w_ins display_none" id="" src="../images/decentralization_w.png"
                                            alt="">
                                    </div>
                                    <p class="tit_sidebar_ins font_s16 line_h19 font_w500 color_blue">Phân quyền
                                    </p>
                                </div>
                                <a class="href_item" href="#"></a>
                            </li>
                            <li class="li_sidebar_ins">
                                <div class="d_flex flex_start align_c">
                                    <div class="img_li_ins">
                                        <img class="img_b_ins" id="" src="../images/delete_b.png" alt="">
                                        <img class="img_w_ins display_none" id="" src="../images/delete_w.png" alt="">
                                    </div>
                                    <p class="tit_sidebar_ins font_s16 line_h19 font_w500 color_blue">Dữ liệu đã xóa gần
                                        đây
                                    </p>
                                </div>
                                <a class="href_item" href="#"></a>
                            </li>
                            <li class="li_sidebar_ins">
                                <div class="d_flex flex_start align_c">
                                    <div class="img_li_ins">
                                        <img class="img_b_ins" id="" src="../images/setting_b.png" alt="">
                                        <img class="img_w_ins display_none" id="" src="../images/setting_w.png" alt="">
                                    </div>
                                    <p class="tit_sidebar_ins font_s16 line_h19 font_w500 color_blue">Cài đặt
                                    </p>
                                </div>
                                <a class="href_item" href="#"></a>
                            </li>

                        </ul>
                    </div>
                    <div class="block_ct_ins">
                        <div class="ct_ins_t">
                            <p class="tit_ins_t color_grey font_s16 line_h19 font_w500">1. Tiêu đề</p>
                            <div class="srt_ct_ins">
                                <p class="color_grey font_s14 line_h17 font_w400">- Tương lai về mặt sinh học và địa
                                    chất của
                                    Trái Đất có thể được ngoại suy bằng cách ước lượng những tác động trong dài hạn của
                                    một
                                    số yếu tố, bao.</p>
                                <p class="color_grey font_s14 line_h17 font_w400">- Tương lai về mặt sinh học và địa
                                    chất của
                                    Trái Đất có thể được ngoại suy bằng cách ước lượng những tác động trong dài hạn của
                                    một
                                    số yếu tố, bao gồm thành phần hóa học của bề mặt Trái Đất, tốc độ nguội đi ở bên
                                    trong
                                    của nó, những tương tác trọng lực với các vật thể khác trong hệ Mặt Trời, và độ sáng
                                    ngày càng tăng của Mặt Trời. Có một nhân tố bất định trong phép ngoại suy này, đó là
                                    những công nghệ mà loài người phát minh ra, chẳng hạn như kỹ thuật khí hậu; ảnh
                                    hưởng
                                    liên tục của chúng có khả năng dẫn đến những thay đổi lớn tới Trái Đất. Công nghệ là
                                    nguyên nhân gây ra sự kiện tuyệt chủng Holocen đang diễn ra và những tác động của nó
                                    có
                                    thể kéo dài tới năm triệu năm. Từ đó, công nghệ có khả năng sẽ dẫn đến sự tuyệt
                                    chủng
                                    của loài người, để hành tinh quay trở lại nhịp độ tiến hóa chậm hơn chỉ nhờ vào
                                    những
                                    quá trình tự nhiên diễn ra một cách lâu dài</p>
                                <div class="img_rvw d_flex flex_column align_c">
                                    <img src="../images/img_hd_rvw.png" alt="">
                                    <p class="color_grey font_s14 line_h17 font_w400">Hình 1. Đây là hình 1</p>
                                </div>
                            </div>
                        </div>
                        <div class="ct_ins_t">
                            <p class="tit_ins_t color_grey font_s16 line_h19 font_w500">1. Tiêu đề</p>
                            <div class="srt_ct_ins">
                                <p class="color_grey font_s14 line_h17 font_w400">- Tương lai về mặt sinh học và địa
                                    chất của
                                    Trái Đất có thể được ngoại suy bằng cách ước lượng những tác động trong dài hạn của
                                    một
                                    số yếu tố, bao.</p>
                                <p class="color_grey font_s14 line_h17 font_w400">- Tương lai về mặt sinh học và địa
                                    chất của
                                    Trái Đất có thể được ngoại suy bằng cách ước lượng những tác động trong dài hạn của
                                    một
                                    số yếu tố, bao gồm thành phần hóa học của bề mặt Trái Đất, tốc độ nguội đi ở bên
                                    trong
                                    của nó, những tương tác trọng lực với các vật thể khác trong hệ Mặt Trời, và độ sáng
                                    ngày càng tăng của Mặt Trời. Có một nhân tố bất định trong phép ngoại suy này, đó là
                                    những công nghệ mà loài người phát minh ra, chẳng hạn như kỹ thuật khí hậu; ảnh
                                    hưởng
                                    liên tục của chúng có khả năng dẫn đến những thay đổi lớn tới Trái Đất. Công nghệ là
                                    nguyên nhân gây ra sự kiện tuyệt chủng Holocen đang diễn ra và những tác động của nó
                                    có
                                    thể kéo dài tới năm triệu năm. Từ đó, công nghệ có khả năng sẽ dẫn đến sự tuyệt
                                    chủng
                                    của loài người, để hành tinh quay trở lại nhịp độ tiến hóa chậm hơn chỉ nhờ vào
                                    những
                                    quá trình tự nhiên diễn ra một cách lâu dài</p>
                                <div class="img_rvw d_flex flex_column align_c">
                                    <img src="../images/img_hd_rvw.png" alt="">
                                    <p class="color_grey font_s14 line_h17 font_w400">Hình 1. Đây là hình 1</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php include("footer-trang-chu.php"); ?>
            </div>
        </div>
    </div>
</body>
<script type="text/javascript" src="../js/jquery-3.4.1.min.js"></script>

<script>

    var menu_tabl = $('.icon_header');
    menu_tabl.click(function () {
        $('.menu_tabl').css('left', '-15px')
        $('.menu_tabl').css('top', '34px')
        $('.menu_tabl').toggle();
    })

    var sidebarActive = $('.li_sidebar_ins');
    sidebarActive.click(function () {
        $(this).parent().find('.li_sidebar_ins').removeClass('sidebar_actived');
        $(this).addClass('sidebar_actived');

        $(this).parent().find('.img_w_ins').addClass('display_none');
        $(this).parent().find('.img_b_ins').removeClass('display_none');

        $(this).find('.img_b_ins').addClass('display_none');
        $(this).find('.img_w_ins').removeClass('display_none');
        

    });

    var nav_sidebar_selected = $('.nav_sidebar_selected');
    nav_sidebar_selected.click(function(){
        if($('.nav_sidebar_ins_tbl .ul_sidebar_ins:hidden').length){
            $('.nav_sidebar_ins_tbl .ul_sidebar_ins,.dropdown_menu2').show();
            $('.dropdown_menu1').hide();
        }else{
            $('.nav_sidebar_ins_tbl .ul_sidebar_ins,.dropdown_menu2').hide();
            $('.dropdown_menu1').show();

        }

    });
    
    $(window).mouseup(function (e){
        if (!nav_sidebar_selected.is(e.target) && nav_sidebar_selected.has(e.target).length === 0) {
            $('.nav_sidebar_ins_tbl .ul_sidebar_ins,.dropdown_menu2').hide();
            $('.dropdown_menu1').show();
        }
    });

</script>

</html>