var page = $(".detail_wh").attr("data-page");
var kho = $(".detail_wh").attr("data-k");
var ht = $(".detail_wh").attr("data-ht");

var ngay_tao_start = $(".detail_wh").attr("data-ngts");
var ngay_tao_end = $(".detail_wh").attr("data-ngte");

var ngay_th_start = $(".detail_wh").attr("data-ngths");
var ngay_th_end = $(".detail_wh").attr("data-ngthe");


var input_val = $(".detail_wh").attr("data-ip");
var curr = $(".detail_wh").attr("data-dis");

$.ajax({
    url: "../render/tb_bc_xuatkho.php",
    type: "POST",
    data:{
        input_val: input_val,
        page: page,

        kho: kho,
        ht: ht,

        ngay_tao_start: ngay_tao_start,
        ngay_tao_end: ngay_tao_end,

        ngay_th_start: ngay_th_start,
        ngay_th_end: ngay_th_end,
        curr: curr

    },
    success: function(data){
        $(".detail_wh").append(data);
    }
});

$(".select_all_wh,.select_all_ex_wh").on('change', function(){
    var page = $(".detail_wh").attr("data-page");
    var kho = $('.select_all_wh').val();
    var ht = $(".select_all_ex_wh").val();
    var ngay_tao_start = $('#date_cr_start').attr("data");
    var ngay_tao_end = $('#date_cr_end').attr("data");
    var ngay_th_start = $('#date_fn_start').attr("data");
    var ngay_th_end = $('#date_fn_end').attr("data");

    var input_val = $("input[name='input_search']").val();
    var curr = $('.show_tr_tb').val();

    if(kho == "" && page != "" && ht == "" && ngay_tao_start == "" && ngay_tao_end == "" && ngay_th_start == "" && ngay_th_end == "" && input_val == "" && curr == 10){
        window.location.href = "/bao-cao-xuat-kho.html?dis=" + curr + '&page=' + page;
    }else{
        window.location.href = "/bao-cao-xuat-kho.html?kho=" + kho + "&hinhthuc=" + ht + "&ngts=" + ngay_tao_start + "&ngte=" + ngay_tao_end + "&ngths=" + ngay_th_start + "&ngthe=" + ngay_th_end + "&input=" + input_val + "&dis=" + curr + "&page=" + page;
    }
    
});

$('.report_wh_ex .date_cr img').click(function(){
    ele = $(this);
    $('#popup_select_date').show();
    $('.popup_select_date .btn_save').on("click", function () {
        selectDate(this, ele);

        var page = $(".detail_wh").attr("data-page");
        var kho = $('.select_all_wh').val();
        var ht = $(".select_all_ex_wh").val();

        var ngay_th_start = $('#date_fn_start').attr("data");
        var ngay_th_end = $('#date_fn_end').attr("data");

        var ngay_tao_start = $(this).parents('.content_popup').find('.d_flex.flex_column:nth-child(1) input').val();
        var ngay_tao_end = $(this).parents('.content_popup').find('.d_flex.flex_column:nth-child(2) input').val();

        var input_val = $("input[name='input_search']").val();
        var curr = $('.show_tr_tb').val();

        if(!compareTime(ngay_tao_start,ngay_tao_end)){
            if(kho == "" && page != "" && ht == "" && ngay_tao_start == "" && ngay_tao_end == "" && ngay_th_start == "" && ngay_th_end == "" && input_val == "" && curr == 10){
                window.location.href = "/bao-cao-xuat-kho.html?dis=" + curr + '&page=' + page;
            }else{
                window.location.href = "/bao-cao-xuat-kho.html?kho=" + kho + "&hinhthuc=" + ht + "&ngts=" + ngay_tao_start + "&ngte=" + ngay_tao_end + "&ngths=" + ngay_th_start + "&ngthe=" + ngay_th_end + "&input=" + input_val + "&dis=" + curr + "&page=" + page;
            }
        }
        
    });
});

$('.report_wh_ex .date_fn img').click(function(){
    ele = $(this);
    $('#popup_select_date').show();
    $('.popup_select_date .btn_save').on("click", function () {
        selectDate(this, ele);

        var page = $(".detail_wh").attr("data-page");
        var kho = $('.select_all_wh').val();
        var ht = $(".select_all_ex_wh").val();

        var ngay_tao_start = $('#date_cr_start').attr("data");
        var ngay_tao_end = $('#date_cr_end').attr("data");

        var ngay_th_start = $(this).parents('.content_popup').find('.d_flex.flex_column:nth-child(1) input').val();
        var ngay_th_end = $(this).parents('.content_popup').find('.d_flex.flex_column:nth-child(2) input').val();

        var input_val = $("input[name='input_search']").val();
        var curr = $('.show_tr_tb').val();

        if(!compareTime(ngay_th_start,ngay_th_end)){
            if(kho == "" && page != "" && ht == "" && ngay_tao_start == "" && ngay_tao_end == "" && ngay_th_start == "" && ngay_th_end == "" && input_val == "" && curr == 10){
                window.location.href = "/bao-cao-xuat-kho.html?dis=" + curr + '&page=' + page;
            }else{
                window.location.href = "/bao-cao-xuat-kho.html?kho=" + kho + "&hinhthuc=" + ht + "&ngts=" + ngay_tao_start + "&ngte=" + ngay_tao_end + "&ngths=" + ngay_th_start + "&ngthe=" + ngay_th_end + "&input=" + input_val + "&dis=" + curr + "&page=" + page;
            }
        }
        
    });
});


$(".icon_sr_wh").click(function(){
    var page = $(".detail_wh").attr("data-page");
    var kho = $('.select_all_wh').val();
    var ht = $(".select_all_ex_wh").val();
    var ngay_tao_start = $('#date_cr_start').attr("data");
    var ngay_tao_end = $('#date_cr_end').attr("data");
    var ngay_th_start = $('#date_fn_start').attr("data");
    var ngay_th_end = $('#date_fn_end').attr("data");

    var input_val = $("input[name='input_search']").val();
    var curr = $('.show_tr_tb').val();

    if(kho == "" && page != "" && ht == "" && ngay_tao_start == "" && ngay_tao_end == "" && ngay_th_start == "" && ngay_th_end == "" && input_val == "" && curr == 10){
        window.location.href = "/bao-cao-xuat-kho.html?dis=" + curr + '&page=' + page;
    }else{
        window.location.href = "/bao-cao-xuat-kho.html?kho=" + kho + "&hinhthuc=" + ht + "&ngts=" + ngay_tao_start + "&ngte=" + ngay_tao_end + "&ngths=" + ngay_th_start + "&ngthe=" + ngay_th_end + "&input=" + input_val + "&dis=" + curr + "&page=" + page;
    }
});

$(document).keyup(function (e) {
    if ($("input[name='input_search']").is(":focus") && (e.keyCode == 13)) {
        $(".icon_sr_wh").click();
    }
});

$(".btn_ex").click(function() {
    window.location.href = '../Excel/bao_cao_xuat.php';
});

function display(select){
    var curr = $(select).val();
    var page = $(".detail_wh").attr("data-page");
    var kho = $('.select_all_wh').val();
    var ht = $(".select_all_ex_wh").val();
    var ngay_tao_start = $('#date_cr_start').attr("data");
    var ngay_tao_end = $('#date_cr_end').attr("data");
    var ngay_th_start = $('#date_fn_start').attr("data");
    var ngay_th_end = $('#date_fn_end').attr("data");

    var input_val = $("input[name='input_search']").val();

    if(kho == "" && page != "" && ht == "" && ngay_tao_start == "" && ngay_tao_end == "" && ngay_th_start == "" && ngay_th_end == "" && input_val == "" && curr == 10){
        window.location.href = "/bao-cao-xuat-kho.html?dis=" + curr + '&page=' + page;
    }else{
        window.location.href = "/bao-cao-xuat-kho.html?kho=" + kho + "&hinhthuc=" + ht + "&ngts=" + ngay_tao_start + "&ngte=" + ngay_tao_end + "&ngths=" + ngay_th_start + "&ngthe=" + ngay_th_end + "&input=" + input_val + "&dis=" + curr + "&page=" + page;
    }
}
