var page = $(".detail_wh").attr("data-page");
var input_val = $(".detail_wh").attr("data-ip");

$.ajax({
    url: "../render/tb_deleted_pxk.php",
    type: "POST",
    data:{
        input_val: input_val,
        page: page
    },
    success: function(data){
        $(".detail_wh").append(data);
    }
});

$(".icon_sr_wh").click(function(){
    var input_val = $("input[name='input_search'").val();
    var page = $(".detail_wh").attr("data-page");

    if(input_val == ""){
        window.location.href = "/du-lieu-da-xoa-gan-day-phieu-xuat-kho.html";
    }else{
        window.location.href = "/du-lieu-da-xoa-gan-day-phieu-xuat-kho.html?input=" + input_val + "&page=" + page;
    }
        
    if(!input_val == "" || input_val == ""){
        $('.btn_del_vv,.btn_del_rm').hide();
    }
});

$(document).keyup(function (e) {
    if ($("input[name='input_search']").is(":focus") && (e.keyCode == 13)) {
        $(".icon_sr_wh").click();
    }
});

$('.popup_del_w .btn_save').on('click',function(){
    var id_vt = $(this).parents('.popup_del_w').attr('data-id');
    var name = $('.popup_del_w strong').text();
    $.ajax({
        url: '../ajax/delete_vinh_vien_pxk.php',
        type: "POST",
        data:{
            id_ct: id_ct,
            id_vt: id_vt,
        },
        success: function(data){
            if(data != ""){
                $('.popup_add_notif_succ').show(0, function(){
                    var text = $('#popup_add_notif_succ .p_add_succ').text('');
                    var text_new = '';
                    text_new += 'Xóa vĩnh viễn phiếu xuất kho';
                    text_new += '<strong>';
                    text_new += '&nbsp' + name;
                    text_new += '</strong>';
                    text_new += '&nbspthành công!';
                    text.append(text_new);
                });
            }else{
                alert('Xóa thất bại!');
            }
        }
    });
});

$('.popup_rm_w .btn_save').on('click',function(){
    var id_vt = $(this).parents('.popup_rm_w').attr('data-id');
    var name = $('.popup_rm_w strong').text();
    $.ajax({
        url: '../ajax/khoi_phuc_pxk.php',
        type: "POST",
        data:{
            id_ct: id_ct,
            id_vt: id_vt,
        },
        success: function(data){
            if(data != ""){
                $('.popup_add_notif_succ').show(0, function(){
                    var text = $('#popup_add_notif_succ .p_add_succ').text('');
                    var text_new = '';
                    text_new += 'Khôi phục phiếu xuất kho';
                    text_new += '<strong>';
                    text_new += '&nbsp' + name;
                    text_new += '</strong>';
                    text_new += '&nbspthành công!';
                    text.append(text_new);
                });
            }else{
                alert('Khôi phục thất bại!');
            }
        }
    });
});

$('.popup_dels_w .btn_save').on('click',function(){
    var list_id_vattu = [];
    var input_checkbox = $('input[type=checkbox]');
    input_checkbox.each(function () {
        if ($(this).is(":checked")) {
            list_id_vattu.push($(this).parent().parent().attr('data-id'));
        }
    })
    var count_del = $('.popup_dels_w .del_w strong').text();
    $.ajax({
        url: '../ajax/delete_vinh_vien_pxk.php',
        type: "POST",
        data:{
            id_ct: id_ct,
            list_id_vattu: list_id_vattu
        },
        success: function(data){
            if(data != ""){
                // alert(data);
                $('.popup_add_notif_succ').show();
                var text = $('#popup_add_notif_succ .p_add_succ').text('');
                var text_new = '';
                text_new += 'Xóa vĩnh viễn';
                text_new += '<strong>';
                text_new += '&nbsp' + count_del;
                text_new += '</strong>';
                text_new += '&nbspphiếu xuất kho';
                text_new += '&nbspthành công!';
                text.append(text_new);
            }else{
                alert('Xóa thất bại!');
            }
        }
    });
});

$('.popup_rms_w .btn_save').on('click',function(){
    var list_id_vattu = [];
    var input_checkbox = $('input[type=checkbox]');
    input_checkbox.each(function () {
        if ($(this).is(":checked")) {
            list_id_vattu.push($(this).parent().parent().attr('data-id'));
        }
    })
    var count_del = $('.popup_rms_w .del_w strong').text();
    $.ajax({
        url: '../ajax/khoi_phuc_pxk.php',
        type: "POST",
        data:{
            id_ct: id_ct,
            list_id_vattu: list_id_vattu
        },
        success: function(data){
            if(data != ""){
                $('.popup_add_notif_succ').show();
                var text = $('#popup_add_notif_succ .p_add_succ').text('');
                var text_new = '';
                text_new += 'Khôi phục';
                text_new += '<strong>';
                text_new += '&nbsp' + count_del;
                text_new += '</strong>';
                text_new += '&nbspphiếu xuất kho';
                text_new += '&nbspthành công!';
                text.append(text_new);
            }else{
                alert('Khôi phục thất bại!');
            }
        }
    });
});

$('.btn_close').click(function () {
    $('.popup_dels_w,.popup_rms_w,.popup_del_w,.popup_rm_w').hide();
    window.location.reload();
});

function xoa(_this){
    var id_vt = $(_this).parent().parent().attr('data-id');
    $('.popup_del_w').attr('data-id',id_vt);
    notifiDataDeleted('phiếu xuất kho', 'Xóa vĩnh viễn vật tư thiết bị', _this);
}

function khoi_phuc(_this){
    var id_vt = $(_this).parent().parent().attr('data-id');
    $('.popup_rm_w').attr('data-id',id_vt);
    notifiDataRemove('phiếu xuất kho', 'Khôi phục vật tư thiết bị', _this);
}