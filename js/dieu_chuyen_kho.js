
var page = $(".detail_wh").attr("data-page");
var kn = $(".detail_wh").attr("data-kn");
var kx = $(".detail_wh").attr("data-kx");
var th = $(".detail_wh").attr("data-th");

var ngay_tao_start = $(".detail_wh").attr("data-ngts");
var ngay_tao_end = $(".detail_wh").attr("data-ngte");

var ngay_th_start = $(".detail_wh").attr("data-ngths");
var ngay_th_end = $(".detail_wh").attr("data-ngthe");

var ngay_ht_start = $(".detail_wh").attr("data-nghts");
var ngay_ht_end = $(".detail_wh").attr("data-nghte");

var input_val = $(".detail_wh").attr("data-ip");
var curr = $(".detail_wh").attr("data-dis");


$.ajax({
    url: "../render/tb_dieu_chuyen_kho.php",
    type: "POST",
    data:{
        kn: kn,
        kx: kx,
        th: th,

        ngay_tao_start: ngay_tao_start,
        ngay_tao_end: ngay_tao_end,

        ngay_th_start: ngay_th_start,
        ngay_th_end: ngay_th_end,

        ngay_ht_start: ngay_ht_start,
        ngay_ht_end: ngay_ht_end,

        input_val: input_val,

        page: page,
        curr: curr
    },
    success: function(data){
        $(".detail_wh").append(data);
    }
});

$(".select_all_ex, .select_all_in, .select_st_tf").on("change",function () {
    var all_kho_xuat = $('.select_all_ex').val();
    var all_kho_nhap = $('.select_all_in').val();
    var trang_thai = $('.select_st_tf').val();
    var page = 1;

    var ngay_tao_start = $('#date_cr_start').attr("data");
    var ngay_tao_end = $('#date_cr_end').attr("data");

    var ngay_th_start = $('#date_pf_start').attr("data");
    var ngay_th_end = $('#date_pf_end').attr("data");

    var ngay_ht_start = $('#date_rq_start').attr("data");
    var ngay_ht_end = $('#date_rq_end').attr("data");

    var input_val = $("input[name='input_search']").val();
    var curr = $('.show_tr_tb').val();
    
    if(all_kho_nhap == "" && all_kho_xuat == "" && trang_thai == "" && ngay_tao_start == "" && ngay_tao_end == "" && ngay_th_start == "" && ngay_th_end == "" && ngay_ht_start == "" && ngay_ht_end == "" && input_val == "" && curr == 10){
        window.location.href = "/dieu-chuyen-kho.html?dis=" + curr + '&page=' + page;
    }else{
        window.location.href = "/dieu-chuyen-kho.html?kx=" + all_kho_xuat + "&kn=" + all_kho_nhap + "&th=" + trang_thai + "&ngts=" + ngay_tao_start + "&ngte=" + ngay_tao_end + "&ngths=" + ngay_th_start + "&ngthe=" + ngay_th_end + "&nghts=" + ngay_ht_start + "&nghte=" + ngay_ht_end + "&input=" + input_val + "&dis=" + curr + "&page=" + page;
    }

    $("input[name='input_search'").val('');
});

$('.warehouse_transfer_main .date_cr img').click(function(){
    ele = $(this);
    $('#popup_select_date').show();
    $('.popup_select_date .btn_save').on("click", function () {
        selectDate(this, ele);

        var page = 1;
        var all_kho_xuat = $('.select_all_ex').val();
        var all_kho_nhap = $('.select_all_in').val();
        var trang_thai = $('.select_st_tf').val();

        var ngay_tao_start = $(this).parents('.content_popup').find('.d_flex.flex_column:nth-child(1) input').val();
        var ngay_tao_end = $(this).parents('.content_popup').find('.d_flex.flex_column:nth-child(2) input').val();

        var ngay_th_start = $('#date_pf_start').attr("data");
        var ngay_th_end = $('#date_pf_end').attr("data");

        var ngay_ht_start = $('#date_rq_start').attr("data");
        var ngay_ht_end = $('#date_rq_end').attr("data");

        var input_val = $("input[name='input_search']").val();
        var curr = $('.show_tr_tb').val();

        if(all_kho_nhap == "" && all_kho_xuat == "" && trang_thai == "" && ngay_tao_start == "" && ngay_tao_end == "" && ngay_th_start == "" && ngay_th_end == "" && ngay_ht_start == "" && ngay_ht_end == "" && input_val == "" && curr == 10){
            window.location.href = "/dieu-chuyen-kho.html?dis=" + curr + '&page=' + page;
        }else{
            window.location.href = "/dieu-chuyen-kho.html?kx=" + all_kho_xuat + "&kn=" + all_kho_nhap + "&th=" + trang_thai + "&ngts=" + ngay_tao_start + "&ngte=" + ngay_tao_end + "&ngths=" + ngay_th_start + "&ngthe=" + ngay_th_end + "&nghts=" + ngay_ht_start + "&nghte=" + ngay_ht_end + "&input=" + input_val + "&dis=" + curr + "&page=" + page;
        }

    })
});

$('.warehouse_transfer_main .date_pf img').click(function(){
    ele = $(this);
    $('#popup_select_date').show();
    $('.popup_select_date .btn_save').on("click", function () {
        selectDate(this, ele);

        var page = 1;
        var all_kho_xuat = $('.select_all_ex').val();
        var all_kho_nhap = $('.select_all_in').val();
        var trang_thai = $('.select_st_tf').val();
        var ngay_tao_start = $('#date_cr_start').attr("data");
        var ngay_tao_end = $('#date_cr_end').attr("data");

        var ngay_th_start = $(this).parents('.content_popup').find('.d_flex.flex_column:nth-child(1) input').val();
        var ngay_th_end = $(this).parents('.content_popup').find('.d_flex.flex_column:nth-child(2) input').val();

        var ngay_ht_start = $('#date_rq_start').attr("data");
        var ngay_ht_end = $('#date_rq_end').attr("data");

        var input_val = $("input[name='input_search']").val();
        var curr = $('.show_tr_tb').val();

        if(all_kho_nhap == "" && all_kho_xuat == "" && trang_thai == "" && ngay_tao_start == "" && ngay_tao_end == "" && ngay_th_start == "" && ngay_th_end == "" && ngay_ht_start == "" && ngay_ht_end == "" && input_val == "" && curr == 10){
            window.location.href = "/dieu-chuyen-kho.html?dis=" + curr + '&page=' + page;
        }else{
            window.location.href = "/dieu-chuyen-kho.html?kx=" + all_kho_xuat + "&kn=" + all_kho_nhap + "&th=" + trang_thai + "&ngts=" + ngay_tao_start + "&ngte=" + ngay_tao_end + "&ngths=" + ngay_th_start + "&ngthe=" + ngay_th_end + "&nghts=" + ngay_ht_start + "&nghte=" + ngay_ht_end + "&input=" + input_val + "&dis=" + curr + "&page=" + page;
        }
    })
});

$('.warehouse_transfer_main .date_rq img').click(function(){
    ele = $(this);
    $('#popup_select_date').show();
    $('.popup_select_date .btn_save').on("click", function () {
        selectDate(this, ele);

        var page = 1;
        var all_kho_xuat = $('.select_all_ex').val();
        var all_kho_nhap = $('.select_all_in').val();
        var trang_thai = $('.select_st_tf').val();

        var ngay_tao_start = $('#date_cr_start').attr("data");
        var ngay_tao_end = $('#date_cr_end').attr("data");

        var ngay_th_start = $('#date_pf_start').attr("data");
        var ngay_th_end = $('#date_pf_end').attr("data");

        var ngay_ht_start = $(this).parents('.content_popup').find('.d_flex.flex_column:nth-child(1) input').val();
        var ngay_ht_end = $(this).parents('.content_popup').find('.d_flex.flex_column:nth-child(2) input').val();

        var input_val = $("input[name='input_search']").val();
        var curr = $('.show_tr_tb').val();

        if(all_kho_nhap == "" && all_kho_xuat == "" && trang_thai == "" && ngay_tao_start == "" && ngay_tao_end == "" && ngay_th_start == "" && ngay_th_end == "" && ngay_ht_start == "" && ngay_ht_end == "" && input_val == "" && curr == 10){
            window.location.href = "/dieu-chuyen-kho.html?dis=" + curr + '&page=' + page;
        }else{
            window.location.href = "/dieu-chuyen-kho.html?kx=" + all_kho_xuat + "&kn=" + all_kho_nhap + "&th=" + trang_thai + "&ngts=" + ngay_tao_start + "&ngte=" + ngay_tao_end + "&ngths=" + ngay_th_start + "&ngthe=" + ngay_th_end + "&nghts=" + ngay_ht_start + "&nghte=" + ngay_ht_end + "&input=" + input_val + "&dis=" + curr + "&page=" + page;
        }
    })
});

$(".icon_sr_wh").click(function(){
    var all_kho_xuat = $('.select_all_ex').val();
    var all_kho_nhap = $('.select_all_in').val();
    var trang_thai = $('.select_st_tf').val();
    var page = 1;

    var ngay_tao_start = $('#date_cr_start').attr("data");
    var ngay_tao_end = $('#date_cr_end').attr("data");

    var ngay_th_start = $('#date_pf_start').attr("data");
    var ngay_th_end = $('#date_pf_end').attr("data");

    var ngay_ht_start = $('#date_rq_start').attr("data");
    var ngay_ht_end = $('#date_rq_end').attr("data");

    var input_val = $("input[name='input_search']").val();
    var curr = $('.show_tr_tb').val();

    if(all_kho_nhap == "" && all_kho_xuat == "" && trang_thai == "" && ngay_tao_start == "" && ngay_tao_end == "" && ngay_th_start == "" && ngay_th_end == "" && ngay_ht_start == "" && ngay_ht_end == "" && input_val == "" && curr == 10){
        window.location.href = "/dieu-chuyen-kho.html?dis=" + curr + '&page=' + page;
    }else{
        window.location.href = "/dieu-chuyen-kho.html?kx=" + all_kho_xuat + "&kn=" + all_kho_nhap + "&th=" + trang_thai + "&ngts=" + ngay_tao_start + "&ngte=" + ngay_tao_end + "&ngths=" + ngay_th_start + "&ngthe=" + ngay_th_end + "&nghts=" + ngay_ht_start + "&nghte=" + ngay_ht_end + "&input=" + input_val + "&dis=" + curr + "&page=" + page;
    }
});

$(document).keyup(function (e) {
    if ($("input[name='input_search']").is(":focus") && (e.keyCode == 13)) {
        $(".icon_sr_wh").click();
    }
});

$(".btn_ex").click(function() {
    window.location.href = '../Excel/dieu_chuyen_kho.php';
});

function display(select){
    var curr = $(select).val();
    var page = 1;
    var all_kho_xuat = $('.select_all_ex').val();
    var all_kho_nhap = $('.select_all_in').val();
    var trang_thai = $('.select_st_tf').val();

    var ngay_tao_start = $('#date_cr_start').attr("data");
    var ngay_tao_end = $('#date_cr_end').attr("data");

    var ngay_th_start = $('#date_pf_start').attr("data");
    var ngay_th_end = $('#date_pf_end').attr("data");

    var ngay_ht_start = $('#date_rq_start').attr("data");
    var ngay_ht_end = $('#date_rq_end').attr("data");

    var input_val = $("input[name='input_search']").val();

    if(all_kho_nhap == "" && all_kho_xuat == "" && trang_thai == "" && ngay_tao_start == "" && ngay_tao_end == "" && ngay_th_start == "" && ngay_th_end == "" && ngay_ht_start == "" && ngay_ht_end == "" && input_val == "" && curr == 10){
        window.location.href = "/dieu-chuyen-kho.html?dis=" + curr + '&page=' + page;
    }else{
        window.location.href = "/dieu-chuyen-kho.html?kx=" + all_kho_xuat + "&kn=" + all_kho_nhap + "&th=" + trang_thai + "&ngts=" + ngay_tao_start + "&ngte=" + ngay_tao_end + "&ngths=" + ngay_th_start + "&ngthe=" + ngay_th_end + "&nghts=" + ngay_ht_start + "&nghte=" + ngay_ht_end + "&input=" + input_val + "&dis=" + curr + "&page=" + page;
    }
}





