var page = $(".detail_unit").attr("data-page");
var input_val = $(".detail_unit").attr("data-ip");
var curr = $(".detail_unit").attr("data-dis");

$.ajax({
    url: "../render/tb_don_vi_tinh.php",
    type: "POST",
    data:{
        input_val: input_val,
        page: page,
        curr: curr
    },
    success: function(data){
        $(".detail_unit").append(data);
    }
});

$(".icon_sr_wh").click(function(){
    var input_val = $("input[name='input_search'").val();
    var page = 1;
    var curr = $('.show_tr_tb').val();
    
    if(input_val == "" && curr == 10){
        window.location.href = "/don-vi-tinh.html?dis=" + curr + '&page=' + page;
    }else{
        window.location.href = "/don-vi-tinh.html?input=" + input_val + "&dis=" + curr + '&page=' + page;
    }
});

$(document).keyup(function (e) {
    if ($("input[name='input_search']").is(":focus") && (e.keyCode == 13)) {
        $(".icon_sr_wh").click();
    }
});

$(".btn_ex").click(function() {
    window.location.href = '../Excel/don_vi_tinh.php';
});

function display(select){
    var curr = $(select).val();
    var page = 1;
    var input_val = $("input[name='input_search']").val();
    if(input_val == "" && curr == 10){
        window.location.href = "/don-vi-tinh.html?dis=" + curr + '&page=' + page;
    }else{
        window.location.href = "/don-vi-tinh.html?input=" + input_val + "&dis=" + curr + '&page=' + page;
    }
    $("input[name='input_search'").val('');
}