var list_item_search_no_sort_price = $('.list_item_search_no_sort_price');
var no_sort_price = $('.no_sort_price');
var list_item_header = $('.list_item_header');
var img_header = $('.img_header');
var header_popup_item = $('.header_popup_item');
var add_new_manufacturer = $('.add_new_manufacturer');
var popup_cancel_equipment_materials = $('.popup_cancel_equipment_materials');
var add_new_materials_equipment = $('.add_new_materials_equipment');
var delete_materials_equipment = $('.delete_materials_equipment');
var event_header_overview_nhac_nho = $('.event_header_overview_nhac_nho');
var event_header_overview_thong_bao = $('.event_header_overview_thong_bao');
var popup_thong_bao = $('.popup_thong_bao');
var popup_nhac_nho = $('.popup_nhac_nho');
var edit_manufacturer = $('.edit_manufacturer');
var cancel_create_equipment_materials = $('.cancel_create_equipment_materials');
var delete_link_equipment = $('.delete_link_equipment');
var popup_cancel_export_note = $('.popup_cancel_export_note');
var check_export_note = $('.check_export_note');
var refuse_export_note = $('.refuse_export_note');
var backgroud_popup_item_w200 = $('.backgroud_popup_item_w200');
var info_header = $('.info_header');
var popup_log_out = $('.popup_log_out');
var popup_cancel_manufacturer = $('.popup_cancel_manufacturer');
var delete_manufacturer = $('.delete_manufacturer');

$(window).click(function (e) {
    if (!no_sort_price.is(e.target) && no_sort_price.has(e.target).length == 0 && !list_item_search_no_sort_price.is(e.target) && list_item_search_no_sort_price.has(e.target).length == 0) {
        list_item_search_no_sort_price.hide();
    }
    if (!list_item_header.is(e.target) && list_item_header.has(e.target).length == 0 && !img_header.is(e.target) && img_header.has(e.target).length == 0 && !backgroud_popup_item_w200.is(e.target) && backgroud_popup_item_w200.has(e.target).length == 0) {
        backgroud_popup_item_w200.hide();
    }
    if (!event_header_overview_thong_bao.is(e.target) && event_header_overview_thong_bao.has(e.target).length == 0 && !popup_thong_bao.is(e.target) && popup_thong_bao.has(e.target).length == 0) {
        popup_thong_bao.hide();
    }
    if (!event_header_overview_nhac_nho.is(e.target) && event_header_overview_nhac_nho.has(e.target).length == 0 && !popup_nhac_nho.is(e.target) && popup_nhac_nho.has(e.target).length == 0) {
        popup_nhac_nho.hide();
    }
    if ($(e.target).is('.popup_cancel_equipment_materials')) {
        popup_cancel_equipment_materials.hide();
    }
    if ($(e.target).is('.add_new_materials_equipment')) {
        add_new_materials_equipment.hide();
    }
    if ($(e.target).is('.add_new_manufacturer')) {
        add_new_manufacturer.hide();
    }
    if ($(e.target).is('.edit_manufacturer')) {
        edit_manufacturer.hide();
    }
});

$('.btn_close2').click(function () {
    popup_cancel_equipment_materials.hide()
    add_new_materials_equipment.hide()
    delete_materials_equipment.hide()
    popup_thong_bao.hide()
    popup_nhac_nho.hide()
    add_new_manufacturer.hide()
    edit_manufacturer.hide()
    cancel_create_equipment_materials.hide()
    delete_link_equipment.hide()
    popup_cancel_export_note.hide()
    check_export_note.hide()
    refuse_export_note.hide()
    delete_manufacturer.hide();
    // delete_export_note.hide()
    popup_log_out.hide()
    popup_cancel_manufacturer.hide()
})


function toggle(data) {
    $('.' + data).toggle();
}

function openAndHide(data, value) {
    console.log(data)
    if (data != '') {
        $('.' + data).hide();
    }
    if (value != '') {
        $('.' + value).show();
    }
}

var currentLocation = window.location.pathname;
var li_sidebar = $('.li_sidebar>a');
var li_sidebar_con = $('.li_sidebar_con>a');
li_sidebar.each(function () {
    var href = $(this).attr('href');
    if (href == currentLocation) {
        $(this).addClass('active');
    }
})

li_sidebar.each(function () {
    var data = $(this).hasClass('active')
    if (data === true) {
        $(this).addClass('active');
    }
})

li_sidebar_con.each(function () {
    var href = $(this).attr('href');
    if (href == currentLocation) {
        $(this).addClass('active');
        $(this).parent().parent().addClass('show');
    }
    if ($(this).hasClass('active')) {
        $(this).parent().parent().parent().find('.item_sidebar_cha').addClass('active');
    }
})

var item_sidebar_cha = $('.item_sidebar_cha');
item_sidebar_cha.click(function () {
    $('.ul_sidebar_con').each(function () {
        if ($(this).hasClass('da_show')) {
            $(this).hide();
            $(this).removeClass("da_show");
        }
    })
    $(this).parents('.li_sidebar').find('.ul_sidebar_con').show();
    $(this).parents('.li_sidebar').find('.ul_sidebar_con').addClass("da_show");
})

$('.item_header').mouseover(function () {
    $(this).find('.title_item_header').css('color', '#fff')
    $(this).css('background', '#4C5BD4')
    $(this).find('.img_b').hide()
    $(this).find('.img_w').show()
})
$('.item_header').mouseout(function () {
    $(this).find('.title_item_header').css('color', '#474747')
    $(this).css('background', '#fff')
    $(this).find('.img_b').show()
    $(this).find('.img_w').hide()
})

window.addEventListener('load', function () {
    document.querySelector('input[type="file"]').addEventListener('change', function () {
        if (this.files && this.files[0]) {
            var img = document.querySelector('img.ready_upload_logo');
            img.onload = () => {
                URL.revokeObjectURL(img.src); // no longer needed, free memory
            }

            img.src = URL.createObjectURL(this.files[0]); // set src to blob url
        }
        $('.upload_file_img').hide();
        $('.upload_logo_vehicle_done').show();
        // reloadImg();
    });
});

$('.del_logo').click(function () {
    $('.ready_upload_logo').attr('src', '');
    $('.upload_logo_vehicle_done').hide();
    $('.upload_file_img').css('display','flex');
    // reloadImg();
})

$('.btn_close_pict_send').click(function () {
    $('.picture_selected_ready').attr('src', '');
    $('.picture_select_send').hide();
    reloadImg();
})

$('.upload_files').change(function () {
    $('.picture_select_send').show();
    reloadImg();
});

if($(window).width() <= 767.98){
    $('.select_user').select2({
        width: '100%'
    });
}else{
    $('.select_user').select2({
        width: '420px'
    });
}

$('.select_all_kho').select2({
    // placeholder: "Tất cả các kho",
    width: '100%'
});

$('.all_equipment_supplies').select2({
    // placeholder: "Tất cả nhóm vật tư thiết bị",
    // width: '100%'
});

$('.all_manufacturer').select2({
    // placeholder: "Tất cả hãng sản xuất",
    // width: '100%'
});
$('.all_provenance').select2({
    // placeholder: "Tất cả xuất xứ",
    // width: '100%'
});
$('.group_materials_equipment').select2({
    // placeholder: "Chọn nhóm vật tư thiết bị",
    width: '100%'
});
$('.select_unit').select2({
    // placeholder: "Chọn đơn vị tính",
    width: '100%'
});
$('.select_manufacturer').select2({
    // placeholder: "Chọn hãng sản xuất",
    width: '100%'
});
$('.select_origin').select2({
    // placeholder: "Chọn xuất xứ",
    width: '100%'
});
$('.select_export_form').select2({
    width: '100%'

});
$('.select_request_form').select2({
    placeholder: "Chọn phiếu yêu cầu",
    width: '100%'
});

$('.select_request_form1').select2({
    placeholder: "Chọn phiếu yêu cầu",
    width: '100%'
});
$('.select_export_kho_5').select2({
    placeholder: "Chọn kho xuất",
    width: '100%'
});
$('.select_request').select2({
    placeholder: "Chọn phiếu yêu cầu",
    width: '100%'
});
$('.select_deliver').select2({
    width: '100%'
});
$('.select_deliver1').select2({
    width: '100%'
});
$('.select_export_kho').select2({
    placeholder: "Chọn kho xuất",
    width: '100%'
});
$('.select_export_kho_1').select2({
    placeholder: "Chọn kho xuất",
    width: '100%'
});
$('.select_import_kho').select2({
    placeholder: "Chọn kho nhập",
    width: '100%'
});
$('.select_transfer_slip').select2({
    placeholder: "Chọn phiếu điều chuyển",
    width: "100 %"
});
$('.select_materials_equipment').select2({
    // placeholder: "Chọn phiếu điều chuyển"
    width: '100%'
});
$('.select_order').select2({
    placeholder: "Chọn đơn hàng",
    width: '100%'
});
$('.select_human_export').select2({
    placeholder: "Chọn người xuất kho",
    width: '100%'
});
$('.no_sort_price').select2({
    // placeholder: "Chọn người xuất kho"
    // width: '100%'
});
$('.select_create.select_status').select2({
    // placeholder: "Chọn người xuất kho"
    width: '100%',
    minimumResultsForSearch: -1
});

$('.select_receiver1').select2({
    // placeholder: "Hiển thị tên người nhận từ đơn hàng",
    width: '100%'
});
$('.select_receiver').select2({
    placeholder: "Hiển thị tên người nhận",
    width: '100%'
});
$('.select_all_form_kho').select2({
    width: '100%'
});
$('.select_all_status_kho').select2({
    width: '100%'
});

$(".select_export_form").on("select2:select", function (e) {
    var select_val = $(e.currentTarget).val();
    if (select_val === 'XK1') {
        $('.selected_construction_export, .table_construction_export').show();
        $('.selected_export_transfer, .selected_export_on_demand, .selected_export_by_order, .selected_other_export, .table_export_transfer, .table_export_by_order, .date_accses').hide();
        $('.select_status').parent().parent().addClass('w_100');
        $('.select_status').parents().removeClass('w_50');
        $('.select_status').parents().removeClass('mr_10');
        $('.select_status').select2("val", "1");
    }
    if (select_val === 'XK2') {
        $('.selected_export_transfer, .table_export_transfer').show();
        $('.selected_construction_export, .selected_export_on_demand, .selected_export_by_order, .selected_other_export, .table_export_by_order, .table_construction_export, .date_accses').hide();
        $('.select_status').parent().parent().addClass('w_100');
        $('.select_status').parents().removeClass('w_50');
        $('.select_status').parents().removeClass('mr_10');
        $('.select_status').select2("val", "1");
    }
    if (select_val === 'XK3') {
        $('.selected_export_on_demand, .table_construction_export').show();
        $('.selected_export_transfer, .selected_construction_export, .selected_export_by_order, .selected_other_export, .table_export_transfer , .table_export_by_order, .date_accses').hide();
        $('.select_status').parent().parent().addClass('w_100');
        $('.select_status').parents().removeClass('w_50');
        $('.select_status').parents().removeClass('mr_10');
        $('.select_status').select2("val", "1");
    }
    if (select_val === 'XK4') {
        $('.selected_export_by_order, .table_export_transfer').show();
        $('.selected_export_transfer, .selected_export_on_demand, .selected_construction_export, .selected_other_export, .table_export_by_order, .table_construction_export, .date_accses').hide();
        $('.select_status').parent().parent().addClass('w_100');
        $('.select_status').parents().removeClass('w_50');
        $('.select_status').parents().removeClass('mr_10');
        $('.select_status').select2("val", "1");
    }
    if (select_val === 'XK5') {
        $('.selected_other_export, .table_export_by_order').show();
        $('.selected_export_transfer, .selected_export_on_demand, .selected_export_by_order, .selected_construction_export, .table_export_transfer, .table_construction_export, .date_accses').hide();
        $('.select_status').parent().parent().addClass('w_100');
        $('.select_status').parents().removeClass('w_50');
        $('.select_status').parents().removeClass('mr_10');
        $('.select_status').select2("val", "1");
    }
});

$(".select_status").on("select2:select", function (e) {
    var select_val = $(e.currentTarget).val();
    if (select_val === '1') {
        $('.date_accses').hide();
        $('.select_status').parent().parent().addClass('w_100');
        $('.select_status').parents().removeClass('w_50');
        $('.select_status').parents().removeClass('mr_10');
    }
    if (select_val === '2') {
        $('.date_accses').show();
        $('.select_status').parents().removeClass('w_100');
        $('.select_status').parent().parent().addClass('w_50');
        $('.select_status').parent().parent().addClass('mr_10');
        $('.select_status').parent().addClass('w_100');
    }
});

function success(data) {
    $('.' + data).hide();
    $('.button_browse, .button_refuse, .button_add_new, .refuse, .approved_export, .pending, .reason_for_refusal').hide()
    $('.button_complete, .approved_wait_export, .human_approved, .approval_date').show()
}

function complete(data) {
    $('.' + data).hide();
    $('.button_browse, .button_refuse, .button_add_new, .refuse, .approved_wait_export, .pending, .button_complete, .approval_date, .reason_for_refusal').hide()
    $('.human_approved, .approved_export, .human_complete, .data_complete, .approval_date').show()
}

function refuse(data) {
    $('.' + data).hide();
    $('.button_refuse, .approved_wait_export, .pending, .button_complete, .approval_date').hide()
    $('.human_approved, .approval_date, .refuse, .reason_for_refusal').show()
}


var open_sidebar_w = $('.open_sidebar_w')
var main_sidebar = $('.main_sidebar')
$(window).click(function (e) {
    if (window.screen.width <= 1024 && !open_sidebar_w.is(e.target) && open_sidebar_w.has(e.target).length == 0 && !main_sidebar.is(e.target) && main_sidebar.has(e.target).length == 0) {
        main_sidebar.hide();
    }
    if (window.screen.width <= 1024 && $(e.target).is('.main_sidebar')) {
        main_sidebar.hide();
    }
})

function reload() {
    window.location.reload()
}