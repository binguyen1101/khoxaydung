var popup_add_wh = $('#popup_add_wh')
$('.close_popup').click(function () {
    popup_add_wh.hide();
})

$(document).ready(function () {
    $('.select_construction,.select_staff,.select_all_form_in,.select_import_form,.all_meterial_wh,.inventory_men,.rq,.select_all_in_note_status,.select_iv_tf').select2()

    $('.select_receiver_wh').select2({
        placeholder: "Chọn người nhận"
    });
    $('.select_shipper_wh').select2({
        placeholder: "Chọn người giao"
    });
    $('.select_staff').select2({
        placeholder: "Chọn nhân viên"
    });
    $('.body_import_warehouse_create .select_shipper_dis').select2({
        placeholder: "Chọn người giao hàng"
    })
    $('.choice_order').select2({
        placeholder: "Chọn đơn hàng"
    });
    $('.select_import_form').select2({
        width: "100%"
    })
    $('.list_wh .all_meterial_wh').select2({
        width: '266px'
    })
    $('.no_sort_quantily').select2({
        width: '206px'
    })
    $('.rq_note_5').select2({
        placeholder: "Chọn phiếu yêu cầu",
        width: '100%'
    })
    $('.body_import_warehouse_create .rq').select2({
        placeholder: "Chọn phiếu yêu cầu"
    })
    $('.choice_wh_ivt').select2({
        placeholder: "Chọn kho kiểm kê"
    })
    $('.inventory_men').select2({
        placeholder: "Chọn người thực hiện"
    })
    $('.header_kho .btn_px').click(function () {
        $('.popup_add_wh').fadeIn('fast');
        $('.popup_add_wh').show();

    });
    $('.wh_main .popup_add_wh .tit_head .close_popup').click(function () {
        $('.popup_add_wh').fadeOut('slow');
        $('.popup_add_wh').hide();
    })
});

// nhập kho
function change_ht(_this){
    var choice_val = $(_this).val();

    $('.delete_3').remove();

    if (choice_val === 'NK1') {
        $('.detail_input_wh_1,.table_input_wh_1').show();
        $('.detail_input_wh_2,.detail_input_wh_3,.detail_input_wh_4,.detail_input_wh_5,.table_input_wh_2,.table_input_wh_3,.table_input_wh_4,.table_input_wh_5').hide();
    }
    if (choice_val === 'NK2') {
        $('.detail_input_wh_2,.table_input_wh_2').show();
        $('.detail_input_wh_1,.detail_input_wh_3,.detail_input_wh_4,.detail_input_wh_5,.table_input_wh_1,.table_input_wh_3,.table_input_wh_4,.table_input_wh_5').hide();
    }
    if (choice_val === 'NK3') {
        $('.detail_input_wh_3,.table_input_wh_3').show();
        $('.detail_input_wh_1,.detail_input_wh_2,.detail_input_wh_4,.detail_input_wh_5,.table_input_wh_1,.table_input_wh_2,.table_input_wh_4,.table_input_wh_5').hide();
    }
    if (choice_val === 'NK4') {
        $('.detail_input_wh_5,.table_input_wh_5').show();
        $('.detail_input_wh_1,.detail_input_wh_2,.detail_input_wh_4,.detail_input_wh_3,.table_input_wh_1,.table_input_wh_2,.table_input_wh_4,.table_input_wh_3').hide();
    }
    if (choice_val === 'NK5') {
        $('.detail_input_wh_4,.table_input_wh_4').show();
        $('.detail_input_wh_1,.detail_input_wh_2,.detail_input_wh_3,.detail_input_wh_5,.table_input_wh_1,.table_input_wh_2,.table_input_wh_3,.table_input_wh_5').hide();
    }

    $('.date_accses').hide();
    $('.select_status').parent().parent().addClass('w_100');
    $('.select_status').parents().removeClass('w_50');
    $('.select_status').parents().removeClass('mr_10');
    $('.select_status').select2("val", "1");
}


// phiếu kiểm kê

$('.choice_tf_note').select2({
    placeholder: "Chọn phiếu điều chuyển"
})

function deleteRow(_this) {
    $(_this).parent().remove();
}

function change_trangthai(_this){
    var select_val = $(_this).val();
    if (select_val === '1') {
        $('.date_accses').hide();
        $(_this).parent().parent().addClass('w_100');
        $(_this).parents().removeClass('w_50');
        $(_this).parents().removeClass('mr_10');
    }
    if (select_val === '4') {
        $('.date_accses').show();
        $(_this).parents().removeClass('w_100');
        $(_this).parent().parent().addClass('w_50');
        $(_this).parent().parent().addClass('mr_10');
        $(_this).parent().addClass('w_100');
    }
}