$(document).ready(function () {
    // Select2
    $('.box_right_ct .icon_header').click(function () {
        toggle('main_sidebar');
    })

    $('.all_type,.select_all_in_wh,.select_sort_invt,.select_sort_invt0,.select_sort_invt1,.select_sort_invt2,.select_all_ex_wh,.select_all_ex,.select_all_in,.select_st_tf,.select_all_wh').select2();
    $('.select_stt').select2({
        minimumResultsForSearch: -1
    });
    $('.select_add_unit').select2({
        placeholder: "Chọn đơn vị tính"
    });
    $('.select_brand').select2({
        placeholder: "Chọn hãng sản xuất"
    });
    $('.select_wh_ex').select2({
        placeholder: "Chọn kho"
    });
    $('.select_wh_in').select2({
        placeholder: "Chọn kho"
    });
    $('.select_shipper').select2({
        placeholder: "Chọn người giao hàng"
    });
    $('.select_shipper_dis').select2({
        placeholder: "Nhập tên người giao hàng"
    });
    $('.select_receiver').select2({
        placeholder: "Chọn người nhận"
        // width: '100 %'
    });
    $(".select_tb").select2({
        placeholder: "Chọn vật tư",
        templateSelection: formatState
    });

    $('.btn_close').click(function () {
        $(this).parent().parent().parent().hide();
    });
    $('.close_popup').click(function () {
        $(this).parent().parent().parent().parent().parent().hide();
    });
    $('.popup_show .btn_cancel').click(function () {
        $(this).parents('.popup_show').hide();
    });

    $(".next_q").click(function () {
        scrollLeft = $(this).parent().find('.table_vt_scr').scrollLeft().toFixed(0);
        var maxScrollLeft = $(this).parent().find('.table_vt_scr').get(0).scrollWidth - $(this).parent().find('.table_vt_scr').get(0).clientWidth;
        if (parseInt(scrollLeft) < maxScrollLeft) {
            $(this).parent().find('.table_vt_scr').animate({ scrollLeft: '+=300' }, 400);
            $(this).parent().find(".pre_q").css('display','flex');
        } else {
            $(this).parent().find('.table_vt_scr').animate({ scrollLeft: '+=0' }, 0);
        }
    });

    $(".pre_q").hide();
    $(".pre_q").click(function () {
        scrollLeft = $(this).parent().find('.table_vt_scr').scrollLeft().toFixed(0);
        if (parseInt(scrollLeft) <= 0) {
            $(this).parent().find('.table_vt_scr').animate({ scrollLeft: '-=0' }, 0);
        } else {
            $(this).parent().find('.table_vt_scr').animate({ scrollLeft: '-=300' }, 400);
            $(this).parent().find(".next_q").css('display','flex');
        }
    });

    $('.table_vt_scr').scroll(function () {
        var tableScrollLeft = $(this).parent().find('.table_vt_scr').scrollLeft().toFixed(0);
        var maxtableScrollLeft = $(this).parent().find('.table_vt_scr').get(0).scrollWidth - $(this).parent().find('.table_vt_scr').get(0).clientWidth;
        var tableScrollLeftNow = parseInt(tableScrollLeft);
        if (tableScrollLeftNow != maxtableScrollLeft) {
            $(this).parent().find('.pre_q,.next_q').css('display', 'flex');
        }
        if (tableScrollLeftNow === maxtableScrollLeft || tableScrollLeftNow === maxtableScrollLeft - 1 || tableScrollLeftNow === maxtableScrollLeft + 1) {
            $(this).parent().find('.next_q').hide();
        }
        if (tableScrollLeftNow === 0) {
            $(this).parent().find('.pre_q').hide();
        }
        if (maxtableScrollLeft - parseInt(tableScrollLeft) <= 300) {
            $(this).find('.next_q').hide();
        }
        if (parseInt(tableScrollLeft) > 0 && parseInt(tableScrollLeft) <= 300) {
            $(this).find('.pre_q').hide();
        }
    });

    // TRANG CHỦ
    // TRANG HƯỚNG DẪN

    // NGHIỆP VỤ KHO CHỜ XỬ LÝ
    $('.wh_pd_detail_rq_add .btn_save').click(function () {
        $('#popup_ex_st_succ').show();
        var text = $('#popup_ex_st_succ .p_add_succ').text('');
        var text_new = '';
        var name = $('.wh_pd_detail_rq_add .l_add_ex div:first-child input').val();
        text_new += 'Thêm mới phiếu xuất kho';
        text_new += '<strong>';
        text_new += '&nbsp' + name;
        text_new += '</strong>';
        text_new += '<br>';
        text_new += 'theo yêu cầu vật tư thành công!';
        text.append(text_new);
    });
    $('.wh_pd_detail_tf_add .btn_save').click(function () {
        $('#popup_ex_st_succ').show();
        var text = $('#popup_ex_st_succ .p_add_succ').text('');
        var text_new = '';
        var name = $('.wh_pd_detail_tf_add .l_add_ex div:first-child input').val();
        text_new += 'Thêm mới phiếu xuất kho';
        text_new += '<strong>';
        text_new += '&nbsp' + name;
        text_new += '</strong>';
        text_new += '<br>';
        text_new += 'theo điều chuyển thành công!';
        text.append(text_new);
    });

    $('.wh_pd_detail_tf_add .btn_cancel').click(function () {
        $('.popup_func_del').show();
    })

    $('.select_status select').change(function () {
        var value = $('.select_status select option:selected').val();
        if ($(window).width() <= 768) {
            if (value === '7') {
                $('.select_status div').css('width', 'calc(100%/2 - 10px)');
                $('.select_status div:last-child').show();
            } else {
                $('.select_status div').css('width', '100%');
                $('.select_status div:last-child').hide();
            }
        } else if (value === '7') {
            $('.select_status div').css('width', 'calc(100%/2 - 10px)');
            $('.select_status div:last-child').show();
        } else {
            $('.select_status div').css('width', '100%');
            $('.select_status').css('margin-bottom', '15px');
            $('.select_status div:last-child').hide();
        }
    });

    // NHÓM VẬT TƯ THIẾT BỊ
    $('.gr_dv .btn_px').click(function () {
        $('#popup_func_dv_add').show();
    });

    // ĐƠN VỊ TÍNH
    $('.btn_add_unit').click(function () {
        $('#popup_func_dv_add').show();
    });

    $('.unit .a_edit').click(function () {
        $('#popup_func_dv_edit').show();
    });


    // ĐIỀU CHUYỂN KHO
    function click_select_date(){
        ele = $(this);
        $('#popup_select_date').show();
    }

    $('.warehouse_transfer_add .btn_cancel').click(function () {
        $('.popup_func_del').show();
    });

    // DỮ LIỆU ĐÃ XÓA GẦN ĐÂY

    $('.dt_dl_n td input[type=checkbox]').click(function () {
        flag = [];
        var input_arr = $('.dt_dl_n td input[type=checkbox]');
        input_arr.each(function () {
            if (570 <= $(window).width() && $(window).width() <= 768) {
                $('.dt_dl_n .export_wh .hd_ex').css('margin-bottom', '15px');
            } else if (($(window).width() <= 467)) {
                $('.dt_dl_n .export_wh .hd_ex').css('margin-top', '15px');
            }

            if ($(this).is(":checked")) {
                flag.push($(this).val());
            }

            if (flag.length < input_arr.length) {
                $('.dt_dl_n th input[type=checkbox]').prop('checked', false);
            } else {
                $('.dt_dl_n th input[type=checkbox]').prop('checked', true);
            }

            if (flag.length > 0) {
                $('.dt_dl_n .btn_del_vv,.dt_dl_n .btn_del_rm').show();
            } else {
                $('.dt_dl_n .btn_del_vv,.dt_dl_n .btn_del_rm').hide();
                $('.dt_dl_n .export_wh .hd_ex').css('margin-bottom', '0');
                $('.dt_dl_n .export_wh .hd_ex').css('margin-top', '0');
            }
        });
        
        var count = flag.length;
        $('.dt_dl_n_device .btn_del_rm').on("click", function () {
            btn_remove(count, 'vật tư thiết bị');
        })

        $('.dt_dl_n_device .btn_del_vv').on("click", function () {
            btn_del(count, 'vật tư thiết bị');
        })

        $('.dt_dl_gr_device .btn_del_rm').on("click", function () {
            btn_remove(count, 'nhóm vật tư thiết bị');
        })

        $('.dt_dl_gr_device .btn_del_vv').on("click", function () {
            btn_del(count, 'nhóm vật tư thiết bị');
        })

        $('.dt_dl_brand .btn_del_rm').on("click", function () {
            btn_remove(count, 'hãng sản xuất');
        })

        $('.dt_dl_brand .btn_del_vv').on("click", function () {
            btn_del(count, 'hãng sản xuất');
        })

        $('.dt_dl_unit .btn_del_rm').on("click", function () {
            btn_remove(count, 'đơn vị tính');
        })

        $('.dt_dl_unit .btn_del_vv').on("click", function () {
            btn_del(count, 'đơn vị tính');
        })

        $('.dt_dl_in_wh .btn_del_rm').on("click", function () {
            btn_remove(count, 'phiếu nhập kho');
        })

        $('.dt_dl_in_wh .btn_del_vv').on("click", function () {
            btn_del(count, 'phiếu nhập kho');
        })

        $('.dt_dl_ex_wh .btn_del_rm').on("click", function () {
            btn_remove(count, 'phiếu xuất kho');
        })

        $('.dt_dl_ex_wh .btn_del_vv').on("click", function () {
            btn_del(count, 'phiếu xuất kho');
        })

        $('.dt_dl_tf_wh .btn_del_rm').on("click", function () {
            btn_remove(count, 'phiếu điều chuyển kho');
        })

        $('.dt_dl_tf_wh .btn_del_vv').on("click", function () {
            btn_del(count, 'phiếu điều chuyển kho');
        })

        $('.dt_dl_ck_wh .btn_del_rm').on("click", function () {
            btn_remove(count, 'phiếu kiểm kê kho');
        })

        $('.dt_dl_ck_wh .btn_del_vv').on("click", function () {
            btn_del(count, 'phiếu kiểm kê kho');
        })
        
    });
    
    $('.dt_dl_n th input[type=checkbox]').click(function () {
        if ($('.dt_dl_n th input[type=checkbox]').is(':checked')) {
            $('.dt_dl_n .btn_del_vv,.dt_dl_n .btn_del_rm').show();
            $('.dt_dl_n td input[type=checkbox]').not(this).prop('checked', true);
            if (570 <= $(window).width() && $(window).width() <= 768) {
                $('.dt_dl_n .export_wh .hd_ex').css('margin-bottom', '15px');
            } else if (($(window).width() <= 467)) {
                $('.dt_dl_n .export_wh .hd_ex').css('margin-top', '15px');
            }
        } else {
            $('.dt_dl_n .btn_del_vv,.dt_dl_n .btn_del_rm').hide();
            $('.dt_dl_n td input[type=checkbox]').not(this).prop('checked', false);
            $('.dt_dl_n .export_wh .hd_ex').css('margin-bottom', '0');
            $('.dt_dl_n .export_wh .hd_ex').css('margin-top', '0');
        }
        $('.dt_dl_n_device .btn_del_rm').on("click", function () {
            btn_remove('tất cả', 'vật tư thiết bị');
        })

        $('.dt_dl_n_device .btn_del_vv').on("click", function () {
            btn_del('tất cả', 'vật tư thiết bị');
        })

        $('.dt_dl_gr_device .btn_del_rm').on("click", function () {
            btn_remove('tất cả', 'nhóm vật tư thiết bị');
        })

        $('.dt_dl_gr_device .btn_del_vv').on("click", function () {
            btn_del('tất cả', 'nhóm vật tư thiết bị');
        })

        $('.dt_dl_brand .btn_del_rm').on("click", function () {
            btn_remove('tất cả', 'hãng sản xuất');
        })

        $('.dt_dl_brand .btn_del_vv').on("click", function () {
            btn_del('tất cả', 'hãng sản xuất');
        })

        $('.dt_dl_unit .btn_del_rm').on("click", function () {
            btn_remove('tất cả', 'đơn vị tính');
        })

        $('.dt_dl_unit .btn_del_vv').on("click", function () {
            btn_del('tất cả', 'đơn vị tính');
        })

        $('.dt_dl_in_wh .btn_del_rm').on("click", function () {
            btn_remove('tất cả', 'phiếu nhập kho');
        })

        $('.dt_dl_in_wh .btn_del_vv').on("click", function () {
            btn_del('tất cả', 'phiếu nhập kho');
        })

        $('.dt_dl_ex_wh .btn_del_rm').on("click", function () {
            btn_remove('tất cả', 'phiếu xuất kho');
        })

        $('.dt_dl_ex_wh .btn_del_vv').on("click", function () {
            btn_del('tất cả', 'phiếu xuất kho');
        })

        $('.dt_dl_tf_wh .btn_del_rm').on("click", function () {
            btn_remove('tất cả', 'phiếu điều chuyển kho');
        })

        $('.dt_dl_tf_wh .btn_del_vv').on("click", function () {
            btn_del('tất cả', 'phiếu điều chuyển kho');
        })

        $('.dt_dl_ck_wh .btn_del_rm').on("click", function () {
            btn_remove('tất cả', 'phiếu kiểm kê kho');
        })

        $('.dt_dl_ck_wh .btn_del_vv').on("click", function () {
            btn_del('tất cả', 'phiếu kiểm kê kho');
        })
    });

});

// Xóa vĩnh viễn
function notifiDataDeleted(tit_deleted, text, _this) {
    $('.popup_del_w').show();
    $('.popup_del_w .tit_deleted').text(tit_deleted);
    var name = $(_this).parent().parent().find('td:nth-child(2)').text();
    $('.popup_del_w strong').text(name);
};

function notifiDataDeleted_del(text_notif, name) {
    $('.popup_add_notif_succ').show();
    var text = $('#popup_add_notif_succ .p_add_succ').text('');
    var text_new = '';
    text_new += text_notif;
    text_new += '<strong>';
    text_new += '&nbsp' + name;
    text_new += '</strong>';
    text_new += '&nbspthành công!';
    text.append(text_new);
}; 

// 
// Khôi phục 
function notifiDataRemove(tit_deleted, text, _this) {
    $('.popup_rm_w').show();
    $('.popup_rm_w .tit_deleted').text(tit_deleted);
    var name = $(_this).parent().parent().find('td:nth-child(2)').text();
    $('.popup_rm_w strong').text(name);
};

function notifiDataRemove_re(text_notif, name) {
    $('.popup_add_notif_succ').show();
    var text = $('#popup_add_notif_succ .p_add_succ').text('');
    var text_new = '';
    text_new += text_notif;
    text_new += '<strong>';
    text_new += '&nbsp' + name;
    text_new += '</strong>';
    text_new += '&nbspthành công!';
    text.append(text_new);
};

// Xóa vĩnh viễn nhiều
function btn_del(count, name_change) {
    $('.popup_dels_w').show();
    $('.popup_dels_w .tit_deleted').text(name_change);
    $('.popup_dels_w strong').text(count);
};

function btn_del_notif(count, name_change) {
    $('.popup_add_notif_succ').show();
    var text = $('#popup_add_notif_succ .p_add_succ').text('');
    var text_new = '';
    text_new += 'Xóa vĩnh viễn';
    text_new += '<strong>';
    text_new += '&nbsp' + count;
    text_new += '</strong>';
    text_new += '&nbsp' + name_change;
    text_new += '&nbspthành công!';
    text.append(text_new);
};

// Khôi phục nhiều
function btn_remove(count, name_change) {
    $('.popup_rms_w').show();
    $('.popup_rms_w .tit_deleted').text(name_change);
    $('.popup_rms_w strong').text(count);
};

function btn_remove_notif(count, name_change) {
    $('.popup_add_notif_succ').show();
    var text = $('#popup_add_notif_succ .p_add_succ').text('');
    var text_new = '';
    text_new += 'Khôi phục';
    text_new += '<strong>';
    text_new += '&nbsp' + count;
    text_new += '</strong>';
    text_new += '&nbsp' + name_change;
    text_new += '&nbspthành công!';
    text.append(text_new);
};
// 

function selectDate(popup, div_select) {
    if (div_select.click()) {
        var date_start = $('#popup_select_date .content_popup div.d_flex.flex_column:nth-child(1) input[type=text]').val();
        var date_end = $('#popup_select_date .content_popup div.d_flex.flex_column:nth-child(2) input[type=text]').val();
        if (compareTime(date_start, date_end)) {
            alert('Thời gian không hợp lệ!');
        } else {
            $(div_select).parent().find('span.date_start').text(date_start);
            $(div_select).parent().find('span.date_end').text(date_end);
            $('#popup_select_date').hide();
            return;
        }
    } else {
        return;
    }
}

function compareTime(time1, time2) {
    return new Date(time1) > new Date(time2);
}

function formatState(state) {
    if (!state.id) {
        return state.text;
    }
    var $state = $(
        '<span><span class="color_blue"></span></span>'
    );

    $state.find("span").text(state.text);

    return $state;
};

function next_q(_this){
    scrollLeft = $(_this).parent().find('.table_vt_scr').scrollLeft().toFixed(0);
    var maxScrollLeft = $(_this).parent().find('.table_vt_scr').get(0).scrollWidth - $(_this).parent().find('.table_vt_scr').get(0).clientWidth;
    if (parseInt(scrollLeft) < maxScrollLeft) {
        $(_this).parent().find('.table_vt_scr').animate({ scrollLeft: '+=300' }, 400);
        $(_this).parent().find(".pre_q").css('display','flex');
    } else {
        $(_this).parent().find('.table_vt_scr').animate({ scrollLeft: '+=0' }, 0);
    }
}

function pre_q(_this){
    scrollLeft = $(_this).parent().find('.table_vt_scr').scrollLeft().toFixed(0);
    if (parseInt(scrollLeft) <= 0) {
        $(_this).parent().find('.table_vt_scr').animate({ scrollLeft: '-=0' }, 0);
    } else {
        $(_this).parent().find('.table_vt_scr').animate({ scrollLeft: '-=300' }, 400);
        $(_this).parent().find(".next_q").css('display','flex');
    }
}

function table_scroll(_this){
    var tableScrollLeft = $(_this).parent().find('.table_vt_scr').scrollLeft().toFixed(0);
    var maxtableScrollLeft = $(_this).parent().find('.table_vt_scr').get(0).scrollWidth - $(_this).parent().find('.table_vt_scr').get(0).clientWidth;
    var tableScrollLeftNow = parseInt(tableScrollLeft);
    if (tableScrollLeftNow != maxtableScrollLeft) {
        $(_this).parent().find('.pre_q,.next_q').css('display', 'flex');
    }
    if (tableScrollLeftNow === maxtableScrollLeft || tableScrollLeftNow === maxtableScrollLeft - 1 || tableScrollLeftNow === maxtableScrollLeft + 1) {
        $(_this).parent().find('.next_q').hide();
    }
    if (tableScrollLeftNow === 0) {
        $(_this).parent().find('.pre_q').hide();
    }
    if (maxtableScrollLeft - parseInt(tableScrollLeft) <= 300) {
        $(_this).find('.next_q').hide();
    }
    if (parseInt(tableScrollLeft) > 0 && parseInt(tableScrollLeft) <= 300) {
        $(_this).find('.pre_q').hide();
    }
}

function remove_td(_this){
    $(_this).parent().remove();
}

function formatNumber(num) {
    return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
}