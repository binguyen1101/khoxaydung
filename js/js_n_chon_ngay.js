
var page = $(".detail_wh").attr("data-page");
var kn = $(".detail_wh").attr("data-kn");
var kx = $(".detail_wh").attr("data-kx");
var th = $(".detail_wh").attr("data-th");

var ngay_tao_start = $(".detail_wh").attr("data-ngts");
var ngay_tao_end = $(".detail_wh").attr("data-ngte");

var ngay_th_start = $(".detail_wh").attr("data-ngths");
var ngay_th_end = $(".detail_wh").attr("data-ngthe");

var ngay_ht_start = $(".detail_wh").attr("data-nghts");
var ngay_ht_end = $(".detail_wh").attr("data-nghte");

$.ajax({
    url: "../render/tb_dieu_chuyen_kho.php",
    type: "POST",
    data:{
        kn: kn,
        kx: kx,
        th: th,

        ngay_tao_start: ngay_tao_start,
        ngay_tao_end: ngay_tao_end,

        ngay_th_start: ngay_th_start,
        ngay_th_end: ngay_th_end,

        ngay_ht_start: ngay_ht_start,
        ngay_ht_end: ngay_ht_end,

        page: page,
    },
    success: function(data){
        $(".detail_wh").append(data);
    }


})
$(".select_all_ex, .select_all_in, .select_st_tf").on("change",function () {
    var all_kho_xuat = $('.select_all_ex').val();
    var all_kho_nhap = $('.select_all_in').val();
    var trang_thai = $('.select_st_tf').val();
    var page = $(".detail_wh").attr("data-page");
    
    if( all_kho_nhap == "" && all_kho_xuat == "" && trang_thai == "" && page != ""){
        window.location.href = "/dieu-chuyen-kho.html";
    }else if(all_kho_nhap != "" && all_kho_xuat == "" && trang_thai == "" && page != ""){
        window.location.href = "/dieu-chuyen-kho.html?kn="+ all_kho_nhap + "&page=" + page;
    } else if(all_kho_nhap != "" && all_kho_xuat != "" && trang_thai == "" && page != ""){
        window.location.href = "/dieu-chuyen-kho.html?kn="+ all_kho_nhap + "&kx=" + all_kho_xuat + "&page=" + page;
    } else if(all_kho_nhap != "" && all_kho_xuat != "" && trang_thai != "" && page != ""){
        window.location.href = "/dieu-chuyen-kho.html?kn="+ all_kho_nhap + "&kx=" + all_kho_xuat + "&th=" + trang_thai + "&page=" + page;
    }else if(all_kho_nhap == "" && all_kho_xuat != "" && trang_thai == "" && page != ""){
        window.location.href = "/dieu-chuyen-kho.html?kx=" + all_kho_xuat;
    }else if(all_kho_nhap == "" && all_kho_xuat != "" && trang_thai != "" && page != ""){
        window.location.href = "/dieu-chuyen-kho.html?kx=" + all_kho_xuat + "&th=" + trang_thai + "&page=" + page;
    }else if(all_kho_nhap == "" && all_kho_xuat == "" && trang_thai != "" && page != ""){
        window.location.href = "/dieu-chuyen-kho.html?th=" + trang_thai + "&page=" + page;
    }else{
        window.location.href = "/dieu-chuyen-kho.html?kn="+ all_kho_nhap + "&th=" + trang_thai + "&page=" + page;
    }

    $("input[name='input_search'").val('');
});

$('.warehouse_transfer_main .date_cr img').click(function(){
    ele = $(this);
    $('#popup_select_date').show();
    $('.popup_select_date .btn_save').on("click", function () {
        selectDate(this, ele);

        var page = $(".detail_wh").attr("data-page");
        var all_kho_xuat = $('.select_all_ex').val();
        var all_kho_nhap = $('.select_all_in').val();
        var trang_thai = $('.select_st_tf').val();
        var ngay_tao_start = $(this).parents('.content_popup').find('.d_flex.flex_column:nth-child(1) input').val();
        var ngay_tao_end = $(this).parents('.content_popup').find('.d_flex.flex_column:nth-child(2) input').val();

            if( all_kho_nhap == "" && all_kho_xuat == "" && trang_thai == "" && page != "" && ngay_tao_start == "" && ngay_tao_end == ""){
                window.location.href = "/dieu-chuyen-kho.html";
            }else if(all_kho_nhap != "" && all_kho_xuat == "" && trang_thai == "" && page != "" && ngay_tao_start != "" && ngay_tao_end != ""){
                window.location.href = "/dieu-chuyen-kho.html?kn="+ all_kho_nhap + "&page=" + page + "&ngts=" + ngay_tao_start + "&ngte=" + ngay_tao_end;
            } else if(all_kho_nhap != "" && all_kho_xuat != "" && trang_thai == "" && page != "" && ngay_tao_start != "" && ngay_tao_end != ""){
                window.location.href = "/dieu-chuyen-kho.html?kn="+ all_kho_nhap + "&kx=" + all_kho_xuat + "&page=" + page + "&ngts=" + ngay_tao_start + "&ngte=" + ngay_tao_end;
            } else if(all_kho_nhap != "" && all_kho_xuat != "" && trang_thai != "" && page != "" && ngay_tao_start != "" && ngay_tao_end != ""){
                window.location.href = "/dieu-chuyen-kho.html?kn="+ all_kho_nhap + "&kx=" + all_kho_xuat + "&th=" + trang_thai + "&page=" + page + "&ngts=" + ngay_tao_start + "&ngte=" + ngay_tao_end;
            }else if(all_kho_nhap == "" && all_kho_xuat != "" && trang_thai == "" && page != "" && ngay_tao_start != "" && ngay_tao_end != ""){
                window.location.href = "/dieu-chuyen-kho.html?kx=" + all_kho_xuat + "&ngts=" + ngay_tao_start + "&ngte=" + ngay_tao_end;
            }else if(all_kho_nhap == "" && all_kho_xuat != "" && trang_thai != "" && page != "" && ngay_tao_start != "" && ngay_tao_end != ""){
                window.location.href = "/dieu-chuyen-kho.html?kx=" + all_kho_xuat + "&th=" + trang_thai + "&page=" + page + "&ngts=" + ngay_tao_start + "&ngte=" + ngay_tao_end;
            }else if(all_kho_nhap == "" && all_kho_xuat == "" && trang_thai != "" && page != "" && ngay_tao_start != "" && ngay_tao_end != ""){
                window.location.href = "/dieu-chuyen-kho.html?th=" + trang_thai + "&page=" + page + "&ngts=" + ngay_tao_start + "&ngte=" + ngay_tao_end;
            }else{
                window.location.href = "/dieu-chuyen-kho.html?kn="+ all_kho_nhap + "&th=" + trang_thai + "&page=" + page + "&ngts=" + ngay_tao_start + "&ngte=" + ngay_tao_end;
            }
    })
});

$('.warehouse_transfer_main .date_pf img').click(function(){
    ele = $(this);
    $('#popup_select_date').show();
    $('.popup_select_date .btn_save').on("click", function () {
        selectDate(this, ele);

        var page = $(".detail_wh").attr("data-page");
        var all_kho_xuat = $('.select_all_ex').val();
        var all_kho_nhap = $('.select_all_in').val();
        var trang_thai = $('.select_st_tf').val();
        var ngay_tao_start = $('#date_cr_start').attr("data");
        var ngay_tao_end = $('#date_cr_end').attr("data");

        var ngay_th_start = $(this).parents('.content_popup').find('.d_flex.flex_column:nth-child(1) input').val();
        var ngay_th_end = $(this).parents('.content_popup').find('.d_flex.flex_column:nth-child(2) input').val();

            if( all_kho_nhap == "" && all_kho_xuat == "" && trang_thai == "" && page != "" && ngay_tao_start == "" && ngay_tao_end == "" && ngay_th_start == "" && ngay_th_end == ""){
                window.location.href = "/dieu-chuyen-kho.html";
            }else if(all_kho_nhap != "" && all_kho_xuat == "" && trang_thai == "" && page != "" && ngay_tao_start != "" && ngay_tao_end != ""  && ngay_th_start != "" && ngay_th_end != ""){
                window.location.href = "/dieu-chuyen-kho.html?kn="+ all_kho_nhap + "&page=" + page + "&ngts=" + ngay_tao_start + "&ngte=" + ngay_tao_end + "&ngths=" + ngay_th_start + "&ngthe=" + ngay_th_end;
            } else if(all_kho_nhap != "" && all_kho_xuat != "" && trang_thai == "" && page != "" && ngay_tao_start != "" && ngay_tao_end != ""  && ngay_th_start != "" && ngay_th_end != ""){
                window.location.href = "/dieu-chuyen-kho.html?kn="+ all_kho_nhap + "&kx=" + all_kho_xuat + "&page=" + page + "&ngts=" + ngay_tao_start + "&ngte=" + ngay_tao_end + "&ngths=" + ngay_th_start + "&ngthe=" + ngay_th_end;
            } else if(all_kho_nhap != "" && all_kho_xuat != "" && trang_thai != "" && page != "" && ngay_tao_start != "" && ngay_tao_end != ""  && ngay_th_start != "" && ngay_th_end != ""){
                window.location.href = "/dieu-chuyen-kho.html?kn="+ all_kho_nhap + "&kx=" + all_kho_xuat + "&th=" + trang_thai + "&page=" + page + "&ngts=" + ngay_tao_start + "&ngte=" + ngay_tao_end + "&ngths=" + ngay_th_start + "&ngthe=" + ngay_th_end;
            }else if(all_kho_nhap == "" && all_kho_xuat != "" && trang_thai == "" && page != "" && ngay_tao_start != "" && ngay_tao_end != ""  && ngay_th_start != "" && ngay_th_end != ""){
                window.location.href = "/dieu-chuyen-kho.html?kx=" + all_kho_xuat + "&ngts=" + ngay_tao_start + "&ngte=" + ngay_tao_end + "&ngths=" + ngay_th_start + "&ngthe=" + ngay_th_end;
            }else if(all_kho_nhap == "" && all_kho_xuat != "" && trang_thai != "" && page != "" && ngay_tao_start != "" && ngay_tao_end != ""  && ngay_th_start != "" && ngay_th_end != ""){
                window.location.href = "/dieu-chuyen-kho.html?kx=" + all_kho_xuat + "&th=" + trang_thai + "&page=" + page + "&ngts=" + ngay_tao_start + "&ngte=" + ngay_tao_end + "&ngths=" + ngay_th_start + "&ngthe=" + ngay_th_end;
            }else if(all_kho_nhap == "" && all_kho_xuat == "" && trang_thai != "" && page != "" && ngay_tao_start != "" && ngay_tao_end != ""  && ngay_th_start != "" && ngay_th_end != ""){
                window.location.href = "/dieu-chuyen-kho.html?th=" + trang_thai + "&page=" + page + "&ngts=" + ngay_tao_start + "&ngte=" + ngay_tao_end + "&ngths=" + ngay_th_start + "&ngthe=" + ngay_th_end;
            }else{
                window.location.href = "/dieu-chuyen-kho.html?kn="+ all_kho_nhap + "&th=" + trang_thai + "&page=" + page + "&ngts=" + ngay_tao_start + "&ngte=" + ngay_tao_end + "&ngths=" + ngay_th_start + "&ngthe=" + ngay_th_end;
            }
    })
});

$('.warehouse_transfer_main .date_rq img').click(function(){
    ele = $(this);
    $('#popup_select_date').show();
    $('.popup_select_date .btn_save').on("click", function () {
        selectDate(this, ele);

        var page = $(".detail_wh").attr("data-page");
        var all_kho_xuat = $('.select_all_ex').val();
        var all_kho_nhap = $('.select_all_in').val();
        var trang_thai = $('.select_st_tf').val();

        var ngay_tao_start = $('#date_cr_start').attr("data");
        var ngay_tao_end = $('#date_cr_end').attr("data");

        var ngay_th_start = $('#date_pf_start').attr("data");
        var ngay_th_end = $('#date_pf_end').attr("data");

        var ngay_ht_start = $(this).parents('.content_popup').find('.d_flex.flex_column:nth-child(1) input').val();
        var ngay_ht_end = $(this).parents('.content_popup').find('.d_flex.flex_column:nth-child(2) input').val();

            if( all_kho_nhap == "" && all_kho_xuat == "" && trang_thai == "" && page != "" && ngay_tao_start == "" && ngay_tao_end == "" && ngay_th_start == "" && ngay_th_end == "" && ngay_ht_start == "" && ngay_ht_end == ""){
                window.location.href = "/dieu-chuyen-kho.html";
            }else if(all_kho_nhap != "" && all_kho_xuat == "" && trang_thai == "" && page != "" && ngay_tao_start != "" && ngay_tao_end != "" && ngay_th_start != "" && ngay_th_end != "" && ngay_ht_start != "" && ngay_ht_end != ""){
                window.location.href = "/dieu-chuyen-kho.html?kn="+ all_kho_nhap + "&page=" + page + "&ngts=" + ngay_tao_start + "&ngte=" + ngay_tao_end + "&ngths=" + ngay_th_start + "&ngthe=" + ngay_th_end + "&nghts=" + ngay_ht_start + "&nghte=" + ngay_ht_end;
            } else if(all_kho_nhap != "" && all_kho_xuat != "" && trang_thai == "" && page != "" && ngay_tao_start != "" && ngay_tao_end != ""  && ngay_th_start != "" && ngay_th_end != "" && ngay_ht_start != "" && ngay_ht_end != ""){
                window.location.href = "/dieu-chuyen-kho.html?kn="+ all_kho_nhap + "&kx=" + all_kho_xuat + "&page=" + page + "&ngts=" + ngay_tao_start + "&ngte=" + ngay_tao_end + "&ngths=" + ngay_th_start + "&ngthe=" + ngay_th_end + "&nghts=" + ngay_ht_start + "&nghte=" + ngay_ht_end;
            } else if(all_kho_nhap != "" && all_kho_xuat != "" && trang_thai != "" && page != "" && ngay_tao_start != "" && ngay_tao_end != ""  && ngay_th_start != "" && ngay_th_end != "" && ngay_ht_start != "" && ngay_ht_end != ""){
                window.location.href = "/dieu-chuyen-kho.html?kn="+ all_kho_nhap + "&kx=" + all_kho_xuat + "&th=" + trang_thai + "&page=" + page + "&ngts=" + ngay_tao_start + "&ngte=" + ngay_tao_end + "&ngths=" + ngay_th_start + "&ngthe=" + ngay_th_end + "&nghts=" + ngay_ht_start + "&nghte=" + ngay_ht_end;
            }else if(all_kho_nhap == "" && all_kho_xuat != "" && trang_thai == "" && page != "" && ngay_tao_start != "" && ngay_tao_end != ""  && ngay_th_start != "" && ngay_th_end != "" && ngay_ht_start != "" && ngay_ht_end != ""){
                window.location.href = "/dieu-chuyen-kho.html?kx=" + all_kho_xuat + "&ngts=" + ngay_tao_start + "&ngte=" + ngay_tao_end + "&ngths=" + ngay_th_start + "&ngthe=" + ngay_th_end + "&nghts=" + ngay_ht_start + "&nghte=" + ngay_ht_end;
            }else if(all_kho_nhap == "" && all_kho_xuat != "" && trang_thai != "" && page != "" && ngay_tao_start != "" && ngay_tao_end != ""  && ngay_th_start != "" && ngay_th_end != "" && ngay_ht_start != "" && ngay_ht_end != ""){
                window.location.href = "/dieu-chuyen-kho.html?kx=" + all_kho_xuat + "&th=" + trang_thai + "&page=" + page + "&ngts=" + ngay_tao_start + "&ngte=" + ngay_tao_end + "&ngths=" + ngay_th_start + "&ngthe=" + ngay_th_end + "&nghts=" + ngay_ht_start + "&nghte=" + ngay_ht_end;
            }else if(all_kho_nhap == "" && all_kho_xuat == "" && trang_thai != "" && page != "" && ngay_tao_start != "" && ngay_tao_end != ""  && ngay_th_start != "" && ngay_th_end != "" && ngay_ht_start != "" && ngay_ht_end != ""){
                window.location.href = "/dieu-chuyen-kho.html?th=" + trang_thai + "&page=" + page + "&ngts=" + ngay_tao_start + "&ngte=" + ngay_tao_end + "&ngths=" + ngay_th_start + "&ngthe=" + ngay_th_end + "&nghts=" + ngay_ht_start + "&nghte=" + ngay_ht_end;
            }else{
                window.location.href = "/dieu-chuyen-kho.html?kn="+ all_kho_nhap + "&th=" + trang_thai + "&page=" + page + "&ngts=" + ngay_tao_start + "&ngte=" + ngay_tao_end + "&ngths=" + ngay_th_start + "&ngthe=" + ngay_th_end + "&nghts=" + ngay_ht_start + "&nghte=" + ngay_ht_end;
            }
    })
});




