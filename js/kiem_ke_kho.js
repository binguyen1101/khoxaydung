
var page = $(".detail_table").attr("data-page");
var all_k = $(".detail_table").attr("data-k");
var th = $(".detail_table").attr("data-th");
var ht = $(".detail_table").attr("data-ht");

var ngay_tao_start = $(".detail_table").attr("data-ngts");
var ngay_tao_end = $(".detail_table").attr("data-ngte");

var ngay_th_start = $(".detail_table").attr("data-ngths");
var ngay_th_end = $(".detail_table").attr("data-ngthe");

var ngay_ycht_start = $(".detail_table").attr("data-ngychts");
var ngay_ycht_end = $(".detail_table").attr("data-ngychte");

var ngay_ht_start = $(".detail_table").attr("data-nghts");
var ngay_ht_end = $(".detail_table").attr("data-nghte");

var input_val = $(".detail_table").attr("data-ip");
var curr = $(".detail_table").attr("data-dis");


$.ajax({
    url: "../render/tb_kiem_ke_kho.php",
    type: "POST",
    data:{
        all_k: all_k,
        th: th,

        ngay_tao_start: ngay_tao_start,
        ngay_tao_end: ngay_tao_end,

        ngay_th_start: ngay_th_start,
        ngay_th_end: ngay_th_end,

        ngay_ycht_start: ngay_ycht_start,
        ngay_ycht_end: ngay_ycht_end,

        ngay_ht_start: ngay_ht_start,
        ngay_ht_end: ngay_ht_end,

        input_val: input_val,

        page: page,
        curr: curr
    },
    success: function(data){
        $(".detail_table").append(data);
    }
});

$(".select_all_wh, .select_iv_tf").on("change",function () {
    var page = 1;
    var all_kho = $('.select_all_wh').val();
    var trang_thai = $('.select_iv_tf').val();
    
    var ngay_tao_start = $('#date_cr_start').attr("data");
    var ngay_tao_end = $('#date_cr_end').attr("data");
    
    var ngay_th_start = $('#date_pf_start').attr("data");
    var ngay_th_end = $('#date_pf_end').attr("data");

    var ngay_ycht_start = $('#date_rq_start').attr("data");
    var ngay_ycht_end = $('#date_rq_end').attr("data");

    var ngay_ht_start = $('#date_fn_start').attr("data");
    var ngay_ht_end = $('#date_fn_end').attr("data");
    
    var input_val = $("input[name='input_search']").val();
    var curr = $('.show_tr_tb').val();
    
    if(all_kho == "" && trang_thai == "" && ngay_tao_start == "" && ngay_tao_end == "" && ngay_th_start == "" && ngay_th_end == "" && ngay_ycht_start == "" && ngay_ycht_end == "" && ngay_ht_start == "" && ngay_ht_end == "" && input_val == "" && curr == 10){
        window.location.href = "/kiem-ke-kho.html?dis=" + curr + '&page=' + page;
    }else{
        window.location.href = "/kiem-ke-kho.html?all_k=" + all_kho + "&th=" + trang_thai + "&ngts=" + ngay_tao_start + "&ngte=" + ngay_tao_end + "&ngths=" + ngay_th_start + "&ngthe=" + ngay_th_end + "&ngychts=" + ngay_ycht_start + "&ngychte=" + ngay_ycht_end + "&nghts=" + ngay_ht_start + "&nghte=" + ngay_ht_end + "&input=" + input_val + "&dis=" + curr + "&page=" + page;
    }

    $("input[name='input_search'").val('');
});

$('.date_cr img').click(function(){
    ele = $(this);
    $('#popup_select_date').show();
    $('.popup_select_date .btn_save').on("click", function () {
        selectDate(this, ele);

        var page = 1;
        var all_kho = $('.select_all_wh').val();
        var trang_thai = $('.select_iv_tf').val();
        
        var ngay_tao_start = $(this).parents('.content_popup').find('.d_flex.flex_column:nth-child(1) input').val();
        var ngay_tao_end = $(this).parents('.content_popup').find('.d_flex.flex_column:nth-child(2) input').val();
        
        var ngay_th_start = $('#date_pf_start').attr("data");
        var ngay_th_end = $('#date_pf_end').attr("data");

        var ngay_ycht_start = $('#date_rq_start').attr("data");
        var ngay_ycht_end = $('#date_rq_end').attr("data");

        var ngay_ht_start = $('#date_fn_start').attr("data");
        var ngay_ht_end = $('#date_fn_end').attr("data");
        
        var input_val = $("input[name='input_search']").val();
        var curr = $('.show_tr_tb').val();
        
        if(all_kho == "" && trang_thai == "" && ngay_tao_start == "" && ngay_tao_end == "" && ngay_th_start == "" && ngay_th_end == "" && ngay_ycht_start == "" && ngay_ycht_end == "" && ngay_ht_start == "" && ngay_ht_end == "" && input_val == "" && curr == 10){
            window.location.href = "/kiem-ke-kho.html?dis=" + curr + '&page=' + page;
        }else{
            window.location.href = "/kiem-ke-kho.html?all_k=" + all_kho + "&th=" + trang_thai + "&ngts=" + ngay_tao_start + "&ngte=" + ngay_tao_end + "&ngths=" + ngay_th_start + "&ngthe=" + ngay_th_end + "&ngychts=" + ngay_ycht_start + "&ngychte=" + ngay_ycht_end + "&nghts=" + ngay_ht_start + "&nghte=" + ngay_ht_end + "&input=" + input_val + "&dis=" + curr + "&page=" + page;
        }

    })
});

$('.date_pf img').click(function(){
    ele = $(this);
    $('#popup_select_date').show();
    $('.popup_select_date .btn_save').on("click", function () {
        selectDate(this, ele);

        var page = 1;
        var all_kho = $('.select_all_wh').val();
        var trang_thai = $('.select_iv_tf').val();
        
        var ngay_tao_start = $('#date_cr_start').attr("data");
        var ngay_tao_end = $('#date_cr_end').attr("data");
        
        var ngay_th_start = $(this).parents('.content_popup').find('.d_flex.flex_column:nth-child(1) input').val();
        var ngay_th_end = $(this).parents('.content_popup').find('.d_flex.flex_column:nth-child(2) input').val();

        var ngay_ycht_start = $('#date_rq_start').attr("data");
        var ngay_ycht_end = $('#date_rq_end').attr("data");

        var ngay_ht_start = $('#date_fn_start').attr("data");
        var ngay_ht_end = $('#date_fn_end').attr("data");
        
        var input_val = $("input[name='input_search']").val();
        var curr = $('.show_tr_tb').val();

        if(all_kho == "" && trang_thai == "" && ngay_tao_start == "" && ngay_tao_end == "" && ngay_th_start == "" && ngay_th_end == "" && ngay_ycht_start == "" && ngay_ycht_end == "" && ngay_ht_start == "" && ngay_ht_end == "" && input_val == "" && curr == 10){
            window.location.href = "/kiem-ke-kho.html?dis=" + curr + '&page=' + page;
        }else{
            window.location.href = "/kiem-ke-kho.html?all_k=" + all_kho + "&th=" + trang_thai + "&ngts=" + ngay_tao_start + "&ngte=" + ngay_tao_end + "&ngths=" + ngay_th_start + "&ngthe=" + ngay_th_end + "&ngychts=" + ngay_ycht_start + "&ngychte=" + ngay_ycht_end + "&nghts=" + ngay_ht_start + "&nghte=" + ngay_ht_end + "&input=" + input_val + "&dis=" + curr + "&page=" + page;
        }
    })
});

$('.date_rq img').click(function(){
    ele = $(this);
    $('#popup_select_date').show();
    $('.popup_select_date .btn_save').on("click", function () {
        selectDate(this, ele);

        var page = 1;
        var all_kho = $('.select_all_wh').val();
        var trang_thai = $('.select_iv_tf').val();
        
        var ngay_tao_start = $('#date_cr_start').attr("data");
        var ngay_tao_end = $('#date_cr_end').attr("data");
        
        var ngay_th_start = $('#date_pf_start').attr("data");
        var ngay_th_end = $('#date_pf_end').attr("data");

        var ngay_ycht_start = $(this).parents('.content_popup').find('.d_flex.flex_column:nth-child(1) input').val();
        var ngay_ycht_end = $(this).parents('.content_popup').find('.d_flex.flex_column:nth-child(2) input').val();

        var ngay_ht_start = $('#date_fn_start').attr("data");
        var ngay_ht_end = $('#date_fn_end').attr("data");
        
        var input_val = $("input[name='input_search']").val();
        var curr = $('.show_tr_tb').val();

        if(all_kho == "" && trang_thai == "" && ngay_tao_start == "" && ngay_tao_end == "" && ngay_th_start == "" && ngay_th_end == "" && ngay_ycht_start == "" && ngay_ycht_end == "" && ngay_ht_start == "" && ngay_ht_end == "" && input_val == "" && curr == 10){
            window.location.href = "/kiem-ke-kho.html?dis=" + curr + '&page=' + page;
        }else{
            window.location.href = "/kiem-ke-kho.html?all_k=" + all_kho + "&th=" + trang_thai + "&ngts=" + ngay_tao_start + "&ngte=" + ngay_tao_end + "&ngths=" + ngay_th_start + "&ngthe=" + ngay_th_end + "&ngychts=" + ngay_ycht_start + "&ngychte=" + ngay_ycht_end + "&nghts=" + ngay_ht_start + "&nghte=" + ngay_ht_end + "&input=" + input_val + "&dis=" + curr + "&page=" + page;
        }
    })
});

$('.date_fn img').click(function(){
    ele = $(this);
    $('#popup_select_date').show();
    $('.popup_select_date .btn_save').on("click", function () {
        selectDate(this, ele);

        var page = 1;
        var all_kho = $('.select_all_wh').val();
        var trang_thai = $('.select_iv_tf').val();
        
        var ngay_tao_start = $('#date_cr_start').attr("data");
        var ngay_tao_end = $('#date_cr_end').attr("data");
        
        var ngay_th_start = $('#date_pf_start').attr("data");
        var ngay_th_end = $('#date_pf_end').attr("data");

        var ngay_ycht_start = $('#date_rq_start').attr("data");
        var ngay_ycht_end = $('#date_rq_end').attr("data");

        var ngay_ht_start = $(this).parents('.content_popup').find('.d_flex.flex_column:nth-child(1) input').val();
        var ngay_ht_end = $(this).parents('.content_popup').find('.d_flex.flex_column:nth-child(2) input').val();
        
        var input_val = $("input[name='input_search']").val();
        var curr = $('.show_tr_tb').val();

        if(all_kho == "" && trang_thai == "" && ngay_tao_start == "" && ngay_tao_end == "" && ngay_th_start == "" && ngay_th_end == "" && ngay_ycht_start == "" && ngay_ycht_end == "" && ngay_ht_start == "" && ngay_ht_end == "" && input_val == "" && curr == 10){
            window.location.href = "/kiem-ke-kho.html?dis=" + curr + '&page=' + page;
        }else{
            window.location.href = "/kiem-ke-kho.html?all_k=" + all_kho + "&th=" + trang_thai + "&ngts=" + ngay_tao_start + "&ngte=" + ngay_tao_end + "&ngths=" + ngay_th_start + "&ngthe=" + ngay_th_end + "&ngychts=" + ngay_ycht_start + "&ngychte=" + ngay_ycht_end + "&nghts=" + ngay_ht_start + "&nghte=" + ngay_ht_end + "&input=" + input_val + "&dis=" + curr + "&page=" + page;
        }
    })
});


$(".icon_sr_wh").click(function(){
    var page = 1;
    var all_kho = $('.select_all_wh').val();
    var trang_thai = $('.select_iv_tf').val();
    
    var ngay_tao_start = $('#date_cr_start').attr("data");
    var ngay_tao_end = $('#date_cr_end').attr("data");
    
    var ngay_th_start = $('#date_pf_start').attr("data");
    var ngay_th_end = $('#date_pf_end').attr("data");

    var ngay_ycht_start = $('#date_rq_start').attr("data");
    var ngay_ycht_end = $('#date_rq_end').attr("data");

    var ngay_ht_start = $('#date_fn_start').attr("data");
    var ngay_ht_end = $('#date_fn_end').attr("data");
    
    var input_val = $("input[name='input_search']").val();
    var curr = $('.show_tr_tb').val();
    
    if(all_kho == "" && trang_thai == "" && ngay_tao_start == "" && ngay_tao_end == "" && ngay_th_start == "" && ngay_th_end == "" && ngay_ycht_start == "" && ngay_ycht_end == "" && ngay_ht_start == "" && ngay_ht_end == "" && input_val == "" && curr == 10){
        window.location.href = "/kiem-ke-kho.html?dis=" + curr + '&page=' + page;
    }else{
        window.location.href = "/kiem-ke-kho.html?all_k=" + all_kho + "&th=" + trang_thai + "&ngts=" + ngay_tao_start + "&ngte=" + ngay_tao_end + "&ngths=" + ngay_th_start + "&ngthe=" + ngay_th_end + "&ngychts=" + ngay_ycht_start + "&ngychte=" + ngay_ycht_end + "&nghts=" + ngay_ht_start + "&nghte=" + ngay_ht_end + "&input=" + input_val + "&dis=" + curr + "&page=" + page;
    }
});

$(document).keyup(function (e) {
    if ($("input[name='input_search']").is(":focus") && (e.keyCode == 13)) {
        $(".icon_sr_wh").click(); 
    }
});

function id_xoa_nhapKho(id){
    var id_phieu =  $(id).attr("data");
    $.ajax({
        url:'../ajax/xoa_kiemKe.php',
        type:'POST',
        data:{
            id_phieu:id_phieu,
        },
        success:function(data){
            if(data == ""){
                alert("Xóa phiếu kiểm kê kho thành công!");
                window.location.reload();
            }else{
                alert("Xóa phiếu kiểm kê kho thất bại!");
            }
        }
    });
}

$(".btn_ex").click(function() {
    window.location.href = '../Excel/kiem_ke.php';
});

function display(select){
    var curr = $(select).val();
    var page = 1;
    var all_kho = $('.select_all_wh').val();
    var trang_thai = $('.select_iv_tf').val();
    
    var ngay_tao_start = $('#date_cr_start').attr("data");
    var ngay_tao_end = $('#date_cr_end').attr("data");
    
    var ngay_th_start = $('#date_pf_start').attr("data");
    var ngay_th_end = $('#date_pf_end').attr("data");

    var ngay_ycht_start = $('#date_rq_start').attr("data");
    var ngay_ycht_end = $('#date_rq_end').attr("data");

    var ngay_ht_start = $('#date_fn_start').attr("data");
    var ngay_ht_end = $('#date_fn_end').attr("data");
    
    var input_val = $("input[name='input_search']").val();
    
    if(all_kho == "" && trang_thai == "" && ngay_tao_start == "" && ngay_tao_end == "" && ngay_th_start == "" && ngay_th_end == "" && ngay_ycht_start == "" && ngay_ycht_end == "" && ngay_ht_start == "" && ngay_ht_end == "" && input_val == "" && curr == 10){
        window.location.href = "/kiem-ke-kho.html?dis=" + curr + '&page=' + page;
    }else{
        window.location.href = "/kiem-ke-kho.html?all_k=" + all_kho + "&th=" + trang_thai + "&ngts=" + ngay_tao_start + "&ngte=" + ngay_tao_end + "&ngths=" + ngay_th_start + "&ngthe=" + ngay_th_end + "&ngychts=" + ngay_ycht_start + "&ngychte=" + ngay_ycht_end + "&nghts=" + ngay_ht_start + "&nghte=" + ngay_ht_end + "&input=" + input_val + "&dis=" + curr + "&page=" + page;
    }
}




