
var page = $(".main_nhapKho").attr("data-page");
var all_k = $(".main_nhapKho").attr("data-k");
var th = $(".main_nhapKho").attr("data-th");
var ht = $(".main_nhapKho").attr("data-ht");

var ngay_tao_start = $(".main_nhapKho").attr("data-ngts");
var ngay_tao_end = $(".main_nhapKho").attr("data-ngte");

var ngay_nh_start = $(".main_nhapKho").attr("data-ngns");
var ngay_nh_end = $(".main_nhapKho").attr("data-ngne");

var input_val = $(".main_nhapKho").attr("data-ip");
var curr = $(".main_nhapKho").attr("data-dis");

$.ajax({
    url: "../render/tb_nhap_kho.php",
    type: "POST",
    data:{
        all_k: all_k,
        th: th,
        ht: ht,

        ngay_tao_start: ngay_tao_start,
        ngay_tao_end: ngay_tao_end,

        ngay_nh_start: ngay_nh_start,
        ngay_nh_end: ngay_nh_end,

        input_val: input_val,

        page: page,
        curr: curr
    },
    success: function(data){
        $(".main_nhapKho").append(data);
    }
});

$(".select_all_kho, .select_all_form_in, .select_all_in_note_status").on("change",function () {
    var page = 1;
    var all_kho = $('.select_all_kho').val();
    var trang_thai = $('.select_all_in_note_status').val();
    var hinh_thuc = $('.select_all_form_in').val();
    
    var ngay_tao_start = $('#date_cr_start').attr("data");
    var ngay_tao_end = $('#date_cr_end').attr("data");
    
    var ngay_nh_start = $('#date_in_start').attr("data");
    var ngay_nh_end = $('#date_in_end').attr("data");
    
    var input_val = $("input[name='input_search']").val();
    var curr = $('.show_tr_tb').val();
    
    if(all_kho == "" && trang_thai == "" && hinh_thuc == "" && ngay_tao_start == "" && ngay_tao_end == "" && ngay_nh_start == "" && ngay_nh_end == "" && input_val == "" && curr == 10){
        window.location.href = "/nhap-kho.html?dis=" + curr + '&page=' + page;
    }else{
        window.location.href = "/nhap-kho.html?all_k=" + all_kho + "&th=" + trang_thai + "&ht=" + hinh_thuc + "&ngts=" + ngay_tao_start + "&ngte=" + ngay_tao_end + "&ngns=" + ngay_nh_start + "&ngne=" + ngay_nh_end + "&input=" + input_val + "&dis=" + curr + "&page=" + page;
    }

    $("input[name='input_search'").val('');
});

$('.date_cr img').click(function(){
    ele = $(this);
    $('#popup_select_date').show();
    $('.popup_select_date .btn_save').on("click", function () {
        selectDate(this, ele);

        var page = 1;
        var all_kho = $('.select_all_kho').val();
        var trang_thai = $('.select_all_in_note_status').val();
        var hinh_thuc = $('.select_all_form_in').val();

        var ngay_tao_start = $(this).parents('.content_popup').find('.d_flex.flex_column:nth-child(1) input').val();
        var ngay_tao_end = $(this).parents('.content_popup').find('.d_flex.flex_column:nth-child(2) input').val();

        var ngay_nh_start = $('#date_in_start').attr("data");
        var ngay_nh_end = $('#date_in_end').attr("data");

        var input_val = $("input[name='input_search']").val();
        var curr = $('.show_tr_tb').val();

        if(all_kho == "" && trang_thai == "" && hinh_thuc == "" && ngay_tao_start == "" && ngay_tao_end == "" && ngay_nh_start == "" && ngay_nh_end == "" && input_val == "" && curr == 10){
            window.location.href = "/nhap-kho.html?dis=" + curr + '&page=' + page;
        }else{
            window.location.href = "/nhap-kho.html?all_k=" + all_kho + "&th=" + trang_thai + "&ht=" + hinh_thuc + "&ngts=" + ngay_tao_start + "&ngte=" + ngay_tao_end + "&ngns=" + ngay_nh_start + "&ngne=" + ngay_nh_end + "&input=" + input_val + "&dis=" + curr + "&page=" + page;
        }

    })
});

$('.date_in img').click(function(){
    ele = $(this);
    $('#popup_select_date').show();
    $('.popup_select_date .btn_save').on("click", function () {
        selectDate(this, ele);

        var page = 1;
        var all_kho = $('.select_all_kho').val();
        var trang_thai = $('.select_all_in_note_status').val();
        var hinh_thuc = $('.select_all_form_in').val();
        var ngay_tao_start = $('#date_cr_start').attr("data");
        var ngay_tao_end = $('#date_cr_end').attr("data");

        var ngay_nh_start = $(this).parents('.content_popup').find('.d_flex.flex_column:nth-child(1) input').val();
        var ngay_nh_end = $(this).parents('.content_popup').find('.d_flex.flex_column:nth-child(2) input').val();

        var input_val = $("input[name='input_search']").val();
        var curr = $('.show_tr_tb').val();

        if(all_kho == "" && trang_thai == "" && hinh_thuc == "" && ngay_tao_start == "" && ngay_tao_end == "" && ngay_nh_start == "" && ngay_nh_end == "" && input_val == "" && curr == 10){
            window.location.href = "/nhap-kho.html?dis=" + curr + '&page=' + page;
        }else{
            window.location.href = "/nhap-kho.html?all_k=" + all_kho + "&th=" + trang_thai + "&ht=" + hinh_thuc + "&ngts=" + ngay_tao_start + "&ngte=" + ngay_tao_end + "&ngns=" + ngay_nh_start + "&ngne=" + ngay_nh_end + "&input=" + input_val + "&dis=" + curr + "&page=" + page;
        }
    })
});


$(".icon_sr_wh").click(function(){
    var page = 1;
    var all_kho = $('.select_all_kho').val();
    var trang_thai = $('.select_all_in_note_status').val();
    var hinh_thuc = $('.select_all_form_in').val();
    
    var ngay_tao_start = $('#date_cr_start').attr("data");
    var ngay_tao_end = $('#date_cr_end').attr("data");
    
    var ngay_nh_start = $('#date_in_start').attr("data");
    var ngay_nh_end = $('#date_in_end').attr("data");
    
    var input_val = $("input[name='input_search']").val();
    var curr = $('.show_tr_tb').val();
    
    if(all_kho == "" && trang_thai == "" && hinh_thuc == "" && ngay_tao_start == "" && ngay_tao_end == "" && ngay_nh_start == "" && ngay_nh_end == "" && input_val == "" && curr == 10){
        window.location.href = "/nhap-kho.html?dis=" + curr + '&page=' + page;
    }else{
        window.location.href = "/nhap-kho.html?all_k=" + all_kho + "&th=" + trang_thai + "&ht=" + hinh_thuc + "&ngts=" + ngay_tao_start + "&ngte=" + ngay_tao_end + "&ngns=" + ngay_nh_start + "&ngne=" + ngay_nh_end + "&input=" + input_val + "&dis=" + curr + "&page=" + page;
    }
});

$(document).keyup(function (e) {
    if ($("input[name='input_search']").is(":focus") && (e.keyCode == 13)) {
        $(".icon_sr_wh").click();
    }
});

function id_xoa_nhapKho(id){
    var id_phieu =  $(id).attr("data");
    $.ajax({
        url:'../ajax/xoa_nhapKho.php',
        type:'POST',
        data:{
            id_phieu:id_phieu,
        },
        success:function(data){
            if(data == ""){
                alert("Xóa phiếu nhập kho thành công!");
                window.location.reload();
            }else{
                alert("Xóa phiếu nhập kho thất bại!");
            }
        }
    });
}

$(".btn_ex").click(function() {
    window.location.href = '../Excel/phieu_nhap.php';
});

function display(select){
    var curr = $(select).val();
    var page = 1;
    var all_kho = $('.select_all_kho').val();
    var trang_thai = $('.select_all_in_note_status').val();
    var hinh_thuc = $('.select_all_form_in').val();
    
    var ngay_tao_start = $('#date_cr_start').attr("data");
    var ngay_tao_end = $('#date_cr_end').attr("data");
    
    var ngay_nh_start = $('#date_in_start').attr("data");
    var ngay_nh_end = $('#date_in_end').attr("data");
    
    var input_val = $("input[name='input_search']").val();
    
    if(all_kho == "" && trang_thai == "" && hinh_thuc == "" && ngay_tao_start == "" && ngay_tao_end == "" && ngay_nh_start == "" && ngay_nh_end == "" && input_val == "" && curr == 10){
        window.location.href = "/nhap-kho.html?dis=" + curr + '&page=' + page;
    }else{
        window.location.href = "/nhap-kho.html?all_k=" + all_kho + "&th=" + trang_thai + "&ht=" + hinh_thuc + "&ngts=" + ngay_tao_start + "&ngte=" + ngay_tao_end + "&ngns=" + ngay_nh_start + "&ngne=" + ngay_nh_end + "&input=" + input_val + "&dis=" + curr + "&page=" + page;
    }
}



