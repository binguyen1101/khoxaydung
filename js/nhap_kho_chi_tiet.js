$('.active8').each(function() {
    if ($(this).hasClass('active8')) {
        $(this).find('a').addClass('active');
    }
});

$('.import_wh_detail .btn_tc').click(function () {
    $('.popup_ref_w').show(0, function () {
        var name = $('.import_wh_detail .ten_phieu').text();
        $('.popup_ref_w .btn_save').on('click',function () {
            var lido_valid = $("#f_reason_ref");
            var lido_tc = $("textarea[name='name_reason_ref']").val();
            var id_phieu = $('.import_wh_detail').attr('data-id');

            lido_valid.validate({
                errorPlacement: function (error, element) {
                    error.appendTo(element.parents(".name_reason_ref"));
                    error.wrap("<span class='error'>");
                },
                rules: {
                    name_reason_ref: "required",
                },
                messages: {
                    name_reason_ref: "Vui lòng nhập lí do từ chối.",
                },
            });

            if(lido_valid.valid() === true){
                $.ajax({
                    url: '../ajax/tu_choi_phieu_nhap.php',
                    type: 'POST',
                    data: {
                        lido_tc: lido_tc,
                        nguoi_tc: nguoi_tc,
                        id_phieu: id_phieu
                    },
                    success: function(data){
                        if(data != ""){
                            $('.popup_add_notif_succ').show();
                            var text = $('#popup_add_notif_succ .p_add_succ').text('');
                            var text_new = '';
                            text_new += 'Từ chối phiếu nhập kho';
                            text_new += '<strong>';
                            text_new += '&nbsp' + name;
                            text_new += '</strong>';
                            text_new += '&nbspthành công!';
                            text.append(text_new);
                            $('.popup_add_notif_succ .btn_close').on('click',function(){
                                $('.popup_ref_w').hide();
                                window.location.reload();
                            });
                        }else{
                            $('.popup_add_notif_succ').show();
                            $('.popup_add_notif_succ .img_f_succ').show();
                            $('.popup_add_notif_succ .img_p_succ').hide();
                            var text = $('#popup_add_notif_succ .p_add_succ').text('');
                            var text_new = '';
                            text_new += 'Từ chối phiếu nhập kho';
                            text_new += '<strong>';
                            text_new += '&nbsp' + name;
                            text_new += '</strong>';
                            text_new += '&nbspthất bại!';
                            text.append(text_new);
                        }
                    }
                });
            }
            
        });
    });
});

$('.import_wh_detail .btn_dt').click(function () {
    $('.popup_succ_w').show(0, function () {
        var name = $('.import_wh_detail .ten_phieu').text();
        $('.popup_succ_w strong').text(name);
        $('.popup_succ_w .btn_save').on('click',function () {
            var id_phieu = $('.import_wh_detail').attr('data-id');

                $.ajax({
                    url: '../ajax/duyet_phieu_nhap.php',
                    type: 'POST',
                    data: {
                        nguoi_duyet: nguoi_duyet,
                        id_phieu: id_phieu
                    },
                    success: function(data){
                        if(data != ""){
                            $('.popup_add_notif_succ').show();
                            var text = $('#popup_add_notif_succ .p_add_succ').text('');
                            var text_new = '';
                            text_new += 'Duyệt phiếu nhập kho';
                            text_new += '<strong>';
                            text_new += '&nbsp' + name;
                            text_new += '</strong>';
                            text_new += '&nbspthành công!';
                            text.append(text_new);
                            $('.popup_add_notif_succ .btn_close').on('click',function(){
                                $('.popup_succ_w').hide();
                                window.location.reload();
                            });
                        }else{
                            $('.popup_add_notif_succ').show();
                            $('.popup_add_notif_succ .img_f_succ').show();
                            $('.popup_add_notif_succ .img_p_succ').hide();
                            var text = $('#popup_add_notif_succ .p_add_succ').text('');
                            var text_new = '';
                            text_new += 'Duyệt phiếu nhập kho';
                            text_new += '<strong>';
                            text_new += '&nbsp' + name;
                            text_new += '</strong>';
                            text_new += '&nbspthất bại!';
                            text.append(text_new);
                        }
                    }
                });
        });
    });
});

$('.import_wh_detail .btn_del').click(function () {
    $('.popup_del_w').show(0, function () {
        var name = $('.import_wh_detail .ten_phieu').text();
        $('.popup_del_w strong').text(name);
        $('.popup_del_w .btn_save').on('click',function () {
        var id_phieu = $('.import_wh_detail').attr('data-id');

            $.ajax({
                url: '../ajax/del_dieu_chuyen_kho.php',
                type: 'POST',
                data: {
                    nguoi_xoa: nguoi_xoa,
                    id_phieu: id_phieu
                },
                success: function(data){
                    if(data != ""){
                        $('.popup_add_notif_succ').show();
                        var text = $('#popup_add_notif_succ .p_add_succ').text('');
                        var text_new = '';
                        text_new += 'Xóa phiếu nhập kho';
                        text_new += '<strong>';
                        text_new += '&nbsp' + name;
                        text_new += '</strong>';
                        text_new += '&nbspthành công!';
                        text.append(text_new);
                        $('.popup_add_notif_succ .btn_close').on('click',function(){
                            $('.popup_del_w').hide();
                            window.location.href = "/nhap-kho.html";
                        });
                    }else{
                        $('.popup_add_notif_succ').show();
                        $('.popup_add_notif_succ .img_f_succ').show();
                        $('.popup_add_notif_succ .img_p_succ').hide();
                        var text = $('#popup_add_notif_succ .p_add_succ').text('');
                        var text_new = '';
                        text_new += 'Xóa phiếu nhập kho';
                        text_new += '<strong>';
                        text_new += '&nbsp' + name;
                        text_new += '</strong>';
                        text_new += '&nbspthất bại!';
                        text.append(text_new);
                    }
                }
            });
        });
    });
});

$('.import_wh_detail .btn_ht').click(function () {
    $('.popup_ht_import_wh').show(0, function () {
        var name = $('.import_wh_detail .ten_phieu').text();
        $('.popup_ht_import_wh strong').text(name);
        $('.popup_ht_import_wh .btn_save').on('click',function () {
            var id_phieu = $('.import_wh_detail').attr('data-id');
            var id_kho = $('.kho_nhap').attr('data-id');

            var listTable = [];
            $(".sl_nhap").each(function() {
                var data = $(this).parent().attr('data-id');
                var soluong = $(this).text();
                listTable.push({
                    'id': data,
                    'soluong': soluong,
                });
            });
            // console.log(listTable);

            $.ajax({
                url: '../ajax/hoan_thanh_phieu.php',
                type: 'POST',
                data: {
                    nguoi_ht: nguoi_ht,
                    id_phieu: id_phieu,
                    id_kho: id_kho,
                    listTable: listTable
                },
                success: function(data){
                    if(data == ""){
                        $('.popup_add_notif_succ').show();
                        var text = $('#popup_add_notif_succ .p_add_succ').text('');
                        var text_new = '';
                        text_new += 'Hoàn thành phiếu nhập kho';
                        text_new += '<strong>';
                        text_new += '&nbsp' + name;
                        text_new += '</strong>';
                        text_new += '&nbspthành công!';
                        text.append(text_new);
                        $('.popup_add_notif_succ .btn_close').on('click',function(){
                            $('.popup_ht_import_wh').hide();
                            window.location.reload();
                        });
                    }else{
                        $('.popup_add_notif_succ').show();
                        $('.popup_add_notif_succ .img_f_succ').show();
                        $('.popup_add_notif_succ .img_p_succ').hide();
                        var text = $('#popup_add_notif_succ .p_add_succ').text('');
                        var text_new = '';
                        text_new += 'Hoàn thành phiếu nhập kho';
                        text_new += '<strong>';
                        text_new += '&nbsp' + name;
                        text_new += '</strong>';
                        text_new += '&nbspthất bại!';
                        text.append(text_new);
                    }
                }
            });
        });
    });
});

$(".btn_ex").click(function() {
    var id_phieu = $(this).attr("data");
    window.location.href = '../Excel/chi_tiet_phieu_nhap.php?id=' + id_phieu;
});