
    function resetFormValidator(formId) {
        $(formId).removeData('validator');
        $(formId).removeData('unobtrusiveValidation');
        $.validator.unobtrusive.parse(formId);
    }
    $('.active8').each(function() {
        if ($(this).hasClass('active8')) {
            $(this).find('a').addClass('active');
        }
    });

    function formatDateVN(date) {
        var currentMonth = (date.getMonth() + 1);
        var currentdate = date.getDate();
        if (currentdate < 10) {
            currentdate = '0' + currentdate;
        }
        if (currentMonth < 10) {
            currentMonth = '0' + currentMonth;
        }
        var formatDate = date.getFullYear() + '-' + currentMonth + '-' + currentdate;
        return formatDate;
    }

    function tong_vt(id) {
        var sl = Number($(id).val());
        var dg = $(id).parents(".table_3").find('.don_gia_3').text();
        var don_gia = dg.replaceAll('.','');
        var tong = sl * don_gia;
        var thanh_tien = formatNumber(tong);
        $(id).parents(".table_3").find('.thanh_tien_3').text(thanh_tien);
    };

    function oninput_down(_this){
        _this.value = _this.value.replace(/[^0-9.]/g, ''); 
        _this.value = _this.value.replace(/(\..*)\./g, '$1');
    }

    function change_vt(_this){
        var value = $(_this).val();
        __this = $(_this);
        var value1 = $('#select_wh_in_4').val();
        var id_cty = $('.nguoi_tao').attr('data-ct');
        $.ajax({
            url: '../ajax/get_dsvt_kho_nk.php',
            type: 'POST',
            data: {
                id_vt: value,
                id_kho: value1,
                id_cty: id_cty
            },
            success: function(data) {
                __this.parent().parent().html(data);
            }
        })
    }

    function change_value() {
        $(".select_tb_3").change(function() {
            var value = $(this).val();
            var _this = $(this);
            var value1 = $('#select_wh_in_3').val()
            var id_cty = $('.nguoi_tao').attr('data-ct');
            $.ajax({
                url: '../ajax/get_dsvt_kho.php',
                type: 'POST',
                data: {
                    id_vt: value,
                    id_kho: value1,
                    id_cty: id_cty
                },
                success: function(data) {
                    _this.parent().parent().html(data);
                }
            })
        })
    }

    // Nhập điều chuyển
    $('.select_transfer_slip').change(function() {
        var value = $(this).val()
        var id_cty = $('.nguoi_tao').attr('data-ct');
        $('.kho_xuat_2').val(pdc[value].kho_xuat_name)
        $('.id_kho_xuat_2').val(pdc[value].kcxl_khoXuat)
        $('.kho_nhap_2').val(pdc[value].kho_nhap_name)
        $('.id_kho_nhap_2').val(pdc[value].kcxl_khoNhap)
        $('.nguoi_giao_2').val(data[pdc[value].kcxl_nguoiThucHien].ep_name)
        $('.id_nguoi_giao_2').val(pdc[value].kcxl_nguoiThucHien)
        $('.phong_ban_giao_2').val(data[pdc[value].kcxl_nguoiThucHien].dep_name)
        $('.id_phong_ban_giao_2').val(data[pdc[value].kcxl_nguoiThucHien].dep_id)
        $('.nguoi_nhan_2').val(data[pdc[value].kcxl_nguoiNhan].ep_name)
        $('.id_nguoi_nhan_2').val(pdc[value].kcxl_nguoiNhan)
        $('.phong_ban_nhan_2').val(data[pdc[value].kcxl_nguoiNhan].dep_name)
        $('.id_phong_ban_nhan_2').val(pdc[value].kcxl_phongBanNguoiNhan)
        $('.date_yc_ht_2').val(pdc[value].kcxl_ngayYeuCauHoanThanh)

        $.ajax({
            url: '../ajax/get_dsvt_pdc.php',
            type: 'POST',
            dataType: 'Json',
            data: {
                id_phieu_pdc: value,
                id_cty: id_cty
            },
            success: function(response) {
                var text = $('.data_table_2').text('');
                var stt = 1;
                var html = '';
                for (index in response) {
                    html += `	 <tr class="color_grey font_s14 line_h16 font_w400 table_3">
 											<td>` + stt++ + `</td>
 											<td>VT - ` + response[index].id + `</td>
 											<td style="text-align: left;">
 												<a href="#" class="color_blue font_w500 id_vt" data ="` + response[index].id + `">` + arr_dsvt_tb[response[index].id].dsvt_name + `</a>
 											</td>
 											<td>` + arr_dsvt_tb[response[index].id].dvt_name + `</td>
 											<td style="text-align: left;">` + arr_dsvt_tb[response[index].id].hsx_name + `</td>
 											<td>` + arr_dsvt_tb[response[index].id].xx_name + `</td>
 											<td style="text-align: right; background: #EEEEEE;">
											 ` + response[index].soluong + `
 											</td>
 											<td>
 												<input class="nhap_so_luong_1" style="border: none; text-align: right; outline: none" type="text" placeholder="Nhập số lượng" onkeyup="tong_vt(this)" oninput="oninput_down(this)">
 											</td>
 											<td style="text-align: right;" class="don_gia_3">` + arr_dsvt_tb[response[index].id].dsvt_donGia + `</td>
 											<td style="text-align: right;" class="thanh_tien_3">0
 											</td>
 										</tr>`;
                }
                $('.data_table_2').append(html);
            }
        })
    })

    // Nhập trả lại từ thi công
    $('#select_shipper_wh_3').change(function() {
        var value = $(this).val();
        $('.phong_giao_3').val(data[value].dep_name)
        $('.id_phong_giao_3').val(data[value].dep_id)
    });

    $('#select_receiver_wh_3').change(function() {
        var value = $(this).val();
        $('.phong_ban_nhan_3').val(data[value].dep_name)
        $('.id_phong_ban_nhan_3').val(data[value].dep_id)
    });
    
    $('#select_wh_in_3').change(function() {
        $('.delete_3').remove()
        var value = $(this).val()
        var id_cty = $('.nguoi_tao').attr('data-ct');
        $.ajax({
            url: '../ajax/get_dsvt_kho.php',
            type: 'POST',
            // dataType: 'Json',
            data: {
                id_kho: value,
                id_cty: id_cty
            },
            success: function(response) {}
        });
    });

    $('.add_row_wh').click(function() {
        var value = $('#select_wh_in_3').val();
        var id_cty = $('.nguoi_tao').attr('data-ct');
        if(value != ""){
            $.ajax({
                url: '../ajax/get_dsvt_kho.php',
                type: 'POST',
                data: {
                    id_kho: value,
                    id_cty: id_cty
                },
                success: function(data) {
                    if (data != 'jkkjk') {
                        $(".table_list_meterial-3 #data_table_3").append(data);
                    } else {
                        $(".table_list_meterial-3 #data_table_3").append();
                    }
                }
            })
        }
    });

    $('#select_shipper_dis_5').change(function() {
        var value = $(this).val()
        $('.id_phong_giao_5').val(data[value].dep_id)
        $('.phong_giao_5').val(data[value].dep_name)
    });

    $('.select_construction').change(function(){
        var id_cong_trinh = $(this).val();
        var id_cty = $('.nguoi_tao').attr('data-ct');
        $.ajax({
            url: "../render/kho_ct.php",
            type: "POST",
            data:{
                id_cty: id_cty,
                id_cong_trinh: id_cong_trinh
            },
            success: function(data){
                $("select[name='select_wh_in_3']").html(data);
            }

        })
    });

    // Vật tư theo yêu cầu
    $('#rq_note_5').change(function() {
        var value = $(this).val()
        var id_cty = $('.nguoi_tao').attr('data-ct'); 
        if(pyc[value].role == 2){
            $('.nguoi_nhan_5').val(data[pyc[value].id_nguoi_yc].ep_name);
            $('.id_nguoi_nhan_5').val(pyc[value].id_nguoi_yc);
            $('.phong_nhan_5').val(data[pyc[value].id_nguoi_yc].dep_name);
            $('.id_phong_nhan_5').val(data[pyc[value].id_nguoi_yc].dep_id);
        }else if(pyc[value].role == 1){
            $('.nguoi_nhan_5').val(tt_user.com_name);
            $('.id_nguoi_nhan_5').val('0');
            $('.phong_nhan_5').val('');
            $('.id_phong_nhan_5').val('');
        }
        $.ajax({
            url: '../render/vattu_nhap_theoYC.php',
            type: 'POST',
            data: {
                id_phieu_yc: value,
                id_cty: id_cty,
            },
            success: function(data) {
                $('#table_input_wh_5 .main_table').html(data);
            }
        })
    });

    // Nhập khác
    $('#select_staff_4').change(function() {
        var value = $(this).val()
        $('.phong_giao_4 ').val(data[value].dep_name)
        $('.id_phong_giao_4 ').val(data[value].dep_id)
    });

    $('#select_wh_in_4').change(function() {
        $('.delete_3').remove()
        var value = $(this).val()
        var id_cty = $('.nguoi_tao').attr('data-ct');
        $.ajax({
            url: '../ajax/get_dsvt_kho_nk.php',
            type: 'POST',
            // dataType: 'Json',
            data: {
                id_kho: value,
                id_cty: id_cty
            },
            success: function(response) {
            }
        });
    })

    $('.add_row_wh_4').click(function() {
        var value = $('#select_wh_in_4').val()
        var id_cty = $('.nguoi_tao').attr('data-ct');
        if(value != ""){
            $.ajax({
                url: '../ajax/get_dsvt_kho_nk.php',
                type: 'POST',
                data: {
                    id_kho: value,
                    id_cty: id_cty
                },
                success: function(data) {
                    if (data != 'jkkjk') {
                        $(".table_list_meterial-3 #data_table_4").append(data);
                    } else {
                        $(".table_list_meterial-3 #data_table_4").append();
                    }
                }
            })
        }
    });

    // Vật tư theo đơn hàng
    $("select[name='order_import_wh_cre']").change(function(){
        var id_dh = $(this).val();
        var id_cty = $('.nguoi_tao').attr('data-ct');

        $("input[name='nha_cung_cap']").val(dh[id_dh].ten_nha_cc_kh);
        $("input[name='nguoi_nhan']").val(data[dh[id_dh].nguoi_nhan_hang].ep_name);
        $("input[name='id_nguoi_nhan']").val(dh[id_dh].nguoi_nhan_hang);
        $("input[name='phong_ban']").val(data[dh[id_dh].nguoi_nhan_hang].dep_name);
        $("input[name='id_pb_nguoi_nhan']").val(data[dh[id_dh].nguoi_nhan_hang].dep_id)

        $.ajax({
            url: "../render/vattu_nhap_Theodh.php",
            type: "POST",
            data:{
                id_cty: id_cty,
                id_dh: id_dh,
            },
            success: function(data){
                $('.table_list_meterial').html(data);
            }
        });

    });

    // Lưu phiếu nhập
    $('.btn_nhapKhoCreate').click(function() {
        var form_add = $("#f_import_wh_create");
        var id_cty = $('.nguoi_tao').attr('data-ct');
        resetFormValidator(form_add);
        var value = $('.select_import_form').val();
        
        if (value == 'NK1') {
            var listTable = [];
            $(".nhap_so_luong").each(function() {
                var data = $(this).parent().parent().attr('data-id');
                var soluong = $(this).val();
                listTable.push({
                    'id': data,
                    'soluong': soluong,
                });
            });
            
            var check_empty_arr = [];
            for(var i = 0; i< listTable.length; i++){
                var check_empty = listTable[i].soluong;
                if(check_empty == ""){
                    check_empty_arr.push(listTable[i].soluong);
                }
            }
            var length_arr_dsvt = listTable.length;
            var length_soluong_empty = check_empty_arr.length;


            if ($('.select_status').val() == '1') {
            form_add.validate({
                    errorPlacement: function(error, element) {
                        error.appendTo(element.parents(".order_import_wh_cre"));
                        error.appendTo(element.parents(".kho_nhap "));
                        error.appendTo(element.parents(".ngay_nhap "));
                        error.wrap("<span class='error'>")
                    },
                    rules: {
                        order_import_wh_cre: "required",
                        wh_imp: "required",
                        ngayNhapKho: "required",
                    },
                    messages: {
                        order_import_wh_cre: "Vui lòng chọn đơn hàng",
                        wh_imp: "Vui lòng chọn kho nhập",
                        ngayNhapKho: "Vui lòng chọn ngày nhập kho",
                    }, 
                });
                if(form_add.valid() === true && length_soluong_empty == 0){
                    var kcxl_hinhThuc = $('select[name="select_import_form"]').val();
                    var kcxl_nguoiTao = $('.nguoi_tao').attr('data');
                    var kcxl_ngayTao = $('input[name="ngay_tao"]').val();
                    var kcxl_donHang = $('select[name="order_import_wh_cre"]').val();
                    var kcxl_trangThai = $('select[name="select_status"]').val();
                    var kcxl_nhaCungCap = $('input[name="nha_cung_cap"]').val();
                    var kcxl_nguoiThucHien = $('input[name="nguoiGiao"]').val();
                    var kcxl_nguoiNhan = $('input[name="id_nguoi_nhan"]').val();
                    var kcxl_phongBanNguoiNhan = $('input[name="id_pb_nguoi_nhan"]').val();
                    var kcxl_khoNhap = $('select[name="wh_imp"]').val();
                    var kcxl_ngayNhapKho = $('input[name ="ngayNhapKho"]').val();
                    var kcxl_ghi_chu = $('textarea[name="ghi_chu"]').val();
                    var listTable = JSON.stringify(listTable);
                    
                    $.ajax({
                        url: '../ajax/add_new_pnk.php',
                        type: 'POST',
                        // dataType: 'Json',
                        data: {
                            kcxl_hinhThuc: kcxl_hinhThuc,
                            kcxl_nguoiTao: kcxl_nguoiTao,
                            kcxl_ngayTao: kcxl_ngayTao,
                            kcxl_donHang: kcxl_donHang,
                            kcxl_trangThai: kcxl_trangThai,
                            kcxl_nhaCungCap: kcxl_nhaCungCap,
                            kcxl_nguoiThucHien: kcxl_nguoiThucHien,
                            kcxl_nguoiNhan: kcxl_nguoiNhan,
                            kcxl_phongBanNguoiNhan: kcxl_phongBanNguoiNhan,
                            kcxl_khoNhap: kcxl_khoNhap,
                            kcxl_ngayNhapKho: kcxl_ngayNhapKho,
                            kcxl_ghi_chu: kcxl_ghi_chu,
                            listTable: listTable,
                            id_cty: id_cty,
                        },
                        success: function(data) {
                            $('.popup_add_phieuNhap').show();
                            var text = $('#popup_add_phieuNhap .p_add_succ').text('');
                            var text_new = '';
                            text_new += 'Thêm mới phiếu nhập kho theo đơn hàng thành công!';
                            text.append(text_new);
                        }
                    });
                }else{
                    alert("Vui lòng nhập đầy đủ thông tin!!!");
                }
            }
            if ($('.select_status').val() == '4') {
                form_add.validate({
                    errorPlacement: function(error, element) {
                        error.appendTo(element.parents(".order_import_wh_cre"));
                        error.appendTo(element.parents(".date_accses"));
                        error.appendTo(element.parents(".kho_nhap"));
                        error.appendTo(element.parents(".ngay_nhap"));
                        error.wrap("<span class='error'>")
                    },
                    rules: {
                        order_import_wh_cre: "required",
                        wh_imp: "required",
                        ngayNhapKho: "required",
                        ngay_hoanThanh: "required",
                    },
                    messages: {
                        order_import_wh_cre: "Vui lòng chọn đơn hàng",
                        wh_imp: "Vui lòng chọn kho nhập",
                        ngayNhapKho: "Vui lòng chọn ngày nhập kho",
                        ngay_hoanThanh: "Vui lòng chọn ngày hoàn thành",
                    }, 
                });
                if(form_add.valid() === true && length_soluong_empty == 0){
                    var kcxl_hinhThuc = $('select[name="select_import_form"]').val();
                    var kcxl_nguoiTao = $('.nguoi_tao').attr('data');
                    var kcxl_ngayTao = $('input[name="ngay_tao"]').val();
                    var kcxl_donHang = $('select[name="order_import_wh_cre"]').val();
                    var kcxl_trangThai = $('select[name="select_status"]').val();
                    var kcxl_ngayHoanThanh = $('input[name="ngay_hoanThanh"]').val();
                    var kcxl_nhaCungCap = $('input[name="nha_cung_cap"]').val();
                    var kcxl_nguoiThucHien = $('input[name="nguoiGiao"]').val();
                    var kcxl_nguoiNhan = $('input[name="id_nguoi_nhan"]').val();
                    var kcxl_phongBanNguoiNhan = $('input[name="id_pb_nguoi_nhan"]').val();
                    var kcxl_khoNhap = $('select[name="wh_imp"]').val();
                    var kcxl_ngayNhapKho = $('input[name ="ngayNhapKho"]').val();
                    var kcxl_ghi_chu = $('textarea[name="ghi_chu"]').val();
                    var listTable = JSON.stringify(listTable);
                    
                    $.ajax({
                        url: '../ajax/add_new_pnk.php',
                        type: 'POST',
                        // dataType: 'Json',
                        data: {
                            kcxl_hinhThuc: kcxl_hinhThuc,
                            kcxl_nguoiTao: kcxl_nguoiTao,
                            kcxl_ngayTao: kcxl_ngayTao,
                            kcxl_donHang: kcxl_donHang,
                            kcxl_trangThai: kcxl_trangThai,
                            kcxl_nhaCungCap: kcxl_nhaCungCap,
                            kcxl_nguoiThucHien: kcxl_nguoiThucHien,
                            kcxl_nguoiNhan: kcxl_nguoiNhan,
                            kcxl_phongBanNguoiNhan: kcxl_phongBanNguoiNhan,
                            kcxl_khoNhap: kcxl_khoNhap,
                            kcxl_ngayNhapKho: kcxl_ngayNhapKho,
                            kcxl_ghi_chu: kcxl_ghi_chu,
                            listTable: listTable,
                            id_cty: id_cty,
                        },
                        success: function(data) {
                            $('.popup_add_phieuNhap').show();
                            var text = $('#popup_add_phieuNhap .p_add_succ').text('');
                            var text_new = '';
                            text_new += 'Thêm mới phiếu nhập kho theo đơn hàng thành công!';
                            text.append(text_new);
                        }
                    });
                }else{
                    alert("Vui lòng nhập đầy đủ thông tin!!!");
                }
            }
        }

        if (value == 'NK2') {
            var listTable = [];
            $(".nhap_so_luong_1").each(function() {
                var data = $(this).parent().parent().find($('.id_vt')).attr('data')
                var soluong = $(this).val()
                listTable.push({
                    'id': data,
                    'soluong': soluong,
                })
            })
            if ($('#select_status_2').val() == '1') {
                form_add.validate({
                    errorPlacement: function(error, element) {
                        error.appendTo(element.parents(".wh_transfer_note"));
                        error.appendTo(element.parents(".data_nhap_2"));
                        error.wrap("<span class='error'>")
                    },
                    rules: {
                        select_transfer_slip: "required",
                        ngay_nhap_2: "required",
                    },
                    messages: {
                        select_transfer_slip: "Vui lòng chọn phiếu điều chuyển kho",
                        ngay_nhap_2: "Vui lòng chọn ngày nhập kho",
                    },
                });
                if (form_add.valid() === true) {
                    var kcxl_hinhThuc = $('select[name="select_import_form"]').val()
                    var kcxl_nguoiTao = $('.nguoi_tao').attr('data')
                    var kcxl_ngayTao = $('.ngay_tao').val()
                    var kcxl_phieuDieuChuyenKho = $('select[name="select_transfer_slip"]').val()
                    var kcxl_trangThai = $('select[name="select_status_2"]').val()
                    var kcxl_ngayYeuCauHoanThanh = $('.date_yc_ht_2').val()
                    var kcxl_nguoiThucHien = $('.id_nguoi_giao_2').val()
                    var kcxl_phongBanNguoiGiao = $('.id_phong_ban_giao_2').val()
                    var kcxl_nguoiNhan = $('.id_nguoi_nhan_2').val()
                    var kcxl_phongBanNguoiNhan = $('.id_phong_ban_nhan_2').val()
                    var kcxl_khoXuat = $('.id_kho_xuat_2').val()
                    var kcxl_khoNhap = $('.id_kho_nhap_2').val()
                    var kcxl_ngayNhapKho = $('input[name = "ngay_nhap_2"]').val()
                    var kcxl_ghi_chu = $('textarea[name="ghi_chu"]').val()
                    var listTable = JSON.stringify(listTable);
                    id_cty
                    $.ajax({
                        url: '../ajax/add_new_pnk.php',
                        type: 'POST',
                        // dataType: 'Json',
                        data: {
                            kcxl_hinhThuc: kcxl_hinhThuc,
                            kcxl_nguoiTao: kcxl_nguoiTao,
                            kcxl_ngayTao: kcxl_ngayTao,
                            kcxl_phieuDieuChuyenKho: kcxl_phieuDieuChuyenKho,
                            kcxl_trangThai: kcxl_trangThai,
                            kcxl_ngayYeuCauHoanThanh: kcxl_ngayYeuCauHoanThanh,
                            kcxl_nguoiThucHien: kcxl_nguoiThucHien,
                            kcxl_phongBanNguoiGiao: kcxl_phongBanNguoiGiao,
                            kcxl_nguoiNhan: kcxl_nguoiNhan,
                            kcxl_phongBanNguoiNhan: kcxl_phongBanNguoiNhan,
                            kcxl_khoXuat: kcxl_khoXuat,
                            kcxl_khoNhap: kcxl_khoNhap,
                            kcxl_ngayNhapKho: kcxl_ngayNhapKho,
                            kcxl_ghi_chu: kcxl_ghi_chu,
                            listTable: listTable,
                            id_cty: id_cty,
                        },
                        success: function(data) {
                            $('.popup_add_phieuNhap').show();
                            var text = $('#popup_add_phieuNhap .p_add_succ').text('');
                            var text_new = '';
                            text_new += 'Thêm mới phiếu nhập kho theo điều chuyển thành công!';
                            text.append(text_new);
                        }
                    })
                }
            }
            if ($('#select_status_2').val() == '4') {
                form_add.validate({
                    errorPlacement: function(error, element) {
                        error.appendTo(element.parents(".wh_transfer_note"));
                        error.appendTo(element.parents(".data_nhap_2"));
                        error.appendTo(element.parents(".date_accses_2"));
                        error.wrap("<span class='error'>")
                    },
                    rules: {
                        select_transfer_slip: "required",
                        ngay_nhap_2: "required",
                        ngay_ht_2: "required",
                    },
                    messages: {
                        select_transfer_slip: "Vui lòng chọn phiếu điều chuyển kho",
                        ngay_nhap_2: "Vui lòng chọn ngày nhập kho",
                        ngay_ht_2: "Vui lòng chọn ngày hoàn thành",
                    },
                });
                if (form_add.valid() === true) {
                    var kcxl_hinhThuc = $('select[name="select_import_form"]').val()
                    var kcxl_nguoiTao = $('.nguoi_tao').attr('data')
                    var kcxl_ngayTao = $('.ngay_tao').val()
                    var kcxl_phieuDieuChuyenKho = $('select[name="select_transfer_slip"]').val()
                    var kcxl_trangThai = $('select[name="select_status_2"]').val()
                    var kcxl_ngayYeuCauHoanThanh = $('.date_yc_ht_2').val()
                    var kcxl_nguoiThucHien = $('.id_nguoi_giao_2').val()
                    var kcxl_phongBanNguoiGiao = $('.id_phong_ban_giao_2').val()
                    var kcxl_nguoiNhan = $('.id_nguoi_nhan_2').val()
                    var kcxl_phongBanNguoiNhan = $('.id_phong_ban_nhan_2').val()
                    var kcxl_khoXuat = $('.id_kho_xuat_2').val()
                    var kcxl_khoNhap = $('.id_kho_nhap_2').val()
                    var kcxl_ngayNhapKho = $('input[name = "ngay_nhap_2"]').val()
                    var kcxl_ghi_chu = $('textarea[name="ghi_chu"]').val()
                    var kcxl_ngayHoanThanh = $('.ngay_ht_2').val()
                    var listTable = JSON.stringify(listTable);
                    id_cty
                    $.ajax({
                        url: '../ajax/add_new_pnk.php',
                        type: 'POST',
                        // dataType: 'Json',
                        data: {
                            kcxl_hinhThuc: kcxl_hinhThuc,
                            kcxl_nguoiTao: kcxl_nguoiTao,
                            kcxl_ngayTao: kcxl_ngayTao,
                            kcxl_phieuDieuChuyenKho: kcxl_phieuDieuChuyenKho,
                            kcxl_trangThai: kcxl_trangThai,
                            kcxl_ngayYeuCauHoanThanh: kcxl_ngayYeuCauHoanThanh,
                            kcxl_nguoiThucHien: kcxl_nguoiThucHien,
                            kcxl_phongBanNguoiGiao: kcxl_phongBanNguoiGiao,
                            kcxl_nguoiNhan: kcxl_nguoiNhan,
                            kcxl_phongBanNguoiNhan: kcxl_phongBanNguoiNhan,
                            kcxl_khoXuat: kcxl_khoXuat,
                            kcxl_khoNhap: kcxl_khoNhap,
                            kcxl_ngayNhapKho: kcxl_ngayNhapKho,
                            kcxl_ghi_chu: kcxl_ghi_chu,
                            kcxl_ngayHoanThanh: kcxl_ngayHoanThanh,
                            listTable: listTable,
                            id_cty: id_cty,
                        },
                        success: function(data) {
                            $('.popup_add_phieuNhap').show();
                            var text = $('#popup_add_phieuNhap .p_add_succ').text('');
                            var text_new = '';
                            text_new += 'Thêm mới phiếu nhập kho theo điều chuyển thành công!';
                            text.append(text_new);
                        }
                    })
                }
            }
        }

        if (value == 'NK3') {
            var listTable = []
            $(".nhap_sl_3").each(function() {
                var data = $(this).parent().parent().find($('.select_tb_3')).val()
                var soluong = $(this).val()
                if(data != "" && soluong != ""){
                    listTable.push({
                        'id': data,
                        'soluong': soluong,
                    });
                }
            })
            if ($('#select_status_3').val() == '1') {
                form_add.validate({
                    errorPlacement: function(error, element) {
                        error.appendTo(element.parents(".choice_construction"));
                        error.appendTo(element.parents(".tb_kn_3"));
                        error.appendTo(element.parents(".tb_date_nk_3"));
                        error.wrap("<span class='error'>")
                    },
                    rules: {
                        select_construction: "required",
                        select_wh_in_3: "required",
                        ngay_nk_3: "required",
                    },
                    messages: {
                        select_construction: "Vui lòng chọn công trình",
                        select_wh_in_3: "Vui lòng chọn kho nhập",
                        ngay_nk_3: "Vui lòng chọn ngày nhập kho",
                    },
                });
                if (form_add.valid() === true && listTable.length > 0) {
                    var kcxl_hinhThuc = $('select[name="select_import_form"]').val();
                    var kcxl_nguoiTao = $('.nguoi_tao').attr('data');
                    var kcxl_ngayTao = $('.ngay_tao').val();
                    var kcxl_congTrinh = $('select[name="select_construction"]').val();
                    var kcxl_trangThai = $('select[name="select_status_3"]').val();
                    var kcxl_nguoiThucHien = $('select[name="select_shipper_wh_3"]').val();
                    var kcxl_phongBanNguoiGiao = $('.id_phong_giao_3').val();
                    var kcxl_nguoiNhan = $('select[name="select_receiver_wh_3"]').val();
                    var kcxl_phongBanNguoiNhan = $('.id_phong_ban_nhan_3').val();
                    var kcxl_khoNhap = $('select[name="select_wh_in_3"]').val();
                    var kcxl_ngayNhapKho = $('input[name = "ngay_nk_3"]').val();
                    var kcxl_ghi_chu = $('textarea[name="ghi_chu"]').val();
                    var listTable = JSON.stringify(listTable);

                    $.ajax({
                        url: '../ajax/add_new_pnk.php',
                        type: 'POST',
                        // dataType: 'Json',
                        data: {
                            kcxl_hinhThuc: kcxl_hinhThuc,
                            kcxl_nguoiTao: kcxl_nguoiTao,
                            kcxl_ngayTao: kcxl_ngayTao,
                            kcxl_congTrinh: kcxl_congTrinh,
                            kcxl_trangThai: kcxl_trangThai,
                            kcxl_nguoiThucHien: kcxl_nguoiThucHien,
                            kcxl_phongBanNguoiGiao: kcxl_phongBanNguoiGiao,
                            kcxl_nguoiNhan: kcxl_nguoiNhan,
                            kcxl_phongBanNguoiNhan: kcxl_phongBanNguoiNhan,
                            kcxl_khoNhap: kcxl_khoNhap,
                            kcxl_ngayNhapKho: kcxl_ngayNhapKho,
                            kcxl_ghi_chu: kcxl_ghi_chu,
                            listTable: listTable,
                            id_cty: id_cty,
                        },
                        success: function(data) {
                            $('.popup_add_phieuNhap').show();
                            var text = $('#popup_add_phieuNhap .p_add_succ').text('');
                            var text_new = '';
                            text_new += 'Thêm mới phiếu nhập kho trả lại từ thi công thành công!';
                            text.append(text_new);
                        }
                    })
                }else{
                    alert("Vui lòng nhập đầy đủ thông tin!!!");
                }
            }
            if ($('#select_status_3').val() == '4') {
                form_add.validate({
                    errorPlacement: function(error, element) {
                        error.appendTo(element.parents(".choice_construction"));
                        error.appendTo(element.parents(".tb_kn_3"));
                        error.appendTo(element.parents(".tb_date_nk_3"));
                        error.appendTo(element.parents(".tb_date_ht_3"));
                        error.wrap("<span class='error'>")
                    },
                    rules: {
                        select_construction: "required",
                        select_wh_in_3: "required",
                        ngay_nk_3: "required",
                        date_ht_3: "required",
                    },
                    messages: {
                        select_construction: "Vui lòng chọn công trình",
                        select_wh_in_3: "Vui lòng chọn kho nhập",
                        ngay_nk_3: "Vui lòng chọn ngày nhập kho",
                        date_ht_3: "Vui lòng chọn ngày hoàn thành",
                    },
                });
                if (form_add.valid() === true && listTable.length > 0) {
                    var kcxl_hinhThuc = $('select[name="select_import_form"]').val();
                    var kcxl_nguoiTao = $('.nguoi_tao').attr('data');
                    var kcxl_ngayTao = $('.ngay_tao').val();
                    var kcxl_congTrinh = $('select[name="select_construction"]').val();
                    var kcxl_trangThai = $('select[name="select_status_3"]').val();
                    var kcxl_ngayHoanThanh = $('.date_ht_3').val();
                    var kcxl_nguoiThucHien = $('select[name="select_shipper_wh_3"]').val();
                    var kcxl_phongBanNguoiGiao = $('.id_phong_giao_3').val();
                    var kcxl_nguoiNhan = $('select[name="select_receiver_wh_3"]').val();
                    var kcxl_phongBanNguoiNhan = $('.id_phong_ban_nhan_3').val();
                    var kcxl_khoNhap = $('select[name="select_wh_in_3"]').val();
                    var kcxl_ngayNhapKho = $('input[name = "ngay_nk_3"]').val();
                    var kcxl_ghi_chu = $('textarea[name="ghi_chu"]').val();
                    var listTable = JSON.stringify(listTable);
                    id_cty
                    $.ajax({
                        url: '../ajax/add_new_pnk.php',
                        type: 'POST',
                        // dataType: 'Json',
                        data: {
                            kcxl_hinhThuc: kcxl_hinhThuc,
                            kcxl_nguoiTao: kcxl_nguoiTao,
                            kcxl_ngayTao: kcxl_ngayTao,
                            kcxl_congTrinh: kcxl_congTrinh,
                            kcxl_trangThai: kcxl_trangThai,
                            kcxl_ngayHoanThanh: kcxl_ngayHoanThanh,
                            kcxl_nguoiThucHien: kcxl_nguoiThucHien,
                            kcxl_phongBanNguoiGiao: kcxl_phongBanNguoiGiao,
                            kcxl_nguoiNhan: kcxl_nguoiNhan,
                            kcxl_phongBanNguoiNhan: kcxl_phongBanNguoiNhan,
                            kcxl_khoNhap: kcxl_khoNhap,
                            kcxl_ngayNhapKho: kcxl_ngayNhapKho,
                            kcxl_ghi_chu: kcxl_ghi_chu,
                            listTable: listTable,
                            id_cty: id_cty,
                        },
                        success: function(data) {
                            $('.popup_add_phieuNhap').show();
                            var text = $('#popup_add_phieuNhap .p_add_succ').text('');
                            var text_new = '';
                            text_new += 'Thêm mới phiếu nhập kho trả lại từ thi công thành công!';
                            text.append(text_new);
                        }
                    })
                }else{
                    alert("Vui lòng nhập đầy đủ thông tin!!!");
                }
            }
        }

        if (value == 'NK4') {
            var listTable = [];
            $(".nhap_so_luong").each(function() {
                var data = $(this).parent().parent().attr('data-id');
                var soluong = $(this).val();
                listTable.push({
                    'id': data,
                    'soluong': soluong,
                });
            });
            
            var check_empty_arr = [];
            for(var i = 0; i< listTable.length; i++){
                var check_empty = listTable[i].soluong;
                if(check_empty == ""){
                    check_empty_arr.push(listTable[i].soluong);
                }
            }
            var length_arr_dsvt = listTable.length;
            var length_soluong_empty = check_empty_arr.length;

            if ($('#select_status_5').val() == '1') {
                form_add.validate({
                    errorPlacement: function(error, element) {
                        error.appendTo(element.parents(".tb_rq_note_5"));
                        error.appendTo(element.parents(".tb_select_wh_in_5"));
                        error.appendTo(element.parents(".tb_date_nk_5"));
                        error.wrap("<span class='error'>")
                    },
                    rules: {
                        rq_note_5: "required",
                        select_wh_in_5: "required",
                        date_nk_5: "required",
                    },
                    messages: {
                        rq_note_5: "Vui lòng chọn phiếu yêu cầu",
                        select_wh_in_5: "Vui lòng chọn kho nhập",
                        date_nk_5: "Vui lòng chọn kho nhập",
                    },
                });
                if (form_add.valid() === true && length_soluong_empty == 0) {
                    var kcxl_hinhThuc = $('select[name="select_import_form"]').val();
                    var kcxl_nguoiTao = $('.nguoi_tao').attr('data');
                    var kcxl_ngayTao = $('.ngay_tao').val();
                    var kcxl_phieuYeuCau = $('select[name="rq_note_5"]').val();
                    var kcxl_trangThai = $('select[name="select_status_5"]').val();
                    var kcxl_nguoiThucHien = $('select[name="select_shipper_dis_5"]').val();
                    var kcxl_phongBanNguoiGiao = $('.id_phong_giao_5').val();
                    var kcxl_nguoiNhan = $('.id_nguoi_nhan_5').val();
                    var kcxl_phongBanNguoiNhan = $('.id_phong_nhan_5').val();
                    var kcxl_khoNhap = $('select[name="select_wh_in_5"]').val();
                    var kcxl_ngayNhapKho = $('input[name = "date_nk_5"]').val();
                    var kcxl_ghi_chu = $('textarea[name="ghi_chu"]').val();
                    var listTable = JSON.stringify(listTable);
                    $.ajax({
                        url: '../ajax/add_new_pnk.php',
                        type: 'POST',
                        // dataType: 'Json',
                        data: {
                            kcxl_hinhThuc: kcxl_hinhThuc,
                            kcxl_nguoiTao: kcxl_nguoiTao,
                            kcxl_ngayTao: kcxl_ngayTao,
                            kcxl_phieuYeuCau: kcxl_phieuYeuCau,
                            kcxl_trangThai: kcxl_trangThai,
                            kcxl_nguoiThucHien: kcxl_nguoiThucHien,
                            kcxl_phongBanNguoiGiao: kcxl_phongBanNguoiGiao,
                            kcxl_nguoiNhan: kcxl_nguoiNhan,
                            kcxl_phongBanNguoiNhan: kcxl_phongBanNguoiNhan,
                            kcxl_khoNhap: kcxl_khoNhap,
                            kcxl_ngayNhapKho: kcxl_ngayNhapKho,
                            kcxl_ghi_chu: kcxl_ghi_chu,
                            listTable: listTable,
                            id_cty: id_cty,
                        },
                        success: function(data) {
                            $('.popup_add_phieuNhap').show();
                            var text = $('#popup_add_phieuNhap .p_add_succ').text('');
                            var text_new = '';
                            text_new += 'Thêm mới phiếu nhập kho theo yêu cầu vật tư thành công!';
                            text.append(text_new);
                        }
                    })
                }else{
                    alert("Vui lòng nhập đầy đủ thông tin!")
                }
            }
            if ($('#select_status_5').val() == '4') {
                var kcxl_hinhThuc = $('select[name="select_import_form"]').val();
                var kcxl_nguoiTao = $('.nguoi_tao').attr('data');
                var kcxl_ngayTao = $('.ngay_tao').val();
                var kcxl_phieuYeuCau = $('select[name="rq_note_5"]').val();
                var kcxl_trangThai = $('select[name="select_status_5"]').val();
                var kcxl_ngayHoanThanh = $('input[name="date_ht_5"]').val();
                var kcxl_nguoiThucHien = $('select[name="select_shipper_dis_5"]').val();
                var kcxl_phongBanNguoiGiao = $('.id_phong_giao_5').val();
                var kcxl_nguoiNhan = $('.id_nguoi_nhan_5').val();
                var kcxl_phongBanNguoiNhan = $('.id_phong_nhan_5').val();
                var kcxl_khoNhap = $('select[name="select_wh_in_5"]').val();
                var kcxl_ngayNhapKho = $('input[name = "date_nk_5"]').val();
                var kcxl_ghi_chu = $('textarea[name="ghi_chu"]').val();
                var listTable = JSON.stringify(listTable);
                form_add.validate({
                    errorPlacement: function(error, element) {
                        error.appendTo(element.parents(".tb_rq_note_5"));
                        error.appendTo(element.parents(".tb_select_wh_in_5"));
                        error.appendTo(element.parents(".tb_date_nk_5"));
                        error.appendTo(element.parents(".tb_date_ht_5"));
                        error.wrap("<span class='error'>")
                    },
                    rules: {
                        rq_note_5: "required",
                        select_wh_in_5: "required",
                        date_nk_5: "required",
                        date_ht_5: "required",
                    },
                    messages: {
                        rq_note_5: "Vui lòng chọn phiếu yêu cầu",
                        select_wh_in_5: "Vui lòng chọn ngày hoàn thành",
                        date_nk_5: "Vui lòng chọn kho nhập",
                        date_ht_5: "Vui lòng chọn ngày nhập kho",
                    },
                });
                if (form_add.valid() === true && length_soluong_empty == 0) {
                    $.ajax({
                        url: '../ajax/add_new_pnk.php',
                        type: 'POST',
                        // dataType: 'Json',
                        data: {
                            kcxl_hinhThuc: kcxl_hinhThuc,
                            kcxl_nguoiTao: kcxl_nguoiTao,
                            kcxl_ngayTao: kcxl_ngayTao,
                            kcxl_phieuYeuCau: kcxl_phieuYeuCau,
                            kcxl_trangThai: kcxl_trangThai,
                            kcxl_ngayHoanThanh: kcxl_ngayHoanThanh,
                            kcxl_nguoiThucHien: kcxl_nguoiThucHien,
                            kcxl_phongBanNguoiGiao: kcxl_phongBanNguoiGiao,
                            kcxl_nguoiNhan: kcxl_nguoiNhan,
                            kcxl_phongBanNguoiNhan: kcxl_phongBanNguoiNhan,
                            kcxl_khoNhap: kcxl_khoNhap,
                            kcxl_ngayNhapKho: kcxl_ngayNhapKho,
                            kcxl_ghi_chu: kcxl_ghi_chu,
                            listTable: listTable,
                            id_cty: id_cty,
                        },
                        success: function(data) {
                            $('.popup_add_phieuNhap').show();
                            var text = $('#popup_add_phieuNhap .p_add_succ').text('');
                            var text_new = '';
                            text_new += 'Thêm mới phiếu nhập kho theo yêu cầu vật tư thành công!';
                            text.append(text_new);
                        }
                    })
                }else{
                    alert("Vui lòng nhập đầy đủ thông tin!")
                }
            }
        }

        if (value == 'NK5') {
            var listTable = []
            $(".nhap_sl_3").each(function() {
                var data = $(this).parent().parent().find($('.select_tb_3')).val()
                var soluong = $(this).val()
                if(data != "" && soluong != ""){
                    listTable.push({
                        'id': data,
                        'soluong': soluong,
                    });
                }
            })

            if ($('#select_status_4').val() == '1') {
                form_add.validate({
                    errorPlacement: function(error, element) {
                        error.appendTo(element.parents(".tb_kn_4"));
                        error.appendTo(element.parents(".tb_date_imp_wh_4"));
                        error.wrap("<span class='error'>")
                    },
                    rules: {
                        select_wh_in_4: "required",
                        date_imp_wh_4: "required",
                    },
                    messages: {
                        select_wh_in_4: "Vui lòng chọn kho nhập",
                        date_imp_wh_4: "Vui lòng chọn ngày nhập kho",
                    },
                });
                if (form_add.valid() === true && listTable.length > 0) {
                    var kcxl_hinhThuc = $('select[name="select_import_form"]').val();
                    var kcxl_nguoiTao = $('.nguoi_tao').attr('data');
                    var kcxl_ngayTao = $('.ngay_tao').val();
                    var kcxl_trangThai = $('select[name="select_status_4"]').val();
                    var kcxl_nguoiThucHien = $('.nguoi_giao_5').val();
                    var kcxl_nguoiNhan = $('select[name="select_staff_4"]').val();
                    var kcxl_phongBanNguoiNhan = $('.id_phong_giao_4').val();
                    var kcxl_khoNhap = $('select[name="select_wh_in_4"]').val();
                    var kcxl_ngayNhapKho = $('input[name = "date_imp_wh_4"]').val();
                    var kcxl_ghi_chu = $('textarea[name="ghi_chu"]').val();
                    var listTable = JSON.stringify(listTable);
                    $.ajax({
                        url: '../ajax/add_new_pnk.php',
                        type: 'POST',
                        // dataType: 'Json',
                        data: {
                            kcxl_hinhThuc: kcxl_hinhThuc,
                            kcxl_nguoiTao: kcxl_nguoiTao,
                            kcxl_ngayTao: kcxl_ngayTao,
                            kcxl_trangThai: kcxl_trangThai,
                            kcxl_nguoiThucHien: kcxl_nguoiThucHien,
                            kcxl_nguoiNhan: kcxl_nguoiNhan,
                            kcxl_phongBanNguoiNhan: kcxl_phongBanNguoiNhan,
                            kcxl_khoNhap: kcxl_khoNhap,
                            kcxl_ngayNhapKho: kcxl_ngayNhapKho,
                            kcxl_ghi_chu: kcxl_ghi_chu,
                            listTable: listTable,
                            id_cty: id_cty,
                        },
                        success: function(data) {
                            $('.popup_add_phieuNhap').show();
                            var text = $('#popup_add_phieuNhap .p_add_succ').text('');
                            var text_new = '';
                            text_new += 'Thêm mới phiếu nhập kho thành công!';
                            text.append(text_new);
                        }
                    })
                }else{
                    alert("Vui lòng nhập đầy đủ thông tin!!!");
                }
            }
            if ($('#select_status_4').val() == '4') {
                var kcxl_hinhThuc = $('select[name="select_import_form"]').val();
                var kcxl_nguoiTao = $('.nguoi_tao').attr('data');
                var kcxl_ngayTao = $('.ngay_tao').val();
                var kcxl_trangThai = $('select[name="select_status_4"]').val();
                var kcxl_ngayHoanThanh = $('input[name="ngay_ht_4"]').val();
                var kcxl_nguoiThucHien = $('.nguoi_giao_5').val();
                var kcxl_nguoiNhan = $('select[name="select_staff_4"]').val();
                var kcxl_phongBanNguoiNhan = $('.id_phong_giao_4').val();
                var kcxl_khoNhap = $('select[name="select_wh_in_4"]').val();
                var kcxl_ngayNhapKho = $('input[name = "date_imp_wh_4"]').val();
                var kcxl_ghi_chu = $('textarea[name="ghi_chu"]').val();
                var listTable = JSON.stringify(listTable);
                form_add.validate({
                    errorPlacement: function(error, element) {
                        error.appendTo(element.parents(".tb_kn_4"));
                        error.appendTo(element.parents(".tb_date_imp_wh_4"));
                        error.appendTo(element.parents(".date_accses_4"));
                        error.wrap("<span class='error'>")
                    },
                    rules: {
                        select_wh_in_4: "required",
                        date_imp_wh_4: "required",
                        ngay_ht_4: "required",
                    },
                    messages: {
                        select_wh_in_4: "Vui lòng chọn kho nhập",
                        date_imp_wh_4: "Vui lòng chọn ngày nhập kho",
                        ngay_ht_4: "Vui lòng chọn ngày hoàn thành",
                    },
                });
                if (form_add.valid() === true && listTable.length > 0) {
                    $.ajax({
                        url: '../ajax/add_new_pnk.php',
                        type: 'POST',
                        // dataType: 'Json',
                        data: {
                            kcxl_hinhThuc: kcxl_hinhThuc,
                            kcxl_nguoiTao: kcxl_nguoiTao,
                            kcxl_ngayTao: kcxl_ngayTao,
                            kcxl_trangThai: kcxl_trangThai,
                            kcxl_ngayHoanThanh: kcxl_ngayHoanThanh,
                            kcxl_nguoiThucHien: kcxl_nguoiThucHien,
                            kcxl_nguoiNhan: kcxl_nguoiNhan,
                            kcxl_phongBanNguoiNhan: kcxl_phongBanNguoiNhan,
                            kcxl_khoNhap: kcxl_khoNhap,
                            kcxl_ngayNhapKho: kcxl_ngayNhapKho,
                            kcxl_ghi_chu: kcxl_ghi_chu,
                            listTable: listTable,
                            id_cty: id_cty,
                        },
                        success: function(data) {
                            $('.popup_add_phieuNhap').show();
                            var text = $('#popup_add_phieuNhap .p_add_succ').text('');
                            var text_new = '';
                            text_new += 'Thêm mới phiếu nhập kho thành công!';
                            text.append(text_new);
                        }
                    })
                }else{
                    alert("Vui lòng nhập đầy đủ thông tin!!!");
                }
            }
        }

    });