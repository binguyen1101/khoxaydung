var page = $(".detail_wh").attr("data-page");
var input_val = $(".detail_wh").attr("data-ip");
var curr = $(".detail_wh").attr("data-dis");

$.ajax({
    url: "../render/tb_nhom_vat_tu.php",
    type: "POST",
    data:{

        input_val: input_val,
        page: page,
        curr: curr
    },
    success: function(data){
        $(".detail_wh").append(data);
    }
});


$(".icon_sr_wh").click(function(){
    var input_val = $("input[name='input_search'").val();
    var page = 1;
    var curr = $('.show_tr_tb').val();
    
    if(input_val == "" && curr == 10){
        window.location.href = "/nhom-vat-tu-thiet-bi.html?dis=" + curr + '&page=' + page;
    }else{
        window.location.href = "/nhom-vat-tu-thiet-bi.html?input=" + input_val + "&dis=" + curr + '&page=' + page;
    }
});

$(document).keyup(function (e) {
    if ($("input[name='input_search']").is(":focus") && (e.keyCode == 13)) {
        $(".icon_sr_wh").click();
    }
});

function display(select){
    var curr = $(select).val();
    var page = 1;
    var input_val = $("input[name='input_search']").val();
    if(input_val == "" && curr == 10){
        window.location.href = "/nhom-vat-tu-thiet-bi.html?dis=" + curr + '&page=' + page;
    }else{
        window.location.href = "/nhom-vat-tu-thiet-bi.html?input=" + input_val + "&dis=" + curr + '&page=' + page;
    }
    $("input[name='input_search'").val('');
}