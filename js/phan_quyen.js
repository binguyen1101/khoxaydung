$('.ro_them').click(function(){
    var ro_sua = $(this).parent().parent().find('.ro_sua');
    var ro_xoa = $(this).parent().parent().find('.ro_xoa');
    if($(this).is(":checked") && !ro_sua.is(":checked") && !ro_xoa.is(":checked")){
        $(this).parent().parent().find('.ro_xem').prop('checked', true);
    }
    // if(!$(this).is(":checked") && !ro_sua.is(":checked") && !ro_xoa.is(":checked")){
    //     $(this).parent().parent().find('.ro_xem').prop('checked', false);
    // }

});

$('.ro_sua').click(function(){
    var ro_them = $(this).parent().parent().find('.ro_them');
    var ro_xoa = $(this).parent().parent().find('.ro_xoa');
    if($(this).is(":checked") && !ro_them.is(":checked") && !ro_xoa.is(":checked")){
        $(this).parent().parent().find('.ro_xem').prop('checked', true);
    }
    // if(!$(this).is(":checked") && !ro_them.is(":checked") && !ro_xoa.is(":checked")){
    //     $(this).parent().parent().find('.ro_xem').prop('checked', false);
    // }
});

$('.ro_xoa').click(function(){
    var ro_sua = $(this).parent().parent().find('.ro_sua');
    var ro_them = $(this).parent().parent().find('.ro_them');
    if($(this).is(":checked") && !ro_sua.is(":checked") && !ro_them.is(":checked")){
        $(this).parent().parent().find('.ro_xem').prop('checked', true);
    }
    // if(!$(this).is(":checked") && !ro_them.is(":checked") && !ro_sua.is(":checked")){
    //     $(this).parent().parent().find('.ro_xem').prop('checked', false);
    // }
});

$('.ro_xem').click(function(){
    var ro_them = $(this).parent().parent().find('.ro_them');
    var ro_sua = $(this).parent().parent().find('.ro_sua');
    var ro_xoa = $(this).parent().parent().find('.ro_xoa');
    if(!$(this).is(":checked")){
        ro_them.prop('checked', false);
        ro_sua.prop('checked', false);
        ro_xoa.prop('checked', false);
    }
});

$('.button_accp').click(function(){

    // Quyền vật tư
    var vat_tu = [];
    if($("input[name='xem'").is(":checked")){
         var xem_vt = "1";
         vat_tu.push(xem_vt);
    }else{
        var xem_vt = "";
    }

    if($("input[name='them'").is(":checked")){
         var them_vt = "2";
         vat_tu.push(them_vt);
        }else{
        var them_vt = "";
    }

    if($("input[name='sua'").is(":checked")){
         var sua_vt = "3";
         vat_tu.push(sua_vt);
        }else{
        var sua_vt = "";
    }

    if($("input[name='xoa'").is(":checked")){
         var xoa_vt = "4";
         vat_tu.push(xoa_vt);
        }else{
        var xoa_vt = "";
    }

    // if($("input[name='xem_dl_cty'").is(":checked")){
    //      var xem_dl_ct = "6";
    //      vat_tu.push(xem_dl_ct);
    //     }else{
    //     var xem_dl_ct = "";
    // }

    // Quyền nhóm vật tư
    var nhom_vat_tu = [];
    if($("input[name='xem1'").is(":checked")){
         var xem_nvt = "1";
         nhom_vat_tu.push(xem_nvt);
    }else{
        var xem_nvt = "";
    }

    if($("input[name='them1'").is(":checked")){
         var them_nvt = "2";
         nhom_vat_tu.push(them_nvt);
        }else{
        var them_nvt = "";
    }

    if($("input[name='sua1'").is(":checked")){
         var sua_nvt = "3";
         nhom_vat_tu.push(sua_nvt);
        }else{
        var sua_nvt = "";
    }

    if($("input[name='xoa1'").is(":checked")){
         var xoa_nvt = "4";
         nhom_vat_tu.push(xoa_nvt);
        }else{
        var xoa_nvt = "";
    }

    // if($("input[name='xem_dl_cty1'").is(":checked")){
    //      var xem_dl_ct1 = "6";
    //      nhom_vat_tu.push(xem_dl_ct1);
    //     }else{
    //     var xem_dl_ct1 = "";
    // }

    // Quyền hãng sản xuất
    var hang_san_xuat = [];
    if($("input[name='xem2'").is(":checked")){
         var xem_hsx = "1";
         hang_san_xuat.push(xem_hsx);
    }else{
        var xem_hsx = "";
    }

    if($("input[name='them2'").is(":checked")){
         var them_hsx = "2";
         hang_san_xuat.push(them_hsx);
        }else{
        var them_hsx = "";
    }

    if($("input[name='sua2'").is(":checked")){
         var sua_hsx = "3";
         hang_san_xuat.push(sua_hsx);
        }else{
        var sua_hsx = "";
    }

    if($("input[name='xoa2'").is(":checked")){
         var xoa_hsx = "4";
         hang_san_xuat.push(xoa_hsx);
        }else{
        var xoa_hsx = "";
    }

    // if($("input[name='xem_dl_cty2'").is(":checked")){
    //      var xem_dl_ct2 = "6";
    //      hang_san_xuat.push(xem_dl_ct2);
    //     }else{
    //     var xem_dl_ct2 = "";
    // }

    // Quyền đơn vị tính
    var don_vi_tinh = [];
    if($("input[name='xem3'").is(":checked")){
         var xem_dvt = "1";
         don_vi_tinh.push(xem_dvt);
    }else{
        var xem_dvt = "";
    }

    if($("input[name='them3'").is(":checked")){
         var them_dvt = "2";
         don_vi_tinh.push(them_dvt);
        }else{
        var them_dvt = "";
    }

    if($("input[name='sua3'").is(":checked")){
         var sua_dvt = "3";
         don_vi_tinh.push(sua_dvt);
        }else{
        var sua_dvt = "";
    }

    if($("input[name='xoa3'").is(":checked")){
         var xoa_dvt = "4";
         don_vi_tinh.push(xoa_dvt);
        }else{
        var xoa_dvt = "";
    }

    // if($("input[name='xem_dl_cty3'").is(":checked")){
    //      var xem_dl_ct3 = "6";
    //      don_vi_tinh.push(xem_dl_ct3);
    //     }else{
    //     var xem_dl_ct3 = "";
    // }

    // Quyền tồn kho
    var ton_kho = [];
    if($("input[name='xem4'").is(":checked")){
         var xem_tk = "1";
         ton_kho.push(xem_tk);
    }else{
        var xem_tk = "";
    }

    if($("input[name='them4'").is(":checked")){
        var them_tk = "2";
        ton_kho.push(them_tk);
       }else{
       var them_tk = "";
   }

   if($("input[name='sua4'").is(":checked")){
        var sua_tk = "3";
        ton_kho.push(sua_tk);
       }else{
       var sua_tk = "";
   }

   if($("input[name='xoa4'").is(":checked")){
        var xoa_tk = "4";
        ton_kho.push(xoa_tk);
       }else{
       var xoa_tk = "";
   }


//     if($("input[name='xem_dl_cty4'").is(":checked")){
//         var xem_dl_ct4 = "6";
//         ton_kho.push(xem_dl_ct4);
//        }else{
//        var xem_dl_ct4 = "";
//    }


   // Quyền nhập kho
   var nhap_kho = [];
   if($("input[name='xem5'").is(":checked")){
        var xem_nk = "1";
        nhap_kho.push(xem_nk);
   }else{
       var xem_nk = "";
   }

   if($("input[name='them5'").is(":checked")){
        var them_nk = "2";
        nhap_kho.push(them_nk);
       }else{
       var them_nk = "";
   }

   if($("input[name='sua5'").is(":checked")){
        var sua_nk = "3";
        nhap_kho.push(sua_nk);
       }else{
       var sua_nk = "";
   }

   if($("input[name='xoa5'").is(":checked")){
        var xoa_nk = "4";
        nhap_kho.push(xoa_nk);
       }else{
       var xoa_nk = "";
   }

   if($("input[name='duyet5'").is(":checked")){
        var xoa_nk = "5";
        nhap_kho.push(xoa_nk);
        }else{
        var xoa_nk = "";
        }

//    if($("input[name='xem_dl_cty5'").is(":checked")){
//         var xem_dl_ct5 = "6";
//         nhap_kho.push(xem_dl_ct5);
//        }else{
//        var xem_dl_ct5 = "";
//    }
   
   // Quyền xuất kho
   var xuat_kho = [];
   if($("input[name='xem6'").is(":checked")){
        var xem_xk = "1";
        xuat_kho.push(xem_xk);
   }else{
       var xem_xk = "";
   }

   if($("input[name='them6'").is(":checked")){
        var them_xk = "2";
        xuat_kho.push(them_xk);
       }else{
       var them_xk = "";
   }

   if($("input[name='sua6'").is(":checked")){
        var sua_xk = "3";
        xuat_kho.push(sua_xk);
       }else{
       var sua_xk = "";
   }

   if($("input[name='xoa6'").is(":checked")){
        var xoa_xk = "4";
        xuat_kho.push(xoa_xk);
       }else{
       var xoa_xk = "";
   }

   if($("input[name='duyet6'").is(":checked")){
        var xoa_xk = "5";
        xuat_kho.push(xoa_xk);
        }else{
        var xoa_xk = "";
        }

//    if($("input[name='xem_dl_cty6'").is(":checked")){
//         var xem_dl_cty6 = "6";
//         xuat_kho.push(xem_dl_cty6);
//        }else{
//        var xem_dl_cty6 = "";
//    }
    
   // Quyền điều chuyển kho
   var dieu_chuyen_kho = [];
   if($("input[name='xem7'").is(":checked")){
        var xem_dck = "1";
        dieu_chuyen_kho.push(xem_dck);
   }else{
       var xem_dck = "";
   }

   if($("input[name='them7'").is(":checked")){
        var them_dck = "2";
        dieu_chuyen_kho.push(them_dck);
       }else{
       var them_dck = "";
   }

   if($("input[name='sua7'").is(":checked")){
        var sua_dck = "3";
        dieu_chuyen_kho.push(sua_dck);
       }else{
       var sua_dck = "";
   }

   if($("input[name='xoa7'").is(":checked")){
        var xoa_dck = "4";
        dieu_chuyen_kho.push(xoa_dck);
       }else{
       var xoa_dck = "";
   }

   if($("input[name='duyet7'").is(":checked")){
        var xoa_dck = "5";
        dieu_chuyen_kho.push(xoa_dck);
        }else{
        var xoa_dck = "";
        }

//    if($("input[name='xem_dl_cty7'").is(":checked")){
//         var xem_dl_cty7 = "6";
//         dieu_chuyen_kho.push(xem_dl_cty7);
//        }else{
//        var xem_dl_cty7 = "";
//    }
    
   // Quyền kiểm kê kho
   var kiem_ke_kho = [];
   if($("input[name='xem8'").is(":checked")){
        var xem_kkk = "1";
        kiem_ke_kho.push(xem_kkk);
   }else{
       var xem_kkk = "";
   }

   if($("input[name='them8'").is(":checked")){
        var them_kkk = "2";
        kiem_ke_kho.push(them_kkk);
       }else{
       var them_kkk = "";
   }

   if($("input[name='sua8'").is(":checked")){
        var sua_kkk = "3";
        kiem_ke_kho.push(sua_kkk);
       }else{
       var sua_kkk = "";
   }

   if($("input[name='xoa8'").is(":checked")){
        var xoa_kkk = "4";
        kiem_ke_kho.push(xoa_kkk);
       }else{
       var xoa_kkk = "";
   }

   if($("input[name='duyet8'").is(":checked")){
        var xoa_kkk = "5";
        kiem_ke_kho.push(xoa_kkk);
        }else{
        var xoa_kkk = "";
        }

//    if($("input[name='xem_dl_cty8'").is(":checked")){
//         var xem_dl_cty8 = "6";
//         kiem_ke_kho.push(xem_dl_cty8);
//        }else{
//        var xem_dl_cty8 = "";
//    }

   // Quyền tồn kho
   var bao_cao = [];
   if($("input[name='xem9'").is(":checked")){
        var xem_bc = "1";
        bao_cao.push(xem_bc);
   }else{
       var xem_bc = "";
   }

//    if($("input[name='xem_dl_cty9'").is(":checked")){
//        var xem_dl_cty9 = "6";
//        bao_cao.push(xem_dl_cty9);
//       }else{
//       var xem_dl_cty9 = "";
//   }

  // Quyền thiết lập phân quyền
  var phan_quyen = [];
  if($("input[name='xem10'").is(":checked")){
       var xem_pq = "1";
       phan_quyen.push(xem_pq);
  }else{
      var xem_pq = "";
  }

  if($("input[name='them10'").is(":checked")){
       var them_pq = "2";
       phan_quyen.push(them_pq);
      }else{
      var them_pq = "";
  }

  if($("input[name='sua10'").is(":checked")){
       var sua_pq = "3";
       phan_quyen.push(sua_pq);
      }else{
      var sua_pq = "";
  }

  if($("input[name='xoa10'").is(":checked")){
       var xoa_pq = "4";
       phan_quyen.push(xoa_pq);
      }else{
      var xoa_pq = "";
  }

//   if($("input[name='xem_dl_cty10'").is(":checked")){
//        var xem_dl_cty10 = "6";
//        phan_quyen.push(xem_dl_cty10);
//       }else{
//       var xem_dl_cty10 = "";
//   }
  
  var id_user = $('.body_equipment_supplies').attr('data-user');
  var id_cty = $('.body_equipment_supplies').attr('data-com');
  $.ajax({
      url: "../ajax/thiet_lap_quyen.php",
      type: "POST",
      data:{
        id_user: id_user,
        id_cty: id_cty,
        vat_tu: vat_tu,
        nhom_vat_tu: nhom_vat_tu,
        hang_san_xuat: hang_san_xuat,
        don_vi_tinh: don_vi_tinh,
        ton_kho: ton_kho,
        nhap_kho: nhap_kho,
        xuat_kho: xuat_kho,
        dieu_chuyen_kho: dieu_chuyen_kho,
        kiem_ke_kho: kiem_ke_kho,
        bao_cao: bao_cao,
        phan_quyen: phan_quyen
      },
      success: function(data){
          if(data != ""){
            alert("Thiết lập quyền thành công!");
            // window.location.href = "/phan-quyen.html";
            window.location.reload();
          }else{
            alert("Thiết lập quyền thất bại!");
          }
      }
  })
});


