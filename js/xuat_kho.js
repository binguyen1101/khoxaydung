
var page = $(".detail_wh").attr("data-page");
var all_k = $(".detail_wh").attr("data-k");
var th = $(".detail_wh").attr("data-th");
var ht = $(".detail_wh").attr("data-ht");

var ngay_tao_start = $(".detail_wh").attr("data-ngts");
var ngay_tao_end = $(".detail_wh").attr("data-ngte");

var ngay_xt_start = $(".detail_wh").attr("data-ngxs");
var ngay_xt_end = $(".detail_wh").attr("data-ngxe");

var input_val = $(".detail_wh").attr("data-ip");
var curr = $(".detail_wh").attr("data-dis");

$.ajax({
    url: "../render/tb_xuat_kho.php",
    type: "POST",
    data:{
        all_k: all_k,
        th: th,
        ht: ht,

        ngay_tao_start: ngay_tao_start,
        ngay_tao_end: ngay_tao_end,

        ngay_xt_start: ngay_xt_start,
        ngay_xt_end: ngay_xt_end,

        input_val: input_val,

        page: page,
        curr: curr
    },
    success: function(data){
        $(".detail_wh").append(data);
    }
});

$(".select_all_kho, .select_all_form_kho, .select_all_status_kho").on("change",function () {
    var page = 1;
    var all_kho = $('.select_all_kho').val();
    var trang_thai = $('.select_all_status_kho').val();
    var hinh_thuc = $('.select_all_form_kho').val();
    
    var ngay_tao_start = $('#date_cr_start').attr("data");
    var ngay_tao_end = $('#date_cr_end').attr("data");
    
    var ngay_xt_start = $('#date_ex_start').attr("data");
    var ngay_xt_end = $('#date_ex_end').attr("data");
    
    var input_val = $("input[name='input_search']").val();
    var curr = $('.show_tr_tb').val();
    
    if(all_kho == "" && trang_thai == "" && hinh_thuc == "" && ngay_tao_start == "" && ngay_tao_end == "" && ngay_xt_start == "" && ngay_xt_end == "" && input_val == "" && curr == 10){
        window.location.href = "/xuat-kho.html?dis=" + curr + '&page=' + page;
    }else{
        window.location.href = "/xuat-kho.html?all_k=" + all_kho + "&th=" + trang_thai + "&ht=" + hinh_thuc + "&ngts=" + ngay_tao_start + "&ngte=" + ngay_tao_end + "&ngxs=" + ngay_xt_start + "&ngxe=" + ngay_xt_end + "&input=" + input_val + "&dis=" + curr + "&page=" + page;
    }

    $("input[name='input_search'").val('');
});

$('.date_create img').click(function(){
    ele = $(this);
    $('#popup_select_date').show();
    $('.popup_select_date .btn_save').on("click", function () {
        selectDate(this, ele);

        var page = 1;
        var all_kho = $('.select_all_kho').val();
        var trang_thai = $('.select_all_status_kho').val();
        var hinh_thuc = $('.select_all_form_kho').val();

        var ngay_tao_start = $(this).parents('.content_popup').find('.d_flex.flex_column:nth-child(1) input').val();
        var ngay_tao_end = $(this).parents('.content_popup').find('.d_flex.flex_column:nth-child(2) input').val();

        var ngay_xt_start = $('#date_ex_start').attr("data");
        var ngay_xt_end = $('#date_ex_end').attr("data");

        var input_val = $("input[name='input_search']").val();
        var curr = $('.show_tr_tb').val();

        if(all_kho == "" && trang_thai == "" && hinh_thuc == "" && ngay_tao_start == "" && ngay_tao_end == "" && ngay_xt_start == "" && ngay_xt_end == "" && input_val == "" && curr == 10){
            window.location.href = "/xuat-kho.html?dis=" + curr + '&page=' + page;
        }else{
            window.location.href = "/xuat-kho.html?all_k=" + all_kho + "&th=" + trang_thai + "&ht=" + hinh_thuc + "&ngts=" + ngay_tao_start + "&ngte=" + ngay_tao_end + "&ngxs=" + ngay_xt_start + "&ngxe=" + ngay_xt_end + "&input=" + input_val + "&dis=" + curr + "&page=" + page;
        }

    })
});

$('.date_cr_export img').click(function(){
    ele = $(this);
    $('#popup_select_date').show();
    $('.popup_select_date .btn_save').on("click", function () {
        selectDate(this, ele);

        var page = 1;
        var all_kho = $('.select_all_kho').val();
        var trang_thai = $('.select_all_status_kho').val();
        var hinh_thuc = $('.select_all_form_kho').val();
        var ngay_tao_start = $('#date_cr_start').attr("data");
        var ngay_tao_end = $('#date_cr_end').attr("data");

        var ngay_xt_start = $(this).parents('.content_popup').find('.d_flex.flex_column:nth-child(1) input').val();
        var ngay_xt_end = $(this).parents('.content_popup').find('.d_flex.flex_column:nth-child(2) input').val();

        var input_val = $("input[name='input_search']").val();
        var curr = $('.show_tr_tb').val();

        if(all_kho == "" && trang_thai == "" && hinh_thuc == "" && ngay_tao_start == "" && ngay_tao_end == "" && ngay_xt_start == "" && ngay_xt_end == "" && input_val == "" && curr == 10){
            window.location.href = "/xuat-kho.html?dis=" + curr + '&page=' + page;
        }else{
            window.location.href = "/xuat-kho.html?all_k=" + all_kho + "&th=" + trang_thai + "&ht=" + hinh_thuc + "&ngts=" + ngay_tao_start + "&ngte=" + ngay_tao_end + "&ngxs=" + ngay_xt_start + "&ngxe=" + ngay_xt_end + "&input=" + input_val + "&dis=" + curr + "&page=" + page;
        }
    })
});


$(".icon_sr_wh").click(function(){
    var page = 1;
    var all_kho = $('.select_all_kho').val();
    var trang_thai = $('.select_all_status_kho').val();
    var hinh_thuc = $('.select_all_form_kho').val();
    
    var ngay_tao_start = $('#date_cr_start').attr("data");
    var ngay_tao_end = $('#date_cr_end').attr("data");
    
    var ngay_xt_start = $('#date_ex_start').attr("data");
    var ngay_xt_end = $('#date_ex_end').attr("data");
    
    var input_val = $("input[name='input_search']").val();
    var curr = $('.show_tr_tb').val();
    
    if(all_kho == "" && trang_thai == "" && hinh_thuc == "" && ngay_tao_start == "" && ngay_tao_end == "" && ngay_xt_start == "" && ngay_xt_end == "" && input_val == "" && curr == 10){
        window.location.href = "/xuat-kho.html?dis=" + curr + '&page=' + page;
    }else{
        window.location.href = "/xuat-kho.html?all_k=" + all_kho + "&th=" + trang_thai + "&ht=" + hinh_thuc + "&ngts=" + ngay_tao_start + "&ngte=" + ngay_tao_end + "&ngxs=" + ngay_xt_start + "&ngxe=" + ngay_xt_end + "&input=" + input_val + "&dis=" + curr + "&page=" + page;
    }
});

$(document).keyup(function (e) {
    if ($("input[name='input_search']").is(":focus") && (e.keyCode == 13)) {
        $(".icon_sr_wh").click();
    }
});

$(".btn_ex").click(function() {
    window.location.href = '../Excel/xuat_kho.php';
});

function display(select){
    var curr = $(select).val();
    var page = 1;
    var all_kho = $('.select_all_kho').val();
    var trang_thai = $('.select_all_status_kho').val();
    var hinh_thuc = $('.select_all_form_kho').val();
    
    var ngay_tao_start = $('#date_cr_start').attr("data");
    var ngay_tao_end = $('#date_cr_end').attr("data");
    
    var ngay_xt_start = $('#date_ex_start').attr("data");
    var ngay_xt_end = $('#date_ex_end').attr("data");
    
    var input_val = $("input[name='input_search']").val();
    var curr = $('.show_tr_tb').val();
    
    if(all_kho == "" && trang_thai == "" && hinh_thuc == "" && ngay_tao_start == "" && ngay_tao_end == "" && ngay_xt_start == "" && ngay_xt_end == "" && input_val == "" && curr == 10){
        window.location.href = "/xuat-kho.html?dis=" + curr + '&page=' + page;
    }else{
        window.location.href = "/xuat-kho.html?all_k=" + all_kho + "&th=" + trang_thai + "&ht=" + hinh_thuc + "&ngts=" + ngay_tao_start + "&ngte=" + ngay_tao_end + "&ngxs=" + ngay_xt_start + "&ngxe=" + ngay_xt_end + "&input=" + input_val + "&dis=" + curr + "&page=" + page;
    }
}





