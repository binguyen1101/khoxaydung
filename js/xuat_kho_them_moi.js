$('.select_request_form').change(function () {
  // console.log(tt_user)
  var value = $(this).val();
  var dateString = new Date(pyc[value].ngay_ht_yc * 1000).toDateString();
  var toDate = formatDateVN(new Date(dateString));
  $('.cong_trinh').val(ctr_name_ct[pyc[value].id_cong_trinh].ctr_name)
  $('.id_cong_trinh').val(ctr_name_ct[pyc[value].id_cong_trinh].ctr_id)
  if (pyc[value].role == 2) {
    $('.nguoi_nhan').val(data[pyc[value].id_nguoi_yc].ep_name)
    $('.id_nguoi_nhan').val(data[pyc[value].id_nguoi_yc].ep_id)
    $('.phong_ban_nhan').val(data[pyc[value].id_nguoi_yc].dep_name)
    $('.id_phong_ban_nhan').val(data[pyc[value].id_nguoi_yc].dep_id)
  }
  if (pyc[value].role == 1) {
    $('.nguoi_nhan').val(tt_user.com_name)
    $('.id_nguoi_nhan').val('0')
    $('.phong_ban_nhan').val(' ')
    $('.id_phong_ban_nhan').val(' ')
  }
  $('.ngay_ht_yc').val(toDate)
  $.ajax({
    url: '../ajax/get_dsvt_yc.php',
    type: 'POST',
    dataType: 'Json',
    data: {
      id_phieu_yc: value
    },
    success: function (response) {
      var result = response.data.items;
      var text = $('.data_table_1').text('');
      var stt = 1;
      $oninput = "this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');";
      var html = '';
      // console.log(result)
      for (index in result) {
        if (arr_dsvt_tb[result[index].id_vat_tu].hsx_name == null) {
          var hsx_name = ''
        } else {
          var hsx_name = arr_dsvt_tb[result[index].id_vat_tu].hsx_name
        }
        if (arr_dsvt_tb[result[index].id_vat_tu].xx_name == null) {
          var xx_name = ''
        } else {
          var xx_name = arr_dsvt_tb[result[index].id_vat_tu].xx_name
        }
        if (arr_dsvt_tb[result[index].id_vat_tu].dsvt_donGia == null) {
          var don_gia = ''
        } else {
          var don_gia = arr_dsvt_tb[result[index].id_vat_tu].dsvt_donGia
        }
        console.log(arr_dsvt_tb[result[index].id_vat_tu])
        html += `	<tr class="color_grey font_s14 line_h16 font_w400">
                  <td>` + (Number(index) + 1) + `</td>
                  <td>VT-` + result[index].id_vat_tu + `</td>
                  <td style="text-align: left;">
                    <a href="#" class="color_blue font_w500 id_vt" data ="` + result[index].id_vat_tu + `">
                    ` + arr_dsvt_tb[result[index].id_vat_tu].dsvt_name + `
                   </a>
                 </td>
                  <td>` + arr_dsvt_tb[result[index].id_vat_tu].dvt_name + `</td>
                  <td style="text-align: left;">` + hsx_name + `</td>
                  <td>` + xx_name + `</td>
                  <td style="text-align: right; background: #EEEEEE;" class="slyc">` + result[index].so_luong_yc_duyet + `</td>
                  <td style="text-align: right; background: #EEEEEE;" class="sld">` + result[index].so_luong_duyet + `</td>
                  <td>
                    <input class="nhap_so_luong" style="border: none; text-align: right;  outline: none" type="text" placeholder="Nhập số lượng" onkeyup="tong_vt(this)" oninput="<?= $oninput ?>">
                  </td>
                  <td style="text-align: right;" class="don_gia_3">` + don_gia + `</td>
                  <td style="text-align: right;" class="thanh_tien_3">
                    
                  </td>
               </tr>`;
      }
      $('#dom-table').append(html);
    }
  })
  var id_cong_trinh = ctr_name_ct[pyc[value].id_cong_trinh].ctr_id;
  var id_cty = id_ct
  $.ajax({
    url: "../render/kho_ct.php",
    type: "POST",
    data: {
      id_cty: id_cty,
      id_cong_trinh: id_cong_trinh
    },
    success: function (data) {
      $("select[name='select_export_kho1']").html(data);
    }
  })
})

$('.select_request_form1').change(function () {
  var value = $(this).val();
  var dateString = new Date(pyc[value].ngay_ht_yc * 1000).toDateString();
  var toDate = formatDateVN(new Date(dateString));
  $('.date_yc_3').val(toDate)
  $('.kho_xuat_3').val(arr_kho3[pyc[value].id_kho].kho_name)
  $('.id_kho_xuat_3').val(pyc[value].id_kho)
  $.ajax({
    url: '../ajax/get_dsvt_yc.php',
    type: 'POST',
    dataType: 'Json',
    data: {
      id_phieu_yc: value
    },
    success: function (response) {
      console.log(response)
      var result = response.data.items;
      var text = $('.data_table_1').text('');
      var stt = 1;
      $oninput = "this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');";
      var html = '';
      console.log(result)
      for (index in result) {
        if (arr_dsvt_tb[result[index].id_vat_tu].hsx_name == null) {
          var hsx_name = ''
        } else {
          var hsx_name = arr_dsvt_tb[result[index].id_vat_tu].hsx_name
        }
        if (arr_dsvt_tb[result[index].id_vat_tu].xx_name == null) {
          var xx_name = ''
        } else {
          var xx_name = arr_dsvt_tb[result[index].id_vat_tu].xx_name
        }
        if (arr_dsvt_tb[result[index].id_vat_tu].dsvt_donGia == null) {
          var don_gia = ''
        } else {
          var don_gia = arr_dsvt_tb[result[index].id_vat_tu].dsvt_donGia
        }
        html += `	<tr class="color_grey font_s14 line_h16 font_w400">
                  <td>` + (Number(index) + 1) + `</td>
                  <td>VT-` + result[index].id_vat_tu + `</td>
                  <td style="text-align: left;">
                    <a href="#" class="color_blue font_w500 id_vt" data ="` + result[index].id_vat_tu + `">
                    ` + arr_dsvt_tb[result[index].id_vat_tu].dsvt_name + `
                   </a>
                 </td>
                  <td>` + arr_dsvt_tb[result[index].id_vat_tu].dvt_name + `</td>
                  <td style="text-align: left;">` + arr_dsvt_tb[result[index].id_vat_tu].hsx_name + `</td>
                  <td>` + arr_dsvt_tb[result[index].id_vat_tu].xx_name + `</td>
                  <td style="text-align: right; background: #EEEEEE;" class="slyc">` + result[index].so_luong_yc_duyet + `</td>
                  <td style="text-align: right; background: #EEEEEE;" class="sld">` + result[index].so_luong_duyet + `</td>
                  <td>
                    <input class="nhap_so_luong" style="border: none; text-align: right;  outline: none" type="text" placeholder="Nhập số lượng" onkeyup="tong_vt(this)" oninput="<?= $oninput ?>">	
                  </td>
                  <td style="text-align: right;" class="don_gia_3">` + arr_dsvt_tb[result[index].id_vat_tu].dsvt_donGia + `</td>
                  <td style="text-align: right;" class="thanh_tien_3">
                    
                  </td>
               </tr>`;
      }
      $('#dom-table').append(html);
    }
  })

})

$('.select_transfer_slip').change(function () {
  var id_cty = id_ct
  var value = $('.select_transfer_slip').val();
  $('.kho_xuat_1').val(pdc[value].kho_xuat_name)
  $('.id_kho_xuat_1').val(pdc[value].kcxl_khoXuat)
  $('.kho_nhap_1').val(pdc[value].kho_nhap_name)
  $('.id_kho_nhap_1').val(pdc[value].kcxl_khoNhap)
  $('.ngay_yeu_cau_1').val(pdc[value].kcxl_ngayYeuCauHoanThanh)
  $('.nguoi_giao_1').val(data[pdc[value].kcxl_nguoiThucHien].ep_name)
  $('.id_nguoi_giao_1').val(pdc[value].kcxl_nguoiThucHien)
  $('.id_phong_ban_giao_1').val(data[pdc[value].kcxl_nguoiThucHien].dep_id)
  $('.phong_ban_giao_1').val(data[pdc[value].kcxl_nguoiThucHien].dep_name)
  $('.nguoi_nhan_1').val(data[pdc[value].kcxl_nguoiNhan].ep_name)
  $('.id_nguoi_nhan_1').val(pdc[value].kcxl_nguoiNhan)
  $('.phong_ban_nhan_1').val(data[pdc[value].kcxl_nguoiNhan].dep_name)
  $('.id_phong_ban_nhan_1').val(pdc[value].kcxl_phongBanNguoiNhan)
  $.ajax({
    url: '../ajax/get_dsvt_pdc.php',
    type: 'POST',
    dataType: 'Json',
    data: {
      id_phieu_pdc: value,
      id_cty: id_cty
    },
    success: function (response) {
      console.log(response)
      var text = $('.data_table_2').text('');
      $oninput = "this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');";
      var stt = 1;
      var html = '';
      for (index in response) {
        if (arr_dsvt_tb[result[index].id_vat_tu].hsx_name == null) {
          var hsx_name = ''
        } else {
          var hsx_name = arr_dsvt_tb[result[index].id_vat_tu].hsx_name
        }
        if (arr_dsvt_tb[result[index].id_vat_tu].xx_name == null) {
          var xx_name = ''
        } else {
          var xx_name = arr_dsvt_tb[result[index].id_vat_tu].xx_name
        }
        if (arr_dsvt_tb[result[index].id_vat_tu].dsvt_donGia == null) {
          var don_gia = ''
        } else {
          var don_gia = arr_dsvt_tb[result[index].id_vat_tu].dsvt_donGia
        }
        html += `	 <tr class="color_grey font_s14 line_h16 font_w400">
                  <td>` + stt++ + `</td>
                  <td>VT-` + response[index].id + `</td>
                  <td style="text-align: left;">
                    <a href="#" class="color_blue font_w500 id_vt_1" data_1 ="` + response[index].id + `">` + arr_dsvt_tb[response[index].id].dsvt_name + `</a>
                  </td>
                  <td>` + arr_dsvt_tb[response[index].id].dvt_name + `</td>
                  <td style="text-align: left;">` + hsx_name + `</td>
                  <td>` + xx_name + `</td>
                  <td style="text-align: right; background: #EEEEEE;" class="slyc">` + response[index].soluong + `</td>
                  <td>
                    <input class="nhap_so_luong_1" style="border: none; text-align: right; outline: none" type="text" placeholder="Nhập số lượng" onkeyup="tong_vt(this)" oninput="<?= $oninput ?>">
                  </td>
                  <td style="text-align: right;" class="don_gia_3">` + don_gia + `</td>
                  <td style="text-align: right;" class="thanh_tien_3">
                  </td>
                </tr>`;
      }
      $('.data_table_2').append(html);
    }
  })
})

$('.add_row').click(function () {
  var value = $('.select_export_kho_5').val()
  var id_cty = id_ct
  if (value != '') {
    $.ajax({
      url: '../ajax/get_dsvt_kho.php',
      type: 'POST',
      data: {
        id_kho: value,
        id_cty: id_cty
      },
      success: function (data) {
        if (data != 'jkkjk') {
          $(".table_khac").append(data);

        } else {
          $(".table_khac").append();
        }
      }
    })
    // change_value()
  }
});

$('#select_deliver_5').change(function () {
  var value = $(this).val();
  $('.ten_pbg_5').val(data[value].dep_name)
  $('.id_pbg_5').val(data[value].dep_id)
})

$('#select_receiver_5').change(function () {
  var value = $(this).val();
  $('.ten_pbn_5').val(data[value].dep_name)
  $('.id_pbn_5').val(data[value].dep_id)
})

$('#select_deliver').change(function () {
  var value = $('.select_deliver').val();
  console.log(data[value].dep_name)
  $('.phong_ban_giao').val(data[value].dep_name)
  $('.id_phong_ban_giao').val(data[value].dep_id)
})

$('.select_deliver1').change(function () {
  var value = $('.select_deliver1').val();
  console.log(data[value].dep_name)
  $('.phong_ban_giao1').val(data[value].dep_name)
  $('.id_phong_ban_giao1').val(data[value].dep_id)
})

$('.select_receiver1').change(function () {
  var value = $('.select_receiver1').val();
  console.log(data[value].dep_name)
  $('.phong_ban_nhan1').val(data[value].dep_name)
  $('.id_phong_ban_nhan1').val(data[value].dep_id)
})

$('#select_export_kho_5').change(function () {
  $('.delete_3').remove()
  var value = $(this).val()
  var id_cty = id_ct
  $.ajax({
    url: '../ajax/get_dsvt_kho.php',
    type: 'POST',
    dataType: 'Json',
    data: {
      id_kho: value,
      id_cty: id_cty
    },
    success: function (response) { }
  })
});

$('#select_order').change(function () {
  var value = $('#select_order').val()
  var dateString = new Date(data_dh_2[value].thoi_han * 1000).toDateString();
  var toDate = formatDateVN(new Date(dateString));
  console.log(value);
  $('.time_ht').val(toDate)
  var id_cty = id_ct

  $.ajax({
    url: "../render/ds_vt_pxk.php",
    type: "POST",
    data: {
      id_cty: id_cty,
      id_dh: value,
    },
    success: function (data) {
      $('.data_table_2').html(data);
    }
  });
})

$('#select_human_export').change(function () {
  var value = $(this).val()
  $('.ten_pb_4').val(data[value].dep_name)
  $('.id_pb_4').val(data[value].dep_id)
})

$('.btn_save').click(function () {
  var form_add = $("#create_export_kho");
  var id_cty = id_ct
  console.log(id_cty)
  resetFormValidator(form_add)
  if ($('.select_export_form').val() === 'XK1') {
    var listTable = []
    $(".nhap_so_luong").each(function () {
      var data = $(this).parent().parent().find($('.id_vt')).attr('data')
      var soluong = $(this).val()
      var slyc = $(this).parents('tr').find('.slyc').html()
      var sld = $(this).parents('tr').find('.sld').html()
      listTable.push({
        'id': data,
        'soluong': soluong,
        'slyc': slyc,
        'sld': sld,
      })
    })
    var check_empty_arr = [];
    for (var i = 0; i < listTable.length; i++) {
      var check_empty = listTable[i].soluong;
      if (check_empty == "") {
        check_empty_arr.push(listTable[i].soluong);
      }
    }
    var length_soluong_empty = check_empty_arr.length;
    if ($('#select_status1').val() === '1') {
      form_add.validate({
        errorPlacement: function (error, element) {
          error.appendTo(element.parents(".select_create_1"));
          error.appendTo(element.parents(".export_kho"));
          error.appendTo(element.parents(".date_export_kho"));
          error.wrap("<span class='error'>")
        },
        rules: {
          select_request_form: "required",
          select_export_kho1: "required",
          selected_date_export_kho: "required",
        },
        messages: {
          select_request_form: "Vui lòng chọn phiếu yêu cầu",
          select_export_kho1: "Vui lòng chọn kho xuất",
          selected_date_export_kho: "Vui lòng chọn ngày xuất kho",
        },
      });
      if (form_add.valid() === true && length_soluong_empty == 0) {
        var kcxl_hinhThuc = $('select[name="select_export_form"]').val()
        var kcxl_nguoiTao = $('.nguoi_tao').attr('data')
        var kcxl_ngayTao = $('.ngay_tao').attr('data')
        var kcxl_phieuYeuCau = $('select[name="select_request_form"]').val()
        var kcxl_trangThai = $('select[name="select_status1"]').val()
        var kcxl_congTrinh = $('.id_cong_trinh').val()
        var kcxl_ngayYeuCauHoanThanh = $('.ngay_ht_yc').val()
        var kcxl_nguoiThucHien = $('select[name="select_deliver"]').val()
        var kcxl_phongBanNguoiGiao = $('.id_phong_ban_giao').val()
        var kcxl_nguoiNhan = $('.id_nguoi_nhan').val()
        var kcxl_phongBanNguoiNhan = $('.id_phong_ban_nhan').val()
        var kcxl_khoXuat = $('select[name="select_export_kho1"]').val()
        var kcxl_ngayXuatKho = $('input[name = "selected_date_export_kho"]').val()
        var kcxl_ghi_chu = $('textarea[name="description"]').val()
        var listTable = JSON.stringify(listTable);
        id_cty;

        $.ajax({
          url: '../ajax/add_new_pxk.php',
          type: 'POST',
          // dataType: 'Json',
          data: {
            kcxl_hinhThuc: kcxl_hinhThuc,
            kcxl_nguoiTao: kcxl_nguoiTao,
            kcxl_ngayTao: kcxl_ngayTao,
            kcxl_phieuYeuCau: kcxl_phieuYeuCau,
            kcxl_trangThai: kcxl_trangThai,
            kcxl_congTrinh: kcxl_congTrinh,
            kcxl_ngayYeuCauHoanThanh: kcxl_ngayYeuCauHoanThanh,
            kcxl_nguoiThucHien: kcxl_nguoiThucHien,
            kcxl_phongBanNguoiGiao: kcxl_phongBanNguoiGiao,
            kcxl_nguoiNhan: kcxl_nguoiNhan,
            kcxl_phongBanNguoiNhan: kcxl_phongBanNguoiNhan,
            kcxl_khoXuat: kcxl_khoXuat,
            kcxl_ngayXuatKho: kcxl_ngayXuatKho,
            kcxl_ghi_chu: kcxl_ghi_chu,
            listTable: listTable,
            id_cty: id_cty,
          },
          success: function (data) {
            //  alert('ssss')
            var text = $('#add_new_export_note_success .p_add_succ').text('');
            var text_new = '';
            text_new += 'Thêm mới phiếu xuất kho theo phiếu yêu cầu thành công!';
            text.append(text_new);
            $('#add_new_export_note_success').show()
          }
        })
      } else {
        alert("Vui lòng nhập đầy đủ thông tin!!!");
      }
    }
    if ($('#select_status1').val() === '2') {
      form_add.validate({
        errorPlacement: function (error, element) {
          error.appendTo(element.parents(".select_create_1"));
          error.appendTo(element.parents(".export_kho"));
          error.appendTo(element.parents(".date_export_kho"));
          error.appendTo(element.parents(".select_create1"));
          error.wrap("<span class='error'>")
        },
        rules: {
          select_request_form: "required",
          select_export_kho1: "required",
          selected_date_export_kho: "required",
          date_accses_export1: "required",
        },
        messages: {
          select_request_form: "Vui lòng chọn phiếu yêu cầu",
          select_export_kho1: "Vui lòng chọn kho xuất",
          selected_date_export_kho: "Vui lòng chọn ngày xuất kho",
          date_accses_export1: "Vui lòng chọn ngày hoàn thành",
        },
      });
      if (form_add.valid() === true && length_soluong_empty == 0) {
        var kcxl_hinhThuc = $('select[name="select_export_form"]').val()
        var kcxl_nguoiTao = $('.nguoi_tao').attr('data')
        var kcxl_ngayTao = $('.ngay_tao').attr('data')
        var kcxl_phieuYeuCau = $('select[name="select_request_form"]').val()
        var kcxl_trangThai = $('select[name="select_status1"]').val()
        var kcxl_congTrinh = $('.id_cong_trinh').val()
        var kcxl_ngayYeuCauHoanThanh = $('.ngay_ht_yc').val()
        var kcxl_nguoiThucHien = $('select[name="select_deliver"]').val()
        var kcxl_phongBanNguoiGiao = $('.id_phong_ban_giao').val()
        var kcxl_nguoiNhan = $('.id_nguoi_nhan').val()
        var kcxl_phongBanNguoiNhan = $('.id_phong_ban_nhan').val()
        var kcxl_khoXuat = $('select[name="select_export_kho1"]').val()
        var kcxl_ngayXuatKho = $('.selected_date_export_kho').val()
        var kcxl_ghi_chu = $('textarea[name="description"]').val()
        var kcxl_ngayHoanThanh = $('input[name="date_accses_export1"]').val()
        var listTable = JSON.stringify(listTable);
        id_cty
        $.ajax({
          url: '../ajax/add_new_pxk.php',
          type: 'POST',
          // dataType: 'Json',
          data: {
            kcxl_hinhThuc: kcxl_hinhThuc,
            kcxl_nguoiTao: kcxl_nguoiTao,
            kcxl_ngayTao: kcxl_ngayTao,
            kcxl_phieuYeuCau: kcxl_phieuYeuCau,
            kcxl_trangThai: kcxl_trangThai,
            kcxl_congTrinh: kcxl_congTrinh,
            kcxl_ngayYeuCauHoanThanh: kcxl_ngayYeuCauHoanThanh,
            kcxl_nguoiThucHien: kcxl_nguoiThucHien,
            kcxl_phongBanNguoiGiao: kcxl_phongBanNguoiGiao,
            kcxl_nguoiNhan: kcxl_nguoiNhan,
            kcxl_phongBanNguoiNhan: kcxl_phongBanNguoiNhan,
            kcxl_khoXuat: kcxl_khoXuat,
            kcxl_ngayXuatKho: kcxl_ngayXuatKho,
            kcxl_ghi_chu: kcxl_ghi_chu,
            kcxl_ngayHoanThanh: kcxl_ngayHoanThanh,
            listTable: listTable,
            id_cty: id_cty,
          },
          success: function (data) {
            var text = $('#add_new_export_note_success .p_add_succ').text('');
            var text_new = '';
            text_new += 'Thêm mới phiếu xuất kho theo phiếu yêu cầu thành công!';
            text.append(text_new);
            $('#add_new_export_note_success').show()
          }
        })
      } else {
        alert("Vui lòng nhập đầy đủ thông tin!!!");
      }
    }
  }
  if ($('.select_export_form').val() === 'XK2') {
    var listTable = []
    $(".nhap_so_luong_1").each(function () {
      var data = $(this).parent().parent().find($('.id_vt_1')).attr('data_1')
      var soluong = $(this).val()
      var slyc = $(this).parents('tr').find('.slyc').text()
      listTable.push({
        'id': data,
        'soluong': soluong,
        'slyc': slyc,
      })
    })
    var check_empty_arr = [];
    for (var i = 0; i < listTable.length; i++) {
      var check_empty = listTable[i].soluong;
      if (check_empty == "") {
        check_empty_arr.push(listTable[i].soluong);
      }
    }
    var length_soluong_empty = check_empty_arr.length;
    if ($('#select_status2').val() === '1') {
      form_add.validate({
        errorPlacement: function (error, element) {
          error.appendTo(element.parents(".select_create2"));
          error.appendTo(element.parents(".date_xuat_2"));
          error.wrap("<span class='error'>")
        },
        rules: {
          select_transfer_slip: "required",
          name_date_xuat_2: "required",
        },
        messages: {
          select_transfer_slip: "Vui lòng chọn phiếu điều chuyển",
          name_date_xuat_2: "Vui lòng chọn ngày xuất kho",
        },
      });
      if (form_add.valid() === true && length_soluong_empty == 0) {
        var kcxl_hinhThuc = $('select[name="select_export_form"]').val()
        var kcxl_nguoiTao = $('.nguoi_tao').attr('data')
        var kcxl_ngayTao = $('.ngay_tao').attr('data')
        var kcxl_phieuDieuChuyenKho = $('select[name="select_transfer_slip"]').val()
        var kcxl_trangThai = $('select[name="select_status2"]').val()
        var kcxl_khoXuat = $('input[name="id_kho_xuat_1"]').val()
        var kcxl_ngayXuatKho = $('#name_date_xuat_2').val()
        var kcxl_khoNhap = $('input[name="id_kho_nhap_1"]').val()
        var kcxl_ngayYeuCauHoanThanh = $('.ngay_yeu_cau_1').val()
        var kcxl_nguoiThucHien = $('.id_nguoi_giao_1').val()
        var kcxl_phongBanNguoiGiao = $('.id_phong_ban_giao_1').val()
        var kcxl_nguoiNhan = $('.id_nguoi_nhan_1').val()
        var kcxl_phongBanNguoiNhan = $('.id_phong_ban_nhan_1').val()
        var kcxl_ghi_chu = $('textarea[name="description"]').val()
        var listTable = JSON.stringify(listTable);
        id_cty
        console.log(listTable)
        $.ajax({
          url: '../ajax/add_new_pxk.php',
          type: 'POST',
          // dataType: 'Json',
          data: {
            kcxl_hinhThuc: kcxl_hinhThuc,
            kcxl_nguoiTao: kcxl_nguoiTao,
            kcxl_ngayTao: kcxl_ngayTao,
            kcxl_phieuDieuChuyenKho: kcxl_phieuDieuChuyenKho,
            kcxl_trangThai: kcxl_trangThai,
            kcxl_khoXuat: kcxl_khoXuat,
            kcxl_ngayXuatKho: kcxl_ngayXuatKho,
            kcxl_khoNhap: kcxl_khoNhap,
            kcxl_ngayYeuCauHoanThanh: kcxl_ngayYeuCauHoanThanh,
            kcxl_nguoiThucHien: kcxl_nguoiThucHien,
            kcxl_phongBanNguoiGiao: kcxl_phongBanNguoiGiao,
            kcxl_nguoiNhan: kcxl_nguoiNhan,
            kcxl_phongBanNguoiNhan: kcxl_phongBanNguoiNhan,
            kcxl_ghi_chu: kcxl_ghi_chu,
            listTable: listTable,
            id_cty: id_cty,
          },
          success: function (data) {
            var text = $('#add_new_export_note_success .p_add_succ').text('');
            var text_new = '';
            text_new += 'Thêm mới phiếu xuất kho theo phiếu điều chuyển thành công!';
            text.append(text_new);
            $('#add_new_export_note_success').show()
          }
        })
      } else {
        alert("Vui lòng nhập đầy đủ thông tin!!!");
      }
    }
    if ($('#select_status2').val() === '2') {
      var form_add = $("#create_export_kho");
      form_add.validate({
        errorPlacement: function (error, element) {
          error.appendTo(element.parents(".select_create2"));
          error.appendTo(element.parents(".date_xuat_2"));
          error.appendTo(element.parents(".date_accses_2"));
          error.wrap("<span class='error'>")
        },
        rules: {
          select_transfer_slip: "required",
          name_date_xuat_2: "required",
          date_accses_export_2: "required",
        },
        messages: {
          select_transfer_slip: "Vui lòng chọn phiếu điều chuyển",
          name_date_xuat_2: "Vui lòng chọn ngày xuất kho",
          date_accses_export_2: "Vui lòng chọn ngày xuất kho",
        },
      });
      if (form_add.valid() === true && length_soluong_empty == 0) {
        var kcxl_hinhThuc = $('select[name="select_export_form"]').val()
        var kcxl_nguoiTao = $('.nguoi_tao').attr('data')
        var kcxl_ngayTao = $('.ngay_tao').attr('data')
        var kcxl_phieuDieuChuyenKho = $('select[name="select_transfer_slip"]').val()
        var kcxl_trangThai = $('select[name="select_status2"]').val()
        var kcxl_ngayHoanThanh = $('input[name="date_accses_export_2"]').val()
        var kcxl_khoXuat = $('input[name="id_kho_xuat_1"]').val()
        var kcxl_ngayXuatKho = $('#name_date_xuat_2').val()
        var kcxl_khoNhap = $('input[name="id_kho_nhap_1"]').val()
        var kcxl_ngayYeuCauHoanThanh = $('.ngay_yeu_cau_1').val()
        var kcxl_nguoiThucHien = $('.id_nguoi_giao_1').val()
        var kcxl_phongBanNguoiGiao = $('.id_phong_ban_giao_1').val()
        var kcxl_nguoiNhan = $('.id_nguoi_nhan_1').val()
        var kcxl_phongBanNguoiNhan = $('.id_phong_ban_nhan_1').val()
        var kcxl_ghi_chu = $('textarea[name="description"]').val()
        var listTable = JSON.stringify(listTable);
        id_cty
        console.log(kcxl_ngayHoanThanh, kcxl_khoXuat, kcxl_ngayXuatKho, kcxl_khoNhap)
        $.ajax({
          url: '../ajax/add_new_pxk.php',
          type: 'POST',
          // dataType: 'Json',
          data: {
            kcxl_hinhThuc: kcxl_hinhThuc,
            kcxl_nguoiTao: kcxl_nguoiTao,
            kcxl_ngayTao: kcxl_ngayTao,
            kcxl_phieuDieuChuyenKho: kcxl_phieuDieuChuyenKho,
            kcxl_trangThai: kcxl_trangThai,
            kcxl_khoXuat: kcxl_khoXuat,
            kcxl_ngayXuatKho: kcxl_ngayXuatKho,
            kcxl_khoNhap: kcxl_khoNhap,
            kcxl_ngayYeuCauHoanThanh: kcxl_ngayYeuCauHoanThanh,
            kcxl_nguoiThucHien: kcxl_nguoiThucHien,
            kcxl_phongBanNguoiGiao: kcxl_phongBanNguoiGiao,
            kcxl_nguoiNhan: kcxl_nguoiNhan,
            kcxl_phongBanNguoiNhan: kcxl_phongBanNguoiNhan,
            kcxl_ghi_chu: kcxl_ghi_chu,
            listTable: listTable,
            id_cty: id_cty,
          },
          success: function (data) {
            var text = $('#add_new_export_note_success .p_add_succ').text('');
            var text_new = '';
            text_new += 'Thêm mới phiếu xuất kho theo phiếu điều chuyển thành công!';
            text.append(text_new);
            $('#add_new_export_note_success').show()
          }
        })
      } else {
        alert("Vui lòng nhập đầy đủ thông tin!!!");
      }
    }
  }
  if ($('.select_export_form').val() === 'XK3') {
    var listTable = []
    $(".nhap_so_luong").each(function () {
      var data = $(this).parent().parent().find($('.id_vt')).attr('data')
      var soluong = $(this).val()
      var slyc = $(this).parents('tr').find('.slyc').text()
      var sld = $(this).parents('tr').find('.sld').text()
      listTable.push({
        'id': data,
        'soluong': soluong,
        'slyc': slyc,
        'sld': sld,
      })
    })
    var check_empty_arr = [];
    for (var i = 0; i < listTable.length; i++) {
      var check_empty = listTable[i].soluong;
      if (check_empty == "") {
        check_empty_arr.push(listTable[i].soluong);
      }
    }
    var length_soluong_empty = check_empty_arr.length;
    if ($('#select_status3').val() === '1') {
      form_add.validate({
        errorPlacement: function (error, element) {
          error.appendTo(element.parents(".request_form1"));
          error.appendTo(element.parents(".date_xuat_3"));
          error.wrap("<span class='error'>")
        },
        rules: {
          select_request_form1: "required",
          ngay_xuat_3: "required",
        },
        messages: {
          select_request_form1: "Vui lòng chọn phiếu yêu cầu",
          ngay_xuat_3: "Vui lòng chọn ngày xuất kho",
        },
      });
      if (form_add.valid() === true && length_soluong_empty == 0) {
        var kcxl_hinhThuc = $('select[name="select_export_form"]').val()
        var kcxl_nguoiTao = $('.nguoi_tao').attr('data')
        var kcxl_ngayTao = $('.ngay_tao').attr('data')
        var kcxl_phieuYeuCau = $('select[name="select_request_form1"]').val()
        var kcxl_trangThai = $('select[name="select_status3"]').val()
        var kcxl_khoXuat = $('.id_kho_xuat_3').val()
        var kcxl_ngayXuatKho = $('input[name = "ngay_xuat_3"]').val()
        var kcxl_ngayYeuCauHoanThanh = $('.date_yc_3').val()
        var kcxl_nguoiThucHien = $('select[name="select_deliver1"]').val()
        var kcxl_phongBanNguoiGiao = $('.id_phong_ban_giao1').val()
        var kcxl_nguoiNhan = $('select[name="select_receiver1"]').val()
        var kcxl_phongBanNguoiNhan = $('.id_phong_ban_nhan1').val()
        var kcxl_ghi_chu = $('textarea[name="description"]').val()
        var listTable = JSON.stringify(listTable);
        id_cty
        $.ajax({
          url: '../ajax/add_new_pxk.php',
          type: 'POST',
          // dataType: 'Json',
          data: {
            kcxl_hinhThuc: kcxl_hinhThuc,
            kcxl_nguoiTao: kcxl_nguoiTao,
            kcxl_ngayTao: kcxl_ngayTao,
            kcxl_phieuYeuCau: kcxl_phieuYeuCau,
            kcxl_trangThai: kcxl_trangThai,
            kcxl_congTrinh: kcxl_congTrinh,
            kcxl_ngayYeuCauHoanThanh: kcxl_ngayYeuCauHoanThanh,
            kcxl_nguoiThucHien: kcxl_nguoiThucHien,
            kcxl_phongBanNguoiGiao: kcxl_phongBanNguoiGiao,
            kcxl_nguoiNhan: kcxl_nguoiNhan,
            kcxl_phongBanNguoiNhan: kcxl_phongBanNguoiNhan,
            kcxl_khoXuat: kcxl_khoXuat,
            kcxl_ngayXuatKho: kcxl_ngayXuatKho,
            kcxl_ghi_chu: kcxl_ghi_chu,
            listTable: listTable,
            id_cty: id_cty,
          },
          success: function (data) {
            var text = $('#add_new_export_note_success .p_add_succ').text('');
            var text_new = '';
            text_new += 'Thêm mới phiếu xuất kho theo phiếu yêu cầu thành công!';
            text.append(text_new);
            $('#add_new_export_note_success').show()
          }
        })
      } else {
        alert("Vui lòng nhập đầy đủ thông tin!!!");
      }
    }
    if ($('#select_status3').val() === '2') {
      form_add.validate({
        errorPlacement: function (error, element) {
          error.appendTo(element.parents(".request_form1"));
          error.appendTo(element.parents(".date_xuat_3"));
          error.appendTo(element.parents(".date_accses_3"));
          error.wrap("<span class='error'>")
        },
        rules: {
          select_request_form1: "required",
          ngay_xuat_3: "required",
          date_accses_export_3: "required",
        },
        messages: {
          select_request_form1: "Vui lòng chọn phiếu yêu cầu",
          ngay_xuat_3: "Vui lòng chọn ngày xuất kho",
          date_accses_export_3: "Vui lòng chọn ngày xuất kho",
        },
      });
      if (form_add.valid() === true && length_soluong_empty == 0) {
        var kcxl_hinhThuc = $('select[name="select_export_form"]').val()
        var kcxl_nguoiTao = $('.nguoi_tao').attr('data')
        var kcxl_ngayTao = $('.ngay_tao').attr('data')
        var kcxl_phieuYeuCau = $('select[name="select_request_form1"]').val()
        var kcxl_trangThai = $('select[name="select_status3"]').val()
        var kcxl_ngayHoanThanh = $('input[name="date_accses_export_3"]').val()
        var kcxl_khoXuat = $('.id_kho_xuat_3').val()
        var kcxl_ngayXuatKho = $('input[name = "ngay_xuat_3"]').val()
        var kcxl_ngayYeuCauHoanThanh = $('.date_yc_3').val()
        var kcxl_nguoiThucHien = $('select[name="select_deliver1"]').val()
        var kcxl_phongBanNguoiGiao = $('.phong_ban_giao1').val()
        var kcxl_nguoiNhan = $('select[name="select_receiver1"]').val()
        var kcxl_phongBanNguoiNhan = $('.phong_ban_nhan1').val()
        var kcxl_ghi_chu = $('textarea[name="description"]').val()
        var listTable = JSON.stringify(listTable);
        id_cty
        $.ajax({
          url: '../ajax/add_new_pxk.php',
          type: 'POST',
          // dataType: 'Json',
          data: {
            kcxl_hinhThuc: kcxl_hinhThuc,
            kcxl_nguoiTao: kcxl_nguoiTao,
            kcxl_ngayTao: kcxl_ngayTao,
            kcxl_phieuYeuCau: kcxl_phieuYeuCau,
            kcxl_trangThai: kcxl_trangThai,
            kcxl_ngayHoanThanh: kcxl_ngayHoanThanh,
            kcxl_congTrinh: kcxl_congTrinh,
            kcxl_ngayYeuCauHoanThanh: kcxl_ngayYeuCauHoanThanh,
            kcxl_nguoiThucHien: kcxl_nguoiThucHien,
            kcxl_phongBanNguoiGiao: kcxl_phongBanNguoiGiao,
            kcxl_nguoiNhan: kcxl_nguoiNhan,
            kcxl_phongBanNguoiNhan: kcxl_phongBanNguoiNhan,
            kcxl_khoXuat: kcxl_khoXuat,
            kcxl_ngayXuatKho: kcxl_ngayXuatKho,
            kcxl_ghi_chu: kcxl_ghi_chu,
            listTable: listTable,
            id_cty: id_cty,
          },
          success: function (data) {
            var text = $('#add_new_export_note_success .p_add_succ').text('');
            var text_new = '';
            text_new += 'Thêm mới phiếu xuất kho theo phiếu yêu cầu thành công!';
            text.append(text_new);
            $('#add_new_export_note_success').show()
          }
        })
      } else {
        alert("Vui lòng nhập đầy đủ thông tin!!!");
      }
    }
  }
  if ($('.select_export_form').val() === 'XK4') {
    var listTable = []
    $(".nhap_so_luong").each(function () {
      var data = $(this).parent().parent().find($('.id_vt')).attr('data')
      var soluong = $(this).val()
      var slyc = $(this).parents('tr').find('.slyc').text()
      listTable.push({
        'id': data,
        'soluong': soluong,
        'slyc': slyc,
      })
    })
    var check_empty_arr = [];
    for (var i = 0; i < listTable.length; i++) {
      var check_empty = listTable[i].soluong;
      if (check_empty == "") {
        check_empty_arr.push(listTable[i].soluong);
      }
    }
    var length_soluong_empty = check_empty_arr.length;
    if ($('#select_status4').val() === '1') {
      form_add.validate({
        errorPlacement: function (error, element) {
          error.appendTo(element.parents(".choose_select_order"));
          error.appendTo(element.parents(".choose_select_export_kho_2"));
          error.appendTo(element.parents(".select_date_xuat_4"));
          error.appendTo(element.parents(".choose_select_human_export"));
          error.wrap("<span class='error'>")
        },
        rules: {
          select_order: "required",
          select_export_kho2: "required",
          date_xuat_4: "required",
          select_human_export: "required",
        },
        messages: {
          select_order: "Vui lòng chọn đơn hàng",
          select_export_kho2: "Vui lòng chọn kho xuất",
          date_xuat_4: "Vui lòng chọn ngày xuất kho",
          select_human_export: "Vui lòng chọn người xuất kho",
        },
      });
      if (form_add.valid() === true && length_soluong_empty == 0) {
        var kcxl_hinhThuc = $('select[name="select_export_form"]').val()
        var kcxl_nguoiTao = $('.nguoi_tao').attr('data')
        var kcxl_ngayTao = $('.ngay_tao').attr('data')
        var kcxl_donHang = $('select[name = "select_order"]').val()
        var kcxl_trangThai = $('select[name="select_status4"]').val()
        var kcxl_khoXuat = $('select[name="select_export_kho2"]').val()
        var kcxl_ngayXuatKho = $('input[name = "date_xuat_4"]').val()
        var kcxl_ngayYeuCauHoanThanh = $('.time_ht').val()
        var kcxl_nguoiThucHien = $('select[name="select_human_export"]').val()
        var kcxl_phongBanNguoiGiao = $('.id_pb_4').val()
        var kcxl_ghi_chu = $('textarea[name="description"]').val()
        var listTable = JSON.stringify(listTable);
        id_cty
        $.ajax({
          url: '../ajax/add_new_pxk.php',
          type: 'POST',
          // dataType: 'Json',
          data: {
            kcxl_hinhThuc: kcxl_hinhThuc,
            kcxl_nguoiTao: kcxl_nguoiTao,
            kcxl_ngayTao: kcxl_ngayTao,
            kcxl_donHang: kcxl_donHang,
            kcxl_trangThai: kcxl_trangThai,
            kcxl_khoXuat: kcxl_khoXuat,
            kcxl_ngayXuatKho: kcxl_ngayXuatKho,
            kcxl_ngayYeuCauHoanThanh: kcxl_ngayYeuCauHoanThanh,
            kcxl_nguoiThucHien: kcxl_nguoiThucHien,
            kcxl_phongBanNguoiGiao: kcxl_phongBanNguoiGiao,
            kcxl_ghi_chu: kcxl_ghi_chu,
            listTable: listTable,
            id_cty: id_cty,
          },
          success: function (data) {
            var text = $('#add_new_export_note_success .p_add_succ').text('');
            var text_new = '';
            text_new += 'Thêm mới phiếu xuất kho theo đơn hàng thành công!';
            text.append(text_new);
            $('#add_new_export_note_success').show()
          }
        })
      } else {
        alert("Vui lòng nhập đầy đủ thông tin!!!");
      }
    }
    if ($('#select_status4').val() === '2') {
      var form_add = $("#create_export_kho");
      form_add.validate({
        errorPlacement: function (error, element) {
          error.appendTo(element.parents(".choose_select_order"));
          error.appendTo(element.parents(".choose_select_export_kho_2"));
          error.appendTo(element.parents(".select_date_xuat_4"));
          error.appendTo(element.parents(".date_accses_4"));
          error.appendTo(element.parents(".choose_select_human_export"));
          error.wrap("<span class='error'>")
        },
        rules: {
          select_order: "required",
          select_export_kho2: "required",
          date_xuat_4: "required",
          date_accses_export_4: "required",
          select_human_export: "required",
        },
        messages: {
          select_order: "Vui lòng chọn đơn hàng",
          select_export_kho2: "Vui lòng chọn kho xuất",
          date_xuat_4: "Vui lòng chọn ngày xuất kho",
          date_accses_export_4: "Vui lòng chọn ngày hoàn thành",
          select_human_export: "Vui lòng chọn người xuất kho",
        },
      });
      if (form_add.valid() === true && length_soluong_empty == 0) {
        var kcxl_hinhThuc = $('select[name="select_export_form"]').val()
        var kcxl_nguoiTao = $('.nguoi_tao').attr('data')
        var kcxl_ngayTao = $('.ngay_tao').attr('data')
        var kcxl_donHang = $('select[name = "select_order"]').val()
        var kcxl_trangThai = $('select[name="select_status4"]').val()
        var kcxl_khoXuat = $('select[name="select_export_kho2"]').val()
        var kcxl_ngayXuatKho = $('input[name = "date_xuat_4"]').val()
        var kcxl_ngayHoanThanh = $('input[name = "date_accses_export_4"]').val()
        var kcxl_ngayYeuCauHoanThanh = $('.time_ht').val()
        var kcxl_nguoiThucHien = $('select[name="select_human_export"]').val()
        var kcxl_phongBanNguoiGiao = $('.id_pb_4').val()
        var kcxl_ghi_chu = $('textarea[name="description"]').val()
        var listTable = JSON.stringify(listTable);
        id_cty
        $.ajax({
          url: '../ajax/add_new_pxk.php',
          type: 'POST',
          // dataType: 'Json',
          data: {
            kcxl_hinhThuc: kcxl_hinhThuc,
            kcxl_nguoiTao: kcxl_nguoiTao,
            kcxl_ngayTao: kcxl_ngayTao,
            kcxl_donHang: kcxl_donHang,
            kcxl_trangThai: kcxl_trangThai,
            kcxl_ngayHoanThanh: kcxl_ngayHoanThanh,
            kcxl_khoXuat: kcxl_khoXuat,
            kcxl_ngayXuatKho: kcxl_ngayXuatKho,
            kcxl_ngayYeuCauHoanThanh: kcxl_ngayYeuCauHoanThanh,
            kcxl_nguoiThucHien: kcxl_nguoiThucHien,
            kcxl_phongBanNguoiGiao: kcxl_phongBanNguoiGiao,
            kcxl_ghi_chu: kcxl_ghi_chu,
            listTable: listTable,
            id_cty: id_cty,
          },
          success: function (data) {
            var text = $('#add_new_export_note_success .p_add_succ').text('');
            var text_new = '';
            text_new += 'Thêm mới phiếu xuất kho theo đơn hàng thành công!';
            text.append(text_new);
            $('#add_new_export_note_success').show()
          }
        })
      } else {
        alert("Vui lòng nhập đầy đủ thông tin!!!");
      }
    }
  }
  if ($('.select_export_form').val() === 'XK5') {
    var listTable = []
    $(".nhap_sl_3").each(function () {
      var data = $(this).parent().parent().find($('.id_vt')).attr('data')
      var soluong = $(this).val()
      listTable.push({
        'id': data,
        'soluong': soluong,
      })
    })
    var check_empty_arr = [];
    for (var i = 0; i < listTable.length; i++) {
      var check_empty = listTable[i].soluong;
      if (check_empty == "") {
        check_empty_arr.push(listTable[i].soluong);
      }
    }
    var length_soluong_empty = check_empty_arr.length;
    if ($('#select_status5').val() === '1') {
      form_add.validate({
        errorPlacement: function (error, element) {
          error.appendTo(element.parents(".choose_select_export_kho_5"));
          error.appendTo(element.parents(".select_date_xuat_6"));
          error.wrap("<span class='error'>")
        },
        rules: {
          select_export_kho_5: "required",
          date_xuat_6: "required",
        },
        messages: {
          select_export_kho_5: "Vui lòng chọn kho xuất",
          date_xuat_6: "Vui lòng chọn ngày xuất kho",
        },
      });
      if (form_add.valid() === true && length_soluong_empty == 0) {
        var kcxl_hinhThuc = $('select[name="select_export_form"]').val()
        var kcxl_nguoiTao = $('.nguoi_tao').attr('data')
        var kcxl_ngayTao = $('.ngay_tao').attr('data')
        var kcxl_trangThai = $('select[name="select_status5"]').val()
        var kcxl_khoXuat = $('select[name="select_export_kho_5"]').val()
        var kcxl_ngayXuatKho = $('input[name = "date_xuat_6"]').val()
        var kcxl_nguoiThucHien = $('select[name="select_deliver_5"]').val()
        var kcxl_phongBanNguoiGiao = $('.id_pbg_5').val()
        var kcxl_nguoiNhan = $('select[name="select_receiver_5"]').val()
        var kcxl_phongBanNguoiNhan = $('.id_pbn_5').val()
        var kcxl_ghi_chu = $('textarea[name="description"]').val()
        var listTable = JSON.stringify(listTable);
        id_cty
        $.ajax({
          url: '../ajax/add_new_pxk.php',
          type: 'POST',
          // dataType: 'Json',
          data: {
            kcxl_hinhThuc: kcxl_hinhThuc,
            kcxl_nguoiTao: kcxl_nguoiTao,
            kcxl_ngayTao: kcxl_ngayTao,
            kcxl_trangThai: kcxl_trangThai,
            kcxl_khoXuat: kcxl_khoXuat,
            kcxl_ngayXuatKho: kcxl_ngayXuatKho,
            kcxl_nguoiThucHien: kcxl_nguoiThucHien,
            kcxl_phongBanNguoiGiao: kcxl_phongBanNguoiGiao,
            kcxl_nguoiNhan: kcxl_nguoiNhan,
            kcxl_phongBanNguoiNhan: kcxl_phongBanNguoiNhan,
            kcxl_ghi_chu: kcxl_ghi_chu,
            listTable: listTable,
            id_cty: id_cty,
          },
          success: function (data) {
            var text = $('#add_new_export_note_success .p_add_succ').text('');
            var text_new = '';
            text_new += 'Thêm mới phiếu xuất kho thành công!';
            text.append(text_new);
            $('#add_new_export_note_success').show()
          }
        })
      } else {
        alert("Vui lòng nhập đầy đủ thông tin!!!");
      }
    }
    if ($('#select_status5').val() === '2') {
      var form_add = $("#create_export_kho");
      form_add.validate({
        errorPlacement: function (error, element) {
          error.appendTo(element.parents(".choose_select_export_kho_5"));
          error.appendTo(element.parents(".tb_date_xuat_6"));
          error.appendTo(element.parents(".date_accses_5"));
          error.wrap("<span class='error'>")
        },
        rules: {
          select_export_kho_5: "required",
          date_xuat_6: "required",
          date_accses_export_5: "required",
        },
        messages: {
          select_export_kho_5: "Vui lòng chọn kho xuất",
          date_xuat_6: "Vui lòng chọn ngày xuất kho",
          date_accses_export_5: "Vui lòng chọn ngày hoàn thành",
        },
      });
      if (form_add.valid() === true && length_soluong_empty == 0) {
        var kcxl_hinhThuc = $('select[name="select_export_form"]').val()
        var kcxl_nguoiTao = $('.nguoi_tao').attr('data')
        var kcxl_ngayTao = $('.ngay_tao').attr('data')
        var kcxl_trangThai = $('select[name="select_status5"]').val()
        var kcxl_ngayHoanThanh = $('input[name="date_accses_export_5"]').val()
        var kcxl_khoXuat = $('select[name="select_export_kho_5"]').val()
        var kcxl_ngayXuatKho = $('input[name = "date_xuat_6"]').val()
        var kcxl_nguoiThucHien = $('select[name="select_deliver_5"]').val()
        var kcxl_phongBanNguoiGiao = $('.id_pbg_5').val()
        var kcxl_nguoiNhan = $('select[name="select_receiver_5"]').val()
        var kcxl_phongBanNguoiNhan = $('.id_pbn_5').val()
        var kcxl_ghi_chu = $('textarea[name="description"]').val()
        var listTable = JSON.stringify(listTable);
        id_cty
        $.ajax({
          url: '../ajax/add_new_pxk.php',
          type: 'POST',
          // dataType: 'Json',
          data: {
            kcxl_hinhThuc: kcxl_hinhThuc,
            kcxl_nguoiTao: kcxl_nguoiTao,
            kcxl_ngayTao: kcxl_ngayTao,
            kcxl_trangThai: kcxl_trangThai,
            kcxl_ngayHoanThanh: kcxl_ngayHoanThanh,
            kcxl_khoXuat: kcxl_khoXuat,
            kcxl_ngayXuatKho: kcxl_ngayXuatKho,
            kcxl_nguoiThucHien: kcxl_nguoiThucHien,
            kcxl_phongBanNguoiGiao: kcxl_phongBanNguoiGiao,
            kcxl_nguoiNhan: kcxl_nguoiNhan,
            kcxl_phongBanNguoiNhan: kcxl_phongBanNguoiNhan,
            kcxl_ghi_chu: kcxl_ghi_chu,
            listTable: listTable,
            id_cty: id_cty,
          },
          success: function (data) {
            var text = $('#add_new_export_note_success .p_add_succ').text('');
            var text_new = '';
            text_new += 'Thêm mới phiếu xuất kho thành công!';
            text.append(text_new);
            $('#add_new_export_note_success').show()
          }
        })
      } else {
        alert("Vui lòng nhập đầy đủ thông tin!!!");
      }
    }
  }
})

function tong_vt(id) {
  var sl = Number($(id).val());
  var dg = $(id).parent().parent().find('.don_gia_3').text();
  var don_gia = dg.replaceAll('.', '');
  var tong = sl * don_gia;
  var thanh_tien = formatNumber(tong);
  console.log(dg)
  $(id).parent().parent().find('.thanh_tien_3').text(thanh_tien);
};

function formatDateVN(date) {
  var currentMonth = (date.getMonth() + 1);
  var currentdate = date.getDate();
  if (currentdate < 10) {
    currentdate = '0' + currentdate;
  }
  if (currentMonth < 10) {
    currentMonth = '0' + currentMonth;
  }
  var formatDate = date.getFullYear() + '-' + currentMonth + '-' + currentdate;
  return formatDate;
}

function toTimestamp(strDate) {
  var datum = Date.parse(strDate);
  return datum / 1000;
}

function resetFormValidator(formId) {
  $(formId).removeData('validator');
  $(formId).removeData('unobtrusiveValidation');
  $.validator.unobtrusive.parse(formId);
}

function change_value() {
  $(".select_tb_3").change(function () {
    var value = $(this).val();
    var _this = $(this);
    var value1 = $('.select_export_kho_5').val()
    var id_cty = id_ct
    $.ajax({
      url: '../ajax/get_dsvt_kho.php',
      type: 'POST',
      data: {
        id_vt: value,
        id_kho: value1,
        id_cty: id_cty
      },
      success: function (data) {
        _this.parent().parent().html(data);
      }
    })
  })
}