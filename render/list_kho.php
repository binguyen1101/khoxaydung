<?php
include("config.php");

$stt = 1;

if (isset($_COOKIE['user']) && $_COOKIE['user'] != "" && $_COOKIE['role'] == 1) {
    $token = $_COOKIE['acc_token'];
    $curl = curl_init();
    $data = array();
    curl_setopt($curl, CURLOPT_POST, 1);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
    curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/user_info_company.php');
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));

    $response = curl_exec($curl);
    curl_close($curl);
    $data_tt = json_decode($response, true);
    $tt_user = $data_tt['data']['user_info_result'];
    // $id_nguoi_xoa = $_SESSION['com_id'];
}

if (isset($_COOKIE['user']) && $_COOKIE['user'] != "" && $_COOKIE['role'] == 2) {
    $token = $_COOKIE['acc_token'];
    $curl = curl_init();
    $data = array();
    curl_setopt($curl, CURLOPT_POST, 1);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
    curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/user_info_employee.php');
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));
    $response = curl_exec($curl);
    curl_close($curl);
    $data_tt = json_decode($response, true);
    $tt_user = $data_tt['data']['user_info_result'];
}

$id_cty = $tt_user['com_id'];

if (isset($_COOKIE['acc_token']) && isset($_COOKIE['rf_token']) && isset($_COOKIE['role']) && $_COOKIE['role'] == 2) {
    $curl = curl_init();
    $token = $_COOKIE['acc_token'];
    curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/list_all_my_partner.php?get_all=true');
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));
    $response = curl_exec($curl);
    curl_close($curl);

    $data_list = json_decode($response, true);
    $data_list_nv = $data_list['data']['items'];
} else {
    $curl = curl_init();
    $token = $_COOKIE['acc_token'];
    curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/list_all_employee_of_company.php');
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));
    $response = curl_exec($curl);
    curl_close($curl);

    $data_list = json_decode($response, true);
    $data_list_nv = $data_list['data']['items'];
}
$count = count($data_list_nv);

$user = [];
for ($i = 0; $i < count($data_list_nv); $i++) {
    $nv = $data_list_nv[$i];
    $user[$nv["ep_id"]] = $nv;
}
// print_r($data_list_nv);
// die();


$page = $_POST['page'];
$sl = getValue('curr','int','POST', '');
$curr = $sl;
$start = ($page - 1) * $curr;
$start = abs($start);

$input_val = $_POST['input_val'];
$input_val = trim($input_val);

$so_phieu = explode('-', $input_val)[0];
$id_so_phieu = explode('-', $input_val)[1];

$arr = [];
$length_id_phieu = strlen($id_so_phieu);
for ($push = 0; $push < $length_id_phieu; $push++) {
    $ki_tu = substr($id_so_phieu, $push, 1);
    array_push($arr, $ki_tu);
}

$count_arr = count($arr);
$kcxl_id = '';

if ($arr[0] > 0) {
    $kcxl_id = implode('', $arr);
} elseif ($arr[0] == 0) {
    for ($j = 1; $j < $count_arr; $j++) {
        if ($arr[$j] > 0) {
            $kcxl_id = substr(implode('', $arr), $j, $count_arr - $j);
            break;
        }
    }
}

$all_kho = new db_query("SELECT * FROM `kho` WHERE `kho_id_ct`= $id_cty LIMIT $start,$curr");

if ($input_val != "") {
    $all_kho = new db_query("SELECT * FROM `kho`
        WHERE ((`kho_id` = '$kcxl_id') OR (`kho_name` = '$input_val') OR (`kho_name` LIKE '%$input_val%')) AND `kho_id_ct` = $id_cty ORDER BY `kho_id` DESC LIMIT $start,$curr");

    $total_sql =  "SELECT COUNT(`kho_id`) AS total FROM `kho` 
        WHERE ((`kho_id` = '$kcxl_id') OR (`kho_name` = '$input_val') OR (`kho_name` LIKE '%$input_val%')) AND `kho_id_ct` = $id_cty";
} else {
    $all_kho = new db_query("SELECT * FROM `kho` WHERE `kho_id_ct` = $id_cty ORDER BY `kho_id` DESC LIMIT $start,$curr");
    $total_sql =  "SELECT COUNT(`kho_id`) AS total FROM `kho` WHERE `kho_id_ct` = $id_cty";
}

$total_sql = new db_query($total_sql);
$total = mysql_fetch_assoc($total_sql->result)['total'];

if ($input_val == "" && $curr == 10) {
    $url = '/danh-sach-kho.html?dis='.$curr;
} else {
    $url = '/danh-sach-kho.html?input='.$input_val."&dis=".$curr;
}

// API công trình
$curl = curl_init();
curl_setopt($curl, CURLOPT_POST, 1);
curl_setopt($curl, CURLOPT_URL, 'https://phanmemquanlycongtrinh.timviec365.vn/api/congtrinh.php');
curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
curl_setopt($curl, CURLOPT_POSTFIELDS, [
    'id_com' => $id_cty,
]);
$response = curl_exec($curl);
curl_close($curl);
$data_list = json_decode($response, true);
$list_phieu_vt = $data_list['data']['items'];
$list_phieu_vt1 = [];
for ($i = 0; $i < count($list_phieu_vt); $i++) {
    $ctr = $list_phieu_vt[$i];
    $list_phieu_vt1[$ctr["ctr_id"]] = $ctr;
}
// echo "<pre>";
// print_r($list_phieu_vt1); die();
// echo "</pre>";
?>
<div class="main_kho position_r d_flex align_c">
    <div class="main_kho box_main_kho table_vt_scr" onscroll="table_scroll(this)">
        <table class="table table_ds_kho" style="width:2071px;">
            <thead>
                <tr class="tittle_ds_kho font_w500 font_s16 line_h19">
                    <td>STT
                        <span class="span_thead"></span>
                    </td>
                    <td>Mã kho
                        <span class="span_thead"></span>
                    </td>
                    <td>Tên kho
                        <span class="span_thead"></span>
                    </td>
                    <td>Công trình
                        <span class="span_thead"></span>
                    </td>
                    <td>Nhân viên quản lí kho
                        <span class="span_thead"></span>
                    </td>
                    <td>Địa chỉ
                        <span class="span_thead"></span>
                    </td>
                    <td>
                        Mô tả
                        <?php if (in_array(3, $ro_ton_kho) || in_array(4, $ro_ton_kho)) { ?>
                            <span class="span_thead"></span>
                        <? } ?>
                    </td>
                    <?php if (in_array(3, $ro_ton_kho) || in_array(4, $ro_ton_kho)) { ?>
                        <td>Chức năng
                        </td>
                    <? } ?>
                </tr>
            </thead>
            <tbody>
                <? while ($item = mysql_fetch_assoc($all_kho->result)) { ?>
                    <tr class="info_k" data-id="<?= $item['kho_id'] ?>">
                        <td><?= $stt++ ?></td>
                        <td class="color_gray">KHO - <?= $item['kho_id'] ?></td>
                        <td><a class="ten_kho font_wB color_blue" href="danh-sach-kho-chi-tiet-<?= $item['kho_id'] ?>.html"> <?= $item['kho_name'] ?></a></td>
                        <td class="ten_cong_trinh"><?= $list_phieu_vt1[$item['kho_congTrinh']]['ctr_name'] ?></td>
                        <td style="text-align: left;" class="ten_nguoi_ql" data-userCr="<?= $item['kho_nhanVienQuanLiKho'] ?>">
                            <?php
                            $nguoi_ql = $item['kho_nhanVienQuanLiKho'];
                            $user_id = $user[$nguoi_ql];
                            $ten_nguoi_ql = $user_id['ep_name'];
                            $anh_nguoi_ql = $user_id['ep_image'];
                            if ($anh_nguoi_ql == "") {
                                $anh = '../images/ava_ad.png';
                            } else {
                                $anh = 'https://chamcong.24hpay.vn/upload/employee/' . $anh_nguoi_ql;
                            }
                            ?>
                            <div class="d_flex flex_start align_c">
                                <img src="<?= $anh; ?>" alt="">
                                <p><?= $ten_nguoi_ql; ?></p>
                            </div>
                        </td>
                        <td class="font_s14 line_h17 dia_chi"><?= $item['kho_address'] ?></td>
                        <td class="mo_ta"><?= $item['kho_description'] ?></td>
                        <?php if (in_array(3, $ro_ton_kho) || in_array(4, $ro_ton_kho)) { ?>
                            <td>
                                <p class="d_flex align_c flex_center">
                                    <?php if (in_array(3, $ro_ton_kho)) { ?>
                                        <img src="../images/pen_blu.png" alt="">
                                        <a class="a_edit color_blue font_s14 line_h17 font_w500 cursor_p" onclick="click_edit(this)">Sửa</a>
                                    <? } ?>
                                    <?php if (in_array(3, $ro_ton_kho) && in_array(4, $ro_ton_kho)) { ?>
                                        <span class="color_blue margin_lr5">|</span>
                                    <? } ?>
                                    <?php if (in_array(4, $ro_ton_kho)) { ?>
                                        <img src="../images/delete_r.png" alt="">
                                        <a class="a_del color_red font_s14 line_h17 font_w500 cursor_p" onclick="xoa(this)" data="<?= $item['kho_id'] ?>">Xóa</a>
                                    <? } ?>
                                </p>
                            </td>
                        <? } ?>
                    </tr>
                <? } ?>
            </tbody>
        </table>
    </div>
    <div class="pre_q d_flex align_c flex_center position_a display_none" onclick="pre_q(this)">
        <span class="pre_arrow"></span>
    </div>
    <div class="next_q d_flex align_c flex_center position_a" onclick="next_q(this)">
        <span class="next_arrow"></span>
    </div>
</div>
<div class="w_navigation d_flex space_b align_c">
    <div class="l_nav d_flex align_c">
        <p class="color_grey font_s14 line_h17 font_w400">Hiển thị:</p>
        <select name="" id="" class="show_tr_tb color_grey font_s14 line_h17 font_w400" onchange="display(this)">
            <option value="10" <?= ($curr == 10) ? "selected" : ""?>>10</option>
            <option value="20" <?= ($curr == 20) ? "selected" : ""?>>20</option>
            <option value="30" <?= ($curr == 30) ? "selected" : ""?>>30</option>
            <option value="40" <?= ($curr == 40) ? "selected" : ""?>>40</option>
        </select>
    </div>
    <div class="r_nav">
        <ul class="d_flex font_s13 line_h15 font_wN">
            <?php
                echo generatePageBar3('', $page, $curr, $total, $url, '&', '', 'paging_detail', 'preview', '<', 'next', '>', '', '<<<', '', '>>>');
            ?>
        </ul>
    </div>
</div>
<script>
    $('.show_tr_tb').select2({
        minimumResultsForSearch: -1
    });
</script>