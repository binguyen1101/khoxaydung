<?php 
  include("config.php");

  $kho_id = getValue('id', 'int', 'POST','');
  $id_cty = getValue('id_cty', 'int', 'POST','');

  if(isset($_COOKIE['acc_token']) && isset($_COOKIE['rf_token']) && isset($_COOKIE['role']) && $_COOKIE['role'] == 2){
      $curl = curl_init();
      $token = $_COOKIE['acc_token'];
      curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/list_all_my_partner.php?get_all=true');
      curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
      curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer '.$token));
      $response = curl_exec($curl);
      curl_close($curl);

      $data_list = json_decode($response,true);
      $data_list_nv =$data_list['data']['items'];
  }else{
      $curl = curl_init();
      $token = $_COOKIE['acc_token'];
      curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/list_all_employee_of_company.php');
      curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
      curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer '.$token));
      $response = curl_exec($curl);
      curl_close($curl);

      $data_list = json_decode($response,true);
      $data_list_nv =$data_list['data']['items'];
  }  

  $info_kho = mysql_fetch_assoc((new db_query("SELECT `kho_id`, `kho_maKho`, `kho_name`, `kho_congTrinh`, `kho_address`, `kho_nhanVienQuanLiKho`, `kho_description` 
  FROM `kho` WHERE `kho_id` = $kho_id AND `kho_id_ct` = $id_cty")) -> result);

  // API công trình
  $curl = curl_init();
  curl_setopt($curl, CURLOPT_POST, 1);
  curl_setopt($curl, CURLOPT_URL, 'https://phanmemquanlycongtrinh.timviec365.vn/api/congtrinh.php');
  curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
  curl_setopt($curl, CURLOPT_POSTFIELDS, [
      'id_com' => $id_cty,
  ]);
  $response = curl_exec($curl);
  curl_close($curl);
  $data_list = json_decode($response, true);
  $list_phieu_vt = $data_list['data']['items'];
  $list_phieu_vt1 = [];
  for ($i = 0; $i < count($list_phieu_vt); $i++) {
      $ctr = $list_phieu_vt[$i];
      $list_phieu_vt1[$ctr["ctr_id"]] = $ctr;
  }
?>

<div class="d_flex flex_column" style="display: none;">
    <p class="color_grey font_s15 line_h18 font_w500">Mã kho</p>
    <input class="color_grey font_s14 line_h17 font_w400" type="text" value="NVT-0000" disabled="disabled">
</div>
<div class="d_flex flex_column name_wh">
    <p class="color_grey font_s15 line_h18 font_w500">Tên kho<span style="color: red;">*</span></p>
    <input name="name_wh" class="color_grey font_s14 line_h17 font_w400" type="text" value="<?=$info_kho['kho_name']?>" placeholder="Nhập tên kho">
</div>
<div class="d_flex flex_column">
    <p class="color_grey font_s15 line_h18 font_w500 ">Công trình</p>
    <select class="select_construction" name="wh_construction">
        <option value="">Chọn công trình</option>
        <?php foreach($list_phieu_vt1 as $val_ct){ ?>
            <option value="<?= $val_ct['ctr_id']?>" <?=($info_kho['kho_congTrinh'] == $val_ct['ctr_id']) ? "selected" : ""?>><?= $val_ct['ctr_name']?></option>
        <?php } ?>
    </select>
</div>
<div class="d_flex flex_column address_wh">
    <p class="color_grey font_s15 line_h18 font_w500">Địa chỉ<span style="color: red;">*</span></p>
    <input name="address_wh" class="color_grey font_s14 line_h17 font_w400" type="text" value="<?=$info_kho['kho_address']?>" placeholder="Nhập địa chỉ kho">
</div>
<div class="d_flex flex_column staff_wh">
    <p class="color_grey font_s15 line_h18 font_w500">Nhân viên quản lí kho<span style="color: red;">*</span></p>
    <select class="select_staff" name="staff_wh">
        <option value=""> Chọn nhân viên</option>
        <? foreach ($data_list_nv as $item) { ?>
            <option value="<?= $item['ep_id'] ?>" <?=($info_kho['kho_nhanVienQuanLiKho'] == $item['ep_id']) ? "selected" : ""?> ><?= $item['ep_name'] ?></option>
        <? } ?>                            
    </select>
</div>
<div class="d_flex flex_column">
    <p class="color_grey font_s15 line_h18 font_w500">Mô tả </p>
    <textarea name="description_wh" id="" rows="5" placeholder="Nhập nội dung"><?=$info_kho['kho_description']?></textarea>
</div>
<div class="btn_ct_pp d_flex flex_center">
    <button type="button" class="btn_cancel back_w color_blue font_s15 line_h18 font_w500 cursor_p" onclick="close_popup()">Hủy</button>
    <button type="button" class="btn_save btn_edit_kho back_blue color_white font_s15 line_h18 font_w500 cursor_p" onclick="sua()">Đồng ý</button>
</div>
<script>
    $('.select_staff').select2();
    $('.select_construction').select2();
</script>