<?php

    include("config.php");

    if (isset($_COOKIE['user']) && $_COOKIE['user'] != "" && $_COOKIE['role'] == 1) {
		$token = $_COOKIE['acc_token'];
		$curl = curl_init();
		$data = array();
		curl_setopt($curl, CURLOPT_POST, 1);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
		curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/user_info_company.php');
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));
	
		$response = curl_exec($curl);
		curl_close($curl);
		$data_tt = json_decode($response, true);
		$tt_user = $data_tt['data']['user_info_result'];
	} else if (isset($_COOKIE['user']) && $_COOKIE['user'] != "" && $_COOKIE['role'] == 2) {
		$token = $_COOKIE['acc_token'];
		$curl = curl_init();
		$data = array();
		curl_setopt($curl, CURLOPT_POST, 1);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
		curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/user_info_employee.php');
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));
		$response = curl_exec($curl);
		curl_close($curl);
		$data_tt = json_decode($response, true);
		$tt_user = $data_tt['data']['user_info_result'];
	}
    
    $id_cty = $tt_user['com_id'];


    $kho = getValue('kho','int','POST','');
    // $kho = $_POST['kho'];
    $ngay_tao = $_POST['ngay_tao'];
    // $sort = getValue('sort', 'int', 'POST', '');

    $page = getValue('page', 'int', 'POST', 1);
    $sl = getValue('curr','int','POST', '');
    $curr = $sl;

    $input_val = $_POST['input_val'];

    $input_val = trim($input_val);

    $so_phieu = explode('-',$input_val)[0];
    $id_so_phieu = explode('-',$input_val)[1];

    $arr = [];
    $length_id_phieu = strlen($id_so_phieu);
    for($push = 0; $push<$length_id_phieu; $push++){
        $ki_tu = substr($id_so_phieu,$push,1);
        array_push($arr,$ki_tu);
    }

    $count_arr = count($arr);
    $kcxl_id = '';

    if($arr[0] > 0){
        $kcxl_id = implode('',$arr);
    }
    elseif($arr[0] == 0){
        for($j = 1; $j < $count_arr; $j++){
            if($arr[$j] > 0){
                $kcxl_id = substr(implode('',$arr),$j ,$count_arr-$j);
                break;
            }
        }
    }

    if($input_val != ""){
        $bao_cao_ton_kho = "SELECT DISTINCT `dsvt_id`, `dsvt_maVatTuThietBi`, `dsvt_name`, `dsvt_dateCreate`, `dsvt_kho`, `dsvt_soLuongTon`,
        `dvt_name`, `hsx_name`, `xx_name`
        FROM `danh-sach-vat-tu` 
        LEFT JOIN `don-vi-tinh` ON `danh-sach-vat-tu`.`dsvt_donViTinh` = `don-vi-tinh`.`dvt_id` 
        LEFT JOIN `hang-san-xuat` ON `danh-sach-vat-tu`.`dsvt_hangSanXuat` = `hang-san-xuat`.`hsx_id` 
        LEFT JOIN `xuat-xu` ON `danh-sach-vat-tu`.`dsvt_xuatXu` = `xuat-xu`.`xx_id` 
        LEFT JOIN `nhom-vat-tu-thiet-bi` ON `danh-sach-vat-tu`.`dsvt_nhomVatTuThietBi` = `nhom-vat-tu-thiet-bi`.`nvt_id`
        LEFT JOIN `so-luong-vat-tu` ON `danh-sach-vat-tu`.`dsvt_id` = `so-luong-vat-tu`.`slvt_maVatTuThietBi`
        WHERE ((`dsvt_check` = 1 AND `dvt_check` = 1 AND `dsvt_maVatTuThietBi` = '$so_phieu' AND (`dsvt_id` = '$kcxl_id')) OR (`dsvt_name` = '$input_val')) OR (`dsvt_id` LIKE '%$input_val%') AND `dsvt_id_ct` = $id_cty ";
     }else {
        $bao_cao_ton_kho = "SELECT DISTINCT `dsvt_id`, `dsvt_maVatTuThietBi`, `dsvt_name`, `dsvt_dateCreate`, `dsvt_kho`, `dsvt_soLuongTon`,
        `dvt_name`, `hsx_name`, `xx_name`
        FROM `danh-sach-vat-tu` 
        LEFT JOIN `don-vi-tinh` ON `danh-sach-vat-tu`.`dsvt_donViTinh` = `don-vi-tinh`.`dvt_id` 
        LEFT JOIN `hang-san-xuat` ON `danh-sach-vat-tu`.`dsvt_hangSanXuat` = `hang-san-xuat`.`hsx_id` 
        LEFT JOIN `xuat-xu` ON `danh-sach-vat-tu`.`dsvt_xuatXu` = `xuat-xu`.`xx_id` 
        LEFT JOIN `nhom-vat-tu-thiet-bi` ON `danh-sach-vat-tu`.`dsvt_nhomVatTuThietBi` = `nhom-vat-tu-thiet-bi`.`nvt_id`
        LEFT JOIN `so-luong-vat-tu` ON `danh-sach-vat-tu`.`dsvt_id` = `so-luong-vat-tu`.`slvt_maVatTuThietBi`
        WHERE `dsvt_check` = 1 AND `dvt_check` = 1 AND `dsvt_id_ct` = $id_cty ";
    }



    $order_by = "ORDER BY `dsvt_id` DESC ";
    // $limited = "LIMIT $start,$curr";    

    if($ngay_tao == ""){
        $nt = "";
    }else {
        $nt = " AND `dsvt_dateCreate` = '$ngay_tao'";
    }

    // if($sort == ""){
    //     $dk_sort = "ORDER BY `dsvt_id` DESC ";
    // }else if($sort == 1){
    //     $dk_sort = "ORDER BY `dsvt_donGia` ASC ";
    // }else if($sort == 2){
    //     $dk_sort = "ORDER BY `dsvt_donGia` DESC ";
    // }

    if($kho == "" && $ngay_tao == "" && $curr == 10){
    // if($kho == "" && $ngay_tao == "" && $sort == ""){
        $url = '/bao-cao-ton-kho.html?dis='.$curr;
    }else{
        $url = "/bao-cao-ton-kho.html?kho=".$kho."&ngay_tao=".$ngay_tao."&dis=".$curr;
        // $url = "/bao-cao-ton-kho.html?kho=".$kho."&ngay_tao=".$ngay_tao."&sort=".$sort;
    }

    $bao_cao_ton_kho .= $nt;
    $bao_cao_ton_kho .= $order_by;
    $bao_cao_ton_kho = new db_query($bao_cao_ton_kho);

    $responsive = [];
    $i = 0 ;
    while (($item = mysql_fetch_assoc($bao_cao_ton_kho->result))) {
        $check_kho = explode(',',$item['dsvt_kho']);
        if(in_array($kho,$check_kho)){
            $info_k['dsvt_id'] = $item['dsvt_id'];
            $info_k['dsvt_name'] = $item['dsvt_name'];
            $info_k['dsvt_soLuongTon'] = $item['dsvt_soLuongTon'];
            $info_k['hsx_name'] = $item['hsx_name'];
            $info_k['dvt_name'] = $item['dvt_name'];
            $info_k['xx_name'] = $item['xx_name'];
            $info_k['dsvt_kho'] = $item['dsvt_kho'];
            $responsive[$i] = $info_k;
            $i++;
        }

    }

    if($page>0){
        $count = count($responsive);

        $kho_p = [];
        if($page > 0){
            for($j = ($curr*($page-1)); $j < $curr*$page; $j++){
                $kho_item = $responsive[$j];
                $kho_p[$j] = $kho_item;
                if(empty($responsive[$j+1])) break;
            }
        }

        foreach ($kho_p as $key => $value) {
            if (empty($value)) {
               unset($kho_p[$key]);
            }
        }
    }
?>

<div class="position_r d_flex align_c">
    <div class="table_vt_scr" onscroll="table_scroll(this)">
        <div class="table_vt">
        <table>
            <tr class="tit_tbl color_white font_s16 line_h19 font_w500 back_blue">
            <th>STT
                <span class="span_tbody"></span>
            </th>
            <th>Mã vật tư thiết bị
                <span class="span_tbody"></span>
            </th>
            <th>Tên đầy đủ vật tư thiết bị
                <span class="span_tbody"></span>
            </th>
            <th>Đơn vị tính
                <span class="span_tbody"></span>
            </th>
            <th>Hãng sản xuất
                <span class="span_tbody"></span>
            </th>
            <th>Xuất xứ
                <span class="span_tbody"></span>
            </th>
            <th>Số lượng
            </th>
            </tr>
            <?php if(!empty($kho_p)){
            $i=1; foreach ($kho_p as $val){ ?>
            <tr class="color_grey font_s14 line_h17 font_w400" data-id="<?= $val['dsvt_id']; ?>">
                <td><?= $i++; ?></td>
                <td>VT - <?=$val['dsvt_id'];?></td>
                <td style="text-align: left;"><a href="/danh-sach-vat-tu-thiet-bi-chi-tiet-<?= $val['dsvt_id']; ?>.html" class="color_blue font_w500"><?= $val['dsvt_name'];?></a></td>
                <td><?= $val['dvt_name'];?></td>
                <td style="text-align: left;"><?= $val['hsx_name'];?></td>
                <td><?= $val['xx_name'];?></td>
                <?php $sl = json_decode($val['dsvt_soLuongTon']); ?>
                <td><?= $sl->$kho ?></td>
            </tr>
            <?php }}?>
        </table>
        </div>
    </div>
    <div class="pre_q d_flex align_c flex_center position_a display_none" onclick="pre_q(this)">
        <span class="pre_arrow"></span>
    </div>
    <div class="next_q d_flex align_c flex_center position_a display_none" onclick="next_q(this)">
        <span class="next_arrow"></span>
    </div>
</div>
<div class="w_navigation d_flex space_b align_c">
    <div class="l_nav d_flex align_c">
        <p class="color_grey font_s14 line_h17 font_w400">Hiển thị:</p>
        <select name="" id="" class="show_tr_tb color_grey font_s14 line_h17 font_w400" onchange="display(this)">
            <option value="10" <?= ($curr == 10) ? "selected" : ""?>>10</option>
            <option value="20" <?= ($curr == 20) ? "selected" : ""?>>20</option>
            <option value="30" <?= ($curr == 30) ? "selected" : ""?>>30</option>
            <option value="40" <?= ($curr == 40) ? "selected" : ""?>>40</option>
        </select>
    </div>
    <div class="r_nav">
        <ul class="d_flex font_s13 line_h15 font_wN">
            <?php
                echo generatePageBar3('', $page, $curr, $count, $url, '&', '', 'paging_detail', 'preview', '<', 'next', '>', '', '<<<', '', '>>>');
            ?>
        </ul>
    </div>
</div>
<script>
    $('.show_tr_tb').select2({
        minimumResultsForSearch: -1
    });
</script>
