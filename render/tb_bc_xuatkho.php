<?php

    include("config.php");

    if(isset($_COOKIE['acc_token']) && isset($_COOKIE['rf_token']) && isset($_COOKIE['role']) && $_COOKIE['role'] == 2){
        $curl = curl_init();
        $token = $_COOKIE['acc_token'];
        curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/list_all_my_partner.php?get_all=true');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer '.$token));
        $response = curl_exec($curl);
        curl_close($curl);

        $data_list = json_decode($response,true);
        $data_list_nv =$data_list['data']['items'];
    }else{
        $curl = curl_init();
        $token = $_COOKIE['acc_token'];
        curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/list_all_employee_of_company.php');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer '.$token));
        $response = curl_exec($curl);
        curl_close($curl);

        $data_list = json_decode($response,true);
        $data_list_nv =$data_list['data']['items'];

    }
    $count = count($data_list_nv);

    $user = [];
    for ($i = 0; $i < count($data_list_nv); $i++){
        $nv = $data_list_nv[$i];
        $user[$nv["ep_id"]] = $nv;
    }

    if (isset($_COOKIE['user']) && $_COOKIE['user'] != "" && $_COOKIE['role'] == 1) {
		$token = $_COOKIE['acc_token'];
		$curl = curl_init();
		$data = array();
		curl_setopt($curl, CURLOPT_POST, 1);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
		curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/user_info_company.php');
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));
	
		$response = curl_exec($curl);
		curl_close($curl);
		$data_tt = json_decode($response, true);
		$tt_user = $data_tt['data']['user_info_result'];
	} else if (isset($_COOKIE['user']) && $_COOKIE['user'] != "" && $_COOKIE['role'] == 2) {
		$token = $_COOKIE['acc_token'];
		$curl = curl_init();
		$data = array();
		curl_setopt($curl, CURLOPT_POST, 1);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
		curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/user_info_employee.php');
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));
		$response = curl_exec($curl);
		curl_close($curl);
		$data_tt = json_decode($response, true);
		$tt_user = $data_tt['data']['user_info_result'];
	}
    
    $id_cty = $tt_user['com_id'];

    $kho = $_POST['kho'];
    $hinhthuc = $_POST['ht'];

    $ngay_tao_start = date('Y-m-d',strtotime($_POST['ngay_tao_start'])); 
    $ngay_tao_end = date('Y-m-d',strtotime($_POST['ngay_tao_end']));

    $ngay_th_start = date('Y-m-d',strtotime($_POST['ngay_th_start'])); 
    $ngay_th_end = date('Y-m-d',strtotime($_POST['ngay_th_end']));

    $page = getValue('page','int','POST','');
    $sl = getValue('curr','int','POST', '');
    $curr = $sl;
    $start = ($page - 1 )*$curr;
    $start = abs($start);
    
    $input_val = $_POST['input_val'];

    $input_val = trim($input_val);

    $so_phieu = explode('-',$input_val)[0];
    $id_so_phieu = explode('-',$input_val)[1];

    $arr = [];
    $length_id_phieu = strlen($id_so_phieu);
    for($push = 0; $push<$length_id_phieu; $push++){
        $ki_tu = substr($id_so_phieu,$push,1);
        array_push($arr,$ki_tu);
    }

    $count_arr = count($arr);
    $kcxl_id = '';

    if($arr[0] > 0){
        $kcxl_id = implode('',$arr);
    }
    elseif($arr[0] == 0){
        for($j = 1; $j < $count_arr; $j++){
            if($arr[$j] > 0){
                $kcxl_id = substr(implode('',$arr),$j ,$count_arr-$j);
                break;
            }
        }
    }

    if($input_val != ""){
        $bao_cao_xuat_kho = "SELECT `kcxl_id`, `kcxl_soPhieu`, `kcxl_trangThai`, `kcxl_hinhThuc`, `kcxl_nguoiTao`,
        `kcxl_ngayHoanThanh`, `kho_name`, `kcxl_khoXuat`, `kcxl_ngayTao`
        FROM `kho-cho-xu-li` INNER JOIN `kho`
        ON `kho-cho-xu-li`.`kcxl_khoXuat` = `kho`.`kho_id`
        WHERE `kho-cho-xu-li`.`kcxl_check` = 1 AND `kcxl_soPhieu` = 'PXK' AND `kcxl_trangThai` = 6 AND (`kcxl_id` = '$kcxl_id' OR `kcxl_id` LIKE '%$input_val%') AND `kcxl_id_ct` = $id_cty ";

        $total_sql = "SELECT COUNT(`kcxl_id`) AS total FROM `kho-cho-xu-li` 
        INNER JOIN `kho` ON `kho-cho-xu-li`.`kcxl_khoXuat` = `kho`.`kho_id`
        WHERE `kho-cho-xu-li`.`kcxl_check` = 1 AND `kcxl_soPhieu` = 'PXK' AND `kcxl_trangThai` = 6 AND (`kcxl_id` = '$kcxl_id' OR `kcxl_id` LIKE '%$input_val%') AND `kcxl_id_ct` = $id_cty ";
    }else{
        $bao_cao_xuat_kho = "SELECT `kcxl_id`, `kcxl_soPhieu`, `kcxl_trangThai`, `kcxl_hinhThuc`, `kcxl_nguoiTao`,
        `kcxl_ngayHoanThanh`, `kho_name`, `kcxl_khoXuat`, `kcxl_ngayTao`
        FROM `kho-cho-xu-li` INNER JOIN `kho`
        ON `kho-cho-xu-li`.`kcxl_khoXuat` = `kho`.`kho_id`
        WHERE `kho-cho-xu-li`.`kcxl_check` = 1 AND `kcxl_soPhieu` = 'PXK' AND `kcxl_trangThai` = 6 AND `kcxl_id_ct` = $id_cty ";

        $total_sql =  "SELECT COUNT(`kcxl_id`) AS total FROM `kho-cho-xu-li` 
        INNER JOIN `kho` ON `kho-cho-xu-li`.`kcxl_khoXuat` = `kho`.`kho_id`
        WHERE `kho-cho-xu-li`.`kcxl_check` = 1 AND `kcxl_soPhieu` = 'PXK' AND `kcxl_trangThai` = 6 AND `kcxl_id_ct` = $id_cty ";
    }

    if($kho == ""){
        $select = "";
    }else {
        $select = " AND `kho_id` = '$kho'";
    }

    if($hinhthuc == ""){ 
        $ht = "";
    }else {
        $ht = " AND `kcxl_hinhThuc` = '$hinhthuc'";
    }

    // ngày tạo start
    if($_POST['ngay_tao_start'] != ""){
        $ngay_tao_start = date('Y-m-d',strtotime($_POST['ngay_tao_start'])); 
    }else{
        $ngay_tao_start = "";
    }
    
    // ngày tạo end
    if($_POST['ngay_tao_end'] != ""){
        $ngay_tao_end = date('Y-m-d',strtotime($_POST['ngay_tao_end']));
    }else{
        $ngay_tao_end = "";
    }

    // ngày hoàn thành start
    if($_POST['ngay_ht_start'] != ""){
        $ngay_ht_start = date('Y-m-d',strtotime($_POST['ngay_ht_start']));
    }else{
        $ngay_ht_start = "";
    }

    // ngày hoàn thành end
    if($_POST['ngay_ht_end'] != ""){
        $ngay_ht_end = date('Y-m-d',strtotime($_POST['ngay_ht_end']));
    }else{
        $ngay_ht_end = "";
    }

    if($ngay_tao_start != "" && $ngay_tao_end != ""){
        $date = "AND `kcxl_ngayTao` <= '$ngay_tao_end' AND  `kcxl_ngayTao` >= '$ngay_tao_start'";
    }elseif($ngay_tao_start != "" && $ngay_tao_end == ""){
        $date = "AND `kcxl_ngayTao` = '$ngay_tao_start' ";
    }elseif($ngay_tao_start == "" && $ngay_tao_end != ""){
        $date = "AND `kcxl_ngayTao` = '$ngay_tao_end' ";
    }

    if($ngay_ht_start != "" && $ngay_ht_end != ""){
        $date_ht = "AND `kcxl_ngayYeuCauHoanThanh` <= '$ngay_ht_end' AND  `kcxl_ngayYeuCauHoanThanh` >= '$ngay_ht_start'";
    }elseif($ngay_ht_start != "" && $ngay_ht_end == ""){
        $date_ht = "AND `kcxl_ngayYeuCauHoanThanh` = '$ngay_ht_start' ";
    }elseif($ngay_ht_start == "" && $ngay_ht_end != ""){
        $date_ht = "AND `kcxl_ngayYeuCauHoanThanh` = '$ngay_ht_end' ";
    }

    $limited = "ORDER BY `kcxl_ngayHoanThanh` DESC LIMIT $start,$curr"; 

    $bao_cao_xuat_kho .= $select;
    $bao_cao_xuat_kho .= $ht;
    $bao_cao_xuat_kho .= $date;
    $bao_cao_xuat_kho .= $date_fn;
    $bao_cao_xuat_kho .= $limited;
    $bao_cao_xuat_kho = new db_query($bao_cao_xuat_kho);   

    $total_sql .= $select;
    $total_sql .= $ht;
    $total_sql .= $date;
    $total_sql .= $date_fn;

    $total_sql1 = new db_query($total_sql);
    $total = mysql_fetch_assoc($total_sql1->result)['total'];

    if($kho == "" && $page != "" && $hinhthuc == "" && $ngay_tao_start == "" && $ngay_tao_end == "" && $ngay_ht_start == "" && $ngay_ht_end == "" && $input_val == "" && $curr == 10){
        $url = "/bao-cao-xuat-kho.html?dis=".$curr;
    }else{
        $url = "/bao-cao-xuat-kho.html?kho=".$kho."&hinhthuc=".$ht."&ngts=".$ngay_tao_start."&ngte=".$ngay_tao_end."&ngths=".$ngay_ht_start."&ngthe=".$ngay_ht_end."&input=".$input_val."&dis=".$curr;
    }


?>


<div class="position_r d_flex align_c">
    <div class="table_vt_scr">
        <div class="table_vt">
        <table>
            <tr class="tit_tbl color_white font_s16 line_h19 font_w500 back_blue">
            <th>STT
                <span class="span_tbody"></span>
            </th>
            <th>Số phiếu
                <span class="span_tbody"></span>
            </th>
            <th>Trạng thái
                <span class="span_tbody"></span>
            </th>
            <th>Hình thức nhập kho
                <span class="span_tbody"></span>
            </th>
            <th style="width: 225px">Người tạo
                <span class="span_tbody"></span>
            </th>
            <th>Ngày tạo
                <span class="span_tbody"></span>
            </th>
            <th>Ngày hoàn thành
            </th>
            </tr>
            <?php $i=1; while($row = mysql_fetch_assoc($bao_cao_xuat_kho->result)) : ?>
            <tr class="color_grey font_s14 line_h17 font_w400">
                <td><?= $i++; ?></td> 
                <td><a href="/xuat-kho-chi-tiet-<?= $row['kcxl_id']; ?>.html" class="color_blue font_w500">PXK - <?=$row['kcxl_id'];?></a></td>
                <td class="color_green"><?php 
                    if($row['kcxl_trangThai'] == 6){
                    echo 'Đã duyệt - Đã xuất kho';
                    }
                ?></td>
                <td>
                <?=hinh_thuc_xuat($row['kcxl_hinhThuc'])?>
                </td>
                <td style="text-align: left;">
                    <?php 
                    $nguoi_tao = $row['kcxl_nguoiTao'];
                    $user_id = $user[$nguoi_tao];
                    $ten_nguoi_tao = $user_id['ep_name'];
                            $anh_nguoi_tao = $user_id['ep_image'];
                            if($anh_nguoi_tao == ""){
                                $anh = '../images/ava_ad.png';
                            }else{
                                $anh = 'https://chamcong.24hpay.vn/upload/employee/'.$anh_nguoi_tao;
                            }
                    ?>
                    <div class="d_flex flex_start align_c">
                        <img src="<?= $anh; ?>" alt="">
                        <p><?= $ten_nguoi_tao; ?></p>
                    </div>
                </td>
                <td><?php echo date('d/m/Y',strtotime($row['kcxl_ngayTao'])); ?></td>
                <td><?php echo date('d/m/Y',strtotime($row['kcxl_ngayHoanThanh'])); ?></td>
            </tr>
            <?php endwhile; ?>
        </table>
        </div>
    </div>
    <div class="pre_q d_flex align_c flex_center position_a display_none">
        <span class="pre_arrow"></span>
    </div>
    <div class="next_q d_flex align_c flex_center position_a display_none">
        <span class="next_arrow"></span>
    </div>
</div>
<div class="w_navigation d_flex space_b align_c">
    <div class="l_nav d_flex align_c">
        <p class="color_grey font_s14 line_h17 font_w400">Hiển thị:</p>
        <select name="" id="" class="show_tr_tb color_grey font_s14 line_h17 font_w400" onchange="display(this)">
            <option value="10" <?= ($curr == 10) ? "selected" : ""?>>10</option>
            <option value="20" <?= ($curr == 20) ? "selected" : ""?>>20</option>
            <option value="30" <?= ($curr == 30) ? "selected" : ""?>>30</option>
            <option value="40" <?= ($curr == 40) ? "selected" : ""?>>40</option>
        </select>
    </div>
    <div class="r_nav">
        <ul class="d_flex font_s13 line_h15 font_wN">
            <?php
                echo generatePageBar3('', $page, $curr, $total, $url, '&', '', 'paging_detail', 'preview', '<', 'next', '>', '', '<<<', '', '>>>');
            ?>
        </ul>
    </div>
</div>
<script>
    $('.show_tr_tb').select2({
        minimumResultsForSearch: -1
    });
</script>
    