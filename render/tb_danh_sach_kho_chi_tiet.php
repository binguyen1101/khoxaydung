<?php
    include("config.php");

    $id_kho = getValue('id_kho', 'int', 'POST', '');
    $page = getValue('page', 'int', 'POST', 1);
    $nvt = getValue('nvt', 'int', 'POST', '');
    $hsx = getValue('hsx', 'int', 'POST', '');
    $xx = getValue('xx', 'int', 'POST', '');
    $sort = getValue('sort', 'int', 'POST', '');
    $curr = 10;

    $input_val = $_POST['input_val'];
    $input_val = trim($input_val);

    $so_phieu = explode('-',$input_val)[0];
    $id_so_phieu = explode('-',$input_val)[1];

    $arr = [];
    $length_id_phieu = strlen($id_so_phieu);
    for($push = 0; $push<$length_id_phieu; $push++){
        $ki_tu = substr($id_so_phieu,$push,1);
        array_push($arr,$ki_tu);
    }

    $count_arr = count($arr);
    $kcxl_id = '';

    if($arr[0] > 0){
        $kcxl_id = implode('',$arr);
    }
    elseif($arr[0] == 0){
        for($j = 1; $j < $count_arr; $j++){
            if($arr[$j] > 0){
                $kcxl_id = substr(implode('',$arr),$j ,$count_arr-$j);
                break;
            }
        }
    }

    if (isset($_COOKIE['user']) && $_COOKIE['user'] != "" && $_COOKIE['role'] == 2) {
        $token = $_COOKIE['acc_token'];
        $curl = curl_init();
        $data = array();
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/user_info_employee.php');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));
        $response = curl_exec($curl);
        curl_close($curl);
        $data_tt = json_decode($response, true);
        $tt_user = $data_tt['data']['user_info_result'];
    }

    $id_cty = $tt_user['com_id'];

    if($input_val != ""){
        $list_all = "SELECT `dsvt_id`, `dsvt_maVatTuThietBi`, `dsvt_name`, `dsvt_donGia`, `dsvt_soLuongTon`, `dsvt_kho`, `dsvt_description`, `nvt_name`, `dvt_name`, `hsx_name`, `xx_name` FROM `danh-sach-vat-tu` 
        LEFT JOIN `nhom-vat-tu-thiet-bi` ON `dsvt_nhomVatTuThietBi` = `nvt_id`
        LEFT JOIN `don-vi-tinh` ON `dsvt_donViTinh` = `dvt_id`
        LEFT JOIN `hang-san-xuat` ON `dsvt_hangSanXuat` = `hsx_id`
        LEFT JOIN `xuat-xu` ON `dsvt_xuatXu` = `xx_id`
        WHERE `dsvt_check` = 1 AND (`dsvt_id` = '$kcxl_id' OR `dsvt_name` LIKE '%$input_val%') AND `dsvt_id_ct` = $id_cty ";
    }else{
        $list_all = "SELECT `dsvt_id`, `dsvt_maVatTuThietBi`, `dsvt_name`, `dsvt_donGia`, `dsvt_soLuongTon`, `dsvt_kho`, `dsvt_description`, `nvt_name`, `dvt_name`, `hsx_name`, `xx_name` FROM `danh-sach-vat-tu` 
        LEFT JOIN `nhom-vat-tu-thiet-bi` ON `dsvt_nhomVatTuThietBi` = `nvt_id`
        LEFT JOIN `don-vi-tinh` ON `dsvt_donViTinh` = `dvt_id`
        LEFT JOIN `hang-san-xuat` ON `dsvt_hangSanXuat` = `hsx_id`
        LEFT JOIN `xuat-xu` ON `dsvt_xuatXu` = `xx_id`
        WHERE `dsvt_check` = 1 AND `dsvt_id_ct` = $id_cty ";
    }

    if ($hsx == "" && $sort == "" && $nvt == "" && $xx == "" && $input_val == "") {
        $url = "/danh-sach-kho-chi-tiet-".$id_kho.".html";
    }
    else{
        $url = "/danh-sach-kho-chi-tiet-".$id_kho.".html?nvt=".$nvt."&hsx=".$hsx."&xx=".$xx."&sort=".$sort."&input=".$input_val;
    }

    if($nvt == ""){
        $dk_nvt = "";
    }else{
        $dk_nvt = "AND `nvt_id` = $nvt ";
    }

    if($hsx == ""){
        $dk_hsx = "";
    }else{
        $dk_hsx = "AND `hsx_id` = $hsx ";
    }

    if($xx == ""){
        $dk_xx = "";
    }else{
        $dk_xx = "AND `xx_id` = $xx ";
    }

    if($sort == ""){
        $dk_sort = "ORDER BY `dsvt_id` DESC ";
    }
    else if($sort == 1){
        $dk_sort = "ORDER BY `dsvt_soLuongTon` ASC ";
    }else if($sort == 2){
        $dk_sort = "ORDER BY `dsvt_soLuongTon` DESC ";
    }


    $list_all .= $dk_hsx;
    $list_all .= $dk_nvt;
    $list_all .= $dk_xx;
    $list_all .= $dk_sort;
    $list_all = new db_query($list_all);

    $responsive = [];
    $i=0;
    while (($item = mysql_fetch_assoc($list_all->result))) {
        $check_kho = explode(',',$item['dsvt_kho']);
        if(in_array($id_kho,$check_kho)){
            $info_k['dsvt_id'] = $item['dsvt_id'];
            $info_k['dsvt_name'] = $item['dsvt_name'];
            $info_k['dsvt_soLuongTon'] = $item['dsvt_soLuongTon'];
            $info_k['nvt_name'] = $item['nvt_name'];
            $info_k['hsx_name'] = $item['hsx_name'];
            $info_k['dvt_name'] = $item['dvt_name'];
            $info_k['xx_name'] = $item['xx_name'];
            $info_k['dsvt_kho'] = $item['dsvt_kho'];
            $info_k['dsvt_description'] = $item['dsvt_description'];
            $responsive[$i] = $info_k;
            $i++;
        }
    }

    if($page>0){
        $count = count($responsive);

        $kho_p = [];
        if($page > 0){
            for($j = ($curr*($page-1)); $j < $curr*$page; $j++){
                $kho_item = $responsive[$j];
                $kho_p[$j] = $kho_item;
                if(empty($responsive[$j+1])) break;
            }
        }

        foreach ($kho_p as $key => $value) {
            if (empty($value)) {
               unset($kho_p[$key]);
            }
        }
    }

?>

<div class="position_r d_flex align_c">
    <div class="main_table mb_15 table_vt_scr" onscroll="table_scroll(this)">
        <table class="table table_add_stoke_info" style="width: 1601px;">
            <tr>
                <th>
                    STT
                    <span class="span_tbody"></span>
                </th>
                <th>Mã vật tư thiết bị
                    <span class="span_tbody"></span>
                </th>
                <th>Tên đầy đủ vật tư thiết bị
                    <span class="span_tbody"></span>
                </th>
                <th>Nhóm vật tư thiết bị
                    <span class="span_tbody"></span>
                </th>
                <th>Đơn vị tính
                    <span class="span_tbody"></span>
                </th>
                <th>Số lượng
                    <span class="span_tbody"></span>
                </th>
                <th>Hãng sản xuất
                    <span class="span_tbody"></span>
                </th>
                <th>Xuất xứ
                    <span class="span_tbody"></span>
                </th>
                <?php if(in_array(3, $ro_ton_kho)) {?>
                    <th>Chức năng
                        <span class="span_tbody"></span>
                    </th>
                <? }?>
            </tr>
            <?php if(!empty($kho_p)){
                $stt = 1;
                foreach ($kho_p as $val){ ?>
            <tr class="font_s14 line_16 color_grey" data-id="<?= $item['dsvt_id'] ?>">
                <td><?= $stt++ ?></td>
                <td>VT - <?=$val['dsvt_id']?></td>
                <td style="text-align: left;">
                    <a class="color_blue line_16 font_s14 font_w500" href="/danh-sach-vat-tu-thiet-bi-chi-tiet-<?= $val['dsvt_id'] ?>.html"><?= $val['dsvt_name']; ?></a>
                </td>
                <td style="text-align: left;"><?= $val['nvt_name']; ?></td>
                <td><?= $val['dvt_name']; ?></td>
                <td><?php  $sl = json_decode($val['dsvt_soLuongTon']);
                            echo $sl->$id_kho;
                ?></td>										
                <td style="text-align: left;"><?= $val['hsx_name']; ?></td>
                <td><?= $val['xx_name']; ?></td>
                <?php if(in_array(3, $ro_ton_kho)) {?>
                    <td>
                        <div class="d_flex align_c flex_center p_set_ft" id="p_set_ft">
                            <img src="../images/pen_blu.png" alt="" style="margin-right:0px;">
                            <p class="font_s14 line_h16 color_blue font_w500 cursor_p" onclick="id_thiet_lap(this)" data="<?= $val['dsvt_id'] ?>">Thiết lập tồn kho</p>
                        </div>
                    </td>
                <? }?>
            </tr>
            <?php }}?>
        </table>
    </div>
    <div class="pre_q d_flex align_c flex_center position_a display_none" onclick="pre_q(this)">
        <span class="pre_arrow"></span>
    </div>
    <div class="next_q d_flex align_c flex_center position_a" onclick="next_q(this)">
        <span class="next_arrow"></span>
    </div>
</div>
<div class="w_navigation d_flex align_c flex_end" id="block08">
    <!-- <div class="d_flex align_c mt_15">
            <p class="line_h16 font_s14 color_grey">Hiển thị:</p>
            <div class="d_flex align_c space_b list_choonse_item ml_10 position_r cursor_p">
                <div class="d_flex align_c" onclick="toggle('list_item_choonse_show')">
                    <p class="line_h16 font_s14 color_grey">40</p>
                    <img class="ml_10" src="../images/list_item.png" alt="">
                </div>
                <div class="list_item_choonse_show position_a" style="display: none;">
                    <p class="item_choonse_show text_a_c font_s14 line_16 color_grey">10</p>
                    <p class="item_choonse_show text_a_c font_s14 line_16 color_grey">20</p>
                    <p class="item_choonse_show text_a_c font_s14 line_16 color_grey">40</p>
                    <p class="item_choonse_show text_a_c font_s14 line_16 color_grey">80</p>
                    <p class="item_choonse_show text_a_c font_s14 line_16 color_grey">100</p>
                </div>
            </div>
        </div> -->
    <div class="r_nav">
        <ul class="d_flex font_s13 line_h15 font_wN">
            <?php
                if ($nvt != "" || $hsx !="" || $xx !="" || $sort !="" || $input_val != "") {
                    echo generatePageBar3('',$page, $curr,$count,$url,'&','','paging_detail','preview','<','next','>','','<<<','','>>>');
                }else{
                    echo generatePageBar3('',$page, $curr,$count,$url,'?','','paging_detail','preview','<','next','>','','<<<','','>>>');
                }
            ?>
        </ul>
    </div>
</div>