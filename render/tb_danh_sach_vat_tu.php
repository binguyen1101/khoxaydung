<?php
include("config.php");

$nvt = getValue('nvt', 'int', 'POST', '');
$hsx = getValue('hsx', 'int', 'POST', '');
$xx = getValue('xx', 'int', 'POST', '');
$sort = getValue('sort', 'int', 'POST', '');
$page = getValue('page', 'int', 'POST', '0');

$sl = getValue('curr','int','POST', '');
$curr = $sl;
$start = ($page - 1) * $curr;
$start = abs($start);

$input_val = $_POST['input_val'];
$input_val = trim($input_val);

$so_phieu = explode('-', $input_val)[0];
$id_so_phieu = explode('-', $input_val)[1];

$arr = [];
$length_id_phieu = strlen($id_so_phieu);
for ($push = 0; $push < $length_id_phieu; $push++) {
    $ki_tu = substr($id_so_phieu, $push, 1);
    array_push($arr, $ki_tu);
}

$count_arr = count($arr);
$kcxl_id = '';

if ($arr[0] > 0) {
    $kcxl_id = implode('', $arr);
} elseif ($arr[0] == 0) {
    for ($j = 1; $j < $count_arr; $j++) {
        if ($arr[$j] > 0) {
            $kcxl_id = substr(implode('', $arr), $j, $count_arr - $j);
            break;
        }
    }
}

if (isset($_COOKIE['user']) && $_COOKIE['user'] != "" && $_COOKIE['role'] == 1) {
    $token = $_COOKIE['acc_token'];
    $curl = curl_init();
    $data = array();
    curl_setopt($curl, CURLOPT_POST, 1);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
    curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/user_info_company.php');
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));

    $response = curl_exec($curl);
    curl_close($curl);
    $data_tt = json_decode($response, true);
    $tt_user = $data_tt['data']['user_info_result'];
    $id_nguoi_xoa = $_SESSION['com_id'];
} else if (isset($_COOKIE['user']) && $_COOKIE['user'] != "" && $_COOKIE['role'] == 2) {
    $token = $_COOKIE['acc_token'];
    $curl = curl_init();
    $data = array();
    curl_setopt($curl, CURLOPT_POST, 1);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
    curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/user_info_employee.php');
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));
    $response = curl_exec($curl);
    curl_close($curl);
    $data_tt = json_decode($response, true);
    $tt_user = $data_tt['data']['user_info_result'];
    $id_nguoi_xoa = $_SESSION['ep_id'];
}

$id_cty = $tt_user['com_id'];

if ($input_val != "") {
    if ($_COOKIE['role'] == 1) {
        $list_all = "SELECT `dsvt_id`, `dsvt_maVatTuThietBi`, `dsvt_name`, `dsvt_donGia`, `dsvt_description`, `nvt_name`, `dvt_name`, `hsx_name`, `xx_name` FROM `danh-sach-vat-tu` 
        LEFT JOIN `nhom-vat-tu-thiet-bi` ON `dsvt_nhomVatTuThietBi` = `nvt_id`
        LEFT JOIN `don-vi-tinh` ON `dsvt_donViTinh` = `dvt_id`
        LEFT JOIN `hang-san-xuat` ON `dsvt_hangSanXuat` = `hsx_id`
        LEFT JOIN `xuat-xu` ON `dsvt_xuatXu` = `xx_id`
        WHERE `dsvt_check` = '1'
        AND (`dsvt_id` = '$kcxl_id' OR `dsvt_id` LIKE '%$input_val%') AND `dsvt_id_ct` = $id_cty ";
        $total_sql =  "SELECT COUNT(`dsvt_id`) AS total FROM `danh-sach-vat-tu` 
            LEFT JOIN `nhom-vat-tu-thiet-bi` ON `dsvt_nhomVatTuThietBi` = `nvt_id`
            LEFT JOIN `don-vi-tinh` ON `dsvt_donViTinh` = `dvt_id`
            LEFT JOIN `hang-san-xuat` ON `dsvt_hangSanXuat` = `hsx_id`
            LEFT JOIN `xuat-xu` ON `dsvt_xuatXu` = `xx_id`
            WHERE `dsvt_check` = '1'
            AND (`dsvt_id` = '$kcxl_id' OR `dsvt_id` LIKE '%$input_val%') AND `dsvt_id_ct` = $id_cty ";
    }
    if ($_COOKIE['role'] == 2) {
        $list_all = "SELECT `dsvt_id`, `dsvt_maVatTuThietBi`, `dsvt_name`, `dsvt_donGia`, `dsvt_description`, `nvt_name`, `dvt_name`, `hsx_name`, `xx_name` FROM `danh-sach-vat-tu` 
        LEFT JOIN `nhom-vat-tu-thiet-bi` ON `dsvt_nhomVatTuThietBi` = `nvt_id`
        LEFT JOIN `don-vi-tinh` ON `dsvt_donViTinh` = `dvt_id`
        LEFT JOIN `hang-san-xuat` ON `dsvt_hangSanXuat` = `hsx_id`
        LEFT JOIN `xuat-xu` ON `dsvt_xuatXu` = `xx_id`
        WHERE `dsvt_check` = '1'
        AND (`dsvt_id` = '$kcxl_id' OR `dsvt_id` LIKE '%$input_val%') AND `dsvt_id_ct` = $id_cty AND NOT `dsvt_userCreateId` = 0  ";
        $total_sql =  "SELECT COUNT(`dsvt_id`) AS total FROM `danh-sach-vat-tu` 
            LEFT JOIN `nhom-vat-tu-thiet-bi` ON `dsvt_nhomVatTuThietBi` = `nvt_id`
            LEFT JOIN `don-vi-tinh` ON `dsvt_donViTinh` = `dvt_id`
            LEFT JOIN `hang-san-xuat` ON `dsvt_hangSanXuat` = `hsx_id`
            LEFT JOIN `xuat-xu` ON `dsvt_xuatXu` = `xx_id`
            WHERE `dsvt_check` = '1'
            AND (`dsvt_id` = '$kcxl_id' OR `dsvt_id` LIKE '%$input_val%') AND `dsvt_id_ct` = $id_cty AND NOT `dsvt_userCreateId` = 0  ";
    }
} else {
    if ($_COOKIE['role'] == 1) {
        $list_all = "SELECT `dsvt_id`, `dsvt_maVatTuThietBi`, `dsvt_name`, `dsvt_donGia`, `dsvt_description`, `nvt_name`, `dvt_name`, `hsx_name`, `xx_name` FROM `danh-sach-vat-tu` 
        LEFT JOIN `nhom-vat-tu-thiet-bi` ON `dsvt_nhomVatTuThietBi` = `nvt_id`
        LEFT JOIN `don-vi-tinh` ON `dsvt_donViTinh` = `dvt_id`
        LEFT JOIN `hang-san-xuat` ON `dsvt_hangSanXuat` = `hsx_id`
        LEFT JOIN `xuat-xu` ON `dsvt_xuatXu` = `xx_id`
        WHERE `dsvt_check` = 1 AND `dsvt_id_ct` = $id_cty ";
    }
    if ($_COOKIE['role'] == 2) {
        $list_all = "SELECT `dsvt_id`, `dsvt_maVatTuThietBi`, `dsvt_name`, `dsvt_donGia`, `dsvt_description`, `nvt_name`, `dvt_name`, `hsx_name`, `xx_name` FROM `danh-sach-vat-tu` 
        LEFT JOIN `nhom-vat-tu-thiet-bi` ON `dsvt_nhomVatTuThietBi` = `nvt_id`
        LEFT JOIN `don-vi-tinh` ON `dsvt_donViTinh` = `dvt_id`
        LEFT JOIN `hang-san-xuat` ON `dsvt_hangSanXuat` = `hsx_id`
        LEFT JOIN `xuat-xu` ON `dsvt_xuatXu` = `xx_id`
        WHERE `dsvt_check` = 1 AND `dsvt_id_ct` = $id_cty AND NOT `dsvt_userCreateId` = 0 ";
    }
}

if ($hsx == "" && $sort == "" && $nvt == "" && $xx == "" && $input_val == "" && $curr == 10) {
    $url = "/danh-sach-vat-tu-thiet-bi.html?dis=".$curr;
} else {
    $url = "/danh-sach-vat-tu-thiet-bi.html?nvt=" . $nvt . "&hsx=" . $hsx . "&xx=" . $xx . "&sort=" . $sort . "&input=" . $input_val."&dis=".$curr;
}

if ($nvt == "") {
    $dk_nvt = "";
} else {
    $dk_nvt = "AND `nvt_id` = $nvt ";
}

if ($hsx == "") {
    $dk_hsx = "";
} else {
    $dk_hsx = "AND `hsx_id` = $hsx ";
}

if ($xx == "") {
    $dk_xx = "";
} else {
    $dk_xx = "AND `xx_id` = $xx ";
}

if ($sort == "") {
    $dk_sort = "ORDER BY `dsvt_id` DESC ";
} else if ($sort == 1) {
    $dk_sort = "ORDER BY `dsvt_donGia` ASC ";
} else if ($sort == 2) {
    $dk_sort = "ORDER BY `dsvt_donGia` DESC ";
}

$limited = "LIMIT $start,$curr";

$list_all .= $dk_hsx;
$list_all .= $dk_nvt;
$list_all .= $dk_xx;
$list_all .= $dk_sort;
$list_all .= $limited;
$list_all = new db_query($list_all);

$total_sql =  "SELECT COUNT(`dsvt_id`) AS total FROM `danh-sach-vat-tu` 
    LEFT JOIN `nhom-vat-tu-thiet-bi` ON `dsvt_nhomVatTuThietBi` = `nvt_id`
    LEFT JOIN `don-vi-tinh` ON `dsvt_donViTinh` = `dvt_id`
    LEFT JOIN `hang-san-xuat` ON `dsvt_hangSanXuat` = `hsx_id`
    LEFT JOIN `xuat-xu` ON `dsvt_xuatXu` = `xx_id`
    WHERE `dsvt_check` = 1 AND `dsvt_id_ct` = $id_cty ";
$total_sql .= $dk_hsx;
$total_sql .= $dk_nvt;
$total_sql .= $dk_xx;

$total_sql1 = new db_query($total_sql);
$total = mysql_fetch_assoc($total_sql1->result)['total'];

?>
<div class="position_r d_flex align_c">
    <div class="main_table mb_15 table_vt_scr" onscroll="table_scroll(this)">
        <table class="table table_equipment_supplies">
            <tr>
                <th>
                    STT
                    <span class="span_tbody"></span>
                </th>
                <th>Mã vật tư thiết bị
                    <span class="span_tbody"></span>
                </th>
                <th>Tên đầy đủ vật tư thiết bị
                    <span class="span_tbody"></span>
                </th>
                <th>Nhóm vật tư thiết bị
                    <span class="span_tbody"></span>
                </th>
                <th>Đơn vị tính
                    <span class="span_tbody"></span>
                </th>
                <th>Hãng sản xuất
                    <span class="span_tbody"></span>
                </th>
                <th>Xuất xứ
                    <span class="span_tbody"></span>
                </th>
                <th class="th_desc">Mô tả
                    <span class="span_tbody"></span>
                </th>
                <th>Đơn giá (VNĐ)
                    <span class="span_tbody"></span>
                </th>
                <?php if (in_array(3, $ro_vattu) || in_array(4, $ro_vattu)) { ?>
                    <th>Chức năng
                        <span class="span_tbody"></span>
                    </th>
                <? } ?>
            </tr>
            <? $stt = 1;
            while ($row_list_all = mysql_fetch_assoc($list_all->result)) { ?>
                <tr data-id="<?= $row_list_all['dsvt_id'] ?>">
                    <td class="font_s14 line_16 color_grey"><?= $stt++ ?></td>
                    <td class="font_s14 line_16 color_grey">VT - <?= $row_list_all['dsvt_id']; ?></td>
                    <td>
                        <a class="color_blue line_16 font_s14 font_w500" href="/danh-sach-vat-tu-thiet-bi-chi-tiet-<?= $row_list_all['dsvt_id'] ?>.html">
                            <?= $row_list_all['dsvt_name'] ?></a>
                    </td>
                    <td class="font_s14 line_16 color_grey"><?= $row_list_all['nvt_name'] ?></td>
                    <td class="font_s14 line_16 color_grey"><?= $row_list_all['dvt_name'] ?></td>
                    <td class="font_s14 line_16 color_grey"><?= $row_list_all['hsx_name'] ?></td>
                    <td class="font_s14 line_16 color_grey"><?= $row_list_all['xx_name'] ?></td>
                    <td class="font_s14 line_16 color_grey text_a_l">
                        <div class="d_flex space_b">
                            <p class="td_desc"><?=$row_list_all['dsvt_description']?></p> 
                            <p class="xem_them color_blue font_s14 line_h16 font_w400 cursor_p display_none">Xem thêm</p>
                        </div>
                    </td>
                    <td class="font_s14 line_16 color_grey text_a_r"><?= $row_list_all['dsvt_donGia'] ?></td>
                    <?php if (in_array(3, $ro_vattu) || in_array(4, $ro_vattu)) { ?>
                        <td>
                            <p class="d_flex align_c flex_center">
                                <?php if (in_array(3, $ro_vattu)) { ?>
                                    <img src="../images/pen_blu.png" alt="">
                                    <a class="color_blue font_s14" href="/danh-sach-vat-tu-thiet-bi-sua-<?= $row_list_all['dsvt_id'] ?>.html">Sửa</a>
                                <?php } ?>
                                <?php if (in_array(3, $ro_vattu) && in_array(4, $ro_vattu)) { ?>
                                    <span class="color_blue font_s14 margin_lr5">|</span>
                                <?php } ?>
                                <?php if (in_array(4, $ro_vattu)) { ?>
                                    <img src="../images/delete_r.png" alt="">
                                    <span class="color_red cursor_p font_s14" onclick="id_xoa(this)">Xóa</span>
                                <?php } ?>
                            </p>
                        </td>
                    <?php } ?>
                </tr>
            <? } ?>
        </table>
    </div>
    <div class="pre_q d_flex align_c flex_center position_a display_none" onclick="pre_q(this)">
        <span class="pre_arrow"></span>
    </div>
    <div class="next_q d_flex align_c flex_center position_a" onclick="next_q(this)">
        <span class="next_arrow"></span>
    </div>
</div>
<div class="w_navigation d_flex space_b align_c">
    <div class="l_nav d_flex align_c">
        <p class="color_grey font_s14 line_h17 font_w400">Hiển thị:</p>
        <select name="" id="" class="show_tr_tb color_grey font_s14 line_h17 font_w400" onchange="display(this)">
            <option value="10" <?= ($curr == 10) ? "selected" : ""?>>10</option>
            <option value="20" <?= ($curr == 20) ? "selected" : ""?>>20</option>
            <option value="30" <?= ($curr == 30) ? "selected" : ""?>>30</option>
            <option value="40" <?= ($curr == 40) ? "selected" : ""?>>40</option>
        </select>
    </div>
    <div class="r_nav">
        <ul class="d_flex font_s13 line_h15 font_wN">
            <?php
                echo generatePageBar3('', $page, $curr, $total, $url, '&', '', 'paging_detail', 'preview', '<', 'next', '>', '', '<<<', '', '>>>');
            ?>
        </ul>
    </div>
</div>
<script type="text/javascript" src="../js/ghi_chu.js"></script>
<script>
    $('.show_tr_tb').select2({
        minimumResultsForSearch: -1
    });
</script>
