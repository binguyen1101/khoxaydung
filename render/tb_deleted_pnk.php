
<?php
    include("config.php"); 

    $page = getValue('page','int','POST','');
    $curr = 10;
    $start = ($page - 1 )*$curr;
    $start = abs($start);

    $input_val = $_POST['input_val'];
    $input_val = trim($input_val);

    $so_phieu = explode('-',$input_val)[0];
    $id_so_phieu = explode('-',$input_val)[1];

    $arr = [];
    $length_id_phieu = strlen($id_so_phieu);
    for($push = 0; $push<$length_id_phieu; $push++){
        $ki_tu = substr($id_so_phieu,$push,1);
        array_push($arr,$ki_tu);
    }

    $count_arr = count($arr);
    $kcxl_id = '';

    if($arr[0] > 0){
        $kcxl_id = implode('',$arr);
    }
    elseif($arr[0] == 0){
        for($j = 1; $j < $count_arr; $j++){
            if($arr[$j] > 0){
                $kcxl_id = substr(implode('',$arr),$j ,$count_arr-$j);
                break;
            }
        }
    }

    if($input_val != ""){
        $du_lieu_da_xoa = new db_query("SELECT `kcxl_id`, `kcxl_soPhieu`, `kcxl_hinhThuc`, 	`kcxl_trangThai`, `kcxl_ghi_chu`, `kcxl_nguoiTao`, `kcxl_ngayTao`,
        `kcxl_ngayNhapKho`, `kcxl_khoNhap`, `kcxl_nguoiXoa`,  `kcxl_ngayXoa`, `kho_name` FROM `kho-cho-xu-li`
        LEFT JOIN `kho` ON `kho-cho-xu-li`.`kcxl_khoNhap` = `kho`.`kho_id`
        WHERE (`kcxl_soPhieu` = '$so_phieu' AND `kcxl_id` = '$kcxl_id' AND `kcxl_check` = 0 AND `kcxl_id_ct` = $id_cty) 
        ORDER BY `kcxl_id` ASC LIMIT $start,$curr
        ");
        $total_sql = "SELECT COUNT(`kcxl_id`) AS total FROM `kho-cho-xu-li`
        LEFT JOIN `kho` ON `kho-cho-xu-li`.`kcxl_khoNhap` = `kho`.`kho_id`
        WHERE (`kcxl_soPhieu` = '$so_phieu' AND `kcxl_id` = '$kcxl_id' AND `kcxl_check` = 0 AND `kcxl_id_ct` = $id_cty)
        ORDER BY `kcxl_id` ";
    } else{
        $du_lieu_da_xoa = new db_query("SELECT `kcxl_id`, `kcxl_soPhieu`, `kcxl_hinhThuc`, 	`kcxl_trangThai`, `kcxl_ghi_chu`, `kcxl_nguoiTao`, `kcxl_ngayTao`,
        `kcxl_ngayNhapKho`, `kcxl_khoNhap`, `kcxl_nguoiXoa`,  `kcxl_ngayXoa`, `kho_name` FROM `kho-cho-xu-li`
        LEFT JOIN `kho` ON `kho-cho-xu-li`.`kcxl_khoNhap` = `kho`.`kho_id`
        WHERE `kcxl_soPhieu` = 'PNK' AND `kcxl_check` = 0 AND `kcxl_id_ct` = $id_cty
        ORDER BY `kcxl_id` ASC LIMIT $start,$curr
        ");
        $total_sql = "SELECT COUNT(`kcxl_id`) AS total FROM `kho-cho-xu-li`
        LEFT JOIN `kho` ON `kho-cho-xu-li`.`kcxl_khoNhap` = `kho`.`kho_id`
        WHERE `kcxl_soPhieu` = 'PNK' AND `kcxl_check` = 0 AND `kcxl_id_ct` = $id_cty
        ORDER BY `kcxl_id` ";
    }

    if(isset($_COOKIE['acc_token']) && isset($_COOKIE['rf_token']) && isset($_COOKIE['role']) && $_COOKIE['role'] == 2){
        $curl = curl_init();
        $token = $_COOKIE['acc_token'];
        curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/list_all_my_partner.php?get_all=true');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer '.$token));
        $response = curl_exec($curl);
        curl_close($curl);

        $data_list = json_decode($response,true);
        $data_list_nv =$data_list['data']['items'];
    }else{
        $curl = curl_init();
        $token = $_COOKIE['acc_token'];
        curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/list_all_employee_of_company.php');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer '.$token));
        $response = curl_exec($curl);
        curl_close($curl);

        $data_list = json_decode($response,true);
        $data_list_nv =$data_list['data']['items'];

    }
    $count = count($data_list_nv);

    $user = [];
    for ($i = 0; $i < count($data_list_nv); $i++){
        $nv = $data_list_nv[$i];
        $user[$nv["ep_id"]] = $nv;
    }

    $total_sql1 = new db_query($total_sql);
    $total = mysql_fetch_assoc($total_sql1->result)['total'];

    if($input_val == ""){
        $url = '/du-lieu-da-xoa-gan-day-phieu-nhap-kho.html';
    }else{
        $url = "/du-lieu-da-xoa-gan-day-phieu-nhap-kho.html?input=".$input_val."&page=".$page;
    }
?>

<div class="position_r d_flex align_c">
    <div class="table_vt_scr" onscroll="table_scroll(this)">
        <div class="table_vt">
            <table style="width: 2352px;">
                <tr class="tit_tbl color_white font_s16 line_h19 font_w500 back_blue">
                    <th><input type="checkbox">
                        <span class="span_tbody"></span>
                    </th>
                    <th>Số phiếu
                        <span class="span_tbody"></span>
                    </th>
                    <th>Hình thức nhập kho
                        <span class="span_tbody"></span>
                    </th>
                    <th>Trạng thái<span class="span_tbody"></span>
                    </th>
                    <th>Ghi chú<span class="span_tbody"></span>
                    </th>
                    <th>Người tạo<span class="span_tbody"></span>
                    </th>
                    <th>Ngày tạo<span class="span_tbody"></span>
                    </th>
                    <th>Ngày nhập kho<span class="span_tbody"></span>
                    </th>
                    <th>Kho nhập<span class="span_tbody"></span>
                    </th>
                    <th>Người xóa<span class="span_tbody"></span>
                    </th>
                    <th>Ngày xóa<span class="span_tbody"></span>
                    </th>
                    <th style="width: 176px">Chứ năng
                    </th>
                </tr>
                <?php while($row = mysql_fetch_assoc($du_lieu_da_xoa->result)) { ?>
                    <tr class="color_grey font_s14 line_h17 font_w400" data-id="<?= $row['kcxl_id']; ?>">
                        <td><input type="checkbox"></td>
                        <td><a class="color_blue font_w500">PNK - <?=$row['kcxl_id'];?>
                        </a></td>
                        <td>
                            <?=hinh_thuc_nhap($row['kcxl_hinhThuc']);?>
                        </td>
                        <td class="<?php 
                                    $trang_thai_color = $row['kcxl_trangThai']; 
                                    switch($trang_thai_color) {
                                        case 2:
                                            $trang_thai_color = 'color_org';
                                            echo $trang_thai_color;
                                            break;
                                        case 3:
                                            $trang_thai_color = 'color_blue';
                                            echo $trang_thai_color;
                                            break;
                                        case 4:
                                            $trang_thai_color = 'color_green';
                                            echo $trang_thai_color;
                                            break;
                                        default:
                                            $trang_thai_color = 'Error';
                                            echo $trang_thai_color;
                                            break;
                                    }
                                ?>">
                                <?php $trang_thai = $row['kcxl_trangThai']; 
                                    switch($trang_thai) {
                                        case 1:
                                            $trang_thai = 'Chờ duyệt';
                                            echo $trang_thai;
                                            break;
                                        case 2:
                                            $trang_thai = 'Từ chối';
                                            echo $trang_thai;
                                            break;
                                        case 3:
                                            $trang_thai = 'Đã duyệt - Chờ nhập kho';
                                            echo $trang_thai;
                                            break;
                                        case 4:
                                            $trang_thai = 'Đã duyệt - Đã nhập kho';
                                            echo $trang_thai;
                                            break;
                                        default:
                                            $trang_thai = 'Error';
                                            echo $trang_thai;
                                            break;
                                    }
                                ?></td>
                        </td>
                        <td style="text-align: left;"><?= $row['kcxl_ghi_chu']; ?></td>
                        <td style="text-align: left;">
                            <?php 
                                $nguoi_tao = $row['kcxl_nguoiTao'];
                                $user_id = $user[$nguoi_tao];
                                $ten_nguoi_tao = $user_id['ep_name'];
                                $anh_nguoi_tao = $user_id['ep_image'];
                                if($anh_nguoi_tao == ""){
                                    $anh0 = '../images/ava_ad.png';
                                }else{
                                    $anh0 = 'https://chamcong.24hpay.vn/upload/employee/'.$anh_nguoi_tao;
                                }
                            ?>
                            <div class="d_flex flex_start align_c">
                                <img src="<?= $anh0; ?>" alt="">
                                <p><?= $ten_nguoi_tao; ?></p>
                            </div>
                        </td>
                        <td><?= date('d/m/Y',strtotime($row['kcxl_ngayTao'])); ?></td>
                        <td><?= date('d/m/Y',strtotime($row['kcxl_ngayNhapKho'])); ?></td>
                        <td style="text-align: left;"><?= $row['kho_name']; ?></td>
                        <td style="text-align: left;">
                            <?php 
                                $nguoi_xoa = $row['kcxl_nguoiXoa'];
                                $user_id_nx = $user[$nguoi_xoa];
                                $ten_nguoi_xoa = $user_id_nx['ep_name'];
                                $anh_nguoi_xoa = $user_id_nx['ep_image'];
                                if($anh_nguoi_xoa == ""){
                                    $anh = '../images/ava_ad.png';
                                }else{
                                    $anh = 'https://chamcong.24hpay.vn/upload/employee/'.$anh_nguoi_xoa;
                                }
                            ?>
                            <div class="d_flex flex_start align_c">
                                <img src="<?= $anh; ?>" alt="">
                                <p><?= $ten_nguoi_xoa; ?></p>
                            </div>
                        </td>
                        <td><?= date('d/m/Y',strtotime($row['kcxl_ngayXoa'])); ?></td>
                        <td>
                            <a class="a_rm color_blue font_s14 line_h17 font_w500 cursor_p" onclick="khoi_phuc(this)">
                                <img src="../images/rm_tb.png" alt="">Khôi phục
                            </a>
                            <span style="border-left: 1px solid #4C5BD4; margin: auto 5px;"></span>
                            <a class="a_del color_red font_s14 line_h17 font_w500 cursor_p" onclick="xoa(this)"> 
                                <img src="../images/del_tb.png" alt="">Xóa
                            </a>
                        </td>
                    </tr>
                <?php }?>
            </table>
        </div>
    </div>
    <div class="pre_q d_flex align_c flex_center position_a display_none" onclick="pre_q(this)">
        <span class="pre_arrow"></span>
    </div>
    <div class="next_q d_flex align_c flex_center position_a" onclick="next_q(this)">
        <span class="next_arrow"></span>
    </div>
</div>
<div class="w_navigation d_flex flex_end align_c">
    <!-- <div class="l_nav d_flex align_c">
        <p class="color_grey font_s14 line_h17 font_w400">Hiển thị:</p>
        <select name="" id="" class="show_tr_tb color_grey font_s14 line_h17 font_w400">
            <option value="">10</option>
            <option value="">20</option>
            <option value="">30</option>
            <option value="">40</option>
        </select>
    </div> -->
    <div class="r_nav">
        <ul class="d_flex font_s13 line_h15 font_wN">
        <?php
            if ($input_val != "") {
                echo generatePageBar3('',$page,10,$total,$url,'&','','paging_detail','preview','<','next','>','','<<<','','>>>');
            }else{
                echo generatePageBar3('',$page,10,$total,$url,'?','','paging_detail','preview','<','next','>','','<<<','','>>>');
            }
        ?>
        </ul>
    </div>
</div>

<script>

    $('.dt_dl_n td input[type=checkbox]').click(function () {
        flag = [];
        var input_arr = $('.dt_dl_n td input[type=checkbox]');
        input_arr.each(function () {
            if (570 <= $(window).width() && $(window).width() <= 768) {
                $('.dt_dl_n .export_wh .hd_ex').css('margin-bottom', '15px');
            } else if (($(window).width() <= 467)) {
                $('.dt_dl_n .export_wh .hd_ex').css('margin-top', '15px');
            }

            if ($(this).is(":checked")) {
                flag.push($(this).val());
            }

            if (flag.length < input_arr.length) {
                $('.dt_dl_n th input[type=checkbox]').prop('checked', false);
            } else {
                $('.dt_dl_n th input[type=checkbox]').prop('checked', true);
            }

            if (flag.length > 0) {
                $('.dt_dl_n .btn_del_vv,.dt_dl_n .btn_del_rm').show();
            } else {
                $('.dt_dl_n .btn_del_vv,.dt_dl_n .btn_del_rm').hide();
                $('.dt_dl_n .export_wh .hd_ex').css('margin-bottom', '0');
                $('.dt_dl_n .export_wh .hd_ex').css('margin-top', '0');
            }
        });
        
        var count = flag.length;
        $('.dt_dl_in_wh .btn_del_rm').on("click", function () {
            btn_remove(count, 'phiếu nhập kho');
        })

        $('.dt_dl_in_wh .btn_del_vv').on("click", function () {
            btn_del(count, 'phiếu nhập kho');
        })
    });

    $('.dt_dl_n th input[type=checkbox]').click(function () {
        if ($('.dt_dl_n th input[type=checkbox]').is(':checked')) {
            $('.dt_dl_n .btn_del_vv,.dt_dl_n .btn_del_rm').show();
            $('.dt_dl_n td input[type=checkbox]').not(this).prop('checked', true);
            if (570 <= $(window).width() && $(window).width() <= 768) {
                $('.dt_dl_n .export_wh .hd_ex').css('margin-bottom', '15px');
            } else if (($(window).width() <= 467)) {
                $('.dt_dl_n .export_wh .hd_ex').css('margin-top', '15px');
            }
        } else {
            $('.dt_dl_n .btn_del_vv,.dt_dl_n .btn_del_rm').hide();
            $('.dt_dl_n td input[type=checkbox]').not(this).prop('checked', false);
            $('.dt_dl_n .export_wh .hd_ex').css('margin-bottom', '0');
            $('.dt_dl_n .export_wh .hd_ex').css('margin-top', '0');
        }
        $('.dt_dl_in_wh .btn_del_rm').on("click", function () {
            btn_remove('tất cả', 'phiếu nhập kho');
        })

        $('.dt_dl_in_wh .btn_del_vv').on("click", function () {
            btn_del('tất cả', 'phiếu nhập kho');
        })

    });
    
</script>