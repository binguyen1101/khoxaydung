
<?php
    include("config.php"); 

    if(isset($_COOKIE['acc_token']) && isset($_COOKIE['rf_token']) && isset($_COOKIE['role']) && $_COOKIE['role'] == 2){
        $curl = curl_init();
        $token = $_COOKIE['acc_token'];
        curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/list_all_my_partner.php?get_all=true');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer '.$token));
        $response = curl_exec($curl);
        curl_close($curl);

        $data_list = json_decode($response,true);
        $data_list_nv =$data_list['data']['items'];
    }else{
        $curl = curl_init();
        $token = $_COOKIE['acc_token'];
        curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/list_all_employee_of_company.php');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer '.$token));
        $response = curl_exec($curl);
        curl_close($curl);

        $data_list = json_decode($response,true);
        $data_list_nv =$data_list['data']['items'];

    }
    $count = count($data_list_nv);

    $user = [];
    for ($i = 0; $i < count($data_list_nv); $i++){
        $nv = $data_list_nv[$i];
        $user[$nv["ep_id"]] = $nv;
    }

    if (isset($_COOKIE['user']) && $_COOKIE['user'] != "" && $_COOKIE['role'] == 2) {
		$token = $_COOKIE['acc_token'];
		$curl = curl_init();
		$data = array();
		curl_setopt($curl, CURLOPT_POST, 1);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
		curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/user_info_employee.php');
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));
		$response = curl_exec($curl);
		curl_close($curl);
		$data_tt = json_decode($response, true);
		$tt_user = $data_tt['data']['user_info_result'];
        $user_id = $_SESSION['ep_id'];

	}else if (isset($_COOKIE['user']) && $_COOKIE['user'] != "" && $_COOKIE['role'] == 1) {
        $token = $_COOKIE['acc_token'];
        $curl = curl_init();
        $data = array();
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/user_info_company.php');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));
        $response = curl_exec($curl);
        curl_close($curl);
        $data_tt = json_decode($response, true);
        $tt_user = $data_tt['data']['user_info_result'];
        $user_id = $_SESSION['com_id'];
    }
    
    $id_cty = $tt_user['com_id'];

    $all_kho_xuat = $_POST['kx'];
    $all_kho_nhap = $_POST['kn'];
    $select_st_tf = $_POST['th'];
 
    // ngày tạo start
    if($_POST['ngay_tao_start'] != ""){
        $ngay_tao_start = date('Y-m-d',strtotime($_POST['ngay_tao_start'])); 
    }else{
        $ngay_tao_start = "";
    }
    
    // ngày tạo end
    if($_POST['ngay_tao_end'] != ""){
        $ngay_tao_end = date('Y-m-d',strtotime($_POST['ngay_tao_end']));
    }else{
        $ngay_tao_end = "";
    }
    
    // ngày thực hiện start
    if($_POST['ngay_th_start'] != ""){
        $ngay_th_start = date('Y-m-d',strtotime($_POST['ngay_th_start'])); 
    }else{
        $ngay_th_start = "";
    }

    // ngày thực hiện end
    if($_POST['ngay_th_end'] != ""){
        $ngay_th_end = date('Y-m-d',strtotime($_POST['ngay_th_end']));
    }else{
        $ngay_th_end = "";
    }

    // ngày hoàn thành start
    if($_POST['ngay_ht_start'] != ""){
        $ngay_ht_start = date('Y-m-d',strtotime($_POST['ngay_ht_start']));
    }else{
        $ngay_ht_start = "";
    }

    // ngày hoàn thành end
    if($_POST['ngay_ht_end'] != ""){
        $ngay_ht_end = date('Y-m-d',strtotime($_POST['ngay_ht_end']));
    }else{
        $ngay_ht_end = "";
    }

    $page = $_POST['page'];
    $sl = getValue('curr','int','POST', '');
    $curr = $sl;
    $start = ($page - 1 )*$curr;
    $start = abs($start);

    $input_val = $_POST['input_val'];

    $input_val = trim($input_val);

    $so_phieu = explode('-',$input_val)[0];
    $id_so_phieu = explode('-',$input_val)[1];

    $arr = [];
    $length_id_phieu = strlen($id_so_phieu);
    for($push = 0; $push<$length_id_phieu; $push++){
        $ki_tu = substr($id_so_phieu,$push,1);
        array_push($arr,$ki_tu);
    }

    $count_arr = count($arr);
    $kcxl_id = '';

    if($arr[0] > 0){
        $kcxl_id = implode('',$arr);
    }
    elseif($arr[0] == 0){
        for($j = 1; $j < $count_arr; $j++){
            if($arr[$j] > 0){
                $kcxl_id = substr(implode('',$arr),$j ,$count_arr-$j);
                break;
            }
        }
    }

    if($input_val != ""){
        if($_COOKIE['role'] == 1){
            $dieu_chuyen_kho = "SELECT `kcxl_id`,`kcxl_soPhieu`, `kcxl_trangThai`, `kcxl_ghi_chu`, `kcxl_nguoiTao`, `kcxl_ngayTao`, 
            `kcxl_ngayThucHienDieuChuyen`, `kcxl_phieuDieuChuyenKho`, `kcxl_ngayYeuCauHoanThanh`, `kcxl_khoNhap`, `kcxl_khoXuat`
            FROM `kho-cho-xu-li`
            WHERE `kcxl_check` = 1 AND `kcxl_soPhieu` = 'ĐCK' AND (`kcxl_id` = '$kcxl_id' OR `kcxl_id` LIKE '%$input_val%') AND `kcxl_id_ct` = $id_cty ";

            $total_sql =  "SELECT COUNT(`kcxl_check`) AS total FROM `kho-cho-xu-li` 
            WHERE `kcxl_check` = 1 AND `kcxl_soPhieu` = 'ĐCK' AND (`kcxl_id` = '$kcxl_id' OR `kcxl_id` LIKE '%$input_val%') ";
        }
        if($_COOKIE['role'] == 2){
            $dieu_chuyen_kho = "SELECT `kcxl_id`,`kcxl_soPhieu`, `kcxl_trangThai`, `kcxl_ghi_chu`, `kcxl_nguoiTao`, `kcxl_ngayTao`, 
            `kcxl_ngayThucHienDieuChuyen`, `kcxl_phieuDieuChuyenKho`, `kcxl_ngayYeuCauHoanThanh`, `kcxl_khoNhap`, `kcxl_khoXuat`
            FROM `kho-cho-xu-li`
            WHERE `kcxl_check` = 1 AND `kcxl_soPhieu` = 'ĐCK' AND (`kcxl_id` = '$kcxl_id' OR `kcxl_id` LIKE '%$input_val%') AND `kcxl_id_ct` = $id_cty AND (`kcxl_nguoiTao` = $user_id OR `kcxl_nguoiThucHien` = $user_id) ";

            $total_sql =  "SELECT COUNT(`kcxl_check`) AS total FROM `kho-cho-xu-li` 
            WHERE `kcxl_check` = 1 AND `kcxl_soPhieu` = 'ĐCK' AND (`kcxl_id` = '$kcxl_id' OR `kcxl_id` LIKE '%$input_val%') AND (`kcxl_nguoiTao` = $user_id OR `kcxl_nguoiThucHien` = $user_id) ";
        }
    }else{
        if($_COOKIE['role'] == 1){
            $dieu_chuyen_kho = "SELECT `kcxl_id`,`kcxl_soPhieu`, `kcxl_trangThai`, `kcxl_ghi_chu`, `kcxl_nguoiTao`, `kcxl_ngayTao`, 
            `kcxl_ngayThucHienDieuChuyen`, `kcxl_phieuDieuChuyenKho`, `kcxl_ngayYeuCauHoanThanh`, `kcxl_khoNhap`, `kcxl_khoXuat`
            FROM `kho-cho-xu-li`
            WHERE `kcxl_check` = 1 AND `kcxl_soPhieu` = 'ĐCK' AND `kcxl_id_ct` = $id_cty ";

            $total_sql =  "SELECT COUNT(`kcxl_check`) AS total FROM `kho-cho-xu-li` 
            WHERE `kcxl_check` = 1 AND `kcxl_soPhieu` = 'ĐCK' AND `kcxl_id_ct` = $id_cty ";
        }
        if($_COOKIE['role'] == 2){
            $dieu_chuyen_kho = "SELECT `kcxl_id`,`kcxl_soPhieu`, `kcxl_trangThai`, `kcxl_ghi_chu`, `kcxl_nguoiTao`, `kcxl_ngayTao`, 
            `kcxl_ngayThucHienDieuChuyen`, `kcxl_phieuDieuChuyenKho`, `kcxl_ngayYeuCauHoanThanh`, `kcxl_khoNhap`, `kcxl_khoXuat`
            FROM `kho-cho-xu-li`
            WHERE `kcxl_check` = 1 AND `kcxl_soPhieu` = 'ĐCK' AND `kcxl_id_ct` = $id_cty AND (`kcxl_nguoiTao` = $user_id OR `kcxl_nguoiThucHien` = $user_id) ";

            $total_sql =  "SELECT COUNT(`kcxl_check`) AS total FROM `kho-cho-xu-li` 
            WHERE `kcxl_check` = 1 AND `kcxl_soPhieu` = 'ĐCK' AND `kcxl_id_ct` = $id_cty AND (`kcxl_nguoiTao` = $user_id OR `kcxl_nguoiThucHien` = $user_id) ";
        }
    }


    if ($all_kho_xuat == "" && $all_kho_nhap == "" && $select_st_tf == "" && $ngay_tao_start == "" && $ngay_tao_end == "" && $ngay_th_start == "" && $ngay_th_end == "" && $ngay_ht_start == "" && $ngay_ht_end == "" && $input_val == "" && $curr == 10) {
        $url = '/dieu-chuyen-kho.html?dis='.$curr;
    }
    else{
        $url = '/dieu-chuyen-kho.html?kx='.$all_kho_xuat.'&kn='.$all_kho_nhap.'&th='.$select_st_tf.'&ngts='.$ngay_tao_start.'&ngte='.$ngay_tao_end.'&ngths='.$ngay_th_start.'&ngthe='.$ngay_th_end.'&nghts='.$ngay_ht_start.'&nghte='.$ngay_ht_end."&input=".$input_val."&dis=".$curr;
    }


    $limited = "LIMIT $start,$curr";


    if($select_st_tf == ""){
        $tf = "";
    }else{
        $tf = "AND `kcxl_trangThai` = '$select_st_tf'";
    }

    if($all_kho_xuat == "" && $all_kho_nhap == ""){
        $select = "";
    }elseif($all_kho_xuat != "" && $all_kho_nhap == ""){
        $select = "AND `kcxl_khoXuat` = '$all_kho_xuat'";
    }else if($all_kho_xuat != "" && $all_kho_nhap != ""){
        $select = "AND `kcxl_khoXuat` = '$all_kho_xuat' AND `kcxl_khoNhap` = '$all_kho_nhap'";
    }else{
        $select = "AND `kcxl_khoNhap` = '$all_kho_nhap'";
    }

    if($ngay_tao_start != "" && $ngay_tao_end != ""){
        $date = "AND `kcxl_ngayTao` <= '$ngay_tao_end' AND  `kcxl_ngayTao` >= '$ngay_tao_start'";
    }elseif($ngay_tao_start != "" && $ngay_tao_end == ""){
        $date = "AND `kcxl_ngayTao` = '$ngay_tao_start' ";
    }elseif($ngay_tao_start == "" && $ngay_tao_end != ""){
        $date = "AND `kcxl_ngayTao` = '$ngay_tao_end' ";
    }

    if($ngay_th_start != "" && $ngay_th_end != ""){
        $date_th = "AND `kcxl_ngayThucHienDieuChuyen` <= '$ngay_th_end' AND  `kcxl_ngayThucHienDieuChuyen` >= '$ngay_th_start'";
    }elseif($ngay_th_start != "" && $ngay_th_end == ""){
        $date_th = "AND `kcxl_ngayThucHienDieuChuyen` = '$ngay_th_start' ";
    }elseif($ngay_th_start == "" && $ngay_th_end != ""){
        $date_th = "AND `kcxl_ngayThucHienDieuChuyen` = '$ngay_th_end' ";
    }

    if($ngay_ht_start != "" && $ngay_ht_end != ""){
        $date_ht = "AND `kcxl_ngayYeuCauHoanThanh` <= '$ngay_ht_end' AND  `kcxl_ngayYeuCauHoanThanh` >= '$ngay_ht_start'";
    }elseif($ngay_ht_start != "" && $ngay_ht_end == ""){
        $date_ht = "AND `kcxl_ngayYeuCauHoanThanh` = '$ngay_ht_start' ";
    }elseif($ngay_ht_start == "" && $ngay_ht_end != ""){
        $date_ht = "AND `kcxl_ngayYeuCauHoanThanh` = '$ngay_ht_end' ";
    }


    $order = "ORDER BY `kho-cho-xu-li`.`kcxl_id` DESC ";

    $dieu_chuyen_kho .= $select;
    $dieu_chuyen_kho .= $tf;
    $dieu_chuyen_kho .= $date;
    $dieu_chuyen_kho .= $date_th;
    $dieu_chuyen_kho .= $date_ht;
    $dieu_chuyen_kho .= $order;
    $dieu_chuyen_kho .= $limited;

    $dieu_chuyen_kho = new db_query($dieu_chuyen_kho);

    $total_sql .= $select;
    $total_sql .= $tf;
    $total_sql .= $date;
    $total_sql .= $date_th;
    $total_sql .= $date_ht;

    $total_sql1 = new db_query($total_sql);
    $total = mysql_fetch_assoc($total_sql1->result)['total'];
    
    $id_nguoi_xoa = $_SESSION['ep_id'];
?>

<div class="position_r d_flex align_c">
    <div class="table_vt_scr" onscroll="table_scroll(this)">
        <div class="table_vt">
        <table>
            <tr class="tit_tbl color_white font_s16 line_h19 font_w500 back_blue">
                <th>STT
                    <span class="span_tbody"></span>
                </th>
                <th>Số phiếu
                    <span class="span_tbody"></span>
                </th>
                <th>Trạng thái
                    <span class="span_tbody"></span>
                </th>
                <th>Ghi chú
                    <span class="span_tbody"></span>
                </th>
                <th>Người tạo
                    <span class="span_tbody"></span>
                </th>
                <th>Ngày tạo
                    <span class="span_tbody"></span>
                </th>
                <th>Ngày thực hiện điều chuyển
                    <span class="span_tbody"></span>
                </th>
                <th>Ngày yêu cầu hoàn thành
                    <span class="span_tbody"></span>
                </th>
                <th>Kho xuất
                    <span class="span_tbody"></span>
                </th>
                <th>Kho nhập
                <?php if(in_array(3, $ro_dc_kho) || in_array(4, $ro_dc_kho)) {?>
                    <span class="span_tbody"></span>
                <?php }?>
                </th>
                <?php if(in_array(3, $ro_dc_kho) || in_array(4, $ro_dc_kho)) {?>
                    <th style="width: 136px;">Chức năng
                    </th>
                <?php }?>
            </tr>
            <?php $i=1; 
            while($row = mysql_fetch_assoc($dieu_chuyen_kho->result)) {
            ?>
            <tr class="color_grey font_s14 line_h17 font_w400" data-id="<?= $row['kcxl_id']; ?>">
                <td><?= $i++; ?></td>
                <td><a href="dieu-chuyen-kho-chi-tiet-<?= $row['kcxl_id']; ?>.html" class="color_blue font_w500">ĐCK - <?= $row['kcxl_id']; ?></a></td>
                <td class="<?= trang_thai_color($row['kcxl_trangThai']); ?>"><?= trang_thai($row['kcxl_trangThai']); ?></td>
                <td style="text-align: left;"><?= $row['kcxl_ghi_chu']; ?></td>
                <td style="text-align: left;">
                <?php 
                    $nguoi_tao = $row['kcxl_nguoiTao'];
                    $user_id = $user[$nguoi_tao];
                    if($nguoi_tao != 0){
                        $ten_nguoi_tao = $user_id['ep_name'];
                        $anh_nguoi_tao = $user_id['ep_image'];
                        if($anh_nguoi_tao == ""){
                            $anh = '../images/ava_ad.png';
                        }else{
                            $anh = 'https://chamcong.24hpay.vn/upload/employee/'.$anh_nguoi_tao;
                        }
                    }else{
                        $ten_nguoi_tao = $tt_user['com_name'];
                        $anh = 'https://chamcong.24hpay.vn/upload/company/logo/' . $tt_user['com_logo'];
                    }
                ?>
                <div class="d_flex flex_start align_c">
                    <img src="<?= $anh; ?>" alt="" class="<?= ($anh == 'https://chamcong.24hpay.vn/upload/company/logo/') ? 'display_none' : '' ?>">
                    <p><?= $ten_nguoi_tao; ?></p>
                </div>
                </td>
                <td><?php echo date('d/m/Y',strtotime($row['kcxl_ngayTao'])); ?></td>
                <td><?php echo date('d/m/Y',strtotime($row['kcxl_ngayThucHienDieuChuyen'])); ?></td>
                <td><?php echo date('d/m/Y',strtotime($row['kcxl_ngayYeuCauHoanThanh'])); ?></td>
                <?
                $kho_nhap = $row['kcxl_khoNhap'];
                $kho_id = new db_query("SELECT `kho_id`, `kho_name` FROM `kho` WHERE `kho_id` = '".$kho_nhap."' ");
                $kho_name = mysql_fetch_assoc($kho_id->result)['kho_name'];

                $kho_xuat = $row['kcxl_khoXuat'];
                $kho_id1 = new db_query("SELECT `kho_id`, `kho_name` FROM `kho` WHERE `kho_id` = '".$kho_xuat."' ");
                $kho_name1 = mysql_fetch_assoc($kho_id1->result)['kho_name'];
                ?>
                <td style="text-align: left;"><?= $kho_name1 ?></td>
                <td style="text-align: left;"><?= $kho_name; ?></td>
                <?php if(in_array(3, $ro_dc_kho) || in_array(4, $ro_dc_kho)) {?>
                    <td>
                        <?php if(in_array(3, $ro_dc_kho) && $_SESSION['quyen'] == '2' && ($row['kcxl_trangThai'] == 1 || $row['kcxl_trangThai'] == 2)){?>
                            <a class="color_blue font_s14 line_h17 font_w500 cursor_p" href="/dieu-chuyen-kho-chinh-sua-<?=$row['kcxl_id']?>.html">
                                <img src="../images/edit_tb.png" alt="">Sửa
                            </a>
                        <?php }elseif(in_array(3, $ro_dc_kho) && $_SESSION['quyen'] == '1' && ($row['kcxl_trangThai'] == 1 || $row['kcxl_trangThai'] == 2)){?>
                            <a class="color_blue font_s14 line_h17 font_w500 cursor_p" href="/dieu-chuyen-kho-chinh-sua-<?=$row['kcxl_id']?>.html">
                                <img src="../images/edit_tb.png" alt="">Sửa
                            </a>
                        <?php }?>
                        <?php if(in_array(3, $ro_dc_kho) && in_array(4, $ro_dc_kho)) {?>
                            <span style="border-left: 1px solid #4C5BD4; margin: auto 5px;" class="<?=((in_array(3, $ro_dc_kho) || in_array(4, $ro_dc_kho)) && $row['kcxl_trangThai'] == 7) ? 'display_none' : '' ?>"></span>
                        <?php }?>
                        <?php if(in_array(4, $ro_dc_kho)){?>
                            <span class="a_del color_red font_s14 line_h17 font_w500 cursor_p">
                            <img src="../images/del_tb.png" alt="">Xóa
                        <?php }?>
                        </span>
                    </td>
                <?php }?>
            </tr>
            <?php }?>
        </table>
        </div>
    </div>
    <div class="pre_q d_flex align_c flex_center position_a display_none" onclick="pre_q(this)">
        <span class="pre_arrow"></span>
    </div>
    <div class="next_q d_flex align_c flex_center position_a" onclick="next_q(this)">
        <span class="next_arrow"></span>
    </div>
</div>
<div class="w_navigation d_flex space_b align_c">
    <div class="l_nav d_flex align_c">
        <p class="color_grey font_s14 line_h17 font_w400">Hiển thị:</p>
        <select name="" id="" class="show_tr_tb color_grey font_s14 line_h17 font_w400" onchange="display(this)">
            <option value="10" <?= ($curr == 10) ? "selected" : ""?>>10</option>
            <option value="20" <?= ($curr == 20) ? "selected" : ""?>>20</option>
            <option value="30" <?= ($curr == 30) ? "selected" : ""?>>30</option>
            <option value="40" <?= ($curr == 40) ? "selected" : ""?>>40</option>
        </select>
    </div>
    <div class="r_nav">
        <ul class="d_flex font_s13 line_h15 font_wN">
            <?php
                echo generatePageBar3('', $page, $curr, $total, $url, '&', '', 'paging_detail', 'preview', '<', 'next', '>', '', '<<<', '', '>>>');
            ?>
        </ul>
    </div>
</div>

<script>
    $('.show_tr_tb').select2({
        minimumResultsForSearch: -1
    });

    $('.a_del').click(function(){
        var id_phieu = $(this).parent().parent().attr('data-id');
        var nguoi_xoa = <?= json_encode($id_nguoi_xoa) ?>;
        $.ajax({
            url: '../ajax/del_dieu_chuyen_kho.php',
            type: 'POST',
            data: {
                id_phieu: id_phieu,
                nguoi_xoa: nguoi_xoa
            },
            success: function(data) {
                window.location.reload();
            }
        })
    });

</script>