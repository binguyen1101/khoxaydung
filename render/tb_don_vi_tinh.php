<?php
    include("config.php");

    $page = $_POST['page'];
    $sl = getValue('curr','int','POST', '');
    $curr = $sl;
    $start = ($page - 1) * $curr;
    $start = abs($start);

    $input_val = $_POST['input_val'];
    $input_val = trim($input_val);

    $so_phieu = explode('-', $input_val)[0];
    $id_so_phieu = explode('-', $input_val)[1];

    $arr = [];
    $length_id_phieu = strlen($id_so_phieu);
    for ($push = 0; $push < $length_id_phieu; $push++) {
        $ki_tu = substr($id_so_phieu, $push, 1);
        array_push($arr, $ki_tu);
    }

    $count_arr = count($arr);
    $kcxl_id = '';

    if ($arr[0] > 0) {
        $kcxl_id = implode('', $arr);
    } elseif ($arr[0] == 0) {
        for ($j = 1; $j < $count_arr; $j++) {
            if ($arr[$j] > 0) {
                $kcxl_id = substr(implode('', $arr), $j, $count_arr - $j);
                break;
            }
        }
    }
    if (isset($_COOKIE['user']) && $_COOKIE['user'] != "" && $_COOKIE['role'] == 1) {
        $token = $_COOKIE['acc_token'];
        $curl = curl_init();
        $data = array();
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/user_info_company.php');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));

        $response = curl_exec($curl);
        curl_close($curl);
        $data_tt = json_decode($response, true);
        $tt_user = $data_tt['data']['user_info_result'];
        $id_nguoi_xoa = $_SESSION['com_id'];
    }

    if (isset($_COOKIE['user']) && $_COOKIE['user'] != "" && $_COOKIE['role'] == 2) {
        $token = $_COOKIE['acc_token'];
        $curl = curl_init();
        $data = array();
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/user_info_employee.php');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));
        $response = curl_exec($curl);
        curl_close($curl);
        $data_tt = json_decode($response, true);
        $tt_user = $data_tt['data']['user_info_result'];
        $id_nguoi_xoa = $_SESSION['ep_id'];
    }

    $id_cty = $tt_user['com_id'];

    $don_vi_tinh = new db_query("SELECT * FROM `don-vi-tinh` WHERE `dvt_check`=1 AND `dvt_id_ct` = $id_cty LIMIT $start,$curr");

    if ($input_val != "") {
        $don_vi_tinh = new db_query("SELECT * FROM `don-vi-tinh`
            WHERE (`dvt_maDonViTinh` = '$so_phieu' AND `dvt_id` = '$kcxl_id' AND `dvt_check` = 1) OR (`dvt_name` LIKE '%$input_val%' AND `dvt_check` = 1)  AND `dvt_id_ct` = $id_cty ORDER BY `dvt_id` DESC LIMIT $start,$curr");
    } else {
        $don_vi_tinh = new db_query("SELECT * FROM `don-vi-tinh` WHERE `dvt_check`=1  AND `dvt_id_ct` = $id_cty ORDER BY `dvt_id` DESC LIMIT $start,$curr");
    }

    $total_sql =  "SELECT COUNT(`dvt_id`) AS total FROM `don-vi-tinh`
        WHERE `dvt_check` = 1 AND `dvt_id_ct` = $id_cty AND (`dvt_id` = '$kcxl_id' OR `dvt_id` LIKE '%$input_val%') ";

    $total_sql = new db_query($total_sql);
    $total = mysql_fetch_assoc($total_sql->result)['total'];

    $url = '/don-vi-tinh.html';
    if ($input_val == "" && $curr == 10) {
        $url = '/don-vi-tinh.html?dis='.$curr;
    } else {
        $url = '/don-vi-tinh.html?input='.$input_val."&dis=".$curr;
    }

?>

<div class="table_gr_vt_scr d_flex align_c position_r">
    <div class="table_gr_vt table_vt_scr" onscroll="table_scroll(this)">
        <table>
            <tr class="tit_tbl color_white font_s16 line_h19 font_w500 back_blue">
                <th>STT<span class="span_tbody"></span></th>
                <th>Mã đơn vị tính<span class="span_tbody"></span></th>
                <th>Tên đơn vị tính<span class="span_tbody"></span></th>
                <th class="th_desc">Mô tả<?php if (in_array(3, $ro_dvt) || in_array(4, $ro_dvt)) { ?><span class="span_tbody"></span><? } ?></th>
                <?php if (in_array(3, $ro_dvt) || in_array(4, $ro_dvt)) { ?>
                    <th style="width: 136px;">Chức năng</th>
                <? } ?>
            </tr>
            <?php $i = 1;
            while ($row = mysql_fetch_assoc($don_vi_tinh->result)) : ?>
                <tr class="color_grey font_s14 line_h17 font_w400" data-id=<?= $row['dvt_id']; ?>>
                    <td><?php echo $i; ?></td>
                    <td>ĐVT - <?= $row['dvt_id']; ?></td>
                    <td style="text-align: left; padding-left: 15px;"><a class="font_w500 color_grey"><?php echo $row['dvt_name']; ?></a></td>
                    <td style="text-align: left; padding-left: 15px;">
                        <div class="d_flex space_b">
                            <p class="td_desc"><?php echo $row['dvt_description']; ?></p>
                            <p class="xem_them color_blue font_s14 line_h16 font_w400 cursor_p display_none">Xem thêm</p>
                        </div>
                    </td>
                    <?php if (in_array(3, $ro_dvt) || in_array(4, $ro_dvt)) { ?>
                        <td>
                            <?php if (in_array(3, $ro_dvt)) { ?>
                                <a class="a_edit color_blue font_s14 line_h17 font_w500 cursor_p">
                                    <img src="../images/edit_tb.png" alt="">Sửa
                                </a>
                            <? } ?>
                            <?php if (in_array(3, $ro_dvt) && in_array(4, $ro_dvt)) { ?><span style="border-left: 1px solid #4C5BD4; margin: auto 5px;"></span><? } ?>
                            <?php if (in_array(4, $ro_dvt)) { ?>
                                <a class="a_del color_red font_s14 line_h17 font_w500 cursor_p">
                                    <img src="../images/del_tb.png" alt="">Xóa
                                </a>
                            <? } ?>
                        </td>
                    <? } ?>
                </tr>
            <?php $i++;
            endwhile; ?>
        </table>
    </div>
    <div class="pre_q d_flex align_c flex_center position_a display_none" onclick="pre_q(this)">
        <span class="pre_arrow"></span>
    </div>
    <div class="next_q d_flex align_c flex_center position_a display_none" onclick="next_q(this)">
        <span class="next_arrow"></span>
    </div>
</div>
<div class="w_navigation d_flex space_b align_c">
    <div class="l_nav d_flex align_c">
        <p class="color_grey font_s14 line_h17 font_w400">Hiển thị:</p>
        <select name="" id="" class="show_tr_tb color_grey font_s14 line_h17 font_w400" onchange="display(this)">
            <option value="10" <?= ($curr == 10) ? "selected" : ""?>>10</option>
            <option value="20" <?= ($curr == 20) ? "selected" : ""?>>20</option>
            <option value="30" <?= ($curr == 30) ? "selected" : ""?>>30</option>
            <option value="40" <?= ($curr == 40) ? "selected" : ""?>>40</option>
        </select>
    </div>
    <div class="r_nav">
        <ul class="d_flex font_s13 line_h15 font_wN">
            <?php
                echo generatePageBar3('', $page, $curr, $total, $url, '&', '', 'paging_detail', 'preview', '<', 'next', '>', '', '<<<', '', '>>>');
            ?>
        </ul>
    </div>
</div>
    <script type="text/javascript" src="../js/ghi_chu.js"></script>

    <script>
        $('.show_tr_tb').select2({
            minimumResultsForSearch: -1
        });
        var id_ct = <?= json_encode($id_cty); ?>;

        $(".icon_sr_wh").click(function() {
            var input_val = $("input[name='input_search'").val();
            $.ajax({
                url: '../render/tb_don_vi_tinh.php',
                data: {
                    input_val: input_val
                },
                type: 'POST',

                success: function(data) {
                    $(".table_gr_vt").html(data);
                }
            });
        })

        $(document).keyup(function(e) {
            if ($("input[name='input_search']").is(":focus") && (e.keyCode == 13)) {
                var input_val = $("input[name='input_search'").val();
                $.ajax({
                    url: '../render/tb_don_vi_tinh.php',
                    data: {
                        input_val: input_val
                    },
                    type: 'POST',

                    success: function(data) {
                        $(".table_gr_vt").html(data);
                    }
                });
            }
        });

        $('.popup_func_dv_add .btn_save').click(function() {
            var dvt_name = $("input[name='name_unit_add']").val();
            var nvttb_description = $("textarea[name='dep_unit_add']").val();
            var form_valid = $(".f_func_add");
            form_valid.validate({
                errorPlacement: function(error, element) {
                    error.appendTo(element.parents(".name_unit_add"));
                    error.wrap("<span class='error'>");
                },
                rules: {
                    name_unit_add: "required",
                },
                messages: {
                    name_unit_add: "Vui lòng nhập tên đơn vị tính.",
                },
            });
            if (form_valid.valid() === true) {
                $.ajax({
                    url: '../ajax/add_don_vi_tinh.php',
                    type: 'POST',
                    data: {
                        id_ct: id_ct,
                        dvt_name: dvt_name,
                        nvttb_description: nvttb_description
                    },
                    success: function(data) {
                        if (data == '') {
                            $('#popup_add_notif_succ').css('z-index', '2');
                            $('.popup_func_dv_add').css('z-index', '1');
                            $('#popup_add_notif_succ .p_add_succ').find('strong').text(dvt_name);
                            var text_stt = 'thành công!'
                            $('#popup_add_notif_succ .p_add_succ').find('strong + span').text(text_stt);
                            $('#popup_add_notif_succ').show();
                        } else if (data != '') {
                            alert("Thêm mới thất bại!");
                        }
                    }
                });
            }
        });

        $('.unit .a_edit').click(function() {
            $('#popup_func_dv_edit').show();
            var id_dvt = $(this).parent().parent().attr('data-id');
            var name = $(this).parent().parent().find('td:nth-child(3) a').text();
            var dep = $(this).parent().parent().find('.td_desc').text();

            $("input[name='name_unit_edit']").val(name);
            $("textarea[name='dep_unit_edit']").val(dep);

            $('.popup_func_dv_edit .btn_save').click(function() {
                var ten_dvt = $("input[name='name_unit_edit']").val();
                var mo_ta = $("textarea[name='dep_unit_edit']").val();
                var form_valid = $(".f_func_edit");
                form_valid.validate({
                    errorPlacement: function(error, element) {
                        error.appendTo(element.parents(".name_unit_edit"));
                        error.wrap("<span class='error'>");
                    },
                    rules: {
                        name_unit_edit: "required"
                    },
                    messages: {
                        name_unit_edit: "Vui lòng nhập tên đơn vị tính."
                    },
                });
                if (form_valid.valid() === true) {
                    $.ajax({
                        url: '../ajax/edit_don_vi_tinh.php',
                        type: 'POST',
                        data: {
                            id_ct: id_ct,
                            id_dvt: id_dvt,
                            ten_dvt: ten_dvt,
                            mo_ta: mo_ta
                        },
                        success: function(data) {
                            if (data == '') {
                                $('#popup_add_notif_succ').css('z-index', '2');
                                $('.popup_func_dv_edit').css('z-index', '1');
                                var text = $('#popup_add_notif_succ .p_add_succ').text('');
                                var text_new = '';
                                $('#popup_add_notif_succ').show();
                                text_new += 'Chỉnh sửa đơn vị tính';
                                text_new += '<strong>';
                                text_new += '&nbsp' + ten_dvt;
                                text_new += '</strong>';
                                text_new += '&nbspthành công!';
                                text.append(text_new);
                            } else if (data != '') {
                                alert("Chỉnh sửa thất bại!");
                            }
                        }
                    });
                }
            });
            $('.btn_cancel,.close_popup').on('click', function() {
                $('.popup_func_dv_edit .btn_save').off();
            });
        });

        $('.unit .a_del').click(function() {
            $('#popup_func_del').show();
            var id_dvt = $(this).parent().parent().attr('data-id');
            var name = $(this).parent().parent().find('td:nth-child(3) a').text();
            var nguoi_xoa = <?= json_encode($id_nguoi_xoa) ?>;
            $('#popup_func_del strong').text(name);

            $('.popup_func_del .btn_save').click(function() {
                $.ajax({
                    url: '../ajax/del_don_vi_tinh.php',
                    type: 'POST',
                    data: {
                        id_ct: id_ct,
                        id_dvt: id_dvt,
                        nguoi_xoa: nguoi_xoa
                    },
                    success: function(data) {
                        if (data == '') {
                            $('#popup_add_notif_succ').css('z-index', '2');
                            $('.popup_func_del').css('z-index', '1');
                            $('#popup_add_notif_succ').show();
                            var text = $('#popup_add_notif_succ .p_add_succ').text('');
                            var text_new = '';
                            text_new += 'Xóa đơn vị tính';
                            text_new += '<strong>';
                            text_new += '&nbsp' + name;
                            text_new += '</strong>';
                            text_new += '&nbspthành công!';
                            text.append(text_new);
                        } else if (data != '') {
                            alert("Xóa thất bại!");
                        }
                    }
                });
            });
            $('.btn_cancel,.close_popup').on('click', function() {
                $('.popup_func_del .btn_save').off();
            });
        });

        $('.btn_close').click(function() {
            window.location.href = "/don-vi-tinh.html";
        });
    </script>