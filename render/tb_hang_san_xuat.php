<?php
    include("config.php");

    $page = $_POST['page'];
    $sl = getValue('curr','int','POST', '');
    $curr = $sl;
    $start = ($page - 1 )*$curr;
    $start = abs($start);

    $input_val = $_POST['input_val'];
    $input_val = trim($input_val);

    $so_phieu = explode('-',$input_val)[0];
    $id_so_phieu = explode('-',$input_val)[1];

    $arr = [];
    $length_id_phieu = strlen($id_so_phieu);
    for($push = 0; $push<$length_id_phieu; $push++){
        $ki_tu = substr($id_so_phieu,$push,1);
        array_push($arr,$ki_tu);
    }

    $count_arr = count($arr);
    $kcxl_id = '';

    if($arr[0] > 0){
        $kcxl_id = implode('',$arr);
    }
    elseif($arr[0] == 0){
        for($j = 1; $j < $count_arr; $j++){
            if($arr[$j] > 0){
                $kcxl_id = substr(implode('',$arr),$j ,$count_arr-$j);
                break;
            }
        }
    }

    if (isset($_COOKIE['user']) && $_COOKIE['user'] != "" && $_COOKIE['role'] == 2) {
		$token = $_COOKIE['acc_token'];
		$curl = curl_init();
		$data = array();
		curl_setopt($curl, CURLOPT_POST, 1);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
		curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/user_info_employee.php');
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));
		$response = curl_exec($curl);
		curl_close($curl);
		$data_tt = json_decode($response, true);
		$tt_user = $data_tt['data']['user_info_result'];
	}
    
    $id_cty = $tt_user['com_id'];
    $id_nguoi_xoa = $_SESSION['ep_id'];

    if($input_val != ""){
        $item = new db_query("SELECT `hsx_id`, `hsx_maHangSanXuat`, `hsx_name`, `hsx_description` FROM `hang-san-xuat`
        WHERE ((`hsx_maHangSanXuat` = '$so_phieu' AND `hsx_id` = '$kcxl_id' AND `hsx_check` = 1) OR (`hsx_name` LIKE '%$input_val%' AND `hsx_check` = 1))  AND `hsx_id_ct` = $id_cty ORDER BY `hsx_id` DESC LIMIT $start,$curr");
    } else{
        $item = new db_query("SELECT `hsx_id`, `hsx_maHangSanXuat`, `hsx_name`, `hsx_description` FROM `hang-san-xuat` WHERE `hsx_check` = 1 AND `hsx_id_ct` = $id_cty ORDER BY `hsx_id` DESC LIMIT $start,$curr");
    }

    $total_sql =  "SELECT COUNT(`hsx_id`) AS total FROM `hang-san-xuat`
    WHERE `hsx_check` = 1 AND `hsx_id_ct` = $id_cty AND (`hsx_id` = '$kcxl_id' OR `hsx_id` LIKE '%$input_val%') ";

    $total_sql = new db_query($total_sql);
    $total = mysql_fetch_assoc($total_sql->result)['total'];
   
    $url = '/hang-san-xuat.html';
    if ($input_val == "" && $curr == 10) {
        $url = '/hang-san-xuat.html?dis='.$curr;
    }
    else{
        $url = '/hang-san-xuat.html?input='.$input_val."&dis=".$curr;
    }
?>
<div class="position_r d_flex align_c">
  <div class="main_table table_vt_scr" onscroll="table_scroll(this)">
    <table class="table table_manufacturer">
      <tr class="tit_tbl color_white font_s16 line_h19 font_w500 back_blue">
        <td>STT</td>
        <td>Mã hãng sản xuất</td>
        <td>Tên hãng sản xuất</td>
        <td class="th_desc">Mô tả</td>
        <?php if(in_array(3, $ro_hsx) || in_array(4, $ro_hsx)) {?>
        <th>Chức năng
        </th>
        <? }?>
      </tr>
      <? $stt = 1; while (($data = mysql_fetch_assoc($item->result))) { ?>
      <tr class="color_grey font_s14 line_h17 font_w400">
        <td>
          <?= $stt++ ?>
        </td>
        <td>HSX -
          <?= $data['hsx_id'] ?>
        </td>
        <td style="text-align: left; padding-left: 15px;">
          <a class="color_blue font_w500" href="/hang-san-xuat-chi-tiet-<?= $data['hsx_id'] ?>.html">
            <?= $data['hsx_name'] ?>
          </a>
        </td>
        <td style="text-align: left; padding-left: 15px;">
          <div class="d_flex space_b">
            <p class="td_desc">
              <?php echo $data['hsx_description']; ?>
            </p>
            <p class="xem_them color_blue font_s14 line_h16 font_w400 cursor_p display_none">Xem thêm</p>
          </div>
        </td>
        <?php if(in_array(3, $ro_hsx) || in_array(4, $ro_hsx)) {?>
        <td>
          <?php if(in_array(3, $ro_hsx)) {?>
          <a class="color_blue font_s14 line_h17 font_w500 row_manufacturer_id cursor_p"
            onclick="openAndHide('','edit_manufacturer')" data="<?= $data['hsx_id'] ?>">
            <img style="margin-right: 5px;" src="../images/edit_tb.png" alt="">Sửa
          </a>
          <?php }?>
          <?php if(in_array(3, $ro_hsx) && in_array(4, $ro_hsx)) {?>
          <span style="border-left: 1px solid #4C5BD4; margin: auto 5px;"></span>
          <?php }?>
          <?php if(in_array(4, $ro_hsx)) {?>
          <a class="color_red font_s14 line_h17 font_w500 cursor_p xoa" data="<?= $data['hsx_id'] ?>">
            <img style="margin-right: 5px;" src="../images/del_tb.png" alt="">Xóa
          </a>
          <?php }?>
        </td>
        <?php }?>
      </tr>
      <? } ?>
    </table>
  </div>
  <div class="pre_q d_flex align_c flex_center position_a display_none" onclick="pre_q(this)">
    <span class="pre_arrow"></span>
  </div>
  <div class="next_q d_flex align_c flex_center position_a display_none" onclick="next_q(this)">
    <span class="next_arrow"></span>
  </div>
</div>
<div class="w_navigation d_flex space_b align_c">
  <div class="l_nav d_flex align_c">
    <p class="color_grey font_s14 line_h17 font_w400">Hiển thị:</p>
    <select name="" id="" class="show_tr_tb color_grey font_s14 line_h17 font_w400" onchange="display(this)">
      <option value="10" <?=($curr == 10) ? "selected" : "" ?>>10</option>
      <option value="20" <?=($curr == 20) ? "selected" : "" ?>>20</option>
      <option value="30" <?=($curr == 30) ? "selected" : "" ?>>30</option>
      <option value="40" <?=($curr == 40) ? "selected" : "" ?>>40</option>
    </select>
  </div>
  <div class="r_nav">
    <ul class="d_flex font_s13 line_h15 font_wN">
      <?php
                echo generatePageBar3('', $page, $curr, $total, $url, '&', '', 'paging_detail', 'preview', '<', 'next', '>', '', '<<<', '', '>>>');
            ?>
    </ul>
  </div>
</div>

<script type="text/javascript" src="../js/ghi_chu.js"></script>
<script>

  $('.show_tr_tb').select2({
    minimumResultsForSearch: -1
  });

  var id_cty = <?= $id_cty ?>;
  var nguoi_xoa = <?= $id_nguoi_xoa ?>;
  $('.them_moi_hvt').click(function () {
    var form_add = $("#add_manufacturer");
    form_add.validate({
      errorPlacement: function (error, element) {
        error.appendTo(element.parents(".input_name_manufacturer"));
        error.wrap("<span class='error'>")
      },
      rules: {
        name_manufacturer: "required",
      },
      messages: {
        name_manufacturer: "Vui lòng nhập tên nhóm thiết bị vật tư",
      },
    });
    if (form_add.valid() === true) {
      var hsx_name = $('input[name="name_manufacturer"]').val()
      var hsx_description = $('textarea[name="description_manufacturer"]').val()
      $.ajax({
        url: '../ajax/add_hang_san_xuat.php',
        type: 'POST',
        data: {
          id_cty: id_cty,
          hsx_name: hsx_name,
          hsx_description: hsx_description
        },
        success: function (data) {
          if (data == '') {
            var text = $('.add_new_materials_equipment_success .add_new_succ').text('');
            var text_new = '';
            text_new += 'Thêm mới hãng sản xuất';
            text_new += '<strong>';
            text_new += '&nbsp' + hsx_name;
            text_new += '</strong>';
            text_new += '&nbspthành công!';
            text.append(text_new);
            $('.add_new_manufacturer').hide()
            $('.add_new_materials_equipment_success').show()
          } else if (data != '') {
            alert(data);
          }
        }
      })
    }
  });

  $('.row_manufacturer_id').click(function () {
    var id = $(this).attr('data')
    $.ajax({
      url: '../render/detail_hang_san_xuat.php',
      type: 'POST',
      data: {
        id: id
      },
      success: function (data) {
        $('#edit_full_manufacturer').html(data)
      }
    })
  });

  $('.xoa').click(function () {
    var name = $(this).parent().parent().find('td:nth-child(3)').text();
    $('.delete_manufacturer').show();
    $('.delete_manufacturer strong').text(name);
    var id = $(this).attr('data');
    $('.delete_manufacturer .button_accp').on("click", function () {
      $.ajax({
        url: '../ajax/del_hang_san_xuat.php',
        type: 'POST',
        data: {
          id: id,
          id_cty: id_cty,
          nguoi_xoa: nguoi_xoa
        },
        success: function (data) {
          if (data == "") {
            $('.popup_delete_manufacturer_success strong').text(name);
            $('.popup_delete_manufacturer_success').show();
          } else {
            alert("Xóa thất bại!");
          }
        }
      })
    })
    $('.button_close,.btn_close2').click(function () {
      $('.delete_manufacturer .button_accp').off();
    });
  });

  $('.btn_close').click(function () {
    window.location.reload();
  })

  function sua_th(id) {
    var form_add = $("#edit_full_manufacturer");
    form_add.validate({
      errorPlacement: function (error, element) {
        error.appendTo(element.parents(".input_name_manufacturer"));
        error.wrap("<span class='error'>")
      },
      rules: {
        name_manufacturer: "required",
      },
      messages: {
        name_manufacturer: "Vui lòng nhập tên nhóm thiết bị vật tư",
      },
    });
    if (form_add.valid() === true) {
      var id = id;
      var hsx_name = $('input[name="edit_name_manufacturer"]').val()
      var hsx_description = $('textarea[name="edit_description_manufacturer"]').val();
      $.ajax({
        url: '../ajax/edit_hang_san_xuat.php',
        type: 'POST',
        data: {
          id: id,
          hsx_name: hsx_name,
          hsx_description: hsx_description
        },
        success: function (data) {
          if (data != "") {
            var text = $('.add_new_materials_equipment_success .add_new_succ').text('');
            var text_new = '';
            text_new += 'Sửa hãng sản xuất';
            text_new += '<strong>';
            text_new += '&nbsp' + hsx_name;
            text_new += '</strong>';
            text_new += '&nbspthành công!';
            text.append(text_new);
            $('.edit_manufacturer').hide();
            $('.add_new_materials_equipment_success').show();
          } else {
            alert("Lỗi");
          }
        }
      })
    }
  }

</script>