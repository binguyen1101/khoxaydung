<?php

include("config.php");

$id_hsx = getValue('id_hsx', 'int', 'POST', '');
$nvt = getValue('nvt', 'int', 'POST', '');
$xx = getValue('xx', 'int', 'POST', '');
$sort = getValue('sort', 'int', 'POST', '');
$page = getValue('page', 'int', 'POST', 1);

$curr = 10;
$start = ($page - 1) * $curr;
$start = abs($start);

$input_val = $_POST['input_val'];
$input_val = trim($input_val);

$so_phieu = explode('-', $input_val)[0];
$id_so_phieu = explode('-', $input_val)[1];

$arr = [];
$length_id_phieu = strlen($id_so_phieu);
for ($push = 0; $push < $length_id_phieu; $push++) {
    $ki_tu = substr($id_so_phieu, $push, 1);
    array_push($arr, $ki_tu);
}

$count_arr = count($arr);
$kcxl_id = '';

if ($arr[0] > 0) {
    $kcxl_id = implode('', $arr);
} elseif ($arr[0] == 0) {
    for ($j = 1; $j < $count_arr; $j++) {
        if ($arr[$j] > 0) {
            $kcxl_id = substr(implode('', $arr), $j, $count_arr - $j);
            break;
        }
    }
}
if (isset($_COOKIE['user']) && $_COOKIE['user'] != "" && $_COOKIE['role'] == 1) {
    $token = $_COOKIE['acc_token'];
    $curl = curl_init();
    $data = array();
    curl_setopt($curl, CURLOPT_POST, 1);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
    curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/user_info_company.php');
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));

    $response = curl_exec($curl);
    curl_close($curl);
    $data_tt = json_decode($response, true);
    $tt_user = $data_tt['data']['user_info_result'];
    $user_id = $_SESSION['com_id'];
    $user_name = $_SESSION['com_name'];
    $id_nguoi_xoa = $_SESSION['com_id'];
}
if (isset($_COOKIE['user']) && $_COOKIE['user'] != "" && $_COOKIE['role'] == 2) {
    $token = $_COOKIE['acc_token'];
    $curl = curl_init();
    $data = array();
    curl_setopt($curl, CURLOPT_POST, 1);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
    curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/user_info_employee.php');
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));
    $response = curl_exec($curl);
    curl_close($curl);
    $data_tt = json_decode($response, true);
    $tt_user = $data_tt['data']['user_info_result'];
    $user_id = $_SESSION['ep_id'];
    $user_name = $_SESSION['ep_name'];
    $id_nguoi_xoa = $_SESSION['ep_id'];
}

$id_cty = $tt_user['com_id'];

if ($input_val != "") {
    if ($_COOKIE['role'] == 1) {
        $item = "SELECT `dsvt_id`, `dsvt_name`, `nvt_name`, `dvt_name`, `dsvt_donGia`, `xx_name`, `dsvt_description` FROM `danh-sach-vat-tu` 
        LEFT JOIN `nhom-vat-tu-thiet-bi` ON `dsvt_nhomVatTuThietBi` = `nvt_id` AND `nvt_check` =1
        LEFT JOIN `don-vi-tinh` ON `dsvt_donViTinh` = `dvt_id` AND `dvt_check` =1
        LEFT JOIN `xuat-xu` ON `dsvt_xuatXu` = `xx_id`
        WHERE `dsvt_hangSanXuat` = $id_hsx And `dsvt_check` = 1
        AND (`dsvt_id` = '$kcxl_id' OR `dsvt_id` LIKE '%$input_val%') AND `dsvt_id_ct` = $id_cty ";

        $total_sql =  "SELECT COUNT(`dsvt_id`) AS total FROM `danh-sach-vat-tu` 
        LEFT JOIN `nhom-vat-tu-thiet-bi` ON `dsvt_nhomVatTuThietBi` = `nvt_id` AND `nvt_check` =1
        LEFT JOIN `don-vi-tinh` ON `dsvt_donViTinh` = `dvt_id` AND `dvt_check` =1
        LEFT JOIN `xuat-xu` ON `dsvt_xuatXu` = `xx_id`
        WHERE `dsvt_hangSanXuat` = $id_hsx And `dsvt_check` = 1
        AND (`dsvt_id` = '$kcxl_id' OR `dsvt_id` LIKE '%$input_val%') AND `dsvt_id_ct` = $id_cty ";
    }
    if ($_COOKIE['role'] == 2) {
        $item = "SELECT `dsvt_id`, `dsvt_name`, `nvt_name`, `dvt_name`, `dsvt_donGia`, `xx_name`, `dsvt_description` FROM `danh-sach-vat-tu` 
        LEFT JOIN `nhom-vat-tu-thiet-bi` ON `dsvt_nhomVatTuThietBi` = `nvt_id` AND `nvt_check` =1
        LEFT JOIN `don-vi-tinh` ON `dsvt_donViTinh` = `dvt_id` AND `dvt_check` =1
        LEFT JOIN `xuat-xu` ON `dsvt_xuatXu` = `xx_id`
        WHERE `dsvt_hangSanXuat` = $id_hsx And `dsvt_check` = 1
        AND (`dsvt_id` = '$kcxl_id' OR `dsvt_id` LIKE '%$input_val%') AND `dsvt_id_ct` = $id_cty AND NOT `dsvt_userCreateId` = 0  ";

        $total_sql =  "SELECT COUNT(`dsvt_id`) AS total FROM `danh-sach-vat-tu` 
        LEFT JOIN `nhom-vat-tu-thiet-bi` ON `dsvt_nhomVatTuThietBi` = `nvt_id` AND `nvt_check` =1
        LEFT JOIN `don-vi-tinh` ON `dsvt_donViTinh` = `dvt_id` AND `dvt_check` =1
        LEFT JOIN `xuat-xu` ON `dsvt_xuatXu` = `xx_id`
        WHERE `dsvt_hangSanXuat` = $id_hsx And `dsvt_check` = 1
        AND (`dsvt_id` = '$kcxl_id' OR `dsvt_id` LIKE '%$input_val%') AND `dsvt_id_ct` = $id_cty AND NOT `dsvt_userCreateId` = 0 ";
    }
} else {
    if ($_COOKIE['role'] == 1) {
        $item = "SELECT `dsvt_id`, `dsvt_name`, `nvt_name`, `dvt_name`, `dsvt_donGia`, `xx_name`, `dsvt_description` FROM `danh-sach-vat-tu` 
        LEFT JOIN `nhom-vat-tu-thiet-bi` ON `dsvt_nhomVatTuThietBi` = `nvt_id` AND `nvt_check` =1
        LEFT JOIN `don-vi-tinh` ON `dsvt_donViTinh` = `dvt_id` AND `dvt_check` =1
        LEFT JOIN `xuat-xu` ON `dsvt_xuatXu` = `xx_id`
        WHERE `dsvt_hangSanXuat` = $id_hsx And `dsvt_check` = 1 AND `dsvt_id_ct` = $id_cty ";

        $total_sql =  "SELECT COUNT(`dsvt_id`) AS total FROM `danh-sach-vat-tu` 
        LEFT JOIN `nhom-vat-tu-thiet-bi` ON `dsvt_nhomVatTuThietBi` = `nvt_id` AND `nvt_check` =1
        LEFT JOIN `don-vi-tinh` ON `dsvt_donViTinh` = `dvt_id` AND `dvt_check` =1
        LEFT JOIN `xuat-xu` ON `dsvt_xuatXu` = `xx_id`
        WHERE `dsvt_hangSanXuat` = $id_hsx And `dsvt_check` = 1 AND `dsvt_id_ct` = $id_cty ";
    }
    if ($_COOKIE['role'] == 2) {
        $item = "SELECT `dsvt_id`, `dsvt_name`, `nvt_name`, `dvt_name`, `dsvt_donGia`, `xx_name`, `dsvt_description` FROM `danh-sach-vat-tu` 
        LEFT JOIN `nhom-vat-tu-thiet-bi` ON `dsvt_nhomVatTuThietBi` = `nvt_id` AND `nvt_check` =1
        LEFT JOIN `don-vi-tinh` ON `dsvt_donViTinh` = `dvt_id` AND `dvt_check` =1
        LEFT JOIN `xuat-xu` ON `dsvt_xuatXu` = `xx_id`
        WHERE `dsvt_hangSanXuat` = $id_hsx And `dsvt_check` = 1 AND `dsvt_id_ct` = $id_cty AND NOT `dsvt_userCreateId` = 0  ";

        $total_sql =  "SELECT COUNT(`dsvt_id`) AS total FROM `danh-sach-vat-tu` 
        LEFT JOIN `nhom-vat-tu-thiet-bi` ON `dsvt_nhomVatTuThietBi` = `nvt_id`  AND `nvt_check` =1
        LEFT JOIN `don-vi-tinh` ON `dsvt_donViTinh` = `dvt_id` AND `dvt_check` =1
        LEFT JOIN `xuat-xu` ON `dsvt_xuatXu` = `xx_id`
        WHERE `dsvt_hangSanXuat` = $id_hsx And `dsvt_check` = 1 AND `dsvt_id_ct` = $id_cty AND NOT `dsvt_userCreateId` = 0 ";
    }
}

if ($sort == "" && $nvt == "" && $xx == "" && $input_val == "") {
    $url = "/hang-san-xuat-chi-tiet-" . $id_nhom . ".html";
} else {
    $url = "/hang-san-xuat-chi-tiet-" . $id_nhom . ".html?nvt=" . $nvt . "&xx=" . $xx . "&sort=" . $sort . "&input=" . $input_val;
}

if ($nvt == "") {
    $dk_nvt = "";
} else {
    $dk_nvt = "AND `nvt_id` = $nvt ";
}

if ($xx == "") {
    $dk_xx = "";
} else {
    $dk_xx = "AND `xx_id` = $xx ";
}

if ($sort == "") {
    $dk_sort = "ORDER BY `dsvt_id` DESC ";
} else if ($sort == 1) {
    $dk_sort = "ORDER BY `dsvt_donGia` ASC ";
} else if ($sort == 2) {
    $dk_sort = "ORDER BY `dsvt_donGia` DESC ";
}


$limited = "LIMIT $start,$curr";

$item .= $dk_nvt;
$item .= $dk_xx;
$item .= $dk_sort;
$item .= $limited;
$item = new db_query($item);

$total_sql1 = new db_query($total_sql);
$total = mysql_fetch_assoc($total_sql1->result)['total'];

?>

<div class="position_r d_flex align_c">
    <div class="main_table table_vt_scr" onscroll="table_scroll(this)">
        <table class="table table_detail_manufacturer">
            <tr class="tit_tbl color_white font_s16 line_h19 font_w500 back_blue">
                <td>STT</td>
                <td>Mã vật tư thiết bị</td>
                <td>Tên đầy đủ vật tư thiết bị</td>
                <td>Nhóm vật tư thiết bị</td>
                <td>Đơn vị tính</td>
                <td>Xuất xứ</td>
                <td>Mô tả</td>
                <td>Đơn giá (VNĐ)</td>
                <?php if (in_array(3, $ro_hsx) || in_array(4, $ro_hsx)) { ?>
                    <th>Chức năng
                        <span class="span_tbody"></span>
                    </th>
                <? } ?>
            </tr>
            <? $stt = 1;
            while ($data = mysql_fetch_assoc($item->result)) { ?>
                <tr class="color_grey font_s14 line_h17 font_w400">
                    <td>
                        <?= $stt++ ?>
                    </td>
                    <td>VT -
                        <?= $data['dsvt_id'] ?>
                    </td>
                    <td class="color_blue font_w500" style="text-align: left; padding-left: 15px;">
                        <a class="color_blue" href="/danh-sach-vat-tu-thiet-bi-chi-tiet-<?= $data['dsvt_id'] ?>.html">
                            <?= $data['dsvt_name'] ?>
                        </a>
                    </td>
                    <td style="text-align: left;">
                        <?= $data['nvt_name'] ?>
                    </td>
                    <td>
                        <?= $data['dvt_name'] ?>
                    </td>
                    <td>
                        <?= $data['xx_name'] ?>
                    </td>
                    <td style="text-align: left;">
                        <?= $data['dsvt_description'] ?>
                    </td>
                    <td style="text-align: right;">
                        <?= $data['dsvt_donGia'] ?>
                    </td>
                    <?php if (in_array(3, $ro_hsx) || in_array(4, $ro_hsx)) { ?>
                        <td>
                            <p class="d_flex align_c flex_center">
                                <?php if (in_array(3, $ro_hsx)) { ?>
                                    <img src="../images/pen_blu.png" alt="">
                                    <a class="color_blue" href="/danh-sach-vat-tu-thiet-bi-sua-<?= $data['dsvt_id'] ?>.html">Sửa</a>
                                <?php } ?>
                                <?php if (in_array(3, $ro_hsx) && in_array(4, $ro_hsx)) { ?>
                                    <span class="color_blue margin_lr5">|</span>
                                <?php } ?>
                                <?php if (in_array(4, $ro_hsx)) { ?>
                                    <img src="../images/delete_r.png" alt="">
                                    <span class="color_red">Xóa</span>
                                <?php } ?>
                            </p>
                        </td>
                    <?php } ?>
                </tr>
            <? } ?>
        </table>
    </div>
    <div class="pre_q d_flex align_c flex_center position_a display_none" onclick="pre_q(this)">
        <span class="pre_arrow"></span>
    </div>
    <div class="next_q d_flex align_c flex_center position_a" onclick="next_q(this)">
        <span class="next_arrow"></span>
    </div>
</div>
<div class="w_navigation d_flex align_c flex_end" id="block08">
    <!-- <div class="d_flex align_c mt_15">
            <p class="line_h16 font_s14 color_grey">Hiển thị:</p>
            <div class="d_flex align_c space_b list_choonse_item ml_10 position_r cursor_p">
                <div class="d_flex align_c" onclick="toggle('list_item_choonse_show')">
                    <p class="line_h16 font_s14 color_grey">40</p>
                    <img class="ml_10" src="../images/list_item.png" alt="">
                </div>
                <div class="list_item_choonse_show position_a" style="display: none;">
                    <p class="item_choonse_show text_a_c font_s14 line_16 color_grey">10</p>
                    <p class="item_choonse_show text_a_c font_s14 line_16 color_grey">20</p>
                    <p class="item_choonse_show text_a_c font_s14 line_16 color_grey">40</p>
                    <p class="item_choonse_show text_a_c font_s14 line_16 color_grey">80</p>
                    <p class="item_choonse_show text_a_c font_s14 line_16 color_grey">100</p>
                </div>
            </div>
        </div> -->
    <div class="r_nav">
        <ul class="d_flex font_s13 line_h15 font_wN">
            <?php
            if ($nvt != "" || $xx != "" || $sort != "" || $input_val != "") {
                echo generatePageBar3('', $page, $curr, $total, $url, '&', '', 'paging_detail', 'preview', '<', 'next', '>', '', '<<<', '', '>>>');
            } else {
                echo generatePageBar3('', $page, $curr, $total, $url, '?', '', 'paging_detail', 'preview', '<', 'next', '>', '', '<<<', '', '>>>');
            }
            ?>
        </ul>
    </div>
</div>