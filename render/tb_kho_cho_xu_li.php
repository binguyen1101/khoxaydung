
<?php 
    include("config.php");
    
    $type = $_POST['type'];
    $page = getValue('page','int','POST', '');
    $sl = getValue('curr','int','POST', '');
    $curr = $sl;
    $start = ($page - 1 )*$curr;
    $start = abs($start);

    if(isset($_COOKIE['acc_token']) && isset($_COOKIE['rf_token']) && isset($_COOKIE['role']) && $_COOKIE['role'] == 2){
        $curl = curl_init();
        $token = $_COOKIE['acc_token'];
        curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/list_all_my_partner.php?get_all=true');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer '.$token));
        $response = curl_exec($curl);
        curl_close($curl);

        $data_list = json_decode($response,true);
        $data_list_nv =$data_list['data']['items'];
    }else{
        $curl = curl_init();
        $token = $_COOKIE['acc_token'];
        curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/list_all_employee_of_company.php');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer '.$token));
        $response = curl_exec($curl);
        curl_close($curl);

        $data_list = json_decode($response,true);
        $data_list_nv =$data_list['data']['items'];

    }
    $count = count($data_list_nv);

    $user = [];
    for ($i = 0; $i < count($data_list_nv); $i++){
        $nv = $data_list_nv[$i];
        $user[$nv["ep_id"]] = $nv;
    }

    if (isset($_COOKIE['user']) && $_COOKIE['user'] != "" && $_COOKIE['role'] == 2) {
		$token = $_COOKIE['acc_token'];
		$curl = curl_init();
		$data = array();
		curl_setopt($curl, CURLOPT_POST, 1);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
		curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/user_info_employee.php');
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));
		$response = curl_exec($curl);
		curl_close($curl);
		$data_tt = json_decode($response, true);
		$tt_user = $data_tt['data']['user_info_result'];
        $user_id = $_SESSION['ep_id'];

	}else if (isset($_COOKIE['user']) && $_COOKIE['user'] != "" && $_COOKIE['role'] == 1) {
        $token = $_COOKIE['acc_token'];
        $curl = curl_init();
        $data = array();
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/user_info_company.php');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));
        $response = curl_exec($curl);
        curl_close($curl);
        $data_tt = json_decode($response, true);
        $tt_user = $data_tt['data']['user_info_result'];
        $user_id = $_SESSION['com_id'];

    }

    $id_cty = $tt_user['com_id'];

    $input_val = $_POST['input_val'];

    $input_val = trim($input_val);

    $so_phieu = explode('-',$input_val)[0];
    $id_so_phieu = explode('-',$input_val)[1];

    $arr = [];
    $length_id_phieu = strlen($id_so_phieu);
    for($push = 0; $push<$length_id_phieu; $push++){
        $ki_tu = substr($id_so_phieu,$push,1);
        array_push($arr,$ki_tu);
    }

    $count_arr = count($arr);
    $kcxl_id = '';

    if($arr[0] > 0){
        $kcxl_id = implode('',$arr);
    }
    elseif($arr[0] == 0){
        for($j = 1; $j < $count_arr; $j++){
            if($arr[$j] > 0){
                $kcxl_id = substr(implode('',$arr),$j ,$count_arr-$j);
                break;
            }
        }
    }
    
    if ($type == "" && $input_val == "" && $curr == 10) {
        $url = '/nghiep-vu-kho-cho-xu-ly.html?dis='.$curr;
    }
    else{
        $url = '/nghiep-vu-kho-cho-xu-ly.html?type='.$type."&input=".$input_val."&dis=".$curr;
    }

    if($input_val != ""){
        if($_COOKIE['role'] == 1){
            $kho_cho_xu_li = "SELECT `kcxl_id`, `kcxl_soPhieu`, `kcxl_trangThai`, `kcxl_nguoiTao`, `kcxl_ngayTao`, `kcxl_nguoiThucHien`, `kcxl_ghi_chu` 
            FROM `kho-cho-xu-li` 
            WHERE `kcxl_check` = 1
            AND NOT `kcxl_trangThai` = 4 
            AND NOT `kcxl_trangThai` = 6 
            AND NOT `kcxl_trangThai` = 7 
            AND NOT `kcxl_trangThai` = 12 
            AND (`kcxl_id` = '$kcxl_id' OR `kcxl_id` LIKE '%$input_val%') AND `kcxl_id_ct` = $id_cty ";

            $total_sql =  "SELECT COUNT(`kcxl_check`) AS total FROM `kho-cho-xu-li` 
            WHERE `kcxl_check` = 1
            AND NOT `kcxl_trangThai` = 4 
            AND NOT `kcxl_trangThai` = 6 
            AND NOT `kcxl_trangThai` = 7 
            AND NOT `kcxl_trangThai` = 12 AND (`kcxl_id` = '$kcxl_id' OR `kcxl_id` LIKE '%$input_val%') AND `kcxl_id_ct` = $id_cty ";
        }
        if($_COOKIE['role'] == 2){
            $kho_cho_xu_li = "SELECT `kcxl_id`, `kcxl_soPhieu`, `kcxl_trangThai`, `kcxl_nguoiTao`, `kcxl_ngayTao`, `kcxl_nguoiThucHien`, `kcxl_ghi_chu` 
            FROM `kho-cho-xu-li` 
            WHERE `kcxl_check` = 1
            AND NOT `kcxl_trangThai` = 4 
            AND NOT `kcxl_trangThai` = 6 
            AND NOT `kcxl_trangThai` = 7 
            AND NOT `kcxl_trangThai` = 12 
            AND (`kcxl_id` = '$kcxl_id' OR `kcxl_id` LIKE '%$input_val%') AND `kcxl_id_ct` = $id_cty AND (`kcxl_nguoiTao` = $user_id OR `kcxl_nguoiThucHien` = $user_id) ";

            $total_sql =  "SELECT COUNT(`kcxl_check`) AS total FROM `kho-cho-xu-li` 
            WHERE `kcxl_check` = 1
            AND NOT `kcxl_trangThai` = 4 
            AND NOT `kcxl_trangThai` = 6 
            AND NOT `kcxl_trangThai` = 7 
            AND NOT `kcxl_trangThai` = 12 AND (`kcxl_id` = '$kcxl_id' OR `kcxl_id` LIKE '%$input_val%') AND `kcxl_id_ct` = $id_cty AND (`kcxl_nguoiTao` = $user_id OR `kcxl_nguoiThucHien` = $user_id) ";
        }

    }else{
        if($_COOKIE['role'] == 1){
            $kho_cho_xu_li = "SELECT `kcxl_id`, `kcxl_soPhieu`, `kcxl_trangThai`, `kcxl_nguoiTao`, `kcxl_ngayTao`, `kcxl_nguoiThucHien`, `kcxl_ghi_chu` 
            FROM `kho-cho-xu-li` 
            WHERE `kcxl_check` = 1
            AND NOT `kcxl_trangThai` = 4 
            AND NOT `kcxl_trangThai` = 6 
            AND NOT `kcxl_trangThai` = 7 
            AND NOT `kcxl_trangThai` = 12 
            AND `kcxl_id_ct` = $id_cty ";

            $total_sql =  "SELECT COUNT(`kcxl_check`) AS total FROM `kho-cho-xu-li` 
            WHERE `kcxl_check` = 1
            AND NOT `kcxl_trangThai` = 4 
            AND NOT `kcxl_trangThai` = 6 
            AND NOT `kcxl_trangThai` = 7 
            AND NOT `kcxl_trangThai` = 12 AND (`kcxl_id` = '$kcxl_id' OR `kcxl_id` LIKE '%$input_val%') AND `kcxl_id_ct` = $id_cty "; 
        }
        if($_COOKIE['role'] == 2){  
            $kho_cho_xu_li = "SELECT `kcxl_id`, `kcxl_soPhieu`, `kcxl_trangThai`, `kcxl_nguoiTao`, `kcxl_ngayTao`, `kcxl_nguoiThucHien`, `kcxl_ghi_chu` 
            FROM `kho-cho-xu-li` 
            WHERE `kcxl_check` = 1
            AND NOT `kcxl_trangThai` = 4 
            AND NOT `kcxl_trangThai` = 6 
            AND NOT `kcxl_trangThai` = 7 
            AND NOT `kcxl_trangThai` = 12 
            AND `kcxl_id_ct` = $id_cty AND (`kcxl_nguoiTao` = $user_id OR `kcxl_nguoiThucHien` = $user_id) ";

            $total_sql =  "SELECT COUNT(`kcxl_check`) AS total FROM `kho-cho-xu-li` 
            WHERE `kcxl_check` = 1
            AND NOT `kcxl_trangThai` = 4 
            AND NOT `kcxl_trangThai` = 6 
            AND NOT `kcxl_trangThai` = 7 
            AND NOT `kcxl_trangThai` = 12 AND (`kcxl_id` = '$kcxl_id' OR `kcxl_id` LIKE '%$input_val%') AND `kcxl_id_ct` = $id_cty AND (`kcxl_nguoiTao` = $user_id OR `kcxl_nguoiThucHien` = $user_id) "; 
        }
    }

    if($type == ""){
        $tp = "";
    }else if($type == "PNK") {
        $tp = "AND `kcxl_soPhieu` = '$type' ";
    }else if($type == "PXK") {
        $tp = "AND `kcxl_soPhieu` = '$type' ";
    }else if($type == "ĐCK") {
        $tp = "AND `kcxl_soPhieu` = '$type' ";
    }else if($type == "PKK") {
        $tp = "AND `kcxl_soPhieu` = '$type' ";
    }

    $limited = "LIMIT $start,$curr";
    $order = "ORDER BY `kho-cho-xu-li`.`kcxl_id` DESC ";

    $kho_cho_xu_li .= $tp;
    $kho_cho_xu_li .= $order;
    $kho_cho_xu_li .= $limited;

    $kho_cho_xu_li = new db_query($kho_cho_xu_li);

    $total_sql .= $tp;


    $total_sql1 = new db_query($total_sql);
    $total = mysql_fetch_assoc($total_sql1->result)['total'];

?>
 <div class="detail_wh">
    <div class="position_r d_flex align_c">
        <div class="table_vt_scr" onscroll="table_scroll(this)">
            <div class="table_vt">
            <table style="min-width: 1488px;">
                <tr class="tit_tbl color_white font_s16 line_h19 font_w500 back_blue">
                    <th>STT<span class="span_tbody"></span></th>
                    <th>Số phiếu<span class="span_tbody"></span></th>
                    <th>Loại phiếu<span class="span_tbody"></span></th>
                    <th>Trạng thái<span class="span_tbody"></span></th>
                    <th>Người tạo<span class="span_tbody"></span></th>
                    <th>Ngày tạo<span class="span_tbody"></span></th>
                    <th>Người thực hiện<span class="span_tbody"></span></th>
                    <th class="th_desc">Ghi chú</th>
                </tr>
                <?php $stt = 1; while($row = mysql_fetch_assoc($kho_cho_xu_li->result)) : ?>
                    <tr class="color_grey font_s14 line_h17 font_w400" data-id="<?= $row['kcxl_id']; ?>">
                        <td><?= $stt++ ?></td>
                        <td><a class="color_blue font_w500" href="<?php
                                $link_phieu = $row['kcxl_soPhieu'];
                                switch($link_phieu) {
                                    case 'PNK':
                                        $link_phieu = 'nhap-kho-chi-tiet-'.$row['kcxl_id'].'.html';
                                        echo $link_phieu;
                                        break;
                                    case 'PXK':
                                        $link_phieu = 'xuat-kho-chi-tiet-'.$row['kcxl_id'].'.html';
                                        echo $link_phieu;
                                        break;
                                    case 'ĐCK':
                                        $link_phieu = 'dieu-chuyen-kho-chi-tiet-'.$row['kcxl_id'].'.html';
                                        echo $link_phieu;
                                        break;
                                    case 'PKK':
                                        $link_phieu = 'kiem-ke-kho-chi-tiet-'.$row['kcxl_id'].'.html';
                                        echo $link_phieu;
                                        break;
                                    default:
                                        $link_phieu = 'nghiep-vu-kho-cho-xu-ly-detail-rq.html?id='.$row['kcxl_id'];
                                        echo $link_phieu;
                                        break;
                                }
                            ?> ">
                        <?php
                            $ma_phieu = $row['kcxl_soPhieu'];
                            switch($ma_phieu) {
                                case 'PNK':
                                    $ma_phieu = 'PNK - '.$row['kcxl_id'];
                                    echo $ma_phieu;
                                    break;
                                case 'PXK':
                                    $ma_phieu = 'PXK - '.$row['kcxl_id'];
                                    echo $ma_phieu;
                                    break;
                                case 'ĐCK':
                                    $ma_phieu = 'ĐCK - '.$row['kcxl_id'];
                                    echo $ma_phieu;
                                    break;
                                case 'PKK':
                                    $ma_phieu = 'PKK - '.$row['kcxl_id'];
                                    echo $ma_phieu;
                                    break;
                                default:
                                    break;
                            }
                        ?>
                    </a></td>
                        <td><?= loai_phieu($row['kcxl_soPhieu']); ?></td>
                        <td class="color_blue"><?= trang_thai($row['kcxl_trangThai']);?></td>
                        <td>
                        <?php 
                            $nguoi_tao = $row['kcxl_nguoiTao'];
                            $user_id = $user[$nguoi_tao];
                            if($nguoi_tao != 0){
                                $ten_nguoi_tao = $user_id['ep_name'];
                                $anh_nguoi_tao = $user_id['ep_image'];
                                if($anh_nguoi_tao == ""){
                                    $anh = '../images/ava_ad.png';
                                }else{
                                    $anh = 'https://chamcong.24hpay.vn/upload/employee/'.$anh_nguoi_tao;
                                }
                            }else{
                                $ten_nguoi_tao = $tt_user['com_name'];
                                $anh = 'https://chamcong.24hpay.vn/upload/company/logo/' . $tt_user['com_logo'];
                            }
                        ?>
                            <div class="d_flex flex_start align_c">
                            <img src="<?= $anh; ?>" alt="" class="<?= ($anh == 'https://chamcong.24hpay.vn/upload/company/logo/') ? 'display_none' : '' ?>">
                            <p><?= $ten_nguoi_tao; ?></p>
                            </div>
                        </td>
                        <td><?php echo date('d/m/Y',strtotime($row['kcxl_ngayTao'])); ?></td>
                        <td>
                        <?php 
                            $arr_ntt = explode(',',$row['kcxl_nguoiThucHien']);
                            for($k=0; $k < count(explode(',',$row['kcxl_nguoiThucHien'])); $k++){
                                $nguoi_thuc_hien = $arr_ntt[$k];
                                $user_id_ntt = $user[$nguoi_thuc_hien];
                                $ten_nguoi_thuc_hien = $user_id_ntt['ep_name'];
                                $anh_nguoi_thuc_hien = $user_id_ntt['ep_image'];
                                if($anh_nguoi_thuc_hien == ""){
                                    $anh_ntt = '../images/ava_ad.png';
                                }else{
                                    $anh_ntt = 'https://chamcong.24hpay.vn/upload/employee/'.$anh_nguoi_thuc_hien;
                                }
                        ?>
                            <div class="nguoi_thuc_hien d_flex flex_start align_c mb_15">
                            <img src="<?= $anh_ntt; ?>" alt="">
                            <p><?= (isset($ten_nguoi_thuc_hien)) ? $ten_nguoi_thuc_hien : $row['kcxl_nguoiThucHien'] ?></p>
                            </div>
                        <? }?>
                        </td>
                        <td style="text-align: left;">
                            <div class="d_flex space_b">
                                <p class="td_desc"><?php echo $row['kcxl_ghi_chu']; ?></p> 
                                <p class="xem_them color_blue font_s14 line_h16 font_w400 cursor_p display_none">Xem thêm</p>
                            </div>
                        </td>
                    </tr>
                <?php 
                endwhile; ?>
            </table>
            </div>
        </div>
        <div class="pre_q d_flex align_c flex_center position_a display_none" onclick="pre_q(this)">
            <span class="pre_arrow"></span>
        </div>
        <div class="next_q d_flex align_c flex_center position_a" onclick="next_q(this)">
            <span class="next_arrow"></span>
        </div>                            
    </div>
    <div class="w_navigation d_flex space_b align_c">
        <div class="l_nav d_flex align_c">
            <p class="color_grey font_s14 line_h17 font_w400">Hiển thị:</p>
            <select name="display_tb" id="" class="show_tr_tb color_grey font_s14 line_h17 font_w400" onchange="display(this)">
                <option value="10" <?= ($curr == 10) ? "selected" : ""?>>10</option>
                <option value="20" <?= ($curr == 20) ? "selected" : ""?>>20</option>
                <option value="30" <?= ($curr == 30) ? "selected" : ""?>>30</option>
                <option value="40" <?= ($curr == 40) ? "selected" : ""?>>40</option>
            </select>
        </div>
        <div class="r_nav">
            <ul class="d_flex font_s13 line_h15 font_wN">
            <?php
                echo generatePageBar3('',$page,$curr,$total,$url,'&','','paging_detail','preview','<','next','>','','<<<','','>>>');
            ?>
            </ul>
        </div>
    </div>
</div>
<script type="text/javascript" src="../js/ghi_chu.js"></script>
<script>
    $('.show_tr_tb').select2({
        minimumResultsForSearch: -1
    });
</script>