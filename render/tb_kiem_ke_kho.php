<?php
include('config.php');

if (isset($_COOKIE['acc_token']) && isset($_COOKIE['rf_token']) && isset($_COOKIE['role']) && $_COOKIE['role'] == 2) {
    $curl = curl_init();
    $token = $_COOKIE['acc_token'];
    curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/list_all_my_partner.php?get_all=true');
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));
    $response = curl_exec($curl);
    curl_close($curl);

    $data_list = json_decode($response, true);
    $data_list_nv = $data_list['data']['items'];
} else {
    $curl = curl_init();
    $token = $_COOKIE['acc_token'];
    curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/list_all_employee_of_company.php');
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));
    $response = curl_exec($curl);
    curl_close($curl);

    $data_list = json_decode($response, true);
    $data_list_nv = $data_list['data']['items'];
}
$count = count($data_list_nv);

$user = [];
for ($i = 0; $i < count($data_list_nv); $i++) {
    $nv = $data_list_nv[$i];
    $user[$nv["ep_id"]] = $nv;
}

if (isset($_COOKIE['user']) && $_COOKIE['user'] != "" && $_COOKIE['role'] == 2) {
    $token = $_COOKIE['acc_token'];
    $curl = curl_init();
    $data = array();
    curl_setopt($curl, CURLOPT_POST, 1);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
    curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/user_info_employee.php');
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));
    $response = curl_exec($curl);
    curl_close($curl);
    $data_tt = json_decode($response, true);
    $tt_user = $data_tt['data']['user_info_result'];
    $user_id = $_SESSION['ep_id'];
} else if (isset($_COOKIE['user']) && $_COOKIE['user'] != "" && $_COOKIE['role'] == 1) {
    $token = $_COOKIE['acc_token'];
    $curl = curl_init();
    $data = array();
    curl_setopt($curl, CURLOPT_POST, 1);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
    curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/user_info_company.php');
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));
    $response = curl_exec($curl);
    curl_close($curl);
    $data_tt = json_decode($response, true);
    $tt_user = $data_tt['data']['user_info_result'];
    $user_id = $_SESSION['com_id'];
}

$id_cty = $tt_user['com_id'];

$all_kho = getValue('all_k', 'int', 'POST', '');
$trang_thai = getValue('th', 'int', 'POST', '');

// ngày tạo start       
if ($_POST['ngay_tao_start'] != "") {
    $ngay_tao_start = date('Y-m-d', strtotime($_POST['ngay_tao_start']));
} else {
    $ngay_tao_start = "";
}

// ngày tạo end
if ($_POST['ngay_tao_end'] != "") {
    $ngay_tao_end = date('Y-m-d', strtotime($_POST['ngay_tao_end']));
} else {
    $ngay_tao_end = "";
}

// ngày thực hiện start
if ($_POST['ngay_th_start'] != "") {
    $ngay_th_start = date('Y-m-d', strtotime($_POST['ngay_th_start']));
} else {
    $ngay_th_start = "";
}

// ngày thực hiện end
if ($_POST['ngay_th_end'] != "") {
    $ngay_th_end = date('Y-m-d', strtotime($_POST['ngay_th_end']));
} else {
    $ngay_th_end = "";
}

// ngày yêu cầu hoàn thành start
if ($_POST['ngay_ycht_start'] != "") {
    $ngay_ycht_start = date('Y-m-d', strtotime($_POST['ngay_ycht_start']));
} else {
    $ngay_ycht_start = "";
}

// ngày yêu cầu hoàn thành end
if ($_POST['ngay_ycht_end'] != "") {
    $ngay_ycht_end = date('Y-m-d', strtotime($_POST['ngay_ycht_end']));
} else {
    $ngay_ycht_end = "";
}

// ngày hoàn thành start
if ($_POST['ngay_ht_start'] != "") {
    $ngay_ht_start = date('Y-m-d', strtotime($_POST['ngay_ht_start']));
} else {
    $ngay_ht_start = "";
}

// ngày hoàn thành end
if ($_POST['ngay_ht_end'] != "") {
    $ngay_ht_end = date('Y-m-d', strtotime($_POST['ngay_ht_end']));
} else {
    $ngay_ht_end = "";
}

$page = getValue('page', 'int', 'POST', 1);
$sl = getValue('curr','int','POST', '');
$curr = $sl;
$start = ($page - 1) * $curr;
$start = abs($start);

$input_val = $_POST['input_val'];

$input_val = trim($input_val);

$so_phieu = explode('-', $input_val)[0];
$id_so_phieu = explode('-', $input_val)[1];

$arr = [];
$length_id_phieu = strlen($id_so_phieu);
for ($push = 0; $push < $length_id_phieu; $push++) {
    $ki_tu = substr($id_so_phieu, $push, 1);
    array_push($arr, $ki_tu);
}

if ($input_val != "") {
    if ($_COOKIE['role'] == 1) {
        $kiemke = "SELECT `kcxl_id`, `kcxl_soPhieu`,`kcxl_trangThai`,`kcxl_khoThucHienKiemKe`,`kcxl_ngayThucHienKiemKe`,`kcxl_ngayYeuCauHoanThanh`,`kcxl_ngayHoanThanh`,`kcxl_nguoiTao`, `kcxl_ngayTao`,`kcxl_ghi_chu`,`kho_name` FROM `kho-cho-xu-li`
        LEFT JOIN `kho` ON `kcxl_khoThucHienKiemKe` = `kho_id`
        WHERE `kcxl_soPhieu` = 'PKK' AND `kcxl_check`= 1 AND (`kcxl_id` = '$kcxl_id' OR `kcxl_id` LIKE '%$input_val%') AND `kcxl_id_ct` = $id_cty ";

        $total_sql =  "SELECT COUNT(`kcxl_check`) AS total FROM `kho-cho-xu-li` 
        LEFT JOIN `kho` ON `kcxl_khoThucHienKiemKe` = `kho_id`
        WHERE `kcxl_soPhieu` = 'PKK' AND `kcxl_check`= 1 AND (`kcxl_id` = '$kcxl_id' OR `kcxl_id` LIKE '%$input_val%') AND `kcxl_id_ct` = $id_cty ";
    }
    if ($_COOKIE['role'] == 2) {
        $kiemke = "SELECT `kcxl_id`, `kcxl_soPhieu`,`kcxl_trangThai`,`kcxl_khoThucHienKiemKe`,`kcxl_ngayThucHienKiemKe`,`kcxl_ngayYeuCauHoanThanh`,`kcxl_ngayHoanThanh`,`kcxl_nguoiTao`, `kcxl_ngayTao`,`kcxl_ghi_chu`,`kho_name` FROM `kho-cho-xu-li`
            LEFT JOIN `kho` ON `kcxl_khoThucHienKiemKe` = `kho_id`
            WHERE `kcxl_soPhieu` = 'PKK' AND `kcxl_check`= 1 AND (`kcxl_id` = '$kcxl_id' OR `kcxl_id` LIKE '%$input_val%') AND `kcxl_id_ct` = $id_cty AND  `kcxl_nguoiTao`= $user_id AND NOT `kcxl_nguoiTao`= 0 ";

        $total_sql =  "SELECT COUNT(`kcxl_check`) AS total FROM `kho-cho-xu-li` 
            LEFT JOIN `kho` ON `kcxl_khoThucHienKiemKe` = `kho_id`
            WHERE `kcxl_soPhieu` = 'PKK' AND `kcxl_check`= 1 AND (`kcxl_id` = '$kcxl_id' OR `kcxl_id` LIKE '%$input_val%') AND `kcxl_id_ct` = $id_cty AND  `kcxl_nguoiTao`= $user_id AND NOT `kcxl_nguoiTao`= 0 ";
    }
} else {
    if ($_COOKIE['role'] == 1) {
        $kiemke = "SELECT `kcxl_id`, `kcxl_soPhieu`,`kcxl_trangThai`,`kcxl_khoThucHienKiemKe`,`kcxl_ngayThucHienKiemKe`,`kcxl_ngayYeuCauHoanThanh`,`kcxl_ngayHoanThanh`,`kcxl_nguoiTao`, `kcxl_ngayTao`,`kcxl_ghi_chu`,`kho_name` FROM `kho-cho-xu-li`
        LEFT JOIN `kho` ON `kcxl_khoThucHienKiemKe` = `kho_id`
        WHERE `kcxl_soPhieu` = 'PKK' AND `kcxl_check`= 1 AND `kcxl_id_ct` = $id_cty ";

        $total_sql =  "SELECT COUNT(`kcxl_check`) AS total FROM `kho-cho-xu-li` 
        LEFT JOIN `kho` ON `kcxl_khoThucHienKiemKe` = `kho_id`
        WHERE `kcxl_soPhieu` = 'PKK' AND `kcxl_check`= 1 AND `kcxl_id_ct` = $id_cty ";
    }
    if ($_COOKIE['role'] == 2) {
        $kiemke = "SELECT `kcxl_id`, `kcxl_soPhieu`,`kcxl_trangThai`,`kcxl_khoThucHienKiemKe`,`kcxl_ngayThucHienKiemKe`,`kcxl_ngayYeuCauHoanThanh`,`kcxl_ngayHoanThanh`,`kcxl_nguoiTao`, `kcxl_ngayTao`,`kcxl_ghi_chu`,`kho_name` FROM `kho-cho-xu-li`
            LEFT JOIN `kho` ON `kcxl_khoThucHienKiemKe` = `kho_id`
            WHERE `kcxl_soPhieu` = 'PKK' AND `kcxl_check`= 1 AND `kcxl_id_ct` = $id_cty AND  `kcxl_nguoiTao`= $user_id AND NOT `kcxl_nguoiTao`= 0 ";

        $total_sql =  "SELECT COUNT(`kcxl_check`) AS total FROM `kho-cho-xu-li` 
            LEFT JOIN `kho` ON `kcxl_khoThucHienKiemKe` = `kho_id`
            WHERE `kcxl_soPhieu` = 'PKK' AND `kcxl_check`= 1 AND `kcxl_id_ct` = $id_cty AND  `kcxl_nguoiTao`= $user_id AND NOT `kcxl_nguoiTao`= 0 ";
    }
}

if ($all_kho == "" && $trang_thai == "" && $ngay_tao_start == "" && $ngay_tao_end == "" && $ngay_th_start == "" && $ngay_th_end == "" && $ngay_ycht_start == "" && $ngay_ycht_end == "" && $ngay_ht_start == "" && $ngay_ht_start == "" && $input_val == "" && $curr == 10) {
    $url = 'kiem-ke-kho.html?dis='.$curr;
} else {
    $url = 'kiem-ke-kho.html?all_k=' . $all_kho . '&th=' . $trang_thai . '&ngts=' . $ngay_tao_start . '&ngth=' . $ngay_tao_end . '&ngnh=' . $ngay_th_start . '&ngne=' . $ngay_ycht_start . '&ngns=' . $ngay_th_start . '&ngne=' . $ngay_th_end . "&input=" . $input_val."&dis=".$curr;
}

if ($trang_thai == 0) {
    $tf = "";
} else {
    $tf = "AND `kcxl_trangThai` = '$trang_thai' ";
}

if ($all_kho == 0) {
    $kho = "";
} else {
    $kho = "AND `kcxl_khoThucHienKiemKe` = '$all_kho' ";
}

if ($ngay_tao_start != "" && $ngay_tao_end != "") {
    $date_cr = "AND `kcxl_ngayTao` <= '$ngay_tao_end' AND  `kcxl_ngayTao` >= '$ngay_tao_start'";
} elseif ($ngay_tao_start != "" && $ngay_tao_end == "") {
    $date_cr = "AND `kcxl_ngayTao` = '$ngay_tao_start' ";
} elseif ($ngay_tao_start == "" && $ngay_tao_end != "") {
    $date_cr = "AND `kcxl_ngayTao` = '$ngay_tao_end' ";
}

if ($ngay_th_start != "" && $ngay_th_end != "") {
    $date_th = "AND `kcxl_ngayThucHienKiemKe` <= '$ngay_th_end' AND  `kcxl_ngayThucHienKiemKe` >= '$ngay_th_start'";
} elseif ($ngay_th_start != "" && $ngay_th_end == "") {
    $date_th = "AND `kcxl_ngayThucHienKiemKe` = '$ngay_th_start' ";
} elseif ($ngay_th_start == "" && $ngay_th_end != "") {
    $date_th = "AND `kcxl_ngayThucHienKiemKe` = '$ngay_th_end' ";
}

if ($ngay_ycht_start != "" && $ngay_ycht_end != "") {
    $date_ycht = "AND `kcxl_ngayYeuCauHoanThanh` <= '$ngay_ycht_end' AND  `kcxl_ngayYeuCauHoanThanh` >= '$ngay_ycht_start'";
} elseif ($ngay_ycht_start != "" && $ngay_ycht_end == "") {
    $date_ycht = "AND `kcxl_ngayYeuCauHoanThanh` = '$ngay_ycht_start' ";
} elseif ($ngay_ycht_start == "" && $ngay_ycht_end != "") {
    $date_ycht = "AND `kcxl_ngayYeuCauHoanThanh` = '$ngay_ycht_end' ";
}

if ($ngay_ht_start != "" && $ngay_ht_end != "") {
    $date_ht = "AND `kcxl_ngayHoanThanh` <= '$ngay_ht_end' AND  `kcxl_ngayHoanThanh` >= '$ngay_ht_start'";
} elseif ($ngay_ht_start != "" && $ngay_ht_end == "") {
    $date_ht = "AND `kcxl_ngayHoanThanh` = '$ngay_ht_start' ";
} elseif ($ngay_ht_start == "" && $ngay_ht_end != "") {
    $date_ht = "AND `kcxl_ngayHoanThanh` = '$ngay_ht_end' ";
}

$limited = "ORDER BY `kcxl_id` DESC LIMIT $start,$curr";

$kiemke .= $kho;
$kiemke .= $tf;
$kiemke .= $date_cr;
$kiemke .= $date_th;
$kiemke .= $date_ycht;
$kiemke .= $date_ht;
$kiemke .= $limited;

$kiemke = new db_query($kiemke);

$total_sql .= $kho;
$total_sql .= $tf;
$total_sql .= $date_cr;
$total_sql .= $date_th;
$total_sql .= $date_ycht;
$total_sql .= $date_ht;

$total_sql1 = new db_query($total_sql);
$total = mysql_fetch_assoc($total_sql1->result)['total'];

?>
<div class="position_r d_flex align_c">
    <div class="table_vt table_vt_scr" onscroll="table_scroll(this)">
        <table>
            <tr class="tit_tbl color_white font_s16 line_h19 font_w500 back_blue">
                <td style="width: 2.6%;" class="color_white font_s16 line_h19 font_w500">STT
                    <span class="span_tbody"></span>
                </td>
                <td class="color_white font_s16 line_h19 font_w500">Số phiếu
                    <span class="span_tbody"></span>
                </td>
                <td class="color_white font_s16 line_h19 font_w500" style="width:10%;">Trạng thái
                    <span class="span_tbody"></span>
                </td>
                <td class="color_white font_s16 line_h19 font_w500">Kho kiểm kê
                    <span class="span_tbody"></span>
                </td>
                <td class="color_white font_s16 line_h19 font_w500">Ngày thực hiện kiểm kê
                    <span class="span_tbody"></span>
                </td>
                <td class="color_white font_s16 line_h19 font_w500">Ngày yêu cầu hoàn thành
                    <span class="span_tbody"></span>
                </td>
                <td class="color_white font_s16 line_h19 font_w500">Ngày hoàn thành
                    <span class="span_tbody"></span>
                </td>
                <td class="color_white font_s16 line_h19 font_w500">Người tạo
                    <span class="span_tbody"></span>
                </td>
                <td class="color_white font_s16 line_h19 font_w500">Ngày tạo
                    <span class="span_tbody"></span>
                </td>
                <td class="color_white font_s16 line_h19 font_w500">Ghi chú
                    <?php if (in_array(3, $ro_kk_kho) || in_array(4, $ro_kk_kho)) { ?>
                        <span class="span_tbody"></span>
                    <?php } ?>
                </td>
                <?php if (in_array(3, $ro_kk_kho) || in_array(4, $ro_kk_kho)) { ?>
                    <td style="width: 5.6%;" class="color_white font_s16 line_h19 font_w500">Chức năng
                    </td>
                <?php } ?>
            </tr>
            <? $stt = 1;
            while ($row = mysql_fetch_assoc($kiemke->result)) { ?>
                <tr class="color_grey font_s14 line_h17 font_w400" data-id="<?= $row['kcxl_id']; ?>">
                    <td><?= $stt++ ?></td>
                    <td><a href="kiem-ke-kho-chi-tiet-<?= $row['kcxl_id']; ?>.html" class="color_blue font_w500">PKK - <?= $row['kcxl_id']; ?></a></td>
                    <td class="<?= trang_thai_color($row['kcxl_trangThai']); ?>"><?= trang_thai($row['kcxl_trangThai']); ?></td>
                    <td style="text-align: left;"><?= $row['kho_name'] ?></td>
                    <td><?php echo date('d/m/Y', strtotime($row['kcxl_ngayThucHienKiemKe'])); ?></td>
                    <td><?php echo date('d/m/Y', strtotime($row['kcxl_ngayYeuCauHoanThanh'])); ?></td>
                    <td><?php echo date('d/m/Y', strtotime($row['kcxl_ngayHoanThanh'])); ?></td>
                    <td style="text-align: left;">
                        <?php
                        $nguoi_tao = $row['kcxl_nguoiTao'];
                        $user1 = $user[$nguoi_tao];
                        if ($nguoi_tao != 0) {
                            $ten_nguoi_tao = $user1['ep_name'];
                            $anh_nguoi_tao = $user1['ep_image'];
                            if ($anh_nguoi_tao == "") {
                                $anh = '../images/ava_ad.png';
                            } else {
                                $anh = 'https://chamcong.24hpay.vn/upload/employee/' . $anh_nguoi_tao;
                            }
                        } else {
                            $ten_nguoi_tao = $tt_user['com_name'];
                            $anh = 'https://chamcong.24hpay.vn/upload/company/logo/' . $tt_user['com_logo'];
                        }
                        ?>
                        <div class="d_flex flex_start align_c">
                            <img src="<?= $anh; ?>" alt="">
                            <p><?= $ten_nguoi_tao; ?></p>
                        </div>
                    </td>
                    <td><?php echo date('d/m/Y', strtotime($row['kcxl_ngayTao'])); ?></td>
                    <td style="text-align: left;"><?= $row['kcxl_ghi_chu'] ?></td>
                    <?php if (in_array(3, $ro_kk_kho) || in_array(4, $ro_kk_kho)) { ?>
                        <td>
                            <p class="d_flex flex_center align_c font_s14 line_h17">
                                <?php if (in_array(3, $ro_kk_kho) && $_SESSION['quyen'] == '2' && ($row['kcxl_trangThai'] == 8 || $row['kcxl_trangThai'] == 9 || $row['kcxl_trangThai'] == 10)) { ?>
                                    <img class="img_fix_blue" src="images/pen_blu.png" alt="">
                                    <a href="/kiem-ke-kho-chinh-sua-<?= $row['kcxl_id']; ?>.html" class="color_blue">Sửa</a>
                                <?php } elseif (in_array(3, $ro_kk_kho) && $_SESSION['quyen'] == '1') { ?>
                                    <img class="img_fix_blue" src="images/pen_blu.png" alt="">
                                    <a href="/kiem-ke-kho-chinh-sua-<?= $row['kcxl_id']; ?>.html" class="color_blue">Sửa</a>
                                <?php } ?>
                                <?php if (in_array(3, $ro_kk_kho) && in_array(4, $ro_kk_kho)) { ?>
                                    <span class="color_blue margin_lr5 <?= (($row['kcxl_trangThai'] == 11 || $row['kcxl_trangThai'] == 12) && $_SESSION['quyen'] == '2') ? 'display_none' : '' ?>">|</span>
                                <?php } ?>
                                <?php if (in_array(4, $ro_kk_kho)) { ?>
                                    <img class="img_delete_red" src="images/delete_r.png" alt="">
                                    <span><a class="color_red cursor_p">Xóa</a></span>
                                <?php } ?>
                            </p>
                        </td>
                    <?php } ?>
                </tr>
            <? } ?>
        </table>
    </div>
    <div class="pre_q d_flex align_c flex_center position_a display_none" onclick="pre_q(this)">
        <span class="pre_arrow"></span>
    </div>
    <div class="next_q d_flex align_c flex_center position_a" onclick="next_q(this)">
        <span class="next_arrow"></span>
    </div>
</div>
<div class="w_navigation d_flex space_b align_c">
    <div class="l_nav d_flex align_c">
        <p class="color_grey font_s14 line_h17 font_w400">Hiển thị:</p>
        <select name="" id="" class="show_tr_tb color_grey font_s14 line_h17 font_w400" onchange="display(this)">
            <option value="10" <?= ($curr == 10) ? "selected" : ""?>>10</option>
            <option value="20" <?= ($curr == 20) ? "selected" : ""?>>20</option>
            <option value="30" <?= ($curr == 30) ? "selected" : ""?>>30</option>
            <option value="40" <?= ($curr == 40) ? "selected" : ""?>>40</option>
        </select>
    </div>
    <div class="r_nav">
        <ul class="d_flex font_s13 line_h15 font_wN">
            <?php
                echo generatePageBar3('', $page, $curr, $total, $url, '&', '', 'paging_detail', 'preview', '<', 'next', '>', '', '<<<', '', '>>>');
            ?>
        </ul>
    </div>
</div>
<script>
    $('.show_tr_tb').select2({
        minimumResultsForSearch: -1
    });
</script>
