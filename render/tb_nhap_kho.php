<?php
    include("config.php");

    if(isset($_COOKIE['acc_token']) && isset($_COOKIE['rf_token']) && isset($_COOKIE['role']) && $_COOKIE['role'] == 2){
        $curl = curl_init();
        $token = $_COOKIE['acc_token'];
        curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/list_all_my_partner.php?get_all=true');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer '.$token));
        $response = curl_exec($curl);
        curl_close($curl);

        $data_list = json_decode($response,true);
        $data_list_nv =$data_list['data']['items'];
    }else{
        $curl = curl_init();
        $token = $_COOKIE['acc_token'];
        curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/list_all_employee_of_company.php');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer '.$token));
        $response = curl_exec($curl);
        curl_close($curl);

        $data_list = json_decode($response,true);
        $data_list_nv =$data_list['data']['items'];

    }
    $count = count($data_list_nv);

    $user = [];
    for ($i = 0; $i < count($data_list_nv); $i++){
        $nv = $data_list_nv[$i];
        $user[$nv["ep_id"]] = $nv;
    }

    if (isset($_COOKIE['user']) && $_COOKIE['user'] != "" && $_COOKIE['role'] == 2) {
		$token = $_COOKIE['acc_token'];
		$curl = curl_init();
		$data = array();
		curl_setopt($curl, CURLOPT_POST, 1);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
		curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/user_info_employee.php');
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));
		$response = curl_exec($curl);
		curl_close($curl);
		$data_tt = json_decode($response, true);
		$tt_user = $data_tt['data']['user_info_result'];
        $user_id = $_SESSION['ep_id'];

	}else if (isset($_COOKIE['user']) && $_COOKIE['user'] != "" && $_COOKIE['role'] == 1) {
        $token = $_COOKIE['acc_token'];
        $curl = curl_init();
        $data = array();
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/user_info_company.php');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));
        $response = curl_exec($curl);
        curl_close($curl);
        $data_tt = json_decode($response, true);
        $tt_user = $data_tt['data']['user_info_result'];
        $user_id = $_SESSION['com_id'];
    }
    
    $id_cty = $tt_user['com_id'];

    $all_kho = getValue('all_k','int','POST','');
    $trang_thai = getValue('th','int','POST','');
    $hinh_thuc = $_POST['ht'];

    // ngày tạo start
    if($_POST['ngay_tao_start'] != ""){
        $ngay_tao_start = date('Y-m-d',strtotime($_POST['ngay_tao_start'])); 
    }else{
        $ngay_tao_start = "";
    }
    
    // ngày tạo end
    if($_POST['ngay_tao_end'] != ""){
        $ngay_tao_end = date('Y-m-d',strtotime($_POST['ngay_tao_end']));
    }else{
        $ngay_tao_end = "";
    }

    // ngày nhập start
    if($_POST['ngay_nh_start'] != ""){
        $ngay_nh_start = date('Y-m-d',strtotime($_POST['ngay_nh_start'])); 
    }else{
        $ngay_nh_start = "";
    }

    // ngày nhập end
    if($_POST['ngay_nh_end'] != ""){
        $ngay_nh_end = date('Y-m-d',strtotime($_POST['ngay_nh_end']));
    }else{
        $ngay_nh_end = "";
    }

    $page = getValue('page','int','POST', 1);
    $sl = getValue('curr','int','POST', '');
    $curr = $sl;
    $start = ($page - 1 )*$curr;
    $start = abs($start);

    $input_val = $_POST['input_val'];

    $input_val = trim($input_val);

    $so_phieu = explode('-',$input_val)[0];
    $id_so_phieu = explode('-',$input_val)[1];

    $arr = [];
    $length_id_phieu = strlen($id_so_phieu);
    for($push = 0; $push<$length_id_phieu; $push++){
        $ki_tu = substr($id_so_phieu,$push,1);
        array_push($arr,$ki_tu);
    }

    $count_arr = count($arr);
    $kcxl_id = '';

    if($arr[0] > 0){
        $kcxl_id = implode('',$arr);
    }
    elseif($arr[0] == 0){
        for($j = 1; $j < $count_arr; $j++){
            if($arr[$j] > 0){
                $kcxl_id = substr(implode('',$arr),$j ,$count_arr-$j);
                break;
            }
        }
    }

    if($input_val != ""){
        if($_COOKIE['role'] == 1){
            $main_nhapKho = "SELECT `kcxl_id`,`kcxl_hinhThuc`,`kcxl_soPhieu`,`kcxl_trangThai`,`kcxl_ngayNhapKho`,`kcxl_khoNhap`,`kcxl_nguoiTao`,`kcxl_ngayTao`,`kcxl_ghi_chu`,`kho_name` FROM `kho-cho-xu-li`
            LEFT JOIN `kho` ON `kcxl_khoNhap` = `kho_id`
            WHERE `kcxl_soPhieu`= 'PNK' AND `kcxl_check`=1 AND (`kcxl_id` = '$kcxl_id' OR `kcxl_id` LIKE '%$input_val%') AND `kcxl_id_ct` = $id_cty ";

            $total_sql =  "SELECT COUNT(`kcxl_check`) AS total FROM `kho-cho-xu-li` 
            LEFT JOIN `kho` ON `kcxl_khoNhap` = `kho_id`
            WHERE `kcxl_check` = 1 AND `kcxl_soPhieu` = 'PNK' AND (`kcxl_id` = '$kcxl_id' OR `kcxl_id` LIKE '%$input_val%') ";
        }
        if($_COOKIE['role'] == 2){
            $main_nhapKho = "SELECT `kcxl_id`,`kcxl_hinhThuc`,`kcxl_soPhieu`,`kcxl_trangThai`,`kcxl_ngayNhapKho`,`kcxl_khoNhap`,`kcxl_nguoiTao`,`kcxl_ngayTao`,`kcxl_ghi_chu`,`kho_name` FROM `kho-cho-xu-li`
            LEFT JOIN `kho` ON `kcxl_khoNhap` = `kho_id`
            WHERE `kcxl_soPhieu`= 'PNK' AND `kcxl_check`=1 AND (`kcxl_id` = '$kcxl_id' OR `kcxl_id` LIKE '%$input_val%') AND `kcxl_id_ct` = $id_cty AND NOT `kcxl_nguoiTao` = 0 AND (`kcxl_nguoiTao` = $user_id OR `kcxl_nguoiThucHien` = $user_id) ";

            $total_sql =  "SELECT COUNT(`kcxl_check`) AS total FROM `kho-cho-xu-li` 
            LEFT JOIN `kho` ON `kcxl_khoNhap` = `kho_id`
            WHERE `kcxl_check` = 1 AND `kcxl_soPhieu` = 'PNK' AND (`kcxl_id` = '$kcxl_id' OR `kcxl_id` LIKE '%$input_val%') AND NOT `kcxl_nguoiTao` = 0 AND (`kcxl_nguoiTao` = $user_id OR `kcxl_nguoiThucHien` = $user_id) ";
        }
    }else{
        if($_COOKIE['role'] == 1){
            $main_nhapKho = "SELECT `kcxl_id`,`kcxl_hinhThuc`,`kcxl_soPhieu`,`kcxl_trangThai`,`kcxl_ngayNhapKho`,`kcxl_khoNhap`,`kcxl_nguoiTao`,`kcxl_ngayTao`,`kcxl_ghi_chu`,`kho_name` FROM `kho-cho-xu-li`
            LEFT JOIN `kho` ON `kcxl_khoNhap` = `kho_id`
            WHERE `kcxl_soPhieu`= 'PNK' AND `kcxl_check`=1 AND `kcxl_id_ct` = $id_cty ";

            $total_sql =  "SELECT COUNT(`kcxl_check`) AS total FROM `kho-cho-xu-li` 
            LEFT JOIN `kho` ON `kcxl_khoNhap` = `kho_id`
            WHERE `kcxl_soPhieu`= 'PNK' AND `kcxl_check`=1 AND `kcxl_id_ct` = $id_cty ";
        }
        if($_COOKIE['role'] == 2){
            $main_nhapKho = "SELECT `kcxl_id`,`kcxl_hinhThuc`,`kcxl_soPhieu`,`kcxl_trangThai`,`kcxl_ngayNhapKho`,`kcxl_khoNhap`,`kcxl_nguoiTao`,`kcxl_ngayTao`,`kcxl_ghi_chu`,`kho_name` FROM `kho-cho-xu-li`
            LEFT JOIN `kho` ON `kcxl_khoNhap` = `kho_id`
            WHERE `kcxl_soPhieu`= 'PNK' AND `kcxl_check`=1 AND `kcxl_id_ct` = $id_cty AND NOT `kcxl_nguoiTao` = 0 AND (`kcxl_nguoiTao` = $user_id OR `kcxl_nguoiThucHien` = $user_id) ";

            $total_sql =  "SELECT COUNT(`kcxl_check`) AS total FROM `kho-cho-xu-li` 
            LEFT JOIN `kho` ON `kcxl_khoNhap` = `kho_id`
            WHERE `kcxl_soPhieu`= 'PNK' AND `kcxl_check`=1 AND `kcxl_id_ct` = $id_cty AND NOT `kcxl_nguoiTao` = 0 AND (`kcxl_nguoiTao` = $user_id OR `kcxl_nguoiThucHien` = $user_id) ";
        }
    }

    if ($all_kho == "" && $trang_thai == "" && $hinh_thuc == "" && $ngay_tao_start == "" && $ngay_tao_end == "" && $ngay_nh_start == "" && $ngay_nh_end == "" && $input_val == "" && $curr == 10) {
        $url = '/nhap-kho.html?dis='.$curr;
    }
    else{
        $url = '/nhap-kho.html?all_k='.$all_kho.'&th='.$trang_thai.'&ht='.$hinh_thuc.'&ngts='.$ngay_tao_start.'&ngte='.$ngay_tao_end.'&ngns='.$ngay_nh_start.'&ngne='.$ngay_nh_end."&input=".$input_val."&dis=".$curr;
    }

    $limited = "LIMIT $start,$curr";

    if($trang_thai == 0){
        $tf = "";
    }else{
        $tf = "AND `kcxl_trangThai` = '$trang_thai' ";
    }

    if($all_kho == 0){
        $kho = "";
    }else{
        $kho = "AND `kcxl_khoNhap` = '$all_kho' ";
    }

    if($hinh_thuc == ""){
        $ht = "";
    }else{
        $ht = "AND `kcxl_hinhThuc` = '$hinh_thuc' ";
    }

    if($ngay_tao_start != "" && $ngay_tao_end != ""){
        $date = "AND `kcxl_ngayTao` <= '$ngay_tao_end' AND `kcxl_ngayTao` >= '$ngay_tao_start' ";
    }elseif($ngay_tao_start != "" && $ngay_tao_end == ""){
        $date = "AND `kcxl_ngayTao` = '$ngay_tao_start' ";
    }elseif($ngay_tao_start == "" && $ngay_tao_end != ""){
        $date = "AND `kcxl_ngayTao` = '$ngay_tao_end' ";
    }

    if($ngay_nh_start != "" && $ngay_nh_end != ""){
        $date_nh = "AND `kcxl_ngayNhapKho` <= '$ngay_nh_end' AND `kcxl_ngayNhapKho` >= '$ngay_nh_start' ";
    }elseif($ngay_nh_start != "" && $ngay_nh_end == ""){
        $date_nh = "AND `kcxl_ngayNhapKho` = '$ngay_nh_start' ";
    }elseif($ngay_nh_start == "" && $ngay_nh_end != ""){
        $date_nh = "AND `kcxl_ngayNhapKho` = '$ngay_nh_end' ";
    }

    $order = "ORDER BY `kho-cho-xu-li`.`kcxl_id` DESC ";

    $main_nhapKho .= $kho;
    $main_nhapKho .= $tf;
    $main_nhapKho .= $ht;
    $main_nhapKho .= $date;
    $main_nhapKho .= $date_nh;
    $main_nhapKho .= $order;
    $main_nhapKho .= $limited;

    $main_nhapKho = new db_query($main_nhapKho);

    $total_sql .= $kho;
    $total_sql .= $tf;
    $total_sql .= $ht;
    $total_sql .= $date;
    $total_sql .= $date_nh;

    $total_sql1 = new db_query($total_sql);
    $total = mysql_fetch_assoc($total_sql1->result)['total'];

?>

<div class="main_add_stock position_r d_flex align_c">
    <div class="main_table_add_stock table_vt_scr" onscroll="table_scroll(this)">
        <table class="table_add_stock">
            <tr>
                <th class="font_s16 line_h19">STT
                    <span class="span_thread"></span>
                </th>
                <th class="font_s16 line_h19">Số phiếu
                    <span class="span_thread"></span>
                </th>
                <th class="font_s16 line_h19">Hình thức nhập kho
                    <span class="span_thread"></span>
                </th>
                <th class="font_s16 line_h19">Trạng thái
                    <span class="span_thread"></span>
                </th>
                <th>Ghi chú
                    <span class="span_thread"></span>
                </th>
                <th class="font_s16 line_h19">Người tạo
                    <span class="span_thread"></span>
                </th>
                <th class="font_s16 line_h19">Ngày tạo
                    <span class="span_thread"></span>
                </th>
                <th class="font_s16 line_h19">Ngày nhập kho
                    <span class="span_thread"></span>
                </th>
                <th class="font_s16 line_h19">Kho nhập
                    <?php if(in_array(3, $ro_nhap_kho) || in_array(4, $ro_nhap_kho)) {?>
                        <span class="span_thread"></span>
                    <? }?>
                </th>
                <?php if(in_array(3, $ro_nhap_kho) || in_array(4, $ro_nhap_kho)) {?>
                <th class="font_s16 line_h19" style="width:6%;">Chức năng
                </th>
                <? }?>
            </tr>
            <?php $stt =1; while($item = mysql_fetch_assoc($main_nhapKho -> result)){?>
            <tr class="color_grey font_s14 line_h17 font_w400" data-id="<?= $item['kcxl_id']; ?>">
                <td><?= $stt++; ?></td>
                <td><a href="nhap-kho-chi-tiet-<?= $item['kcxl_id']; ?>.html" class="color_blue font_s14 line_h17 font_w500">PNK - <?= $item['kcxl_id']; ?></a></td>
                <td class="color_grey font_s14 line_h16"><?= hinh_thuc_nhap($item['kcxl_hinhThuc']);?>                
                </td>
                <td class="<?= trang_thai_color($item['kcxl_trangThai']); ?> font_s14 line_h17 font_w400"><?= trang_thai($item['kcxl_trangThai']); ?></td>
                <td class="font_s14 line_h16" style="text-align: left;"><?=$item['kcxl_ghi_chu'] ?></td>
                <td style="text-align: left;">
                    <?php 
                    $nguoi_tao = $item['kcxl_nguoiTao'];
                    $user_id = $user[$nguoi_tao];
                    if($nguoi_tao != 0){
                        $ten_nguoi_tao = $user_id['ep_name'];
                        $anh_nguoi_tao = $user_id['ep_image'];
                        if($anh_nguoi_tao == ""){
                            $anh = '../images/ava_ad.png';
                        }else{
                            $anh = 'https://chamcong.24hpay.vn/upload/employee/'.$anh_nguoi_tao;
                        }
                    }else{
                        $ten_nguoi_tao = $tt_user['com_name'];
                        $anh = 'https://chamcong.24hpay.vn/upload/company/logo/' . $tt_user['com_logo'];
                    }
                    ?>
                    <div class="d_flex flex_start align_c">
                        <img src="<?= $anh; ?>" alt="" class="<?= ($anh == 'https://chamcong.24hpay.vn/upload/company/logo/') ? 'display_none' : '' ?>">
                        <p><?= $ten_nguoi_tao; ?></p>
                    </div>
                </td>
                <td class="font_s14 line_h16"><?=$item['kcxl_ngayTao']; ?></td>
                <td class="font_s14 line_h16"><?=$item['kcxl_ngayNhapKho']; ?></td>
                <td class="font_s14 line_h16" style="text-align: left;"><?=$item['kho_name']; ?></td>
                <?php if(in_array(3, $ro_nhap_kho) || in_array(4, $ro_nhap_kho)) {?>
                    <td class="">
                        <p class="d_flex flex_center align_c">
                            <?php if(in_array(3, $ro_nhap_kho) && $_SESSION['quyen'] == '2' && ($item['kcxl_trangThai']==1 || $item['kcxl_trangThai']==2)) {?>
                                <img src="../images/pen_blu.png" alt="">
                                <a class="a_edit color_blue font_s14 line_h17 font_w500 cursor_p" href="/nhap-kho-chinh-sua-<?= $item['kcxl_id']; ?>.html">Sửa</a>
                            <?php }elseif(in_array(3, $ro_nhap_kho) && $_SESSION['quyen'] == '1' && ($item['kcxl_trangThai']==1 || $item['kcxl_trangThai']==2 || $item['kcxl_trangThai']==3)) {?>
                                <img src="../images/pen_blu.png" alt="">
                                <a class="a_edit color_blue font_s14 line_h17 font_w500 cursor_p" href="/nhap-kho-chinh-sua-<?= $item['kcxl_id']; ?>.html">Sửa</a>
                            <? }?>

                            <?php if(in_array(3, $ro_nhap_kho) && in_array(4, $ro_nhap_kho)) {?>
                                <span class="color_blue margin_lr5 <?=((($item['kcxl_trangThai']==3 || $item['kcxl_trangThai']==4) && $_SESSION['quyen'] == '2') || $item['kcxl_trangThai']==4) ? 'display_none' : ''?>">|</span>
                            <? }?>

                            <?php if(in_array(4, $ro_nhap_kho)) {?>
                                <img src="../images/delete_r.png" alt="">
                                <a class="a_del color_red font_s14 line_h17 font_w500 cursor_p" onclick="id_xoa_nhapKho(this)" data="<?= $item['kcxl_id'] ?>">Xóa</a>
                            <? }?>

                        </p>
                    </td>
                <? }?>
            </tr>
            <?}?>            
        </table>
    </div>
    <div class="pre_q d_flex align_c flex_center position_a display_none" onclick="pre_q(this)">
        <span class="pre_arrow"></span>
    </div>
    <div class="next_q d_flex align_c flex_center position_a" onclick="next_q(this)">
        <span class="next_arrow"></span>
    </div>
</div>
<div class="w_navigation d_flex space_b align_c">
    <div class="l_nav d_flex align_c">
        <p class="color_grey font_s14 line_h17 font_w400">Hiển thị:</p>
        <select name="" id="" class="show_tr_tb color_grey font_s14 line_h17 font_w400" onchange="display(this)">
            <option value="10" <?= ($curr == 10) ? "selected" : ""?>>10</option>
            <option value="20" <?= ($curr == 20) ? "selected" : ""?>>20</option>
            <option value="30" <?= ($curr == 30) ? "selected" : ""?>>30</option>
            <option value="40" <?= ($curr == 40) ? "selected" : ""?>>40</option>
        </select>
    </div>
    <div class="r_nav">
        <ul class="d_flex font_s13 line_h15 font_wN">
            <?php
                echo generatePageBar3('', $page, $curr, $total, $url, '&', '', 'paging_detail', 'preview', '<', 'next', '>', '', '<<<', '', '>>>');
            ?>
        </ul>
    </div>
</div>
<script>
    $('.show_tr_tb').select2({
        minimumResultsForSearch: -1
    });
</script>