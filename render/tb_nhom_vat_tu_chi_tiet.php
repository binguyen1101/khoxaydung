<?php
include("config.php");

$id_nhom = getValue('id_nhom', 'int', 'POST', '');
$hsx = getValue('hsx', 'int', 'POST', '');
$xx = getValue('xx', 'int', 'POST', '');
$sort = getValue('sort', 'int', 'POST', '');
$page = getValue('page', 'int', 'POST', 1);

$curr = 10;
$start = ($page - 1) * $curr;
$start = abs($start);

$input_val = $_POST['input_val'];
$input_val = trim($input_val);

$so_phieu = explode('-', $input_val)[0];
$id_so_phieu = explode('-', $input_val)[1];

$arr = [];
$length_id_phieu = strlen($id_so_phieu);
for ($push = 0; $push < $length_id_phieu; $push++) {
    $ki_tu = substr($id_so_phieu, $push, 1);
    array_push($arr, $ki_tu);
}

$count_arr = count($arr);
$kcxl_id = '';

if ($arr[0] > 0) {
    $kcxl_id = implode('', $arr);
} elseif ($arr[0] == 0) {
    for ($j = 1; $j < $count_arr; $j++) {
        if ($arr[$j] > 0) {
            $kcxl_id = substr(implode('', $arr), $j, $count_arr - $j);
            break;
        }
    }
}
if (isset($_COOKIE['user']) && $_COOKIE['user'] != "" && $_COOKIE['role'] == 1) {
    $token = $_COOKIE['acc_token'];
    $curl = curl_init();
    $data = array();
    curl_setopt($curl, CURLOPT_POST, 1);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
    curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/user_info_company.php');
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));

    $response = curl_exec($curl);
    curl_close($curl);
    $data_tt = json_decode($response, true);
    $tt_user = $data_tt['data']['user_info_result'];
    $id_nguoi_xoa = $_SESSION['com_id'];
}

if (isset($_COOKIE['user']) && $_COOKIE['user'] != "" && $_COOKIE['role'] == 2) {
    $token = $_COOKIE['acc_token'];
    $curl = curl_init();
    $data = array();
    curl_setopt($curl, CURLOPT_POST, 1);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
    curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/user_info_employee.php');
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));
    $response = curl_exec($curl);
    curl_close($curl);
    $data_tt = json_decode($response, true);
    $tt_user = $data_tt['data']['user_info_result'];
    $id_nguoi_xoa = $_SESSION['ep_id'];
}

$id_cty = $tt_user['com_id'];

if ($input_val != "") {
    if ($_COOKIE['role'] == 1) {
        $ds_vat_tu = "SELECT `dsvt_id`, `dsvt_maVatTuThietBi`,  `dsvt_name`,  `dsvt_description`, `dsvt_donGia`, `dvt_name`, `hsx_name`, `xx_name` 
        FROM `danh-sach-vat-tu` 
        LEFT JOIN `don-vi-tinh` ON `danh-sach-vat-tu`.`dsvt_donViTinh` =`don-vi-tinh`.`dvt_id` AND `dvt_check` = 1 
        LEFT JOIN `hang-san-xuat` ON  `danh-sach-vat-tu`.`dsvt_hangSanXuat`=`hang-san-xuat`.`hsx_id` AND `hsx_check` = 1
        LEFT JOIN `xuat-xu` ON  `danh-sach-vat-tu`.`dsvt_xuatXu`=`xuat-xu`.`xx_id` 
        WHERE `dsvt_nhomVatTuThietBi` = $id_nhom AND  `dsvt_check`=1  
        AND (`dsvt_id` = '$kcxl_id' OR `dsvt_id` LIKE '%$input_val%') AND `dsvt_id_ct` = $id_cty ";
    }
    if ($_COOKIE['role'] == 2) {
        $ds_vat_tu = "SELECT `dsvt_id`, `dsvt_maVatTuThietBi`,  `dsvt_name`,  `dsvt_description`, `dsvt_donGia`, `dvt_name`, `hsx_name`, `xx_name` 
        FROM `danh-sach-vat-tu` 
        LEFT JOIN `don-vi-tinh` ON `danh-sach-vat-tu`.`dsvt_donViTinh` =`don-vi-tinh`.`dvt_id` AND `dvt_check` = 1 
        LEFT JOIN `hang-san-xuat` ON  `danh-sach-vat-tu`.`dsvt_hangSanXuat`=`hang-san-xuat`.`hsx_id` AND `hsx_check` = 1
        LEFT JOIN `xuat-xu` ON  `danh-sach-vat-tu`.`dsvt_xuatXu`=`xuat-xu`.`xx_id` 
        WHERE `dsvt_nhomVatTuThietBi` = $id_nhom AND  `dsvt_check`=1 
        AND (`dsvt_id` = '$kcxl_id' OR `dsvt_id` LIKE '%$input_val%') AND `dsvt_id_ct` = $id_cty AND NOT `dsvt_userCreateId` = 0 ";
    }
} else {
    if ($_COOKIE['role'] == 1) {
        $ds_vat_tu = "SELECT `dsvt_id`, `dsvt_maVatTuThietBi`,  `dsvt_name`,  `dsvt_description`, `dsvt_donGia`, `dvt_name`, `hsx_name`, `xx_name` 
        FROM `danh-sach-vat-tu` 
        LEFT JOIN `don-vi-tinh` ON  `danh-sach-vat-tu`.`dsvt_donViTinh`=`don-vi-tinh`.`dvt_id` AND `dvt_check` =1
        LEFT JOIN `hang-san-xuat` ON  `danh-sach-vat-tu`.`dsvt_hangSanXuat`=`hang-san-xuat`.`hsx_id` AND `hsx_check` = 1
        LEFT JOIN `xuat-xu` ON  `danh-sach-vat-tu`.`dsvt_xuatXu`=`xuat-xu`.`xx_id` 
        WHERE `dsvt_nhomVatTuThietBi` = $id_nhom AND `dsvt_check`=1   AND `dsvt_id_ct` = $id_cty ";
    }
    if ($_COOKIE['role'] == 2) {
        $ds_vat_tu = "SELECT `dsvt_id`, `dsvt_maVatTuThietBi`,  `dsvt_name`,  `dsvt_description`, `dsvt_donGia`, `dvt_name`, `hsx_name`, `xx_name` 
        FROM `danh-sach-vat-tu` 
        LEFT JOIN `don-vi-tinh` ON  `danh-sach-vat-tu`.`dsvt_donViTinh`=`don-vi-tinh`.`dvt_id` AND `dvt_check` =1
        LEFT JOIN `hang-san-xuat` ON  `danh-sach-vat-tu`.`dsvt_hangSanXuat`=`hang-san-xuat`.`hsx_id` AND `hsx_check` = 1
        LEFT JOIN `xuat-xu` ON  `danh-sach-vat-tu`.`dsvt_xuatXu`=`xuat-xu`.`xx_id` 
        WHERE `dsvt_nhomVatTuThietBi` = $id_nhom AND `dsvt_check`=1   AND `dsvt_id_ct` = $id_cty AND NOT `dsvt_userCreateId` = 0 ";
    }
}

if ($sort == "" && $hsx == "" && $xx == "" && $input_val == "") {
    $url = "/nhom-vat-tu-chi-tiet-" . $id_nhom . ".html";
} else {
    $url = "/nhom-vat-tu-chi-tiet-" . $id_nhom . ".html?hsx=" . $hsx . "&xx=" . $xx . "&sort=" . $sort . "&input=" . $input_val;
}

if ($hsx == "") {
    $dk_hsx = "";
} else {
    $dk_hsx = "AND `hsx_id` = $hsx ";
}

if ($xx == "") {
    $dk_xx = "";
} else {
    $dk_xx = "AND `xx_id` = $xx ";
}

if ($sort == "") {
    $dk_sort = "ORDER BY `dsvt_id` DESC ";
} else if ($sort == 1) {
    $dk_sort = "ORDER BY `dsvt_donGia` ASC ";
} else if ($sort == 2) {
    $dk_sort = "ORDER BY `dsvt_donGia` DESC ";
}


$limited = "LIMIT $start,$curr";

$ds_vat_tu .= $dk_hsx;
$ds_vat_tu .= $dk_xx;
$ds_vat_tu .= $dk_sort;
$ds_vat_tu .= $limited;
$ds_vat_tu = new db_query($ds_vat_tu);

$total_sql =  "SELECT COUNT(`dsvt_id`) AS total FROM `danh-sach-vat-tu`
    WHERE `dsvt_id` = 1 AND (`dsvt_id` = '$kcxl_id' OR `dsvt_id` LIKE '%$input_val%') ";

$total_sql1 = new db_query($total_sql);
$total = mysql_fetch_assoc($total_sql1->result)['total'];

?>

<div class="tb_operation_wh position_r d_flex align_c">
    <div class="table_vt_scr" onscroll="table_scroll(this)">
        <div class="table_ds_vt">
            <table style="width: 1711px;">
                <tr class="tit_tbl color_white font_s16 line_h19 font_w500 back_blue">
                    <th>STT<span class="span_tbody"></span></th>
                    <th>Mã vật tư thiết bị<span class="span_tbody"></span></th>
                    <th>Tên đầy đủ vật tư thiết bị<span class="span_tbody"></span></th>
                    <th>Đơn vị tính<span class="span_tbody"></span></th>
                    <th>Hãng sản xuất<span class="span_tbody"></span></th>
                    <th>Xuất xứ<span class="span_tbody"></span></th>
                    <th>Mô tả<span class="span_tbody"></span></th>
                    <th>Đơn giá (VNĐ)<span class="span_tbody"></span></th>
                    <?php if (in_array(3, $ro_nhom_vt) || in_array(4, $ro_nhom_vt)) { ?>
                        <th style="width: 136px;">Chức năng</th>
                    <? } ?>
                </tr>
                <?php $i++;
                while ($row = mysql_fetch_assoc($ds_vat_tu->result)) : ?>
                    <tr class="color_grey font_s14 line_h17 font_w400" data-id="<?= $row['dsvt_id']; ?>">
                        <td><?php echo $i; ?></td>
                        <td>VT - <?= $row['dsvt_id'] ?></td>
                        <td style="text-align: left;"><a href="/danh-sach-vat-tu-thiet-bi-chi-tiet-<?= $row['dsvt_id']; ?>.html" class="color_blue font_w500"><?php echo $row['dsvt_name']; ?></a></td>
                        <td><?php echo $row['dvt_name']; ?></td>
                        <td style="text-align: left;"><?php echo $row['hsx_name']; ?></td>
                        <td><?php echo $row['xx_name']; ?></td>
                        <td style="text-align: left;"><?php echo $row['dsvt_description']; ?></td>
                        <td style="text-align: right;"><?php echo number_format($row['dsvt_donGia'], 0, '', '.') . '&nbspVNĐ'; ?></td>
                        <?php if (in_array(3, $ro_nhom_vt) || in_array(4, $ro_nhom_vt)) { ?>
                            <td>
                                <?php if (in_array(3, $ro_nhom_vt)) { ?>
                                    <a class="color_blue font_s14 line_h17 font_w500 cursor_p" href="nhom-vat-tu-thiet-bi-chinh-sua-vat-tu-<?= $row['dsvt_id']; ?>.html?id_gp=<?= $id_nhom ?>">
                                        <img src="../images/edit_tb.png" alt="">Sửa
                                    </a>
                                <? } ?>
                                <?php if (in_array(3, $ro_nhom_vt) && in_array(4, $ro_nhom_vt)) { ?><span style="border-left: 1px solid #4C5BD4; margin: auto 5px;"></span><? } ?>
                                <?php if (in_array(4, $ro_nhom_vt)) { ?>
                                    <a class="a_del color_red font_s14 line_h17 font_w500 cursor_p" onclick="delete_dv(this)">
                                        <img src="../images/del_tb.png" alt="">Xóa
                                    </a>
                                <? } ?>
                            </td>
                        <? } ?>
                    </tr>
                <?php $i++;
                endwhile; ?>
            </table>
        </div>
    </div>
    <div class="pre_q d_flex align_c flex_center position_a display_none" onclick="pre_q(this)">
        <span class="pre_arrow"></span>
    </div>
    <div class="next_q d_flex align_c flex_center position_a" onclick="next_q(this)">
        <span class="next_arrow"></span>
    </div>
</div>
<div class="w_navigation d_flex flex_end align_c">
    <!-- <div class="l_nav d_flex align_c">
        <p class="color_grey font_s14 line_h17 font_w400">Hiển thị:</p>
        <select name="" id="" class="show_tr_tb color_grey font_s14 line_h17 font_w400">
        <option value="">10</option>
        <option value="">20</option>
        <option value="">30</option>
        <option value="">40</option>
        </select>
    </div> -->
    <div class="r_nav">
        <ul class="d_flex font_s13 line_h15 font_wN">
            <?php
            if ($hsx != "" || $xx != "" || $sort != "" || $input_val != "") {
                echo generatePageBar3('', $page, $curr, $total, $url, '&', '', 'paging_detail', 'preview', '<', 'next', '>', '', '<<<', '', '>>>');
            } else {
                echo generatePageBar3('', $page, $curr, $total, $url, '?', '', 'paging_detail', 'preview', '<', 'next', '>', '', '<<<', '', '>>>');
            }
            ?>
        </ul>
    </div>
</div>
<script>
    function delete_dv(_this) {
        var id_vt = $(_this).parent().parent().attr('data-id');
        var id_cty = <?= json_encode($id_cty); ?>;
        var id_nguoi_xoa = <?= json_encode($id_nguoi_xoa); ?>;
        var name = $(_this).parent().parent().find('td:nth-child(3)').text();
        $('.delete_materials_equipment').show();
        $('.delete_materials_equipment strong').text(name);
        $('.delete_materials_equipment .button_accp').on("click", function() {
            $.ajax({
                url: "../ajax/delete_danh_sach_vat_tu.php",
                type: "POST",
                data: {
                    id_vt: id_vt,
                    id_cty: id_cty,
                    id_nguoi_xoa: id_nguoi_xoa
                },
                success: function(data) {
                    if (data == "") {
                        $('.popup_delete_materials_equipment_success strong').text(name);
                        $('.popup_delete_materials_equipment_success a').attr('href','nhom-vat-tu-thiet-bi-chi-tiet-'+ <?=json_decode($id_nhom);?> +'.html');

                    } else {
                        alert("Xóa thất bại!");
                    }
                }
            });
        });
        $('.button_close,.btn_close2').click(function() {
            $('.delete_materials_equipment .button_accp').off();
        });
    }
</script>