<?php
    include("config.php"); 
    $page = getValue('page','int','POST',1);
    $search = getValue('search','int','POST',"");
    $com_id = getValue('com_id','int','POST','');
    $currentP = 20; // mặc định theo API không sửa

    if (isset($_SESSION['quyen']) && $_SESSION['quyen'] == 1) {
        $curl = curl_init();
        $token = $_COOKIE['acc_token'];
        curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/list_all_employee_of_company.php');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));
        $response = curl_exec($curl);
        curl_close($curl);
    } elseif (isset($_SESSION['quyen']) && ($_SESSION['quyen'] == 2)) {
        $curl = curl_init();
        $token = $_COOKIE['acc_token'];
        curl_setopt($curl, CURLOPT_URL, 'https://chamcong.24hpay.vn/service/list_all_my_partner.php?get_all=true');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));
        $response = curl_exec($curl);
        curl_close($curl);
    }

    $data_list = json_decode($response, true);
    $data_list_nv = $data_list['data']['items'];

    $user = [];
    for ($i = 0; $i < count($data_list_nv); $i++){
        $item = $data_list_nv[$i];
        $user[$i] = $item;
    }

    if(empty($search)){
        $count = count($data_list_nv);
        $user_p = [];
        if($page > 0){
            for($j = ($currentP*($page-1)); $j < $currentP*$page; $j++){
                $user_item = $user[$j];
                $user_p[$j] = $user_item;
            }
        }
    }else{
        $user_p = [];

        for ($j = 0; $j < count($data_list_nv); $j++){
            if($data_list_nv[$j]['ep_id'] == $search){
                $user_item = $data_list_nv[$j];
                $user_p[$j] = $user_item;
            }
        }
        $count = count($user_p);
    }

    // echo "<pre>";
    // print_r($user_p);
    // echo "</pre>";


    if($search == ""){
        $url = "/phan-quyen.html";
    }else{
        $url = "/phan-quyen.html?search=".$search;
    }


    $chuc_vu = array("1"=>"SINH VIÊN THỰC TẬP", "2"=>"NHÂN VIÊN THỬ VIỆC", "3"=>"NHÂN VIÊN CHÍNH THỨC",
                    "4"=>"TRƯỞNG NHÓM", "5"=>"PHÓ TRƯỞNG PHÒNG", "6"=>"TRƯỞNG PHÒNG",
                    "7"=>"PHÓ GIÁM ĐỐC", "8"=>"GIÁM ĐỐC", "9"=>"NHÂN VIÊN PART TIME",
                    "10"=>"PHÓ BAN DỰ ÁN", "11"=>"TRƯỞNG BAN DỰ ÁN", "12"=>"PHÓ TỔ TRƯỞNG",
                    "13"=>"TỔ TRƯỞNG", "14"=>"PHÓ TỔNG GIÁM ĐỐC", "16"=>"TỔNG GIÁM ĐỐC",
                    "17"=>"THÀNH VIÊN HỘI ĐỒNG QUẢN TRỊ", "18"=>"PHÓ CHỦ TỊCH HỘI ĐỒNG QUẢN TRỊ", "19"=>"CHỦ TỊCH HỘI ĐỒNG QUẢN TRỊ",
                    "20"=>"NHÓM PHÓ", "21"=>"TỔNG GIÁM ĐỐC TẬP ĐOÀN", "22"=>"PHÓ TỔNG GIÁM ĐỐC TẬP ĐOÀN"
                    );

?>

<div class="main_table position_r mb_15 d_flex align_c table_vt_scr" onscroll="table_scroll(this)">
    <table class="table table_decentralization table_permissions">
        <tr>
            <th>
                Chọn
                <span class="span_tbody"></span>
            </th>
            <th>Mã NV
                <span class="span_tbody"></span>
            </th>
            <th>Họ tên
                <span class="span_tbody"></span>
            </th>
            <th>Phòng ban
                <span class="span_tbody"></span>
            </th>
            <th>Chức danh
            <?php if(in_array(2,$ro_phan_quyen) || in_array(3,$ro_phan_quyen)){ ?>
                <span class="span_tbody"></span>
            <?php }?>
            </th>
            <?php if(in_array(2,$ro_phan_quyen) || in_array(3,$ro_phan_quyen)){ ?>
            <th>Chức năng
            </th>
            <?php }?>
        </tr>
        <?php foreach($user_p as $user) {?>
        <tr data-id = "<?=$user['ep_id']?>">
            <td class="font_s14 line_16 color_grey">
                <input class="hw_check" type="checkbox">
            </td>
            <td class="font_s14 line_16 color_grey">NV -
                <?=$user['ep_id']?>
            </td>
            <td class="font_s14 line_16 color_grey">
                <div class="d_flex align_c">
                    <?php
                    if ($user['ep_image'] == "") {
                        $anh = '../images/ava_ad.png';
                    } else {
                        $anh = 'https://chamcong.24hpay.vn/upload/employee/' . $user['ep_image'];
                    }
                    ?>
                    <img class="ava_human_table" src="<?=$anh?>" alt="">
                    <p>
                        <?=$user['ep_name']?>
                    </p>
                </div>
            </td>
            <td class="font_s14 line_16 color_grey" style="text-align: left;">
                <?=$user['dep_name']?>
            </td>
            <td class="font_s14 line_16 color_grey">
                <?php 
                $id_chu_vu = $user['position_id'];
                echo $chuc_vu[$id_chu_vu];
            ?>
            </td>
            <?php if(in_array(2,$ro_phan_quyen) || in_array(3,$ro_phan_quyen)){ ?>
            <td class="font_s14 line_16 color_grey">
                <a href="/phan-quyen-chi-tiet-<?=$user['ep_id']?>.html">
                    <div class="d_flex align_c flex_center">
                        <img class="" src="../images/key_b.png" alt="">
                        <p class="color_blue">Thiết lập quyền</p>
                    </div>
                </a>
            </td>
            <?php }?>
        </tr>
        <?php }?>
    </table>
    <div class="pre_q d_flex align_c flex_center position_a display_none" onclick="pre_q(this)">
        <span class="pre_arrow"></span>
    </div>
    <div class="next_q d_flex align_c flex_center position_a" onclick="next_q(this)">
        <span class="next_arrow"></span>
    </div>
</div>
<div class="w_navigation d_flex flex_end align_c">
    <!-- <div class="l_nav d_flex align_c">
        <p class="color_grey font_s14 line_h17 font_w400">Hiển thị:</p>
        <select name="" id="" class="show_tr_tb color_grey font_s14 line_h17 font_w400">
            <option value="">10</option>
            <option value="">20</option>
            <option value="">30</option>
            <option value="">40</option>
        </select>
    </div> -->
    <div class="r_nav">
        <ul class="d_flex font_s13 line_h15 font_wN">
            <?php
                if($search != ""){
                    echo generatePageBar3('',$page,$currentP,$count,$url,'&','','paging_detail','preview','<','next','>','','<<<','','>>>');
                }else{
                    echo generatePageBar3('',$page,$currentP,$count,$url,'?','','paging_detail','preview','<','next','>','','<<<','','>>>');
                }
            ?>
        </ul>
    </div>
</div>
<script>
    $('.table_decentralization input[type=checkbox]').click(function () {
        if($(this).is(':checked')){
            $('.go_set_permissions').show();
            $('input[type=checkbox]').not(this).prop('checked', false);
        }
        if(!$(this).is(':checked')){
            $('.go_set_permissions').hide();
        }
    });
</script>