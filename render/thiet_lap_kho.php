<?php
    include('config.php');

    $id = getValue('id','int','POST','');
    $id_kho = getValue('id_kho','int','POST','');
    $set_kho = new db_query("SELECT `dsvt_id`, `dsvt_maVatTuThietBi`, `dsvt_name`, `dsvt_donViTinh`,`dsvt_soLuongToiThieu`, `dsvt_soLuongToiDa`,`dvt_name` FROM `danh-sach-vat-tu`
    LEFT JOIN `don-vi-tinh` ON `dsvt_donViTinh` = `dvt_id`
     WHERE `dsvt_id`= '$id' ");
    $item = mysql_fetch_assoc($set_kho -> result);
    $sl_min = json_decode($item['dsvt_soLuongToiThieu'])->$id_kho;
    $sl_max = json_decode($item['dsvt_soLuongToiDa'])->$id_kho;
?>
 <div class="d_flex flex_column">
        <p class="color_grey font_s15 line_h18 font_w500">Mã vật tư thiết bị</p>
        <input class="color_grey font_s14 line_h17 font_w400" type="text"  value="<?=$item['dsvt_maVatTuThietBi'] ?> - <?= $item['dsvt_id'] ?>" disabled="disabled">
    </div>
    <div class="d_flex flex_column">
        <p class="color_grey font_s15 line_h18 font_w500">Tên vật tư thiết bị<span style="color: red;">*</span></p>
        <input class="color_grey font_s14 line_h17 font_w400" type="text" name="ten_vt"  disabled="disable" value="<?=$item['dsvt_name'] ?>">
    </div>
    <div class="d_flex flex_column">
        <p class="color_grey font_s15 line_h18 font_w500">Đơn vị tính</p>
        <input class="color_grey font_s14 line_h17 font_w400" type="text" name="don_vi_tinh" readonly="readonly" value="<?=$item['dvt_name'] ?>">
    </div>
    <div class="d_flex flex_column">
        <p class="color_grey font_s15 line_h18 font_w500">Nhập số lượng tồn kho tối thiểu</p>
        <input class="color_grey font_s14 line_h17 font_w400" type="text" name="sltt" value="<?=($item['dsvt_soLuongToiThieu'] == "") ? "0" : $sl_min ?>">
    </div>
    <div class="d_flex flex_column">
        <p class="color_grey font_s15 line_h18 font_w500">Nhập số lượng tồn kho tối đa</p>
        <input class="color_grey font_s14 line_h17 font_w400" type="text" name="sltd" value="<?=($item['dsvt_soLuongToiDa'] == "") ? "0" : $sl_max ?>">
    </div>
    <div class="btn_ct_pp d_flex flex_center">
        <button class="btn_cancel back_w color_blue font_s15 line_h18 font_w500 cursor_p" onclick="close_popup()">Hủy</button>
        <button class="btn_save back_blue color_white font_s15 line_h18 font_w500 cursor_p btn_acp_thietLap" data-id="<?= $item['dsvt_id'] ?>" onclick="sua_thiet_lap(this)">Đồng ý</button>
    </div>