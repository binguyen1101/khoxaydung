<? 
    include("config.php");
    $id = $_POST['id'];

    $vattu = new db_query("SELECT `kcxl_id`, `kcxl_soPhieu`, `kcxl_trangThai`, `dsvt_id`,`slvt_idPhieu`, `slvt_maPhieu`,`dsvt_donGia`, 
    `dsvt_maVatTuThietBi`, `dsvt_name`, `dvt_name`, `hsx_name`, `xx_name`, `slvt_soLuongDieuChuyen`,`slvt_slTon`,`dsvt_description`
    FROM `danh-sach-vat-tu`
    LEFT JOIN `don-vi-tinh` ON `dsvt_donViTinh` =`dvt_id`
    LEFT JOIN `hang-san-xuat` ON `dsvt_hangSanXuat` = `hsx_id`
    LEFT JOIN `xuat-xu` ON `dsvt_xuatXu` = `xx_id`
    LEFT JOIN `so-luong-vat-tu` ON `dsvt_id` = `slvt_maVatTuThietBi`
    LEFT JOIN `kho-cho-xu-li` ON `slvt_idPhieu` = `kcxl_id`
    WHERE `dsvt_check` = '1' AND `kcxl_id`='$id' 
    ORDER BY `dsvt_id` ASC ");
?>

<p class="tit_table_vt color_blue font_s16 line_h19 font_w700"> Danh sách vật tư</p>                 
                   
<div class="position_r align_c d_flex ">
    <div class="main_table table_vt_scr">
        <table class="table table_list_meterial-2" style="width: 1672px;">
            <tr>
                <th>STT
                    <span class="span_thread"></span>
                </th>
                <th>Mã vật tư
                    <span class="span_thread"></span>
                </th>
                <th>Tên đầy đủ vật tư thiết bị
                    <span class="span_thread"></span>
                </th>
                <th>Đơn vị tính
                    <span class="span_thread"></span>
                </th>
                <th> Hãng sản xuất
                    <span class="span_thread"></span>
                </th>
                <th> Xuất xứ
                    <span class="span_thread"></span>
                </th>
                <th> Số lượng điều chuyển
                    <span class="span_thread"></span>
                </th>
                <th> Số lượng thực tế nhập kho
                    <span class="span_thread"></span>
                </th>
                <th>Đơn giá (VNĐ)
                    <span class="span_thread"></span>
                </th>
                <th>Thành tiền (VNĐ)
                    <span class="span_thread"></span>
                </th>
            </tr>
            <? while($row = mysql_fetch_assoc($vattu -> result)){?>
            <tr class="color_grey font_s14 line_h17 font_w400" >
                <td>1</td>
                <td><?= phieu($row['dsvt_id'],$row['dsvt_maVatTuThietBi']) ?> <input type="hidden" name="id_tk" value="<?= $row['kcxl_id']; ?>"> </td>
                <td class="color_blue font_s14 line_h17 font_w500"><?= $row['dsvt_name'] ?></td>
                <td><?= $row['dvt_name']; ?></td>
                <td><?= $row['hsx_name']; ?></td>
                <td><?= $row['xx_name']; ?></td>
                <td><?= $row['slvt_soLuongDieuChuyen']; ?></td>
                <td style="text-align: right;"><input placeholder="Nhập số lượng"></td>
                <td><?= $row['dsvt_donGia']; ?></td>
                <td></td>
            </tr>
            <?}?>
        </table>
    </div>
    <div class="pre_q d_flex align_c flex_center position_a">
        <span class="pre_arrow"></span>
    </div>
    <div class="next_q d_flex align_c flex_center position_a">
        <span class="next_arrow"></span>
    </div>
</div>