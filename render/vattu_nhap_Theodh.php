<?php

    include("config.php");
    $id_dh = getValue('id_dh','int','POST', "");
    $id_cty = getValue('id_cty','int','POST', "");
    
    // API vật tư đơn hàng
    $curl_vtdh = curl_init();
    curl_setopt($curl_vtdh, CURLOPT_POST, 1);
    curl_setopt($curl_vtdh, CURLOPT_URL, 'https://phanmemquanlycungung.timviec365.vn/api/vat_tu_dh.php');
    curl_setopt($curl_vtdh, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl_vtdh, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($curl_vtdh, CURLOPT_POSTFIELDS, [
        'com_id' => $id_cty,
        'dh_id' => $id_dh,
    ]);
    $response_vtdh = curl_exec($curl_vtdh);
    curl_close($curl_vtdh);
    $data_list_vtdh = json_decode($response_vtdh, true);
    $list_vtdh = $data_list_vtdh['data']['items'];

    $list_vtdh1 = [];
    for ($j = 0; $j < count($list_vtdh); $j++) {
        $list_vtdh_p = $list_vtdh[$j];
        $list_vtdh1[$list_vtdh_p["id"]] = $list_vtdh_p;
    }

    $oninput = "this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');";
?>
<table class="table table_list_meterial" style="width:1689px;">
    <tr>
        <th>STT
            <span class="span_thread"></span>
        </th>
        <th>Mã vật tư
            <span class="span_thread"></span>
        </th>
        <th>Tên đầy đủ vật tư thiết bị
            <span class="span_thread"></span>
        </th>
        <th>Đơn vị tính
            <span class="span_thread"></span>
        </th>
        <th> Hãng sản xuất
            <span class="span_thread"></span>
        </th>
        <th> Xuất xứ
            <span class="span_thread"></span>
        </th>
        <th> Số lượng theo đơn hàng
            <span class="span_thread"></span>
        </th>
        <th> Số lượng thực tế nhập kho
            <span class="span_thread"></span>
        </th>
        <th>Đơn giá (VNĐ)
            <span class="span_thread"></span>
        </th>
        <th>Thành tiền (VNĐ)
            <span class="span_thread"></span>
        </th>
    </tr>
    <?php $stt = 1; foreach($list_vtdh as $dsvt){
        $id_dsvt = $dsvt['id_vat_tu'];
        $sql_dsvt = new db_query("SELECT `dvt_name`, `hsx_name`, `xx_name`, `dsvt_donGia`, `dsvt_name` FROM `danh-sach-vat-tu`
            LEFT JOIN `don-vi-tinh` ON `dsvt_donViTinh` = `dvt_id` AND `dvt_check`= 1
            LEFT JOIN `hang-san-xuat` ON `dsvt_hangSanXuat` = `hsx_id` AND `hsx_check`= 1
            LEFT JOIN `xuat-xu` ON `dsvt_xuatXu` = `xx_id`
            WHERE `dsvt_id` = $id_dsvt AND `dsvt_check` = 1 AND `dsvt_id_ct` = $id_cty
        ");
        $info_dsvt = mysql_fetch_assoc($sql_dsvt->result);
        ?>
    <tr class="color_grey font_s14 line_h17 font_w400 table_3" data-id="<?=$id_dsvt?>">
        <td class="font_s14 line_h17 color_grey font_w400"><?= $stt++; ?></td>
        <td class="font_s14 line_h17 color_grey font_w400">VT -  <?= $id_dsvt; ?></td>
        <td class="font_s14 line_h17 color_blue font_w500" style="text-align: left;"><?= $info_dsvt['dsvt_name']; ?></td>
        <td class="font_s14 line_h17 color_grey font_w400"><?=$info_dsvt['dvt_name']?></td>
        <td class="font_s14 line_h17 color_grey font_w400" style="text-align: left;"><?=$info_dsvt['hsx_name']?></td>
        <td class="font_s14 line_h17 color_grey font_w400"><?=$info_dsvt['xx_name']?></td>
        <td class="font_s14 line_h17 color_grey font_w400" style="text-align: right;background:#EEEEEE;"><?=$dsvt['so_luong_ky_nay']?></td>
        <td class="font_s14 line_h17 color_grey font_w400" style="text-align: right;"><input class="nhap_so_luong" placeholder="Nhập số lượng" oninput="<?=$oninput?>" onkeyup=tong_vt(this)></td>
        <td class="font_s14 line_h17 color_grey font_w400 don_gia_3" style="text-align: right;"><?=$info_dsvt['dsvt_donGia']?></td>
        <td class="font_s14 line_h17 color_grey font_w400 thanh_tien_3" style="text-align: right;"></td>
    </tr>
    <?php } ?>
</table>