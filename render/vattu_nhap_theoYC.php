<?php
    include("config.php");
    // $id_phieu = getValue('id_phieu','int','POST', "");
    $id_phieu_yc = getValue('id_phieu_yc','int','POST', "");
    $id_cty = getValue('id_cty','int','POST', "");
    
    // API vật tư theo phiếu yêu cầu
    $curl_vt_yc = curl_init();
    curl_setopt($curl_vt_yc, CURLOPT_POST, 1);
    curl_setopt($curl_vt_yc, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl_vt_yc, CURLOPT_URL, "https://phanmemquanlycungung.timviec365.vn/api/vat_tu_yc.php");
    curl_setopt($curl_vt_yc, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($curl_vt_yc, CURLOPT_POSTFIELDS, [
        'id_phieu' => $id_phieu_yc,
    ]);
    $response_vt_yc = curl_exec($curl_vt_yc);
    curl_close($curl_vt_yc);
    $emp_json = json_decode($response_vt_yc, true);
    $emp_arr = $emp_json['data']['items'];
    $emp_vt_yc = [];
    for ($i = 0; $i < count($emp_arr); $i++) {
        $vt_yc = $emp_arr[$i];
        $emp_vt_yc[$vt_yc["id"]] = $vt_yc;
    }

    $oninput = "this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');";
?>
<table class="table table_list_meterial-5" style="width: 1855px;">
    <tr>
        <th>STT
            <span class="span_thread"></span>
        </th>
        <th>Mã vật tư
            <span class="span_thread"></span>
        </th>
        <th>Tên đầy đủ vật tư thiết bị
            <span class="span_thread"></span>
        </th>
        <th>Đơn vị tính
            <span class="span_thread"></span>
        </th>
        <th> Hãng sản xuất
            <span class="span_thread"></span>
        </th>
        <th> Xuất xứ
            <span class="span_thread"></span>
        </th>
        <th> Số lượng theo yêu cầu
            <span class="span_thread"></span>
        </th>
        <th> Số lượng được duyệt
            <span class="span_thread"></span>
        </th>
        <th> Số lượng thực tế nhập kho
            <span class="span_thread"></span>
        </th>
        <th>Đơn giá (VNĐ)
            <span class="span_thread"></span>
        </th>
        <th>Thành tiền (VNĐ)
            <span class="span_thread"></span>
        </th>
    </tr>
    <?php $stt = 1; foreach($emp_vt_yc as $dsvt){
        $id_dsvt = $dsvt['id_vat_tu'];
        $sql_dsvt = new db_query("SELECT `dvt_name`, `hsx_name`, `xx_name`, `dsvt_donGia`, `dsvt_name` FROM `danh-sach-vat-tu`
            LEFT JOIN `don-vi-tinh` ON `dsvt_donViTinh` = `dvt_id` AND `dvt_check` = 1
            LEFT JOIN `hang-san-xuat` ON `dsvt_hangSanXuat` = `hsx_id` AND `hsx_check` = 1
            LEFT JOIN `xuat-xu` ON `dsvt_xuatXu` = `xx_id`
            WHERE `dsvt_id` = $id_dsvt AND `dsvt_check` = 1 AND `dsvt_id_ct` = $id_cty
        ");
        $info_dsvt = mysql_fetch_assoc($sql_dsvt->result);

        // $sql_sl = new db_query("SELECT `slvt_soLuongNhapKho` FROM `so-luong-vat-tu` WHERE `slvt_maVatTuThietBi` = $id_dsvt AND `slvt_idPhieu` = $id_phieu AND `slvt_id_ct` = $id_cty");
        // $sl = mysql_fetch_assoc($sql_sl->result)['slvt_soLuongNhapKho'];
        ?>
        <tr class="color_grey font_s14 line_h17 font_w400 table_3" data-id="<?=$id_dsvt?>">
            <td class="font_s14 line_h17 color_grey font_w400"><?= $stt++; ?></td>
            <td class="font_s14 line_h17 color_grey font_w400">VT -  <?= $id_dsvt; ?></td>
            <td class="font_s14 line_h17 color_blue font_w500" style="text-align: left;"><?= $info_dsvt['dsvt_name']; ?></td>
            <td class="font_s14 line_h17 color_grey font_w400"><?=$info_dsvt['dvt_name']?></td>
            <td class="font_s14 line_h17 color_grey font_w400" style="text-align: left;"><?=$info_dsvt['hsx_name']?></td>
            <td class="font_s14 line_h17 color_grey font_w400"><?=$info_dsvt['xx_name']?></td>
            <td class="font_s14 line_h17 color_grey font_w400" style="text-align: right;background:#EEEEEE;"><?=$dsvt['so_luong_yc_duyet']?></td>
            <td class="font_s14 line_h17 color_grey font_w400" style="text-align: right;background:#EEEEEE;"><?=$dsvt['so_luong_duyet']?></td>
            <td class="font_s14 line_h17 color_grey font_w400" style="text-align: right;"><input class="nhap_so_luong" placeholder="Nhập số lượng" oninput="<?=$oninput?>" onkeyup=tong_vt(this)></td>
            <td class="font_s14 line_h17 color_grey font_w400 don_gia_3" style="text-align: right;"><?= $info_dsvt['dsvt_donGia']; ?></td>
            <td class="font_s14 line_h17 color_grey font_w400 thanh_tien_3" style="text-align: right;"></td>
        </tr>
<?php } ?>
</table>
