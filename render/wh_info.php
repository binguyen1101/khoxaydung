<?
include('config.php');

$id_kho = getValue('id', 'int', 'GET', '');


$page = $_POST['page'];
$wh_info = new db_query("SELECT `dsvt_id`, `dsvt_maVatTuThietBi` ,`dsvt_name`, `dsvt_nhomVatTuThietBi`,
 `dsvt_donViTinh`,`dsvt_hangSanXuat`, `dsvt_xuatXu`,`nvt_name`,`hsx_name`,`dvt_name`,`xx_name`,`slvt_slTon`
FROM `danh-sach-vat-tu`
INNER JOIN `kho` ON `kho_id`=`dsvt_kho`
INNER JOIN `nhom-vat-tu-thiet-bi` ON `dsvt_nhomVatTuThietBi` = `nvt_id`
INNER JOIN `hang-san-xuat` ON `dsvt_hangSanXuat` = `hsx_id`
INNER JOIN `don-vi-tinh` ON `dsvt_donViTinh` = `dvt_id`
INNER JOIN `xuat-xu` ON `dsvt_xuatXu` = `xx_id`
INNER JOIN `so-luong-vat-tu` ON `dsvt_id` = `slvt_maVatTuThietBi`
WHERE `dsvt_check` = '1' AND `dsvt_kho`='$id_kho'");
$stt=1;

?>
<div class="position_r d_flex align_c">
    <div class="main_table mb_15 table_vt_scr">
        <table class="table table_add_stoke_info" style="width: 1601px;">
            <tr>
                <th>
                    STT
                    <span class="span_tbody"></span>
                </th>
                <th>Mã vật tư thiết bị
                    <span class="span_tbody"></span>
                </th>
                <th>Tên đầy đủ vật tư thiết bị
                    <span class="span_tbody"></span>
                </th>
                <th>Nhóm vật tư thiết bị
                    <span class="span_tbody"></span>
                </th>
                <th>Số lượng
                    <span class="span_tbody"></span>
                </th>
                <th>Đơn vị tính
                    <span class="span_tbody"></span>
                </th>
                <th>Hãng sản xuất
                    <span class="span_tbody"></span>
                </th>
                <th>Xuất xứ
                    <span class="span_tbody"></span>
                </th>
                <th>Chức năng
                    <span class="span_tbody"></span>
                </th>
            </tr> 
            <? while($item = mysql_fetch_assoc($wh_info->result)){?>           
                <tr class="font_s14 line_16 color_grey">
                    <td><?= $stt++ ?></td>
                    <td><?= phieu($item['dsvt_id'],$item['dsvt_maVatTuThietBi']); ?></td>
                    <td style="text-align: left;">
                        <a class="color_blue line_16 font_s14 font_w500" href="/danh-sach-vat-tu-thiet-bi-chi-tiet-<?= $item['dsvt_id'] ?>.html"><?= $item['dsvt_name']; ?></a>
                    </td>
                    <td style="text-align: left;"><?=$item['nvt_name']; ?></td>
                    <td>1000</td>
                    <td><?=$item['dvt_name'] ?></td>
                    <td style="text-align: left;"><?=$item['hsx_name']; ?></td>
                    <td><?=$item['xx_name'] ?></td>
                    <td>
                        <div class="d_flex align_c p_set_ft" id="p_set_ft">
                            <img src="../images/pen_blu.png" alt="" style="margin-right:0px;">
                            <p class="font_s14 line_h16 color_blue font_w500 cursor_p" onclick="id_thiet_lap(this)" data="<?= $item['dsvt_id'] ?>">Thiết lập tồn kho</p>
                        </div>
                    </td>
                </tr>
            <?}?>                       
        </table>
    </div>
    <div class="pre_q d_flex align_c flex_center position_a">
        <span class="pre_arrow"></span>
    </div>
    <div class="next_q d_flex align_c flex_center position_a">
        <span class="next_arrow"></span>
    </div>
</div>
<div class="d_flex align_c space_b">
    <div class="d_flex align_c">
        <p class="line_h16 font_s14 color_grey">Hiển thị:</p>
        <div class="d_flex align_c space_b list_choonse_item ml_10 position_r cursor_p">
            <div class="d_flex align_c" onclick="toggle('list_item_choonse_show')">
                <p class="line_h16 font_s14 color_grey">40</p>
                <img class="ml_10" src="../images/list_item.png" alt="">
            </div>
            <div class="list_item_choonse_show position_a" style="display: none;">
                <p class="item_choonse_show text_a_c font_s14 line_16 color_grey">10</p>
                <p class="item_choonse_show text_a_c font_s14 line_16 color_grey">20</p>
                <p class="item_choonse_show text_a_c font_s14 line_16 color_grey">40</p>
                <p class="item_choonse_show text_a_c font_s14 line_16 color_grey">80</p>
                <p class="item_choonse_show text_a_c font_s14 line_16 color_grey">100</p>
            </div>
        </div>
    </div>
    <div class="d_flex align_c">
        <div class="item_pagination cursor_p">
            <img src="../images/back_item_g.png" alt="">
        </div>
        <p class="item_pagination ml_15 font_s13 line_h15 color_grey3 number_item_pagination cursor_p">1</p>
        <p class="item_pagination ml_10 font_s13 line_h15 color_grey3 number_item_pagination cursor_p">2</p>
        <p class="item_pagination ml_10 font_s13 line_h15 color_grey3 number_item_pagination cursor_p">3</p>
        <p class="item_pagination ml_10 font_s13 line_h15 color_grey3 number_item_pagination cursor_p">4</p>
        <p class="item_pagination ml_10 font_s13 line_h15 color_grey3 number_item_pagination cursor_p">5</p>
        <div class="item_pagination  ml_15  next_item_g cursor_p">
            <img src="../images/next_item_g.png" alt="">
        </div>
    </div>
</div>